# Infra

Infrastructure ('infra') refers to all of the servers and tools which the
Chromium project depends on for development.

Most of these are maintained at Google by a dedicated team, but (almost) all of
these are modifiable by anyone in the Chromium project.

To check out (most) of the Chromium Infra code use:

    fetch infra

**Note**: you probably should **NOT** try and work on infrastructure code from a
Windows box. This is somewhat based on lore, but generally infrastructure
scripts aren't developed on Windows and tend to break there. For example, 'fetch
infra' currently fails on Windows.

Join [infra-dev@chromium.org](mailto:infra-dev@chromium.org) for discussion of
Chromium infrastructure related issues.

Most of our documentation is auto-generated from files in the infra
respositories. You can see it here:

*   [Primary
    docs](https://chromium.googlesource.com/infra/infra/+/master/doc/index.md)
*   [Depot Tools man pages and
    tutorial](https://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools.html)

Services

** Service**** Source**** Repository** gclient, git cl, etc.
[depot_tools](https://chromium.googlesource.com/chromium/tools/depot_tools.git)
Chromium[codereview.appspot.com](http://codereview.appspot.com)[ Rietveld
(chromium
branch)](https://code.google.com/p/rietveld/source/browse/?name=chromium)
code.google.com[chromium.googlesource.com](http://chromium.googlesource.com)
[Gitiles](https://code.google.com/p/gitiles/)
code.google.com[crrev.com](http://crrev.com)
[infra/appengine/cr_rev](https://chromium.googlesource.com/infra/infra/+/master/appengine/cr_rev/)
Infra
[chromium-status](http://chromium-status.appspot.com), blink-status, etc.
[infra/appengine/chromium_status](https://chromium.googlesource.com/infra/infra/+/master/appengine/chromium_status/)
Infra buildbot masters (build.chromium.org/p/\*)
[build/masters](https://chromium.googlesource.com/chromium/tools/build/+/master/masters/)
Infra
[test-results.appspot.com](http://test-results.appspot.com)
[blink/Tools/TestResultsServer](http://blink.lc/blink/tree/Tools/TestResultServer)
Blink [sheriff-o-matic.appspot.com](http://sheriff-o-matic.appspot.com)
[blink/Tools/GardeningServer](http://blink.lc/blink/tree/Tools/GardeningServer)
(frontend)
[infra/tools/builder_alerts/](https://chromium.googlesource.com/infra/infra/+/master/infra/tools/builder_alerts/)
(backend) Blink
Infra gatekeeper (thing that closes the tree), recipes (modern builder
configuration language), swarming (next-gen parallel testing)
[build/scripts/slave](https://chromium.googlesource.com/chromium/tools/build/+/master/scripts/slave/)
Infra [chrome-infra-stats.appspot.com](http://chrome-infra-stats.appspot.com)
(stats on builds)
[infra/appengine/chrome_infra_stats](https://chromium.googlesource.com/infra/infra/+/master/appengine/chrome_infra_stats/)
Infra

Google employees can find more information at the [internal Chrome
Infrastructure](https://sites.google.com/a/google.com/chrome-infrastructure/)
site.
