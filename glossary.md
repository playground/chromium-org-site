# Glossary

## Acronyms

*   **CL**: "Change List", a set of changes to files (akin to a single git
    commit).
*   **CQ**: "Commit Queue", infrastructure to automatically
    check/build/test/verify/etc... CLs before merging into the tree. See also
    the [Chromium CQ](developers/testing/commit-queue/index.md) and [Chromium OS
    CQ](developers/tree-sheriffs/sheriff-details-chromium-os/commit-queue-overview/index.md)
    pages.
*   **[CRX file](https://developer.chrome.com/extensions/crx.html):** CRX files
    are ZIP files with a special header and the .crx file extension used to
    package Extensions and Apps.
*   **DUT**: "Device under test", used to refer to the system running Chromium
    \[OS\] and where tests are being executed.
*   **GTTF**: "Green Tree Task Force"
*   **LGTM**: "Looks good to me", commonly used to approve a code review.
*   **LKCR**: "Last known compilable revision" - similar to LKGR (below), just
    taking only the compile into account, no tests.
*   **[LKGR](http://chromium-status.appspot.com/lkgr)**: "Last known good
    revision", the last build that passed a minimal set of tests on the
    buildbots.
*   **LKGM**: "Last known good manifest", the last manifest version that passed
    a minimal set of tests.
*   **MVP**: "Minimum viable product", used to refer to the subset of a feature
    we want to ship initially.
*   **PFQ**: "Preflight queue", used to describe bot configurations in the
    waterfall that run to test/gate changes before they're allowed into the tree
    for everyone to see. See also the [PFQ
    FAQ](developers/tree-sheriffs/sheriff-details-chromium-os/pre-flight-queue-faq.md).
*   **PTAL**: "Please take a\[nother\] look", often used when someone is happy
    with the state of a CL and want reviewers to look \[again\].
*   **RSLGTM**: "Rubber stamp looks good to me", used when the reviewer is
    merely granting OWNERS approval without doing a proper code review.
*   **SGTM**: ~~Secret Google Time Machine~~ "Sounds good to me"
*   **[TBR](https://chromium.googlesource.com/chromium/src/+/master/docs/code_reviews.md#TBR-To-Be-Reviewed)**:
    "To be reviewed". In [specific
    circumstances](https://chromium.googlesource.com/chromium/src/+/master/docs/code_reviews.md#TBR-To-Be-Reviewed)
    used to land code and have it reviewed later.
*   **ToT**: "Tip of Tree", as in the latest revision of the source tree.
*   **[TPM](http://en.wikipedia.org/wiki/Trusted_Platform_Module)****:**
    "Trusted Platform Module", Tamper-resistant chip that the CPU can talk to.
    Securely stores keys and does cryptographic ops. We use this to encrypt the
    keys used to encrypt user files (to make passphrase recovery more
    difficult). See also
    [TpmQuickRef](https://code.google.com/p/chromium/wiki/TpmQuickRef).
*   **WAI**: "Working As Intended", e.g. the behavior described is not a bug,
    but working as it is supposed to. This is not to say the intention cannot
    change (as a feature request), simply that it is not a bug.
*   **WIP**: "Work In Progress" - e.g. a patch that's not finished, but may be
    worth an early look

### English Acronyms and Abbreviations

*   **AFAICT:** as far as I can tell
*   **AFAIK**: as far as I know
*   **e.g.**: (latin) for example
*   **FWIW**: for what it's worth
*   **IANAL**: I am not a lawyer
*   **IIRC**: if I recall/remember correctly
*   **IIUC**: if I understand correctly
*   **IMO**: in my opinion
*   **IMHO**: in my honest opinion
*   **IOW**: in other words
*   **i.e.**: (latin) in other words
*   **nit**: short for "nitpick"; refers to a trivial suggestion such as style
    issues
*   **PSA**: public service announcement
*   **WRT**: with respect to

## Chrome Concepts

*   **Chrome Component:** Components of chrome that can be updated independently
    from Chrome its self. Examples are PDF Viewer, Flash Plugin.
*   **Component App / Component Extension**: App or Extension built and shipped
    with Chrome. Examples are [Bookmark
    Manager](https://code.google.com/p/chromium/codesearch#search/&sq=package:chromium&type=cs&q=bookmark_manager),
    [File
    manager](https://code.google.com/p/chromium/codesearch#search/&sq=package:chromium&type=cs&q=file_manager).
*   **Default Apps:** Apps or Extensions that are shipped with Chrome as .CRX
    files and installed on first run.
*   **[Extension](http://developer.chrome.com/extensions)**: Third party
    developed code that modifies the browser.
*   **[Packaged App](http://developer.chrome.com/apps)**: Packaged apps run
    outside of the browser, are built using web technologies and privileged
    APIs.
*   **Packaged App (old)**: Older packaged apps (pre 2013) still ran in tabs,
    but with offline packaged resources.
*   **Shared Modules:** Extensions or Apps that export resources accessible from
    other Ext/Apps. Dependencies are installed automatically.
*   **Aura**: The unified graphics compositor
    ([docs](developers/design-documents/aura/index.md)).
*   **Ash**: The Aura shell (e.g. the Chromium OS look); see Aura for more info.

## Building

*   **buildbot**: A column in the build waterfall, or the slave (machine)
    connected to that column, or the [build waterfall
    infrastructure](http://dev.chromium.org/developers/testing/chromium-build-infrastructure/tour-of-the-chromium-buildbot)
    as a whole.
*   **clobber**: To delete your build output directory.
*   **component build**: A shared library / DLL build, not a static library
    build.
*   **land**: Landing a patch means to commit it.
*   **slave**: A machine connected to the buildbot master, running a sequence of
    build and test steps.
*   **[tryserver](http://build.chromium.org/buildbot/try-server/waterfall)**: A
    machine that runs a subset of all tests on all platforms.
*   **sheriff**: The person currently charged with watching over the build
    waterfall to make sure it stays green (not failing). There are usually two
    sheriffs at one time. The current sheriffs can be seen in the upper left
    corner of the
    [waterfall](http://build.chromium.org/buildbot/waterfall/waterfall) page.
*   **symbolication**: The process of resolving stack addresses and backtraces
    to human readable source code methods/lines/etc...
*   **tree**: This means the source tree in subversion. Often used in the
    context of "the tree is closed" meaning commits are currently disallowed.
*   **try**: To try a patch means to submit it to the tryserver before
    committing.
*   **[waterfall](http://build.chromium.org/buildbot/waterfall/waterfall)**: The
    page showing the status of all the buildbots.

## General

*   **Flakiness**: Intermittent test failures (including crashes and hangs),
    often caused by a poorly written test.
*   **Jank/Jankiness**: User-perceptible UI lag.
*   **Chumping**: Bypassing the CQ and committing your change directly to the
    tree. Generally frowned upon as it means automatic testing was bypassed
    before the CL hits developer systems.

## User Interface

*   **Bookmark bubble**: A "modal" bubble that appears when the user adds a
    bookmark allowing them to edit properties or cancel the addition.
*   **Download bar**: The bar that appears at the bottom of the browser during
    or after a file has been downloaded.
*   **Extensions bar**: Similar to the download bar, appears at the bottom of
    the screen when the user has installed an extension.
*   **Infobar**: The thing that drops down below asking if you want to save a
    password, did you mean to go to another URL, etc.
*   **NTB**: New Tab button (the button in the tab strip for creating a new tab)
*   **NTP or NNTP**: The New Tab Page, or the freshly rebuilt new tab
    functionality dubbed New New Tab Page.
*   **Status bubble**: The transient bubble at the bottom left that appears when
    you hover over a url or a site is loading.

## Video

*   **channels**: The number of audio channels present. We use "mono" to refer
    to 1 channel, "stereo" to refer to 2 channels, and "multichannel" to refer
    to 3+ channels.
*   **clicking**: Audio artifacts caused by bad/corrupted samples.
*   **corruption**: Visible video decoding artifacts. Usually a result of
    decoder error or seeking without fully flushing decoder state. Looks similar
    to
    [this](http://6541078575799853287-a-chromium-org-s-sites.googlegroups.com/a/chromium.org/dev/developers/common-terms--techno-babble/corruption.png).
*   **FFmpeg**: The open source library Chromium uses for decoding audio and
    video files.
*   **sample**: A single uncompressed audio unit. Changes depending on the
    format but is typically a signed 16-bit integer.
*   **sample bits**: The number of bits per audio sample. Typical values are 8,
    16, 24 or 32.
*   **sample rate**: The number of audio samples per second. Typical values for
    compressed audio formats (AAC/MP3/Vorbis) are 44.1 kHz or 48 kHz.
*   **stuttering**: Short video or audio pauses. Makes the playback look/sound
    jerky, and is often caused by insufficient data or processor.
*   **sync**: Audio/video synchronization.

## Toolchain (compiler/debugger/linker/etc...)

*   **ASan, LSan, MSan, TSan**:
    [AddressSanitizer](developers/testing/addresssanitizer.md),
    [LeakSanitizer](developers/testing/leaksanitizer.md),
    [MemorySanitizer](developers/testing/memorysanitizer.md) and
    [ThreadSanitizer](developers/testing/threadsanitizer-tsan-v2.md), bug
    detection tools used in Chromium testing. ASan detects addressability issues
    (buffer overflow, use after free etc), LSan detects memory leaks, MSan
    detects use of uninitialized memory and TSan detects data races.
*   **AFDO**: Automatic FDO; see FDO & PGO.
*   **FDO**: Feedback-Directed Optimization; see AFDO & PGO.
*   **fission**: A new system for speeding up processing of debug information
    when using GCC; see [this page](http://gcc.gnu.org/wiki/DebugFission) for
    more details.
*   **gold**: The GNU linker; a newer/faster open source linker written in C++
    and supporting threading.
*   **ICE**: Internal Compiler Error; something really bad happened and you
    should file a bug.
*   **PGO**: Profile Guided Optimization; see AFDO & FDO.

## Chromium OS

*   **board**: The name of the system you're building Chromium OS for; see the
    [official Chrome OS device
    list](chromium-os/developer-information-for-chrome-os-devices/index.md) for
    examples.
*   **devserver**: System for updating packages on a Chromium OS device without
    having to use a USB stick or doing a full reimage. See the [Dev Server
    page](chromium-os/how-tos-and-troubleshooting/using-the-dev-server/index.md).
*   **powerwash**: Wiping of the stateful partition (system & all users) to get
    a device back into a pristine state. The TPM is not cleared, and Lockbox is
    kept intact (thus it is not the same as a factory reset). See the [Powerwash
    design doc](chromium-os/chromiumos-design-docs/powerwash.md).
