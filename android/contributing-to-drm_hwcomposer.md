# Contributing to drm_hwcomposer

### Code Overview

For an overview of the drm_hwcomposer code, [click
here.](drm_hwcomposer-overview.md)

### Viewing Patches

To view the in-review patches for drm_hwcomposer, visit [this
page](https://chromium-review.googlesource.com/#/q/project:chromiumos/drm_hwcomposer+status:open).
To view \*all\* patches for drm_hwcomposer, visit [this
page](https://chromium-review.googlesource.com/#/q/project:chromiumos/drm_hwcomposer).

### Discussing Potential Patches

If you have an idea for a patch and want to discuss it before committing to the
work, consult someone in the [OWNERS
file](https://chromium.googlesource.com/chromiumos/drm_hwcomposer/+/master/OWNERS)
on #dri-devel IRC channel (freenode) or via email.

### Submitting Patches

### In your <android_src>/external/drm_hwcomposer directory, add the chromium upstream as a remote and fetch the code:

    git remote add -f <remote> https://chromium.googlesource.com/chromiumos/drm_hwcomposer

Create your working branch to track cros/master:

    git branch -t <branch> <remote>/master && git checkout <branch>

Make your awesome changes.

Ensure that your change conforms with the [Google C++ style
guide](https://google.github.io/styleguide/cppguide.html) (modulo some custom
goo added in .clang-format, located in the drm_hwcomposer directory). The
following command will show you which parts of your diff do not conform. If
you're happy with the proposed change, you can call clang-format-diff again with
the -i argument to apply the changes directly to your files.

    git diff | clang-format-diff-3.5 -p 1 -style=file

Once your patch has been tested and conforms to the style guide, commit it to
your local tree.

    git commit -as

In the commit message, ensure that you have the following:

*   Signed-off-by line (git commit -s will do this for you)
*   Subject prefixed with drm_hwcomposer (yeah yeah yeah, if all the subjects
    are prefixed, then none are. Deal with it.)
*   If you are fixing a bug that's on the tracker, add a BUG= line with the
    tracker reference. If there is no bug on the tracker, strongly consider
    creating one. If you still don't think your change warrants a bug, use
    BUG=None.
*   TEST= line to describe how you tested the change.

    drm_hwcomposer: Fix *ALL* the bugs
    This change fixes all of the bugs.
    BUG=chromium:1337
    TEST=Tested drm_hwcomposer on ryu, no crashy-crashy
    Signed-off-by: Sean Paul <seanpaul@chromium.org>

Before merging your change, you or your company must have signed the
Contributors License Agreement (CLA). Check out the [Legal section of this
page](../developers/contributing-code/index.md).

Once you're happy with the change, upload it to Gerrit for a code review:

    git push <remote> <branch>:refs/for/master

Gerrit will append a Change-Id to your commit and upload the patch for review.
Take a look at the OWNERS file in the root directory of drm_hwcomposer and add
reviewers from there (best to add everyone, or just zachr).

Once your change has been reviewed, it will be submitted by an owner.
