window.addEventListener("load", onLoad, true);
document.addEventListener("mouseover", onMouseOver, true);

function onLoad() {
}

function onMouseOver() {
  var time = new Date();
  return "onMouseOver: " + time;
}

function loadDynamicScript() {
  var request = new XMLHttpRequest();
  request.open('GET','dynamicScript.js', true);
  request.send();
  request.onreadystatechange = function() {
    if (request.readyState != 4)
      return;
    eval(request.responseText);
    document.getElementById("dynamicScriptFunctionButton").disabled = false;
    document.getElementById("loadDynamicScriptButton").disabled = true;
  }
}

function append() {
  var parentElement = document.getElementById("parent");
  var childElement = document.createElement("div");
  childElement.textContent = "child element";
  childElement.style.border = "solid 1px";
  childElement.style.margin = "5px";
  parentElement.appendChild(childElement);
}

function retrieveData() {
  var request = new XMLHttpRequest();
  request.open('GET','data.txt', true);
  request.send();
}
