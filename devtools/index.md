# Google Chrome Developer Tools

The docs for [Google Chrome Developer
Tools](https://developers.google.com/web/tools/chrome-devtools/) has moved.

*   [How to contribute to the
    DevTools](https://developers.google.com/chrome-developer-tools/docs/contributing)
*   [Capturing a Timeline Trace](capturing-a-timeline-trace/index.md)
