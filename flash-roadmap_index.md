# Flash Roadmap

## Upcoming Changes

### HTML5 By Default (Target: Chrome 55+ - Dec, 2016)

#### Summary

Navigator.Plugins() and Navigator.MimeTypes() will only report the presence of
Flash Player if the user has indicated that the domain should execute Flash. If
a site offers an HTML5 experience, this change will make that the primary
experience. We will continue to ship Flash Player with Chrome, and if a site
truly requires Flash, a prompt will appear at the top of the page when the user
first visits that site, giving them the option of allowing it to run for that
site (see the
[proposal](https://docs.google.com/presentation/d/106_KLNJfwb9L-1hVVa4i29aw1YXUy9qFX-Ye4kvJj-4/edit#slide=id.p)
for the mock-ups).

#### Announcements/ Artifacts

*   [Proposal](https://docs.google.com/presentation/d/106_KLNJfwb9L-1hVVa4i29aw1YXUy9qFX-Ye4kvJj-4/edit#slide=id.p)
*   [Design
    Doc](https://docs.google.com/a/chromium.org/document/d/1QD3HQdkBgJea_92f5rars06Bd_SJz5IXjuNHhBuWCcg/edit?usp=sharing)
*   [Intent to
    Implement](https://groups.google.com/a/chromium.org/forum/#!searchin/chromium-dev/HTML5$20by$20default/chromium-dev/0wWoRRhTA_E/__E3jf40OAAJ)
*   [Site Engagement
    Scoring](https://www.chromium.org/developers/design-documents/site-engagement)
*   [Updated Intent to Implement (using Site Engagement
    scoring)](https://groups.google.com/a/chromium.org/d/msg/chromium-dev/ad7Posd6cdI/5EEOduWiCwAJ)

#### Status

HTML5 by default has shipped and we are currently in the process of ramping up
the SEI threshold, per the schedule below.

Currently 87.5% of population have an SEI threshold score of 4 and 12.5% has a
threshold score of 8 (we do this to measure the impact of the threshold change).
By the end of the month we will progress on to the next phase on the ramp and
everyone will be at least at 8.

#### Shipping Schedule

HTML5 by Default was initially rolled out to 1% of Chrome 55 Stable users
(December), followed by a full deployment (i.e. to 100% of users) in Chrome 56
Stable (February).

Flash prompting will only be enabled for sites whose [Site Engagement
Index](https://www.chromium.org/developers/design-documents/site-engagement)
(SEI) is below a certain threshold. For Chrome 55, starting in January 2017
prompts will only appear for sites where the user’s SEI is less than 1. That
threshold will increase to 100 through October 2017, when all Flash sites will
require an initial prompt. As a reminder, users will only be prompted once per
site.

Here’s a summary of thresholds and % of users:

Site Engagement Threshold

User % Enabled

January 2017

1 (Stable 55)

1% (Stable 55), 50% (Beta 56)

February 2017

2

100% (Stable 56)

March 2017

4

100%

April 2017

8

100%

May 2017

16

100%

June 2017

32

100%

July 2017

32

100%

August 2017

32

100%

September 2017

64

100%

October 2017

100

100%

#### Developer Recommendations

Ultimately we recommend migrating towards HTML5 content, however for sites that
still require Flash Player in the interim we recommend presenting users with
either one of the following enablement options:

*   (Best approach) Present a link/ image to "Enable" Flash Player that pointed
    to "<https://get.adobe.com/flashplayer/>." When users click on that link
    Chrome will present the necessary UI to enable Flash Player for the site. It
    will look something like
    [this](https://docs.google.com/presentation/d/106_KLNJfwb9L-1hVVa4i29aw1YXUy9qFX-Ye4kvJj-4/edit#slide=id.g1270f83468_0_12).
*   (Alternate approach) Provide the following instructions to your users:
        *   Go to "chrome://settings/contentExceptions#plugins"
        *   Add an Allow entry for "\[\*.\]yourdomain.com"

### PPS Tiny - Remove Un-sized (0x0 or hidden) content Exceptions (Target: Chrome 59 - June, 2017)

#### Summary

We are removing the exception for 0x0 (unsized/hidden) content from Plugin Power
Savings Mode.

#### Rationale

We originally intended on blocking this type of content in the PPS Tiny launch,
however due to a technical oversight we unintentionally left the exception in.
This change brings the implementation in line with what we originally
communicated and intended.

#### Announcements/ Artifacts

N/A

### PPS Tiny - No Same Origin Exceptions (Target: Chrome 60 - Aug, 2017)

#### Summary

We are removing the final exception for Plugin Power Savings Mode, which
permitted small (5x5) content, hosted on the same origin, to run.

#### Rationale

The exception was meant to be a temporary relief for smaller developers, for
features that are well now very supported by the web platform (e.g. clipboard
access, audio streams, etc...).

#### Announcements/ Artifacts

*   [Intent to
    Implement](https://groups.google.com/a/chromium.org/d/msg/chromium-dev/Elg7Vhpeb38/zQCvXQ_vAAAJ)

### Unify Flash Settings (Target: Chrome 61 - Sept, 2017)

#### Summary

Reduce the number of Flash Player modes from 3 options (Always Allow, Ask,
Block) to 2 (a single toggle for Ask or Block).

#### Rationale

The intent is to simplify the user choice down to a single option, enable Flash
Player (default == enabled), that is easy for users to understand. Power users
will be able to add exceptions (including those with wildcards) explicitly Allow
Flash to run.

#### Announcements/ Artifacts

*   TBD

### Universal Click to Play (Target: Chrome 66+ - Apr/Jun, 2018)

#### Summary

Click to Play will be enabled for all Flash Player instances, even on sites
where Flash Player is allowed to run per HTML5 by Default.

#### Rationale

Require affirmative user choice to run Flash Player content, without that choice
persisting across multiple sessions.

#### Announcements/ Artifacts

*   TBD

## Shipped Changes

### Plugin Power Savings Mode (Shipped: Chrome 42 - Sept, 2015)

#### Summary

Chrome pauses non-essential(1) Flash Content, by replacing the plugin content
with a static image preview and a play button overlayed. Users can re-enable
this content by clicking play.

(1) - Non-essential content being smaller than 300x400 pixels or smaller than
5x5 pixels.

#### Rationale

Limit Flash Playbacks to visible main body content (e.g. video, games, etc...)
and still permit streaming audio services to function.

#### Announcements/ Artifacts

*   [Design
    Doc](https://docs.google.com/document/d/1r4xFSsR4gtjBf1gOP4zHGWIFBV7WWZMgCiAHeepoHVw/edit#%5C)

### Plugin Power Savings Mode - Tiny (Shipped: Chrome 53 - Sept, 2016)

#### Summary

A further restriction to Plugin Power Savings Mode that removes the ability to
run 5x5 or smaller content, from a different origin.

#### Rationale

Much of this content (5x5 below) was used for viewability detection (i.e. to see
if an ad was on that page), requiring Chrome to spin up a relatively expensive
(in terms of performance) Flash process in order for the site to infer
viewability.

With the introduction of Intersection Observer in Chrome 51, which added
platform support for this use case, there was no longer a need to continue
granting this exception.

We left an exception for "same origin" 5x5 Flash content, to give smaller sites
(e.g. using things like clipboard access) time to migrate.

#### Announcement/ Artifacts

*   [Intent to
    Implement](https://groups.google.com/a/chromium.org/forum/#!topic/chromium-dev/QL2K4yFVg_U)

### YouTube Embed Re-Writer (Shipped: Chrome 54 - Oct, 2016)

#### Summary

Chrome to automatically use the HTML5 content of a YouTube embed when the Flash
one is used.

#### Rationale

This will allow the long tail of websites that never updated to the HTML5 embeds
to no longer require Flash for Chrome users, thus reducing overall usage of
Flash in Chrome.

#### Announcement/ Artifacts

*   [Intent to
    Implement](https://groups.google.com/a/chromium.org/forum/#!msg/chromium-dev/BW8g1iB0jLs/OeWeRCTuBAAJ)
*   [Design
    Doc](https://docs.google.com/document/d/1xHBaX2WhfZVmfeWyLNOX76l9cPiXrQNXqRrUMtlvfks/edit)

### De-couple Flash Player (Shipped: Chrome 54 - Oct, 2016)

#### Summary

Chrome to exclusively use the component updater to distribute Flash Player, and
separating it from Chrome's default distribution bundle.

#### Rationale

Enable Chrome to rapidly distribute Flash Player updates, without re-building
the core product, making it easier to match Adobe's monthly release cadence.

This feature was fundamentally technology gated, requiring development of
in-line on-demand Flash component installs, differential component updates, and
building out special serving infrastructure.

#### Announcement/ Artifacts

N/A

## Relevant Links

### Adobe

*   [Flash, HTML5 and Open Web
    Standards](https://blogs.adobe.com/conversations/2015/11/flash-html5-and-open-web-standards.html)
    (November 2015)

### Apple

*   [Next Steps for Legacy
    Plug-ins](https://webkit.org/blog/6589/next-steps-for-legacy-plug-ins/)
    (June 2016)

### Microsoft

*   [Putting Users in Control of
    Flash](https://blogs.windows.com/msedgedev/2016/04/07/putting-users-in-control-of-flash/)
    (April 2016)
*   [Extending User Control of Flash with
    Click-to-Run](https://blogs.windows.com/msedgedev/2016/12/14/edge-flash-click-run/)
    (December 2016)

### Mozilla

*   [Mozilla Flash
    Roadmap](https://developer.mozilla.org/en-US/docs/Plugins/Roadmap)
*   [NPAPI Plugins in
    Firefox](https://blog.mozilla.org/futurereleases/2015/10/08/npapi-plugins-in-firefox/)
    (October 2015)
*   [Reducing Adobe Flash Usage in
    Firefox](https://blog.mozilla.org/futurereleases/2016/07/20/reducing-adobe-flash-usage-in-firefox/)
    (July 2016)
