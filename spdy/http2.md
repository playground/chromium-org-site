# HTTP/2

## Instructions

1.  Get and build the latest Chromium source:
    <https://code.google.com/p/chromium/wiki/UsingGit>, **OR** join the [Chrome
    Canary](https://www.google.com/intl/en/chrome/browser/canary.html) channel.

2.  Run Chrome with the "--enable-spdy4" command-line flag (an internal name for
    HTTP/2).

3.  Enjoy!

    Contact [Bence Béky](mailto:bnc@chromium.org) if you have questions.
