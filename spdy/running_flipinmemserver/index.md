# Running flip_in_mem_edsm_server

Background:

flip_in_mem_edsm_server is a "simple" webserver binary intended to be used for
benchmarking and comparison of HTTP and SPDY.

It can also be used as a SPDY<->HTTP reverse proxy to further enable deployment
or testing.

In its original (server) mode, you need to provide it content (which it will
cache in-memory, so nothing too large!), possibly some cert files (if using
SPDY), and configure its options properly.

This guide assumes that you've checked out the latest chromium code, and your
current working directory is chromium/src.

In case you're wondering, EDSM stands for event driven state machine.

Generating the needed SSL information:

If you don't already have a test key.pem and certificate.pem, you can generate
one like so:

`openssl genrsa -out key.pem 1024`

`openssl req -new -key key.pem -out request.pem # and answer the questions at
the prompt with whatever`

`openssl x509 -req -days 30 -in request.pem -signkey key.pem -out cert.pem`

This will leave you with testkey.pem and testcertificate.pem which you can use
with the server.

Compiling flip_in_mem_edsm_server

These days, all of the SSL dependencies have been checked into the tree. All you
need to do is:

ninja -C out/Debug flip_in_mem_edsm_server

Generating Benchmark Data

If running flip_in_mem_edsm_server in server mode, you'll want something for it
to serve. The easiest way to generate that data is to launch chrome with the
following command-line-flag

chrome --record-mode

Chrome will then store everything in the cache... so you'll want to go and visit
whatever sites you'd like to benchmark later-- chrome will be storing that on
disk as you go. Keep in mind it stores the whole shebang, including cookies, so
be aware if sending these files to your colleagues!

Unfortunately, you're not done yet. We still have to dump the chrome cache files
into a format that the flip_in_mem_edsm_server can read. That will require
another binary, so lets build it now: As far as I know, this binary only works
on Windows.

ninja -C out/Debug flip_in_mem_edsm_server

Running flip_in_mem_edsm_server

Assuming that you already have cached data from Chrome that you want to
benchmark and the SSL files (from above), you'd stick all of those files into:

$CWD/GET_ (assuming that the method of the requests was GET. If you wanted
responses to a request with HEAD method, then you'd stick the appropriate
responses into a directory named HEAD_, etc).

You'd put the test cert and key into the $CWD/cert.pem and $CWD/key.pem,
respectively.

At this point you can run the flip_in_mem_edsm_server by doing:

`./out/Debug/flip_in_mem_edsm_server`

By default, the server will look for key.pem and cert.pem in the
current-working-directory, and will run HTTPS on port 16002, and SPDY (SSL) on
port 10040.

Another common use-case would be to want to serve HTTP and SPDY (NO-SSL). To do
this, find FLAGS_use_ssl = true; and change it to FLAGS_use_ssl = false; then
recompile.

Yet another common use-case would be to serve files which were \*not\* captured
by Chrome's record mode. To do this, you'll want to turn off URL encoding (or
figure out how to encode your URLs..).

You can turn off encoding by finding FLAGS_need_to_encode_url = true; and
changing the 'true' to 'false' and then (of course) recompiling.

The binary supports these flags:

-accept_backlog_size (The size of the TCP accept backlog) type: int32

default: 1024

-accepts_per_wake (The number of times that accept() will be called when

the alarm goes off when the accept_using_alarm flag is set to true. If

set to 0, accept() will be performed until the accept queue is completely

drained and the accept() call returns an error) type: int32 default: 0

-cache_base_dir (The directory where cache locates) type: string

default: "."

-flip_port (The port on which the flip server listens) type: int32

default: 10040

-need_to_encode_url (If true, then encode url to filename) type: bool

default: true

-no_nagle (If true, then disables the nagle algorithm) type: bool

default: true

-pageload_html_file (The path to the html that does the pageload in iframe)

type: string

default: "experimental/users/fenix/flip/loadtime_measurement.html"

-port (The port on which the http server listens) type: int32

default: 16002

-record_mode (If set to true, record requests in file named as fd used)

type: bool default: false

-record_path (The path to save the record files) type: string default: "."

-response_count_until_close (The number of responses given before the

server closes the connection) type: int32 default: 1000000

-reuseport (If set to false a single socket will be used. If set to true

then a new socket will be created for each accept thread. Note that this

only works with kernels that support SO_REUSEPORT) type: bool

default: false

-server_think_time_in_s ( The amount of time the server delays before

sending back thereply) type: double default: 0

-ssl_cert_name (The name of the cert .pem file) type: string

default: "cert.pem"

-ssl_key_name (The name of the key .pem file) type: string

default: "key.pem"

-urls_file (The path to the urls file which includes the urls for testing)

type: string default: "experimental/users/fenix/flip/urls.txt"

-use_compression (Does the server compress data frames) type: bool

default: false

-use_cwnd_opener (Does the server advance cwnd by sending no-op packets)

type: bool default: false

-use_ssl (If set to true, then the server will act as an SSL server for

both HTTP and FLIP) type: bool default: true

-use_xac (Does the server send X-Associated-Content headers) type: bool

default: false

-use_xsub (Does the server send X-Subresource headers) type: bool

default: false

If you're not encoding your filenames, you'll need to modify the value of the
--need_to_encode_url variable to be false.

Running chromium so that it talks with your locally running
flip_in_mem_edsm_server

**THIS SECTION IS OUT OF DATE**

You should be able to

Cut and paste the following into a file:

`#!/bin/bash`

`if [ "$(dpkg --print-architecture)" == "amd64" ]; then`

` echo -ne "\n\nYou have a 64 bit computer\n\n"`

` linux_versions="linux64";`

`else`

` echo -ne "\n\nYou have a 32 bit computer\n\n"`

` linux_versions="linux";`

`fi`

`for linux_version in $linux_versions; do`

` install_dir=$HOME/spdy-chrome-canary/$linux_version`

` if mkdir -p $install_dir; then`

` echo "Directory: \"$install_dir\" made or already existed"`

` else`

` echo "$install_dir exists, but is not a directory."`

` echo "Please remove whatever is there so this script can proceed next time."`

` exit 128`

` fi`

` pushd $install_dir`

` install -d chrome-linux-old`

` mv chrome-linux chrome-linux-old/chrome-linux-\`date +"%F-%k-%M-%S-%N"\``

` rm -rf chrome-linux chrome-linux-old chrome-linux.zip`

` wget
http://build.chromium.org/buildbot/continuous/$linux_version/LATEST/chrome-linux.zip`

` unzip chrome-linux.zip`

` rm -rf chrome-linux.zip`

` popd`

` filename_base=SPDY-Chrome-$linux_version`

` cat >> `$HOME/Desktop/$filename_base.desktop <<-EOF

`[Desktop Entry]`

`Version=1.0`

`Encoding=UTF-8`

`Name=$filename_base`

`Categories=Application;Network;WebBrowser;`

`Exec=$install_dir/chrome-linux/chrome --use-spdy --enable-logging --log-level=0
--user-data-dir=.$filename_base %U`

`Icon=/tmp/chrome-linux/product_logo_48.png`

`MimeType=text/html;text/xml;`

`Terminal=false`

`Type=Application`

`EOF`

` filename_base=SPDY-Chrome-local-server-$linux_version`

` cat >> `$HOME/Desktop/$filename_base.desktop <<-EOF

`[Desktop Entry]`

`Version=1.0`

`Encoding=UTF-8`

`Name=$filename_base`

`Categories=Application;Network;WebBrowser;`

`Exec=$install_dir/chrome-linux/chrome --use-spdy --enable-logging --log-level=0
--user-data-dir=.$filename_base --host-resolver-rules='MAP * localhost:10040'
%U`

`Icon=/tmp/chrome-linux/product_logo_48.png`

`MimeType=text/html;text/xml;`

`Terminal=false`

`Type=Application`

`EOF`

` filename_base=NO-SPDY-Chrome-local-server-$linux_version`

` cat >> `$HOME/Desktop/$filename_base.desktop <<-EOF

`[Desktop Entry]`

`Version=1.0`

`Encoding=UTF-8`

`Name=$filename_base`

`Categories=Application;Network;WebBrowser;`

`Exec=$install_dir/chrome-linux/chrome --enable-logging --log-level=0
--user-data-dir=.$filename_base --host-resolver-rules='MAP * localhost:16002'
%U`

`Icon=/tmp/chrome-linux/product_logo_48.png`

`MimeType=text/html;text/xml;`

`Terminal=false`

`Type=Application`

`EOF`

`done`

Then, run that file (e.g. bash my_filename_here)

This should download the LATEST version of chrome, and create icons for you to
run chromium against a local server in both SPDY and non-SPDY modes.

Since the server is built (by default) with SSL support turned on, SPDY-SSL and
HTTPS will work with it.... HTTP will not. If you wish to benchmark HTTP, you'll
need to find the FLAGS_use_ssl variable in the serve and set that to false and
recompile (sorry!).
