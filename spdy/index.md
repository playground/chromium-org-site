# SPDY

SPDY is an experiment with protocols for the web. Its goal is to reduce the
latency of web pages.

*   Documentation
    *   [SPDY: An experimental protocol for a faster
        web](spdy-whitepaper/index.md)
    *   [SPDY protocol specification](spdy-protocol/index.md)
        *   [Server Push and Server Hint](link-headers-and-server-hint/index.md)
    *   Research
        *   [An Argument For Changing TCP Slow
            Start](An_Argument_For_Changing_TCP_Slow_Start.pdf) - Mike Belshe
            01/11/10
        *   [More Bandwidth Doesn't Matter
            (much)](http://docs.google.com/a/chromium.org/viewer?a=v&pid=sites&srcid=Y2hyb21pdW0ub3JnfGRldnxneDoxMzcyOWI1N2I4YzI3NzE2)
            - Mike Belshe 04/08/10
    *   Standards Work
        *   [SSL Next-Protocol-Negotiation Extension
            ](http://tools.ietf.org/html/draft-agl-tls-nextprotoneg-00.html)-
            Adam Langley 01/20/10
        *   [TLS False
            Start](https://tools.ietf.org/html/draft-bmoeller-tls-falsestart-00)
            - Langley, Modadugu, Mueller 06/2010
        *   [TLS Snap
            Start](http://tools.ietf.org/html/draft-agl-tls-snapstart-00) - Adam
            Langley 06/18/2010
    *   [SPDY Proxy Design](spdy-proxy.md)
    *   [SPDY Proxy Examples](spdy-proxy-examples.md)
    *   [SPDY Server and Proxy Authentication](spdy-authentication.md)
*   Code
    *   Chromium client implementation:
        <http://src.chromium.org/viewvc/chrome/trunk/src/net/spdy/>
    *   Server tools:
        <http://src.chromium.org/viewvc/chrome/trunk/src/net/tools/flip_server/>
        *   [Running flip_in_mem_edsm_server](running_flipinmemserver/index.md)
*   Discuss
    *   <http://groups.google.com/group/spdy-dev>
*   External work
    *   Servers
        *   Jetty Web Server: <http://wiki.eclipse.org/Jetty/Feature/SPDY>
        *   Apache module for SPDY: <http://code.google.com/p/mod-spdy/>
        *   Python implementation of a SPDY server:
            <http://github.com/mnot/nbhttp/tree/spdy>
        *   Ruby SPDY: <https://github.com/igrigorik/spdy>
        *   node.js SPDY: <https://github.com/indutny/node-spdy>
    *   Libraries
        *   Netty SPDY (Java library): <http://netty.io/blog/2012/02/04/>
        *   Ruby wrapper around Chromium SPDY framer:
            <https://github.com/romanbsd/spdy>
        *   Go SPDY: <http://godoc.org/code.google.com/p/go.net/spdy>
        *   Erlang SPDY: <https://github.com/RJ/erlang-spdy>
        *   Java SPDY:
            <http://svn.apache.org/repos/asf/tomcat/trunk/modules/tomcat-lite>
        *   C SPDY (libspdy): <http://libspdy.org/index.html> (to be used by
            libcurl: <http://daniel.haxx.se/blog/2011/10/18/libspdy/>)
        *   C SPDY (spindly): <https://github.com/bagder/spindly>
        *   C SPDY (spdylay): <https://github.com/tatsuhiro-t/spdylay>
        *   iPhone SPDY: <https://github.com/sorced-jim/SPDY-for-iPhone>
    *   Browsers
        *   Google Chrome
        *   Mozilla Firefox
*   Tools
    *   [Summary of SPDY tools](spdy-tools-and-debugging.md)
    *   [Chrome page benchmarking
        tool](../developers/design-documents/extensions/how-the-extension-system-works/chrome-benchmarking-extension/index.md)
