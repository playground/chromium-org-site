# Server Push and Server Hints

In order to deliver resources to browsers more quickly, SPDY employs two
mechanisms, Server Push and Server Hint.

### Server Push

Server Push is where the server pushes a resource directly to the client without
the client asking for the resource. The server is making an assumption here that
pushing the resource is desirable. Pushing a cacheable resource can be risky, as
the browser might already have the resource and the push can be redundant.

Specifications:

*   [SPDY Specification](../spdy-protocol/index.md)

Server Hint

Server Hint is a mechanism where the server can notify the client of a resource
that will be needed before the client can discover it. The server does not send
the entire contents of the resource, but rather just the URL as an early part of
a response. The client can then validate its cache (potentially even eliminating
the need for a GET-if-modified-since), and will formally request the resource
only if needed.

Server Hint is implemented using the LINK header with HTTP, and overlaps with
existing link prefetching semantics.

Specifications:

*   [LINK rel=prefetch from
    Mozilla](https://developer.mozilla.org/en/Link_prefetching_FAQ)
*   [LINK rel=subresource](link-rel-subresource.md)

### Comparison of Server Push and Server Hint

Pro ConServer Push

*   Lowest Latency Delivery Mechanism
    *   Saves 1 round-trip

*   If the client already has the resource cache, the load is wasteful
*   The Application must decide if it should push the resource, the protocol
    cannot know whether the client has a resource

Server Hint

*   Provides early discovery of critical resources
*   Can include etag or last-modified information to avoid over-the-network
    cache validation checks
*   Allows the client to participate in cache validation before sending the
    resource

*   Still requires a round-trip between the client and server to fetch the
    resource.
