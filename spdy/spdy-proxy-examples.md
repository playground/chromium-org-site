# SPDY Proxy Examples

### Introduction

Chrome's support for talking to a SPDY proxy involves a new "HTTPS" proxy type.
When using an HTTPS proxy, Chrome will establish an SSL connection to the proxy.
If the SSL connection negotiated NPN, then Chrome will speak SPDY to the proxy.
Otherwise, it will speak HTTP. In the case of HTTP, an HTTPS proxy behaves
exactly like an HTTP Proxy, except for the SSL connection to the client. In the
case of SPDY, Chrome will use the same HTTP Proxy semantics, but frame using
SPDY.

The case of fetching HTTPS urls is complicated by the fact that regardless of
how the client is speaking to the proxy, an SSL connection will be established
(through a tunnel) to the origin server which may, or may not negotiate SPDY.
Depending on the NPN negotiations between the client and the proxy, and the
client and the origin, there are actually 4 variations:

1.  HTTP to proxy, HTTP to origin
2.  HTTP to proxy, SPDY to origin
3.  SPDY to proxy, HTTP to origin
4.  SPDY to proxy, SPDY to origin

Here are some examples of traffic flows. The steps to set up communication with
the proxy are colored blue. The steps to set up an SSL tunnel are purple. And
the steps to communicate directly with the origin server are green.

HTTPS Proxy

#### HTTP URL

#### #### Here, a client uses an HTTPS proxy to fetch an HTTP url. From the
          perspective of the client, the process is exactly the same as it would
          be if sending the request directly, via HTTP, to the origin server,
          but in this case the request goes to the proxy instead.

#### ####      Client              HTTPS Proxy            Origin Server
#### ####      ------              -----------            -------------
#### ####     *** If an available connection to the proxy exists, jump to step 9 ***
#### ####     *** Client opens TCP connection to Proxy ***
#### ####        |---------------------->|                      |
#### ####        | 1) SYN                |                      |
#### ####        |                       |                      |
#### ####        |<----------------------|                      |
#### ####        | 2) SYN_ACK            |                      |
#### ####        |                       |                      |
#### ####        |---------------------->|                      | 
#### ####        | 3) ACK                |                      |
#### ####        |                       |                      |
#### ####     *** Client performs SSL handshake with Proxy ***
#### ####        |---------------------->|                      |
#### ####        | 4) Client Hello       |                      |
#### ####        |                       |                      |
#### ####        |<----------------------|                      |
#### ####        | 5) Server Hello       |                      |
#### ####        |                       |                      |
#### ####        |---------------------->|                      |
#### ####        | 6) Client Key Exchange|                      |
#### ####        |                       |                      |
#### ####        |---------------------->|                      |
#### ####        | 7) Client Finished    |                      |
#### ####        |                       |                      |
#### ####        |<----------------------|                      |
#### ####        | 8) Server Finished    |                      |
#### ####        |                       |                      |
#### ####     *** Client requests url ***   
#### ####         ---------------------->|                      |
#### ####        |9) GET http://origin/  |                      |
#### ####        |                       |                      |
#### ####        |                    *** Proxy connects (or reuses connection) to origin ***
#### ####        |                       |--------------------->|
#### ####        |                       |10) SYN               |
#### ####        |                       |                      |
#### ####        |                       |<---------------------|
#### ####        |                       |11) SYN_ACK           |
#### ####        |                       |                      |
#### ####        |                       |--------------------->|
#### ####        |                       |12) ACK               |
#### ####        |                       |                      |
#### ####        |                    *** Proxy issues request via HTTP to backend ***
#### ####        |                       |--------------------->|
#### ####        |                       |13) GET http://origin/ HTTP/1.1
#### ####        |                       |                      |
#### ####        |                       |                      |
#### ####        |                    *** Origin sends HTTP response to Proxy ***
#### ####        |                       |<---------------------|
#### ####        |                       |14) HTTP/1.1 200 OK   |
#### ####        |                       |                      |
#### ####     *** Proxy reads HTTP response from backend sends HTTP response to client ***
#### ####        |<----------------------|                      |
#### ####        |13) HTTP/1.1 200 OK    |                      |
#### ####        |                       |                      |

#### #### #### #### #### #### After request has been received, the client is
                              free to re-use the connection to the proxy to
                              fetch another URL (even from a different origin
                              server).

#### #### #### #### HTTPS URL via HTTP

#### Here, a client uses an HTTPS proxy to fetch an HTTPS url. Since the client
     does not want the proxy to see the contents of the traffic it will send to
     the server, the client will establish a tunnel through the proxy, to the
     origin server, and will then use that tunnel to send the request. In this
     example, the origin server does not negotiate SPDY.

####      Client              HTTPS Proxy            Origin Server
####      ------              -----------            -------------
####     *** If an available tunnel to the origin exists, jump to step 19 ***
####     *** If an available connection to the proxy exists, jump to step 9 ***
####     *** Client opens TCP connection to Proxy ***
####        |---------------------->|                      |
####        | 1) SYN                |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 2) SYN_ACK            |                      |
####        |                       |                      |
####        |---------------------->|                      | 
####        | 3) ACK                |                      |
####        |                       |                      |
####     *** Client performs SSL handshake with Proxy ***
####        |---------------------->|                      |
####        | 4) Client Hello       |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 5) Server Hello       |                      |
####        |                       |                      |
####        |---------------------->|                      |
####        | 6) Client Key Exchange|                      |
####        |                       |                      |
####        |---------------------->|                      |
####        | 7) Client Finished    |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 8) Server Finished    |                      |
####        |                       |                      |
####     *** Client sends CONNECT request to Proxy via SPDY ***   
####         ---------------------->|                      |
####        |9) CONNECT origin:443  |                      |
####        |                       |                      |
####        |                    *** Proxy connects (or reuses connection) to origin ***
####        |                       |--------------------->|
####        |                       |10) SYN               |
####        |                       |                      |
####        |                       |<---------------------|
####        |                       |11) SYN_ACK           |
####        |                       |                      |
####        |                       |--------------------->|
####        |                       |12) ACK               |
####        |                       |                      |
####     *** Proxy sends HTTP CONNECT response to client ***
####        |<----------------------|                      |
####        |13) HTTP/1.1 200 Connection Established       |
####        |                       |                      |
####     *** Client performs SSL handshake with Origin (through proxy tunnel) ***
####        |=============================================>|
####        |14) Client Hello                              |
####        |                                              |
####        |<=============================================|
####        |15) Server Hello                              |
####        |                                              |
####        |=============================================>|
####        |16) Client Key Exchange                       |
####        |                                              |
####        |=============================================>|
####        |17) Client Finished                           |
####        |                                              |
####        |<=============================================|
####        |18) Server Finished                           |
####        |                                              |
####     *** Client issues request via HTTP to origin ***
####        |=============================================>|
####        |19) GET / HTTP/1.1                            |
####        |    Host: origin                              |
####        |                                              |
####        |*** Origin sends HTTP response to Client ***
####        |<=============================================|
####        |20) HTTP/1.1 200 OK                           |
####        |                                              |

#### #### #### After response has been received, the client is free to re-use
               the SSL tunnel to request additional HTTPS URL from the same
               origin server.

#### #### #### #### HTTPS URL via SPDY

#### Here, a client uses an HTTPS proxy to fetch an HTTPS url. Since the client
     does not want the proxy to see the contents of the traffic it will send to
     the server, the client will establish a tunnel through the proxy, to the
     origin server, and will then use that tunnel to send the request. In this
     example, the origin server negotiates SPDY.

####      Client              HTTPS Proxy            Origin Server
####      ------              -----------            -------------
####     *** If an available tunnel to the origin exists, jump to step 19 ***
####     *** If an available connection to the proxy exists, jump to step 9 ***
####     *** Client opens TCP connection to Proxy ***
####        |---------------------->|                      |
####        | 1) SYN                |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 2) SYN_ACK            |                      |
####        |                       |                      |
####        |---------------------->|                      | 
####        | 3) ACK                |                      |
####        |                       |                      |
####     *** Client performs SSL handshake with Proxy ***
####        |---------------------->|                      |
####        | 4) Client Hello       |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 5) Server Hello       |                      |
####        |                       |                      |
####        |---------------------->|                      |
####        | 6) Client Key Exchange|                      |
####        |                       |                      |
####        |---------------------->|                      |
####        | 7) Client Finished    |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 8) Server Finished    |                      |
####        |                       |                      |
####     *** Client sends CONNECT request to Proxy via SPDY ***   
####         ---------------------->|                      |
####        |9) CONNECT origin:443  |                      |
####        |                       |                      |
####        |                    *** Proxy connects (or reuses connection) to origin ***
####        |                       |--------------------->|
####        |                       |10) SYN               |
####        |                       |                      |
####        |                       |<---------------------|
####        |                       |11) SYN_ACK           |
####        |                       |                      |
####        |                       |--------------------->|
####        |                       |12) ACK               |
####        |                       |                      |
####     *** Proxy sends HTTP CONNECT response to client ***
####        |<----------------------|                      |
####        |13) HTTP/1.1 200 Connection Established       |
####        |                       |                      |
####     *** Client performs SSL handshake with Origin (through proxy tunnel) ***
####        |=============================================>|
####        |14) Client Hello                              |
####        |                                              |
####        |<=============================================|
####        |15) Server Hello                              |
####        |                                              |
####        |=============================================>|
####        |16) Client Key Exchange                       |
####        |                                              |
####        |=============================================>|
####        |17) Client Finished                           |
####        |                                              |
####        |<=============================================|
####        |18) Server Finished                           |
####        |                                              |
####     *** Client issues request via SPDY to origin ***
####        |=============================================>|
####        |19)SYN_STREAM                                 |
####        |   stream_id = 1                              |
####        |   method = GET                               |
####        |   url = /                                    |
####        |   host = origin                              |
####        |                                              |
####        |*** Origin sends SPDY response to Client ***
####        |<=============================================|
####        |20)SYN_REPLY                                  |
####        |   stream_id = 1                              |
####        |   status = 200 OK                            |
####        |                                              |

#### #### #### After connect response has been received, the client is free to
               re-use the SPDY session over the SSL tunnel to request additional
               HTTPS URL from the same origin server via a new SPDY stream.

### SPDY Proxy

#### HTTP URL

#### Here, a client uses a SPDY proxy to fetch an HTTP url. From the perspective
     of the client, the process is exactly the same as it would be if sending
     the request directly, via SPDY, to the origin server, but in this case the
     request goes to the proxy instead.

####      Client              HTTPS Proxy            Origin Server
####      ------              -----------            -------------
####     *** If an available connection to the proxy exists, jump to step 9 ***
####     *** Client opens TCP connection to Proxy ***
####        |---------------------->|                      |
####        | 1) SYN                |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 2) SYN_ACK            |                      |
####        |                       |                      |
####        |---------------------->|                      | 
####        | 3) ACK                |                      |
####        |                       |                      |
####     *** Client performs SSL handshake with Proxy ***
####        |---------------------->|                      |
####        | 4) Client Hello       |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 5) Server Hello       |                      |
####        |                       |                      |
####        |---------------------->|                      |
####        | 6) Client Key Exchange|                      |
####        |                       |                      |
####        |---------------------->|                      |
####        | 7) Client Finished    |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 8) Server Finished    |                      |
####        |                       |                      |
####     *** Client requests urls over SPDY ***            |
####         ---------------------->|                      |
####        |9) SYN_STREAM          |                      |
####        |   stream_id = 1       |                      |
####        |   method = GET        |                      |
####        |   url = http://origin/|                      |
####        |                       |                      |
####        |                    *** Proxy connects (or reuses connection) to origin ***
####        |                       |--------------------->|
####        |                       |10) SYN               |
####        |                       |                      |
####        |                       |<---------------------|
####        |                       |11) SYN_ACK           |
####        |                       |                      |
####        |                       |--------------------->|
####        |                       |12) ACK               |
####        |                       |                      |
####        |                    *** Proxy issues request via HTTP to backend ***
####        |                       |--------------------->|
####        |                       |13) GET http://origin/ HTTP/1.1
####        |                       |                      |
####        |                       |                      |
####        |                    *** Origin sends HTTP response to Proxy ***
####        |                       |<---------------------|
####        |                       |14) HTTP/1.1 200      |
####        |                       |                      |
####     *** Proxy reads HTTP response from backend sends SPDY response to client ***
####        |<----------------------|                      |
####        | 15) SYN_REPLY         |                      |
####        |     stream_id = 1     |                      |
####        |     status = 200 OK   |                      |
####        |                       |                      |

#### #### #### After connection has been established, the client is free to
               re-use the SPDY session to the proxy to fetch another URL (even
               from a different origin server) via a new SPDY stream.

#### HTTPS URL via HTTP

### Here, a client uses a SPDY proxy to fetch an HTTPS url. Since the client does not want the proxy to see the contents of the traffic it will send to the server, the client will establish a tunnel through the proxy, to the origin server, and will then use that tunnel to send the request. In this example, the origin server does not negotiates SPDY.

####      Client              HTTPS Proxy            Origin Server
####      ------              -----------            -------------
####     *** If an available tunnel to the origin exists, jump to step 19 ***
####     *** If an available connection to the proxy exists, jump to step 9 ***
####     *** Client opens TCP connection to Proxy ***
####        |---------------------->|                      |
####        | 1) SYN                |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 2) SYN_ACK            |                      |
####        |                       |                      |
####        |---------------------->|                      | 
####        | 3) ACK                |                      |
####        |                       |                      |
####     *** Client performs SSL handshake with Proxy ***
####        |---------------------->|                      |
####        | 4) Client Hello       |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 5) Server Hello       |                      |
####        |                       |                      |
####        |---------------------->|                      |
####        | 6) Client Key Exchange|                      |
####        |                       |                      |
####        |---------------------->|                      |
####        | 7) Client Finished    |                      |
####        |                       |                      |
####        |<----------------------|                      |
####        | 8) Server Finished    |                      |
####        |                       |                      |
####     *** Client sends CONNECT request to Proxy via SPDY ***   
####         ---------------------->|                      |
####        | 9)SYN_STREAM          |                      |
####        |   stream_id = 1       |                      |
####        |   method = CONNECT    |                      |
####        |   url = origin:443    |                      |
####        |                       |                      |
####        |                    *** Proxy connects (or reuses connection) to origin ***
####        |                       |--------------------->|
####        |                       |10) SYN               |
####        |                       |                      |
####        |                       |<---------------------|
####        |                       |11) SYN_ACK           |
####        |                       |                      |
####        |                       |--------------------->|
####        |                       |12) ACK               |
####        |                       |                      |
####     *** Proxy sends SPDY CONNECT response to client ***
####        |<----------------------|                      |
####        |13)SYN_REPLY           |                      |
####        |   stream_id = 1       |                      |
####        |   status = 200 Connection Established        |
####        |                       |                      |
####     *** Client performs SSL handshake with Origin (through proxy tunnel) ***
####        |=============================================>|
####        |14) Client Hello                              |
####        |                                              |
####        |<=============================================|
####        |15) Server Hello                              |
####        |                                              |
####        |=============================================>|
####        |16) Client Key Exchange                       |
####        |                                              |
####        |=============================================>|
####        |17) Client Finished                           |
####        |                                              |
####        |<=============================================|
####        |18) Server Finished                           |
####        |                                              |
####     *** Client issues request via HTTP to origin ***
####        |=============================================>|
####        |19) GET / HTTP/1.1                            |
####        |    Host: origin                              |
####        |                                              |
####        |*** Origin sends HTTP response to Client ***
####        |<=============================================|
####        |20) HTTP/1.1 200 OK                           |
####        |                                              |

#### #### #### After response has been received, the client is free to re-use
               the SSL tunnel to request additional HTTPS URL from the same
               origin server.

#### #### #### #### HTTPS URL via SPDY

#### #### #### ### Here, a client uses a SPDY proxy to fetch an HTTPS url. Since the client does not want the proxy to see the contents of the traffic it will send to the server, the client will establish a tunnel through the proxy, to the origin server, and will then use that tunnel to send the request. In this example, the origin server negotiates SPDY.

#### #### #### ####      Client              HTTPS Proxy            Origin Server
#### #### #### ####      ------              -----------            -------------
#### #### #### ####     *** If an available tunnel to the origin exists, jump to step 19 ***
#### #### #### ####     *** If an available connection to the proxy exists, jump to step 9 ***
#### #### #### ####     *** Client opens TCP connection to Proxy ***
#### #### #### ####        |---------------------->|                      |
#### #### #### ####        | 1) SYN                |                      |
#### #### #### ####        |                       |                      |
#### #### #### ####        |<----------------------|                      |
#### #### #### ####        | 2) SYN_ACK            |                      |
#### #### #### ####        |                       |                      |
#### #### #### ####        |---------------------->|                      | 
#### #### #### ####        | 3) ACK                |                      |
#### #### #### ####        |                       |                      |
#### #### #### ####     *** Client performs SSL handshake with Proxy ***
#### #### #### ####        |---------------------->|                      |
#### #### #### ####        | 4) Client Hello       |                      |
#### #### #### ####        |                       |                      |
#### #### #### ####        |<----------------------|                      |
#### #### #### ####        | 5) Server Hello       |                      |
#### #### #### ####        |                       |                      |
#### #### #### ####        |---------------------->|                      |
#### #### #### ####        | 6) Client Key Exchange|                      |
#### #### #### ####        |                       |                      |
#### #### #### ####        |---------------------->|                      |
#### #### #### ####        | 7) Client Finished    |                      |
#### #### #### ####        |                       |                      |
#### #### #### ####        |<----------------------|                      |
#### #### #### ####        | 8) Server Finished    |                      |
#### #### #### ####        |                       |                      |
#### #### #### ####     *** Client sends CONNECT request to Proxy via SPDY ***   
#### #### #### ####         ---------------------->|                      |
#### #### #### ####        | 9)SYN_STREAM          |                      |
#### #### #### ####        |   stream_id = 1       |                      |
#### #### #### ####        |   method = CONNECT    |                      |
#### #### #### ####        |   url = origin:443    |                      |
#### #### #### ####        |                       |                      |
#### #### #### ####        |                    *** Proxy connects (or reuses connection) to origin ***
#### #### #### ####        |                       |--------------------->|
#### #### #### ####        |                       |10) SYN               |
#### #### #### ####        |                       |                      |
#### #### #### ####        |                       |<---------------------|
#### #### #### ####        |                       |11) SYN_ACK           |
#### #### #### ####        |                       |                      |
#### #### #### ####        |                       |--------------------->|
#### #### #### ####        |                       |12) ACK               |
#### #### #### ####        |                       |                      |
#### #### #### ####     *** Proxy sends SPDY CONNECT response to client ***
#### #### #### ####        |<----------------------|                      |
#### #### #### ####        |13)SYN_REPLY           |                      |
#### #### #### ####        |   stream_id = 1       |                      |
#### #### #### ####        |   status = 200 Connection Established        |
#### #### #### ####        |                       |                      |
#### #### #### ####     *** Client performs SSL handshake with Origin (through proxy tunnel) ***
#### #### #### ####        |=============================================>|
#### #### #### ####        |14) Client Hello                              |
#### #### #### ####        |                                              |
#### #### #### ####        |<=============================================|
#### #### #### ####        |15) Server Hello                              |
#### #### #### ####        |                                              |
#### #### #### ####        |=============================================>|
#### #### #### ####        |16) Client Key Exchange                       |
#### #### #### ####        |                                              |
#### #### #### ####        |=============================================>|
#### #### #### ####        |17) Client Finished                           |
#### #### #### ####        |                                              |
#### #### #### ####        |<=============================================|
#### #### #### ####        |18) Server Finished                           |
#### #### #### ####        |                                              |
#### #### #### ####     *** Client issues request via SPDY to origin ***
#### #### #### ####        |=============================================>|
#### #### #### ####        |19)SYN_STREAM                                 |
#### #### #### ####        |   stream_id = 1                              |
#### #### #### ####        |   method = GET                               |
#### #### #### ####        |   url = /                                    |
#### #### #### ####        |   host = origin                              |
#### #### #### ####        |                                              |
#### #### #### ####        |*** Origin sends SPDY response to Client ***
#### #### #### ####        |<=============================================|
#### #### #### ####        |20)SYN_REPLY                                  |
#### #### #### ####        |   stream_id = 1                              |
#### #### #### ####        |   status = 200 Connection Established        |
#### #### #### ####        |                                              |

#### #### #### #### #### #### After connect response has been received, the
                              client is free to re-use the SPDY session over the
                              SSL tunnel to request additional HTTPS URL from
                              the same origin server via a new SPDY stream.
