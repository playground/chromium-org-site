# SPDY Protocol

*   [Draft 4](http://mbelshe.github.com/SPDY-Specification/)
    *   This is a placeholder. Not spec'd or implemented yet. Expected changes:
        *   Server push of DNS records & certificates for better connection
            sharing
        *   Better compression
        *   Better prioritization
        *   Better flow control mechanisms
*   [Draft 3.1](spdy-protocol-draft3-1.md)
    Adds session-layer flow control.
*   [Draft 3](spdy-protocol-draft3.md)
    Adds flow control.
*   [Draft 2](spdy-protocol-draft2.md)
    Clarifies Server Push & improves stream cancellation semantics.
*   [Draft 1](spdy-protocol-draft1/index.md)
    Originally published Nov 11, 2009.
