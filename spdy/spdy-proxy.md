# SPDY Proxy

One potential use for SPDY is to provide fast, secure access to a proxy. For
instance, if we had a forward proxy which took SPDY requests from the client and
could issue HTTP requests to the web, then we could leverage the benefits of
SPDY over the Client->Proxy link without needing to upgrade the entire web. Some
environments where this could be particularly useful include:

*   Mobile networks, where RTTs are very high
*   Public networks, where local WiFi or other protocols may be insecure, but
    SPDY connections to a secure proxy could add a layer of protection.

**Basic Design**

To implement a SPDY proxy, several changes to Chromium are required:

*   SPDY protocol support
*   Chromium Secure-Proxy support
*   Tunnelling of SSL connections over a SPDY proxy
*   PAC support

**SPDY Protocol Support**

This work is largely done as part of the basic SPDY project. URLs in SPDY
requests are already fully qualified and therefore need no changes to work with
a proxy. Similar to with HTTP, some slight header changes need to be made when
communicating with the proxy to support proxy Authentication to a SPDY proxy.

**Chromium Secure-Proxy support**

Browsers today do not support secure proxies. Although proxies can tunnel SSL,
connectivity to the proxy itself is only over HTTP. To support SPDY, we need to
modify chromium to support a SSL-based proxy.

**Tunneling of SSL connections over a SPDY proxy**

The network stack today only supports a single layer of SSL to the origin
server. Once we introduce a secure proxy, we must modify the browser to be able
to speak SSL to the Proxy, and then be able to tunnel another SSL stack (for
end-to-end connectivity over that.

Tunnelling of SSL connections over SPDY will be similar to tunneling of SSL
connections over HTTP. We'll first issue a CONNECT request to the proxy, and
when that succeeds, upgrade a SPDY stream into a tunnelled SSL mode.

**PAC Support**

To support a SPDY proxy, we plan to modify Chrome's [Proxy-Autoconfiguration
format](http://en.wikipedia.org/wiki/Proxy_auto-config). Today,
FindProxyForURL() can return "PROXY proxy.foo.com" to indicate that a resource
should be fetched through a proxy. We plan to augment the format of this file so
that secure proxies (potentially HTTP over SSL as well as SPDY over SSL) can be
used. To do so, FindProxyForURL() can return a string such as "HTTPS
proxy.foo.com".
