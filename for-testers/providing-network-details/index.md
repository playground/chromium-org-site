# How to capture a NetLog dump

A *NetLog dump* is a log file of the browser's network-level events and state.
You may be asked to provide this log when experiencing page load or performance
problems.

To create a *NetLog dump* open a new tab and navigate to:

chrome://net-export/

In Chrome 58 (desktop), chrome://net-export/ looks like this:

![image](Screen-Shot-2017-04-26-at-11.18.49-AM.png)

**Step-by-step guide**

1.  Open a new tab and go to chrome://net-export/
2.  Click the **Start Logging** button.
3.  Reproduce the network problem **in a different tab** (the
    chrome://net-export/ window needs to stay open or logging will stop
4.  Click **Stop Logging** button.
5.  Provide the resulting log file to the bug investigator
    *   Either attach the file to your existing or [new bug
        report](https://crbug.com/new), or send email to the bug investigator.
    *   **Include any relevant URLs or details to focus on.**

**PRIVACY**: When attaching log files to bug reports note that bug reports are
**publicly visible** by default. If you would prefer it be only visible to
Google employees, mention this on the bug before attaching it. Someone will
restrict visibility on the bug for you.

**Advanced: ****Byte-level captures**

By default NetLog dumps do not include the raw bytes (encrypted or otherwise)
that were transmitted over the network.

To include this data in the log file, select the **Include raw bytes** option at
the bottom of the chrome://net-export/ page.

**PRIVACY: **Captures with this level of detail may include personal information
and should generally be emailed rather than posted on public forums or public
bugs.

![An image showing how to enable byte-level capture on
mobile.](inclue-bytes-mobile.png)

**Advanced: ****Logging on startup**

If the problem that you want to log happens very early and you cannot start
chrome://net-export in time, you can add a command line argument to Chrome that
will start logging to a file from startup:

--log-net-log=C:\\some_path\\some_file_name.json

For info about adding command line options see
[command-line-flags](../command-line-flags.md).
