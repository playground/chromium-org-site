# Spell Check: Basic Testing

## Testcases

Test case Steps Expected Result
Red underline for spelling errors

1.  Launch Chromium.
2.  Navigate to <http://mail.google.com>.
3.  Sign in to your account.
4.  Click **Compose Mail**.
5.  Enter **gogl** plus a space in the message field.

**gogle** will be underlined in red.
Login form: username will not be red underlined

1.  Launch Chromium.
2.  Visit <http://mail.google.com> (if signed in, sign out to see the login
    page)
3.  Enter any text in the **Username** field.

Username will not be underlined in red.

Mis-spelled text that is copied and pasted will be underlined in red

1.  Launch Chromium.
2.  Navigate to <http://mail.google.com>.
3.  Sign in to your account.
4.  Click **Compose Mail**
5.  Copy and paste any mis-spelled text into the message field.

Mis-spelled text will be underlined in red.
