# Constrained Windows: Basic Testing

## Testcases

Test case Steps Expected Result Simple test: constrained window

1.  Log in to <http://mail.google.com>.
2.  Google Talk in Gmail will load on left side. Click on any contact to open a
    chat window
3.  Pop-out the chat window.

Chat window will open as a constrained window on the left side of the page ;
title bar : default icon and title "name - chat - Google Chrome" Pop-up
displayed in constrained window

1.  Visit any website which opens a pop-up

If your settings are checked for : Options> Under the hood> Web Content : Notify
me when a pop-up is blocked then blocked pop-up will be notified at bottom right
side of the page with title bar : "Blocked pop-up"
On clicking blocked pop-up, title bar will change to "default icon and title -
Google Chrome". If no title then "default icon and Google Chrome"
Play video in constrained window

1.  Visit any website which plays video in constrained window

A constrained window will launch for the video link ; title bar : "default icon
and title - google chrome" Constrained window - launch with window.open() and
close with window.close()

1.  Visit <http://html.tests.googlepages.com/example1.html>
2.  Click **Open window**.
3.  Click **Close window**.

After Step 2: A constrained window with height=200px and width=300px will be
launched.
After Step 3: The constrained window will be closed.
