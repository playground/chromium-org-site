# Uncaught SecurityError: Failed to read the 'localStorage' property from 'Window': Access is denied for this document.

This exception is thrown when the "Block third-party cookies and site data"
checkbox is set in Content Settings.

To find the setting, open Chrome settings, type "third" in the search box, click
the Content Settings button, and view the fourth item under Cookies.

![image](blockthirdpartycookies.png)

If this setting is checked, third-party scripts cookies are disallowed and
access to localStorage may result in thrown SecurityError exceptions.
