# Chromium Bug Labels

Label    Allowed values          Description     Type-*value*

*   `Bug`
*   `Bug-Regression`
*   `Bug-Security`
*   `Compat`
*   `Feature`
*   `Launch`

The issue type. An issue can only have one type.

Pri-*value*

*   `0 to 3`

The priority. An issue can only have one priority value. `0` is most urgent; `3`
is least urgent.        OS-*value*

*   `All`
*   `Chrome`
*   `Linux`
*   `Mac`
*   `Windows`

The operating system(s) on which the bug occurs.         M-*value*

*   `27, 28, 29`

A release milestone before which we want to resolve the issue. An issue can only
be assigned to one milestone† . `Mstone-X` is 'no milestone' (doesn't apply or
not blocking any milestone).
† Except for security bugs. For those, the additional milestones denote branches
that the bug should be merged to once fixed. See the [severity
guidelines](../../developers/severity-guidelines.md).    Cr-*value*

*   Blink
*   Internals
*   Platform
*   UI-Shell
*   UI-Browser == Browser
*   Cr-OS-Hardware
*   Cr-OS-Kernel
*   Cr-OS-Systems

The product category to which an issue belongs. A bug can belong to multiple
categories.
Cr-Blink HTML, CSS, Javascript, and HTML5 features Cr-Internals Ugly guts,
networking, IPC, storage backend, installer, etc Cr-Platform Developer Platform
and Tools (Ext, AppsV2, NaCl, DevTools) Cr-UI-Shell Chrome OS Shell & Window
Manager Cr-UI-Browser Browser related features (e.g. bookmarks, omnibox, etc...)
Cr-OS-Hardware Chrome OS hardware related issues Cr-OS-Kernel Chrome OS kernel
level issues Cr-OS-Systems Chrome OS system level issues
Restrict-View-EditIssue          -       Used for security bugs to make a bug
visible only to project members and the reporter.

Some other labels include:

*   Needs-Reduction: this bug requires a reduced test case. See
    <http://dev.chromium.org/for-testers/backend-testing/website-compatibility/reduced-test-cases>.
*   Cr-UI-Internationalization: Internationalization issues (font selection and
    fallback, right-to-left display issues, IME, etc.)
*   Cr-UI-Localization: Translation issues
*   Type-Bug-Regression: Features that worked in prior releases and are now
    broken. These should be Pri-1.
