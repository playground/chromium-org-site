# FAFT

## **Overview**

FAFT (Fully Automated Firmware Tests) is a collection of tests and related
infrastructure that exercise and verify capabilities of Chrome OS. The features
tested by FAFT are implemented through low-level software (firmware/BIOS) and
hardware. FAFT evolved from SAFT (Semi-Automated Firmware Tests) and you can
locate [tests in the FAFT suite in the Autotest
tree](https://chromium.googlesource.com/chromiumos/third_party/autotest/+/master/server/site_tests/)
as directories with the prefix firmware_. The founding principles of FAFT are:

*   Fully automated, no human intervention required
*   Real test of physical hardware, like USB plug-in, Ctrl-D key press
*   High test coverage of complicated verified boot flows
*   Easy to integrate with existing test infrastructure, i.e. deploy to test
    lab, run remotely, continuous testing, etc.

To access some of these low-level capabilities, the tests require a [servo
board](../../chromium-os/servo/index.md). You connect the servo board directly
to the test device to enable access to low-level system hardware interfaces, as
well as staging areas for backup software (on a USB drive). During testing the
tests may corrupt various states in the EC, firmware, and kernel to verify
recovery processes. In these cases you can almost always use FAFT to restore the
system to its original state.

The FAFT suite of tests can be invoked locally or remotely. This document
describes how to set up the local configuration only. In remote configurations
for automated test labs, a beagleboard device is required to enable
communication between the device and servo board, and the test server.

The Chrome OS firmware/BIOS control (among other things) initial setup of the
system hardware during the boot process. They are necessarily complicated,
providing reliability against various corruption scenarios and security to
ensure trusted software is controlling the system. Currently, the purpose of
FAFT is to exercise EC firmware and BIOS firmware (such as U-Boot).

## **Setting up the Hardware**

The most common hardware configuration for running FAFT includes:

*   a test controller (your host workstation with a working chroot environment)
*   the test device (a device that can boot Chrome OS)
*   a servo board
*   related connectors

![image](FAFT-Hardware-Setup-Layout.png)

The following photo shows the details of how to connect the servo board to the
test controller, test device, and network:

![image](Servo2_with_labels.jpg)

## **Preliminary Tasks**

After the hardware components are correctly configured, prepare and install a
test Chromium OS image:

1.  Build the binary (chromiumos_test_image.bin) with build_image test, or fetch
    the file from a buildbot.
2.  Load the test image onto a USB drive.
3.  Insert the USB drive into the servo board, as shown in the photo.
4.  Install the test image onto the internal disk by booting from the USB drive
    and running chromeos-install.

To run FAFT you use the
[test_that](http://www.chromium.org/chromium-os/testing/test_that-basic-usage)
tool, which does not automatically start a servod process for communicating with
the servo board. Before running any tests:

1.  Run $ sudo servod --board=$BOARD where $BOARD is the code name of the board
    you are testing. For example: $ sudo servod --board=lumpy
2.  Run the firmware_FAFTSetup test to verify basic functionality and ensure
    that your setup is correct. If test_that is in /usr/bin, the syntax is `$
    /usr/bin/test_that --board=$BOARD $REMOTE_IP ``firmware_FAFTSetup`

## Running Test Suites

To run FAFT on a Chromebook:

1.  Run the entire faft_bios suite:

$ /usr/bin/test_that --board=$BOARD $REMOTE_IP suite:faft_bios

1.  Run the entire faft_ec suite:

`$ /usr/bin/test_that --board=$BOARD $REMOTE_IP suite:faft_ec`

To run FAFT on a Chromebox or Chromebase, a [USB-KM232
cable](http://www.google.com/url?q=http%3A%2F%2Fwww.hagstromelectronics.com%2Fproducts%2Fusbkm232.html&sa=D&sntz=1&usg=AFrqEzdlz5LHuzhyH0dCklA57f60223exw)
and a
[RS232-USB](https://www.google.com/search?q=TRENDnet+USB+2.0+to+RS-232+DB9+Serial+Converter%2C+TU-S9&oq=TRENDnet+USB+2.0+to+RS-232+DB9+Serial+Converter%2C+TU-S9&aqs=chrome..69i57j0.367j0j4&sourceid=chrome&es_sm=94&ie=UTF-8#q=USB+2.0+to+RS-232+DB9+Serial+Converter%2C+TU-S9)
cable are required.

1.  Connect the USB-KM232 to the RS232-to-USB cable, then connect the RS232 side
    to the test controller.
2.  Connect the USB side to the Chrome device.
3.  Check the device on the host by running tail -f /var/log/messages. It should
    be named /dev/ttyUSB0.
4.  Assign this value to the USBKM232_UART_DEVICE environment variable and make
    the drive partition writeable. For example:

`$ export USBKM232_UART_DEVICE=/dev/ttyUSB0`
`$ sudo chmod a+rw $USBKM232_UART_DEVICE`

1.  When starting servod, specify the --usbkm232 argument.

`$ ``sudo servod --board=$BOARD --usbkm232=$USBKM232_UART_DEVICE`

1.  The test_that command line stays the same:

`$ /usr/bin/test_that --board=$BOARD $REMOTE_IP`` f:.*DevMode/control$`

## FAQ

Q: All of my FAFT tests are failing. What should I do?

A: Run `firmware_FAFTSetup` as a single test. Once it fails, check the log and
determine which step failed and why.

Q: A few of my FAFT tests failed, but most tests are passing. What should I do?

A: Re-run the failed tests by themselves and they may pass. Sometimes tests fail
due to flaky infrastructure or other non-firmware bugs.

Q: I still need help. Who can help me?

A: Try joining the [FAFT-users chromium.org mailing
list](https://groups.google.com/a/chromium.org/forum/#!forum/faft-users) and
asking for help. Be sure to include logs and test details in your request for
help.

Q: I got an error while running FAFT: AutoservRunError: command execution error:
sudo -n which flash_ec . What's wrong?

A: Run sudo emerge chromeos-ec inside your chroot.
