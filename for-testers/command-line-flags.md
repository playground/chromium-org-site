# How to specify command line flags

Let's say that you want to add two command line flags to chrome: --foo and
--bar=2.

### Windows

1.  Exit any running-instance of chrome.
2.  Find the shortcut you normally use to launch chrome.
3.  Create a copy of it
4.  Right click on the new shortcut, and select Properties
5.  At the very end of the Target: text box, add a space and then the desired
    command line flags.
    It should end in something like ...\\chrome.exe" --foo --bar=2
6.  Double click the new shortcut to launch chrome with the new command line
    flags.

### Mac OS X

1.  Quit any running instance of chrome.
2.  Launch /Applications/Utilities/Terminal.app
3.  At the command prompt enter:
    /Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome --foo
    --bar=2

### Linux

1.  Exit any running instance of chrome.
2.  Execute in a console:
    google-chrome --foo --bar=2
    (If you are using a different named chrome/chromium build, change the
    command accordingly)

### Chrome OS

1.  Put the device into dev mode so you can get a root shell
2.  Modify /etc/chrome_dev.conf (read the comments in the file for more details)
3.  Restart the UI via:
    `sudo restart ui`

### Android

1.  See more at
    <https://sites.google.com/a/chromium.org/dev/developers/how-tos/run-chromium-with-flags>
