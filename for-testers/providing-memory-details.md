# Reporting memory bloat/leak issues

## How to file a bug for memory bloat/leak issues:

*   Use [this
    template](https://code.google.com/p/chromium/issues/entry?template=Memory%20usage)
    when filing a bug for memory bloat/leak issues.

*   Provide as much specifics as possible (see the next section)

## How to provide memory details for investigating bugs:

*   Close all running instances of your browser.

*   Start your browser, create a new tab and go to chrome://memory-internals

*   Click the update button.

*   Take a first snapshot of the initial memory usage (see instructions below)

*   Reproduce the memory bloat by using other tabs.

*   Go back to the chrome://memory-internals tab and take a second snapshot
    (don’t forget to click on the update button).

*   Edit the logs to remove any information you do not want to share (see the
    instructions below for details).

*   Either attach the snapshots to a bug in crbug.com, or send email to the bug
    investigator. Include any relevant info (e.g. which tab/process/extension
    you believe is at fault) or additional details to focus on.

*   Also helpful: double checking that you can reproduce the memory bloat in an
    incognito window or a new user profile (in both case, close any other
    running instances and unnecessary browser windows).

## How to take a snapshot of memory usage:

1.  Click on the update button (see screenshot below).

2.  The information shown by chrome://memory-internals should be updated and the
    JSON field should contain a machine readable version of this information.

3.  Review carefully the information displayed below the JSON section. In
    particular, please note that beside memory usage, information such as
    **installed ****extensions, **per tab navigation history are reported **for
    all running Chrome/Chromium browser instances **(including other active user
    profiles as well as incognito windows)**. ** For this reason, we recommend
    to close any unnecessary browser instances/windows beforehand.

4.  Click in the JSON text field, select all and copy (see screenshot below).

5.  In your favorite text editor, paste the JSON data.

6.  From the JSON data, edit out anything you don’t want to share. If you do
    this, please do your best to keep the structure of the data intact.

7.  Save the snapshot to a file.
