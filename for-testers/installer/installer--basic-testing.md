# Installer: Basic Testing

## Test cases

### Install

Test case Steps Expected Result Fresh InstallInstall latest build of Google
Chrome.

*   On completion of installation, First Run UI dialog will be launched with no
    other alert prompts
*   First Run UI dialog: Shortcuts will be created according to the user's
    choice. Bookmarks, passwords, and other settings will be imported/not
    imported from IE/FF as per user's choice.
*   A new folder will be created:
    **XP**: C:\\Documents and Settings\\username\\Local Settings\\Application
    Data\\Google\\Google Chrome\\Application
    **Vista**: C:\\Users\\username\\AppData\\Local\\Google\\Google
    Chrome\\Application
*   Google Chrome can be launched from shortcuts and the Add/Remove panel (XP) /
    Programs and Features (Vista)

Recovery

If Google Chrome files are accidentally deleted and Google Chrome cannot be
launched, run the installer again.

*   On completion of installation, Google Chrome can be launched from shortcuts,
    Add/Remove panel (XP) / Programs and Features (Vista)

### Overinstall

Test case Steps Expected Result Overinstall Overinstall latest build of Google
Chrome

*   On launching Google Chrome, **about:version** should show the latest build
    number.
*   Google Chrome can be launched from shortcuts, Add/Remove panel (XP) /
    Programs and Features (Vista)

### First Run UI

Test case Steps Expected Result First Run UI dialog launch
Do a fresh install of Google Chrome. (Note: First Run UI will not be launched if
it is not a fresh install)

*   On completion of installation, First Run UI dialog will be launched giving
    the user choice to customize the settings.
*   A file called **First Run** will be created under:
    **XP**: C:\\Documents and Settings\\username\\Local Settings\\Application
    Data\\Google\\Google Chrome\\Application
    **Vista**: C:\\Users\\username\\AppData\\Local\\Google\\Google
    Chrome\\Application

First Run UI file not found Delete First Run file.

*   On Google Chrome launch, First Run UI dialog will be launched

### Uninstall

Test case Steps Expected Result Uninstall Google Chrome Click **Start > All
Programs > Google Chrome** and select **Uninstall**
or
Windows XP: From the **Add/Remove Programs** dialog, uninstall Google Chrome.
Windows Vista: From the **Programs and Features** window, uninstall Google
Chrome.

Verify the following on your local machine:

*   Google Chrome is uninstalled with no issues.
*   The Google Chrome folder is removed from:
    **XP**: C:\\Documents and Settings\\user\\Local Settings\\Application
    Data\\Google\\Google Chrome\\Application\\
    **Vista**: C:\\Users\\username\\AppData\\Local\\Google\\Google
    Chrome\\Application
*   Google Chrome will not appear in the **Add/Remove** panel on XP, or the
    **Programs and Features** window in Vista.
*   Shortcuts are deleted.

Uninstall Google Chrome while Google Chrome is running

1.  Launch Google Chrome.
2.  Try to uninstall Google Chrome.

The following message will appear:

**Please close all Google Chrome windows and try again**

Google Chrome will not be uninstalled.

On closing Google Chrome and uninstalling, Google Chrome will uninstall with no
issues.
