# Frontend testing

Note: these pages describe the testing plan for Google Chrome. Some of it only
applies to Google Chrome, but much of it also applies to Chromium, which is why
it's included here.

*   [Spell Check](frontend-testing/spell-check/index.md)
*   [Constrained Windows](frontend-testing/constrained-windows/index.md)
*   [Windows-specific interoperability
    issues](frontend-testing/windows-specific-interoperability-issues/index.md)
