# For Testers

#### Reporting Bugs

*   [Bug reporting guidelines and how to report
    bugs](bug-reporting-guidelines/index.md)
*   [Glossary](https://www.chromium.org/glossary)
*   [Enable logging](enable-logging/index.md)
*   [Recording
    traces](../developers/how-tos/trace-event-profiling-tool/recording-tracing-runs/index.md)

#### Contributing

*   [Find duplicate bugs](http://code.google.com/p/chromium/issues/list)
*   [Confirm unconfirmed bugs
    ](http://code.google.com/p/chromium/issues/list?q=status:Unconfirmed)
*   [Confirm unconfirmed Mac
    bugs](http://code.google.com/p/chromium/issues/list?can=2&q=+os:mac+status:unconfirmed&colspec=ID+Stars+Pri+Area+Type+Status+Summary+Modified+Owner+Mstone+OS&cells=tiles)
*   [Confirm unconfirmed Linux
    bugs](http://code.google.com/p/chromium/issues/list?can=2&q=os:Linux+status:unconfirmed&colspec=ID+Stars+Pri+Area+Type+Status+Summary+Modified+Owner+Mstone+OS&x=mstone&y=area&cells=tiles)
*   [Help us by creating reduced test cases](../system/errors/NodeNotFound)
*   [Verify Mac fixed
    bugs](http://code.google.com/p/chromium/issues/list?can=1&q=os:mac+status:fixed&colspec=ID+Stars+Pri+Area+Type+Status+Summary+Modified+Owner+Mstone+OS&x=mstone&y=area&cells=tiles)
*   [Verify Linux fixed
    bugs](http://code.google.com/p/chromium/issues/list?can=1&q=os:Linux+status:fixed&colspec=ID+Stars+Pri+Area+Type+Status+Summary+Modified+Owner+Mstone+OS&x=mstone&y=area&cells=tiles)
*   [Frontend testing](frontend-testing_index.md)
*   [Backend testing](../system/errors/NodeNotFound)
*   [Firmware testing](faft/index.md)
*   [Installer](installer/index.md)
