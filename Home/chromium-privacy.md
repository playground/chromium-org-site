# Chromium Privacy

Our promise: *Chromium provides users full transparency and control over the
information managed by the browser.*

Furthermore, we’re constantly trying to improve privacy aspects of your browsing
experience by implementing new innovative privacy features.

### How can I get involved?

If you discover a privacy issue, e.g. a behavior where your personal information
is not adequately protected, or used in an non-transparent or unexpected
fashion, or bugs related to existing privacy features such as the content
settings, please [file a
bug](http://code.google.com/p/chromium/issues/entry?template=Privacy%20issue) in
our public bug tracker.

For privacy issues other than reporting a bug, you can email us:
[privacy@chromium.org](mailto:privacy@chromium.org). For non-confidential
discussions, please post to the [technical discussion
forums](http://dev.chromium.org/developers/technical-discussion-groups).

### Privacy Reviews

**To ensure users maintain a consistent way to control their experience with the
web, new privacy relevant features are required to undergo a privacy review.
Through these reviews, developers receive feedback on how to improve user
control and over how Chromium handles user data.**

### Additional Information on Chromium, Google Chrome, and Privacy

**Features that communicate with Google made available through the compilation of code in Chromium are subject to the [Google Privacy Policy](http://www.google.com/policies/privacy/). **
**Additionally, the [Google Chrome Privacy
Notice](http://www.google.com/chrome/intl/en/privacy.html) describes the privacy
practices that are specific to the Google Chrome family of products. If you want
to learn more about Google Chrome's privacy features you can also check out the
[Google Chrome Privacy landing
page](https://www.google.com/chrome/browser/features.html#privacy) and the
[Google Chrome Privacy
Whitepaper](http://www.google.com/intl/en/landing/chrome/google-chrome-privacy-whitepaper.pdf)
which describes what information is transmitted to Google and why, as well as
how to disable certain features in Google Chrome which affect your privacy.**
