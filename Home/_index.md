# Chromium

Chromium is an open-source browser project that aims to build a safer, faster,
and more stable way for all Internet users to experience the web. This site
contains design documents, architecture overviews, testing information, and more
to help you learn to build and work with the Chromium source code.

#### Learn how to

*   [Browse](https://chromium.googlesource.com/chromium/src.git) or
    [search](https://cs.chromium.org/) the sources online
*   [Get the Code: Checkout, Build, &
    Run](../developers/how-tos/get-the-code/index.md)
*   [Contributing code](https://www.chromium.org/developers/contributing-code)
*   Debug on [Windows](../developers/how-tos/debugging-on-windows/index.md), [OS
    X](../developers/how-tos/debugging-on-os-x/index.md),
    [Linux](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_debugging.md)
    or [Android](../developers/how-tos/debugging-on-android.md).
*   [Report a bug](../for-testers/bug-reporting-guidelines/index.md)

#### Other important links

*   [Discussion Groups](../developers/discussion-groups/index.md) (such as
    [chromium-discuss](http://groups.google.com/a/chromium.org/group/chromium-discuss))
*   [Chromium Blog](http://blog.chromium.org)
*   [Chromium Issue Tracker](https://bugs.chromium.org/p/chromium/issues/list)
*   [Chromium Release Calendar](../developers/calendar/index.md)
*   [For Third Party Developers: Chrome's Blacklist and Implementation
    Details](third-party-developers.md)
*   [For Webmasters: Common pitfalls making a site work in multiple
    browsers.](chromecompatfaq.md)
*   [For Web Developers: Google Chrome Developer Tools](../devtools/index.md)

#### Sections

*   [Getting Involved](../getting-involved/index.md): learn how you can help the
    Chromium project
*   [For Developers](../developers/_index.md): design docs, how-tos, and other
    useful information for developers
*   [For Testers](../for-testers/index.md): bug reporting guidelines, test
    plans, and other quality-related documentation
*   [User Experience](../user-experience/index.md): the design philosophy behind
    many of Chromium's features
*   [Issue Tracking](../issue-tracking/index.md): process documentation related
    to issue tracking and management.
*   [Contact](../contact.md): report a bug or a security issue, or get in touch
    with individual members of the team
*   [Security](chromium-security/index.md): learn about Chromium security, and
    how to contact us or get involved
*   [Privacy](chromium-privacy.md): information about Chromium privacy, how to
    get more information, and how to contact us

#### Life Of A Chromium Developer

Interested but don't know where to start? Need a quick crash course on Chromium
development? Take a look through the Life Of A Chromium Developer slide deck:

#### Life of a Chromium Developer

Life of a Chromium Developer

Chromium committers: click
[here](https://www.google.com/a/chromium.org/ServiceLogin2?continue=http%3A%2F%2Fdocs.google.com%2Fa%2Fchromium.org%2Fpresent%2Fedit%3Fid%3D0AetfwCoL2lQAZGQ5bXJ0NDVfMGRtdGQ0OWM2&service=writely&passive=true)
to log in to edit the slide deck.
