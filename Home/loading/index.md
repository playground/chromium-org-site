# Loading

## Information about Loading efforts in Chromium

## **North Star**

Loading on the web is user centric, sustainable, fast and delightful.

*   **user centric:** the experience is described through key user moments or
    needs, and assessed against a perceptual model of performance.
*   **sustainable: **a sensible contract between Web developers, UA and users
    about how a web application loads, with usage of memory / power / data that
    is proportional to value.
*   **fast:**
    *   [First meaningful
        paint](https://docs.google.com/document/d/1oiy0_ych1v2ADhyG_QW7Ps4BNER2ShlJjx2zCbVzVyY/edit#heading=h.7mxprlepfkfu)
        in under 1 second.
    *   First meaningful paint for transitions (e.g. back navigation) in under
        100ms.
*   **delightful:**
    *   No user-visible reflows after reaching first meaningful paint.
    *   Decent response time and scrolling performance after reaching first
        meaningful paint.
    *   [First Time to
        interactive](https://docs.google.com/document/d/1oiy0_ych1v2ADhyG_QW7Ps4BNER2ShlJjx2zCbVzVyY/edit#heading=h.bii3b6sjxvi5)
        in under 5, and [Consistently
        Interactive](https://docs.google.com/document/d/1GGiI9-7KeY3TPqS3YT271upUVimo-XiL5mwWorDUD4c/edit)
        under 10 seconds.
    *   Response time < 100ms and Scrolling at 60fps after reaching TTI.

These are the goals we should strive for.

Some aspects might be extremely challenging but making progress toward these is
what should drive our work.

## How do I find out what's happening?

*   Communications: loading-dev@chromium.org is our public discussion group for
    all things related to Loading in Chrome.
*   crbug:
    [performance-loading](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=Performance%3DLoading+&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids),
    [performance-data](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=Performance%3DData+&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids),
    [blink](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink%3ELoader%2CBlink%3ENetwork%2CBlink%3EServiceWorker%2CBlink%3EWorker&sort=pri+-component&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids)

## I have a reproducible bad Loading user experience, what do I do?

## *   As a user:
##     *   File a [Speed
           bug](https://bugs.chromium.org/p/chromium/issues/entry?template=Speed%20Bug)
##     *   Include the "Loading" keyword in the subject if the issue fits within
           the scope of the North Star.
##     *   If you are not sure, don't include the "Loading" keyword, triage will
           make sure it shows up in the right bucket.
## *   As a chromium developer
##     *   Same steps but try to [record a
           trace](https://www.chromium.org/developers/how-tos/trace-event-profiling-tool/recording-tracing-runs)
           (select every trace categories on the left side).

## I'm a dev and interested in helping on Loading. How do I get started?

Reach out via loading-dev@ and tell us more about you:

*   share your particular interest and expertise
*   tell us how familiar you are with chromium development
*   point to CLs if not a lot of developers are familiar with your work

## Contacts

Your friendly PM: kenjibaheux

Blink TLs: kinuko, kouhei (blink>loader), falken (blink>worker,
blink>serviceworker), tyoshino (blink>network)

Chrome TLs: creis (navigation), mattcary (speculative features, optimized
loading), clamy (plznavigate)

"Here here!": reach out to kenjibaheux@ if you want your name to be added here.

## Other resources

*   TODO (backlog, etc)
