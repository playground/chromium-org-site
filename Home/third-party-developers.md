# Third Party Developers

## Depending on Google Chrome’s Implementation Details

A recurring problem for the Chrome ecosystem is third party programs that rely
on private implementation details of Chrome to operate. Because Chrome is always
updating, unsupported dependencies on how Chrome operates have a very high risk
of failure. Activities like reading and writing the preferences file
directly\[1\], locking the Chrome preferences directory\[2\], injecting DLLs
into Chrome\[3\] and relying on assumptions about Chrome’s window
management\[4\] can all be problematic when Chrome updates.

Issues have arisen with toolbars, PC cleaners, so-called ‘browser managers’ and
many other third party programs ranging from innocuous and well intentioned to
likely malicious - all of them causing problems for users by assuming that
Chrome’s implementation is static and dependencies can be safely introduced.
Such assumptions are one of the leading causes of Chrome crashes\[3\], profile
and IndexDB/LevelDB corruption\[5\] and broken Chrome UI\[6\]. Sometimes they
even prevent Chrome from successfully installing in the first place\[2\].

The best and most reliable way to help your users make changes to Chrome is by
showing them how to make those changes within Chrome’s UI. If your native app
relies on Chrome’s internal implementation details, you should expect it to
break on a regular basis.

Footnotes:

*   \[1\] [crbug.com/321370](http://crbug.com/321370),
    [crbug.com/327111](http://crbug.com/327111)

*   \[2\] [crbug.com/226844](http://crbug.com/226844)

*   \[3\] <http://goo.gl/b65QoW>

*   \[4\] [crbug.com/327110](http://crbug.com/327110)

*   \[5\] Dec ‘13 unidentified 3rd party software caused 5x spike in CURRENT
    file corruption

*   \[6\] [crbug.com/226844](http://crbug.com/226844)

## Programmatic changing of Chrome Settings on Chrome

Since one common source of instability and unwelcome loss of user control is
third party programs from Windows programs attempting to offer changes to common
search related settings such as homepage and default search engine we have built
an extension based [Settings
API](https://developer.chrome.com/extensions/settings_override) for Windows.
Using the API will ensure that the offer works correctly and also that users
have given consent and control over the changes to their critical browser
settings. Where available, the API is the only approved path for making changes
to those settings in Chrome.

## Google Chrome’s DLL Blacklisting Policy

Some DLLs are known to cause stability issues when loaded into Chrome. This
outlines how a DLL is blacklisted, as well as what steps can be taken to remove
a DLL from the blacklist.

### Getting Blacklisted

Once a DLL has been determined to cause a noticeable decrease in Google Chrome’s
stability, that DLL will be added to this [list of troublesome DLLs for
sandboxed
processes](https://code.google.com/p/chromium/codesearch#chromium/src/content/common/sandbox_win.cc&l=43)
or this [list of troublesome DLLs for the browser
process](https://code.google.com/p/chromium/codesearch#chromium/src/chrome_elf/blacklist/blacklist.cc&l=24),
which Chrome prevents from being injected into the corresponding process.

Once a DLL has been selected, an effort will be made to find the developer of
the DLL. If the developers are found, they can receive some debugging
information to assist them in fixing the problem. Additionally, an announcement
about our intention to blacklist the DLL will be sent to
[chrome-blacklisting-announce@chromium.org](mailto:chrome-blacklisting-announce@chromium.org).

If a DLL is causing serious stability issues, it will be added to the blacklist
right away.

If the DLL doesn’t need to be blacklisted right away, the developer will have at
least 3 business days to respond to the issue. If there is no response from the
company, the DLL will be added to the blacklist.

If the company responds, they will then have an additional week to fix the
troublesome DLL. If the DLL is still causing a noticeable decrease in Chrome
stability, it can be blacklisted.

If at any point during this process the DLL starts causing serious stability
issue, it may be immediately blacklisted.

***Note For Chromium Developers:***

If you are blacklisting or wanted to blacklist a dll, you are responsible for
following the instructions above (trying to find and contact the developer at
first, and then alerting
[chrome-blacklisting-announce@chromium.org](mailto:chrome-blacklisting-announce@chromium.org)
when the dll is actually blocked).

***Note For Google Developers:***

If you have a dll that you think should be blacklisted, the internal page
go/modulestabilityreport can help you determine the stability of the dll and if
it should be blacklisted.

### **Getting Removed from the DLL Blacklist**

If your DLL has been blacklisted and the issue has been fixed, you may request
to be removed from the blacklist by contacting
[chrome-blacklisting@chromium.org](mailto:chrome-blacklisting@chromium.org). If
the DLL still causes stability issues, it will be blocked again.

### New Troublesome DLLs to be blocked

Name

Date to be Blacklisted
