# User Metrics

**Template for UMA enumeration comment:**

// This enum is used to define the buckets for an enumerated UMA histogram.

// Hence,

// (a) existing enumerated constants should never be deleted or reordered, and

// (b) new constants should only be appended at the end of the enumeration.

**See also:**

[Rappor (Randomized Aggregatable Privacy Preserving Ordinal
Responses)](../developers/design-documents/rappor.md)
