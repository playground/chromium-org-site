# Quarterly Updates

We post a newsletter-y update quarterly on
[security-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!searchin/security-dev/quarter$20summary%7Csort:date).
It's an open list, so
[subscribe](https://groups.google.com/a/chromium.org/forum/#!forum/security-dev)
if you're interested in updates, discussion, or feisty rants related to Chromium
security.

## Q1 2017

Greetings and salutations,

It's time for another update from your friends in Chrome Security, who are hard
at work trying to make Chrome the[ most secure platform to browse the
Internet](https://www.chromium.org/Home/chromium-security). Here’s a recap from
last quarter:

Our [Bugs--](https://dev.chromium.org/Home/chromium-security/bugs) effort aims
to find (and exterminate) security bugs. In order to get bugs fixed faster, we
released a [new tool](https://github.com/google/clusterfuzz-tools) to improve
developer experience when trying to reproduce ClusterFuzz bugs. We have
overhauled a significant part of the [ClusterFuzz
UI](https://github.com/google/oss-fuzz/blob/master/docs/clusterfuzz.md) which
now feature a new fuzzer statistics page, crash statistics page and fuzzer
performance analyzer. We’ve also continued to improve our
[OSS-Fuzz](https://github.com/google/oss-fuzz)
[offering](https://opensource.googleblog.com/2016/12/announcing-oss-fuzz-continuous-fuzzing.html),
adding numerous
[features](https://github.com/google/oss-fuzz/issues?page=3&q=is%3Aissue+is%3Aclosed)
requested by developers and reaching [1000
bugs](https://bugs.chromium.org/p/oss-fuzz/issues/list?can=1&q=status%3AFixed%2CVerified+Type%3ABug%2CBug-Security+-component%3AInfra+)
milestone with 47
[projects](https://github.com/google/oss-fuzz/tree/master/projects) in just five
months since launch.

Members of the Chrome Security team attended the 10th annual [Pwn2Own
competition](http://blog.trendmicro.com/pwn2own-returns-for-2017-to-celebrate-10-years-of-exploits/)
at CanSecWest. While Chrome was again a target this year, no team was able to
demonstrate a fully working chain to Windows SYSTEM code execution in the time
allowed!

Bugs still happen, so our[
Guts](https://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense. Chrome 56 takes advantage of [Control Flow Guard
(CFG)](https://msdn.microsoft.com/en-us/library/windows/desktop/mt637065(v=vs.85).aspx)
on Windows for Microsoft system DLLs inside the Chrome.exe processes. CFG makes
exploiting corruption vulnerabilities more challenging by limiting valid call
targets, and is available from Win 8.1 Update 3.

[Site
Isolation](https://www.chromium.org/developers/design-documents/site-isolation)
makes the most of Chrome's multi-process architecture to help reduce the scope
of attacks. The big news in Q1 is that we launched --isolate-extensions to
Chrome Stable in Chrome 56! This first use of out-of-process iframes (OOPIFs)
ensures that web content is never put into an extension process. To maintain the
launch and prepare for additional uses of OOPIFs, we fixed numerous bugs,
cleaned up old code, reduced OOPIF memory usage, and added OOPIF support for
more features (e.g., IntersectionObserver, and hit testing and IME on Android).
Our next step is expanding the OOPIF-based <webview> trial from Canary to Dev
channel and adding more uses of dedicated processes.

Beyond the browser, our [web platform
efforts](https://dev.chromium.org/Home/chromium-security/owp) foster
cross-vendor cooperation on developer-facing security features. Over the
holidays, Google's security team gave us a holiday gift consisting entirely of
[interesting ways to bypass CSP's
nonces](http://sebastian-lekies.de/csp/bypasses.php). We've fixed some
[obvious](https://bugs.chromium.org/p/chromium/issues/detail?id=679291)
[bugs](https://bugs.chromium.org/p/chromium/issues/detail?id=680072) they
uncovered, and we'll continue working with other vendors to harden the spec and
our implementations. In other CSP news, we polished a mechanism to [enforce CSP
on child frames](https://w3c.github.io/webappsec-csp/embedded/), shipped a
[\`script-sample\`
property](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/XlcpobBfJOI/8WYpiyk0CQAJ)
in CSP reports, and [allowed hashes to match external
scripts](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/t2ai4lsHhWI/MndrZyEWCwAJ).
We're also gathering data to support a few [dangling markup
mitigations](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/rOs6YRyBEpw/D3pzVwGJAgAJ),
and dropped support for subresource URLs with [embedded
credentials](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/lx-U_JR2BF0/Hsg1fiZiBAAJ)
and [legacy
protocols](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/bIJdwwoQ98U/-F1aL2FgBAAJ).

We also spend time building[ security
features](https://dev.chromium.org/developers/design-documents/safebrowsing)
that[ users see](https://dev.chromium.org/Home/chromium-security/enamel). To
protect users from Data URI phishing attacks, Chrome shows the “not secure”
warning on Data URIs and [intends to deprecate and
remove](https://groups.google.com/a/chromium.org/forum/#!topic/blink-dev/GbVcuwg_QjM)
content-initiated top-frame navigations to Data URIs. We also brought [AIA
fetching](https://docs.google.com/document/d/1ryqFMSHHRDERg1jm3LeVt7VMfxtXXrI8p49gmtniNP0/edit)
to Chrome for Android, and early metrics show over an 85% reduction in the
fraction of HTTPS warnings caused by misconfigured certificate chains on
Android. We made additional progress on improving Chrome’s captive portal
detection. Chrome now keeps precise attribution of where bad downloads come
from, so we can catch malware and UwS earlier. Chrome 57 also saw the launch of
a [secure time
service](https://www.chromium.org/developers/design-documents/sane-time), for
which early data shows detection of bad client clocks when validating
certificates improving from 78% to 95%.

We see migration to HTTPS as foundational to any web security whatsoever, so
we're actively working to drive #MOARTLS across Google and the Internet at
large. To help people understand the security limitations of non-secure HTTP,
Chrome now marks HTTP pages with passwords or credit card form fields as “not
secure” in the address bar, and is experimenting with in-form contextual
warnings. We’ll [remove
support](https://bugs.chromium.org/p/chromium/issues/detail?id=672605) for EME
over non-secure origins in Chrome 58, and we’ll [remove
support](https://groups.google.com/a/chromium.org/forum/m/#!topic/blink-dev/IVgkxkRNtMo)
for notifications over non-secure origins in Chrome 61. We talked about our
#MOARTLS methodology and the HTTPS business case at
[Enigma](https://www.youtube.com/watch?v=jplIY1GXBHM&feature=youtu.be).

In addition to #MOARTLS, we want to ensure more secure TLS through work on
protocols and the certificate ecosystem. TLS 1.3 is the next, major version of
the Transport Layer Security protocol. In Q1, Chrome tried the first,
significant deployment of TLS 1.3 by a browser. Based on what we learned from
that we hope to fully enable TLS 1.3 in Chrome in Q2.

In February, researchers from Google and CWI Amsterdam successfully mounted a
[collision](https://security.googleblog.com/2017/02/announcing-first-sha1-collision.html)
attack against the SHA-1 hash algorithm. It had been known to be weak for a very
long time, and in Chrome 56 dropped support for website certificates that used
SHA-1. This was the culmination of a plan first
[announced](https://security.googleblog.com/2014/09/gradually-sunsetting-sha-1.html)
back in 2014, which we've
[updated](https://security.googleblog.com/2015/12/an-update-on-sha-1-certificates-in.html)
a [few
times](https://security.googleblog.com/2016/11/sha-1-certificates-in-chrome.html)
since.

As ever, many thanks to all those in the Chromium community who help make the
web more secure!

Cheers

Andrew, on behalf of the Chrome Security Team

For more thrilling security updates and feisty rants,[ subscribe to
security-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/security-dev).
You can find older updates at[
https://dev.chromium.org/Home/chromium-security/quarterly-updates](https://dev.chromium.org/Home/chromium-security/quarterly-updates).

## Q4 2016

Greetings and salutations,

It's time for another update from your friends in Chrome Security, who are hard
at work trying to make Chrome the[ most secure platform to browse the
Internet](https://www.chromium.org/Home/chromium-security). Here’s a recap from
the last quarter of 2016:

Our [Bugs--](https://dev.chromium.org/Home/chromium-security/bugs) effort aims
to find (and exterminate) security bugs.

We announced [OSS-Fuzz](https://github.com/google/oss-fuzz), a new Beta program
developed over the past years with the [Core Infrastructure
Initiative](https://www.coreinfrastructure.org/) community. This program will
provide continuous fuzzing for select core open source software. See full blog
post
[here](https://security.googleblog.com/2016/12/announcing-oss-fuzz-continuous-fuzzing.html).
So far, more than [50
projects](https://github.com/google/oss-fuzz/tree/master/projects) have been
integrated with OSS-Fuzz and we found ~350
[bugs](https://bugs.chromium.org/p/oss-fuzz/issues/list?can=1&q=Type%3ABug%2CBug-Security%20-component%3AInfra%20-status%3ADuplicate%2CWontFix&colspec=ID%20Type%20Component%20Status%20Proj%20Reported%20Owner%20Summary&num=100&start=300).

Security bugs submitted by external researchers can receive cash money from the
[Chrome VRP](https://g.co/ChromeBugRewards).

Last year the Chrome VRP paid out almost one million dollars! More details in a
[blog
post](https://security.googleblog.com/2017/01/vulnerability-rewards-program-2016-year.html)
we did with our colleagues in the Google and Android VRPs.

Bugs still happen, so our[
Guts](https://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense.

[Win32k
lockdown](https://docs.google.com/document/d/1gJDlk-9xkh6_8M_awrczWCaUuyr0Zd2TKjNBCiPO_G4/edit#heading=h.ieb3qn2r8rq1)
for Pepper processes, including Adobe Flash and PDFium was shipped to Windows 10
clients on all channels in October 2016. Soon after the mitigation was enabled,
a Flash 0-day that used win32k.sys as a [privilege
escalation](https://technet.microsoft.com/en-us/library/security/ms16-135.aspx)
vector was discovered being used in the wild, and this was successfully blocked
by this mitigation! James Forshaw from Project Zero also wrote a
[blog](https://googleprojectzero.blogspot.com/2016/11/breaking-chain.html) about
the process of shipping this new mitigation.

A new security mitigation on >= Win8 hit stable in October 2016 (Chrome 54).
This mitigation disables extension points (legacy hooking), blocking a number of
third-party injection vectors. Enabled on all child processes - [CL
chain](https://chromium.googlesource.com/chromium/src/+/c06d6fb1850d6217d35a5cccb1abccd6db0e7a2a).
As usual, you can find the Chromium sandbox documentation
[here](../../developers/design-documents/sandbox/index.md).

[Site
Isolation](https://www.chromium.org/developers/design-documents/site-isolation)
makes the most of Chrome's multi-process architecture to help reduce the scope
of attacks.

Our earlier plan to launch --isolate-extensions in Chrome 54 hit a last minute
delay, and we're now aiming to turn it on in Chrome 56. In the meantime, we've
added support for drag and drop into [out-of-process
iframes](https://www.chromium.org/developers/design-documents/oop-iframes)
(OOPIFs) and for printing an OOPIF. We've fixed several other security and
functional issues for --isolate-extensions as well. We've also started an A/B
trial on Canary to use OOPIFs for Chrome App <webview> tags, and we're close to
starting an A/B trial of --top-document-isolation.

Beyond the browser, our[ web platform
efforts](https://dev.chromium.org/Home/chromium-security/owp) foster
cross-vendor cooperation on developer-facing security features.

After a good deal of experimentation, we (finally) [tightened the behavior of
cookies' \`secure\`
attribute](https://www.chromestatus.com/feature/4506322921848832). [Referrer
Policy](https://www.w3.org/TR/referrer-policy/) moved to a candidate
recommendation, we made solid progress on
[Clear-Site-Data](https://w3c.github.io/webappsec-clear-site-data/), and we
expect to start an [origin
trial](https://github.com/jpchase/OriginTrials/blob/gh-pages/developer-guide.md)
for [Suborigins](https://w3c.github.io/webappsec-suborigins/) shortly.

Looking to the future, we've started to flesh out our proposal for [stronger
origin isolation properties](https://wicg.github.io/isolation/), continued
discussions on a proposal for [setting origin-wide
policy](https://wicg.github.io/origin-policy/), and began working with the IETF
to expand [opt-in Certificate Transparency
enforcement](https://datatracker.ietf.org/doc/draft-stark-expect-ct/) to the
open web. We hope to further solidify all of these proposals in Q1.

We also spend time building[ security
features](https://dev.chromium.org/developers/design-documents/safebrowsing)
that[ users see](https://dev.chromium.org/Home/chromium-security/enamel).

Our security indicator text labels launched in Chrome 55 for “Secure” HTTPS,
“Not Secure” broken HTTPS, and “Dangerous” pages flagged by Safe Browsing. As
part of our long-term effort to mark HTTP pages as non-secure, we built
[address-bar warnings into Chrome
56](https://blog.chromium.org/2016/12/chrome-56-beta-not-secure-warning-web.html)
to mark HTTP pages with a password or credit card form fields as “Not secure”.

We see migration to HTTPS as foundational to any web security whatsoever, so
we're actively working to drive #MOARTLS across Google and the Internet at
large.[ ](https://twitter.com/estark37)

We added a new [HTTPS Usage
section](https://www.google.com/transparencyreport/https/metrics/?hl=en) to the
Transparency Report, which shows how the percentage of Chrome pages loaded over
HTTPS increases with time. We talked externally at O’Reilly Security NYC +
Amsterdam and [Chrome Dev Summit](https://www.youtube.com/watch?v=iP75a1Y9saY)
about upcoming HTTP UI changes and the business case for HTTPS. We published
[positive
stories](https://blog.chromium.org/2016/11/heres-to-more-https-on-web.html)
about HTTPS migrations.

In addition to #MOARTLS, we want to ensure more secure TLS.

We [concluded](https://www.imperialviolet.org/2016/11/28/cecpq1.html) our
experiment with post-quantum key agreement in TLS. We implemented TLS 1.3 draft
18, which will be enabled for a fraction of users with Chrome 56.

And here are some other areas we're still investing heavily in:

Keeping users safe from Unwanted Software (UwS, pronounced 'ooze') and improving
the [Chrome Cleanup Tool](https://www.google.com/chrome/cleanup-tool/), which
has helped millions remove UwS that was injecting ads, changing settings, and
otherwise blighting their machines.

Working on usable, understandable permissions prompts. We're experimenting with
different prompt UIs, tracking prompt interaction rates, and continuing to learn
how best to ensure users are in control of powerful permissions.

As ever, many thanks to all those in the Chromium community who help make the
web more secure!

Cheers

Andrew, on behalf of the Chrome Security Team

For more thrilling security updates and feisty rants,[ subscribe to
security-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/security-dev).
You can find older updates at[
https://dev.chromium.org/Home/chromium-security/quarterly-updates](https://dev.chromium.org/Home/chromium-security/quarterly-updates).

## Q3 2016

Greetings and salutations!

It's time for another update from your friends in Chrome Security, who are hard
at work trying to make Chrome the[ most secure platform to browse the
Internet](https://www.chromium.org/Home/chromium-security). Here’s a recap from
last quarter:

Our[ Bugs--](https://dev.chromium.org/Home/chromium-security/bugs) effort aims
to find (and exterminate) security bugs.

We have continued to improve upon our [libFuzzer](http://go/libfuzzer-chrome)
and
[AFL](https://cs.chromium.org/chromium/src/third_party/afl/?q=third_party/afl&sq=package:chromium&dr)
integration with ClusterFuzz, which includes automated performance analysis and
quarantining of bad units (like slow units, leaks, etc). We have scaled our code
coverage to
~[160](https://cs.chromium.org/search/?q=%22+int+LLVMFuzzerTestOneInput%22+-libFuzzer/src+-llvm/+-buildtools+-file:.md&sq=package:chromium&type=cs)
targets with help from Chrome developers, who contributed these during the
month-long
[Fuzzathon](https://groups.google.com/a/chromium.org/forum/#!topic/chromium-dev/MAiBRTllPuI).
We have improved our infrastructure reliability and response times by adding a
24x7 monitoring solution, and fixing more than two dozen fuzzers in the process.
Finally, we have refined our crash bucketization algorithm and enabled automatic
bug filing remove human latency in filing regression bugs — long live the
machines!

For [Site
Isolation](https://www.chromium.org/developers/design-documents/site-isolation),
the first uses of [out-of-process
iframes](https://www.chromium.org/developers/design-documents/oop-iframes)
(OOPIFs) have reached the Stable channel in Chrome 54!

We're using OOPIFs for --isolate-extensions mode, which ensures that web content
is never put into a privileged extension process. In the past quarter, we made
significant progress and fixed all our blocking bugs, including enabling the new
session history logic by default, supporting cross-process POST submissions, and
IME in OOPIFs. We also fixed bugs in painting, input events, and many other
areas. As a result, --isolate-extensions mode has been enabled for 50% of M54
Beta users and is turned on by default in M55. From here, we plan to further
improve OOPIFs to support --top-document-isolation mode, Chrome App <webview>
tags, and Site Isolation for real web sites.

We also spend time building[ security
features](https://dev.chromium.org/developers/design-documents/safebrowsing)
that[ users see](https://dev.chromium.org/Home/chromium-security/enamel).

We
[overhauled](https://docs.google.com/document/u/2/d/1jIfCjcsZUL6ouLgPOsMORGcTTXc7OTwkcBQCkZvDyGE/pub)
Chrome’s site security indicators in Chrome 52 on Mac and Chrome 53 on all other
platforms, including adding new icons for Safe Browsing. These icons were the
result of extensive user research which we shared in a [peer-reviewed
paper](https://www.usenix.org/system/files/conference/soups2016/soups2016-paper-porter-felt.pdf).
Lastly, we made recovering blocked-downloads much [less
confusing](https://docs.google.com/document/d/1M9AvvXafVRSNquKGhzjAqo3Pl7sSATuEECA78pfkpmA/edit#).

We like to avoid showing unnecessarily scary warnings when we can. We analyzed
data from opted-in Safe Browsing Extended Reporting users to quantify the major
causes of spurious TLS warnings, like [bad client
clocks](https://dev.chromium.org/developers/design-documents/sane-time) and
[misconfigured intermediate
certificates](https://docs.google.com/document/d/1ryqFMSHHRDERg1jm3LeVt7VMfxtXXrI8p49gmtniNP0/edit?pli=1).
We also launched two experiments,
[Expect-CT](https://docs.google.com/document/d/1VDtHiKa5c96ohP_p-V1k6u83fIh952e_szZVypO4AvQ/edit)
and
[Expect-Staple](https://docs.google.com/document/d/1aISglJIIwglcOAhqNfK-2vtQl-_dWAapc-VLDh-9-BE/edit),
to help site owners deploy advanced new TLS features (Certificate Transparency
and OCSP stapling) without causing warnings for their users.

Beyond the browser, our[ web platform
efforts](https://dev.chromium.org/Home/chromium-security/owp) foster
cross-vendor cooperation on developer-facing security features.

We continued to lock down the security of the web platform while also expanding
capabilities to developers. We helped lock down cookies by starting to ship
[Strict Secure Cookies](https://www.chromestatus.com/feature/4506322921848832).
Similarly, we also shipped the [Referrer
Policy](https://www.w3.org/TR/referrer-policy/) spec and policy header. [Content
Security Policy](https://www.w3.org/TR/CSP3/) was expanded with the
[strict-dynamic](https://www.w3.org/TR/CSP3/#strict-dynamic-usage) and
[unsafe-hashed-attributes](https://www.w3.org/TR/CSP3/#unsafe-hashed-attributes-usage)
directives. Our work on
[suborigins](https://w3c.github.io/webappsec-suborigins/) continued, updating
the serialization and adding new web platform support.

We've also been working on making users feel more in control of powerful
permissions.

In M55 and M56 we will be running experiments on permissions prompts to evaluate
how this affects acceptance and decision rates. The experiments are to let users
make temporary decisions, to auto-deny prompts if users keep ignoring them, and
making permission prompts modal.

We see migration to HTTPS as foundational to any web security whatsoever, so
we're actively working to drive #MOARTLS across Google and the Internet at
large.[ ](https://twitter.com/estark37)

We
[announced](https://security.googleblog.com/2016/09/moving-towards-more-secure-web.html)
concrete steps towards marking HTTP sites as non-secure in Chrome UI — starting
with marking HTTP pages with password or credit card form fields as “Not secure”
starting in Chrome 56 (Jan 2017). We [added YouTube and
Calendar](https://security.googleblog.com/2016/08/adding-youtube-and-calendar-to-https.html)
to the HTTPS Transparency Report. We’re also happy to report that
[www.google.com](http://www.google.com/) [uses
HSTS](https://security.googleblog.com/2016/07/bringing-hsts-to-wwwgooglecom.html)!

In addition to #MOARTLS, we want to ensure more secure TLS.

We continue to work on TLS 1.3, a major revision of TLS. For current revisions,
we’re also keeping the TLS ecosystem running smoothly with a little
[grease](https://tools.ietf.org/html/draft-davidben-tls-grease-01). We have
removed [DHE based
ciphers](https://www.chromestatus.com/feature/5128908798164992) and added
[RSA-PSS](https://www.chromestatus.com/features/5748550642171904). Finally,
having removed RC4 from Chrome earlier this year, we’ve now removed it from
BoringSSL’s TLS logic completely.

We launched a very rough prototype of
[Roughtime](https://roughtime.googlesource.com/roughtime), a combination of NTP
and Certificate Transparency. In parallel we’re investigating what reduction in
Chrome certificate errors a secure clock like Roughtime could give us.

We also continued our experiments with [post-quantum
cryptography](https://security.googleblog.com/2016/07/experimenting-with-post-quantum.html)
by implementing [CECPQ1](https://www.chromestatus.com/features/5749214348836864)
to help gather some real world data.

As ever, many thanks to all those in the Chromium community who help make the
web more secure!

Cheers

Andrew on behalf of the Chrome Security Team

## Q2 2016

Greetings Earthlings,

It's time for another update from your friends in Chrome Security, who are hard
at work trying to make Chrome the[ most secure platform to browse the
Internet](https://www.chromium.org/Home/chromium-security). Here’s a recap from
last quarter:

**Our[ Bugs--](https://dev.chromium.org/Home/chromium-security/bugs)**** effort
aims to find (and exterminate) security bugs.** At the start of the quarter, we
initiated a team-wide Security FixIt to trim the backlog of open issues… a bit
of Spring cleaning our issue tracker, if you will :) With the help of dozens of
engineers across Chrome, we fixed over 61 Medium+ severity security bugs in 2
weeks and brought the count of open issues down to 22! On the fuzzing front,
we’ve added support for[ AFL](http://lcamtuf.coredump.cx/afl/) and continued to
improve the[
libFuzzer](http://llvm.org/docs/LibFuzzer.html)-[ClusterFuzz](https://www.chromium.org/Home/chromium-security/bugs/using-clusterfuzz)
integration, both of which allow coverage-guided testing on a per-function
basis. The number of libFuzzer based fuzzers have expanded from 70 to[
115](https://cs.chromium.org/search/?q=TestOneInput%5C(const+-file:third_party/llvm+-file:third_party/libFuzzer/src&sq=package:chromium&type=cs),
and we’re processing ~500 Billion testcases every day! We’re also researching
new ways to improve fuzzer efficiency and maximize code coverage
([example](https://chromium.googlesource.com/chromium/src/+/1a6bef1675e05626a4692ab6fa43cbbc5515299b)).
In response to recent trends from[ Vulnerability Reward Program
(VRP)](https://www.google.com/about/appsecurity/chrome-rewards/index.html) and[
Pwnium](http://blog.chromium.org/2015/02/pwnium-v-never-ending-pwnium.html)
submissions, we wrote a new fuzzer for v8 builtins, which has already yielded[
bugs](https://bugs.chromium.org/p/chromium/issues/detail?id=625752). Not
everything can be automated, so we started auditing parts of[
mojo](https://www.chromium.org/developers/design-documents/mojo), Chrome’s new
IPC mechanism, and found several issues
([1](https://bugs.chromium.org/p/chromium/issues/detail?id=611887),[
2](https://bugs.chromium.org/p/chromium/issues/detail?id=612364),[
3](https://bugs.chromium.org/p/chromium/issues/detail?id=612613),[
4](https://bugs.chromium.org/p/chromium/issues/detail?id=613698),[
5](https://bugs.chromium.org/p/chromium/issues/detail?id=622522)).

**Bugs still happen, so our[
Guts](https://dev.chromium.org/Home/chromium-security/guts)**** effort builds in
multiple layers of defense.** Many Android apps use[
WebView](https://developer.android.com/reference/android/webkit/WebView.html) to
display web content inline within their app. A compromised WebView can get
access to an app’s private user data and a number of Android system services /
device drivers. To mitigate this risk, in the upcoming release of[ Android
N](https://developer.android.com/preview/support.html#dp3), we’ve worked to
move[
WebView](https://developer.android.com/reference/android/webkit/WebView.html)
rendering out-of-process into a sandboxed process. This new process model is
still experimental and can be enabled under Developer Options in Settings. On
Windows, a series of ongoing stability experiments with[ App
Container](https://www.chromium.org/developers/design-documents/sandbox#TOC-App-Container-low-box-token-:)
and[ win32k
lockdown](https://www.chromium.org/developers/design-documents/sandbox#TOC-Win32k.sys-lockdown:)
for[
PPAPI](https://www.chromium.org/nativeclient/getting-started/getting-started-background-and-basics#TOC-Pepper-Plugin-API-PPAPI-)
processes (i.e. Flash and pdfium) have given us good data that puts us in a
position to launch both of these new security mitigations on Windows 10 very
soon!

**For[ Site
Isolation](https://www.chromium.org/developers/design-documents/site-isolation)****,
we're getting close to enabling --isolate-extensions for everyone.** We've been
hard at work fixing launch blocking bugs, and[ out-of-process
iframes](https://www.chromium.org/developers/design-documents/oop-iframes)
(OOPIFs) now have support for POST submissions, fullscreen, find-in-page, zoom,
scrolling, Flash, modal dialogs, and file choosers, among other features. We've
also made lots of progress on the new navigation codepath, IME, and the task
manager, along with fixing many layout tests and crashes. Finally, we're
experimenting with --top-document-isolation mode to keep the main page
responsive despite slow third party iframes, and with using OOPIFs to replace
BrowserPlugin for the <webview> tag.

**We also spend time building[ security
features](https://dev.chromium.org/developers/design-documents/safebrowsing)
that[ users see](https://dev.chromium.org/Home/chromium-security/enamel)****.
**We’re overhauling the omnibox security iconography in Chrome -- new,[
improved](https://www.usenix.org/conference/soups2016/technical-sessions/presentation/porter-felt)
connection security indicators are now in Chrome Beta (52) on Mac and Chrome Dev
(53) for all other platforms. We created a[
reference](https://github.com/google/safebrowsing) interstitial warning that
developers can use for their implementations of the[ Safe Browsing
API](https://developers.google.com/safe-browsing/). Speaking of Safe Browsing,
we’ve extended protection to cover files downloaded by Flash apps, we’re
evaluating many more file types than before, and we closed several gaps that
were reported via our Safe Browsing[ Download Protection
VRP](https://www.google.com/about/appsecurity/chrome-rewards/) program.

**Beyond the browser, our[ web platform
efforts](https://dev.chromium.org/Home/chromium-security/owp)**** foster
cross-vendor cooperation on developer-facing security features. ** We shipped an
implementation of the[ Credential Management
API](https://w3c.github.io/webappsec-credential-management/) (and[ presented a
detailed overview](https://youtu.be/MnvUlGFb3GQ?t=7m23s) at Google I/O),
iterated on[ Referrer Policy](https://www.w3.org/TR/referrer-policy/) with a[
\`referrer-policy\`
header](https://www.w3.org/TR/referrer-policy/#referrer-policy-header)
implementation behind a flag, and improved our support for[ SameSite
cookies](https://tools.ietf.org/html/draft-ietf-httpbis-cookie-same-site). We're
continuing to experiment with[
Suborigins](https://w3c.github.io/webappsec-suborigins/) with developers both
inside and outside Google, built a prototype of[
CORS-RFC1918](https://mikewest.github.io/cors-rfc1918/), and introduce safety
nets to protect against XSS vulnerabilities due to browser bugs\[1\].

**We've also been working on making users feel more in control of powerful
permissions.** All permissions will soon be[ scoped to
origins](https://codereview.chromium.org/2075103002/), and we've started
implementing[ permission
delegation](https://noncombatant.github.io/permission-delegation-api/) (which is
becoming part of[ feature policy](https://github.com/igrigorik/feature-policy)).
We’re also actively working to show fewer permission prompts to users, and to
improve the prompts and UI we do show... subtle, critical work that make web
security more human-friendly (and thus, effective).

**We see migration to HTTPS as foundational to any web security whatsoever, so
we're actively working to drive #MOARTLS across Google and the Internet at
large.**[ Emily](https://twitter.com/estark37) and[
Emily](https://twitter.com/emschec) busted HTTPS myths for large audiences at[
Google
I/O](https://www.youtube.com/watch?v=YMfW1bfyGSY&index=19&list=PLOU2XLYxmsILe6_eGvDN3GyiodoV3qNSC)
and the[ Progressive Web App dev
summit](https://www.youtube.com/watch?v=e6DUrH56g14). The[ HSTS Preload
list](https://cs.chromium.org/chromium/src/net/http/transport_security_state_static.json)
has seen[ 3x growth](https://twitter.com/lgarron/status/747530273047273472)
since the beginning of the year – a great problem to have! We’ve addressed some[
growth
hurdles](https://docs.google.com/document/d/1LqpwT2aAekrWPtLui5GYdHSGlZNMNRYmPR14NXMRsQ4/edit#)
by a rewrite of the[ submission site](https://hstspreload.appspot.com/), and
we’re actively working on the preload list infrastructure and how to
additionally scale in the long term.

**In addition to #MOARTLS, we want to ensure more secure**** TLS.** Some of us
have been involved in the[ TLS 1.3 standardization
work](https://tlswg.github.io/tls13-spec/) and[
implementation](https://boringssl.googlesource.com/boringssl/). On the PKI
front, and as part of our[ Expect CT
project](https://docs.google.com/document/d/1VDtHiKa5c96ohP_p-V1k6u83fIh952e_szZVypO4AvQ/edit),
we built the infrastructure in Chrome that will help site owners track down
certificates for their sites that are not publicly logged in[ Certificate
Transparency](https://www.certificate-transparency.org/) logs. As of Chrome 53,
we’ll be requiring Certificate Transparency information for certificates issued
by Symantec-operated CAs, per[ our announcement last
year](https://security.googleblog.com/2015/10/sustaining-digital-certificate-security.html).
We also[ launched some post-quantum cipher suite
experiments](https://security.googleblog.com/2016/07/experimenting-with-post-quantum.html)
to protect everyone from... crypto hackers of the future and more advanced
worlds ;)

For more thrilling security updates and feisty rants, [subscribe to
security-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/security-dev).
You can find older updates at
<https://dev.chromium.org/Home/chromium-security/quarterly-updates>.

Happy Hacking,

Parisa, on behalf of Chrome Security

\[1\] Please[ let us
know](http://dev.chromium.org/Home/chromium-security/reporting-security-bugs) if
you manage to work around them!

## Q1 2016

Greetings web fans,

The[ Bugs--](https://dev.chromium.org/Home/chromium-security/bugs) effort aims
to find (and exterminate) security bugs. On the fuzzing front, we’ve continued
to improve the integration between
[libFuzzer](http://llvm.org/docs/LibFuzzer.html) and
[ClusterFuzz](https://www.chromium.org/Home/chromium-security/bugs/using-clusterfuzz),
which allows coverage-guided testing on a per-function basis. With the help of
many developers across several teams, we’ve expanded our collection of fuzzing
targets in Chromium (that use libFuzzer) to 70! Not all bugs can be found by
fuzzing, so we invest effort in targeted code audits too. We wrote a [guest post
on the Project Zero
blog](https://googleprojectzero.blogspot.com/2016/02/racing-midi-messages-in-chrome.html)
describing one of the more interesting vulnerabilities we discovered. Since we
find a lot of bugs, we also want to make them easier to manage. We’ve updated
our [Sheriffbot](https://www.chromium.org/issue-tracking/autotriage) tool to
simplify the addition of new rules and expanded it to help manage functional
bugs in addition just security issues. We’ve also automated assigning [security
severity](https://www.chromium.org/developers/severity-guidelines)
recommendations. Finally, we continue to run our [vulnerability reward
program](https://www.google.com/about/appsecurity/chrome-rewards/) to recognize
bugs discovered from researchers outside of the team. As of M50, [we’ve paid out
over $2.5
million](https://chrome.googleblog.com/2016/04/chrome-50-releases-and-counting.html)
since the start of the reward program, including over $500,000 in 2015. Our
median payment amount for 2015 was $3,000 (up from $2,000 for 2014), and we want
to see that increase again this year!

Bugs still happen, so our[
Guts](https://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense. On Android, our
[seccomp-bpf](https://www.kernel.org/doc/Documentation/prctl/seccomp_filter.txt)
experiment has been running on the Dev channel and will advance to the Stable
and Beta channels with M50.

Chrome on Windows is evolving rapidly in step with the operating system. We
shipped [four new layers of defense in
depth](https://bugs.chromium.org/p/chromium/issues/detail?id=504006) to take
advantage of the latest capabilities in Windows 10, some of which patch
vulnerabilities found by our own
[research](https://googleprojectzero.blogspot.co.uk/2015/05/in-console-able.html)
and
[feedback](https://bugs.chromium.org/p/project-zero/issues/detail?id=213&redir=1)!
There was great media attention when these changes landed, from [Ars
Technica](http://arstechnica.com/information-technology/2016/02/chrome-picks-up-bonus-security-features-on-windows-10/)
to a [Risky Business podcast](http://risky.biz/RB398), which said: “There have
been some engineering changes to Chrome on Windows 10 which look pretty good. …
It’s definitely the go-to browser, when it comes to not getting owned on the
internet. And it’s a great example of Google pushing the state of the art in
operating systems.”

For our[ Site
Isolation](https://www.chromium.org/developers/design-documents/site-isolation)
effort, we have expanded our on-going launch trial of --isolate-extensions to
include 50% of both Dev Channel and Canary Channel users! This mode uses
[out-of-process
iframes](https://www.chromium.org/developers/design-documents/oop-iframes)
(OOPIFs) to keep dangerous web content out of extension processes. (See
[here](https://www.chromium.org/developers/design-documents/oop-iframes#TOC-Dogfooding)
for how to try it.) We've fixed many launch blocking bugs, and improved support
for navigation, input events, hit testing, and security features like CSP and
mixed content. We improved our test coverage and made progress on updating
features like fullscreen, zoom, and find-in-page to work with OOPIFs. We're also
excited to see progress on other potential uses of OOPIFs, including the
<webview> tag and an experimental "top document isolation" mode.

We spend time building[ security
features](https://dev.chromium.org/developers/design-documents/safebrowsing)
that[ people see](https://dev.chromium.org/Home/chromium-security/enamel). In
response to user feedback, we’ve replaced [the old full screen
prompt](https://bugs.chromium.org/p/chromium/issues/attachment?aid=178299&inline=1)
with a [new, lighter weight ephemeral
message](https://bugs.chromium.org/p/chromium/issues/attachment?aid=222712&inline=1)
in M50 across Windows and Linux. We launched a few bug fixes and updates to the
[Security
panel](https://developers.google.com/web/updates/2015/12/security-panel?hl=en),
which we continue to iterate on and support in an effort to drive forward HTTPS
adoption. We also continued our work on [removing powerful features on insecure
origins](https://www.chromium.org/Home/chromium-security/deprecating-powerful-features-on-insecure-origins)
(e.g. [geolocation](https://crbug.com/561641)).

We’re working on preventing abuse of powerful features on the web. We continue
to support great “permissions request” UX, and have started reaching out to top
websites to directly help them improve how they request permissions for
[powerful
APIs](https://www.chromium.org/Home/chromium-security/prefer-secure-origins-for-powerful-new-features).
To give top-level websites more control over how iframes use permissions, we
started external discussions about a new [Permission Delegation
API](http://noncombatant.github.io/permission-delegation-api/). We also
[extended](https://security.googleblog.com/2016/03/get-rich-or-hack-tryin.html)
our [vulnerability rewards
program](https://www.google.com/about/appsecurity/chrome-rewards/index.html#rewards)
to support [Safe
Browsing](https://www.google.com/transparencyreport/safebrowsing/) reports, in a
first program of its kind.

Beyond the browser, our[ web platform
efforts](https://dev.chromium.org/Home/chromium-security/owp) foster
cross-vendor cooperation on developer-facing security features. We now have an
implementation of [Suborigins](https://w3c.github.io/webappsec-suborigins/)
behind a flag, and have been experimenting with Google developers on usage. We
polished up the [Referrer Policy](https://www.w3.org/TR/referrer-policy/) spec,
refined its integration with
[ServiceWorker](https://www.w3.org/TR/service-workers/) and Fetch, and shipped
the \`referrerpolicy\` attribute from that document. We're excited about the
potential of new CSP expressions like
['unsafe-dynamic'](https://w3c.github.io/webappsec-csp/#unsafe-dynamic-usage),
which will ship in Chrome 52 (and is experimentally deployed on [our shiny new
bug tracker](https://bugs.chromium.org/p/chromium/issues/list)). In that same
release, we finally shipped [SameSite
cookies](https://tools.ietf.org/html/draft-west-first-party-cookies), which we
hope will help prevent
[CSRF](https://en.wikipedia.org/wiki/Cross-site_request_forgery). Lastly, we're
working to pay down some technical debt by refactoring our Mixed Content
implementation and X-Frame-Options to work in an OOPIF world.

We see migration to HTTPS as foundational to any security whatsoever
([and](https://tools.ietf.org/html/rfc7258)[
we're](https://www.iab.org/2014/11/14/iab-statement-on-internet-confidentiality/)[
not](https://www.iab.net/iablog/2015/03/adopting-encryption-the-need-for-https.html)[
the](https://w3ctag.github.io/web-https/)[ only](https://https.cio.gov/)[
ones](https://blog.mozilla.org/security/2015/04/30/deprecating-non-secure-http/)),
so we're actively working to drive #MOARTLS across Google and the Internet at
large. We worked with a number of teams across Google to help [publish an HTTPS
Report
Card](https://security.googleblog.com/2016/03/securing-web-together_15.html),
which aims to hold Google and other top sites accountable, as well as encourage
others to encrypt the web. In addition to #MOARTLS, we want to ensure more
secure TLS. [We mentioned we were working on it last
time](https://groups.google.com/a/chromium.org/forum/#!msg/security-dev/kVfCywocUO8/vgi_rQuhKgAJ),
but RC4 support is dead! The [insecure TLS version fallback is also
gone](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/yz1lU9YTeys/yCsK50I3CQAJ).
With help from the [libFuzzer](http://llvm.org/docs/LibFuzzer.html) folks, we
got much better fuzzing coverage on
[BoringSSL](https://boringssl.googlesource.com/boringssl/), which resulted in
[CVE-2016-0705](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-0705).
We ended up adding a "fuzzer mode" to the SSL stack to help the fuzzer get past
cryptographic invariants in the handshake, which smoked out some minor (memory
leak) bugs.

Last, but not least, we rewrote a large chunk of BoringSSL's ASN.1 parsing with
a simpler and more standards-compliant stack.

For more thrilling security updates and feisty rants, subscribe to
[security-dev@chromium.org](mailto:security-dev@chromium.org). You can find
older updates at
<https://dev.chromium.org/Home/chromium-security/quarterly-updates>.

Happy Hacking,

Parisa, on behalf of Chrome Security

## Q4 2015

Happy 2016 from the Chrome Security Team!

For those that don’t know us already, we do stuff to help make Chrome the [most
secure platform to browse the
Internet](https://www.chromium.org/Home/chromium-security). Here’s a recap of
some work from last quarter:

**The [Bugs--](https://dev.chromium.org/Home/chromium-security/bugs) effort aims
to find (and exterminate) security bugs.** We’ve integrated
[libFuzzer](http://llvm.org/docs/LibFuzzer.html) into
[ClusterFuzz](https://www.chromium.org/Home/chromium-security/bugs/using-clusterfuzz),
which means we can do coverage-guided fuzz testing on a per-function basis. The
result, as you may have guessed, is several [new
bugs](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+label%3AStability-LibFuzzer&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles).
The Bugs-- team has a larger goal this year to help Chromium developers write a
ClusterFuzz fuzzer alongside every unittest, and libFuzzer integration is an
important step toward achieving that goal. Separately, we’ve made security
improvements and cleanups in the [Pdfium](https://code.google.com/p/pdfium/)
codebase and [fixed lots of open
bugs](https://code.google.com/p/chromium/issues/list?can=1&q=status%3AFixed+type%3DBug-Security+owner%3Aochang+Cr%3DInternals-Plugins-PDF).
We also started some manual code auditing efforts, and discovered several high
severity bugs ([here](https://crbug.com/555784),
[here](https://crbug.com/559310), and [here](https://crbug.com/570427)), and
[1](https://crbug.com/564501) critical severity bug.

**Bugs still happen, so our
[Guts](https://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense.** On Android, we’re running an experiment that adds
an additional
[seccomp-bpf](https://www.kernel.org/doc/Documentation/prctl/seccomp_filter.txt)
sandbox to renderer processes, like we already do on Desktop Linux and Chrome
OS. On Windows 8 (and above), a Win32k lockdown experiment has been implemented
for PPAPI plugins including Flash and Pdfium to help reduce the kernel attack
surface for potential sandbox escapes. Also on Windows 8 (and above), an
AppContainer sandbox experiment has been introduced, which further reduces
kernel attack surface and blocks network communication from renderers.

**Our [Site
Isolation](https://www.chromium.org/developers/design-documents/site-isolation)
effort reached a large milestone in December****: running trials of the
--isolate-extensions mode on real Chrome Canary users!** This mode uses
[out-of-process
iframes](https://www.chromium.org/developers/design-documents/oop-iframes) to
isolate extension processes from web content for security. ([Give it a
try!](https://dev.chromium.org/developers/design-documents/oop-iframes#TOC-Dogfooding))
The trials were made possible by many updates to session history, session
restore, extensions, painting, focus, save page, popup menus, and more, as well
as numerous crash fixes. We are continuing to fix the [remaining blocking
issues](https://csreis.github.io/oop-iframe-dependencies/), and we aim to launch
both --isolate-extensions and the broader Site Isolation feature in 2016.

**We also spend time building [security
features](https://dev.chromium.org/developers/design-documents/safebrowsing)
that [users see](https://dev.chromium.org/Home/chromium-security/enamel). **The
Safe Browsing team publicly announced a [new social engineering
policy](https://googleonlinesecurity.blogspot.com/2015/11/safe-browsing-protection-from-even-more.html),
expanding Chrome’s protection against deceptive sites beyond phishing. One major
milestone is the [launch of Safe Browsing in Chrome for
Android](https://googleonlinesecurity.blogspot.com/2015/12/protecting-hundreds-of-millions-more.html),
protecting hundreds of millions of additional users from phishing, malware, and
other web threats! This is on by default and is already stopping millions of
attacks on mobile Chrome users. The next time you come across a Safe Browsing
warning, you can search for the blocked website in the
[new](https://googleonlinesecurity.blogspot.com/2015/10/behind-red-warning-more-info-about.html)
[Site Status
section](https://www.google.com/transparencyreport/safebrowsing/diagnostic/) of
the Transparency Report to learn why it’s been flagged by our systems. On the
other hand, we’re also trying to show users fewer security warnings in the first
place by decreasing our false positive rate for HTTPS warnings. We spent a large
part of the quarter analyzing client errors that contribute to false alarm HTTPS
errors; check out our [Real World Crypto
talk](https://docs.google.com/presentation/d/1Qmpl-5epx0B5C2t4XsUTyjgbwab_rXfK_4iHqX3IC30/edit?pref=2&pli=1#slide=id.gf44795496_0_1)
for more details.

**Beyond the browser, our [web platform
efforts](https://dev.chromium.org/Home/chromium-security/owp) foster
cross-vendor cooperation on developer-facing security features.** We've made
good progress [with folks in the
IETF](https://lists.w3.org/Archives/Public/ietf-http-wg/2015OctDec/0165.html) to
make some meaningful changes to cookies; [cookie
prefixes](https://www.chromestatus.com/features/4952188392570880) and [locking
down 'secure' cookies](https://www.chromestatus.com/features/4506322921848832)
will be shipping shortly. [Subresource
Integrity](https://lists.w3.org/Archives/Public/public-webappsec/2016Jan/0020.html)
and [Mixed
Content](https://lists.w3.org/Archives/Public/public-webappsec/2016Jan/0021.html)
are trucking along the W3C Recommendation path, we've solidified our [Suborigins
proposal](https://w3c.github.io/webappsec-suborigins/), and have our eyes on
some new hotness like [HSTS Priming](https://mikewest.github.io/hsts-priming/),
[CSP3](https://w3c.github.io/webappsec-csp/)
[bits](https://w3c.github.io/webappsec-csp/embedded/)
[and](https://w3c.github.io/reporting/)
[pieces](https://w3c.github.io/webappsec-csp/cookies/), and [limiting access to
local network resources](https://mikewest.github.io/cors-rfc1918/).

**We see migration to HTTPS as foundational to any security whatsoever
([and](https://tools.ietf.org/html/rfc7258)
[we're](https://www.iab.org/2014/11/14/iab-statement-on-internet-confidentiality/)
[not](https://www.iab.net/iablog/2015/03/adopting-encryption-the-need-for-https.html)
[the](https://w3ctag.github.io/web-https/) [only](https://https.cio.gov/)
[ones](https://blog.mozilla.org/security/2015/04/30/deprecating-non-secure-http/)),****
so we're actively working to drive #MOARTLS across Google and the Internet at
large.** We've continued our effort to [deprecate powerful features on insecure
origins](https://www.chromium.org/Home/chromium-security/deprecating-powerful-features-on-insecure-origins)
by readying to block insecure usage of [geolocation
APIs](http://dev.w3.org/geo/api/spec-source.html). We also took to the
[stage](https://www.youtube.com/watch?v=9WuP4KcDBpI) at the [Chrome Dev
Summit](https://developer.chrome.com/devsummit) to spread the word, telling
developers about what we’re doing in Chrome to make deploying TLS easier and
more secure.

In addition to more TLS, we want to ensure more secure TLS, which depends
heavily on the certificate ecosystem. Via [Certificate
Transparency](http://www.certificate-transparency.org/), we [detected a
fraudulent Symantec-issued certificate in
September](https://googleonlinesecurity.blogspot.com/2015/09/improved-digital-certificate-security.html),
which [subsequently revealed a pattern of additional misissued
certificates](https://googleonlinesecurity.blogspot.com/2015/10/sustaining-digital-certificate-security.html).
Independent of that incident, we took proactive measures to protect users from a
Symantec Root Certificate that was being decommissioned in a way that puts users
at risk (i.e. no longer complying with the CA/Browser Forum’s [Baseline
Requirements](https://cabforum.org/about-the-baseline-requirements/)). Other
efforts include working with
[Mozilla](https://groups.google.com/forum/#!topic/mozilla.dev.platform/JIEFcrGhqSM/discussion)
and
[Microsoft](https://blogs.windows.com/msedgedev/2015/09/01/ending-support-for-the-rc4-cipher-in-microsoft-edge-and-internet-explorer-11/)
to [phase out RC4 ciphersuite
support](https://groups.google.com/a/chromium.org/forum/#!msg/security-dev/kVfCywocUO8/vgi_rQuhKgAJ),
and [continuing the deprecation of SHA-1
certificates](https://googleonlinesecurity.blogspot.com/2015/12/an-update-on-sha-1-certificates-in.html),
which were shown to be [even weaker than previously
believed](https://sites.google.com/site/itstheshappening/). To make it easier
for developers and site operators to understand these changes, we debuted a new
[Security
Panel](https://developers.google.com/web/updates/2015/12/security-panel?hl=en)
that provides enhanced diagnostics and will continue to be improved with richer
diagnostics in the coming months.

For more thrilling security updates and feisty rants, subscribe to
[security-dev@chromium.org](mailto:security-dev@chromium.org). You can find
older updates at
<https://dev.chromium.org/Home/chromium-security/quarterly-updates>.

Happy Hacking,

Parisa, on behalf of Chrome Security

## Q3 2015

Hello from the Chrome Security Team!

For those that don’t know us already, we do stuff to help make Chrome the[ most
secure platform to browse the
Internet](https://www.chromium.org/Home/chromium-security). Here’s a recap of
some work from last quarter:

The[ Bugs--](https://dev.chromium.org/Home/chromium-security/bugs) effort aims
to find (and exterminate) security bugs. We’ve continued our collaboration with
Android Security team and now have a fully functional
[AddressSanitizer](http://clang.llvm.org/docs/AddressSanitizer.html) (ASAN)
build configuration of [AOSP](https://source.android.com/index.html) master
(public instructions
[here](http://source.android.com/devices/tech/debug/asan.html)).
[ClusterFuzz](http://dev.chromium.org/Home/chromium-security/bugs/using-clusterfuzz)
is helping Android Security team triage and verify bugs, including incoming
[vulnerability
reward](https://www.google.com/about/appsecurity/android-rewards/) submissions,
and now supports custom APK uploads and the ability to launch commands. Back on
the Chrome front, we’re working on enabling [Control Flow Integrity (CFI)
checks](https://code.google.com/p/chromium/issues/detail?id=464797) on Linux,
which converts invalid vptr accesses into non-exploitable crashes; [8
bugs](https://code.google.com/p/chromium/issues/list?can=1&q=linux_cfi_chrome&colspec=ID+Pri+M+Stars+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)
discovered so far! We’ve made [numerous
improvements](https://code.google.com/p/chromium/issues/detail?id=511270) to how
we fuzz Chrome on Android with respect to speed and accuracy. We also made some
progress toward our goal of expanding ClusterFuzz platform support to include
iOS. In our efforts to improve Chrome Stability, we added
[LeakSanitizer](http://clang.llvm.org/docs/LeakSanitizer.html) (LSAN) into our
list of supported memory tools, which has already found [38
bugs](https://code.google.com/p/chromium/issues/list?can=1&q=label%3AClusterFuzz+-status%3AWontFix%2CDuplicate+%22-leak%22+linux_lsan_chrome_mp&sort=id+-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles).

Bugs still happen, so our[
Guts](https://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense. Plugin security remains a very important area of
work. With the final [death](https://g.co/npapi) of unsandboxed NPAPI plugins in
September, we’ve continued to introduce mitigations for the remaining sandboxed
PPAPI (Pepper) plugins. First, we implemented [support for Flash component
updates on Linux](https://codereview.chromium.org/1261333004/), a long-standing
feature request, which allows us to respond to [Flash
0-day](http://www.zdnet.com/article/adobe-releases-emergency-patch-for-flash-zero-day-flaw/)
incidents without waiting to qualify a new release of Chrome. We’ve also been
spending time improving the code quality and test coverage of
[Pdfium](https://code.google.com/p/pdfium/), the [now open-source version of the
Foxit PDF
reader](https://plus.sandbox.google.com/+FrancoisBeaufort/posts/9wwSiWDDKKP). In
addition, we have been having some success with enabling [Win32k syscall
filtering](https://www.chromium.org/developers/design-documents/sandbox#TOC-Process-mitigation-policies-Win32k-lockdown-)
on Windows PPAPI processes (PDFium and Adobe Flash). This makes it even tougher
for attackers to get out of the Chromium Flash sandbox, and can be enabled on
Windows 8 and above on Canary channel right now by toggling the settings in
chrome://flags/#enable-ppapi-win32k-lockdown.

We’ve been making steady progress on [Site
Isolation](https://www.chromium.org/developers/design-documents/site-isolation),
and are preparing to enable out-of-process iframes (OOPIFs) for web pages inside
extension processes. You can test this mode before it launches with
--isolate-extensions. We have [performance
bots](https://chromeperf.appspot.com/report?sid=74ebe5d13c09e34915152f51ee178f77421b1703d2bf26a7a586375f2c5e2cc7)
and UMA stats lined up, and we'll start with some early trials on Canary and Dev
channel. Meanwhile, we've added support for hit testing in the browser process,
scrolling, context menus, and script calls between all reachable frames (even
with changes to window.opener).

Not all security problems can be solved in[ Chrome’s
guts](https://dev.chromium.org/Home/chromium-security/guts), so[ we work on
making security more
user-friendly](https://dev.chromium.org/Home/chromium-security/enamel) too. To
support developers migrating to HTTPS, starting with M46, Chrome is marking the
“[HTTPS with Minor
Errors](https://googleonlinesecurity.blogspot.com/2015/10/simplifying-page-security-icon-in-chrome.html)”
state using the same neutral page icon as HTTP pages (instead of showing the
yellow lock icon). We’ve started analyzing invalid (anonymized!) TLS certificate
reports gathered from the field, to understand the root causes of unnecessary
TLS/SSL warnings. One of the first causes we identified and fixed was
certificate hostname mismatches due to a missing ‘www’. We also launched
[HPKP](https://tools.ietf.org/html/rfc7469) violation reporting in Chrome,
helping developers detect misconfigurations and attacks by sending a report when
a pin is violated. Finally, in an effort to support the Chrome experience across
languages and locales, we made strides in improving how the omnibox is displayed
in [RTL languages](https://en.wikipedia.org/wiki/Right-to-left).

Beyond the browser, our [web platform
efforts](https://dev.chromium.org/Home/chromium-security/owp) foster
cross-vendor cooperation on developer-facing security features. We shipped
[Subresource Integrity](https://w3c.github.io/webappsec-subresource-integrity/)
(SRI), which defends against resource substitution attacks by allowing
developers to specify a hash against which a script or stylesheet is matched
before it's executed. We’re excited to see large sites, like
[Github](http://githubengineering.com/subresource-integrity/), already deploying
SRI! We've sketched out a concept for a [Clear Site
Data](http://w3c.github.io/webappsec-clear-site-data/) feature which we hope
will make it possible for sites to reset their storage, and we're hard at work
on the next iteration of [Content Security
Policy](https://w3c.github.io/webappsec-csp/). Both of these will hopefully
start seeing some implementation in Q4.

We see migration to HTTPS as foundational to any security whatsoever
([and](https://tools.ietf.org/html/rfc7258)
[we're](https://www.iab.org/2014/11/14/iab-statement-on-internet-confidentiality/)
[not](https://www.iab.net/iablog/2015/03/adopting-encryption-the-need-for-https.html)
[the](https://w3ctag.github.io/web-https/) [only](https://https.cio.gov/)
[ones](https://blog.mozilla.org/security/2015/04/30/deprecating-non-secure-http/)),
so we're actively working to drive #MOARTLS across Google and the Internet at
large. We shipped [Upgrade Insecure
Requests](http://www.w3.org/TR/upgrade-insecure-requests/), which eases the
transition to HTTPS by transparently correcting a page's spelling from
\`http://\` to \`https://\` for all resources before any requests are triggered.
We've also continued our effort to [deprecate powerful features on insecure
origins](https://www.chromium.org/Home/chromium-security/deprecating-powerful-features-on-insecure-origins)
by solidifying the definition of a "[Secure
Context](https://w3c.github.io/webappsec-secure-contexts/)", and applying that
definition to block insecure usage of
[getUserMedia()](https://groups.google.com/a/chromium.org/d/msg/blink-dev/Dsoq5xPdzyw/21znuLWVCgAJ).

For more thrilling security updates and feisty rants, subscribe to
[security-dev@chromium.org](mailto:security-dev@chromium.org).

Happy Hacking,

Parisa, on behalf of Chrome Security

## Q2 2015

Hello from the Chrome Security Team!

For those that don’t know us already, we do stuff to help make Chrome the[ most
secure platform to browse the
Internet](https://www.chromium.org/Home/chromium-security). Here’s a recap of
some work from last quarter:

The[ Bugs--](https://dev.chromium.org/Home/chromium-security/bugs) effort aims
to find (and exterminate) security bugs. At the start of the quarter, we
initiated a [Security
FixIt](https://docs.google.com/spreadsheets/d/1za0aWxDx4ga3dKBwrlS8COci6_VXLBaiVg3x5Y6rrFg/edit#gid=0)
to trim back the fat backlog of open issues. With the help of dozens of
engineers across Chrome, we fixed over 40 Medium+ severity security bugs in 2
weeks and brought the count of issues down to 15! We also collaborated with
Android Security Attacks Team and added native platform fuzzing support to
[ClusterFuzz](https://www.chromium.org/Home/chromium-security/bugs/using-clusterfuzz)
(and
[imported](https://cs.corp.google.com/#android/vendor/google_experimental/users/natashenka/&q=natashenka)
[their](https://cs.corp.google.com/#android/vendor/google/tools/security/ServiceProbe/src/com/google/wireless/android/security/serviceprobe/ServiceFuzzer.java&l=22)
[fuzzers](https://cs.corp.google.com/#android/vendor/google_experimental/users/jlarimer/lhf/README.TXT&l=1)),
which resulted in ~30 new bugs discovered. ClusterFuzz now supports fuzzing on
all devices of the Nexus family (5,6,7,9) and Android One and is running on a
few dozen devices in the Android Lab. On top of this, we have doubled our
fuzzing capacity in [Compute Engine](https://cloud.google.com/compute/) to ~8000
cores by leveraging [Preemptible
VMs](http://googlecloudplatform.blogspot.com/2015/05/Introducing-Preemptible-VMs-a-new-class-of-compute-available-at-70-off-standard-pricing.html).
Lastly, we have upgraded all of our sanitizer builds on Linux (ASan, MSan, TSan
and UBSan) to report [edge-level
coverage](http://clang.llvm.org/docs/SanitizerCoverage.html) data, which is now
aggregated in the ClusterFuzz
[dashboard](https://cluster-fuzz.appspot.com/#coverage). We’re using this
coverage information to expand data bundles by existing fuzzers and improve our
corpus distillation.

Bugs still happen, so our[
Guts](https://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense. Our [Site
Isolation](https://www.chromium.org/developers/design-documents/site-isolation)
project is getting closer to its first stage of launch: using out-of-process
iframes (OOPIFs) for web pages inside extension processes. We've made
substantial progress (with lots of help from others on the Chrome team!) on core
Chrome features when using --site-per-process: OOPIFs now work with
back/forward, DevTools, and extensions, and they use
[Surfaces](https://www.chromium.org/developers/design-documents/chromium-graphics/surfaces)
for efficient painting (and soon input event hit-testing). We've collected some
preliminary performance data using
[Telemetry](https://www.chromium.org/developers/telemetry), we've fixed lots of
crashes, and we've started enforcing cross-site security restrictions on cookies
and passwords. Much work remains, but we're looking forward to turning on these
protections for real users!

On Linux and Chrome OS, we’ve made changes to restrict [one PID namespace per
renderer process](https://crbug.com/460972), which strengthens and cleans-up our
sandbox (shipping in Chrome 45). We also finished up a [major cleanup necessary
toward deprecating the setuid sandbox](https://crbug.com/312380), which should
be happening soon. Work continued to prepare for the launch of Windows 10, which
offers some opportunities for new security
[mitigations](https://www.chromium.org/developers/design-documents/sandbox#TOC-Process-mitigation-policies-Win32k-lockdown-);
the new version looks like the most secure Windows yet, so be sure to upgrade
when it comes out!

Not all security problems can be solved in[ Chrome’s
guts](https://dev.chromium.org/Home/chromium-security/guts), so[ we work on
making security more
user-friendly](https://dev.chromium.org/Home/chromium-security/enamel) too.
We’ve continued our efforts to avoid showing unnecessary TLS/SSL warnings:
decisions are now remembered for a week instead of a session, and a [new
checkbox on TLS/SSL warnings allows users to send us invalid certificate chains
](https://www.google.com/chrome/browser/privacy/whitepaper.html#ssl)that help us
root out false-positive warnings. Since developers and power users have been
asking for more tools to debug TLS/SSL issues, we’ve started building more
security information into DevTools and plan to launch a first version in Q3!

Another large focus for the team has been improving how users are asked for
permissions, like camera and geolocation. We’ve finalized a redesign of the
fullscreen permission flow that we hope to launch by the end of the year, fixed
a number of bugs relating to permission prompts, and launched another round of
updates to PageInfo and Website Settings on Android.

Beyond the browser, our [web platform
efforts](https://dev.chromium.org/Home/chromium-security/owp) foster
cross-vendor cooperation on developer-facing security features. The W3C's[
WebAppSec](http://www.w3.org/2011/webappsec/) working group continues to be a
fairly productive venue for a number of important features: we've polished the
[Subresource
Integrity](https://w3c.github.io/webappsec/specs/subresourceintegrity/) spec and
shipped an implementation in Chrome 46, published first drafts of [Credential
Management](https://w3c.github.io/webappsec/specs/credentialmanagement/) and
[Entry Point Regulation](https://w3c.github.io/webappsec/specs/epr/), continue
to push [Content Security Policy Level 2](http://www.w3.org/TR/CSP2/) and [Mixed
Content](http://www.w3.org/TR/mixed-content/) towards "Recommendation" status,
and fixed some [longstanding](https://crbug.com/483458)
[bugs](https://crbug.com/508310) with our [Referrer
Policy](http://www.w3.org/TR/referrer-policy/) implementation.

Elsewhere, we've started prototyping [Per-Page
Suborigins](https://code.google.com/p/chromium/issues/detail?id=336894) with the
intent of bringing a concrete proposal to WebAppSec, published a new draft of
[First-Party-Only
cookies](https://tools.ietf.org/html/draft-west-first-party-cookies-03) (and are
working through some [infrastructure
improvements](https://docs.google.com/document/d/19NACt9PXOUTJi60klT2ZGcFlgHM5wM1Owtcw2GQOKPI/edit)
so we can ship them), and [poked at sandboxed
iframes](https://wiki.whatwg.org/index.php?title=Iframe_sandbox_improvments) to
make it possible to sandbox ads.

We see migration to HTTPS as foundational to any security whatsoever
([and](https://tools.ietf.org/html/rfc7258)
[we're](https://www.iab.org/2014/11/14/iab-statement-on-internet-confidentiality/)
[not](https://www.iab.net/iablog/2015/03/adopting-encryption-the-need-for-https.html)
[the](https://w3ctag.github.io/web-https/) [only](https://https.cio.gov/)
[ones](https://blog.mozilla.org/security/2015/04/30/deprecating-non-secure-http/)),
so we're actively working to drive #MOARTLS across Google and the Internet at
large. As a small practical step on top of the [HTTPS webmasters fundamentals
section](https://developers.google.com/web/fundamentals/discovery-and-distribution/security-with-https/?hl=en),
we’ve added some functionality to [Webmaster
Tools](https://www.google.com/webmasters/tools/home?hl=en&pli=1) to provide
better assistance to webmasters when dealing with common errors in managing a
site over TLS (launching soon!). Also, we're now measuring the usage of
[pre-existing, powerful features on non-secure
origins](https://www.chromium.org/Home/chromium-security/deprecating-powerful-features-on-insecure-origins),
and are now printing deprecation warnings in the JavaScript console. Our
ultimate goal is to make all powerful features, such as Geolocation and
getUserMedia, available only to secure origins.

For more thrilling security updates and feisty rants, subscribe to
[security-dev@chromium.org](mailto:security-dev@chromium.org).

Happy Hacking,

Parisa, on behalf of Chrome Security

## Q1 2015

Hello from the Chrome Security Team!

For those that don’t know us already, we do stuff to help make Chrome the[ most
secure platform to browse the
Internet](https://www.chromium.org/Home/chromium-security). Here’s a recap of
some work from last quarter:

The[ Bugs--](https://dev.chromium.org/Home/chromium-security/bugs) effort aims
to find (and exterminate) security bugs. Last quarter, we rewrote our IPC
fuzzer, which resulted in
[lots](https://code.google.com/p/chromium/issues/list?can=1&q=linux_asan_chrome_ipc+type%3Dbug-security+-status%3AWontFix%2CDuplicate+&sort=-id+-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
[more](https://code.google.com/p/chromium/issues/list?can=1&q=linux_asan_chrome_ipc_32bit+type%3Dbug-security+-status%3AWontFix%2CDuplicate&sort=-id+-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
[bugs](https://code.google.com/p/chromium/issues/list?can=1&q=linux_msan_chrome_ipc+type%3Dbug-security+-status%3AWontFix%2CDuplicate&sort=-id+-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
discovered by
[ClusterFuzz](https://www.chromium.org/Home/chromium-security/bugs/using-clusterfuzz)!
We also expanded fuzzing platform support (Android Lollipop, Linux with Nvidia
GPU), added [archived
builds](http://storage.cloud.google.com/chrome-test-builds/media) for
[proprietary media codecs](http://www.chromium.org/audio-video) testing on all
platforms, and used more code annotations to find bugs (like
[this](https://code.google.com/p/chromium/issues/detail?id=468519&can=1&q=%22Container-overflow%20in%20%22%20type%3Dbug-security%20-status%3AWontFix%2CDuplicate&sort=-id%20-security_severity%20-secseverity%20-owner%20-modified&colspec=ID%20Pri%20Status%20Summary%20Modified%20OS%20M%20Security_severity%20Security_impact%20Owner%20Reporter)
or
[this](https://code.google.com/p/chromium/issues/detail?id=468406&can=1&q=%22Container-overflow%20in%20%22%20type%3Dbug-security%20-status%3AWontFix%2CDuplicate&sort=-id%20-security_severity%20-secseverity%20-owner%20-modified&colspec=ID%20Pri%20Status%20Summary%20Modified%20OS%20M%20Security_severity%20Security_impact%20Owner%20Reporter)).
We auto-add previous crash tests to our data corpus, which helps to catch
regressions even if a developer forgets to add a test
([example](https://code.google.com/p/chromium/issues/detail?id=478745)). We’ve
also started experimenting with enabling and leveraging code coverage
information from fuzzing. Contrary to what [some reports may
imply](http://secunia.com/resources/vulnerability-review/browser-security/), [we
don’t think vulnerability counting is a good standalone metric for
security](http://lcamtuf.blogspot.com/2010/05/vulnerability-databases-and-pie-charts.html),
and more bugs discovered internally
([653](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+label%3AClusterFuzz+opened-after%3A2014%2F1%2F1+opened-before%3A2015%2F1%2F1+-status%3AWontFix%2CDuplicate&sort=-id+-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
bugs in 2014 vs.
[380](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+label%3AClusterFuzz+opened-after%3A2013%2F1%2F1+opened-before%3A2014%2F1%2F1+-status%3AWontFix%2CDuplicate&sort=-id+-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
bugs in 2013), means more bugs fixed, means safer software! Outside of
engineering, inferno@ gave a talk at [nullcon](http://nullcon.net/) about Chrome
fuzzing
([slides](http://nullcon.net/website/archives/ppt/goa-15/analyzing-chrome-crash-reports-at-scale-by-abhishek-arya.pdf))
and we launched [never-ending
Pwnium](http://blog.chromium.org/2015/02/pwnium-v-never-ending-pwnium.html) with
a rewards pool up to $∞ million!

Bugs still happen, so our[
Guts](https://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense. On Linux and Chrome OS, we did some work to improve
the seccomp-BPF compiler and infrastructure. On modern kernels, we [finally
completed the switch](https://crbug.com/312380) from the [setuid sandbox to a
new design using
](https://code.google.com/p/chromium/wiki/LinuxSandboxing#The_setuid_sandbox)[unprivileged
namespaces](https://code.google.com/p/chromium/wiki/LinuxSandboxing#User_namespaces_sandbox)[.
](https://code.google.com/p/chromium/wiki/LinuxSandboxing#The_setuid_sandbox)We’re
also working on a generic, re-usable sandbox API on Linux, which we hope can be
useful to other Linux projects that want to employ sandboxing. On Android, we’ve
been experimenting with single-threaded renderer execution, which can yield
performance and security benefits for Chrome. We’ve also been involved with the
ambitious [Mojo](https://www.chromium.org/developers/design-documents/mojo)
effort. On OSX, we [shipped
crashpad](https://groups.google.com/a/chromium.org/forum/#!topic/chromium-dev/6eouc7q2j_g)
(which was a necessary project to investigate those sometimes-security-relevant
crashes!). Finally, on Windows, the support to block
[Win32k](https://www.chromium.org/developers/design-documents/sandbox#TOC-Process-mitigation-policies-Win32k-lockdown-)
system calls from renderers on Windows 8 and above is now enabled on Stable -
and renderers on these systems are also running within [App
Containers](https://www.chromium.org/developers/design-documents/sandbox#TOC-App-Container-low-box-token-)
on Chrome Beta, which blocks their access to the network. We also ensured all
Chrome allocations are safe - and use less memory (!) - by moving to the Windows
heap.

On our [Site
Isolation](https://www.chromium.org/developers/design-documents/site-isolation)
project, we’ve made progress on the underlying architecture so that complex
pages are correct and stable (e.g. rendering any combination of iframes,
evaluating renderer-side security checks, sending postMessage between subframes,
keeping script references alive). Great progress has also been made on session
history, DevTools, and test/performance infrastructure, and other teams have
started updating their features for out-of-process iframes after our [Site
Isolation
Summit](https://www.youtube.com/playlist?list=PL9ioqAuyl6UJmC0hyI-k1wYW08O71lBn8).

Not all security problems can be solved in[ Chrome’s
guts](https://dev.chromium.org/Home/chromium-security/guts), so[ we work on
making security more
user-friendly](https://dev.chromium.org/Home/chromium-security/enamel) too. In
an effort to determine the causes of SSL errors, we’ve added a new checkbox on
SSL warnings that allows users to send us invalid certificate chains for
analysis. We’ve started looking at the data, and in the coming months we plan to
introduce new warnings that provide specific troubleshooting steps for common
causes of spurious warnings. We also recently launched the new permissions
bubble UI, which solves some of the problems we had with permissions infobars
(like better coalescing of multiple permission requests). And for our Android
users, we recently revamped PageInfo and Site Settings, making it easier than
ever for people to manage their permissions. Desktop updates to PageInfo and
Site Settings are in progress, too. Finally, we just launched a new extension,
[Chrome User Experience
Surveys](https://chrome.google.com/webstore/detail/chrome-user-experience-su/hgimloonaobbeeagepickockgdcghfnn?utm_source=newsletter&utm_medium=security&utm_campaign=CUES%20extension),
which asks people for in-the-moment feedback after they use certain Chrome
features. If you’re interested in helping improve Chrome, you should try it out!

Beyond the browser, our [web platform
efforts](https://dev.chromium.org/Home/chromium-security/owp) foster
cross-vendor cooperation on developer-facing security features. We're working
hard with the good folks in the W3C's
[WebAppSec](http://www.w3.org/2011/webappsec/) working group to make progress on
a number of specifications: [CSP 2](http://www.w3.org/TR/CSP2/) and [Mixed
Content](http://www.w3.org/TR/mixed-content/) have been published as Candidate
Recommendations, [Subresource Integrity](http://www.w3.org/TR/SRI/) is
implemented behind a flag and the spec is coming together nicely, and we've
fixed a number of [Referrer Policy](http://www.w3.org/TR/referrer-policy/)
issues. [First-Party-Only
Cookies](https://tools.ietf.org/html/draft-west-first-party-cookies) are just
about ready to go, and [Origin
Cookies](https://tools.ietf.org/html/draft-west-origin-cookies) are on deck.

We see migration to HTTPS as foundational to any security whatsoever
([and](https://tools.ietf.org/html/rfc7258)
[we're](https://www.iab.org/2014/11/14/iab-statement-on-internet-confidentiality/)
[not](https://www.iab.net/iablog/2015/03/adopting-encryption-the-need-for-https.html)
[the](https://w3ctag.github.io/web-https/) [only](https://https.cio.gov/)
[ones](https://blog.mozilla.org/security/2015/04/30/deprecating-non-secure-http/)),
so we're actively working to [define the properties of secure
contexts](https://w3c.github.io/webappsec/specs/powerfulfeatures/), [deprecate
powerful features on insecure
origins](deprecating-powerful-features-on-insecure-origins.md), and to make it
simpler for developers to [Upgrade Insecure
Requests](https://w3c.github.io/webappsec/specs/upgrade/) on existing sites.

For more thrilling security updates and feisty rants, subscribe to
[security-dev@chromium.org](mailto:security-dev@chromium.org).

Happy Hacking,

Parisa, on behalf of Chrome Security

P.S. Go here to travel back in time and view previous [Chrome security quarterly
updates](http://dev.chromium.org/Home/chromium-security/quarterly-updates).

## Q4 2014

Hello from the Chrome Security Team!

For those that don’t know us already, we do stuff to help make Chrome the[ most
secure platform to browse the
Internet](http://www.chromium.org/Home/chromium-security). Here’s a recap of
some work from last quarter:

The[ Bugs--](http://dev.chromium.org/Home/chromium-security/bugs) effort aims to
find (and exterminate) security bugs. Last quarter, we incorporated more
[coverage data](https://code.google.com/p/address-sanitizer/wiki/AsanCoverage)
into our [ClusterFuzz dashboard](https://cluster-fuzz.appspot.com/#coverage),
especially for
[Android](https://cluster-fuzz.appspot.com/coverage?platform=ANDROID). With
this, we hope to optimize our test cases and improve fuzzing efficiency. We also
incorporated 5 new fuzzers from the external research community as part of the
fuzzer reward program. This has resulted in
[33](https://code.google.com/p/chromium/issues/list?can=1&q=decoder_langfuzz+Type%3DBug-Security+-status%3ADuplicate&colspec=ID+Pri+M+Week+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)
[new](https://code.google.com/p/chromium/issues/list?can=1&q=therealholden_worker+Type%3DBug-Security+-status%3ADuplicate&colspec=ID+Pri+M+Week+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)
[security](https://code.google.com/p/chromium/issues/list?can=1&q=cdiehl_peach+Type%3DBug-Security+-status%3ADuplicate&colspec=ID+Pri+M+Week+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)
vulnerabilities. Finally, we wrote a multi-threaded test case minimizer from
scratch based on [delta debugging](https://www.st.cs.uni-saarland.de/dd/) (a
long-standing request from blink devs!) which produces clean, small,
reproducible test cases. In reward program news, we've paid over $1.6 million
for externally reported Chrome bugs since 2010 ([$4 million total across
Google](http://googleonlinesecurity.blogspot.com/2015/01/security-reward-programs-year-in-review.html)).
In 2014, over 50% of reward program bugs were found and fixed before they hit
the stable channel, protecting our main user population. Oh, and in case you
didn’t notice, the [rewards we’re paying out for vulnerabilities went
up](http://googleonlinesecurity.blogspot.com/2014/09/fewer-bugs-mo-money.html)
again.

Bugs still happen, so our[
Guts](http://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense. We’re most excited about progress toward a tighter
sandbox for Chrome on Android (via
[seccomp-bpf](https://www.kernel.org/doc/Documentation/prctl/seccomp_filter.txt)),
which required landing seccomp-bpf support in Android and [enabling TSYNC on all
Chrome OS](https://codereview.chromium.org/759343002/) and Nexus kernels. We’ve
continued to improve our Linux / Chrome OS sandboxing by (1) adding full
cross-process interaction restrictions at the BPF sandbox level, (2) making API
improvements and some code refactoring of //sandbox/linux, and (3) implementing
a more powerful policy system for the GPU sandbox.

After ~2 years of work on [Site
Isolation](http://dev.chromium.org/developers/design-documents/site-isolation),
we’re happy to announce that [out-of-process
iframes](http://dev.chromium.org/developers/design-documents/oop-iframes) are
working well enough that some Chrome features have [started updating to support
them](https://docs.google.com/document/d/1dCR2aEoBJj_Yqcs6GuM7lUPr0gag77L5OSgDa8bebsI/edit?usp=sharing)!
These include autofill (done), accessibility (nearly done), <webview>
(prototyping), devtools, and extensions. We know how complex a rollout this will
be, and we’re ready with testing infrastructure and
[FYI](http://build.chromium.org/p/chromium.fyi/builders/Site%20Isolation%20Linux)
[bots](http://build.chromium.org/p/chromium.fyi/builders/Site%20Isolation%20Win).
As we announced at our recent Site Isolation Summit
([video](https://www.youtube.com/playlist?list=PL9ioqAuyl6UJmC0hyI-k1wYW08O71lBn8),
[slides](https://docs.google.com/presentation/d/10HTTK4dsxO5p6FcpEOq8EkuV4yiBx2n6dBki8cqDWyo/edit?usp=sharing)),
our goal for Q1 is to finish up OOPIF support with the help of all of Chrome.

Not all security problems can be solved in [Chrome’s
Guts](http://dev.chromium.org/Home/chromium-security/guts), so[ we work on
making security more
user-friendly](http://dev.chromium.org/Home/chromium-security/enamel) too. For
the past few months, we’ve been looking deeper into the causes of SSL errors by
looking at UMA stats and monitoring user help forums. One source of SSL errors
is system clocks with the wrong time, so we landed a more informative error
message in Chrome 40 to let users know they need to fix their clock. We’ve also
started working on a warning interstitial for [captive
portals](http://en.wikipedia.org/wiki/Captive_portal) to distinguish those SSL
errors from the rest. Finally, we [proposed a plan for browsers to migrate their
user interface from marking insecure origins (i.e. HTTP) as explicitly
insecure](http://dev.chromium.org/Home/chromium-security/marking-http-as-non-secure);
the [initial
discussion](https://groups.google.com/a/chromium.org/forum/#!topic/security-dev/DHQLv76QaEM%5B1-25-false%5D)
and [external attention](http://www.bbc.com/news/technology-30505970) has been
generally positive.

Over the past few years, we’ve worked on a bunch of isolated projects to push
security on the Open Web Platform forward and make it possible for developers to
write more secure apps. We recognized we can move faster if we get some of the
team fully dedicated to this work, so we formed a new group that will focus on
[web platform efforts](http://dev.chromium.org/Home/chromium-security/owp).

As usual, for more thrilling security updates and feisty rants, subscribe to
[security-dev@chromium.org](mailto:security-dev@chromium.org).

To a safer web in 2015!

Parisa, on behalf of Chrome Security

## Q3 2014

Hello from the Chrome Security Team!

For those that don’t know us already, we do stuff to help make Chrome the[ most
secure platform to browse the
Internet](http://www.chromium.org/Home/chromium-security). Here’s a recap of
some work from last quarter:

The[ Bugs--](http://dev.chromium.org/Home/chromium-security/bugs) effort aims to
find (and exterminate) security bugs. We increased Clusterfuzz cores across all
desktop platforms (Mac, Android, Windows, and Linux), resulting in
[155](https://code.google.com/p/chromium/issues/list?can=1&q=Type%3DBug-Security+label%3AClusterFuzz+opened-after%3A2014%2F7%2F1+opened-before%3A2014%2F9%2F31+-status%3AWontFix%2CDuplicate&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
security and
[275](https://code.google.com/p/chromium/issues/list?can=1&q=Type%3DBug+label%3AClusterFuzz+opened-after%3A2014%2F7%2F1+opened-before%3A2014%2F9%2F31+-status%3AWontFix%2CDuplicate&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
functional bugs since last update! We also started fuzzing D-Bus system services
on Chrome OS, which is our first attempt at leveraging Clusterfuzz for the
operating system. One of the common security pitfalls in C++ is bad casting
(often rooted in aggressive polymorphism). To address, [one of our
interns](http://www.cc.gatech.edu/~blee303/) tweaked [UBSAN (Undefined Behavior
Sanitizer)
vptr](https://drive.google.com/file/d/0Bxvv8gduedamTEJCUlN6eERtWUE/view?usp=sharing)
to detect bad-casting at runtime, which resulted in [11 new security
bugs](https://code.google.com/p/chromium/issues/list?can=1&q=Type%3DBug-Security+linux_ubsan_vptr_chrome+-status%3ADuplicate%2CWontFix&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles&)!
We’ve continued to collaborate with external researchers on new fuzzing
techniques to find bugs in V8, Pdfium, Web Workers, IDB, and more. Shout out to
attekett, cloudfuzzer, decoder.oh, and therealholden for their attention and
bugs over the past quarter!

Finding bugs is only half the battle, so we also did a few things to make it
easier to get security bugs fixed, including (1) a new [security
sheriff](http://dev.chromium.org/Home/chromium-security/security-sheriff)
[dashboard](https://cluster-fuzz.appspot.com/#sheriff) and (2) contributing to
the [FindIt
project](https://chromium.googlesource.com/chromium/src.git/+/lkgr/tools/findit/),
which helps narrow down suspected CL(s) for a crash (given a regression range
and stacktrace), thereby saving manual triage cycles.

Bugs still happen, so our[
Guts](http://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense. We did a number of things to push
[seccomp-bpf](http://blog.chromium.org/2012/11/a-safer-playground-for-your-linux-and.html)
onto more platforms and architectures, including: (1) adding support for
[MIPS](https://code.google.com/p/chromium/issues/detail?id=369594) and
[ARM64](https://code.google.com/p/chromium/issues/detail?id=355125), (2) adding
a [new capability](https://lkml.org/lkml/2014/7/17/844) to initialize
seccomp-bpf in the presence of threads (bringing us a big step closer to a
stronger sandbox on Android), (3) [general tightening of the
sandboxes](https://crbug.com/413855), and (4) writing a [domain-specific
language to better express BPF
policies](https://drive.google.com/file/d/0B9LSc_-kpOQPVHhvcVBza3NWR0k/view). We
also helped ensure a [safe launch of Android apps on Chrome
OS](http://chrome.blogspot.com/2014/09/first-set-of-android-apps-coming-to.html),
and continued sandboxing new system services.

On Windows, we [launched Win64 to
Stable](http://blog.chromium.org/2014/08/64-bits-of-awesome-64-bit-windows_26.html),
giving users a safer, speedier, and more stable version of Chrome! On Windows 8,
we added Win32k system call filtering behind a
[switch](https://code.google.com/p/chromium/codesearch#chromium/src/content/public/common/content_switches.cc&sq=package:chromium&l=966),
further reducing the kernel attack surface accessible from the renderer. We also
locked down the [alternate
desktop](http://www.chromium.org/developers/design-documents/sandbox#TOC-The-alternate-desktop)
sandbox tokens and refactored the sandbox startup to cache tokens, which
improves new tab responsiveness.

Finally, work continues on [site
isolation](http://www.chromium.org/developers/design-documents/site-isolation).
Over the past few months, we’ve started creating RemoteFrames in Blink's frame
tree to support out-of-process iframes (OOPIF) and got
[Linux](http://build.chromium.org/p/chromium.fyi/builders/Site%20Isolation%20Linux)
and
[Windows](http://build.chromium.org/p/chromium.fyi/builders/Site%20Isolation%20Win)
FYI bots running tests with --site-per-process. We’ve also been working with the
[Accessibility](https://groups.google.com/a/chromium.org/forum/#!forum/chromium-accessibility)
team as our guinea pig feature to support OOPIF, and since that work is nearly
done, we’re reaching out to more teams over the next few months to update their
features (see our [FAQ about updating
features](https://docs.google.com/a/chromium.org/document/d/1Iqe_CzFVA6hyxe7h2bUKusxsjB6frXfdAYLerM3JjPo/edit)).

Not all security problems can be solved in[ Chrome’s
guts](http://dev.chromium.org/Home/chromium-security/guts), so[ we work on
making security more
user-friendly](http://dev.chromium.org/Home/chromium-security/enamel) too.
SSL-related warnings are still a major source of user pain and confusion. Over
the past few months, we’ve been focused on determining the causes of false
positive SSL errors (via adding UMA stats for known client / server errors) and
investigating
[pinning](http://dev.chromium.org/Home/chromium-security/education/tls#TOC-Certificate-Pinning)
violation reports. We’ve also been [experimenting with cert memory
strategies](https://codereview.chromium.org/369703002/) and integrating relevant
detail when we detect a (likely) benign SSL error due to captive portal or a bad
clock.

Developers are users too, so we know it’s important to support new web security
features and ensure new features are safe to use by default. In that vein, we
recently landed a first pass at [subresource
integrity](https://w3c.github.io/webappsec/specs/subresourceintegrity/)
[support](https://codereview.chromium.org/566083003/) behind a flag (with
[useful console errors](https://codereview.chromium.org/596043003/)), we’re
[shipping](https://groups.google.com/a/chromium.org/d/msg/blink-dev/wToP6b04zVE/imuPatGy3awJ)
most of [CSP 2](https://w3c.github.io/webappsec/specs/content-security-policy/)
in M40, we’ve continued to [tighten
up](https://groups.google.com/a/chromium.org/d/msg/blink-dev/Uxzvrqb6IeU/9FAie9Py4cIJ)
handling of [mixed
content](https://w3c.github.io/webappsec/specs/mixedcontent/), and are working
to define and implement [referrer
policies](https://w3c.github.io/webappsec/specs/referrer-policy/). We’ve also
been helping on some security consulting for [Service
Worker](http://dev.chromium.org/blink/serviceworker); kudos to the team for
making changes to [handle plugins more
securely](https://code.google.com/p/chromium/issues/detail?id=413094), [restrict
usage to secure origins](http://crbug.com/394213), and for addressing some
memory caching issues. If you want to learn more about what’s going on in the
Blink Security world, check out the
[Blink-SecurityFeature](https://code.google.com/p/chromium/issues/list?q=label:Cr-Blink-SecurityFeature)
label.

And then there’s other random things, like ad-hoc hunting for security bugs
(e.g. [local privilege escalation bug in
pppd](https://code.google.com/p/chromium/issues/detail?id=390709)), [giving
Chromebooks](http://www.washingtonpost.com/blogs/the-switch/wp/2014/08/18/finding-a-safe-space-for-kids-to-hack/)
to [kids at
DEFCON](https://www.flickr.com/photos/asirap/sets/72157645916802437), and
various artistic endeavors, like [color-by-risk
diagramming](https://docs.google.com/a/chromium.org/drawings/d/1TuECFL9K7J5q5UePJLC-YH3satvb1RrjLRH-tW_VKeE/edit)
and [security-inspired
fashion](https://www.flickr.com/photos/asirap/14798014040/).

For more thrilling security updates and feisty rants, subscribe to
[security-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/security-dev).

Happy Hacking (and Halloween),

Parisa, on behalf of Chrome Security

## Q2 2014

Hello from the Chromium Security Team!

For those that don’t know us already, we do stuff to help make Chrome the[ most
secure platform to browse the
Internet](http://www.chromium.org/Home/chromium-security). Here’s a recap of
some work from last quarter:

One of our primary responsibilities is security **adviser**, and the main way we
do this is via security reviews. A few weeks ago, jschuh@ announced [a new and
improved security review
process](http://www.chromium.org/Home/chromium-security/security-reviews) that
helps teams better assess their current security posture and helps our team
collect more meaningful data about Chrome engineering. All features for M37 went
through the new process, and we’ll be shepherding new projects and launches
through this process going forward.

The[ Bugs--](http://dev.chromium.org/Home/chromium-security/bugs) effort aims to
find (and exterminate) security bugs. One of our best ways of finding bugs and
getting them fixed quickly is fuzz testing via
[ClusterFuzz](https://cluster-fuzz.appspot.com/). This quarter, we started
fuzzing Chrome on Mac OS (extending the existing platform coverage on Windows,
Linux, and Android). We also added [code coverage
stats](https://cluster-fuzz.appspot.com/#coverage) to the ClusterFuzz UI, which
some teams have been finding helpful as a complement to their QA testing, as
well as [fuzzer stats](https://cluster-fuzz.appspot.com/#fuzzerstats), which [V8
team now
checks](https://cluster-fuzz.appspot.com/fuzzerstats?fuzzer_name=stgao_chromebot2&job_type=linux_asan_chrome_v8&last_n_revisions=30)
in new rollouts. Finally, we added some new fuzzers (WebGL, GPU commands) and
integrated a number of memory debugging tools to find new classes of bugs (e.g.
[AddressSanitizer](https://code.google.com/p/address-sanitizer/) on Windows
found
[22](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+%22windows_asan_chrome%22&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
bugs, [Dr. Memory](http://www.drmemory.org/) on Windows found
[1](https://code.google.com/p/chromium/issues/list?can=2&q=Stability%3DMemory-DrMemory+label%3Aclusterfuzz&colspec=ID+Pri+M+Iteration+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)
bug,
[MemorySanitizer](https://code.google.com/p/memory-sanitizer/wiki/MemorySanitizer)
on Linux found
[146](https://code.google.com/p/chromium/issues/list?can=1&q=label%3AClusterFuzz+Stability%3DMemory-MemorySanitizer+&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
bugs, and
[LeakSanitizer](https://code.google.com/p/address-sanitizer/wiki/LeakSanitizer)
on Linux found
[18](https://code.google.com/p/chromium/issues/list?can=1&q=label%3AClusterFuzz+Stability%3DMemory-LeakSanitizer+&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
bugs).

Another source of security bugs is our [vulnerability reward
program](http://dev.chromium.org/Home/chromium-security/vulnerability-rewards-program),
which saw a quiet quarter: only 32 reports opened in Q2 (lowest participation in
12 months) and an average payout of $765 per bug (lowest value in 12 months).
This trend is likely due to (1) fuzzers, both internal and external, finding
over 50% of all reported bugs in Q2, (2) a reflection of both the increasing
difficulty of finding bugs and outdated reward amounts being less competitive,
and (3) researcher fatigue / lack of interest or stimulus. Plans for Q3 include
reinvigorating participation in the rewards program through a more generous
reward structure and coming up with clever ways to keep researchers engaged.

Outside of external bug reports, we spent quite a bit of time [improving the
security posture of
Pdfium](http://dev.chromium.org/Home/chromium-security/pdfium-security)
(Chrome's [recently opensourced](http://blog.foxitsoftware.com/?p=641) PDF
renderer) via finding / fixing
~[150](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+Cr%3DInternals-Plugins-PDF+closed-after%3A2014%2F4%2F1+closed-before%3A2014%2F7%2F31&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
bugs, removing risky code (e.g. [custom
allocator](https://pdfium.googlesource.com/pdfium/+/3522876d5291922ddc62bf1b70d02743b0850673)),
and using [secure integer
library](https://code.google.com/p/chromium/codesearch#chromium/src/base/numerics/&ct=rc&cd=1&q=numerics&sq=package:chromium)
for overflow checks. Thanks to ifratric@, mjurczyk@, and gynvael@ for their PDF
fuzzing help!

Bugs still happen, so our
[Guts](http://dev.chromium.org/Home/chromium-security/guts) effort builds in
multiple layers of defense. We did lots of sandboxing work across platforms last
quarter. On Mac OS, rsesek@ started working on a brand new [bootstrap sandbox
for
OSX](https://docs.google.com/a/chromium.org/document/d/108sr6gBxqdrnzVPsb_4_JbDyW1V4-DRQUC4R8YvM40M/edit)
(//sandbox/mac) and on Android, he got a proof-of-concept renderer running under
[seccomp-bpf](http://lwn.net/Articles/475043/). On Linux and Chrome OS, we
continued to improve the sandboxing testing framework and wrote dozens of new
tests; all our security tests are now running on the Chrome OS BVT. We also
refactored all of NaCl-related “outer” sandboxing to support a new and faster
Non-SFI mode for [NaCl](https://developer.chrome.com/native-client). This is
being used to run Android apps on Chrome, as you may have seen [demoed at Google
I/O](https://www.google.com/events/io).

After many months of hard work, we’re ecstatic to announce that we [released
Win64 on dev and
canary](http://blog.chromium.org/2014/06/try-out-new-64-bit-windows-canary-and.html)
to our Windows 7 and Windows 8 users. This release takes advantage of [High
Entropy
ASLR](http://blogs.technet.com/b/srd/archive/2013/12/11/software-defense-mitigating-common-exploitation-techniques.aspx)
on Windows 8, and the extra bits help improve the effectiveness of heap
partitioning and mitigate common exploitation techniques (e.g. JIT spraying).
The Win64 release also reduced ~⅓ of the crashes we were seeing on Windows, so
it’s more stable too!

Finally, work continues on [site
isolation](http://www.chromium.org/developers/design-documents/site-isolation):
lots of code written / rewritten / rearchitected and unknown unknowns discovered
along the way. We're close to having "remote" frames for each out-of-process
iframe, and you can now see subframe processes in Chrome's Task Manager when
visiting a test page like
[this](http://csreis.github.io/tests/cross-site-iframe.html) with the
--site-per-process flag.

Not all security problems can be solved in[ Chrome’s
guts](http://dev.chromium.org/Home/chromium-security/guts), so[ we work on
making security more
user-friendly](http://dev.chromium.org/Home/chromium-security/enamel) too. The
themes of Q2 were SSL and permissions. For SSL, we nailed down a new ["Prefer
Safe Origins for Powerful Features"
policy](http://dev.chromium.org/Home/chromium-security/prefer-secure-origins-for-powerful-new-features),
which we’ll transition to going forward; kudos to palmer@ and sleevi@ for
ironing out all the details and getting us to a safer default state. We’ve also
been trying to improve the experience of our SSL interstitial, which most people
ignore :-/ Work includes launching new UX for [SSL
warnings](https://test-sspev.verisign.com/) and incorporating captive portal
status (ongoing). Congrats to agl@ for launching
[boringssl](https://www.imperialviolet.org/2014/06/20/boringssl.html) - if
boring means avoiding Heartbleed-style hysteria, sounds good to us!

On the permissions front, we’re working on ways to give users more control over
application privileges, such as (1) reducing the number of install-time CRX
permissions, (2) running UX experiments on the effectiveness of permissions, and
(3) working on building a security and permissions model to bring native
capabilities to the web.

For more thrilling security updates and feisty rants, subscribe to
[security-dev@chromium.org](mailto:security-dev@chromium.org).

In the meantime, happy hacking!

Parisa, on behalf of Chrome Security

P.S. A big kudos to the V8 team, and jkummerow@ in particular, for their extra
security efforts this quarter! The team rapidly responded to and fixed a number
of security bugs on top of doing some security-inspired hardening of V8 runtime
functions.

## Q1 2014

Hello from the Chrome Security Team!

For those that don’t know us already, we help make Chrome the [most secure
platform to browse the
Internet](http://www.chromium.org/Home/chromium-security). In addition to
security reviews and consulting, running a [vulnerability reward
program](http://dev.chromium.org/Home/chromium-security/vulnerability-rewards-program),
and dealing with security surprises, we instigate and work on engineering
projects that make Chrome safer. Here’s a recap of some work from last quarter:

The [Bugs-- ](http://dev.chromium.org/Home/chromium-security/bugs)effort aims to
find (and exterminate) exploitable bugs. A major accomplishment from Q1 was
getting [ClusterFuzz](https://cluster-fuzz.appspot.com/) coverage for Chrome on
Android; we’re aiming to scale up resources from [a few devices on inferno@’s
desk](https://plus.sandbox.google.com/u/0/103970240746069356722/posts/LckbsWq6QFZ)
to 100 bots over the next few months. On the fuzzer front, mbarbella@ wrote a
new V8 fuzzer that helped shake out
[30+](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Dbug-security+-status%3Aduplicate+mbarbella_js_mutation&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles)
[bugs](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Dbug-security+-status%3Aduplicate+mbarbella_js_mutation_test&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&x=m&y=releaseblock&cells=tiles);
kudos to the V8 team for being so proactive at fixing these issues and
prioritizing additional proactive security work this quarter. Spring welcomed a
hot new line of PoC exploits at [Pwn2Own](http://www.pwn2own.com/) and [Pwnium
4](http://blog.chromium.org/2014/01/show-off-your-security-skills.html):
highlights included a classic ensemble of overly broad IPC paired with a Windows
“feature,” and a bold chain of 5 intricate bugs for persistent system compromise
on Chrome OS; more details posted soon
[here](http://dev.chromium.org/Home/chromium-security/bugs). Beyond exploit
contests, we’ve rewarded $52,000 for reports received this year (from 16
researchers for 23 security bugs) via our ongoing [vulnerability reward
program](http://dev.chromium.org/Home/chromium-security/vulnerability-rewards-program).
We also started rewarding researchers for bugs in [Chrome extensions developed
"by
Google.”](http://googleonlinesecurity.blogspot.com/2014/02/security-reward-programs-update.html)
Outside of finding and fixing bugs, jschuh@ landed [a safe numeric
class](https://code.google.com/p/chromium/issues/detail?id=332611) to help
prevent arithmetic overflow bugs from being introduced in the first place; use
it and you'll [sleep better](https://xkcd.com/571/) too!

Bugs still happen, so we build in multiple layers of defense. One of our most
common techniques is
[sandboxing](http://dev.chromium.org/Home/chromium-security/guts), which helps
to reduce the impact of any single bug. Simple in theory, but challenging to
implement, maintain, and improve across all platforms. On Linux and Chrome OS,
we spent a lot of the quarter paying back technical debt: cleaning up the GPU
sandbox, writing and fixing tests, and replacing the setuid sandbox. On Android,
we reached consensus with the Android Frameworks team on a path forward for
seccomp-bpf sandboxing for Clank. We've started writing the CTS tests to verify
this in Android, landed the baseline policy in upstream Clankium, and are
working on the required [upstream Linux Kernel
changes](https://lkml.org/lkml/2014/1/13/795) to be incorporated into Chrome
Linux, Chrome OS, and Android L. The [site isolation
project](http://www.chromium.org/developers/design-documents/site-isolation)
(i.e. sandboxing at the site level) landed a usable cross-process iframe
implementation behind --site-per-process, which supports user interaction,
nested iframes (one per doc), sad frame, and basic DevTools support. Major
refactoring of Chrome and Blink, performance testing, and working with teams
that need to update for site isolation continues this quarter. On Windows, we
shipped Win64 canaries, landed code to sandbox the auto update mechanism, and
improved the existing sandboxing, reducing the win32k attack surface by ~30%.
Thanks to the Windows Aura team, we’ve also made tremendous progress on
disabling win32k entirely in the Chrome sandbox, which will eventually eliminate
most Windows-specific sandbox escapes.

Not all security can be solved in [Chromium’s
Guts](http://dev.chromium.org/Home/chromium-security/guts), so [we work on
making security more
user-friendly](http://dev.chromium.org/Home/chromium-security/enamel) too. We
finally landed the controversial change to [remember passwords, even when
autocomplete='off'](https://code.google.com/p/chromium/issues/detail?id=177288)
in
[M34](http://blog.chromium.org/2014/02/chrome-34-responsive-images-and_9316.html),
which is a small, but [significant change to return control back to the
user](security-faq/index.md). We also made some [tweaks to the malware download
UX in
M32](https://docs.google.com/a/google.com/presentation/d/16ygiQS0_5b9A4NwHxpcd6sW3b_Up81_qXU-XY86JHc4/edit#slide=12);
previously users installed ~29% of downloads that were known malware, and that
number is now down to <5%! We’ve recently been thinking a lot about how to
improve the security of Chrome Extensions and Apps, including experimenting with
several changes to the permission dialog to see if we can reduce the amount of
malicious crx installed by users without reducing the amount of non-malicious
items. Separately, we want to make it easier for developers to write secure
APIs, so meacer@ wrote up some [security
tips](https://docs.google.com/document/d/1RamP4-HJ7GAJY3yv2ju2cK50K9GhOsydJN6KIO81das/pub)
to help developers avoid common abuse patterns we’ve identified from bad actors.

Finally, since [Heartbleed](http://heartbleed.com/) is still on the forefront of
many minds, a reminder that Chrome and Chrome OS [were not directly
affected](http://googleonlinesecurity.blogspot.com/2014/04/google-services-updated-to-address.html).
And if you're curious about how and why Chrome does SSL cert revocation the way
it does, agl@ wrote a great
[post](https://www.imperialviolet.org/2014/04/19/revchecking.html) explaining
that too.

For more thrilling security updates and feisty rants, subscribe to[
security-dev@chromium.org](mailto:security-dev@chromium.org).

Happy Hacking,
Parisa, on behalf of Chrome Security

## Q4 2013

Hello from the Chrome Security Team!
For those that don’t know us already, we help make Chromium the [most secure
browsing platform in the
market](http://www.chromium.org/Home/chromium-security). In addition to security
reviews and consulting, running a [vulnerability reward
program](http://dev.chromium.org/Home/chromium-security/vulnerability-rewards-program),
and dealing with security surprises, we instigate and work on engineering
projects that make Chrome more secure.
The end of last year flew by, but here are a couple of things we’re most proud
of from the last quarter of 2013:
Make security more usable: We made a number of changes to the malware download
warning to discourage users from installing malware. We also worked on a
reporting feature that lets users upload suspicious files to [Safe
Browsing](http://www.google.com/transparencyreport/safebrowsing/), which will
help Safe Browsing catch malicious downloads even faster.
Since PDFs are a common vehicle for exploit delivery, we’ve modified PDF
handling in Chrome so that they're all opened in Chrome’s PDF viewer by default.
This is a huge security win because we believe Chrome’s PDF viewer is the
safest, most hardened, and security-tested viewer available. [Malware via
Microsoft .docs are also
common](https://www.eff.org/deeplinks/2014/01/vietnamese-malware-gets-personal),
so we’re eagerly awaiting the day we can open Office Docs in
[Quickoffice](https://support.google.com/quickoffice/answer/2986862?hl=en) by
default.
Find (and fix) more security bugs: We recently welcomed a new member to the
team,
[Sheriffbot](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+commentby%3Aclusterfuzz%40chromium.org+-reporter%3Aclusterfuzz%40chromium.org&sort=-id+-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner+Reporter&cells=tiles).
He’s already started making the [mortal security
sheriffs’](http://dev.chromium.org/Home/chromium-security/security-sheriff)
lives easier by finding new owners, adding Cr- area labels, helping apply and
fix bug labels, and reminding people about open security bugs they have assigned
to them.
Our fuzzing mammoth, [ClusterFuzz](https://cluster-fuzz.appspot.com/), is now
fully supported on Windows and has helped find
[32](https://code.google.com/p/chromium/issues/list?can=1&q=type:bug-security%20label:Hotlist-SyzyASAN%20label:ClusterFuzz&sort=-id+-security_severity+-secseverity+-owner+-modified&colspec=ID%20Pri%20Status%20Summary%20Modified%20OS%20M%20Security_severity%20Security_impact%20Owner%20Reporter)
new bugs. We’ve added a bunch of new fuzzers to cover Chromium IPC (5 high
severity bugs), networking protocols (1 critical severity bug from a certificate
fuzzer, 1 medium severity bug from an HTTP protocol fuzzer), and WebGL (1 high
severity bug in Angle). Want to write a
[fuzzer](http://en.wikipedia.org/wiki/Fuzz_testing) to add security fuzzing
coverage to your code? Check out the [ClusterFuzz
documentation](http://dev.chromium.org/Home/chromium-security/bugs/using-clusterfuzz),
or get in touch.
In November, we helped sponsor a [Pwn2Own contest at the PacSec conference in
Tokyo](http://h30499.www3.hp.com/t5/HP-Security-Research-Blog/Mobile-Pwn2Own-2013/ba-p/6202185#.Ut6t45DTm91).
Our good friend, Pinkie Pie, exploited an integer overflow in V8 to get reliable
code execution in the renderer, and then exploited a bug in a Clipboard IPC
message to get code execution in the browser process (by spraying multiple
gigabytes of shared memory). We’ll be publishing a full write-up of the exploit
[on our site](http://dev.chromium.org/Home/chromium-security/bugs) soon, and are
starting to get excited about our [upcoming
Pwnium](http://blog.chromium.org/2014/01/show-off-your-security-skills.html) in
March.
Secure by default, defense in depth: In [Chrome
32](http://googlechromereleases.blogspot.com/2014/01/stable-channel-update.html),
we started [blocking NPAPI by
default](http://blog.chromium.org/2013/09/saying-goodbye-to-our-old-friend-npapi.html)
and have plans to completely remove support by the end of the year. This change
significantly reduces Chrome’s exposure to browser plugin vulnerabilities. We
also implemented additional [heap partitioning for buffers and
strings](https://code.google.com/p/chromium/issues/detail?id=270531) in Blink,
which further mitigates memory exploitation techniques. Our Win64 port of
Chromium is now continuously tested on the main waterfall and is on track to
ship this quarter. Lastly, we migrated our Linux and Chrome OS sandbox to a new
policy format and did a lot of overdue sandbox code cleanup.
On our [site
isolation](http://dev.chromium.org/developers/design-documents/site-isolation)
project, we’ve started landing infrastructure code on trunk to support
out-of-process iframes. We are few CLs away from having functional cross-process
iframe behind a flag and expect it to be complete by the end of January!
Mobile, mobile, mobile: We’ve started focusing more attention to hardening
Chrome on Android. In particular, we’ve been hacking on approaches for strong
sandboxing (e.g.
[seccomp-bpf](http://blog.chromium.org/2012/11/a-safer-playground-for-your-linux-and.html)),
adding [Safe Browsing](http://www.google.com/transparencyreport/safebrowsing/)
protection, and getting [ClusterFuzz](https://cluster-fuzz.appspot.com/) tuned
for Android.
For more thrilling security updates and feisty rants, catch ya on
[security-dev@chromium.org](mailto:security-dev@chromium.org).
Happy Hacking,
Parisa, on behalf of Chrome Security

## Q3 2013

An early
[boo](http://mountainbikerak.blogspot.com/2010/11/google-chrome-pumpkin.html)
and (late) quarter update from the Chrome Security Team!

For those that don’t know us already, we help make Chromium the [most secure
browsing platform in the
market](http://www.chromium.org/Home/chromium-security). In addition to security
reviews and consulting, running a [vulnerability reward
program](http://dev.chromium.org/Home/chromium-security/vulnerability-rewards-program),
and dealing with security surprises, we instigate and work on engineering
projects that make Chrome more secure.

Last quarter, we reorganized the larger team into 3 subgroups:

**Bugs--**, a group focused on finding security bugs, responding to them, and
helping get them fixed. The group is currently working on expanding
[Clusterfuzz](https://cluster-fuzz.appspot.com/) coverage to other platforms
(Windows and Mac), adding fuzzers to cover IPC, networking, and WebGL, adding
more [security
ASSERTS](http://svnsearch.org/svnsearch/repos/BLINK/search?logMessage=ASSERT_WITH_SECURITY_IMPLICATION)
to catch memory corruption bugs. They're also automating some of the grungy and
manual parts of being [security
sheriff](http://dev.chromium.org/Home/chromium-security/security-sheriff) to
free up human cycles for more exciting things.

Enamel, a group focused on usability problems that affect end user security or
the development of secure web applications. In the near-term, Enamel is working
on: improving the malware download warnings, SSL warnings, and extension
permission dialogs; making it safer to open PDFs and .docs in Chrome; and
investigating ways to combat popular phishing attacks.

Guts, a group focused on ensuring Chrome’s architecture is secure by design and
resilient to exploitation. Our largest project here is [site
isolation](http://dev.chromium.org/developers/design-documents/site-isolation),
and in Q4, we’re aiming to have a usable cross-process iframe implementation
(behind a flag ;) Other Guts top priorities include sandboxing work (stronger
sandboxing on Android, making Chrome OS’s
[seccomp-bpf](https://code.google.com/p/chromium/wiki/LinuxSandboxing#The_seccomp-bpf_sandbox)
easier to maintain and better tested), supporting [NPAPI
deprecation](http://blog.chromium.org/2013/09/saying-goodbye-to-our-old-friend-npapi.html),
launching 64bit Chrome for Windows, and Blink memory hardening (e.g. heap
partitioning).

Retrospectively, here are some of notable security wins from recent Chrome
releases:

In [Chrome
29](http://googlechromereleases.blogspot.com/2013/08/stable-channel-update.html),
we tightened up the sandboxing policies on Linux and added some defenses to the
Omaha (Chrome Update) plugin, which is a particularly exposed and attractive
target in Chrome. The first parts of Blink heap partition were released, and
we’ve had “backchannel” feedback that we made an impact on the greyhat exploit
market.

In [Chrome
30](http://googlechromereleases.blogspot.com/2013/10/stable-channel-update.html)
we fixed a load of security bugs! The spike in bugs was likely due to a few
factors: (1) we started accepting fuzzers (7 total) from invited external
researchers as part of a Beta extension to our vulnerability reward program
(resulting in
[26](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+label%3AExternal-Fuzzer-Contribution+-status%3AWontFix%2CDuplicate&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Stars+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner&x=m&y=releaseblock&cells=tiles)
new bugs), (2) we [increased reward
payouts](http://googleonlinesecurity.blogspot.com/2013/08/security-rewards-at-google-two.html)
to spark renewed interest from the public, and (3) we found a bunch of new
buffer (over|under)flow and casting bugs ourselves by adding
[ASSERT_WITH_SECURITY_IMPLICATION](http://svnsearch.org/svnsearch/repos/BLINK/search?logMessage=ASSERT_WITH_SECURITY_IMPLICATION)s
in Blink. In M30, we also added [a new layer of
sandboxing](https://code.google.com/p/chromium/issues/detail?id=168812) to NaCl
on Chrome OS, with seccomp-bpf.

Last, but not least, we want to give a shout out to individuals outside the
security team that made an extraordinary effort to improve Chrome security:

*   Jochen Eisinger for redoing the pop-up blocker... so that it [actually
    blocks pop-ups](https://code.google.com/p/chromium/issues/detail?id=38458)
    (instead of hiding them). Beyond frustrating users, this bug was a security
    liability, but due to the complexity of the fix, languished in the issue
    tracker for years.
*   Mike West for his work on CSP, as well as tightening downloading of bad
    content types.
*   Avi Drissman for fixing a [longstanding bug where PDF password input was not
    masked](http://code.google.com/p/chromium/issues/detail?id=54748#c49).
*   Ben Hawkes and Ivan Fratic for finding
    [four](https://code.google.com/p/chromium/issues/list?can=1&q=label%3AWinFuzz)
    potentially exploitable Chrome bugs using WinFuzz.
*   Mateusz Jurczyk on finding ton of bugs in VP9 video decoder.

Happy Hacking,
Parisa, on behalf of Chrome Security

## Q2 2013

Hello from the Chrome Security Team!

For those that don’t know us, we’re here to help make Chrome a very (the most!)
[secure browser](http://www.chromium.org/Home/chromium-security). That boils
down to a fair amount of work on security reviews (and other consulting), but
here’s some insight into some of the other things we were up to last quarter:

Bug Fixin’ and Code Reviews

At the start of the quarter, we initiated a Code 28 on security bugs to trim
back the fat backlog of open issues. With the help of dozens of engineers across
Chrome, we fixed over 100 security bugs in just over 4 weeks and brought the
count of Medium+ severity issues to single digits. (We’ve lapsed a bit in the
past week, but hopefully will recover once everyone returns from July vacation
:)

As of July 1st, [Clusterfuzz](http://goto/clusterfuzz) has helped us find and
fix
[822](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+ClusterFuzz+status%3AFixed+closed-before%3A2013%2F7%2F1&sort=-id+-security_severity+-secseverity+-owner+-modified&colspec=ID+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner&x=m&y=releaseblock&cells=tiles)
bugs! Last quarter, we added a [new
check](http://svnsearch.org/svnsearch/repos/BLINK/search?logMessage=ASSERT_WITH_SECURITY_IMPLICATION)
to identify out of bound memory accesses and bad casts
(ASSERT_WITH_SECURITY_IMPLICATION), which resulted in
~[72](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+%22ASSERTION+FAILED%22+status%3AFixed+opened-after%3A2013%2F1%2F1&sort=-security_severity+-secseverity+-owner+-modified&colspec=ID+Stars+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner&x=m&y=releaseblock&cells=tiles)
new bugs identified and fixed. We’re also beta testing a “Fuzzer Donation”
extension to our [vulnerability reward
program](http://dev.chromium.org/Home/chromium-security/vulnerability-rewards-program).

Anecdotally, this quarter we noticed an increase in the number of IPC reviews
and marked decrease in security issues! Not sure if our recent [security tips
doc](security-tips-for-ipc) is to credit, but well done to all the IPC authors
and editors!

Process hardening

We’ve mostly wrapped up the [binding integrity](binding-integrity.md) exploit
mitigation changes we started last quarter, and it’s now landed on all desktop
platforms and Clank. Remaining work entails [making additional V8 wrapped types
inherit from
ScriptWrappable](https://code.google.com/p/chromium/issues/detail?id=236671&can=1&q=tsepez%20scriptwrappable&colspec=ID%20Pri%20M%20Iteration%20ReleaseBlock%20Cr%20Status%20Owner%20Summary%20OS%20Modified)
so more Chrome code benefits from this protection. We also started a new memory
hardening change that aims to [place DOM nodes inside their own heap
partition](https://code.google.com/p/chromium/issues/detail?id=246860). Why
would we want to do that? Used-after-free memory bugs are common. By having a
separate partition, the attacker gets a more limited choice of what to overlap
on top of the freed memory slot, which makes these types of bugs substantially
harder to exploit. (It turns out there is some performance improvement in doing
this too!)

Sandboxing++

We’re constantly trying to improve [Chrome
sandboxing](../../developers/design-documents/sandbox/index.md). On [Chrome OS
and Linux](https://code.google.com/p/chromium/wiki/LinuxSandboxing), The GPU
process is now sandboxed on ARM
([M28](https://code.google.com/p/chromium/issues/detail?id=235870)) and we’ve
been been working on [sandboxing
NaCl](https://code.google.com/p/chromium/issues/detail?id=168812) under
[seccomp-bpf](http://blog.chromium.org/2012/11/a-safer-playground-for-your-linux-and.html).
We’ve also increased seccomp-bpf test coverage and locked down sandbox
parameters (i.e. less attack surface). Part of the Chrome seccomp-bpf sandbox is
now used in google3 (//third_party/chrome_seccomp), and Seccomp-legacy and
SELinux [have been
deprecated](https://code.google.com/p/chromium/wiki/LinuxSandboxing) as
sandboxing mechanisms.

Chrome work across platforms

*   Mobile platforms pose a number of challenges to replicating some of the
    security features we’re most proud of on desktop, but with only expected
    growth of mobile, we know we need to shift some security love here. We’re
    getting more people ramped up to help on consulting (security and code
    reviews) and making headway on short and long-term goals.

*   On Windows, we’re still chugging along sorting out tests and build
    infrastructure to get a stable Win64 release build for canary tests.

*   On Chrome OS, work on kernel ASLR is ongoing, and we continued sandboxing
    [system
    daemons](https://code.google.com/p/chromium/issues/detail?id=224082).

Site Isolation Efforts

After some design and planning in Q1, we started building the early support for
[out-of-process
iframes](http://www.chromium.org/developers/design-documents/oop-iframes) so
that [Chrome's sandbox can help us enforce the Same Origin
Policy](http://www.chromium.org/developers/design-documents/site-isolation). In
Q2, we added a FrameTreeNode class to track frames in the browser process,
refactored some navigation logic, made DOMWindow own its Document (rather than
vice versa) in Blink, and got our prototype to handle simple input events. We'll
be using these changes to get basic out-of-process iframes working behind a flag
in Q3!

Extensions & Apps

This quarter, we detected and removed ~N bad extensions from the Web Store that
were either automatically detected or manually flagged as malicious or violating
our policies. We’ve started transitioning manual CRX malware reviews to a newly
formed team, who are staffing and ramping up to handle this significant
workload. Finally, we’ve been looking at ways to improve the permission dialog
for extensions so that it’s easier for users to understand the security
implications of what they’re installing, and working on a set of experiments to
understand how changes to the permissions dialog affect user installation of
malware.

Happy Q3!

Parisa, on behalf of Chrome Security

## Q1 2013

Hi from the Chrome Security Team!

For those that don’t know us already, we’re here to help make Chrome the [most
secure browser in the market](http://www.chromium.org/Home/chromium-security).
We do a fair bit of work on security reviews of new features (and other
consulting), but here’s a summary of some of the other things we were up to last
quarter:

Bug, bugs, bugs

Though some time is still spent [handeling external security
reports](http://www.chromium.org/Home/chromium-security/security-sheriff)
(mainly from participants of our [vulnerability reward
program](http://www.chromium.org/Home/chromium-security/vulnerability-rewards-program)),
we spent comparatively more time in Q1 hunting for security bugs ourselves. In
particular, we audited a bunch of IPC implementations after the
[two](http://blog.chromium.org/2012/10/pwnium-2-results-and-wrap-up_10.html)
[impressive](http://blog.chromium.org/2012/05/tale-of-two-pwnies-part-1.html)
IPC-based exploits from last year - aedla found some juicy sandbox bypass
vulnerabilities ([161564](http://crbug.com/161564),
[162114](http://crbug.com/162114), [167840](http://crbug.com/167840),
[169685](http://crbug.com/169685)) and cdn and cevans found / fixed a bunch of
other interesting memory corruption bugs
([169973](https://code.google.com/p/chromium/issues/detail?id=169973),
[166708](https://code.google.com/p/chromium/issues/detail?id=166708),
[164682](https://code.google.com/p/chromium/issues/detail?id=164682)).
Underground rumors indicate many of these internally discovered bugs collided
with discoveries from third party researchers (that were either sitting on or
using them for their own purposes). At this point, most of the IPCs that handle
file paths have been audited, and we’ve started putting together a doc with
[security tips to mind when writing
IPC](https://sites.google.com/a/google.com/chrome-security/security-tips-for-ipc).

On the fuzzing front, we updated and added a number of [fuzzers to
Clusterfuzz](https://cluster-fuzz.appspot.com/#fuzzers): HTML (ifratric,
mjurczyk), Flash (fjserna), CSS (bcrane), V8 (farcasia), Video VTT (yihongg),
extension APIs (meacer), WebRTC (phoglund), Canvas/Skia (aarya), and
Flicker/media (aarya); aarya also taught Clusterfuzz to look for dangerous
ASSERTs with security implications, which resulted in even more bugs. Kudos to
Clusterfuzz and the ASAN team for kicking out another [132 security
bugs](https://code.google.com/p/chromium/issues/list?can=1&q=type%3Abug-security+label%3AClusterFuzz+opened-after%3A2012%2F12%2F31+-status%3Awontfix+-status%3Aduplicate+opened-before%3A2013%2F4%2F1&sort=-id+-security_severity+-secseverity+-owner+-modified&colspec=ID+Stars+Pri+Status+Summary+Modified+OS+M+Security_severity+Security_impact+Owner&x=m&y=releaseblock&cells=tiles)
last quarter! One downside to all these new bugs is that our queue of open
security bugs across Chrome has really spiked ([85+ as of
today](https://code.google.com/p/chromium/issues/list?can=2&q=Security_Severity%3DCritical+OR+Security_Severity%3DHigh+OR+Security_Severity%3DMedium&colspec=ID+Pri+M+Iteration+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)).
PIease help us fix these bugs!

Process hardening

We’re constantly thinking about proactive hardening we can add to Chrome to
eliminate or mitigate exploitation techniques. We find inspiration not only from
cutting edge security defense research, but also industry chatter around what
the grey and black hats are using to exploit Chrome and other browsers. This
past quarter jln implemented more fine grained support for [sandboxing on
Linux](https://code.google.com/p/chromium/wiki/LinuxSandboxing), in addition to
some low level tcmalloc changes that improve ASLR and general allocator security
on 64-bit platforms. With jorgelo, they also implemented support for a stronger
GPU sandbox on Chrome OS (which we believe was instrumental in avoiding a Pwnium
3 exploit). tsepez landed support for [V8 bindings
integrity](binding-integrity.md) on Linux and Mac OS, a novel feature that
ensures DOM objects are valid when bound to Javascript; this avoids exploitation
of type confusion bugs in the DOM, which Chrome has suffered from in the past.
palmer just enabled bindings integrity for Chrome on Android, and work is in
progress on Windows.

Work across platforms

One of our key goals is to get Chrome running natively on 64-bit Windows, where
the platform mitigations against certain attacks (such as heap spray) are
stronger than when running within a WOW64 process. (We’ve also seen some
performance bump on graphics and media on 64-bit Windows!) We made serious
progress on this work in Q1, coordinating with engineers on a dozen different
teams to land fixes in our codebase (and dependencies), working with Adobe on
early Flapper builds, porting components of the Windows sandbox to Win64, and
landing 100+ generic Win64 build system and API fixes. Thanks to all that have
made this possible!

As Chrome usage on mobile platforms increases, so too must our security
attention. We’ve set out some short and long-term goals for mobile Chrome
security, and are excited to start working with the Clank team on better
sandboxing and improved HTTPS authentication.

Site isolation

Work continues on the ambitious project to support [site-per-process
sandboxing](http://www.chromium.org/developers/design-documents/site-isolation),
which should help us [prevent additional attacks aimed at stealing or tampering
with user data from a specific
site](https://docs.google.com/a/google.com/document/d/1X5xZ2hYZurR_c2zU11AoEn15Ebu1er4cCLEudLJvPHA/edit).
Last quarter, we published a more complete [design for out-of-process
iframes](http://www.chromium.org/developers/design-documents/oop-iframes), set
up performance and testing infrastructure, and hacked together a prototype
implementation that helped confirm the feasibility of this project and surface
some challenges and open questions that need more investigation.

Extensions

When not feeding the team fish, meacer added a lot of features to Navitron to
make flagged extensions easier to review and remove from the WebStore. To put
this work in perspective, each week ~X new items are submitted to Webstore, ~Y
of them are automatically flagged as malware (and taken down), ~Z malware
escalations are manually escalated from extension reviewers (and then reviewed
again by security;. meacer also added a fuzzer for extensions and apps APIs, and
has been fixing [the resulting
bugs](https://code.google.com/p/chromium/issues/list?can=1&q=Fuzzer%3A+Meacer_extension_apis&colspec=ID+Pri+M+Iteration+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles).

Until we meet again (probably in the issue tracker)...

Parisa, on behalf of Chrome Security
