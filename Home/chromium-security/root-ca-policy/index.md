# Root Certificate Policy

## Google Chrome

Google Chrome attempts to use the root certificate store of the underlying
operating system to determine whether an SSL certificate presented by a site is
indeed trustworthy, with a few exceptions.

### Root Certificate Programs

In order for Chrome to be able to trust a root certificate, it must either be
included by the underlying operating system or explicitly added by users. If you
are a root CA, the following contacts should be used:

*   Microsoft Windows: [Microsoft Root Certificate
    Program](https://aka.ms/rootcert).
*   Apple OS X: [Apple Root Certificate
    Program](http://www.apple.com/certificateauthority/ca_program.html)
*   Linux: There is no central root certificate program as part of Linux. When
    running on Linux, Google Chrome uses the [Mozilla Network Security Services
    (NSS)](http://www.mozilla.org/projects/security/pki/nss/) library to perform
    certificate verification. When packaged or built from source, NSS includes
    certificates vetted according to the [Mozilla Root Certificate
    Program](http://www.mozilla.org/projects/security/certs/policy/). For most
    Linux users, it is sufficient that once included in the Mozilla Root
    Program, users of Google Chrome should see your root CA as trusted. However,
    please be aware that Linux distributions which package NSS may further alter
    this list with additions or removals based on local, distribution-specific
    root certificate programs, if any.
*   Android: Please file a bug at
    <http://code.google.com/p/android/issues/entry> . Note that, similar to
    Linux, the certificates included within the Android sources may be further
    altered by device manufacturers or carriers, pursuant to their local
    programs.

### Extended Validation Certificates

Google Chrome maintains a hard-coded list in the binary of which root
certificates are "EV-Qualified", along with the appropriate OID that must appear
on certificates issued from that root to be considered EV certificates. If you
are a root CA who issues EV certificates and Google Chrome does not already
recognize these certificates as EV certificates, please file a bug in our bug
tracker (<https://bugs.chromium.org/p/chromium/issues/entry>) including the name
of your CA, the fingerprint of your EV root, the OID you use to issue EV certs,
a link to your WebTrust for CAs - EV audit, and a link to a server hosting a
test or real certificate issued from this root.

From 1 January 2015, Google Chrome requires all EV certificates to use
Certificate Transparency. Full details can be found in
[CTPolicyMay2016edition.pdf](CTPolicyMay2016edition.pdf).

### Removal of Trust

Google Chrome reserves the right to distrust root certificates present in the
operating system's root certificate list. At the core of trust in the PKI system
is the fact that the operation of Root CAs is beyond reproach. If one of these
guardians of trust were to operate in a non-trustworthy way, it would be no
different than a police officer who was covering up a crime or protecting the
identity of a criminal (because it reflected personally on the officer), or a
firefighter who was not responding to fires in which people died. If one of
these bastions of public trust (police, fire) were violating the trust we had
placed in them, the reaction would be strong and swift. And, it is worth noting
that this is so egregious a violation, that there is no consideration as to the
collateral damage that might be caused by removing him/her from society - for
example, hardship to his family, since he is the sole breadwinner. Our hearts
would go out to those who were adversely affected, but it would not alter the
effect.

We trust root certificate authorities to properly perform the following tasks:

*   Verify the identity of the requester to the extent dictated by the type of
    certificate (for example, domain control for server certs, full identity and
    organizational affiliation for EV certs).
*   Ensure that there is no way to issue a certificate without a permanent
    record.
*   Keep unalterable logs of all certificates signed by their CA.
*   Audit those logs frequently for evidence of unauthorized issuance
*   In the event of the mis-issuance of a certificate, proactively 1)
    communicate that to any parties that might be affected, and 2) revoke any
    mis-issued certificates, and publish notice of that revocation.
*   Protect their infrastructure so as to minimize the chance that an intruder
    could gain access and issue fraudulent certs.
*   In the event of a broader compromise, conduct a full post-mortem, and
    publicly publish the findings and plans for remediation of all problems, in
    addition to contacting the organizations directly.

Note: the amount of communication required for a mis-issuance is proportional to
the possible effect. For example, if an authorized employee simply misspells a
server name of a customer, then notifying the customer, revoking the certificate
and reissuing a correct one is probably adequate. However, if unknown
individuals were able to issue multiple fraudulent certs, especially for
well-known internet sites (like Microsoft, Yahoo or Google), then immediate full
and public disclosure is expected in addition to specific outreach to the
affected site(s). Any error should be on the side of over-disclosure.

**In the case of a compromise of a root certificate authority, Google reserves
the right to add that root certificate to the list of root certificates that
Google Chrome will not trust**, regardless of the settings of the underlying
operating system. That decision will be based in part on the response and how
proactive the root certificate authority is in regards to discovering and
mitigating the incident.

Let’s look at a couple of examples of recent compromises relative to these
principles.

In March of 2011, Comodo issued fraudulent certs for a number of well-known
internet sites including Microsoft, Yahoo and Google. This was not due to a
compromise at Comodo, but rather at an authorized Registration Authority
operating on Comodo’s behalf. In that case, Comodo immediately spotted the
mis-issuance, revoked the certificates, notified the affected parties, and made
a full and public disclosure of what had happened, albeit a week after the
event. While the compromise itself cannot be minimized, Comodo mostly acted in a
manner consistent with the trust placed in them as a Root CA (earlier disclosure
would have been better).

In another case, in July 2011, Diginotar discovered a compromise to their
systems, resulting in the mis-issuance of hundreds of certificates. Upon
discovery, they chose to quietly revoke the certificates and not announce to
anyone the occurrence. Subsequently, a certificate for \*.google.com which was
issued by their Root CA was found in the wild, and reported by an astute user
who was using Chrome as his browser. Chrome has extra checks built in for
accessing Google sites, and displayed a warning to the user. The discovery only
became public because the user posted his discovery on a public web forum.

This case is very different from the Comodo case, despite a surface similarity.
First, Diginotar did not have adequate controls (logging) in place to detect
certificate mis-issuance. They only noticed once it was brought to their
attention by third parties. They also (apparently) did not have sufficiently
adequate controls in place to prevent unauthorized access to their network and
systems. It is unclear whether they had adequate audit procedures in place as
those depend upon adequate logging, which was not present. In addition to this,
once these breaches were discovered, Diginotar did not publicly disclose the
breaches, nor did they publish a post-mortem describing what happened, and what
was being done to remediate the problem. We do not know if Diginotar notified
the organizations for whom there was a mis-issuance, as they have not been
forthcoming with that information. Based on which organizations were affected,
we strongly believe that they did not, as those organizations would have almost
certainly announced the mis-issuance themselves.

As a further indication of their lack of regard for proper security, once their
certificates had been removed from browsers (essentially, removing their trusted
status), they simply advised end-users to “click-thru” warnings that the browser
generates at sites that cannot present a chain of trust to an established Root
certificate. Thus, Diginotar was advocating even more bad security practices to
circumvent the provisions put into place to protect end-users from their
original bad practices. (Note: Diginotar removed the direction to click-thru
warnings a couple of days later, and replaced it with a statement that 99.9% of
the time the warnings are incorrect, and the certificates can be trusted. They
also posted directions on how to download the Diginotar Root certificate and
install it manually as a trusted Root certificate.)

These actions by Diginotar raised serious questions about 1) their competence to
run a Certificate Authority (of any kind, much less a Root CA), and 2) their
interest in protecting the Internet as a whole, over their own interests. For
these reasons, we no longer felt that Diginotar deserved the trusted position
they once had, and we removed them from that position. We felt that removing
their trusted status was warranted for all CAs that Diginotar operates. Further,
we intended to block their attempts to restore themselves to a position of trust
by creating (or using) alternate CAs.

It is imperative that a user of Google Chrome can be confident that when proper
SSL indications are shown in the browser, the user is in fact communicating with
the intended site and not an attacker or other man-in-the-middle using a root
certificate obtained improperly from a CA. Anything that contravenes this
principle, including issuance of certificates for a website to a party other
than the legitimate operator of that website, or delegation of authority that
results in the issuance of certificates for a website to a party other than the
legitimate operator of that website, is a serious violation of trust that will
be dealt with in accordance to this policy.

## Chrome OS

The root certificate policy for Chrome OS is forthcoming.
