# Certificate Transparency

## Overview

Chromium includes support for Certificate Transparency, a protocol defined by
[RFC 6962](https://tools.ietf.org/html/rfc6962). Certificate Transparency is a
way for interested parties, such as Certificate Authorities, to provide a
publicly auditable record of certificate issuance, through submitting these
certificates to a Certificate Transparency Log.

Discussions related to Chromium's Certificate Transparency support happen on the
[Certificate Transparency
Policy](https://groups.google.com/a/chromium.org/forum/#!forum/ct-policy)
group/mailing list.

## For Certificate Authorities

In order to help protect the users of the Chromium Projects, CAs are expected to
support Certificate Transparency. This allows users, the Chromium Authors, and
the public to publicly audit that CAs are conforming to the policies set out in
the [Root Certificate Policy](root-ca-policy/index.md).

Currently, Chromium does not enforce that all Root CAs support Certificate
Transparency for all certificates. However, it is required for some CAs, and it
is required in order to have a certificate recognized as an EV certificate, that
the certificate MUST be CT Qualified. For more details, including timelines and
requirements, see the [Certificate Transparency in Chrome
Policy](root-ca-policy/CTPolicyMay2016edition.pdf).

## For Log Operators

In order for a Log to be included within Chromium, it must meet the requirements
of the [Certificate Transparency Log
Policy](certificate-transparency/log-policy/index.md). The Log Policy also
describes the steps for Log Operators to submit Logs for inclusion within
Chromium.

## Recognized Logs

The following table includes information about the Certificate Transparency Logs
that are recognized by Chromium. It includes information about who operates the
log, the name the log has been given, and the URL that can be used for logging
certificates or inspecting the certificates that have been logged.

Note: The authoritative list is maintained in the Chromium [code
base](https://cs.chromium.org/chromium/src/net/data/ssl/certificate_transparency/log_list.json).
This is merely informational.

**Qualified Logs**

** Log Operator** ** Name** ** Log URL** ** Maximum Merge Delay** ** Included
Since** [Google](https://www.google.com) Google 'Pilot' Log
<https://ct.googleapis.com/pilot> 24 hours * Revision*:
<https://crrev.com/237785>
* Chrome:* 35 [Google](https://www.google.com) Google 'Aviator' Log
<https://ct.googleapis.com/aviator> 24 hours * Revision*:
<https://crrev.com/237785>
* Chrome*: 35
*Note: Frozen (not accepting new certificates)*
[DigiCert](https://www.digicert.com/) DigiCert's Certificate Transparency log
<https://ct1.digicert-ct.com/log/> 24 hours *Revision*:
<https://crrev.com/309831>
* Chrome*: 41 [Google](https://www.google.com) Google 'Rocketeer' Log
<https://ct.googleapis.com/rocketeer> 24 hours *Revision:
*<https://crrev.com/325382>
*Chrome*: 43 [Symantec](http://www.symantec.com/)
Symantec Log <https://ct.ws.symantec.com> 24 hours *Revision*:
<https://crrev.com/483625>
*Chrome*: 45 [Symantec](http://www.symantec.com/) Symantec 'Vega' Log
<https://vega.ws.symantec.com/> 24 hours *Revision*: <https://crrev.com/376143>
* Chrome*: 50 [CNNIC](http://cnnic.cn/) CNNIC CT Log
<https://ctserver.cnnic.cn/> 24 hours *Revision*: <https://crrev.com/396817>
*Chrome*: 53 [WoSign](https://www.wosign.com/) WoSign Log
<https://ctlog.wosign.com/> 24 hours *Revision*: <https://crrev.com/414378>
*Chrome*: 54 [StartCom](https://www.startssl.com/) StartCom CT Log
<https://ct.startssl.com/> 24 hours *Revision*: <https://crrev.com/414440>
*Chrome*: 54 [Google](https://www.google.com/) Google 'Skydiver' Log
<https://ct.googleapis.com/skydiver/> 24 hours *Revision*:
<https://crrev.com/429670>
*Chrome*: 55 [Google](https://www.google.com/) Google 'Icarus' Log
<https://ct.googleapis.com/icarus/> 24 hours *Revision*:
<https://crrev.com/429670>
*Chrome*: 55 [Comodo](https://ssl.comodo.com/) Comodo 'Sabre' Log
<https://sabre.ct.comodo.com/> 24 hours *Revision*: <https://crrev.com/482145>
*Chrome*: 60 [Comodo](https://ssl.comodo.com/) Comodo 'Mammoth' Log
<https://mammoth.ct.comodo.com/> 24 hours *Revision*: <https://crrev.com/482145>
*Chrome*: 60
**Once, but no longer, Qualified Logs**
**Log Operator** **Name** **Log URL** **Maximum Merge Delay** **Included Since**
[Certly](https://certly.io/) Certly.IO Log <https://log.certly.io> 24 hours
*Revision*: <https://crrev.com/325382>
*Chrome*: 43
*Last Accepted SCT: *15 April 2016 00:00:00 UTC.
[Izenpe](http://www.izenpe.com/) Izenpe Log <https://ct.izenpe.com> 24 hours
*Revision*: <https://crrev.com/326301>
*Chrome*: 44
*Last Accepted SCT*: 30 May 2016 00:00:00 UTC. [Venafi](https://www.venafi.com/)
Venafi CT Log Server <https://ctlog.api.venafi.com/ct/v1> 24 hours *Revision*:
<https://crrev.com/349170>
*Chrome*: 47
*Last Accepted SCT: *28 Feb 2017 18:42:26 UTC.
