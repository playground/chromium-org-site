# Security Sheriff

**What's a security sheriff?**

A Security Sheriff is a member of a rotation that occurs in 1-week time slots.
([Here is the rotation
schedule](https://docs.google.com/spreadsheets/d/10sLYZbi6QfLcXrhO-j5eSc82uc7NKnBz_o1pR9y8h7U/edit#gid=0).)
Each week as a primary and secondary sheriff, and during their rotation both
have various important responsibilities:

Primary Sheriff:

*   Look at [every incoming security bug report on the
    dashboard](https://cluster-fuzz.appspot.com/?noredirect=1#sheriff) and
    ensure it is accurately triaged, and actively progressing towards getting
    fixed.
*   Keep the Handoff Log up to date
*   Shout for help if the incoming bug rate is too high ([suggested vocal
    exercises](https://youtu.be/5y_SbnPx_cE?t=37s)) - first person to ask is the
    secondary sheriff
*   Stay sharp, keep in shape ([hand-stand
    pushups](https://www.youtube.com/watch?v=jZ1ZDlLImF8#t=50) are standard for
    the primary sheriff), and remember you may be [called upon during
    emergencies](https://www.youtube.com/watch?v=buHaKYL9Jhg).

Secondary Sheriff:

*   Ensure that all incoming queries to the **security****@chromium.org** and
    the **chrome-security@google.com** get a reply (by someone; not necessarily
    the sheriff themselves).
    *   Direct links to lists (both require permission):
        **[security@chromium.org
        list](https://groups.google.com/a/chromium.org/forum/#!forum/security)**
        and **[chrome-security@google.com
        list](https://groups.google.com/a/google.com/forum/#!forum/chrome-security)**
    *   Note: external emails will always come in on **security@chromium.org**
        as **chrome-security@google.com** is a google-only list, but both need
        to be triaged.
*   Ensure [accurate label management](../security-labels.md) on bugs, for
    example applying the correct Merge and Restrict-View labels when a bug
    transitions to Fixed.
*   Change bugs status to fixed for those that developer forgets to close. Make
    sure to read bug comments where developer might point out that it needs more
    cls, etc, and wait 24 hours before closing clusterfuzz bugs, to give
    clusterfuzz a chance to close it automatically.
*   Look at open security bug reports and check that progress is occurring.
*   Generally keep an eye on all bug traffic in case anything needs actioning or
    replying to.
*   Stay sharp, keep in shape ([finger
    exercises](https://youtu.be/20elMaVZ9lg?t=47s) are standard for the
    secondary sheriff), and remember you may be [called upon during
    emergencies](https://www.youtube.com/watch?v=30BTQ6B4Y6M).

**I'm the security sheriff, what do I do?**

Take a quick look through the [Sheriff Handoff Log (Googlers
only)](http://go/chrome-security-sheriff-handoff).

Click [here](https://cluster-fuzz.appspot.com/#sheriff) to get to the sheriff
dashboard, which should display all the incoming bug reports that need to be
addressed.

Do as much as you can for the week to triage, shepherd, and wrap up open
security bugs. What follows are the details of what that entails, but it
practically means whacking out all the items from the burn down list
(prioritized from top to bottom) in our dashboard. **I****f you're ever stuck or
in doubt, ask for help on #chrome-security!**

![image](Life%20of%20a%20Security%20Issue%20%283%29.png)

## **Diagnose the issue.**

## ([link](https://twitter.com/__apf__/status/728776130564526080))

*   **If the report is invalid, **then remove the Restrict-View-SecurityTeam
    label and mark it WontFix.
*   **If the report is a duplicate, **mark it Duplicate, and if the issue this
    is a duplicate of is public, remove the Restrict-View-SecurityTeam label.
*   **If the report is primarily a privacy issue, **then send it to the privacy
    team:
    *   add the Privacy component so that it enters their triage queue
    *   cc any security team members, including yourself, who may be interested
        in the privacy issue
    *   change the Restrict-View-SecurityTeam label to
        Restrict-View-ChromePrivacy
        *   Note that security team members don't automatically have privacy bug
            access, so this will probably make the issue inaccessible to you.
*   **If the report is asking about why something is or is not on the Safe
    Browsing list:**
    *   assign it to zbutler@, who will triage it for the Safe Browsing team
    *   remove the Restrict-View-SecurityTeam label and add the
        Restrict-View-Google label
    *   change Type-Bug-Security label to Type-Bug
    *   add the Security component
*   **If the report is a potentially valid bug but is not a security
    vulnerability, then:**
    *   remove the Restrict-View-SecurityTeam label. If necessary, add one of
        the other Restrict-View-\* labels:
        *   Restrict-View-Google if this is a crash report
        *   Restrict-View-EditIssue if the bug can be abused (e.g. Denial of
            service)
    *   change Type-Bug-Security label to Type-Bug (or whatever Type is
        appropriate)
    *   add appropriate component or CCs to ensure it does get triaged
    *   add the Security component or the Team-Security-UX label if the security
        team should still track the issue (e.g. security features).
*   **If the report doesn't have enough information, **ask the reporter for more
    information, add the Needs-Feedback label and wait for 24 hours for a
    response.
*   **If the report smells like a vulnerability, keep going.**

## **Verify (and label!) the bug.**

**Step 1. Reproduce legitimate sounding issues.**

If you can't reproduce the issue, ask for help on IRC (#chrome-security), or
find an area owner to help.

Tips for reproducing bugs

*   <https://cluster-fuzz.appspot.com/#uploadusertestcase> allows you to upload
    files to reproduce crashes on various platforms and will identify revision
    ranges when the regression was introduced. If a test case requires multiple
    files, they can be uploaded together in a zip or tar archive.
*   When you can't just build from a specific branch locally, check out
    <https://dev.chromium.org/getting-involved/dev-channel> or
    <https://commondatastorage.googleapis.com/chromium-browser-asan/index.html>
    for latest release of a specific version.
*   There are many tools available to help you reproduce various memory issues
    reliably. If you aren't already familiar with them, check out
    [AddressSanitizer](../../../developers/testing/addresssanitizer.md),
    [MemorySanitizer](../../../developers/testing/memorysanitizer.md),
    [ThreadSanitizer](../../../developers/testing/threadsanitizer-tsan-v2.md),
    and
    [UndefinedBehaviorSanitizer](../../../developers/testing/undefinedbehaviorsanitizer.md).
*   If you run into issues with a reproducible ClusterFuzz test case (like
    missing symbols, or if anything else seems off), try uploading the test case
    again using a different job type with a more mature tool (e.g. ASan on
    Linux). It may give more complete information.

**Step 2. Assess the severity.**

Severity guidelines are [here](../../../developers/severity-guidelines.md). If
it's a critical vulnerability, act quick! We aim to get users patched in < 30
days. Remember that if something requires an unusual configuration or
complicated user interaction, the severity rating should be lowered.

**Bug chains** are typically composed of several individual security bugs and
should be split into a new bug for each potential fix required, so this allows
each team to work on fixing their part of the chain. In cases like this, leave
the main bug as the severity/priority of the full chain, and mark child bugs as
being blockers of the parent bug each with their own separate severity. Each
child bug can have its own priority. Examples of this in action are
[352369](https://crbug.com/352369) and [453937](https://crbug.com/453937).

**Step 3. [Label, label, label.](../security-labels.md)**

Much of Chrome's development and release process depends on bugs having the
right labels. Labels are important! Check out all the security-specific labels
we use to track bugs [here](../security-labels.md).

Labels to **double check **(that should already be there if the bug was filed
using the Security template):

*   Restrict-View-SecurityTeam
*   Type-Bug-Security
*   **If the reporter wants to remain anonymous, add
    Restrict-View-SecurityEmbargo.**

Mandatory labels to **add**:

1.  Security_Severity-{Critical, High, Medium, Low}
2.  Security_Impact-{Head, Beta, Stable, None} (Note: if a bug affects both the
    beta and stable channels, assign only the earliest label.
    Security_Impact-None applies to issues that to not impact chrome directly,
    or are disabled.)
3.  Pri-{0, 1, 2, 3}
4.  OS-{All, Android, Chrome, Linux, Mac, Windows}
5.  M-{<milestone>} (Check the [severity
    guidelines](../../../developers/severity-guidelines.md) for which milestones
    to apply, and [OmahaProxy](https://omahaproxy.appspot.com/) for current
    milestones.)
6.  Component (Helpful for making sure we have the right owner and ccs on a
    bug.)

And then **add the general bug labels **too (described
[here](../../../getting-involved/bug-triage.md)):

*   Update the bug status: Untriaged, Available, Assigned, or Started (as
    appropriate).
*   Apply additional labels as needed.
    *   If it involves the user, add the Team-Security-UX label so that the
        Enamel team can track it. Example topics are: spoofing, phishing,
        permissions.
    *   Stability-Crash for crash reports.
    *   Any changes that will be merged to the beta branch must be labeled as
        ReleaseBlock-Stable.
*   **Ensure the comment adequately explains any status changes**;* severity,
    milestone, and priority assignment generally require explanatory text.*

Some initial **follow-up** or **hand-off** may be required

*   Report suspected malicious URLs to SafeBrowsing.
    *   Public URL: <https://www.google.com/safebrowsing/report_badware/>
    *   Googlers: see instructions at
        [go/report-safe-browsing](https://goto.google.com/report-safe-browsing)
*   Report suspected malicious file attachments to SafeBrowsing and VirusTotal.
*   Make sure the report is properly forwarded when the vulnerability is in an
    upstream project, the OS, or some other dependency.
*   For vulnerabilities in services Chrome uses (e.g. Omaha, Chrome Web Store,
    SafeBrowsing), make sure the affected team is informed and has access to the
    necessary bugs.

## **Find an owner to fix the bug.**

That owner can be you! Otherwise, this is one of the more grey areas of
sheriffing. With experience, you'll figure out good goto people for certain
areas. Until then, here are tips:

*   Try to determine the correct component before continuing on. It's not enough
    on its own, but it's a good starting point. Many components will
    automatically apply some CCs who may be able to help you out. If it's a
    crash bug, see if ClusterFuzz is able to provide one (will appear in the
    same card as the culprit CL).
*   For crashes, check to see if ClusterFuzz provides a culprit CL. Before you
    assign a bug based on this, do a quick sanity check to ensure the CL could
    have caused the bug. If the result seems wrong, apply the
    Test-Predator-Wrong label to the bug and keep going.
*   If you're able to narrow this to a specific regression range, usually from
    ClusterFuzz for crash bugs, do a quick pass over the git log to see if any
    CLs stand out. If you aren't sure, don't be afraid to add CCs to the bug and
    ask!
*   At this point, you'll probably need to dive in and attempt to root cause the
    bug, which is another complicated grey area that you'll figure out with
    experience. Try not to spend too much time for this on any given bug, as
    some cases will simply be too difficult without a deep understanding of
    certain portions of the codebase.
    *   If you can narrow the bug to a specific file or block of code, or if
        something stands out as suspicious, try to assign an owner based on git
        blame or add some CCs based on OWNERS files
    *   If not, consider searching in the issue tracker for people that fixed
        similar bugs or bugs in similar areas of the code base, such as issues
        with the same components, recently. For example, let's say you were
        trying to figure out a good person to assign a Content>Fonts issue. Look
        for "status=fixed, verified" and query by when the issues were closed
        after (i.e. w/ in the last 30 days == closed>today-30).

*   A few components have their own triage processes or points of contact who
    can help.

    *   V8 bugs? Looking for V8 rolls within the regression range, then look
        within the CLs of those rolls to find possible culprits. v8 have their
        own sheriffing people, and rotation. Follow the process here:
        <https://github.com/v8/v8/wiki/Triaging-issues>. Note: V8 Sheriffs do
        have the Security-Team permission, so they can see the bugs without
        explicit CC.
    *   Skia bugs? If you made it this far and still aren't sure, assign them to
        hcm. Be careful while triaging these! The place where we're crashing
        isn't necessarily the place where the bug was introduced, so blame may
        be misleading.
    *   URL spoofing issues, especially related to RTL or IDNs? See
        [go/url-spoofs](http://go/url-spoofs) for a guide to triaging these.

*   Still stuck? Ask #chrome-security or someone from
    [go/chrome-security-secondary-sheriffs](https://goto.google.com/chrome-security-secondary-sheriffs)
    for help! That's why we're here. Don't be afraid to do this!

*   Make sure that the person you assign to handle a bug is not OOO. And,
    generally, explicitly CC more than 1 person on the bug, if possible, and
    preferably people from more than 1 geographic region. (See the OWNERS
    file(s) that affect(s) the relevant area of code.)

Sometimes, finding an owner isn't enough to ensure that a bug will get fixed.
Check the stale bug list on the [security
dashboard](https://cluster-fuzz.appspot.com/#sheriff) and try resolve some of
the problems that might be blocking these issues. If you get in touch with a bug
owner off of the issue tracker, be sure to have them update the bug so that
future sheriffs are aware of the status.

Q: Why isn’t setting the component alone good enough? A: CC’s are critical
because just assigning to a component is ineffective because the Component’s
team cannot see the issues unless they have the Security View permissions.

**Using the Permission API Kill Switch.**

If you find a vulnerability in a Permission API and need to use the Global
Permissions Kill Switch, then follow the instructions
[here](https://docs.google.com/document/d/17JeYt3c1GgghYoxy4NKJnlxrteAX8F4x-MAzTeXqP4U).

## **Wrap up the fixed issue.**

1.  Change Restrict-View-Security**Team** to Restrict-View-Security**Notify**
    (so people on security-notify@chromium.org can get access to the bug).
2.  For any Security_Severity-{Critical, High, Medium} bugs that
    Security_Impact-{Beta, Stable}, add Merge-Requested so that the fix gets
    merged into the next release. Exercise discretion according to security
    severity and risk associated with the bug fix; you can ask the patch author
    whether any risky code paths are affected. The actual merging and drafting
    of release notes is taken care of by the [security release management
    role](../security-release-management.md).
3.  Chrome's [Vulnerability Rewards
    Program](https://www.google.com/about/appsecurity/chrome-rewards/index.html)
    TPM adds the reward-topanel label by mass modification, but **do **label any
    bugs reported by a @chromium.org email that should be rewarded - e.g. "I'm
    filing this on behalf of" or the like.

## End of Rotation

Update the [Sheriff Handoff Log (Googlers
only)](http://go/chrome-security-sheriff-handoff).
