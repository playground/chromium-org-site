# Clean Software Alliance (CSA)

Chrome is participating in a pilot test of the [Clean Software
Alliance](http://www.cs-alliance.org/) (CSA), and provides a feature by which
CSA Installer Members can record information about what has been installed on
the system. These records consist of the following information:

*   <vendor_id> - unique identifier of the installation vendor.

*   <install_id> - unique installation ID (per vendor).

*   <publisher_id> - unique identifier of the publisher (or main product) being
    installed.

*   <install_time_client> - installation start time as per the user’s machine.

*   <install_time_server> - installation start time as per the server time.

*   <advertisers_id> - a comma-delimited list of unique advertiser IDs accepted
    by the user and installed in addition to the main product.

*   <hmac_sha256_validation> - hmacSHA256 hash of a string containing all the
    entry fields using a private salt provided by every vendor. This field is
    used to validate the entry data and to prevent abuse.

If Chrome detects a [security
incident](https://www.google.ca/chrome/browser/privacy/whitepaper.html#malware),
some of this data may be reported as part of its environmental data collection.
