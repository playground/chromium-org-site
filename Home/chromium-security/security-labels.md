# Security labels/components

Bug database labels are used very heavily for security bugs. We rely on the
labels being correct for a variety of reasons, including driving fixing efforts,
driving release management efforts (merges and release notes) and also
historical queries and data mining.

Because of the extent to which we rely on labels, it is an important part of the
Security Sheriff duty to ensure that all security bugs are correctly tagged and
managed.

Any issue that relates to security should have one of the following:

*   Security (component): **Features that are related to security.**
*   Type-Bug-Security: Designates a security vulnerability that impacts users.
    This label should not be used for new features that relate to security, or
    general remediation / refactoring ideas. (Use Cr-Security for that.)

Labels relevant for any Type-Bug-Security:

*   Security_Severity-{Critical, High, Medium, Low, None}: Designates the
    severity of a vulnerability according to our [severity
    guidelines](../../developers/severity-guidelines.md).
*   Security_Impact-{Head, Beta, Stable, None}: Designates which branch(es) were
    impacted by the bug. Only apply the label corresponding with the earliest
    affected branch. "None" means that a security bug is in a disable feature,
    or otherwise doesn't impact chrome.
*   Restrict-View-SecurityTeam or Restrict-View-SecurityNotify: Labels that
    restrict access to the bug for members of security@chromium.org or
    security-notify@chromium.org, respectively. Should a bug ever contain
    confidential information, or a reporter who wishes to remain anonymous, we
    add Restrict-View-Google.
*   reward-{topanel, unpaid, na, inprocess, #}: Labels used in tracking bugs
    nominated for our [vulnerability reward
    program](vulnerability-rewards-program.md).
*   M-#: Target milestone for the fix.
*   Component: **For bugs filed as Type-Bug-Security, we also want to track
    which component the bug is in.**
*   **ReleaseBlock-Stable: When we find a security bug regression that has not
    yet shipped to stable, we use this label to try and prevent the security
    regression from ever affecting users of the stable channel.**
*   **Pri-#: "Critical" bugs are generally 0; "high" and "medium" bugs 1; "low"
    bugs 2 and "none" bugs three. ****ReleaseBlock-Stable can usually imply
    ****Pri-0****. ([details](../../developers/severity-guidelines.md))**
*   **OS-{All,Chrome,Linux,Windows,Android,iOS,...}: Denotes which operating
    systems are affected.**
*   **Merge-{Request-\*, Approved-\*, Merged-\*}: Security fixes are frequently
    merged to earlier release branches**.****
*   ****Release-#-M##: Denotes which exact patch a security fix made it into.
    This is more fine-grained than the M label. ********Release-0-M50 denotes
    the initial release of a M50 to stable.****
*   ****CVE-####-####: **For security bugs that get assigned a CVE, we tag the
    appropriate bug(s) with the label for easy searching.******

A week or two in the life of a security bug!

Given the importance and volume of labels, an example might be useful:

*   An external researcher files a security bug, with a repro that demonstrates
    memory corruption against the latest (e.g.) M29 dev channel. The labels
    Restrict-View-SecurityTeam and Type-Bug-Security will be applied.
*   The sheriff jumps right on it and uses ClusterFuzz confirms that the bug is
    a novel and nasty-looking buffer overflow in the renderer process.
    ClusterFuzz also confirms that all current releases are affect. Since M27 is
    the current stable release, and M28 is in beta, these labels are added:
    M-27, Security_Impact-Stable, Security_Impact-Beta. The severity of a buffer
    overflow implies: Security_Severity-High, Pri-1. Any external report for a
    confirmed medium-or-higher severity vulnerability needs: reward-topanel. The
    stack trace provided by ClusterFuzz suggests additional labels:
    Cr-Blink-DOM, OS-All.
*   Within a day or two, the sheriff was able to get the bug assigned and -- oh
    joy! -- fixed very quickly. The sheriff sees the bug status go to "Fixed" in
    their bug mail and adds: Merge-Approved. Also, they change
    Restrict-View-SecurityTeam to Restrict-View-SecurityNotify.
*   Later that week, the Chrome Security manager does a sweep of all
    reward-topanel bugs. This one gets rewarded, so that one reward label is
    replaced with two: reward-1000 and reward-unpaid. Later, reward-unpaid
    becomes reward-inprocess and is later still removed when all is done. Of
    course, reward-1000 remains forever. (We use it to track total payout!)
*   The next week, a Chrome TPM states that the first Chrome M27 stable patch is
    going on and asks if we want to include security fixes. We do, of course.
    Having had this particular fix "bake" in another M29 dev channel, the Chrome
    Security release manager decides to merge it. **Merge-Approved-M## is
    replaced with ****Merge-Merged-M##}. (IMPORTANT: this transition only occurs
    after the fix is merged to ALL applicable branches, i.e. M28 as well as M27
    in this case.) We now know that users of Chrome stable will get the fix in
    the first M27 stable patch so the following labels are changed / applied:
    **M-27, Release-1. Since the bug was externally reported, it definitely gets
    its own CVE, and a label is eventually added: CVE-2013-31337.
*   Some time after the patch is released to all stable users,
    Restrict-View-SecurityNotify is removed.
