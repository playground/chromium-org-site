# Deprecating Powerful Features on Insecure Origins

We (Chrome Security) originally sent this out to various browser development
mailing lists. See the original [intent-to-deprecate email on
blink-dev](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/2LXKVWYkOus/gT-ZamfwAKsJ).
This is based on the original idea to [prefer secure origins for powerful new
features.](prefer-secure-origins-for-powerful-new-features.md)

**This is a living document** — as we learn more, we'll probably need to change
this page.

## Proposal

We want to start applying the concepts in
<https://w3c.github.io/webappsec-secure-contexts/>to features that have already
shipped and which do not meet the (new, not present at the time) requirements.
In particular, this approximately requires that powerful features only be
accessible on "secure origins" (such as HTTPS) where the [full ancestor chain is
also secure](https://w3c.github.io/webappsec-secure-contexts/#ancestors).

We want to start by requiring secure origins for these existing features:

*   Geolocation — [requires secure origins as of
    M50](https://developers.google.com/web/updates/2016/04/geolocation-on-secure-contexts-only)
*   Device motion / orientation
*   EME
*   *getUserMedia*
*   AppCache
*   Notifications

As with gradually [marking HTTP as non-secure](marking-http-as-non-secure.md),
we expect to gradually migrate these features to secure-only, based on
thresholds of usage, starting with lowest usage and moving towards higher. We
also expect to gradually indicate in the UX that the features are deprecated for
non-secure origins.

The deprecation strategy for each of these features is not decided on and may
very well differ from feature to feature. We don’t currently know what the
thresholds will be, or how heavily used the features are on what kinds of
origins. We are in the process of gathering data, and will report back when we
have it. There are no firm plans at all at this time, other than eventual
deprecation. We intend for this to stimulate a public discussion of the best way
to approach this deprecation. So, to that point, we'd love to hear what the
community thinks.

## Testing a Powerful Feature

If a feature is powerful and not available on HTTP, and you are a developer that
needs to keep testing a feature on a server that does not have a valid
certificate, you have several options:

1.  localhost is treated as a secure origin over HTTP, so if you're able to run
    your server from localhost, you should be able to test the feature on that
    server.
2.  You can run chrome with the
    --unsafely-treat-insecure-origin-as-secure="http://example.com" flag
    (replacing "example.com" with the origin you actually want to test), which
    will treat that origin as secure for this session. Note that you also need
    to include the --user-data-dir=/test/only/profile/dir to create a fresh
    testing profile for the flag to work. Note that on Android and ChromeOS this
    requires having a device with root access/dev mode.
3.  Create a self-signed certificate for temporary testing. This requires
    advancing through an invalid certificate interstitial, which is generally
    not recommended, but testing without inputting sensitive information is a
    reasonable time to do so. Note that because of this interstitial
    click-through, we highly recommend options (1) and (2) instead, but they are
    difficult to do on mobile. See this [post on setting up a self-signed
    certificate for a
    server](https://mail.mozilla.org/pipermail/web-vr-discuss/2016-July/001272.html)
    for more information on how to do this.

We are hoping to come up with better metheds for testing powerful features on
insecure origins, and we'll be sure to update this page once we've developed
them. Feel free to contribute ideas to
[security-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/security-dev).

## Relevant Bugs

*   <https://crbug.com/481604> Add deprecation warnings to console for powerful
    features
*   <https://crbug.com/464874> Remove persistence of powerful features on
    insecure origins
*   <https://crbug.com/520765> General OWP tracking bug for removal of features
*   <https://crbug.com/561641> Remove geolocation from insecure origins
*   <https://crbug.com/588931> Deprecate and remove AppCache on insecure origins
*   <https://crbug.com/672605> Remove support for EME on insecure origins
*   <https://crbug.com/679821> Remove support for Insecure usage of
    Notifications
