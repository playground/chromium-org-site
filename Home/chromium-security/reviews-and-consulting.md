# Reviews and Consulting

So, ya need some security help? Our talents are many, but here are the things we
normally assist Chromium engineers with:

**Security Reviews**

Features that are new or substantial refactors / expansions should have an
associated [Chrome Launch
bug](https://docs.google.com/a/google.com/document/d/1_QHBMEZa1UNbNx7yIK7BGmpAq57SDgWFKMUBIEFha8A/edit)
- creating that will trigger our security review process. The purpose of a
security review is not the elimination of all possible vulnerabilities, but
rather the promotion of [secure design and implementation practices in
Chromium](education/index.md). It also helps the security team stay engaged with
the rest of Chromium engineering.

Here's how the process works:

*   Visit the [security questionnaire landing
    page](https://chrome-security-survey.appspot.com/).
*   If you don't have the "engineering practices" entry for your team, start by
    completing one.
*   [Create a brief project
    questionnaire](https://chrome-security-survey.appspot.com/new_project.html)
    describing your current work.
*   Sit back and relax. Someone on the security team will get back to you.

Important: if you have a good reason for not being able to complete the
questionnaire, or if it's not suitable for what you're working on, feel free to
get in touch with [us](mailto:security@chromium.org) directly and provide a
free-form explanation of what's on your mind. Please don't abuse this: if we end
up having to ask all the same questions that would be answered by the
questionnaire, it's just a waste of everyone's time.

**Adding new code to third_party**

Third party code is a hot spot for security vulnerabilities, so make sure to
[give security a head's up by getting a code
review](http://www.chromium.org/developers/adding-3rd-party-libraries#TOC-Get-a-Review).

**One-off questions**

If you are not sure what to do, or need other help, you can always just reach us
at [security@chromium.org](mailto:security@chromium.org).
