# Audio/Video

Everything you need to know about audio/video inside Chromium and Chromium OS!

### Whom To Contact

It's best to have discussions on chromium-dev@chromium.org.

We are component
[Internals>Media](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3AInternals%3EMedia)
on the Chromium bug tracker.

### Documentation

Here's our outdated [design doc for HTML5
audio/video](../developers/design-documents/video/index.md).

### Codec, Containers and File Extensions Support

Container formats

*   Ogg
*   WebM
*   WAV

*   MP4 \[Google Chrome only\]

Codec formats

Audio

*   FLAC
*   Opus
*   PCM 8-bit unsigned integer
*   PCM 16-bit signed integer little endian
*   PCM 32-bit float little endian
*   Vorbis

*   MP3 \[Google Chrome only\]
*   AAC \[Google Chrome only\]

*   AMR-NB \[Google Chrome OS only\]
*   AMR-WB \[Google Chrome OS only\]
*   PCM μ-law \[Google Chrome OS only\]
*   GSM \[Google Chrome OS Only\]

Video

*   VP8
*   VP9

*   Theora \[Except on Android variants\]

*   H.264 \[Google Chrome only\]

*   MPEG-4 \[Google Chrome OS only\]

File Extensions \[defined in
[src/net/base/mime_util.cc](https://chromium.googlesource.com/chromium/src/+/master/net/base/mime_util.cc)\]

*   .flac
*   .ogv
*   .ogm
*   .ogg
*   .oga
*   .webm
*   .wav

*   .mp4 \[Google Chrome only\]
*   .m4v \[Google Chrome only\]
*   .m4a \[Google Chrome only\]
*   .mp3 \[Google Chrome only\]

*   .amr \[Google Chrome OS only\]
*   .avi \[Google Chrome OS only\]
*   .3gp \[Google Chrome OS only\]

### Code Location

**Chromium**

media/ - Home to all things media!
media/audio - OS audio input/output abstractions
media/video/capture - OS camera input abstraction
media/video - software/hardware video decoder interfaces + implementations
third_party/ffmpeg - Chromium's copy of FFmpeg
third_party/libvpx - Chromium's copy of libvpx

**Blink**

Source/core/html/HTMLMediaElement.{cpp,h,idl} - media element base class

Source/core/html/HTMLAudioElement.{cpp,h,idl} - audio element implementation

Source/core/html/HTMLVideoElement.{cpp,h,idl} - video element implementation

**Particularly Interesting Bits**

net/base/mime_util.cc - defines canPlayType() behaviour and file extension
mapping

media/blink/buffered_data_source.{cc,h} - Chromium's main implementation of
DataSource for the media pipeline

media/blink/buffered_resource_loader.{cc,h} - Implements the sliding window
buffering strategy (see below)

third_party/WebKit/public/platform/WebMediaPlayer.h - Blink's media player
interface for providing HTML5 audio/video functionality

media/blink/webmediaplayer_impl.{cc,h} - Chromium's main implementation of
WebMediaPlayer

### How does everything get instantiated?

WebFrameClient::createMediaPlayer() is the Blink embedder API for creating a
WebMediaPlayer and passing it back to Blink. Every HTML5 audio/video element
will ask the embedder to create a WebMediaPlayer.

For Chromium this is handled in RenderFrameImpl.

### GYP/GN Flags

There are a few GYP/GN flags which can alter the behaviour of Chromium's HTML5
audio/video implementation.

ffmpeg_branding

Overrides which version of FFmpeg to use

Default: $(branding)

Values:

Chrome - includes additional proprietary codecs (MP3, etc..) for use with Google
Chrome

Chromium - builds default set of codecs

proprietary_codecs

Alters the list of codecs Chromium claims to support, which affects <source> and
canPlayType() behaviour

Default: 0(gyp)/false(gn)

Values:

0/false - <source> and canPlayType() assume the default set of codecs

1/true - <source> and canPlayType() assume they support additional proprietary
codecs

LayoutTests

We run the following layout tests:

LayoutTests/media

LayoutTests/http/tests/media

### How the %#$& does buffering work?

Chromium uses a combination of range requests and an in-memory sliding window to
buffer media. We have a low and high watermark that is used to determine when to
purposely stall the HTTP request and when to resume the HTTP request.

It's complicated, so here's a picture:

![image](ChromiumMediaBuffering.png)
