*   [Chromium](Home/_index.md)
*   [Fixing Google Chrome compatibility bugs in websites -
    FAQ](Home/chromecompatfaq.md)
*   [Chromium Clobber Landmines](Home/chromium-clobber-landmines.md)
*   [Chromium Privacy](Home/chromium-privacy.md)
*   [Chromium Security](Home/chromium-security/index.md)
*   [So you want to add a new
    permission?](Home/chromium-security/adding-permissions.md)
*   [Binding Integrity](Home/chromium-security/binding-integrity.md)
*   [BoringSSL](Home/chromium-security/boringssl/index.md)
*   [Contributing to
    BoringSSL](Home/chromium-security/boringssl/contributing.md)
*   [Security Brag Sheet](Home/chromium-security/brag-sheet.md)
*   [Security Bugs--](Home/chromium-security/bugs/index.md)
*   [Clusterfuzz automatic bug
    filing](Home/chromium-security/bugs/automatic-filing.md)
*   [Common Fuzzer
    Mistakes](Home/chromium-security/bugs/common-fuzzer-mistakes.md)
*   [Developing Fuzzers for
    ClusterFuzz](Home/chromium-security/bugs/developing-fuzzers-for-clusterfuzz.md)
*   [Reproducing ClusterFuzz
    bugs](Home/chromium-security/bugs/reproducing-clusterfuzz-bugs.md)
*   [Using ClusterFuzz](Home/chromium-security/bugs/using-clusterfuzz.md)
*   [Certificate
    Transparency](Home/chromium-security/certificate-transparency_index.md)
*   [Certificate Transparency Log
    Policy](Home/chromium-security/certificate-transparency/log-policy/index.md)
*   [Chromium and EMET](Home/chromium-security/chromium-and-emet.md)
*   [Clean Software Alliance
    (CSA)](Home/chromium-security/clean-software-alliance.md)
*   [Technical analysis of client identification
    mechanisms](Home/chromium-security/client-identification-mechanisms.md)
*   [Core Principles](Home/chromium-security/core-principles.md)
*   [CRLSets](Home/chromium-security/crlsets.md)
*   [Deprecating Permissions in Cross-Origin
    Iframes](Home/chromium-security/deprecating-permissions-in-cross-origin-iframes.md)
*   [Deprecating Powerful Features on Insecure
    Origins](Home/chromium-security/deprecating-powerful-features-on-insecure-origins.md)
*   [Education](Home/chromium-security/education/index.md)
*   [Security Tips for Developing
    CRX](Home/chromium-security/education/security-tips-for-crx-and-apps.md)
*   [Security Tips for
    IPC](Home/chromium-security/education/security-tips-for-ipc.md)
*   [TLS / SSL](Home/chromium-security/education/tls/index.md)
*   [A further update on SHA-1 certificates in
    Chrome](Home/chromium-security/education/tls/sha-1.md)
*   [Security UX](Home/chromium-security/enamel/index.md)
*   [Goals For The Origin Info
    Bubble](Home/chromium-security/enamel/goals-for-the-origin-info-bubble.md)
*   [Permissions](Home/chromium-security/enamel/permissions.md)
*   [Restricting IFrame
    Permissions](Home/chromium-security/enamel/restricting-iframe-permissions.md)
*   [Secure Architecture](Home/chromium-security/guts/index.md)
*   [Security Hall of Fame](Home/chromium-security/hall-of-fame.md)
*   [IPC Security Reviews](Home/chromium-security/ipc-security-reviews.md)
*   [Malicious Extensions
    Protection](Home/chromium-security/malicious-extensions-protection.md)
*   [Marking HTTP As
    Non-Secure](Home/chromium-security/marking-http-as-non-secure.md)
*   [Web Platform Security](Home/chromium-security/owp/index.md)
*   [Web Platform Security
    backlog](Home/chromium-security/owp/web-platform-security-backlog.md)
*   [PDFium Security](Home/chromium-security/pdfium-security.md)
*   [Prefer Secure Origins For Powerful New
    Features](Home/chromium-security/prefer-secure-origins-for-powerful-new-features.md)
*   [Pwnium 2](Home/chromium-security/pwnium-2.md)
*   [Pwnium 3](Home/chromium-security/pwnium-3.md)
*   [Pwnium 4](Home/chromium-security/pwnium-4.md)
*   [Quarterly Updates](Home/chromium-security/quarterly-updates.md)
*   [Reporting Security Bugs](Home/chromium-security/reporting-security-bugs.md)
*   [Reviews and Consulting](Home/chromium-security/reviews-and-consulting.md)
*   [Root Certificate Policy](Home/chromium-security/root-ca-policy/index.md)
*   [Security Bug Lifecycle](Home/chromium-security/security-bug-lifecycle.md)
*   [Security FAQ](Home/chromium-security/security-faq/index.md)
*   [Service Worker Security
    FAQ](Home/chromium-security/security-faq/service-worker-security-faq.md)
*   [Security labels/components](Home/chromium-security/security-labels.md)
*   [Security Release
    Management](Home/chromium-security/security-release-management.md)
*   [Chrome Security Reviews](Home/chromium-security/security-reviews.md)
*   [Security Sheriff](Home/chromium-security/security-sheriff/index.md)
*   [Vulnerability Rewards
    Program](Home/chromium-security/vulnerability-rewards-program.md)
*   [debugging-on-webview](Home/debugging-on-webview.md)
*   [DOMUI Testing](Home/index.md)
*   [WebUI browser_tests](Home/domui-testing/webui-browser_tests/index.md)
*   [WebUI accessibility
    audit](Home/domui-testing/webui-browser_tests/webui-accessibility-audit.md)
*   [Ice Cream Sandwich Support
    Deprecation](Home/ice-cream-sandwich-support-deprecation-faq.md)
*   [Loading](Home/loading/index.md)
*   [clovis](Home/loading/clovis.md)
*   [Memory! Landing page for information on memory efforts and tools in
    Chromium](Home/memory.md)
*   [Third Party Developers](Home/third-party-developers.md)
*   [User Metrics](Home/user-metrics.md)
*   [Documentation for Administrators](administrators/index.md)
*   [Advanced Integration for SAML SSO on Chrome
    Devices](administrators/advanced-integration-for-saml-sso-on-chrome-devices.md)
*   [Developer Guide: Certificate Management Extension API on Chrome
    OS](administrators/certificate-management-extension-api-on-chrome-os.md)
*   [Common Problems and
    Solutions](administrators/common-problems-and-solutions.md)
*   [Complex policies on
    Windows](administrators/complex-policies-on-windows/index.md)
*   [Configuring Other
    Preferences](administrators/configuring-other-preferences.md)
*   [Configuring Apps and Extensions by
    Policy](administrators/configuring-policy-for-extensions.md)
*   [Diagnostic Mode](administrators/diagnostic-mode/index.md)
*   [ESET
    Anti-Virus](administrators/err_eset_anti_virus_ssl_interception/index.md)
*   [ERR_SSL_WEAK_SERVER_EPHEMERAL_DH_KEY](administrators/err_ssl_weak_server_ephemeral_dh_key/index.md)
*   [Frequently Asked Questions](administrators/frequently-asked-questions.md)
*   [Google Chrome Installation](administrators/installation.md)
*   [iOS MDM Policy Format](administrators/ios-mdm-policy-format/index.md)
*   [Linux Quick Start](administrators/linux-quick-start.md)
*   [Mac Quick Start](administrators/mac-quick-start.md)
*   [Policy List](administrators/policy-list-3/index.md)
*   [Supported Directory
    Variables](administrators/policy-list-3/user-data-directory-variables.md)
*   [Policy Templates](administrators/policy-templates.md)
*   [Pre-installed Extensions](administrators/pre-installed-extensions.md)
*   [Turning Off Auto Updates in Google
    Chrome](administrators/turning-off-auto-updates.md)
*   [URL Blacklist filter format](administrators/url-blacklist-filter-format.md)
*   [Windows Quick Start](administrators/windows-quick-start/index.md)
*   [Android](android/index.md)
*   [Contributing to drm_hwcomposer](android/contributing-to-drm_hwcomposer.md)
*   [drm_hwcomposer Overview](android/drm_hwcomposer-overview.md)
*   [Audio/Video](audio-video/index.md)
*   [Media Internals](audio-video/media-internals.md)
*   [Blink](blink/index.md)
*   [ActiveDOMObject](blink/activedomobject.md)
*   [Animation Team](blink/animation-team/index.md)
*   [Animation Team Resources](blink/animation-team/resources.md)
*   [Blink gardening](blink/blink-gardening.md)
*   [Garbage Collection for Blink C++ objects (a.k.a.
    Oilpan)](blink/blink-gc.md)
*   [Blink-in-JavaScript](blink/blink-in-js.md)
*   [Blink Networking APIs](blink/blink-network-stack.md)
*   [Blink post-merge FAQ](blink/blink-post-merge-faq.md)
*   [Blink, Testing, and the W3C](blink/blink-testing-and-the-w3c.md)
*   [Component:Blink bug labeling rotation](blink/blink-triaging.md)
*   [Blink Coding Style Guidelines](blink/coding-style/index.md)
*   [Layout Test Style
    Guidelines](blink/coding-style/layout-test-style-guidelines.md)
*   [Conversion Cheatsheet](blink/conversion-cheatsheet.md)
*   [Deprecating Features](blink/deprecating-features.md)
*   [Developer FAQ](blink/developer-faq/index.md)
*   [Directory dependency in Blink (Still
    DRAFT)](blink/directory-dependency-in-blink/index.md)
*   [DOM Exceptions](blink/dom-exceptions.md)
*   [Getting Started with Blink
    Debugging](blink/getting-started-with-blink-debugging.md)
*   [How repaint works](blink/how-repaint-works.md)
*   [Working with web-platform-tests in blink](blink/importing-the-w3c-tests.md)
*   ["Intent to {Implement,Ship}" Security
    Triage](blink/intent-security-triage.md)
*   [Launching Features](blink/launching-features.md)
*   [Memory Team](blink/memory-team.md)
*   [Origin Trials](blink/origin-trials/index.md)
*   [Running an Origin Trial](blink/origin-trials/running-an-origin-trial.md)
*   [Web Platform Predictability](blink/platform-predictability/index.md)
*   [Web compat analysis tools](blink/platform-predictability/compat-tools.md)
*   [Objectives](blink/platform-predictability/objectives.md)
*   [Public C++ API](blink/public-c-api.md)
*   [Breaking changes](blink/removing-features.md)
*   [Runtime Enabled Features](blink/runtime-enabled-features.md)
*   [Service workers](blink/serviceworker/index.md)
*   [Getting Started](blink/serviceworker/getting-started.md)
*   [Service Worker Debugging](blink/serviceworker/service-worker-faq/index.md)
*   [Service Worker Testing](blink/serviceworker/testing.md)
*   [Handling Blink failures](blink/sheriffing/index.md)
*   [Triaging Gasper Alerts](blink/sheriffing/triaging-gasper-alerts.md)
*   [Slimming Paint (a.k.a. Redesigning Painting and
    Compositing)](blink/slimming-paint/index.md)
*   [Historical documents](blink/slimming-paint/historical-documents.md)
*   [Try bots](blink/slimming-paint/try-bots.md)
*   [Unit Testing in Blink](blink/unittesting/index.md)
*   [V8 Bindings, Promises](blink/v8-bindings.md)
*   [Web Workers](blink/web-workers.md)
*   [Web Components Project](blink/webcomponents-team.md)
*   [WebCrypto](blink/webcrypto.md)
*   [Web IDL in Blink](blink/webidl/index.md)
*   [Blink IDL Extended
    Attributes](blink/webidl/blink-idl-extended-attributes.md)
*   [When will a fix ship in Chrome (stable or
    canary)?](blink/when-will-a-fix-ship-in-chrome-stable-or-canary/index.md)
*   [Chrome Release Channels](chrome-release-channels.md)
*   [Chromium OS](chromium-os/index.md)
*   [2014 Firmware Summit](chromium-os/2014-firmware-summit/index.md)
*   [Optimize Android Apps for Chromebooks](chromium-os/android-apps.md)
*   [boot-complete.conf](chromium-os/boot-complete-conf.md)
*   [Boot Milestones](chromium-os/boot-milestones.md)
*   [Chromium OS Build](chromium-os/build/index.md)
*   [Builder Overview](chromium-os/build/builder-overview.md)
*   [Per-repo and per-directory configuration of CQ and
    pre-CQ](chromium-os/build/bypassing-tests-on-a-per-project-basis.md)
*   [C++ exception support](chromium-os/build/c-exception-support.md)
*   [cbuildbot: Tutorial on adding data to
    metadata.json](chromium-os/build/cbuildbot-metadata-json-tutorial.md)
*   [Anatomy of a cbuildbot build](chromium-os/build/cbuildbot-overview.md)
*   [Writing multiprocess programs in
    Python](chromium-os/build/chromite-parallel.md)
*   [chroot_version_hooks](chromium-os/build/chroot_version_hooks.md)
*   [Commit Queue Stats](chromium-os/build/cq-stats.md)
*   [Cros Deploy](chromium-os/build/cros-deploy.md)
*   [Cros Flash](chromium-os/build/cros-flash.md)
*   [Build FAQ](chromium-os/build/faq.md)
*   [Google Storage Utility (gsutil) FAQ](chromium-os/build/gsutil-faq.md)
*   [Improving Build Times](chromium-os/build/improving-build-times.md)
*   [Chromium OS Local Trybots](chromium-os/build/local-trybot-documentation.md)
*   [Presubmit Hooks](chromium-os/build/presubmit-hooks.md)
*   [Chromium OS SDK Creation](chromium-os/build/sdk-creation.md)
*   [Waterfall Tour](chromium-os/build/tour-of-the-chromiumos-buildbot.md)
*   [Updating our version of
    \`repo\`](chromium-os/build/updating-our-version-of-repo.md)
*   [Chromium OS Remote Trybots](chromium-os/build/using-remote-trybots.md)
*   [Waterfall Tour](chromium-os/build/waterfall-tour.md)
*   [USB Type-C Cable and Adapter Tips and
    Tricks](chromium-os/cable-and-adapter-tips-and-tricks/index.md)
*   [Chrome OS Systems Supporting Android
    Apps](chromium-os/chrome-os-systems-supporting-android-apps.md)
*   [Chrome OS Graphics Reading
    List](chromium-os/chromeos-graphics-reading-list.md)
*   [Chromium OS FAQ](chromium-os/chromium-os-faq.md)
*   [Design Documents](chromium-os/chromiumos-design-docs/index.md)
*   [Autoupdate
    Details](chromium-os/chromiumos-design-docs/autoupdate-details/index.md)
*   [Chrome OS User-Land Boot
    Design](chromium-os/chromiumos-design-docs/boot-design/index.md)
*   [Chrome OS Volume
    Keys](chromium-os/chromiumos-design-docs/chrome-os-volume-keys.md)
*   [Chromium OS
    Cgroups](chromium-os/chromiumos-design-docs/chromium-os-cgroups.md)
*   [Kernel
    Design](chromium-os/chromiumos-design-docs/chromium-os-kernel/index.md)
*   [Documented Certificate of
    Ownership](chromium-os/chromiumos-design-docs/chromium-os-kernel/dco.md)
*   [Submitting
    Patches](chromium-os/chromiumos-design-docs/chromium-os-kernel/submitting-patches.md)
*   [Cros - How Chromium talks to Chromium
    OS](chromium-os/chromiumos-design-docs/chromium-os-libcros.md)
*   [Chromium OS
    Printing](chromium-os/chromiumos-design-docs/chromium-os-printing-design.md)
*   [CRAS: Chromium OS Audio
    Server](chromium-os/chromiumos-design-docs/cras-chromeos-audio-server.md)
*   [CrOS network: notes on ChromiumOS
    networking](chromium-os/chromiumos-design-docs/cros-network/index.md)
*   [Cellular Activation (and Chrome/CrOS network management
    API)](chromium-os/chromiumos-design-docs/cros-network/cellular-activation.md)
*   [chrome network
    debugging](chromium-os/chromiumos-design-docs/cros-network/chrome-network-debugging.md)
*   [fake-cromo](chromium-os/chromiumos-design-docs/cros-network/fake-cromo.md)
*   [fake-gsm-modem](chromium-os/chromiumos-design-docs/cros-network/fake-gsm-modem.md)
*   [fakemodem (for testing celluar modems with AT command
    interfaces)](chromium-os/chromiumos-design-docs/cros-network/fakemodem.md)
*   [Developer Mode](chromium-os/chromiumos-design-docs/developer-mode.md)
*   [Developer Shell
    Access](chromium-os/chromiumos-design-docs/developer-shell-access.md)
*   [Disk Format](chromium-os/chromiumos-design-docs/disk-format/index.md)
*   [EC-3PO: The EC console
    interpreter](chromium-os/chromiumos-design-docs/ec-3po/index.md)
*   [File
    System/Autoupdate](chromium-os/chromiumos-design-docs/filesystem-autoupdate/index.md)
*   [File System/Autoupdate
    Supplements](chromium-os/chromiumos-design-docs/filesystem-autoupdate-supplements/index.md)
*   [Firmware Boot and
    Recovery](chromium-os/chromiumos-design-docs/firmware-boot-and-recovery/index.md)
*   [Firmware Updates](chromium-os/chromiumos-design-docs/firmware-updates.md)
*   [Hardening against malicious stateful
    data](chromium-os/chromiumos-design-docs/hardening-against-malicious-stateful-data.md)
*   [Keyboard-Controlled Reset
    Circuit](chromium-os/chromiumos-design-docs/keyboard-controlled-reset-circuit.md)
*   [Library
    Optimization](chromium-os/chromiumos-design-docs/library-optimization.md)
*   [Login](chromium-os/chromiumos-design-docs/login.md)
*   [Lucid Sleep](chromium-os/chromiumos-design-docs/lucid-sleep.md)
*   [Network Portal
    Detection](chromium-os/chromiumos-design-docs/network-portal-detection/index.md)
*   [onc](chromium-os/chromiumos-design-docs/onc.md)
*   [Open Network
    Configuration](chromium-os/chromiumos-design-docs/open-network-configuration.md)
*   [Out of memory
    handling](chromium-os/chromiumos-design-docs/out-of-memory-handling/index.md)
*   [Partition
    Resizing](chromium-os/chromiumos-design-docs/partition-resizing/index.md)
*   [Powerwash](chromium-os/chromiumos-design-docs/powerwash.md)
*   [Protecting Cached User
    Data](chromium-os/chromiumos-design-docs/protecting-cached-user-data/index.md)
*   [Recovery Mode](chromium-os/chromiumos-design-docs/recovery-mode/index.md)
*   [Security Overview](chromium-os/chromiumos-design-docs/security-overview.md)
*   [Software
    Architecture](chromium-os/chromiumos-design-docs/software-architecture/index.md)
*   [Source Code
    Management](chromium-os/chromiumos-design-docs/source-code-management/index.md)
*   [System Hardening](chromium-os/chromiumos-design-docs/system-hardening.md)
*   [system
    notifications](chromium-os/chromiumos-design-docs/system-notifications.md)
*   [Tab Discarding and
    Reloading](chromium-os/chromiumos-design-docs/tab-discarding-and-reloading.md)
*   [Text Input](chromium-os/chromiumos-design-docs/text-input/index.md)
*   [Syncing Languages and Input
    Methods](chromium-os/chromiumos-design-docs/text-input/syncing-input-methods.md)
*   [Touch Firmware
    Updater](chromium-os/chromiumos-design-docs/touch-firmware-updater.md)
*   [Upstream First](chromium-os/chromiumos-design-docs/upstream-first.md)
*   [User Accounts and
    Management](chromium-os/chromiumos-design-docs/user-accounts-and-management.md)
*   [Userland Boot in Chrome
    OS](chromium-os/chromiumos-design-docs/userland-boot.md)
*   [Verified Boot](chromium-os/chromiumos-design-docs/verified-boot/index.md)
*   [Firmware Verified Boot Crypto
    Specification](chromium-os/chromiumos-design-docs/verified-boot-crypto/index.md)
*   [Verified Boot Data
    Structures](chromium-os/chromiumos-design-docs/verified-boot-data-structures/index.md)
*   [Design proposal: Verify prebuilts using content
    hashing](chromium-os/chromiumos-design-docs/verify-prebuilts-using-content-hashing.md)
*   [Chromium OS Developer Guide](chromium-os/developer-guide/index.md)
*   [Building for Beaglebone](chromium-os/developer-guide/beaglebone.md)
*   [Chromium OS
    Sandboxing](chromium-os/developer-guide/chromium-os-sandboxing.md)
*   [Simple developer
    workflow](chromium-os/developer-guide/developer-workflow.md)
*   [Directory Structure (Chromium
    OS)](chromium-os/developer-guide/directory-structure.md)
*   [Disk Layout Format](chromium-os/developer-guide/disk-layout-format.md)
*   [Gerrit credentials setup (for Chromium OS and Chrome
    OS)](chromium-os/developer-guide/gerrit-guide/index.md)
*   [Setting up your Account on
    gerrit.chromium.org](chromium-os/developer-guide/gerrit-guide/legacy.md)
*   [Go in Chromium OS](chromium-os/developer-guide/go-in-chromium-os.md)
*   [Using Chromium OS SDK as standalone,
    DIY](chromium-os/developer-guide/using-sdk-standalone.md)
*   [Controlling Enabled
    Consoles](chromium-os/developer-guide/using-serial-tty.md)
*   [Developer Information for Chrome OS
    Devices](chromium-os/developer-information-for-chrome-os-devices/index.md)
*   [Acer AC700
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/acer-ac700-chromebook/index.md)
*   [Acer C670 Chromebook
    11](chromium-os/developer-information-for-chrome-os-devices/acer-c670-chromebook/index.md)
*   [Acer C7
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/acer-c7-chromebook/index.md)
*   [Acer C720 & C720P & C740
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/acer-c720-chromebook/index.md)
*   [Acer C910 Chromebook
    15](chromium-os/developer-information-for-chrome-os-devices/acer-c910-chromebook/index.md)
*   [Acer CB5-311 Chromebook
    13](chromium-os/developer-information-for-chrome-os-devices/acer-cb5-311-chromebook-13/index.md)
*   [Asus
    Chromebox](chromium-os/developer-information-for-chrome-os-devices/asus-chromebox.md)
*   [Chromebook Pixel
    (2013)](chromium-os/developer-information-for-chrome-os-devices/chromebook-pixel/index.md)
*   [Chromebook Pixel
    (2015)](chromium-os/developer-information-for-chrome-os-devices/chromebook-pixel-2015.md)
*   [Cr-48 Chrome Notebook Developer
    Information](chromium-os/developer-information-for-chrome-os-devices/cr-48-chrome-notebook-developer-information/index.md)
*   [How to boot Ubuntu on a
    Cr-48](chromium-os/developer-information-for-chrome-os-devices/cr-48-chrome-notebook-developer-information/how-to-boot-ubuntu-on-a-cr-48.md)
*   [Custom
    Firmware](chromium-os/developer-information-for-chrome-os-devices/custom-firmware.md)
*   [Dell Chromebook
    11](chromium-os/developer-information-for-chrome-os-devices/dell-chromebook-11/index.md)
*   [Generic Chrome OS Device
    Instructions](chromium-os/developer-information-for-chrome-os-devices/generic.md)
*   [Insyde H2O
    Firmware](chromium-os/developer-information-for-chrome-os-devices/h2c-firmware.md)
*   [HP Chromebook
    11](chromium-os/developer-information-for-chrome-os-devices/hp-chromebook-11.md)
*   [HP Chromebook
    14](chromium-os/developer-information-for-chrome-os-devices/hp-chromebook-14/index.md)
*   [HP
    Chromebox](chromium-os/developer-information-for-chrome-os-devices/hp-chromebox.md)
*   [HP Pavilion
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/hp-pavilion-14-chromebook/index.md)
*   [Lenovo Chromebook
    11](chromium-os/developer-information-for-chrome-os-devices/lenovo-chromebook-11.md)
*   [Lenovo Chromebook N20 &
    N20P](chromium-os/developer-information-for-chrome-os-devices/lenovo-chromebook-n20/index.md)
*   [Lenovo Thinkpad X131e
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/lenovo-thinkpad-x131e-chromebook/index.md)
*   [LG Chromebase
    22CV241-W](chromium-os/developer-information-for-chrome-os-devices/lg-chromebase-22cv241-w/index.md)
*   [Running virtual machines on your
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/running-virtual-machines-on-your-chromebook.md)
*   [Samsung ARM
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/samsung-arm-chromebook/index.md)
*   [Samsung Chromebook
    2](chromium-os/developer-information-for-chrome-os-devices/samsung-chromebook-2.md)
*   [Samsung Series 5 550 Chromebook and Series 3
    Chromebox](chromium-os/developer-information-for-chrome-os-devices/samsung-sandy-bridge/index.md)
*   [Hacking VMX Support Into Coreboot (lumpy &
    stumpy)](chromium-os/developer-information-for-chrome-os-devices/samsung-sandy-bridge/coreboot-vmx-hack.md)
*   [Samsung Series 5
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/samsung-series-5-chromebook/index.md)
*   [Toshiba CB30
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/toshiba-cb30-chromebook/index.md)
*   [TP-LINK OnHub
    TGR1900](chromium-os/developer-information-for-chrome-os-devices/tp-link-onhub-tgr1900/index.md)
*   [Upstream coreboot on Intel Haswell
    Chromebook](chromium-os/developer-information-for-chrome-os-devices/upstream-coreboot-on-intel-haswell-chromebook.md)
*   [Workaround for battery discharge in
    dev-mode](chromium-os/developer-information-for-chrome-os-devices/workaround-for-battery-discharge-in-dev-mode.md)
*   [Developing apps on your
    Chromebook](chromium-os/developing-apps-on-your-chromium-os-device.md)
*   [USB Type-C to DP Adapter](chromium-os/dingdong/index.md)
*   [Technical Discussion Groups for Chromium
    OS](chromium-os/discussion-groups.md)
*   [Chromium Embedded Controller (EC)
    Development](chromium-os/ec-development/index.md)
*   [AP / EC communication](chromium-os/ec-development/ap-ec-communication.md)
*   [EC-3PO](chromium-os/ec-development/ec-3po.md)
*   [EC Image Geometry
    Spec](chromium-os/ec-development/ec-image-geometry-spec/index.md)
*   [Get Started Building EC Images
    (Quickly)](chromium-os/ec-development/getting-started-building-ec-images-quickly.md)
*   [USB-C / PD Low Power
    Boot](chromium-os/ec-development/usb-low-power-boot/index.md)
*   [Energy Aware Scheduling](chromium-os/energy-aware-scheduling.md)
*   [External BSP Hosting](chromium-os/external-bsp-hosting.md)
*   [Extracting a Recovery Kernel from a Recovery
    Image](chromium-os/extracting-a-recovery-kernel-from-a-recovery-image.md)
*   [Firmware Overview and Porting
    Guide](chromium-os/firmware-porting-guide/index.md)
*   [1. Overview of the Porting
    Process](chromium-os/firmware-porting-guide/1-overview/index.md)
*   [The crosfw
    script](chromium-os/firmware-porting-guide/1-overview/the-crosfw-script.md)
*   [2. Concepts](chromium-os/firmware-porting-guide/2-concepts/index.md)
*   [FMAP](chromium-os/firmware-porting-guide/fmap.md)
*   [3. U-Boot Drivers](chromium-os/firmware-porting-guide/u-boot-drivers.md)
*   [Appendix A: Using nv-U-Boot on the Samsung ARM
    Chromebook](chromium-os/firmware-porting-guide/using-nv-u-boot-on-the-samsung-arm-chromebook.md)
*   [Firmware Summit 2014](chromium-os/firmware-summit.md)
*   [Firmware Update](chromium-os/firmware-update.md)
*   [How to force the out-of-box experience
    (OOBE)](chromium-os/force-out-of-box-experience-oobe.md)
*   [Firmware Management Parameters](chromium-os/fwmp.md)
*   [Portage New & Upgrade Package
    Process](chromium-os/gentoo-package-upgrade-process.md)
*   [Gerrit for Chromium OS devs](chromium-os/gerrit-for-chromium-os-devs.md)
*   [Getting Developer Hardware](chromium-os/getting-dev-hardware_index.md)
*   [Developer
    Hardware](chromium-os/getting-dev-hardware/dev-hardware-list/index.md)
*   [Getting Involved](chromium-os/getting-involved-in-chromium-os.md)
*   [Getting started with
    glmark2](chromium-os/getting-started-with-glmark2/index.md)
*   [Getting started with
    Platform2](chromium-os/getting-started-with-platform2.md)
*   [GFX test notes](chromium-os/gfx-test-notes.md)
*   [USB Type-C to HDMI Adapter](chromium-os/hoho/index.md)
*   [How Tos and
    Troubleshooting](chromium-os/how-tos-and-troubleshooting/index.md)
*   [Timechart
    how-to](chromium-os/how-tos-and-troubleshooting/a-brief-perf-how-to/index.md)
*   [Adding a New
    Package](chromium-os/how-tos-and-troubleshooting/add-a-new-package.md)
*   [Boot mainline kernel on
    veyron-jaq](chromium-os/how-tos-and-troubleshooting/boot-mainline-kernel-on-veyron-jaq/index.md)
*   [Building Chromium for Chromium OS (simple
    chrome)](chromium-os/how-tos-and-troubleshooting/building-chromium-browser/index.md)
*   [Chrome API keys in the Chromium OS SDK
    chroot](chromium-os/how-tos-and-troubleshooting/building-chromium-browser/chrome-api-keys-in-the-chroot.md)
*   [Factory
    Software](chromium-os/how-tos-and-troubleshooting/building-factory-test-images.md)
*   [Chrome with libcras on
    gPrecise](chromium-os/how-tos-and-troubleshooting/chrome-with-libcras-on-gprecise.md)
*   [Chromium OS Architecture Porting
    Guide](chromium-os/how-tos-and-troubleshooting/chromiumos-architecture-porting-guide.md)
*   [Chromium OS Board Porting
    Guide](chromium-os/how-tos-and-troubleshooting/chromiumos-board-porting-guide/index.md)
*   [Creating Private Board
    Overlays](chromium-os/how-tos-and-troubleshooting/chromiumos-board-porting-guide/private-boards.md)
*   [Create a CL](chromium-os/how-tos-and-troubleshooting/create-a-cl.md)
*   [Creating Local
    Mirrors](chromium-os/how-tos-and-troubleshooting/creating-local-mirrors.md)
*   [Debugging a cellular
    modem](chromium-os/how-tos-and-troubleshooting/debugging-3g/index.md)
*   [LTE: Manual E362
    setup](chromium-os/how-tos-and-troubleshooting/debugging-3g/manual-e362-setup.md)
*   [Modem debugging with mmcli (from the modemmanager-next
    package)](chromium-os/how-tos-and-troubleshooting/debugging-3g/modem-debugging-with-mmcli.md)
*   [Debugging
    Features](chromium-os/how-tos-and-troubleshooting/debugging-features.md)
*   [Helping debug system
    hangs](chromium-os/how-tos-and-troubleshooting/debugging-hangs.md)
*   [Chromium on Chromium OS Debugging
    Tips](chromium-os/how-tos-and-troubleshooting/debugging-tips/index.md)
*   [Host File
    Access](chromium-os/how-tos-and-troubleshooting/debugging-tips/host-file-access.md)
*   [Setting up
    tcpdump](chromium-os/how-tos-and-troubleshooting/debugging-tips/setting-up-tcpdump.md)
*   [Target Serial
    Access](chromium-os/how-tos-and-troubleshooting/debugging-tips/target-serial-access.md)
*   [High-Level Developer
    FAQ](chromium-os/how-tos-and-troubleshooting/developer-faq.md)
*   [Git Client-Side
    FAQ](chromium-os/how-tos-and-troubleshooting/gclient-and-git-faq.md)
*   [Gerrit Dependent
    Changes](chromium-os/how-tos-and-troubleshooting/gerrit-dependent-changes.md)
*   [Cellular modem
    FAQ](chromium-os/how-tos-and-troubleshooting/get-a-3g-modem-running/index.md)
*   [Git documentation
    elsewhere](chromium-os/how-tos-and-troubleshooting/git-helpers.md)
*   [Git server-side
    information](chromium-os/how-tos-and-troubleshooting/git-server-side-information.md)
*   [Git
    Troubleshooting](chromium-os/how-tos-and-troubleshooting/git-troubleshooting.md)
*   [Helper Scripts](chromium-os/how-tos-and-troubleshooting/helper-scripts.md)
*   [Dev-Install: Installing Developer and Test packages onto a Chrome OS
    device](chromium-os/how-tos-and-troubleshooting/install-software-on-base-images.md)
*   [Kernel
    Configuration](chromium-os/how-tos-and-troubleshooting/kernel-configuration/index.md)
*   [Kernel FAQ](chromium-os/how-tos-and-troubleshooting/kernel-faq.md)
*   [Kernel rebase
    notes](chromium-os/how-tos-and-troubleshooting/kernel-rebase-notes.md)
*   [Chrome OS build with LLVM Clang and
    ASAN](chromium-os/how-tos-and-troubleshooting/llvm-clang-build.md)
*   [Tools for Measuring Boot Time
    Performance](chromium-os/how-tos-and-troubleshooting/measuring-boot-time-performance.md)
*   [ModemManager care and
    feeding](chromium-os/how-tos-and-troubleshooting/modemmanager.md)
*   [Network-based
    Development](chromium-os/how-tos-and-troubleshooting/network-based-development.md)
*   [NFS-quickstart](chromium-os/how-tos-and-troubleshooting/nfs-quickstart.md)
*   [OpenVPN Manual
    Setup](chromium-os/how-tos-and-troubleshooting/openvpn-manual-setup.md)
*   [EBuild FAQ](chromium-os/how-tos-and-troubleshooting/portage-build-faq.md)
*   [Kernel Crash Logs in Chromium
    OS](chromium-os/how-tos-and-troubleshooting/ramoops-in-chromium-os.md)
*   [Remote Debugging in
    ChromiumOS](chromium-os/how-tos-and-troubleshooting/remote-debugging.md)
*   [Running a Chromium OS image under
    KVM](chromium-os/how-tos-and-troubleshooting/running-chromeos-image-under-virtual-machines/index.md)
*   [Chromium OS \[serial\] Console Debugging
    HOWTO](chromium-os/how-tos-and-troubleshooting/serial-debugging-howto.md)
*   [TSC
    resynchronization](chromium-os/how-tos-and-troubleshooting/tsc-resynchronization.md)
*   [Upgrade Ebuild EAPIs
    HOWTO](chromium-os/how-tos-and-troubleshooting/upgrade-ebuild-eapis.md)
*   [Using an Upstream Kernel on Chrome
    OS](chromium-os/how-tos-and-troubleshooting/using-an-upstream-kernel-on-snow.md)
*   [Dev
    server](chromium-os/how-tos-and-troubleshooting/using-the-dev-server/index.md)
*   [XBuddy for
    Devserver](chromium-os/how-tos-and-troubleshooting/using-the-dev-server/xbuddy-for-devserver.md)
*   [Visualizing the
    Rootfs](chromium-os/how-tos-and-troubleshooting/visualizing-the-rootfs/index.md)
*   [Working on a Chromium OS
    Branch](chromium-os/how-tos-and-troubleshooting/working-on-a-branch.md)
*   [Working with your Overlay
    FAQ](chromium-os/how-tos-and-troubleshooting/working-with-your-overlay-faq.md)
*   [Licenses](chromium-os/licenses.md)
*   [Licensing in Chromium OS](chromium-os/licensing/index.md)
*   [Licensing for Chromium OS
    Developers](chromium-os/licensing-for-chromiumos-developers.md)
*   [Licensing for Chromium OS Package
    Owners](chromium-os/licensing-for-chromiumos-package-owners.md)
*   [Licensing Handling For OS
    Builders](chromium-os/licensing/building-a-distro.md)
*   [45W USB Type-C charging adapter control
    board](chromium-os/minimuffin/index.md)
*   [monitoring tools](chromium-os/monitoring-tools/index.md)
*   [packages](chromium-os/packages/index.md)
*   [biod: Biometrics Daemon](chromium-os/packages/biod/index.md)
*   [Crash Reporting (Chrome OS
    System)](chromium-os/packages/crash-reporting/index.md)
*   [Debugging a
    Minidump](chromium-os/packages/crash-reporting/debugging-a-minidump.md)
*   [Crash Reporting FAQ](chromium-os/packages/crash-reporting/faq.md)
*   [Stack Traces From Chrome OS Crash
    Dumps](chromium-os/packages/crash-reporting/stack-traces-from-chrome-os-crash-dumps.md)
*   [flashrom](chromium-os/packages/cros-flashrom.md)
*   [Implicit System Dependencies](chromium-os/packages/implicit-system.md)
*   [libchrome](chromium-os/packages/libchrome.md)
*   [libchromeos](chromium-os/packages/libchromeos.md)
*   [portage: the Gentoo package manager (aka
    emerge)](chromium-os/packages/portage.md)
*   [Power Manager](chromium-os/packages/power_manager/index.md)
*   [Battery
    Notifications](chromium-os/packages/power_manager/battery-notifications.md)
*   [Inactivity Delays](chromium-os/packages/power_manager/inactivity-delays.md)
*   [Input](chromium-os/packages/power_manager/input.md)
*   [Keyboard
    Backlight](chromium-os/packages/power_manager/keyboard-backlight.md)
*   [Screen Brightness](chromium-os/packages/power_manager/screen-brightness.md)
*   [Suspend and
    Resume](chromium-os/packages/power_manager/suspend-and-resume/index.md)
*   [pydevi2c](chromium-os/packages/pydevi2c.md)
*   [thd](chromium-os/packages/thd.md)
*   [USB Type-C functional testing board](chromium-os/plankton/index.md)
*   [The Chromium OS Platform](chromium-os/platform.md)
*   [Poking around your Chrome OS
    Device](chromium-os/poking-around-your-chrome-os-device/index.md)
*   [Portage Package Upgrade Initiative](chromium-os/portage-package-status.md)
*   [Profiling in Chromium OS](chromium-os/profiling-in-chromeos.md)
*   [python-mock](chromium-os/python-mock.md)
*   [Python Style Guidelines](chromium-os/python-style-guidelines.md)
*   [random notes by quiche@](chromium-os/quiche-notes.md)
*   [Quick Start Guide](chromium-os/quick-start-guide.md)
*   [Recall](chromium-os/recall.md)
*   [Servo](chromium-os/servo/index.md)
*   [Shell Style Guidelines](chromium-os/shell-style-guidelines.md)
*   [Sideload Android apps on your
    Chromebook](chromium-os/sideload-android-apps-on-chromebook.md)
*   [Testing Home](chromium-os/testing/_index.md)
*   [Chromium OS Unit
    Testing](chromium-os/testing/adding-unit-tests-to-the-build.md)
*   [AFE RPC Infrastructure](chromium-os/testing/afe-rpc-infrastructure.md)
*   [Anatomy of test.test](chromium-os/testing/anatomy-of-test-test.md)
*   [arcplusplus-testing](chromium-os/testing/arcplusplus-testing/index.md)
*   [Autoserv Packaging](chromium-os/testing/autoserv-packaging.md)
*   [Autotest and Ebuilds](chromium-os/testing/autotest-and-ebuilds.md)
*   [Autotest Design Patterns](chromium-os/testing/autotest-design-patterns.md)
*   [Autotest Developer
    FAQ](chromium-os/testing/autotest-developer-faq/index.md)
*   [Autotest Server Usage
    Guide](chromium-os/testing/autotest-developer-faq/autotest-server-usage.md)
*   [Setup Autotest
    Server](chromium-os/testing/autotest-developer-faq/setup-autotest-server.md)
*   [Setting up SSH Access to your test
    device.](chromium-os/testing/autotest-developer-faq/ssh-test-keys-setup.md)
*   [Autotest Graphics
    Documentation](chromium-os/testing/autotest-graphics-documentation.md)
*   [Autotest Keyvals](chromium-os/testing/autotest-keyvals/index.md)
*   [Perf Keyvals](chromium-os/testing/autotest-keyvals/perf-keyvals.md)
*   [Autotest for Chromium OS
    developers](chromium-os/testing/autotest-user-doc/index.md)
*   [Building and Running
    Tests](chromium-os/testing/building-and-running-tests.md)
*   [Cellular: Testing](chromium-os/testing/cellular-testing.md)
*   [Chamelium](chromium-os/testing/chamelium/index.md)
*   [Chamelium Audio Board](chromium-os/testing/chamelium-audio-board/index.md)
*   [Chamelium Audio
    streaming](chromium-os/testing/chamelium-audio-streaming/index.md)
*   [Collecting Stats for
    Graphite](chromium-os/testing/collecting-stats-for-graphite.md)
*   [Common Testing Workflows](chromium-os/testing/common-testing-workflows.md)
*   [cros-autotest](chromium-os/testing/cros-autotest/index.md)
*   [Dynamic Suites](chromium-os/testing/dynamic-suites/index.md)
*   [Running Dynamic Test
    Suites](chromium-os/testing/dynamic-suites/dynamic-test-suites.md)
*   [Test Dependencies in Dynamic
    Suites](chromium-os/testing/dynamic-suites/test-dependencies-in-dynamic-suites.md)
*   [Dynamic Test Suites](chromium-os/testing/dynamic-test-suites.md)
*   [Existing Autotest
    Utilities](chromium-os/testing/existing-autotest-utilities.md)
*   [GS Offloader](chromium-os/testing/gs-offloader.md)
*   [Hardware_Qualification](chromium-os/testing/hardware_qualification/index.md)
*   [Infrastructure
    Components](chromium-os/testing/infrastructure-components/index.md)
*   [Keyvals](chromium-os/testing/infrastructure-components/keyvals.md)
*   [MobLab](chromium-os/testing/moblab/index.md)
*   [MobLab FAQ](chromium-os/testing/moblab-faq.md)
*   [Moblabv2](chromium-os/testing/moblab-landing/index.md)
*   [moblab-ann](chromium-os/testing/moblab-landing/moblab-ann/index.md)
*   [Release
    8872.67.0](chromium-os/testing/moblab-landing/moblab-ann/release8872670.md)
*   [Purchase / Prepare Moblab
    hardware](chromium-os/testing/moblab-landing/moblab-purchasing.md)
*   [prepare-dut](chromium-os/testing/moblab-landing/prepare-dut.md)
*   [Prepare Moblab](chromium-os/testing/moblab-landing/prepare-moblab/index.md)
*   [Required network
    environment](chromium-os/testing/moblab-landing/required-network-environment/index.md)
*   [Running FAFT with
    MobLab](chromium-os/testing/moblab/faft-testing-with-moblab/index.md)
*   [Mob Monitor](chromium-os/testing/moblab/mob-monitor/index.md)
*   [Overview of MobLab](chromium-os/testing/moblab/overview-of-moblab/index.md)
*   [Partner Testing](chromium-os/testing/moblab/partner_testing.md)
*   [Pre-requirements of issue
    creation](chromium-os/testing/moblab/pre-requirements-of-issue-creation.md)
*   [MobLab Release Notes](chromium-os/testing/moblab/releasenotes/index.md)
*   [Setting Up MobLab](chromium-os/testing/moblab/setup/index.md)
*   [Using MobLab](chromium-os/testing/moblab/usingmoblab/index.md)
*   [View Test Result via
    WMatrix](chromium-os/testing/moblab/view-test-result-vis-wmatrix/index.md)
*   [User Guide: Chrome OS Performance Tests and the Chrome Performance
    Dashboard](chromium-os/testing/perf-data.md)
*   [Regression Detection for Performance
    Tests](chromium-os/testing/perf-regression-detection.md)
*   [Power testing](chromium-os/testing/index.md)
*   [PLTP](chromium-os/testing/power-testing/pltp/index.md)
*   [Running unittests via QEMU](chromium-os/testing/qemu-unittests.md)
*   [Running Smoke Suite On a VM
    Image](chromium-os/testing/running-smoke-suite-on-a-vm-image.md)
*   [Running Unit Tests On The
    Target](chromium-os/testing/running-unit-tests-on-the-target.md)
*   [Specific Test
    Documentation](chromium-os/testing/specific-test-documentation.md)
*   [suite_scheduler](chromium-os/testing/suite_scheduler.md)
*   [Suite Scheduler Design Doc](chromium-os/testing/suite_scheduler-1.md)
*   [Test Code Labs](chromium-os/testing/test-code-labs/index.md)
*   [Autotest Client
    Tests](chromium-os/testing/test-code-labs/autotest-client-tests/index.md)
*   [Autotest Client Helper
    Libraries](chromium-os/testing/test-code-labs/autotest-client-tests/autotest-client-helper-libraries.md)
*   [Autotest Results
    Logs](chromium-os/testing/test-code-labs/autotest-client-tests/autotest-results-logs.md)
*   [Basic Ebuild
    Troubleshooting](chromium-os/testing/test-code-labs/autotest-client-tests/basic-ebuild-troubleshooting.md)
*   [Creating and deploying Chromium OS Dynamic Test
    Suites](chromium-os/testing/test-code-labs/dynamic-suite-codelab.md)
*   [Server Side Test for Chromium OS Autotest
    Codelab](chromium-os/testing/test-code-labs/server-side-test/index.md)
*   [Test Dependencies in Dynamic
    Suites](chromium-os/testing/test-dependencies-in-dynamic-suites.md)
*   [Test Lab Setup](chromium-os/testing/test-lab-setup.md)
*   [Test Suites](chromium-os/testing/test-suites.md)
*   [Writing and Developing
    Tests](chromium-os/testing/writing-and-developing-tests/index.md)
*   [Test Deps
    FAQ](chromium-os/testing/writing-and-developing-tests/test-deps-faq.md)
*   [Testing Your Changes
    FAQ](chromium-os/testing/writing-and-developing-tests/testing-your-changes-faq.md)
*   [Writing Tests
    FAQ](chromium-os/testing/writing-and-developing-tests/writing-tests-faq.md)
*   [Tips And Tricks for Chromium OS
    Developers](chromium-os/tips-and-tricks-for-chromium-os-developers.md)
*   [USB-PD Sniffer](chromium-os/twinkie/index.md)
*   [Build Sigrok and Pulseview from
    sources](chromium-os/twinkie/build-sigrok-and-pulseview-from-sources.md)
*   [User Experience](chromium-os/user-experience/index.md)
*   [Access Points](chromium-os/user-experience/access-points/index.md)
*   [Content Browser](chromium-os/user-experience/content-browser/index.md)
*   [Form Factors
    Exploration](chromium-os/user-experience/form-factors/index.md)
*   [Tablet](chromium-os/user-experience/form-factors/tablet/index.md)
*   [Open/Save Dialogs](chromium-os/user-experience/opensave-dialogs/index.md)
*   [Panels](chromium-os/user-experience/panels/index.md)
*   [Settings](chromium-os/user-experience/settings/index.md)
*   [Shelf](chromium-os/user-experience/shelf/index.md)
*   [System Status
    Icons](chromium-os/user-experience/system-status-icons/index.md)
*   [Pinned Tabs](chromium-os/user-experience/tab-ui/index.md)
*   [Window Management /
    Overview](chromium-os/user-experience/window-management/index.md)
*   [Window UI Variants](chromium-os/user-experience/window-ui/index.md)
*   [Using Eclipse to debug Chrome on Chrome
    OS](chromium-os/using-eclipse-to-debug-chrome-on-chrome-os/index.md)
*   [Home](chromium-projects/index.md)
*   [Chromium Code of Conduct](conduct.md)
*   [Contact](contact.md)
*   [For Developers](developers/_index.md)
*   [Web page views](developers/Web-page-views/index.md)
*   [about-signin-internals](developers/about-signin-internals.md)
*   [Accessibility for Chromium Developers](developers/accessibility/index.md)
*   [ChromeVox (for developers)](developers/accessibility/chromevox.md)
*   [ChromeVox on Desktop
    Linux](developers/accessibility/chromevox-on-desktop-linux.md)
*   [HTML Accessibility](developers/accessibility/html-accessibility.md)
*   [Linux Accessibility](developers/accessibility/linux-accessibility.md)
*   [Mac Accessibility](developers/accessibility/mac-accessibility.md)
*   [Accessibility Testing](developers/accessibility/testing.md)
*   [Views accessibility](developers/accessibility/views-accessibility.md)
*   [Blink Accessibility](developers/accessibility/webkit-accessibility.md)
*   [WebUI accessibility
    audit](developers/accessibility/webui-accessibility-audit.md)
*   [Windows accessibility](developers/accessibility/windows-accessibility.md)
*   [Adding third_party Libraries](developers/adding-3rd-party-libraries.md)
*   [Eclipse Configuration for Android](developers/android-eclipse-dev.md)
*   [Android WebView](developers/androidwebview/index.md)
*   [Try out WebView
    Beta](developers/androidwebview/android-webview-beta/index.md)
*   [WebView FAQ for Symantec Certificate Transparency
    Issue](developers/androidwebview/webview-ct-bug.md)
*   [Information for Third-party Applications on Mac](developers/applescript.md)
*   [bisect-builds.py](developers/bisect-builds-py.md)
*   [Blink GC Plugin Errors](developers/blink-gc-plugin-errors.md)
*   [Chromium Development Calendar and Release
    Info](developers/calendar/index.md)
*   [Viewing change logs for Chromium and Blink](developers/change-logs.md)
*   [Checkstyle](developers/checkstyle.md)
*   [Chromium String usage](developers/chromium-string-usage.md)
*   [clang mac](developers/clang-mac.md)
*   [Class Diagram: Blink core to Chrome
    Browser](developers/class-diagram-webkit-webcore-to-chrome-browser.md)
*   [Cluster Telemetry](developers/cluster-telemetry.md)
*   [Code Browsing in Chromium](developers/code-browsing-in-chromium.md)
*   [Code Coverage](developers/code-coverage.md)
*   [Coding Style](developers/coding-style/index.md)
*   [Chromium Style Checker
    Errors](developers/coding-style/chromium-style-checker-errors.md)
*   [Cocoa Dos and Don'ts](developers/coding-style/cocoa-dos-and-donts.md)
*   [C++ Dos and Don'ts](developers/coding-style/cpp-dos-and-donts.md)
*   [Important Abstractions and Data
    Structures](developers/coding-style/important-abstractions-and-data-structures.md)
*   [Java](developers/coding-style/java.md)
*   [Responsibilities of Committers and
    Reviewers](developers/committers-responsibility.md)
*   [Common Terms &
    Technobabble](developers/common-terms--techno-babble/index.md)
*   [Content module](developers/content-module/index.md)
*   [Content API](developers/content-module/content-api.md)
*   [Contributing Code](developers/contributing-code/index.md)
*   [CL Footer Syntax](developers/contributing-code/-bug-syntax.md)
*   [Directly committing code](developers/contributing-code/direct-commit.md)
*   [External Contributor
    Checklist](developers/contributing-code/external-contributor-checklist.md)
*   [Minimizing Review Lag Across Time
    Zones](developers/contributing-code/minimizing-review-lag-across-time-zones.md)
*   [Watchlists](developers/contributing-code/watchlists.md)
*   [Core Principles](developers/core-principles/index.md)
*   [No Hidden Preferences](developers/core-principles/no-hidden-preferences.md)
*   [C++ in Chromium 101 - Codelab](developers/cpp-in-chromium-101-codelab.md)
*   [Crash Reports](developers/crash-reports/index.md)
*   [Crash with invalid
    handle](developers/crash-reports/crash-with-invalid-handle.md)
*   [Handle file upload
    scheme](developers/crash-reports/handle-file-upload-scheme.md)
*   [Creating and Using Profiles](developers/creating-and-using-profiles.md)
*   [Creating V8 profiling timeline
    plots](developers/creating-v8-profiling-timeline-plots/index.md)
*   [Debugging with Crash Keys](developers/debugging-with-crash-keys.md)
*   [Decoding Crash Dumps](developers/decoding-crash-dumps.md)
*   [Deep Memory Profiler](developers/deep-memory-profiler/index.md)
*   [C++ Object Type Identifier (a.k.a. Type
    Profiler)](developers/deep-memory-profiler/cpp-object-type-identifier.md)
*   [Design Documents](developers/design-documents/index.md)
*   [Building 64-bit Chromium](developers/design-documents/64-bit-support.md)
*   [Design document for
    about:conflicts](developers/design-documents/about-conflicts/index.md)
*   [Accessibility Technical
    Documentation](developers/design-documents/accessibility/index.md)
*   [Accessibility Issue
    Tracker](developers/design-documents/accessibility/tracker/index.md)
*   [Adaptive spell checking for multilingual
    users](developers/design-documents/advancedspellchecker.md)
*   [JNI on Chromium for Android](developers/design-documents/android-jni.md)
*   [AppleScript Support](developers/design-documents/applescript.md)
*   [Mac App Mode (draft)](developers/design-documents/appmode-mac.md)
*   [Aura](developers/design-documents/aura/index.md)
*   [Aura
    (obsolete)](developers/design-documents/aura-desktop-window-manager/index.md)
*   [Ash Color
    Chooser](developers/design-documents/aura/ash-color-chooser/index.md)
*   [Aura and Shell
    dependencies](developers/design-documents/aura/aura-and-shell-dependencies/index.md)
*   [Aura Overview](developers/design-documents/aura/aura-overview/index.md)
*   [Client API](developers/design-documents/aura/client-api/index.md)
*   [Event Handling](developers/design-documents/aura/event-handling/index.md)
*   [Focus and
    Activation](developers/design-documents/aura/focus-and-activation.md)
*   [Gesture
    Recognizer](developers/design-documents/aura/gesture-recognizer/index.md)
*   [Gesture Recognizer
    Tests](developers/design-documents/aura/gesture-recognizer/gesture-recognizer-tests.md)
*   [Graphics
    Architecture](developers/design-documents/aura/graphics-architecture/index.md)
*   [Layout Managers](developers/design-documents/aura/layout-managers/index.md)
*   [Multi-desktop](developers/design-documents/aura/multi-desktop.md)
*   [System Tray](developers/design-documents/aura/system-tray.md)
*   [Views](developers/design-documents/aura/views/index.md)
*   [Auto-Throttled Screen Capture and
    Mirroring](developers/design-documents/auto-throttled-screen-capture-and-mirroring.md)
*   [Automatic Spelling
    Correction](developers/design-documents/automaticspellingcorrection.md)
*   [Blink Coordinate
    Spaces](developers/design-documents/blink-coordinate-spaces/index.md)
*   [Blink
    ScrollbarThemes](developers/design-documents/blink-scrollbarthemes.md)
*   [Blocking Cross-Site Documents for Site
    Isolation](developers/design-documents/blocking-cross-site-documents.md)
*   [Bluetooth Design
    Docs](developers/design-documents/bluetooth-design-docs_index.md)
*   [Web Bluetooth through Bluetooth Android class diagram for
    getCharacteristic,
    getPrimaryService](developers/design-documents/bluetooth-design-docs/web-bluetooth-through-bluetooth-android-class-diagram/index.md)
*   [Browser
    Components](developers/design-documents/browser-components/index.md)
*   [Cookbook](developers/design-documents/browser-components/cookbook.md)
*   [Browser Window](developers/design-documents/browser-window/index.md)
*   [Chaps Technical
    Design](developers/design-documents/chaps-technical-design/index.md)
*   [views](developers/design-documents/chromeviews.md)
*   [Chromium Graphics // Chrome
    GPU](developers/design-documents/chromium-graphics/index.md)
*   [Debugging with
    Nsight](developers/design-documents/chromium-graphics/debugging-with-nsight.md)
*   [How to get GPU
    Rasterization](developers/design-documents/chromium-graphics/how-to-get-gpu-rasterization/index.md)
*   [Mac Browser Compositor aka Mac Ubercompositor aka Mac Delegated
    Rendering](developers/design-documents/chromium-graphics/mac-delegated-rendering/index.md)
*   [Handling Mac Overscroll On the Compositor
    Thread](developers/design-documents/chromium-graphics/mac-impl-events.md)
*   [Surfaces](developers/design-documents/chromium-graphics/surfaces.md)
*   [Menus, Hotkeys, & Command Dispatch
    (Mac)](developers/design-documents/command-dispatch-mac.md)
*   [Compositor (Touch) Hit
    Testing](developers/design-documents/compositor-hit-testing.md)
*   [Compositor Thread
    Architecture](developers/design-documents/compositor-thread-architecture.md)
*   [Confirm to Quit
    Experiment](developers/design-documents/confirm-to-quit-experiment/index.md)
*   [Constrained Popup
    Windows](developers/design-documents/constrained-popup-windows.md)
*   [Conventions and patterns for multi-platform
    development](developers/design-documents/conventions-and-patterns-for-multi-platform-development.md)
*   [Browser Components / Layered Components
    Cookbook](developers/design-documents/cookbook/index.md)
*   [Design document: Cookie prompt
    replacement](developers/design-documents/cookie-prompt-replacement.md)
*   [cookie-split-loading](developers/design-documents/cookie-split-loading/index.md)
*   [Cookies and
    Prerender](developers/design-documents/cookies-and-prerender.md)
*   [Crypto](developers/design-documents/crypto.md)
*   [Dependency
    Management](developers/design-documents/dependency-management.md)
*   [Desktop
    Notifications](developers/design-documents/desktop-notifications/index.md)
*   [API
    Specification](developers/design-documents/desktop-notifications/api-specification.md)
*   [DirectWrite Font Cache
    (obsolete)](developers/design-documents/directwrite-font-cache.md)
*   [How Chromium Displays Web
    Pages](developers/design-documents/displaying-a-web-page-in-chrome/index.md)
*   [DNS Prefetching](developers/design-documents/dns-prefetching.md)
*   [Download Manager
    Sequences](developers/design-documents/downloadmanagersequences/index.md)
*   [Embedding Flash Fullscreen in the Browser
    Window](developers/design-documents/embedding-flash-fullscreen-in-the-browser-window/index.md)
*   [Encrypted Partition
    Recovery](developers/design-documents/encrypted-partition-recovery.md)
*   [Experiments](developers/design-documents/experiments.md)
*   [Extensions](developers/design-documents/extensions/index.md)
*   [How the Extension System
    Works](developers/design-documents/extensions/how-the-extension-system-works/index.md)
*   [Accessibility](developers/design-documents/extensions/how-the-extension-system-works/accessibility.md)
*   [APIs as stateless service
    calls](developers/design-documents/extensions/how-the-extension-system-works/api-pattern-design-doc.md)
*   [Benchmarking
    Extension](developers/design-documents/extensions/how-the-extension-system-works/chrome-benchmarking-extension/index.md)
*   [Default
    Apps](developers/design-documents/extensions/how-the-extension-system-works/default-apps.md)
*   [Extension Documentation
    System](developers/design-documents/extensions/how-the-extension-system-works/docs/index.md)
*   [How Extension Docs Are
    Served](developers/design-documents/extensions/how-the-extension-system-works/docs/how-docs-are-served/index.md)
*   [Updating the Release
    Notes](developers/design-documents/extensions/how-the-extension-system-works/docs/how-to-update-the-release-notes.md)
*   [Extensions
    Manifesto](developers/design-documents/extensions/how-the-extension-system-works/extension-manifesto.md)
*   [i18n for
    extensions](developers/design-documents/extensions/how-the-extension-system-works/i18n.md)
*   [Proposed & Proposing New
    Changes](developers/design-documents/extensions/proposed-changes/index.md)
*   [API Proposals (New APIs Start
    Here)](developers/design-documents/extensions/proposed-changes/apis-under-development/index.md)
*   [Auto-install of android companion
    extensions](developers/design-documents/extensions/proposed-changes/apis-under-development/auto-install-of-android-companion-extensions.md)
*   [Bluetooth Extension
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/bluetooth-extension-api.md)
*   [BrowserKeys API
    Proposal](developers/design-documents/extensions/proposed-changes/apis-under-development/browser-keys.md)
*   [Clear Browsing Data
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/clear.md)
*   [Context Menu API
    Proposal](developers/design-documents/extensions/proposed-changes/apis-under-development/context-menu-api/index.md)
*   [Desktop Notification
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/desktop-notification-api.md)
*   [Downloads
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/downloads-api.md)
*   [executeScript() and
    executeCSS()](developers/design-documents/extensions/proposed-changes/apis-under-development/executecontentscript-proposal.md)
*   [Font
    Settings](developers/design-documents/extensions/proposed-changes/apis-under-development/font-settings.md)
*   [Get Views by
    Type](developers/design-documents/extensions/proposed-changes/apis-under-development/get-extension-views-by-type-proposal.md)
*   [Gleam
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/gleam-api.md)
*   [History
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/history-api.md)
*   [i18n-api](developers/design-documents/extensions/proposed-changes/apis-under-development/i18n-api.md)
*   [Input Method
    Editor](developers/design-documents/extensions/proposed-changes/apis-under-development/input-method-editor.md)
*   [\[Deprecated\] Instructions for API launch
    engineers](developers/design-documents/extensions/proposed-changes/apis-under-development/instructions-for-api-shepherds.md)
*   [Language Detection for chunks of
    text](developers/design-documents/extensions/proposed-changes/apis-under-development/language-detection.md)
*   [Managed Storage
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/managed-storage-api.md)
*   [MediaGallery](developers/design-documents/extensions/proposed-changes/apis-under-development/media-gallery.md)
*   [Notifications of Web Request and
    Navigation](developers/design-documents/extensions/proposed-changes/apis-under-development/notifications-of-web-request-and-navigation.md)
*   [Offscreen Tabs
    (experimental)](developers/design-documents/extensions/proposed-changes/apis-under-development/offscreen-tabs/index.md)
*   [Omnibox
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/omnibox-api.md)
*   [Panels](developers/design-documents/extensions/proposed-changes/apis-under-development/panels/index.md)
*   [Power Management
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/power-management-api.md)
*   [Preferences
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/preference-api.md)
*   [Privacy-relevant Preferences
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/privacy.md)
*   [Process Model
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/processes-api/index.md)
*   [Profile Extension
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/profile-extension-api.md)
*   [Cookies
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/proposal-chrome-extensions-cookies-api.md)
*   [Push
    Messaging](developers/design-documents/extensions/proposed-changes/apis-under-development/push-messaging.md)
*   [RLZ Chrome API
    Proposal](developers/design-documents/extensions/proposed-changes/apis-under-development/rlz-api.md)
*   [Settings
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/settings.md)
*   [Settings
    pages](developers/design-documents/extensions/proposed-changes/apis-under-development/settings_pages.md)
*   [System Indicator
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/system-indicator-api.md)
*   [SystemInfo Extension
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/systeminfo.md)
*   [Text Translate
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/text-translate-api.md)
*   [USB
    API](developers/design-documents/extensions/proposed-changes/apis-under-development/usb-api.md)
*   [webNavigation
    v2](developers/design-documents/extensions/proposed-changes/apis-under-development/webnavigation-v2.md)
*   [Tab Content Capture into
    MediaStreams](developers/design-documents/extensions/proposed-changes/apis-under-development/webrtc-tab-content-capture/index.md)
*   [Window
    Management](developers/design-documents/extensions/proposed-changes/apis-under-development/window-management.md)
*   [API Wish
    List](developers/design-documents/extensions/proposed-changes/apiwishlist.md)
*   [Implementing a new extension
    API](developers/design-documents/extensions/proposed-changes/creating-new-apis.md)
*   [Extension
    System](developers/design-documents/extensions/proposed-changes/extension-system-changes/index.md)
*   [Breaking
    Changes](developers/design-documents/extensions/proposed-changes/extension-system-changes/breaking-changes.md)
*   [Install Dialog
    v2](developers/design-documents/extensions/proposed-changes/extension-system-changes/install-dialog-2.md)
*   [Spellcheck
    API](developers/design-documents/extensions/proposed-changes/spellcheck-api.md)
*   [Find Bar](developers/design-documents/find-bar/index.md)
*   [First Run
    Customizations](developers/design-documents/first-run-customizations.md)
*   [Focus and Activation in Views and
    Aura](developers/design-documents/focus-and-activation-in-views-and-aura.md)
*   [Form Autofill](developers/design-documents/form-autofill_index.md)
*   [Internationalization of
    Autofill](developers/design-documents/form-autofill/internationalization-of-autofill/index.md)
*   [Password Form Styles that Chromium
    Understands](developers/design-documents/form-styles-that-chromium-understands.md)
*   [fullscreen-mac](developers/design-documents/fullscreen-mac.md)
*   [A Generic theme for the Test
    Shell](developers/design-documents/generic-theme-for-test-shell/index.md)
*   [Chromium Print
    Proxy](developers/design-documents/google-cloud-print-proxy-design.md)
*   [GPU Accelerated Compositing in
    Chrome](developers/design-documents/gpu-accelerated-compositing-in-chrome/index.md)
*   [GPU Architecture
    Roadmap](developers/design-documents/gpu-accelerated-compositing-in-chrome/gpu-architecture-roadmap.md)
*   [GPU Command Buffer](developers/design-documents/gpu-command-buffer.md)
*   [GPU Memory Buffer](developers/design-documents/gpu-memory-buffer.md)
*   [Graphics and Skia](developers/design-documents/graphics-and-skia.md)
*   [greasemonkey](developers/design-documents/greasemonkey.md)
*   [High DPI Resources](developers/design-documents/high-dpi-resources.md)
*   [HTTP
    authentication](developers/design-documents/http-authentication/index.md)
*   [Writing a SPNEGO Authenticator for Chrome on
    Android](developers/design-documents/http-authentication/writing-a-spnego-authenticator-for-chrome-on-android.md)
*   [Idealized MediaStream
    Design](developers/design-documents/idealized-mediastream-design.md)
*   [IDL build](developers/design-documents/idl-build.md)
*   [IDL compiler](developers/design-documents/idl-compiler.md)
*   [IDN in Google Chrome](developers/design-documents/idn-in-google-chrome.md)
*   [Filter Effects](developers/design-documents/image-filters.md)
*   [Immersive fullscreen](developers/design-documents/immersive-fullscreen.md)
*   [Multithreaded
    Rasterization](developers/design-documents/impl-side-painting.md)
*   [IndexedDB Design Doc](developers/design-documents/indexeddb/index.md)
*   [IndexedDBBackup](developers/design-documents/indexeddb/indexeddbbackup.md)
*   [Info Bars](developers/design-documents/info-bars/index.md)
*   [Instant](developers/design-documents/instant/index.md)
*   [Instant Support](developers/design-documents/instant/instant-support.md)
*   [Inter-process Communication
    (IPC)](developers/design-documents/inter-process-communication.md)
*   [iosurface-meeting-notes](developers/design-documents/iosurface-meeting-notes.md)
*   [Isolated Sites](developers/design-documents/isolated-sites.md)
*   [Java Resources on
    Android](developers/design-documents/java-resources-on-android.md)
*   [Layered Components:
    Design](developers/design-documents/layered-components-design.md)
*   [Layered Components: Technical
    Approach](developers/design-documents/layered-components-technical-approach/index.md)
*   [Making Autofill Into a Layered
    Component](developers/design-documents/layered-components-technical-approach/making-autofill-into-a-layered-component.md)
*   [Tests Results
    Dashboard](developers/design-documents/layout-tests-results-dashboard.md)
*   [LinuxResourcesAndLocalizedStrings](developers/design-documents/linuxresourcesandlocalizedstrings.md)
*   [Mac Window Occlusion API Use](developers/design-documents/mac-occlusion.md)
*   [Mac Plugins](developers/design-documents/mac-plugins.md)
*   [Mac XIB Files](developers/design-documents/mac-xib-files.md)
*   [Making Chrome Independent of
    Extensions](developers/design-documents/making-chrome-independent-of-extensions.md)
*   [Media Router & Web Presentation
    API](developers/design-documents/media-router/index.md)
*   [Mojo](developers/design-documents/mojo/index.md)
*   [Associated
    Interfaces](developers/design-documents/mojo/associated-interfaces.md)
*   [Calling Mojo from
    Blink](developers/design-documents/mojo/calling-mojo-from-blink.md)
*   [Chrome IPC To Mojo IPC Cheat
    Sheet](developers/design-documents/mojo/chrome-ipc-to-mojo-ipc-cheat-sheet.md)
*   [Mojo Migration
    Guide](developers/design-documents/mojo/mojo-migration-guide/index.md)
*   [Synchronous Calls](developers/design-documents/mojo/synchronous-calls.md)
*   [Type Mapping in C++](developers/design-documents/mojo/type-mapping.md)
*   [Message Validation & Error
    Handling](developers/design-documents/mojo/validation.md)
*   [Versioning](developers/design-documents/mojo/versioning.md)
*   [Mouse Lock (Pointer Lock)](developers/design-documents/mouse-lock.md)
*   [Multi-column layout](developers/design-documents/multi-column-layout.md)
*   [Multi-process
    Architecture](developers/design-documents/multi-process-architecture/index.md)
*   [How to Add New Features (without bloating
    RenderView/RenderViewHost/WebContents)](developers/design-documents/multi-process-architecture/how-to-add-new-features.md)
*   [Multi-process Resource
    Loading](developers/design-documents/multi-process-resource-loading/index.md)
*   [Native Controls](developers/design-documents/native-controls/index.md)
*   [Network Settings](developers/design-documents/network-settings.md)
*   [Network Stack](developers/design-documents/network-stack/index.md)
*   [CookieMonster](developers/design-documents/network-stack/cookiemonster/index.md)
*   [Debugging problems with the network
    proxy](developers/design-documents/network-stack/debugging-net-proxy.md)
*   [Disk Cache](developers/design-documents/network-stack/disk-cache/index.md)
*   [Disk Cache Benchmarking & Performance
    Tracking](developers/design-documents/network-stack/disk-cache/disk-cache-benchmarking.md)
*   [Disk Cache
    3.0](developers/design-documents/network-stack/disk-cache/disk-cache-v3.md)
*   [Very Simple
    Backend](developers/design-documents/network-stack/disk-cache/very-simple-backend.md)
*   [HTTP Authentication
    Throttling](developers/design-documents/network-stack/http-authentication-throttling.md)
*   [HTTP Cache](developers/design-documents/network-stack/http-cache/index.md)
*   [HTTP
    Pipelining](developers/design-documents/network-stack/http-pipelining.md)
*   [NetLog: Chrome’s network logging
    system](developers/design-documents/network-stack/netlog/index.md)
*   [Network bug
    triage](developers/design-documents/network-stack/network-bug-triage_index.md)
*   [Triaging Downloads
    Bugs](developers/design-documents/network-stack/network-bug-triage/downloads-bug-triage/index.md)
*   [Salient Bug
    List](developers/design-documents/network-stack/network-bug-triage/downloads-bug-triage/salient-bug-list.md)
*   [Network Stack
    Objectives](developers/design-documents/network-stack/network-stack-objectives.md)
*   [Network Stack Use in
    Chromium](developers/design-documents/network-stack/network-stack-use-in-chromium.md)
*   [Preconnect](developers/design-documents/network-stack/preconnect.md)
*   [Proxy settings and
    fallback](developers/design-documents/network-stack/proxy-settings-fallback/index.md)
*   [Configuring a SOCKS proxy server in
    Chrome](developers/design-documents/network-stack/socks-proxy.md)
*   [Omnibox/IME
    Coordination](developers/design-documents/omnibox-ime-coordination/index.md)
*   [Out-of-Process iframes
    (OOPIFs)](developers/design-documents/oop-iframes/index.md)
*   [FrameHandle](developers/design-documents/oop-iframes/framehandle.md)
*   [Rendering and compositing out of process
    iframes](developers/design-documents/oop-iframes/oop-iframes-rendering/index.md)
*   [Mach based OS X Interprocess Communication
    (Obsolete)](developers/design-documents/os-x-interprocess-communication.md)
*   [OS X Password Manager/Keychain
    Integration](developers/design-documents/os-x-password-manager-keychain-integration.md)
*   [Password
    Generation](developers/design-documents/password-generation/index.md)
*   [Pepper plugin
    implementation](developers/design-documents/pepper-plugin-implementation/index.md)
*   [Per-page Suborigins](developers/design-documents/per-page-suborigins.md)
*   [Plugin
    Architecture](developers/design-documents/plugin-architecture/index.md)
*   [Preferences](developers/design-documents/preferences.md)
*   [Chrome Prerendering](developers/design-documents/prerender/index.md)
*   [Print Preview](developers/design-documents/print-preview.md)
*   [Printing](developers/design-documents/printing.md)
*   [Process Models](developers/design-documents/process-models.md)
*   [Profile
    Architecture](developers/design-documents/profile-architecture/index.md)
*   [Profile Chooser
    Menu](developers/design-documents/profile-chooser-menu/index.md)
*   [Rappor (Randomized Aggregatable Privacy Preserving Ordinal
    Responses)](developers/design-documents/rappor.md)
*   [Rendering Architecture
    Diagrams](developers/design-documents/rendering-architecture-diagrams/index.md)
*   [Rendering Benchmarks (aka Smoothness
    benchmarks)](developers/design-documents/rendering-benchmarks.md)
*   [RenderText and Chrome UI text
    drawing](developers/design-documents/rendertext.md)
*   [Safe Browsing](developers/design-documents/safebrowsing/index.md)
*   [SafeSearch](developers/design-documents/safesearch.md)
*   [Sandbox](developers/design-documents/sandbox/index.md)
*   [Sandbox FAQ](developers/design-documents/sandbox/Sandbox-FAQ.md)
*   [OSX Sandboxing
    Design](developers/design-documents/sandbox/osx-sandboxing-design.md)
*   [Sane time](developers/design-documents/sane-time.md)
*   [Secure Web Proxy](developers/design-documents/secure-web-proxy.md)
*   [Service Processes](developers/design-documents/service-processes.md)
*   [Site Engagement](developers/design-documents/site-engagement.md)
*   [Site Isolation](developers/design-documents/site-isolation/index.md)
*   [Software Updates:
    Courgette](developers/design-documents/software-updates-courgette.md)
*   [Startup](developers/design-documents/startup.md)
*   [Structure of Layered Components and iOS Code Within the Chromium
    Codebase](developers/design-documents/structure-of-layered-components-within-the-chromium-codebase.md)
*   [Sync](developers/design-documents/sync/index.md)
*   [embassy](developers/design-documents/sync/embassy.md)
*   [Sync Data Best
    Practices](developers/design-documents/sync/sync-data-best-practices.md)
*   [Syncable Service
    API](developers/design-documents/sync/syncable-service-api/index.md)
*   [Unified Sync And Storage
    Overview](developers/design-documents/sync/unified-sync-and-storage-overview/index.md)
*   [System Dictionary Pop-Up
    Architecture](developers/design-documents/system-dictionary-pop-up-architecture.md)
*   [Tab Strip Design (Mac)](developers/design-documents/tab-strip-mac/index.md)
*   [Tabtastic 2
    Requirements](developers/design-documents/tabtastic-2-requirements.md)
*   [Themes](developers/design-documents/themes.md)
*   [Threading](developers/design-documents/threading/index.md)
*   [Subtle Threading Bugs and Patterns to avoid
    them](developers/design-documents/threading/suble-threading-bugs-and-patterns-to-avoid-them.md)
*   [Time Safety and Code
    Readability](developers/design-documents/time-safety-and-readability.md)
*   [Time Sources](developers/design-documents/time-sources.md)
*   [TPM Usage](developers/design-documents/tpm-usage.md)
*   [Translate](developers/design-documents/translate.md)
*   [UI Development
    Practices](developers/design-documents/ui-development-practices.md)
*   [UI Localization (aka
    Translations)](developers/design-documents/ui-localization/index.md)
*   [Mac Notes](developers/design-documents/ui-localization/mac-notes.md)
*   [UI Mirroring
    Infrastructure](developers/design-documents/ui-mirroring-infrastructure/index.md)
*   [User Scripts](developers/design-documents/user-scripts.md)
*   [Video](developers/design-documents/video/index.md)
*   [Video Capture](developers/design-documents/video-capture/index.md)
*   [Video Playback and
    Compositor](developers/design-documents/video-playback-and-compositor.md)
*   [Rect-based event targeting in
    views](developers/design-documents/views-rect-based-targeting.md)
*   [views Windowing](developers/design-documents/views-windowing/index.md)
*   [Web MIDI](developers/design-documents/web-midi/index.md)
*   [WebNavigation API
    internals](developers/design-documents/webnavigation-api-internals.md)
*   [Widget Refactor](developers/design-documents/widget-refactor.md)
*   [Wrench Menu (Mac)](developers/design-documents/wrench-menu-mac/index.md)
*   [Diagnostics](developers/diagnostics/index.md)
*   [Chrome Frame
    Troubleshooting](developers/diagnostics/gcf_troubleshooting/index.md)
*   [Installer Error Strings](developers/diagnostics/installer-error-strings.md)
*   [Discussion Groups](developers/discussion-groups/index.md)
*   [Google Plus communities related to Chrome OS and Chromium
    OS](developers/discussion-groups/google-plus-communities-related-to-chrome-os-and-chromium-os.md)
*   [Experimental Branches](developers/experimental-branches.md)
*   [Chrome Extension Developer FAQ for upcoming changes in May 2015 related to
    hosting extensions](developers/extensions-deployment-faq.md)
*   [F-Script Anywhere](developers/f-script-anywhere/index.md)
*   [Fast Intro to Git Internals](developers/fast-intro-to-git-internals.md)
*   [Finding somebody who knows how a piece of code
    works](developers/finding-somebody-who-knows-how-a-piece-of-code-works.md)
*   [Generated files](developers/generated-files.md)
*   [Gerrit Guide](developers/gerrit-guide.md)
*   [git-cache](developers/git-cache.md)
*   [GitHub Collaboration](developers/github-collaboration.md)
*   [GN build configuration](developers/gn-build-configuration.md)
*   [GYP build parameters](developers/gyp-environment-variables/index.md)
*   [How-Tos](developers/how-tos/index.md)
*   [(Quickly!) Building for CrOS (ARM &
    x64)](developers/how-tos/-quickly-building-for-cros-arm-x64/index.md)
*   [Build Instructions
    (Android)](developers/how-tos/android-build-instructions.md)
*   [ANGLE Wrangling](developers/how-tos/angle-wrangling.md)
*   [API Keys](developers/how-tos/api-keys.md)
*   [Build Instructions (Android
    WebView)](developers/how-tos/build-instructions-android-webview.md)
*   [Build Instructions (Cast)](developers/how-tos/build-instructions-cast.md)
*   [Build Instructions (Chromium OS on
    Linux)](developers/how-tos/build-instructions-chromeos.md)
*   [Build Instructions
    (Windows)](developers/how-tos/build-instructions-windows.md)
*   [Chrome Frame CFInstall
    script](developers/how-tos/chrome-frame-cfinstall.md)
*   [Chrome Frame](developers/how-tos/chrome-frame-getting-started/index.md)
*   [Chromium Modularization](developers/how-tos/chromium-modularization.md)
*   [Closure Compilation](developers/how-tos/closure-compilation.md)
*   [Component build / Shared Library / Multi-DLL
    build](developers/how-tos/component-build.md)
*   [cscope/emacs: example Linux
    setup](developers/how-tos/cscope-emacs-example-linux-setup/index.md)
*   [Debugging GPU related
    code](developers/how-tos/debugging-gpu-related-code.md)
*   [Debugging Chromium on Android](developers/how-tos/debugging-on-android.md)
*   [Debugging Chromium on Mac OS
    X](developers/how-tos/debugging-on-os-x/index.md)
*   [Building with Ninja, Debugging with
    Xcode](developers/how-tos/debugging-on-os-x/building-with-ninja-debugging-with-xcode.md)
*   [Debugging Chromium on
    Windows](developers/how-tos/debugging-on-windows/index.md)
*   [Example of working with a
    dump.](developers/how-tos/debugging-on-windows/example-of-working-with-a-dump.md)
*   [Graphics Debugging in Visual Studio
    2013](developers/how-tos/debugging-on-windows/graphics-debugging-in-visual-studio-2013/index.md)
*   [WinDBG help](developers/how-tos/debugging-on-windows/windbg-help.md)
*   [Using depot_tools](developers/how-tos/depottools/index.md)
*   [gclient](developers/how-tos/depottools/gclient.md)
*   [Presubmit Scripts](developers/how-tos/depottools/presubmit-scripts.md)
*   [How to use drover](developers/how-tos/drover.md)
*   [Editing the spell checking
    dictionaries](developers/how-tos/editing-the-spell-checking-dictionaries.md)
*   [Enterprise](developers/how-tos/enterprise/index.md)
*   [Policy Settings in
    Chrome](developers/how-tos/enterprise/adding-new-policies.md)
*   [Protobuf-encoded policy
    blobs](developers/how-tos/enterprise/protobuf-encoded-policy-blobs/index.md)
*   [Running the cloud policy test
    server](developers/how-tos/enterprise/running-the-cloud-policy-test-server.md)
*   [How to file Web Bluetooth
    bugs](developers/how-tos/file-web-bluetooth-bugs.md)
*   [Get the Code: Checkout, Build, & Run
    Chromium](developers/how-tos/get-the-code/index.md)
*   [Get the Code (Deprecated)](developers/how-tos/get-the-code-v2.md)
*   [Committing and reverting changes
    manually](developers/how-tos/get-the-code/committing-and-reverting-changes-manually.md)
*   [Gclient Managed
    Mode](developers/how-tos/get-the-code/gclient-managed-mode.md)
*   [Managing Multiple Working
    Directories](developers/how-tos/get-the-code/multiple-working-directories.md)
*   [Working with
    Branches](developers/how-tos/get-the-code/working-with-branches.md)
*   [Working with Nested Third Party
    Repositories](developers/how-tos/get-the-code/working-with-nested-repos.md)
*   [Working with Release
    Branches](developers/how-tos/get-the-code/working-with-release-branches.md)
*   [Getting Around the Chromium Source Code Directory
    Structure](developers/how-tos/getting-around-the-chrome-source-code/index.md)
*   [GPU Overdraw Debugging
    Tool](developers/how-tos/gpu-overdraw-debugging-tool.md)
*   [GPU Bots & Pixel Wrangling](developers/how-tos/gpu-wrangling/index.md)
*   [check_gpu_bots
    script](developers/how-tos/gpu-wrangling/check_gpu_bots-script/index.md)
*   [Setting up Visual Studio Debugger
    Visualizers](developers/how-tos/how-to-set-up-visual-studio-debugger-visualizers.md)
*   [Inspecting Ash with DevTools](developers/how-tos/inspecting-ash/index.md)
*   [Install depot_tools](developers/how-tos/install-depot-tools.md)
*   [Leak GDI Objects in
    Windows](developers/how-tos/leak-gdi-object-in-windows.md)
*   [Mac Development](developers/how-tos/mac-development.md)
*   [How to make a web standards
    proposal](developers/how-tos/make-a-web-standards-proposal.md)
*   [The old instructions for getting the
    code](developers/how-tos/old-get-the-code.md)
*   [Optimizing Energy
    Consumption](developers/how-tos/optimizing-energy-consumption.md)
*   [Order file development
    guide](developers/how-tos/order-file-development-guide.md)
*   [Reliability Tests](developers/how-tos/reliability-tests/index.md)
*   [Retrieving Crash Reports on
    iOS](developers/how-tos/retrieving-crash-reports-on-ios.md)
*   [Run Chromium with flags](developers/how-tos/run-chromium-with-flags.md)
*   [Run Mojo Shell](developers/how-tos/run-mojo-shell.md)
*   [ScopedLogger](developers/how-tos/scopedlogger.md)
*   [Submitting a Performance
    Bug](developers/how-tos/submitting-a-performance-bug/index.md)
*   [The Trace Event Profiling Tool
    (about:tracing)](developers/how-tos/trace-event-profiling-tool/index.md)
*   [Anatomy of
    Jank](developers/how-tos/trace-event-profiling-tool/anatomy-of-jank/index.md)
*   [Chrome Frame Viewer Overview and Getting
    Started](developers/how-tos/trace-event-profiling-tool/frame-viewer/index.md)
*   [Memory profiling in
    chrome://tracing](developers/how-tos/trace-event-profiling-tool/memory/index.md)
*   [GPU Memory
    Tracing](developers/how-tos/trace-event-profiling-tool/memory/gpu-memory/index.md)
*   [Heap Profiling with
    memory-infra](developers/how-tos/trace-event-profiling-tool/memory/heap-profiling-with-memory-infra/index.md)
*   [HowTo: Adding Memory Infra Tracing to a
    Component](developers/how-tos/trace-event-profiling-tool/memory/howto-adding-memory-infra-tracing-to-a-component.md)
*   [Startup tracing with memory
    profiling](developers/how-tos/trace-event-profiling-tool/memory/startup-tracing-with-memory-profiling.md)
*   [Recording Tracing
    Runs](developers/how-tos/trace-event-profiling-tool/recording-tracing-runs/index.md)
*   [How to save .skp files from
    Chromium](developers/how-tos/trace-event-profiling-tool/saving-skp-s-from-chromium/index.md)
*   [Understanding about:tracing
    results](developers/how-tos/trace-event-profiling-tool/trace-event-reading/index.md)
*   [Adding Traces to
    Chromium/WebKit/Javascript](developers/how-tos/trace-event-profiling-tool/tracing-event-instrumentation.md)
*   [FrameViewer
    How-To](developers/how-tos/trace-event-profiling-tool/using-frameviewer/index.md)
*   [Using Dr. Memory](developers/how-tos/using-drmemory.md)
*   [Using R to reduce Page Cycler
    Regressions](developers/how-tos/using-r-to-reduce-page-cycler-regressions.md)
*   [Using the heap leak
    checker](developers/how-tos/using-the-heap-leak-checker.md)
*   [Vectorized icons in native Chrome
    UI](developers/how-tos/vectorized-icons-in-native-chrome-ui.md)
*   [VisualStudio Tricks](developers/how-tos/visualstudio-tricks.md)
*   [webkit-gardening](developers/how-tos/webkit-gardening.md)
*   [A short trip through the Chromium installer's
    mind!](developers/installer.md)
*   [IRC](developers/irc.md)
*   [JavaScript Unit tests Cookbook for Chrome Remote
    Desktop](developers/javascript-unittests/index.md)
*   [Jinja](developers/jinja.md)
*   [Learning your way around the
    code](developers/learning-your-way-around-the-code.md)
*   [Libraries Guide](developers/libraries-guide.md)
*   [Linux Technical FAQ](developers/linux-technical-faq.md)
*   [Chrome C++ Lock and
    ConditionVariable](developers/lock-and-condition-variable.md)
*   [Mandoline](developers/mandoline/index.md)
*   [Mandoline: Build, Debug & Test Instructions](developers/mandoline/build.md)
*   [Markdown Documentation guide](developers/markdown-documentation.md)
*   [MD5 Certificate Statistics](developers/md5-certificate-statistics.md)
*   [Meet the Web Platform Companion
    Guide](developers/meet-the-web-platform-companion.md)
*   [Memory](developers/memory.md)
*   [Efforts against Memory Bloat](developers/memory-bloat.md)
*   [Memory Usage Backgrounder](developers/memory-usage-backgrounder/index.md)
*   [Memory_Watcher](developers/memory_watcher.md)
*   [mus+ash](developers/mus-ash.md)
*   [New Features](developers/new-features.md)
*   [NPAPI deprecation: developer guide](developers/npapi-deprecation/index.md)
*   [OS X keyboard handling](developers/os-x-keyboard-handling.md)
*   [OWNERS Files](developers/owners-files.md)
*   [Benchmarks == Measurements + Page
    Set](developers/page-sets-and-benchmarks.md)
*   [Pepper API Best Practices](developers/pepper-api-best-practices.md)
*   [Polymer 0.8](developers/polymer-0-8.md)
*   [Polymer 1.0](developers/polymer-1-0.md)
*   [Postmortems](developers/postmortems.md)
*   [Profiling Chromium and Blink](developers/profiling-chromium-and-webkit.md)
*   [Profiling Blink using Flame
    Graphs](developers/profiling-flame-graphs/index.md)
*   [Quarantined pages](developers/quarantined-pages/index.md)
*   [Meeting Notes](developers/quarantined-pages/meeting-notes.md)
*   [Quick reference](developers/quick-reference.md)
*   [Recent Changes to the Credential Management
    API](developers/recent-changes-credential-management-api.md)
*   [Jank Case Study 1:
    theverge.com](developers/rendering-performance-case-study-1/index.md)
*   [BiDi support in WebKit](developers/rtl-in-webkit/index.md)
*   [Severity Guidelines for Security Issues](developers/severity-guidelines.md)
*   [Shutdown](developers/shutdown.md)
*   [SlickEdit Editor Notes](developers/slickedit-editor-notes.md)
*   [Smart Pointer Guidelines](developers/smart-pointer-guidelines.md)
*   [Speed Hall of Fame](developers/speed-hall-of-fame.md)
*   [Speed Infra](developers/speed-infra/index.md)
*   [FAQ for when a perf regression bug is assigned to
    you](developers/speed-infra/perf-bug-faq.md)
*   [Status Update Email Best
    Practices](developers/status-update-email-best-practices.md)
*   [Using Sublime Text as your IDE](developers/sublime-text/index.md)
*   [Sync diagnostics](developers/sync-diagnostics.md)
*   [Tech Talks: Videos & Presentations](developers/index.md)
*   [Release process](developers/tech-talk-videos/release-process/index.md)
*   [Technical Discussion Groups](developers/technical-discussion-groups.md)
*   [Telemetry: Introduction](developers/telemetry/index.md)
*   [Telemetry: Add a Measurement](developers/telemetry/add_a_measurement.md)
*   [Telemetry: Diagnosing Test
    Failures](developers/telemetry/diagnosing-test-failures/index.md)
*   [Performance Try Bots](developers/telemetry/performance-try-bots/index.md)
*   [Telemetry: Profiling](developers/telemetry/profiling.md)
*   [Telemetry: Record a Page
    Set](developers/telemetry/record_a_page_set/index.md)
*   [Telemetry: Run Benchmarks Locally](developers/telemetry/run_locally.md)
*   [Running Telemetry on Chrome
    OS](developers/telemetry/running-telemetry-on-chrome-os.md)
*   [Telemetry: Feature
    Guidelines](developers/telemetry/telemetry-feature-guidelines.md)
*   [Telemetry Mac Keychain
    Setup](developers/telemetry/telemetry-mac-keychain-setup.md)
*   [Telemetry: Profile
    Generation](developers/telemetry/telemetry-profile-generation.md)
*   [Telemetry: Unit tests](developers/telemetry/telemetry-unittests.md)
*   [Telemetry: Cloud Storage](developers/telemetry/upload_to_cloud_storage.md)
*   [Testing and infrastructure](developers/testing/index.md)
*   [Adding Performance
    Tests](developers/testing/adding-performance-tests/index.md)
*   [Adding new tests to the Main Chromium
    Waterfall](developers/testing/adding-tests-to-the-main-waterfall.md)
*   [Adding new tests to the Memory
    Waterfall](developers/testing/adding-tests-to-the-memory-waterfall.md)
*   [AddressSanitizer (ASan)](developers/testing/addresssanitizer.md)
*   [Android Tests](developers/testing/android-tests/index.md)
*   [Android WebView
    tests](developers/testing/android-tests/android-webview-tests.md)
*   [Testing Android code that crosses the C++/Java
    boundary](developers/testing/android-tests/testing-android-code-that-crosses-the-c-java-boundary.md)
*   [BidiChecker](developers/testing/bidichecker.md)
*   [Browser Tests](developers/testing/browser-tests.md)
*   [Making changes to test dashboard](developers/testing/changedashboard.md)
*   [chrome.test APIs](developers/testing/chrome-test-apis.md)
*   [Chromium build
    infrastructure](developers/testing/chromium-build-infrastructure/index.md)
*   [Buildbot
    Annotations](developers/testing/chromium-build-infrastructure/buildbot-annotations.md)
*   [Getting the buildbot
    source](developers/testing/chromium-build-infrastructure/getting-the-buildbot-source/index.md)
*   [4. Maintain your Buildbot
    infrastructure](developers/testing/chromium-build-infrastructure/getting-the-buildbot-source/buildbot-maintenance.md)
*   [1. Configuring/setting up the buildbot
    instances](developers/testing/chromium-build-infrastructure/getting-the-buildbot-source/configuring-your-buildbot.md)
*   [3. Forking your
    buildbot](developers/testing/chromium-build-infrastructure/getting-the-buildbot-source/forking-your-buildbot.md)
*   [2. Running a buildbot
    instance](developers/testing/chromium-build-infrastructure/getting-the-buildbot-source/running-a-buildbot-instance.md)
*   [5. Buildbot Tips and
    Tricks](developers/testing/chromium-build-infrastructure/getting-the-buildbot-source/tips.md)
*   [Performance Test Plots - to track performance
    regressions](developers/testing/chromium-build-infrastructure/performance-test-plots/index.md)
*   [How to repro a build from a
    waterfall.](developers/testing/chromium-build-infrastructure/repro-a-build.md)
*   [Tour of the Chromium Buildbot
    Waterfall](developers/testing/chromium-build-infrastructure/tour-of-the-chromium-buildbot/index.md)
*   [Linux Builder
    (dbg-shlib)](developers/testing/chromium-build-infrastructure/tour-of-the-chromium-buildbot/linux-builder-dbg-shlib.md)
*   [Chromium Commit Queue](developers/testing/commit-queue/index.md)
*   [Analyze step(s)](developers/testing/commit-queue/chromium_trybot-json.md)
*   [Design doc: Chromium Commit
    queue](developers/testing/commit-queue/design.md)
*   [Design: 3-way Integration with Rietveld and the Try
    Server](developers/testing/commit-queue/integration-with-rietveld.md)
*   [Contacting a Trooper](developers/testing/contacting-a-trooper.md)
*   [Control Flow Integrity](developers/testing/control-flow-integrity/index.md)
*   [Overhead](developers/testing/control-flow-integrity/overhead.md)
*   [Dr. Fuzz](developers/testing/dr-fuzz.md)
*   [Fake Bidi](developers/testing/fake-bidi.md)
*   [Flakiness Dashboard HOWTO](developers/testing/flakiness-dashboard/index.md)
*   [Frame Rate Test](developers/testing/frame-rate-test.md)
*   [fuzzers](developers/testing/fuzzers.md)
*   [Gatekeeper-NG](developers/testing/gatekeeper-ng.md)
*   [GPU Testing](developers/testing/gpu-testing/index.md)
*   [GPU Bot Details](developers/testing/gpu-testing/gpu-bot-details.md)
*   [Instrumented libraries for dynamic
    tools](developers/testing/instrumented-libraries-for-dynamic-tools.md)
*   [Isolated Testing](developers/testing/isolated-testing/index.md)
*   [Deterministic
    builds](developers/testing/isolated-testing/deterministic-builds.md)
*   [Isolated Testing for SWEs](developers/testing/isolated-testing/for-swes.md)
*   [Isolated Testing
    Infrastructure](developers/testing/isolated-testing/infrastructure.md)
*   [LeakSanitizer](developers/testing/leaksanitizer.md)
*   [LibFuzzer](developers/testing/libfuzzer.md)
*   [MemorySanitizer (MSan)](developers/testing/memorysanitizer.md)
*   [Multi-Process RAM usage analysis on
    Android](developers/testing/multi-process-ram-usage-analysis-on-android.md)
*   [No-compile Tests](developers/testing/no-compile-tests.md)
*   [Page Heap for Chromium](developers/testing/page-heap-for-chrome.md)
*   [Running tests locally](developers/testing/running-tests.md)
*   [ThreadSanitizer (TSan) v. 2](developers/testing/threadsanitizer-tsan-v2.md)
*   [Time Complexity Tests](developers/testing/time-complexity-tests.md)
*   [Try Server usage](developers/testing/try-server-usage/index.md)
*   [Design doc: Try server](developers/testing/try-server-usage/design.md)
*   [UndefinedBehaviorSanitizer
    (UBSan)](developers/testing/undefinedbehaviorsanitizer.md)
*   [WebGL Conformance Tests](developers/testing/webgl-conformance-tests.md)
*   [Layout Tests](developers/testing/webkit-layout-tests/index.md)
*   [Running layout tests using the content
    shell](developers/testing/webkit-layout-tests/content-shell.md)
*   [Fixing layout test
    flakiness](developers/testing/webkit-layout-tests/identifying-tests-that-depend-on-order.md)
*   [Layout Test Expectations and
    Baselines](developers/testing/webkit-layout-tests/testexpectations.md)
*   [Using breakpad with content
    shell](developers/testing/webkit-layout-tests/using-breakpad-with-content-shell.md)
*   [Windows Installer Tests](developers/testing/windows-installer-tests.md)
*   [The JSON Test Results Format](developers/the-json-test-results-format.md)
*   [The Rendering Critical Path](developers/the-rendering-critical-path.md)
*   [The Zen of Merge Requests](developers/the-zen-of-merge-requests.md)
*   [Thread and Task Profiling and
    Tracking](developers/threaded-task-tracking.md)
*   [ticket-milestone-punting](developers/ticket-milestone-punting/index.md)
*   [Ticket Milestone Punting](developers/ticket-milestone-punting-1.md)
*   [Ticket Milestone
    Punting](developers/ticket-milestone-punting/ticket-milestone-punting-1.md)
*   [Tools we use in Chromium](developers/tools-we-use-in-chromium_index.md)
*   [GRIT](developers/tools-we-use-in-chromium/grit/index.md)
*   [GRIT Design
    Overview](developers/tools-we-use-in-chromium/grit/grit-design-overview.md)
*   [GRIT Regression Test
    Plan](developers/tools-we-use-in-chromium/grit/grit-regression-test-plan.md)
*   [GRIT User's
    Guide](developers/tools-we-use-in-chromium/grit/grit-users-guide.md)
*   [How to contribute to
    GRIT](developers/tools-we-use-in-chromium/grit/how-to-contribute-to-grit.md)
*   [Tree Sheriffs](developers/tree-sheriffs/index.md)
*   [Chrome on Chrome OS
    Gardening](developers/tree-sheriffs/chrome-in-chromeos-gardening.md)
*   [Chromium OS Sheriff FAQ](developers/tree-sheriffs/new-chromium-os-faq.md)
*   [Perf Sheriffing](developers/tree-sheriffs/perf-sheriffs/index.md)
*   [Bisecting Performance
    Regressions](developers/tree-sheriffs/perf-sheriffs/bisecting-performance-regressions/index.md)
*   [Sheriff Details:
    Chromium](developers/tree-sheriffs/sheriff-details-chromium/index.md)
*   [Sheriff FAQ: Chromium
    OS](developers/tree-sheriffs/sheriff-details-chromium-os/index.md)
*   [Chromium OS Commit Queue
    Overview](developers/tree-sheriffs/sheriff-details-chromium-os/commit-queue-overview/index.md)
*   [Bypassing tests on a per-project
    basis](developers/tree-sheriffs/sheriff-details-chromium-os/commit-queue-overview/bypassing-tests-on-a-per-project-basis.md)
*   [Pre Flight Queue
    FAQ](developers/tree-sheriffs/sheriff-details-chromium-os/pre-flight-queue-faq.md)
*   [Sheriff FAQ: Chromium OS ASAN
    bots](developers/tree-sheriffs/sheriff-details-chromium-os/sheriff-faq-chromium-os-asan-bots.md)
*   [Sheriff Log: Chromium OS
    (go/croslog)](developers/tree-sheriffs/sheriff-details-chromium-os/sheriff-log-chromium-os/index.md)
*   [Sheriff Log: Chromium OS
    (ARCHIVE!)](developers/tree-sheriffs/sheriff-details-chromium-os/sheriff-log-chromium-os/archive.md)
*   [Handling a failing
    test](developers/tree-sheriffs/sheriff-details-chromium/handling-a-failing-test.md)
*   [Memory
    sheriff](developers/tree-sheriffs/sheriff-details-chromium/memory-sheriff.md)
*   [Sheriff details: NaCl](developers/tree-sheriffs/sheriff-details-nacl.md)
*   [sheriff-o-matic](developers/tree-sheriffs/sheriff-o-matic.md)
*   [Sheriff Philosophy](developers/tree-sheriffs/sheriff-philosophy.md)
*   [Sheriffing Bug Queues](developers/tree-sheriffs/sheriffing-bug-queues.md)
*   [Chromium Triggered Reset API
    (Windows-only)](developers/triggered-reset-api.md)
*   [U-Boot](developers/u-boot/index.md)
*   [Updating WebUI for Material
    Design](developers/updating-webui-for-material-design/index.md)
*   [Settings: Material
    Design](developers/updating-webui-for-material-design/settings-material-design.md)
*   [Testing asynchronous
    UI](developers/updating-webui-for-material-design/testing-asynchronous-ui.md)
*   [Testing WebUI with
    Mocha](developers/updating-webui-for-material-design/testing-webui-with-mocha.md)
*   [Adding SVG
    icons](developers/updating-webui-for-material-design/using-polymer-icons.md)
*   [Useful extensions for developers](developers/useful-extensions.md)
*   [Using Atom as your IDE](developers/using-atom-as-your-ide.md)
*   [Using Eclipse with Chromium](developers/using-eclipse-with-chromium.md)
*   [Using requestAutocomplete()](developers/using-requestautocomplete/index.md)
*   [Version Numbers](developers/version-numbers.md)
*   [Web Development Style Guide](developers/web-development-style-guide.md)
*   [Web IDL interfaces](developers/web-idl-interfaces.md)
*   [Web Intents in Chrome](developers/web-intents-in-chrome/index.md)
*   [Web Platform Status](developers/web-platform-status/index.md)
*   [HTML5 Forms Status](developers/web-platform-status/forms.md)
*   [Blink Core Projects](developers/webkit-core-projects.md)
*   [WebKit development workflow](developers/webkit-development-workflow.md)
*   [Creating Chrome WebUI Interfaces](developers/webui.md)
*   [Windows Binary Sizes](developers/windows-binary-sizes.md)
*   [Google Chrome Developer Tools](devtools/index.md)
*   [Announcements](devtools/announcements.md)
*   [breakpoints-tutorial](devtools/breakpoints-tutorial/index.md)
*   [Capturing a Timeline Trace](devtools/capturing-a-timeline-trace/index.md)
*   [Google Chrome Developer Tools
    Tutorial](devtools/google-chrome-developer-tools-tutorial/index.md)
*   [DirectWrite Font Proxy](directwrite-font-proxy/index.md)
*   [Embedded Search API](embeddedsearch.md)
*   [Flash Roadmap](flash-roadmap_index.md)
*   [Flash Usage Trends](flash-roadmap/flash-usage-trends/index.md)
*   [For Testers](for-testers/index.md)
*   [Bug Life Cycle and Reporting
    Guidelines](for-testers/bug-reporting-guidelines/index.md)
*   [Chromium Bug
    Labels](for-testers/bug-reporting-guidelines/chromium-bug-labels.md)
*   [Reporting a Hang
    Bug](for-testers/bug-reporting-guidelines/hanging-tabs/index.md)
*   [Reporting a Crash
    Bug](for-testers/bug-reporting-guidelines/reporting-crash-bug.md)
*   [Triage Best
    Practices](for-testers/bug-reporting-guidelines/triage-best-practices.md)
*   [Uncaught SecurityError: Failed to read the 'localStorage' property from
    'Window': Access is denied for this
    document.](for-testers/bug-reporting-guidelines/uncaught-securityerror-failed-to-read-the-localstorage-property-from-window-access-is-denied-for-this-document/index.md)
*   [Bug Reporting Guidlines for the Mac & Linux
    builds](for-testers/bug-reporting-guidlines-for-the-mac-linux-builds.md)
*   [How to specify command line flags](for-testers/command-line-flags.md)
*   [How to enable logging](for-testers/enable-logging/index.md)
*   [FAFT](for-testers/faft/index.md)
*   [Frontend testing](for-testers/frontend-testing_index.md)
*   [Constrained Windows (test
    plan)](for-testers/frontend-testing/constrained-windows/index.md)
*   [Constrained Windows: Basic
    Testing](for-testers/frontend-testing/constrained-windows/constrained-windows--basic-testing.md)
*   [Spell Check (test plan)](for-testers/frontend-testing/spell-check/index.md)
*   [Spell Check: Basic
    Testing](for-testers/frontend-testing/spell-check/spell-check--basic-testing.md)
*   [Windows-specific interoperability
    issues](for-testers/frontend-testing/windows-specific-interoperability-issues/index.md)
*   [Installer (test plan)](for-testers/installer/index.md)
*   [Installer: Basic
    Testing](for-testers/installer/installer--basic-testing.md)
*   [Reporting memory bloat/leak
    issues](for-testers/providing-memory-details.md)
*   [How to capture a NetLog
    dump](for-testers/providing-network-details/index.md)
*   [SAFT](for-testers/saft.md)
*   [Test Cases](for-testers/test-cases.md)
*   [Touch Firmware Tests](for-testers/touch-firmware-tests/index.md)
*   [Unhealthy tests report](for-testers/unhealthy-tests-report.md)
*   [Getting Involved](getting-involved/index.md)
*   [Become a Committer](getting-involved/become-a-committer.md)
*   [Triaging Bugs](getting-involved/bug-triage.md)
*   [Chrome Release Channels](getting-involved/dev-channel/index.md)
*   [Download Chromium](getting-involved/download-chromium.md)
*   [Get Bug-Editing Privileges](getting-involved/get-bug-editing-privileges.md)
*   [SummerOfCode2013](getting-involved/summerofcode2013.md)
*   [Glossary](glossary.md)
*   [HTTP Strict Transport Security](hsts/index.md)
*   [Infra](infra.md)
*   [IRC Support FAQ](irc-support-faq.md)
*   [IRC Support FAQ](irc-support-faq-1.md)
*   [Issue Tracking](issue-tracking/index.md)
*   [Sheriffbot: Auto-triage Rules for Bugs](issue-tracking/autotriage.md)
*   [Creating a Bug Template Url](issue-tracking/creating-a-bug-template-url.md)
*   [Editing Components](issue-tracking/editing-components/index.md)
*   [How to Bulk Edit](issue-tracking/how-to-bulk-edit/index.md)
*   [Issue Tracking Lifecycle](issue-tracking/issue-tracking-lifecycle/index.md)
*   [Label and Component Naming Style
    Guide](issue-tracking/label-and-component-naming-style-guide.md)
*   [Migrating Issue
    Components](issue-tracking/migrating-issue-components/index.md)
*   [Release Block Guidelines](issue-tracking/release-block-guidelines.md)
*   [Requesting a Component or
    Label](issue-tracking/requesting-a-component-or-label.md)
*   [Tips and Tricks](issue-tracking/tips-and-tricks.md)
*   [Layout Test Contest](index.md)
*   [layout-test-contest-leaderboard](layout-test-contest/layout-test-contest-leaderboard/index.md)
*   [Updated Leaderboard Fri Oct 11 2013 15:48:57 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-fri-oct-11-2013-15-48-57-gmt-0700-pdt.md)
*   [Updated Leaderboard Fri Oct 11 2013 16:48:09 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-fri-oct-11-2013-16-48-09-gmt-0700-pdt.md)
*   [Updated Leaderboard Fri Oct 18 2013 17:25:20 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-fri-oct-18-2013-17-25-20-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 07 2013 13:55:08 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-07-2013-13-55-08-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 07 2013 14:57:47 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-07-2013-14-57-47-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 07 2013 15:05:48 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-07-2013-15-05-48-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 07 2013 15:28:28 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-07-2013-15-28-28-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 14 2013 10:23:23 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-14-2013-10-23-23-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 14 2013 10:24:05 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-14-2013-10-24-05-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 14 2013 10:24:44 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-14-2013-10-24-44-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 14 2013 10:26:17 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-14-2013-10-26-17-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 14 2013 10:26:43 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-14-2013-10-26-43-gmt-0700-pdt.md)
*   [Updated Leaderboard Mon Oct 14 2013 11:34:09 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-mon-oct-14-2013-11-34-09-gmt-0700-pdt.md)
*   [Updated Leaderboard Sun Oct 27 2013 16:30:36 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-sun-oct-27-2013-16-30-36-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 10 2013 14:40:46 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-10-2013-14-40-46-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 10 2013 16:36:05 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-10-2013-16-36-05-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 10 2013 16:56:34 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-10-2013-16-56-34-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 10 2013 17:28:08 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-10-2013-17-28-08-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 17 2013 00:17:03 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-17-2013-00-17-03-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 17 2013 08:02:26 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-17-2013-08-02-26-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 17 2013 11:18:35 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-17-2013-11-18-35-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 17 2013 11:21:06 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-17-2013-11-21-06-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 17 2013 12:32:46 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-17-2013-12-32-46-gmt-0700-pdt.md)
*   [Updated Leaderboard Thu Oct 24 2013 10:15:27 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-thu-oct-24-2013-10-15-27-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 08 2013 09:39:07 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-08-2013-09-39-07-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 08 2013 14:03:58 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-08-2013-14-03-58-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 08 2013 16:24:01 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-08-2013-16-24-01-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 08 2013 16:38:14 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-08-2013-16-38-14-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 08 2013 16:38:45 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-08-2013-16-38-45-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 08 2013 16:56:36 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-08-2013-16-56-36-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 08 2013 17:18:19 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-08-2013-17-18-19-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 15 2013 16:06:33 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-15-2013-16-06-33-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 22 2013 08:07:44 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-22-2013-08-07-44-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 22 2013 12:39:53 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-22-2013-12-39-53-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 22 2013 15:31:43 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-22-2013-15-31-43-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 22 2013 15:44:22 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-22-2013-15-44-22-gmt-0700-pdt.md)
*   [Updated Leaderboard Tue Oct 29 2013 07:49:13 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-tue-oct-29-2013-07-49-13-gmt-0700-pdt.md)
*   [Updated Leaderboard Wed Oct 09 2013 14:50:58 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-wed-oct-09-2013-14-50-58-gmt-0700-pdt.md)
*   [Updated Leaderboard Wed Oct 16 2013 12:59:55 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-wed-oct-16-2013-12-59-55-gmt-0700-pdt.md)
*   [Updated Leaderboard Wed Oct 16 2013 13:15:03 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-wed-oct-16-2013-13-15-03-gmt-0700-pdt.md)
*   [Updated Leaderboard Wed Oct 23 2013 13:06:58 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-wed-oct-23-2013-13-06-58-gmt-0700-pdt.md)
*   [Updated Leaderboard Wed Oct 23 2013 14:06:11 GMT-0700
    (PDT)](layout-test-contest/layout-test-contest-leaderboard/updated-leaderboard-wed-oct-23-2013-14-06-11-gmt-0700-pdt.md)
*   [Native Client](nativeclient/index.md)
*   [6: Day-to-Day Engineering](nativeclient/day-to-day/index.md)
*   [Native Client Code Review and Check-in
    Process](nativeclient/day-to-day/codereview.md)
*   [Infrastructure FAQ](nativeclient/day-to-day/infrastructure-faq.md)
*   [1: Getting Started](nativeclient/getting-started_index.md)
*   [Getting Started: Background and
    Basics](nativeclient/getting-started/getting-started-background-and-basics/index.md)
*   [2: How Tos](nativeclient/how-tos/index.md)
*   [3D Tips and Best
    Practices](nativeclient/how-tos/3d-tips-and-best-practices.md)
*   [Building and Testing the Native Client Trusted Code
    Base](nativeclient/how-tos/build-tcb.md)
*   [Building and Testing GCC and GNU
    binutils](nativeclient/how-tos/building-and-testing-gcc-and-gnu-binutils.md)
*   [Debugging
    Documentation](nativeclient/how-tos/debugging-documentation/index.md)
*   [Debugging a Trusted
    Plugin](nativeclient/how-tos/debugging-documentation/debugging-a-trusted-plugin/index.md)
*   [Debugging a Trusted Plugin on
    Linux](nativeclient/how-tos/debugging-documentation/debugging-a-trusted-plugin/debugging-a-trusted-plugin-on-linux.md)
*   [Debugging a Trusted Plugin on
    Windows](nativeclient/how-tos/debugging-documentation/debugging-a-trusted-plugin/debugging-trusted-plugin-on-windows.md)
*   [Debugging a Trusted Plugin on
    Mac](nativeclient/how-tos/debugging-documentation/debugging-a-trusted-plugin/trusted-debugging-on-mac.md)
*   [Debugging with debug stub
    (recommended)](nativeclient/how-tos/debugging-documentation/debugging-with-debug-stub-recommended/index.md)
*   [Debugging NaCl apps in Chrome
    OS](nativeclient/how-tos/debugging-documentation/debugging-with-debug-stub-recommended/debugging-nacl-apps-in-chrome-os.md)
*   [Debugging NaCl apps in Eclipse
    CDT](nativeclient/how-tos/debugging-documentation/debugging-with-debug-stub-recommended/debugging-nacl-apps-in-eclipse-cdt/index.md)
*   [Getting started with debug
    stub](nativeclient/how-tos/debugging-documentation/debugging-with-debug-stub-recommended/getting-started-with-debug-stub.md)
*   [Running under
    Valgrind](nativeclient/how-tos/debugging-documentation/running-under-valgrind.md)
*   [Exception Handling
    Interface](nativeclient/how-tos/exception-handling-interface.md)
*   [Developing Native
    Client](nativeclient/how-tos/how-to-use-git-svn-with-native-client.md)
*   [How to write assembler for x86 NaCl
    platform.](nativeclient/how-tos/how-to-write-assembler-for-x86-nacl-platform.md)
*   [Working on the git toolchain
    repos](nativeclient/how-tos/working-on-the-git-toolchain-repos.md)
*   [The life of sel_ldr](nativeclient/life-of-sel_ldr.md)
*   [nameservice](nativeclient/nameservice/index.md)
*   [naming: issues and use
    cases](nativeclient/nameservice/naming-issues----rationale.md)
*   [PNaCl](nativeclient/pnacl/index.md)
*   [Aligned bundling support in
    LLVM](nativeclient/pnacl/aligned-bundling-support-in-llvm/index.md)
*   [PNaCl Bitcode Reference Manual](nativeclient/pnacl/bitcode-abi.md)
*   [Building and using PNaCl components for Chromium distribution
    packagers](nativeclient/pnacl/building-pnacl-components-for-distribution-packagers.md)
*   [Developing PNaCl](nativeclient/pnacl/developing-pnacl.md)
*   [Experimenting with generated
    bitcode](nativeclient/pnacl/experimenting-with-generated-bitcode.md)
*   [Introduction to Portable Native
    Client](nativeclient/pnacl/introduction-to-portable-native-client.md)
*   [Stability of the PNaCl bitcode
    ABI](nativeclient/pnacl/stability-of-the-pnacl-bitcode-abi/index.md)
*   [Subzero](nativeclient/pnacl/subzero/index.md)
*   [Simple loop example](nativeclient/pnacl/subzero/simple-loop-example.md)
*   [4: Reference](nativeclient/reference/index.md)
*   [Anatomy of a Syscall](nativeclient/reference/anatomy-of-a-sys/index.md)
*   [Overview of Native Client for ARM](nativeclient/reference/arm-overview.md)
*   [External Resource
    Directory](nativeclient/reference/external-resource-directory.md)
*   [Research Papers](nativeclient/reference/research-papers/index.md)
*   [Native Client Coding Style Guidelines](nativeclient/styleguide.md)
*   [Network Speed Experiments](network-speed-experiments.md)
*   [Omnibox: History Provider](omnibox-history-provider/index.md)
*   [Omnibox History Quick Provider Index
    Caching](omnibox-history-provider/caching/index.md)
*   [QUIC, a multiplexed stream transport over UDP](quic/index.md)
*   [Playing with QUIC](quic/playing-with-quic.md)
*   [QUIC FAQ](quic/quic-faq.md)
*   [Rvalue references in Chromium](rvalue-references.md)
*   [SearchBox API](searchbox.md)
*   [security/test](security-test.md)
*   [Servicification](servicification.md)
*   [SPDY](spdy/index.md)
*   [HTTP/2](spdy/http2.md)
*   [Server Push and Server Hints](spdy/link-headers-and-server-hint/index.md)
*   [LINK
    rel=subresource](spdy/link-headers-and-server-hint/link-rel-subresource.md)
*   [Running flip_in_mem_edsm_server](spdy/running_flipinmemserver/index.md)
*   [SPDY Server and Proxy Authentication](spdy/spdy-authentication.md)
*   [SPDY Best Practices](spdy/spdy-best-practices.md)
*   [SPDY data](spdy/spdy-data/index.md)
*   [SPDY Protocol](spdy/spdy-protocol/index.md)
*   [SPDY Protocol - Draft 1](spdy/spdy-protocol/spdy-protocol-draft1/index.md)
*   [ControlFrameTypes](spdy/spdy-protocol/spdy-protocol-draft1/controlframetypes.md)
*   [SPDY Protocol - Draft 2](spdy/spdy-protocol/spdy-protocol-draft2.md)
*   [SPDY Protocol - Draft 3](spdy/spdy-protocol/spdy-protocol-draft3.md)
*   [SPDY Protocol - Draft 3.1](spdy/spdy-protocol/spdy-protocol-draft3-1.md)
*   [SPDY Protocol - Draft 3.2](spdy/spdy-protocol/spdy-protocol-draft3-2.md)
*   [SPDYProtocol.xml](spdy/spdy-protocol/spdyprotocol-xml.md)
*   [SPDY Proxy](spdy/spdy-proxy.md)
*   [SPDY Proxy Examples](spdy/spdy-proxy-examples.md)
*   [SPDY Tools and Debugging](spdy/spdy-tools-and-debugging.md)
*   [SPDY: An experimental protocol for a faster
    web](spdy/spdy-whitepaper/index.md)
*   [HSTS](sts/index.md)
*   [Tab to Search](tab-to-search.md)
*   [Teams](teams/_index.md)
*   [Binding Team](teams/binding-team.md)
*   [Device Team](teams/device-team/index.md)
*   [Device OKRs](teams/device-team/device-okrs.md)
*   [DOM Team](teams/dom-team.md)
*   [Input Team](teams/index.md)
*   [Input Objectives](teams/input-dev/input-objectives/index.md)
*   [2015 OKRs](teams/input-dev/input-objectives/2015-okrs.md)
*   [2016 OKRs](teams/input-dev/input-objectives/2016-okrs.md)
*   [2017 OKRS](teams/input-dev/input-objectives/2017-okrs.md)
*   [Layout Team](teams/layout-team/index.md)
*   [Bug Triage Instructions](teams/layout-team/bug-triage.md)
*   [Eliminating Simple Text](teams/layout-team/eliminating-simple-text.md)
*   [Layout Team Meeting Notes](teams/layout-team/meeting-notes/index.md)
*   [Tuesday, December 9, 2014](teams/layout-team/meeting-notes/20141209.md)
*   [Q1 OKRs - Wednesday, January 7,
    2015](teams/layout-team/meeting-notes/20150107.md)
*   [Friday, October 2, 2015 (Q3/Q4
    OKRs)](teams/layout-team/meeting-notes/friday-october-2-2015.md)
*   [Monday, March 2, 2015](teams/layout-team/meeting-notes/march-2-2015.md)
*   [Thursday, May 28, 2015: Layout Moose post BlinkOn Sync
    Up](teams/layout-team/meeting-notes/may-28-2015.md)
*   [Monday, April 11,
    2016](teams/layout-team/meeting-notes/monday-april-11-2016.md)
*   [Monday, April 13,
    2015](teams/layout-team/meeting-notes/monday-april-13-2015.md)
*   [Monday, April 20,
    2015](teams/layout-team/meeting-notes/monday-april-20-2015.md)
*   [Monday, April 25,
    2016](teams/layout-team/meeting-notes/monday-april-25-2016.md)
*   [Monday, April 27,
    2015](teams/layout-team/meeting-notes/monday-april-27-2015.md)
*   [Monday, April 4,
    2016](teams/layout-team/meeting-notes/monday-april-4-2016.md)
*   [Monday, April 6,
    2015](teams/layout-team/meeting-notes/monday-april-6-2015.md)
*   [Monday, August 10,
    2015](teams/layout-team/meeting-notes/monday-august-10-2015.md)
*   [Monday, August 17,
    2015](teams/layout-team/meeting-notes/monday-august-17-2015.md)
*   [Monday, August 24,
    2015](teams/layout-team/meeting-notes/monday-august-24-2015.md)
*   [Monday, August 29,
    2016](teams/layout-team/meeting-notes/monday-august-29-2016.md)
*   [Monday, August 3,
    2015](teams/layout-team/meeting-notes/monday-august-3-2015.md)
*   [Monday, August 31,
    2015](teams/layout-team/meeting-notes/monday-august-31-2015.md)
*   [Monday, February 1,
    2016](teams/layout-team/meeting-notes/monday-february-1-2016.md)
*   [Monday, February 2,
    2015](teams/layout-team/meeting-notes/monday-february-2.md)
*   [Monday, March 21,
    2016](teams/layout-team/meeting-notes/monday-february-21-2016.md)
*   [Monday, February 22,
    2016](teams/layout-team/meeting-notes/monday-february-22-2016.md)
*   [Monday, February 23,
    2015](teams/layout-team/meeting-notes/monday-february-23-2015.md)
*   [Monday, March 28,
    2016](teams/layout-team/meeting-notes/monday-february-28-2016.md)
*   [Monday, February 29,
    2016](teams/layout-team/meeting-notes/monday-february-29-2016.md)
*   [Monday, February 8,
    2016](teams/layout-team/meeting-notes/monday-february-8-2016.md)
*   [Monday, February 9,
    2015](teams/layout-team/meeting-notes/monday-february-9.md)
*   [Monday, January 11,
    2016](teams/layout-team/meeting-notes/monday-january-11-2016.md)
*   [Monday, January 4,
    2016](teams/layout-team/meeting-notes/monday-january-4-2016.md)
*   [Monday, July 13,
    2015](teams/layout-team/meeting-notes/monday-july-13-2015.md)
*   [Monday, July 18,
    2016](teams/layout-team/meeting-notes/monday-july-18-2016.md)
*   [Monday, July 20,
    2015](teams/layout-team/meeting-notes/monday-july-20-2015.md)
*   [Monday, June 1,
    2015](teams/layout-team/meeting-notes/monday-june-1-2015.md)
*   [Monday, June 22,
    2015](teams/layout-team/meeting-notes/monday-june-22-2015.md)
*   [Monday, June 27,
    2016](teams/layout-team/meeting-notes/monday-june-27-2016.md)
*   [Monday, June 6,
    2016](teams/layout-team/meeting-notes/monday-june-6-2016.md)
*   [Monday, June 8,
    2015](teams/layout-team/meeting-notes/monday-june-8-2015.md)
*   [Monday, March 16,
    2015](teams/layout-team/meeting-notes/monday-march-16-2015.md)
*   [Monday, March 23,
    2015](teams/layout-team/meeting-notes/monday-march-23-2015.md)
*   [Monday, March 30,
    2015](teams/layout-team/meeting-notes/monday-march-30-2015.md)
*   [Monday, March 7,
    2016](teams/layout-team/meeting-notes/monday-march-7-2016.md)
*   [Monday, March 9,
    2015](teams/layout-team/meeting-notes/monday-march-9-2015.md)
*   [Monday, May 16,
    2016](teams/layout-team/meeting-notes/monday-may-16-2016.md)
*   [Monday, May 4, 2015](teams/layout-team/meeting-notes/monday-may-4-2015.md)
*   [Monday, November 23,
    2015](teams/layout-team/meeting-notes/monday-november-23-2015.md)
*   [Monday, November 30,
    2015](teams/layout-team/meeting-notes/monday-november-30-2015.md)
*   [Monday, October 12,
    2015](teams/layout-team/meeting-notes/monday-october-12-2015.md)
*   [Monday, October 19,
    2015](teams/layout-team/meeting-notes/monday-october-19-2015.md)
*   [Monday, October 5,
    2015](teams/layout-team/meeting-notes/monday-october-5-2015.md)
*   [Monday, September 14,
    2015](teams/layout-team/meeting-notes/monday-september-14-2015.md)
*   [Monday, September 28,
    2015](teams/layout-team/meeting-notes/monday-september-28-2015.md)
*   [Thursday, April 2, 2015: Q1/Q2
    OKRs](teams/layout-team/meeting-notes/thursday-april-2-2015.md)
*   [Thursday, June 4, 2015: Mid-Q2 OKR
    Check-in](teams/layout-team/meeting-notes/thursday-june-4-2015.md)
*   [Tuesday, February 16,
    2016](teams/layout-team/meeting-notes/tuesday-february-16-2016.md)
*   [Tuesday, June 16,
    2015](teams/layout-team/meeting-notes/tuesday-june-16-2015.md)
*   [Tuesday, March 15,
    2016](teams/layout-team/meeting-notes/tuesday-march-15-2016.md)
*   [Tuesday, May 26,
    2015](teams/layout-team/meeting-notes/tuesday-may-26-2015.md)
*   [Wednesday, February 18,
    2015](teams/layout-team/meeting-notes/wednesday-february-18.md)
*   [Objectives and Key Results](teams/layout-team/okrs/index.md)
*   [2015 Q1](teams/layout-team/okrs/2015q1.md)
*   [2015 Q2](teams/layout-team/okrs/2015q2.md)
*   [2015 Q3](teams/layout-team/okrs/2015q3.md)
*   [2015 Q4](teams/layout-team/okrs/2015q4.md)
*   [2016 Q1](teams/layout-team/okrs/2016q1.md)
*   [Roadmap / Potential Projects](teams/layout-team/potential-projects.md)
*   [Paint Team (paint-dev)](teams/paint-team/index.md)
*   [Canvas team OKRs](teams/paint-team/canvas-okrs.md)
*   [Paint team OKRs](teams/paint-team/okrs.md)
*   [Paint team members](teams/paint-team/paint-team-members.md)
*   [Speed Metrics Team](teams/speed-metrics-team.md)
*   [Style and Animations Team](teams/style-team.md)
*   [Worker Team](teams/worker-team.md)
*   [Anti-DDoS HTTP Throttling of Extension-Originated
    Requests](throttling/index.md)
*   [Anti-DDoS HTTP Throttling in Older Versions of
    Chrome](throttling/anti-ddos-http-throttling-in-older-versions-of-chrome.md)
*   [User Experience](user-experience/index.md)
*   [Accessibility: Assistive technology
    support](user-experience/assistive-technology-support.md)
*   [Bookmarklets](user-experience/bookmarklets/index.md)
*   [Bookmarks](user-experience/bookmarks/index.md)
*   [Downloads](user-experience/downloads/index.md)
*   [Downloads](user-experience/downloads-1.md)
*   [Subscribing to feeds](user-experience/feed-subscriptions/index.md)
*   [Find in Page](user-experience/find-in-page/index.md)
*   [History](user-experience/history/index.md)
*   [Incognito](user-experience/incognito/index.md)
*   [Infobars](user-experience/infobars/index.md)
*   [Geolocation](user-experience/infobars/geolocation/index.md)
*   [Accessibility: Keyboard Access](user-experience/keyboard-access.md)
*   [Accessibility: Low-Vision Support](user-experience/low-vision-support.md)
*   [Multiple Chrome Users](user-experience/multi-profiles/index.md)
*   [Multitouch](user-experience/multitouch/index.md)
*   [New Tab Page](user-experience/new-tab-page/index.md)
*   [Notifications](user-experience/notifications/index.md)
*   [Omnibox](user-experience/omnibox/index.md)
*   [Omnibox Bug Triage
    Process](user-experience/omnibox/omnibox-bug-triage-process.md)
*   [Options](user-experience/options/index.md)
*   [Resolution Independence](user-experience/resolution-independence/index.md)
*   [Screen Reader Support](user-experience/screen-reader-support.md)
*   [Status Bubble](user-experience/status-bubble/index.md)
*   [Tabs](user-experience/tabs/index.md)
*   [Throbber](user-experience/tabs/throbber/index.md)
*   [Toolbar](user-experience/toolbar.md)
*   [Accessibility: Touch Access](user-experience/touch-access/index.md)
*   [User Data Directory](user-experience/user-data-directory.md)
*   [Visual Design](user-experience/visual-design/index.md)
*   [Window Frame](user-experience/window-frame/index.md)
*   [X-Subresources](x-subresources/index.md)
*   [X-Subresources: python server
    experiment](x-subresources/x-subresources-1.md)
