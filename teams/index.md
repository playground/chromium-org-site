# Input Team

*The Chromium Input team (aka input-dev)* is a web platform team focused on
making touch (P1) and other forms of input (P2) awesome on the web. High-level
goals:

*   Measure & improve input-to-paint latency. Web pages should stick to your
    finger!
*   Reduce web-developer pain points around input and scrolling, including
    improving interoperability between browser engines, predictability,
    simplicity and consistency between chromium platforms.
*   Enable high-performance rich touch interactions (such as pull-to-refresh) on
    the web.
*   Ensure Chrome UI features driven by input over the contents area provide a
    great experience to both end-users and web developers.
*   Enable the productivity of any chromium developer working with input-related
    code through effective code ownership and code health initiatives.

See also our concrete [quarterly
objectives](input-dev/input-objectives/index.md).

**Contacts**

[input-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/input-dev)
is our mailing list, and we sometimes use the #chromium-input-dev IRC channel on
Freenode.

**Useful bug queries**

*   [Hotlist-InputDev](https://code.google.com/p/chromium/issues/list?can=2&q=hotlist:input-dev&sort=pri+mstone&colspec=ID%20Pri%20Mstone%20ReleaseBlock%20OS%20Area%20Feature%20Status%20Owner%20Summary)
    - all (or most) bugs we're tracking
*   Other potentially relevant labels: [Cr-Blink-Input, Cr-Blink-Scroll or
    Cr-Internals-Input-Touch-Screen
    categories](https://code.google.com/p/chromium/issues/list?can=2&q=cr%3Dblink-input%2Cblink-scroll%2Cinternals-input-touch-screen&sort=pri+mstone&colspec=ID+Pri+Mstone+ReleaseBlock+OS+Area+Feature+Status+Owner+Summary&x=m&y=releaseblock&cells=tiles)
*   [Available
    bugs](https://code.google.com/p/chromium/issues/list?can=2&q=hotlist%3Dinput-dev+label%3Apoachable+OR+hotlist%3Dinput-dev+status%3Davailable&colspec=ID+Pri+M+Week+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)
*   [Code cleanup
    ideas](https://code.google.com/p/chromium/issues/list?can=2&q=hotlist%3Dinput-dev+label%3ACodeHealth&colspec=ID+Pri+M+Week+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)

**Key Learning Resources**

*   [Chromium touch pipeline
    overview](https://docs.google.com/a/chromium.org/presentation/d/10oIOTWFKIHArnfk8ZZx-9evvDpWC9EwRjDrZIz83Dz0/edit)
*   [Scrolling in blink](http://bit.ly/blink-scrolling)
*   [Android tracing cheat
    sheet](https://docs.google.com/presentation/u/1/d/1poMF7AEu5vd21BzUTIYfm2SXurEMlv2OVDlzs6JNRfg/edit?usp=sharing)
*   [Blink coordinate
    spaces](../developers/design-documents/blink-coordinate-spaces/index.md)
*   [Touch events specification - current
    draft](http://w3c.github.io/touch-events/)
*   W3C [public-touchevents
    archive](http://lists.w3.org/Archives/Public/public-touchevents/)
*   [Touch event behavior details across
    browsers](https://docs.google.com/a/chromium.org/document/d/12k_LL_Ot9GjF8zGWP9eI_3IMbSizD72susba0frg44Y/edit#heading=h.nxfgrfmqhzn7)
*   [Issues with touch
    events](https://docs.google.com/a/chromium.org/document/d/12-HPlSIF7-ISY8TQHtuQ3IqDi-isZVI0Yzv5zwl90VU/edit#heading=h.spopy4jje82p)
*   [Rick Byers' G+ stream](http://plus.google.com/+RickByers)
*   [Debugging common website touch
    issues](https://docs.google.com/a/chromium.org/document/d/1iQtI4f47_gBTCDRALcA4l9HL_h-yZLOdvWLUi2xqJXQ/edit)

**Design Docs**

*   [Chromium input latency
    tracking](https://docs.google.com/a/chromium.org/document/d/1NUYMVyUJSU2NYrpGhKNfjSmmVZyPYoqbrPs7tbdY9PY/edit)
*   [Scroll-blocks-on: scrolling performance vs. richness
    tradeoff](https://docs.google.com/a/chromium.org/document/d/1aOQRw76C0enLBd0mCG_-IM6bso7DxXwvqTiRWgNdTn8/edit)
*   [Chromium throttled async touchmove
    scrolling](https://docs.google.com/a/chromium.org/document/d/1sfUup3nsJG3zJTf0YR0s2C5vgFTYEmfEqZs01VVj8tE/edit)
*   [Gesture
    Recognition](http://www.chromium.org/developers/design-documents/aura/gesture-recognizer)
*   [Vsync-aligned buffered
    input](https://docs.google.com/document/d/1L2JTgYMksmXgujKxxhyV45xL8jNhbCh60NQHoueKyS4/edit?usp=sharing)
*   **TODO: more**

**Presentations / talks**

*   Extensible Scrolling - BlinkOn 3 (Nov 2014):
    [slides](https://docs.google.com/a/chromium.org/presentation/d/1P5LYe-jqC0mSFJoBDz8gfJZMDwj6aGeFYLx_AD6LHVU/edit#slide=id.p),
    [video](https://www.youtube.com/watch?v=L8aTuoQWI8A)
*   [Smooth to the touch: chromium's challenges in input on
    mobile](https://docs.google.com/a/chromium.org/presentation/d/1VYfCKye4TM-QiR_hiLvwYxhci_xc5YcA4oZxtrp2qes/edit#slide=id.p)
    - BlinkOn 2 (May 2014)
*   [Input - BlinkOn
    1](https://docs.google.com/a/chromium.org/presentation/d/1J1jG0XF6k42PA4s-otHFXZxrou7aKwYKYF90xPOe9bQ/edit#slide=id.p)
    (Sept 2013)
*   [Point, click, tap, touch - Building multi-device web
    interfaces](https://developers.google.com/events/io/sessions/361772634) -
    Google I/O 2013
