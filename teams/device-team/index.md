# Device Team

The device team works on increasing Web App capabilities by exposing device
features.

Components:

*   Blink>Bluetooth
*   Blink>GamepadAPI
*   Blink>Location
*   Blink>NFC
*   Blink>Sensor
*   Blink>USB
*   Blink>WakeLock
*   IO>Bluetooth
*   IO>Gamepad
*   IO>USB

## Communication

*   device-dev@chromium.org [email
    group](https://groups.google.com/a/chromium.org/forum/#!forum/device-dev).
*   IRC freenode #chromium-devices:
    <http://webchat.freenode.net/?channels=#chromium-devices>
    *   Be patient for a response, consider IRC Cloud for persistent
        connections.
*   #devices channel on Chromium Dev Slack:
    <https://chromiumdev-slack.herokuapp.com/>

## [Objectives and Key Results (OKRs)](device-okrs.md)

## Team Members

*   cco3@chromium.org
*   juncai@chromium.org
*   mattreynolds@chromium.org
*   reillyg@chromium.org
*   scheib@chromium.org (TL)

## Triage

#### Device Team Triage Links

Device Team Triage Links

#### DeviceDev Triage Guidelines

DeviceDev Triage Guidelines
