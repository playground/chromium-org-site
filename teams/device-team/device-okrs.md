# Device OKRs

We use [Objectives and Key Results](https://en.wikipedia.org/wiki/OKR) to
communicate the work we plan to do over the next several months. They are a
snapshot, and work may change when justified. We aspire to achieve roughly 70%
of these goals.

## 2016 Q2 Planned

Own Device components

Owners: [conleyo](https://easyokrs.googleplex.com/view/conleyo/),
[device-api](https://easyokrs.googleplex.com/view/device-api/),
[dft](https://easyokrs.googleplex.com/view/dft/),
[juncai](https://easyokrs.googleplex.com/view/juncai/),
[jyasskin](https://easyokrs.googleplex.com/view/jyasskin/),
[mattreynolds](https://easyokrs.googleplex.com/view/mattreynolds/),
[mcasas](https://easyokrs.googleplex.com/view/mcasas/),
[ortuno](https://easyokrs.googleplex.com/view/ortuno/),
[reillyg](https://easyokrs.googleplex.com/view/reillyg/),
[scheib](https://easyokrs.googleplex.com/view/scheib/)

*   Several new components were taken on by the newly chartered
    [go/deviceteam](https://goto.google.com/deviceteam). It now includes:
    AmbientLight Bluetooth DeviceOrientation GamepadAPI ImageCapture Location
    USB WakeLock
    [#wp-predictability](https://easyokrs.googleplex.com/search/?q=wp-predictability)

*   P0Triage SLO met
*   Several components were not previously being monitored.
*   P1Backlogs populated and prioritized for components & team.
*   P2Specifications moved to appropriate working groups
*   E.g. geolocation needs updates for error messages

P1Create Web Platform Tests

Owners: [device-api](https://easyokrs.googleplex.com/view/device-api/),
[scheib](https://easyokrs.googleplex.com/view/scheib/)

*   [#wp-predictability](https://easyokrs.googleplex.com/search/?q=wp-predictability)

*   P1Web Bluetooth Web Platform Tests upstreamed
*   Owner: [ortuno](https://easyokrs.googleplex.com/view/ortuno/)
*   P2WebUSB Web Platform Tests upstreamed
*   Owner: [reillyg](https://easyokrs.googleplex.com/view/reillyg/)

P1Servicification of device on target for end of year 2017

Owners: [device-api](https://easyokrs.googleplex.com/view/device-api/),
[scheib](https://easyokrs.googleplex.com/view/scheib/)

*   The servicification team has an annual OKR
    <https://easyokrs.googleplex.com/o/2536591>/ which includes
    [go/deviceservice](https://goto.google.com/deviceservice), which Device team
    needs to help with.
    [#wp-architecture](https://easyokrs.googleplex.com/search/?q=wp-architecture)

*   P1Gamepad work scoped
*   Owner: [mattreynolds](https://easyokrs.googleplex.com/view/mattreynolds/)
*   Gamepad has not been maintained, it needs investigation to determine how to
    proceed beyond current blocker blundell identified: "Not obvious how to
    unwind coupling to ContentViewCore.java. Has browser-side clients: Ash and
    Pepper hosting"
*   P2WebUSB consuming USB from the device service. (stretch)
*   Owners: [juncai](https://easyokrs.googleplex.com/view/juncai/),
    [reillyg](https://easyokrs.googleplex.com/view/reillyg/)
*   P2Device Sensors
*   Owners: [juncai](https://easyokrs.googleplex.com/view/juncai/),
    [reillyg](https://easyokrs.googleplex.com/view/reillyg/)
*   P2Geolocation
*   Owner: [mcasas](https://easyokrs.googleplex.com/view/mcasas/)

P1Enable new media applications vertical on the web

Category: Web CaptureOwners:
[device-api](https://easyokrs.googleplex.com/view/device-api/),
[mcasas](https://easyokrs.googleplex.com/view/mcasas/)

*   [#wp-fizz](https://easyokrs.googleplex.com/search/?q=wp-fizz)

*   P1Ship Image Capture (M59)
*   <https://w3c.github.io/mediacapture-image>
*   P2Promote Image Capture API publically via Tweets / Additional Blog Posts
*   P2Move Image Capture Spec to CR
*   P2Shape Detection: Start origin trial in M60
*   P2Media Capabilities API for encoding
*   P3\[stretch\] Implement Image Capture Win Capabilities/Settings
*   P3\[stretch\] Shape Detection: Finish the implementation of
    Face/Barcode/Text detection with all sources in Win10

P2Enable new capabilities in Bluetooth, Gamepad, Geolocation, Sensors, USB

Owners: [device-api](https://easyokrs.googleplex.com/view/device-api/),
[scheib](https://easyokrs.googleplex.com/view/scheib/)

*   [#wp-fizz](https://easyokrs.googleplex.com/search/?q=wp-fizz)

*   P1Geolocation quality improvements by using GMSCore
*   Owner: [mcasas](https://easyokrs.googleplex.com/view/mcasas/)
*   P1Geolocation x-geo header designed in coordination with Location Attach
    team
*   Owners: [dft](https://easyokrs.googleplex.com/view/dft/),
    [mcasas](https://easyokrs.googleplex.com/view/mcasas/),
    [scheib](https://easyokrs.googleplex.com/view/scheib/)
*   Search Location Attach team would like to improve the x-geo header quality.
*   P1WebUSB Intent to Ship for M60
*   Owner: [reillyg](https://easyokrs.googleplex.com/view/reillyg/)
*   P1Web Bluetooth high priority issues fixed (those on
    [go/wbbacklog](https://goto.google.com/wbbacklog) higher than Windows)
*   Owner: [ortuno](https://easyokrs.googleplex.com/view/ortuno/)
*   12 issues on the [go/wbbacklog](https://goto.google.com/wbbacklog) are
    higher priority than Windows at start.
*   P2Web Bluetooth connections on Windows (stretch)
*   Owner: [ortuno](https://easyokrs.googleplex.com/view/ortuno/)
*   Windows implementation can not yet connect to already paired devices, nor
    discover devices.
*   P3Web Bluetooth Scanning implemented by Estimote (stretch)
*   Owners: [ortuno](https://easyokrs.googleplex.com/view/ortuno/),
    [scheib](https://easyokrs.googleplex.com/view/scheib/)
*   Work with Estimote to implement scanning API for Web Bluetooth.
*   P3Sensors implemented by Intel (external dep)
*   Owners: [reillyg](https://easyokrs.googleplex.com/view/reillyg/),
    [scheib](https://easyokrs.googleplex.com/view/scheib/)
*   Support Intel in their implementation of sensors APIs
    <https://www.chromestatus.com/features/5698781827825664>
*   P3WebNFC to Origin Trial by Intel (external dep)
*   Owners: [reillyg](https://easyokrs.googleplex.com/view/reillyg/),
    [scheib](https://easyokrs.googleplex.com/view/scheib/)
*   Some uncertainty about timeline as they have several steps outstanding on
    <https://bugs.chromium.org/p/chromium/issues/detail?id=520391> before being
    ready.
*   P3Gamepad DualShock 4 Controller support started by Sony (external dep)
*   Owners: [mattreynolds](https://easyokrs.googleplex.com/view/mattreynolds/),
    [scheib](https://easyokrs.googleplex.com/view/scheib/)
*   We are at idea only stage in Q1, need a design doc, specification proposals,
    and implementation work to start in Q2.
