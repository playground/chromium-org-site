# DOM Team

We evolve, implement and standardize HTML syntax, semantics and DOM.

Some specific HTML elements are owned by other teams:

*   canvas - Paint/Skia Team
*   video, audio, track, cue - Media Team

The team is responsible for the following bug components:

*   Blink>DOM
*   Blink>Editing
*   Blink>Focus
*   Blink>Forms
*   Blink>HTML
*   Blink>XML

## Discussion

Feel free to
[read](https://groups.google.com/a/chromium.org/forum/#!forum/dom-dev) or
[post](mailto:dom-dev@chromium.org) to dom-dev@chromium.org.

## Projects

*   [Web Components](../blink/webcomponents-team.md) (contact:
    [hayato](mailto:hayato@chromium.org))
*   Editing (contact: [yosin](mailto:yosin@chromium.org))

## Team Members

*   dominicc (TL)
*   hayato
*   kochi
*   tkent
*   xiaochengh
*   yoichio
*   yosin
*   yuzus
