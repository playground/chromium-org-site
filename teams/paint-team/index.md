# Paint Team (paint-dev)

This is the home page for the Chromium Paint team (or just Paint team, or
paint-dev).

## Charter

The Paint team's responsibility is to own and evolve:

*   the Paint Invalidation, Paint, 2D Canvas and SVG code in Blink;
*   the interface between Blink and the Chrome Compositor ("cc");
*   and integration between that interface with the mechanics of cc and Skia.

See below for details on what these bullets mean.

Our 2017 mission is to make the code we own as **fast** and **clean** as
possible.

### Details

The *Paint Invalidation* code in Blink is in charge of marking which elements in
the RenderObject and RenderLayer tree need Paint. The relevant code is scattered
across the various classes in
[Source/core/layout/](https://cs.chromium.org/chromium/src/third_party/WebKit/Source/core/layout/).

The *Paint* code in Blink is in charge of mapping from a description of the
style and layout of a tree of RenderObjects to a display list of low-level
graphics commands suitable for giving to a rasterization library such as Skia,
and making decisions for compositing in cc. The relevant code is all
[Source/core/paint/](https://code.google.com/p/chromium/codesearch#chromium/src/third_party/WebKit/Source/core/paint/),
[Source/core/layout/compositing/](https://code.google.com/p/chromium/codesearch#chromium/src/third_party/WebKit/Source/core/layout/compositing/),
plus a few additional locations in Blink.

The *Canvas* code in Blink is sprinkled through the pipeline all the way from
the HTMLCanvasElement to its implementation in each phase of the pipeline.

The *SVG* code in Blink is in
[Source/core/svg](https://code.google.com/p/chromium/codesearch#chromium/src/third_party/WebKit/Source/core/layout/)
as well as the SVG-named files in Source/core/layout and Source/core/paint.

The *interface between Blink and cc* is the data structure used to pass the
output of Blink to cc for compositing and rasterization onto the screen. It
includes various interfaces in Source/platform/graphics,

The *integration with cc* means most of the "main-thread" cc code, such as the
code in building property trees method, the code used to communicate with Blink
to fetch the painted output, and the implementation of the recording and raster
interfaces to composite and draw that painted output. The code is in various
places in
[src/cc/](https://code.google.com/p/chromium/codesearch#chromium/src/cc/)

The *integration with Skia* means collaboration with Skia engineers to improve
interfaces for clarity, proper pipeline layering and performance, plus some
implementation. The code is in
[src/third_party/skia/](https://code.google.com/p/chromium/codesearch#chromium/src/cc/).

**Activities**

The primary activity of the Paint team at present is [Slimming
Paint](../../blink/slimming-paint/index.md).

## Resources

Markdown documentation:
[paint](https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit/Source/core/paint/README.md),
[platform
paint](https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit/Source/platform/graphics/paint/README.md)

[OKRs (Objectives and Key Results)](okrs.md) (and [for Canvas](canvas-okrs.md))

[Team meeting
notes](https://docs.google.com/document/d/1S-gx4azHdYKPHKiLftIgRoAHPeCQmitX9z4Nvi4bBwY/edit)

Bugs:
[Blink>Paint](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink%3EPaint&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids),
[Blink>Paint>Invalidation](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink%3EPaint%3EInvalidation&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids),
[Hotlist-Slimming-Paint](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=label%3AHotlist-Slimming-Paint&colspec=ID+Pri+M+Iteration+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles).
(And sometimes
[Internals>Compositing](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3AInternals%3ECompositing+&colspec=ID+Pri+M+Iteration+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles))
