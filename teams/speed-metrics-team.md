# Speed Metrics Team

## Summary

Chrome Speed Metrics is part of the larger [Chrome Speed
program](https://sites.google.com/a/google.com/chrome-speed/). The current
mission is to:

Make the web faster by defining and owning the core performance metrics Chrome
uses to measure success, and producing actionable analyses of these metrics.

In particular, our goals are:

*   Define a small, high quality set of user experience metrics which Chrome
    devs align on.

*   Expose these metrics consistently to Chrome and Web devs, in the lab and the
    wild.

*   Analyze these metrics, producing actionable reports driving our performance
    efforts.

*   Own implementation for these metrics for TBMv2, UMA/UKM, and web perf APIs.

## Goals

#### Define Key User Experience Metrics

Chrome needs a small, consistent set of high quality user experience metrics.
This group will be responsible for authoring reference implementations of these
metrics implemented using Trace Based Metrics v2 (TBMv2) in
[tracing/metrics](https://cs.chromium.org/chromium/src/third_party/catapult/tracing/tracing/metrics/?q=tracing/metri&sq=package:chromium).
These reference implementations will often require adding C++ instrumentation,
sometimes collaborating with the Speed Architecture team. Some metrics work will
also be driven in MTV, such as the work on Frame Throughput. Chrome Speed
Metrics will also own UMA/UKM metrics (collaborating with speed architecture),
and speed metrics related Web Perf APIs.

The wider set of folks involved in defining these metrics will include:

*   Track domain experts

*   Track tooling team members

*   Devtools folks

*   DevX, documenting what these metrics mean for external developers.

*   Occasional other experts (e.g., UMA folks)

#### Expose Consistent Metrics Everywhere

This group will be responsible for ensuring that our core metrics are exposed
everywhere. This will include collaborating with devtools, lighthouse, etc to
make sure our metrics are easy to expose, and are exposed effectively.

#### Analyze Chrome Performance, providing data to drive our performance efforts

Metrics aren’t useful if no one looks at them. We’ll perform detailed analysis
on our key metrics and breakdown metrics, providing actionable reports on how
Chrome performs in the lab and in the wild. These reports will be used to guide
regular decision making processes as part of Chrome’s planning cycle, as well as
to inspire Chrome engineers with concrete ideas for how to improve Chrome’s
performance.

#### Own Core Metrics

The Chrome Speed Metrics team will gradually gain ownership of
[tracing/metrics](https://cs.chromium.org/chromium/src/third_party/catapult/tracing/tracing/metrics/?q=tracing/metri&sq=package:chromium),
and will be responsible for the long term code health of this directory. We’ll
also slowly be ramping up ownership in the Web Perf API space.

## Additional Links:

Mailing list:
[progressive-web-metrics@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/progressive-web-metrics)

Tech Lead: [tdresser@chromium.org](mailto:tdresser@chromium.org)

[Survey of Current
Metrics](https://docs.google.com/document/d/1Ww487ZskJ-xBmJGwPO-XPz_QcJvw-kSNffm0nPhVpj8)
