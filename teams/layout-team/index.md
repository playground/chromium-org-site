# Layout Team

The layout team is a long-term engineering team tasked with maintaining,
supporting, and improving the layout capabilities of the Chromium Blink
rendering engine. This includes line layout, block layout, text, fonts, and
high-dpi support. It is distinct from [style](../style-team.md) and
[paint](../paint-team/index.md), both of which are handled by their respective
teams.

## Team Charter

Our overarching goal is to **improve the experience for end users and platform
developers alike**.

*   By offering fast, correct, and compatible layout features supporting the
    majority of writing systems and scripts.
*   By providing layered and powerful APIs for developers.
*   By driving the layout capabilities of the web forward through collaboration
    with web developers, other browser vendors, and standard bodies.
*   And finally, by intervening when required. Thereby allowing a better user
    experience for existing and legacy content.

## Big ticket items for 2017

*   Schedulable, interruptible engine
    *   LayoutNG can render ~50% of web content.
*   Give framework authors the ability to extend the rendering engine
    *   Ship at least one CSS Houdini API.
*   High quality predictable platform
    *   Maintain bug SLA
    *   Full test and design doc coverage.
    *   Ship CSS Grid.
    *   Unify scrolling implementation (root layer scrolling).
    *   Ship OpenType variation support.
    *   Intersection Observer v2.
*   Improve rendering performance
    *   Improve line layout performance by 20%

## Team Members

*   Emil A Eklund [eae](mailto:eae@chromium.org) SFO (tech lead)
*   Stefan Zager [szager](mailto:szager@chromium.org) SFO
*   David Grogan [dgrogan](mailto:dgrogan@chromium.org) SFO
*   Ian Kilpatrick [ikilpatrick](mailto:ikilpatrick@chromium.org) MTV
*   Steve Kobes [skobes](mailto:skobes@chromium.org) MTV
*   Aleks Totic [atotic](mailto:atotic@chromium.org) MTV
*   Gleb Lanbin [glebl](mailto:glebl@chromium.org) MTV
*   Christian Biesinger [cbiesinger](mailto:cbiesinger@chromium.org) NYC
*   Morten Stenshorne [mstensho](mailto:mstensho@opera.com) OSL
*   Karl Anders Øygard [karlo](mailto:karlo@opera.com) OSL
*   Dominik Röttsches [drott](mailto:drott@chromium.org) HEL
*   Koji Ishi [kojii](mailto:kojii@chromium.org) TOK

A large number of other individuals not listed above routinely contribute to our
layout code and their impact and contributions should in no way be downplayed.
This merely lists the people that formally make up the layout team or have
committed to a certain task or project.

If you are looking for someone to review a change to the layout code anyone on
the list above should be able to help or connect you with the right person. For
questions about the design or implementation of a certain layout sub-system or
about layout in general the entire team may be reached through the
[layout-dev](mailto:layout-dev@chromium.org) mailing list.

For higher level questions or if you are interested in joining the team, please
reach out to [Emil](mailto:eae@chromium.org).

## Design Documents

*   [LayoutUnit & Subpixel Layout](https://trac.webkit.org/wiki/LayoutUnit)
    (2012)
*   [Line Box float -> LayoutUnit transition
    plan](https://docs.google.com/a/chromium.org/document/d/1fro9Drq78rYBwr6K9CPK-y0TDSVxlBuXl6A54XnKAyE/edit)
    (2014)
*   [Blink Coordinate
    Spaces](../../developers/design-documents/blink-coordinate-spaces/index.md)
    (2015)
*   [Eliminating Simple Text](eliminating-simple-text.md) (2015)
*   [LayoutObject Tree API
    Proposal](https://docs.google.com/document/d/1qc5Ni-TfCyvTi6DWBQQ_S_MWJlViJ-ikMEr1FSL0hRc/edit)
    (2015)
*   [Using Zoom to Implement Device Scale
    Factor](https://docs.google.com/document/d/1CZSCPzOYujdUMyChocwzOBPKxYAoTsEoezMye30Hdcs/)
    (2015)
*   [Scroll Anchoring](http:////bit.ly/scroll-anchoring) (2016)
*   [Shaper Driven Line
    Breaking](https://docs.google.com/document/d/1eMTBKTnWEMDu00uS2p8Xj-l9Pk7Kf0q5y3FbcCrWYjU/edit?usp=sharing)
    (2016)
*   [Root Layer
    Scrolling](https://docs.google.com/document/d/137p-8FcnRh3C3KXi_x4-fK-SOgj5qOMgarjsqQOn5QQ/)
    (2017)

## Meeting Notes

There is a regular weekly stand-up meeting on Mondays at 11:00 PST (14:00 EST,
20:00 CET). Additional meetings are held semi regularly and announced on the
[layout-dev](mailto:layout-dev@chromium.org) mailing list.

Recent meetings:

*   [Monday, August 29, 2016](meeting-notes/monday-august-29-2016.md)
*   [Monday, July 18, 2016](meeting-notes/monday-july-18-2016.md)
*   [Monday, June 27, 2016](meeting-notes/monday-june-27-2016.md)

Please see the [meeting notes](meeting-notes/index.md) page for notes from
previous meetings.

## Bugs & Triage Policy

The layout team is responsible for all bugs marked Cr-Blink-Layout and
Cr-Blink-Fonts, including sub-labels. Our policy is that all new bugs are to be
triaged within a week of being filed and all P-0 and P-1 bugs are to be fixed in
time for the next release. This is a relatively new policy and until we have
worked down the existing backlog this only applies to *new* bug reports while
we're working our way through the backlog.

*   [Bug Triage Instructions](bug-triage.md)
*   [Untriaged
    bugs](https://code.google.com/p/chromium/issues/list?can=2&q=status%3Duntriaged+label%3ACr-Blink-Layout%2CCr-Blink-Fonts&sort=-id&colspec=ID+Pri+M+Stars+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)
*   [Open P0 and P1
    bugs](https://code.google.com/p/chromium/issues/list?can=2&q=Pri%3D0%2C1+label%3ACr-Blink-Layout%2CCr-Blink-Fonts&colspec=ID+Pri+M+Stars+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)
*   [All open
    bugs](https://code.google.com/p/chromium/issues/list?can=2&q=label%3ACr-Blink-Layout%2CCr-Blink-Fonts&sort=-id&colspec=ID+Pri+M+Stars+ReleaseBlock+Cr+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=tiles)
*   [Project Warden
    bugs](https://code.google.com/p/chromium/issues/list?can=2&q=label%3AProject-Warden)
    \[legacy\]

## Links

*   [Readmap & Potential Projects](potential-projects.md)
*   [Team mailing
    list](https://groups.google.com/a/chromium.org/forum/#!forum/layout-dev)
*   [Profiling Blink using Flame
    Graphs](../../developers/profiling-flame-graphs/index.md)
*   [Skia telemetry try
    bot](http://skia-tree-status.appspot.com/skia-telemetry/chromium_try)
