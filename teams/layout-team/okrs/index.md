# Objectives and Key Results

This page holds our quarterly objectives and a set of key results for each
objective. At the beginning of each quarter the OKRs from the preceding quarter
are evaluated and graded and the ones for the coming quarter are agreed upon.

*   [2016 Q1](2016q1.md)
*   [2015 Q4](2015q4.md)
*   [2015 Q3](2015q3.md)
*   [2015 Q2](2015q2.md)
*   [2015 Q1](2015q1.md)
