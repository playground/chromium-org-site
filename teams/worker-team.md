# Worker Team

The Blink Worker team works on Worker Threads in Blink, including [Service
Worker](../blink/serviceworker/index.md), Shared Worker, and Dedicated Worker.

**Charter**

Our team's responsibility is to own, evolve and maintain
(Dedicated/Shared/Service)Worker and Worklet infrastructure:

*   core/workers/\*
*   modules/serviceworker/\*
*   content/\*/\*worker\*

**Docs**

*   [Web Workers in Blink (Google
    Doc)](https://docs.google.com/document/d/1i3IA3TG00rpQ7MKlpNFYUF6EfLcV01_Cv3IYG_DjF7M/edit#heading=h.7smox3ra3f6n)
*   [Service Workers: deep dive (BlinkOn4
    slides)](https://docs.google.com/presentation/d/15bHnAocAoSxYJAsF5JET34zMbzlxkc_NKGmP9BamqsM/edit#slide=id.g9e4def4c1_0_35)

**Members**

falken@chromium.org

horo@chromium.org

kinuko@chromium.org

nhiroki@chromium.org

shimazu@chromium.org

ikilpatrick@chromium.org (for worklets)

yiyix@chromium.org (20%)

yuryu@chromium.org (20%)

TL:

falken@chromium.org

PM:

kenjibaheux@chromium.org

Mailing List:

worker-dev@chromium.org
