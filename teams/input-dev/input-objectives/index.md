# Input Objectives

At Google we define and track progress against our goals using
["OKRs"](https://www.gv.com/lib/how-google-sets-goals-objectives-and-key-results-okrs)
(Objectives and Key Results). Here are most of the OKRs for the input-dev team
(a couple minor KRs relating to internal projects such as future Android
releases have been omitted for confidentiality reasons). Note that these are
**intentionally aggressive** and so we will be happy if we deliver 60%-70% of
them.

## 2017 Q2

**Eventing Sub-team**

### Improve Smoothness and Predictability of Events

Align Events with Appropriate Lifecycle P1 \[0.3\] Ship rAF aligned mouse events
\[0.4\] Address regressions with vsync aligned gestures \[0.3\] Ship Vsync
aligned gestures

Teach scheduler about non-blocking input events P1 \[0.5\] Simplify notification
of input events to render scheduler \[0.5\] Notify scheduler to adjust user
model based on non-blocking event

Resample input events on main thread and compositor P2 \[0.3\] Draft design doc
for resampling algorithm and matrix \[0.3\] Implement resampling behind a flag
\[0.2\] Launch Finch Trial and analysis data \[0.2\] Ship input resampling

### Product Excellence

Be Responsive to our Users P1 \[0.5\] Triage all new issues within 7 days
\[0.2\] Triage all open issues that have not been modified in 180 days \[0.3\]
Fix 7 top starred > 10 stars issues

Drive down regression bugs P1 Close 33% (9 bugs) of our open regressions.

Solve non-consistent target issues with Touch Adjustment P1 \[0.1\] Write
touch/pointer tests to expose interop issues \[0.3\] Propose a solution and
draft design doc \[0.4\] Implement behind a flag \[0.2\] Ship new Touch
Adjustment https://crbug.com/625198

Take over ownership of IO>PointerLock issues P2 \[0.5\] Fix 5 IO>PointerLock
issues \[0.5\] Triage and prioritize all issues.

Study & design a sensible hover/active solution for touch+mouse P2 \[0.4\] Write
a doc describing and comparing hover behavior on major browsers. \[0.4\] Propose
a design based on (possibly) decoupled active & hover states (no new APIs)
\[0.2\] Implement a prototype based on the design

Fix Chrome’s non-standard/interop APIs related to input. P2
https://bugs.chromium.org/p/chromium/issues/list?can=2&q=summary%3Astandardize+summary%3Aremove+Hotlist%3DInput-Dev
\[1.0\] Finalize standardize/remove decision for 5 non-standard web APIs.

### Add Richness to Web Platform

Experiment with a simple user activation indicator that is good for
cross-browser implementation P1 \[0.3\] Prototype a simple bit-per-frame user
activation and perform lab-test. crbug.com/696617 \[0.2\] Propose a detailed
design for implementation. \[0.3\] Implement the design behind a flag, covering
all user activation clients. \[0.2\] Experimentally switch away from current
UserGestureIndicator in M59/60 beta. \[stretch\] Publish a report on observed
regressions and next steps.

Define input events for animation worklet P1 \[0.3\] Create a polyfill-based
touch-drag prototype to aid design discussion. \[0.4\] Design doc on event model
for animation worklet, reach consensus on final design goals. \[0.3\] Complete
an experimental plumbing to route input events to a worker thread.

Enable Input Automation APIs in WPT P1 \[0.3\] Add key injection APIs in GPU
Benchmarking \[0.3\] Add input injecting input API with consensus from other
vendors \[0.2\] Plumb the input injecting API to enable the manual tests to run
on Chrome bots \[0.2\] Update at least 3 manual tests to use the new input
injection API

Expose fractional co-ordinates for PointerEvents of mouse type P2 \[0.2\] Plumb
fractional mouse co-ordinates in Windows (browser side) \[0.1\] Plumb fractional
mouse co-ordinates in Android (browser side) \[0.1\] Plumb fractional mouse
co-ordinates in Mac (browser side) \[0.2\] Plumb fractional mouse co-ordinates
in Linux and ChromeOS (browser side) \[0.3\] Remove WebMouseEvent coord
truncation, fix regressions. \[0.1\] Expose fractional coordinates for
PointerEvents of mouse type (blink side)

Make Pointer Events spec & tests ready for L2 P2 \[0.3\] Drive solving all of
the 4 current L2 Blocking issues. \[0.7\] Resolve all of the current 29 tests
with less than 2 passing browsers from failure list.

Champion Auxclick P3 \[0.7\] Merge auxclick spec into UI Event spec \[0.3\] Get
at least Edge or Safari to implement auxclick

Predicted points API P3 \[0.7\] Put together challenges and different solutions
we had for this API in a doc \[0.3\] Put a prototype together in Chrome to
demonstrate this feature

Drive TouchEvents-to-PointerEvents switchover for Google apps #internal \[0.5\]
Support Google Maps API switchover to PointerEvents. \[0.5\] Make Google Maps
usable with Android mouse. b/37211276

Keyboard Input Mode Consensus P3 \[0.5\] Continue with driving consensus in a
design internally \[0.5\] Publish design externally

Add Richness to Editing events P3 \[0.3\] Make sure InputEvent was shipped
without substantial issue \[0.4\] Rewrite all layout tests into WPT through
Input Automation API \[0.3\] Follow and fix incoming bugs

Synthetic pointer actions in WebDriver P4 \[0.3\] Design doc for exposing the
synthetic pointer actions in webdriver \[0.4\] Implementation of the synthetic
pointer actions in webdriver \[0.3\] Exposing the functionality in WebDriver Js

### Code Health

Target input events to either the RenderFrame or RenderView not both P1 Make
input event targeting on Chrome IPC consistent. 21 messages are sent to the
RenderFrame, and the remaining goto the RenderView on two different IPC channel
identifiers.

Point person for MUS hit test API P1 \[0.4\] Propose and agree on Mojo Interface
\[0.6\] Implementing the interface

Move input messages to be mojo based P2 \[0.4\] Define a mojo InputHandler
interface to the compositor thread and convert existing messages \[0.4\] Define
a mojo InputHandlerHost interface and convert existing messages \[0.2\] Cleanup
existing layering not necessary

Complete switch to pointer events input flow in Blink code P2 \[0.4\] Replace
all occurrences of WebTouchEvent in Blink with unpacked WebPointerEvent. \[0.3\]
Move grouping of touch events to the end of pipeline using raf signal \[0.2\]
Align WebMouseEvent attributes with WebPointerEvent \[0.1\] Merge WebMouseEvent
into WebPointerEvent

Using WM_POINTER for all input types on Windows 8+ P2 \[0.5\] Listen to
WM_POINTER to handle all mouse events to replace WM_MOUSE on Windows 8+ \[0.5\]
Listen to WM_POINTER to handle all touch events to replace WM_TOUCH on Windows
8+

### Performance

Hold the line on input performance P1 No regressions in key metrics that last a
milestone and monitor usage of touch-action

Add richer set of metrics to input pipeline P1 \[0.3\] Add
Event.Latency.Scroll(Begin|Update).(Touch|Wheel).TimeToScrollUpdateSwapBegin2 to
UKM \[0.2\] Do the initial round of analysis on RAPPOR data gathered last
quarter for input metrics and file at least 2 bugs for slow sites found via
RAPPOR \[0.2\] Prepare a document explaining a flow for analyzing UKM data from
dashboard or raw data \[0.3\] Compare the Rappor and UKM and decide what we will
be doing with RAPPOR metrics

Quantify accuracy of Expected Queueing Time metric P2 Measure how closely actual
queueing time correlates to expected queueing time.

EGL frame timestamps for input latency metrics P4 \[0.7\] Utilize Android O
frame-timing data \[0.3\] Compare the new metric with the old
TimeToScrollUpdateSwapBegin2 metrics and write a doc explaining the results

## Scrolling Sub-team

### Add Richness to Web Platform

document.rootScroller P1 https://crbug.com/505516 \[0.5\] Finish an
implementation that’s usable for AMP case \[0.2\] Preliminary spec describing
the new implementation \[0.2\] Ship origin trial in M60 \[0.1\] Create example
demos

scroll-boundary-behavior P1 \[0.6\] Land drafted overscroll-action API in M60
behind a flag \[0.2\] Implement scroll-boundary-behavior on both cc and main
thread \[0.2\] Ship the final API in M61

Unify On-Screen Keyboard Resize Behavior \[Stretch\] P4 https://crbug.com/404315
\[0.2\] Determine if existing implementation is sufficiently flexible to support
app-like scenarios \[0.3\] Propose any missing APIs needed for PWA like
scenarios \[0.3\] Create GitHub explainer and demos (similar to URL bar
resizing) \[0.2\] Fully implemented by M61 feature freeze

### Improve Scrolling UX

Ensure Overlay Scrollbar launch is Successful P1 https://crbug.com/307091
\[0.3\] Land in M59 \[0.4\] Address all P0/P1 implementation issues \[0.3\] All
polish items in the spreadsheet are addressed

### Performance

Touchpad Scroll latching P1 https://crbug.com/526463 \[0.3\] Latching logic
should work for OOPIFs \[0.3\] Improve test coverage: unittests of all the
classes with the flag must cover both latching and propagation cases, and no
flaky layouttests \[0.2\] Enable the flag on the waterfall by default and
address any regression bugs. \[0.2\] Ship a finch trial in M60

Async wheel events P1 https://crbug.com/706175 #WP-Alignment \[0.3\] Finish the
implementation behind a flag in M60 \[0.2\] Design doc for wheel scroll latching
and async wheel events \[0.3\] Enable the flag on the waterfall by default and
address any regression bugs. \[0.2\] Ship a finch trial by EOQ

MouseWheel listeners on scrollers out of the scroll chain shouldn’t block
scrolling P3 https://crbug.com/700075 \[0.2\] Add metrics to have an
approximation of cases that scrolls are blocked on listeners that are not part
of the scroll chain. \[0.6\] Implement tracking the wheel event per individual
scroller by EOQ \[0.2\] Ship a finch trial

### Improve Code Health

Unify main and compositor scrolling paths P2 https://crbug.com/476553 \[0.4\]
Create design doc outlining a path to scroll unification between main and impl
threads \[0.2\] Create detailed implementation plan/design doc for
reraster-on-scroll \[0.4\] Share with relevant teams, iterate and come to
consensus shared vision

Use scroll gestures for all user scrolls P2 \[0.5\] Keyboard Gesture Scrolling
\[0.2\] Autoscroll \[0.3\] Scrollbar Gesture Scrolling

Unify Mac scrollbars P3 \[0.4\] Remove scrollbar paint timer \[0.4\] Decouple
ScrollAnimatorMac from painting \[0.2\] \[Stretch\] Paint Mac Scrollbars from
WebThemeEngine

Decouple Scrollbars from ScrollableAreas in Blink P3 https://crbug.com/661236
\[0.6\] Move all scrollbar generation/management code out of ScrollableArea
\[0.2\] Move scrollbar creation for main frame to happen in RootFrameViewport
\[0.2\] Fix blocked issues https://crbug.com/456861, https://crbug.com/531603

### Improve threaded rendering code health

LTHI Scroll logic refactoring P2 \[0.75\] Design doc \[0.25\] An internal scroll
manager that isolates scrolling logics in LTHI

## Threaded Rendering Sub-team

### Enable rich scroll effects

Finish position:sticky P1 #wp-ergonomics #wp-predictability \[0.6\] fix all
current P2+ bugs (5 bugs) \[0.4\] Upstream web platform tests for spec’d
testable exposed behavior

Scroll Snap Points P1 Land the spec-matching version (behind flag)

CSS OM Smooth Scroll P2 Ship CSSOM View smooth scroll API (enabled by default)

Scroll-linked animations polyfill P3 #wp-ergonomics wait for the existing
(external) editors to have something ready for review, then look at an
implementation and/or polyfill strategy at that point \[0.6\] Implement JS
polyfill \[0.3\] Implement CSS polyfill \[0.1\] Publish on shared team github

### Enable performance isolated effects on the web (AnimationWorklet)

Implement new AnimationWorklet API behind flag P1 \[P1\] Parse
animator-root/animator CSS properties \[P1\] Add logic to work with CSS \[P1\]
Populate input styleMap for each proxy \[P1\] Plumb mutation signal and state to
animators \[P1\] Support document/scroll timelines \[P2\] Switch to using output
styleMap for each proxy \[P2\] Support slotting of elements \[P3\] Allow
worklets that run on dedicated non-compositor thread \[P3\] Add logic for
registering new animators in Javascript

Schedule animation calls at commit deadline P2

\[Stretch\] Ship origin trial P3 - amp or fb as potential clients

### Make compositing excellent

Resolve at least as many Hotlist-ThreadedRendering bugs as incoming P1

Promotion of opaque fixed (and sticky) position elements P1 \[0.6\] Resolve
consistent scroll position between composited and non composited elements
\[0.4\] Enable by default, 4% more composited scrolls

Enable compositing border-radius with tiled masks P1 \[0.2\] Investigate if mask
tiling can be disabled on AA bugs \[0.5\] Implement demoting logic \[0.2\]
Disable mask tiling on bugs and rare cases (AA, filters) \[0.1\] Stretch -
Antialising bug (both RPDQ and content quads, but on RPDQ it’ll be a regression)
- not urgent if tiling is disabled.

Draft plan shared with team for raster inducing animations P2

Prototype compositor side hit testing (shared with paint team) P2

Triage - track incoming vs redirected / fixed / closed P2

Enable reporting subpixel scroll offsets back to blink P3 - Fix issue 456622 -
bokan@ can provide consulting

### Identify opportunities / improve understanding of threaded rendering limitations in the wild

Metrics about interactions with scrollers P2 \[0.5\] Land UMA metrics regarding
size of scroller on page load and on scroll \[0.3\] Use results from the metrics
to launch a Finch trial to see the differences w, w/o compositing small
scrollers \[0.2\] Document and plan for the next step

Analyze CPU/GPU cost due to layer explosion P3 \[0.3\] Use current UMA metrics
to analyze CPU cost \[0.4\] Land new metrics regarding GPU memory cost due to
layer creation \[0.3\] Analyze GPU cost

Analyze main thread scrolling reasons; from uma data added in previous quarter
P3 \[0.2\] Resume the non composited main thread scrolling reasons recording
\[0.4\] Identify and resolve top remaining reason after opaque fixed / sticky
promotion. \[0.2\] Work on the case that transform with non-integer offset cases
\[0.2\] Work on the case that translucent element without text

Save composited filter result (18% pages have filter according to CSS
popularity) P3 \[0.4\] How often do we have a composited filter, land UMA
metrics \[0.3\] Timing metrics \[0.3\] Stretch - Figure out a way to apply the
animated-blur machinery in chromium to improve the perf

Perf party P3 \[0.4\] File 3 bugs. \[0.4\] Identify resolution of those bugs.
\[0.2\] Fix them.

Publish three UI pattern blog posts P3 \[0.25\] animated blur \[0.25\]
directional shadow \[0.5\] hero transition in scrolling content (google music)

### Improve threaded rendering code health

LTHI Scroll logic refactoring P2 \[0.75\] Design doc \[0.25\] An internal scroll
manager that isolates scrolling logics in LTHI

## Historical Objectives and Results

## [2017 OKRs](2017-okrs.md)

## [2016 OKRs](2016-okrs.md)

## [2015 OKRs](2015-okrs.md)
