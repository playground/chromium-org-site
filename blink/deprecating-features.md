# Deprecating Features

## How To Measure Usage and Notify Developers

1.  Add your feature to the [UseCounter::Feature
    enum](https://code.google.com/p/chromium/codesearch#chromium/src/third_party/WebKit/Source/core/page/UseCounter.h&sq=package:chromium&type=cs&q=file:UseCounter.h%20Feature).
2.  Add a clever deprecation message to the big switch in
    [UseCounter::deprecationMessage](https://code.google.com/p/chromium/codesearch#search/&sq=package:chromium&type=cs&q=file%3AUseCounter.cpp%20deprecationMessage).
3.  Instrument your code by either:
    *   Adding `DeprecateAs=[your enum value here]` to the feature's IDL
        definition (see `window.performance.webkitGetEntries`, for example).
    *   Adding a call to `UseCounter::countDeprecation` somewhere relevant (as
        we're dong for the [prefixed Content Security Policy
        headers](http://src.chromium.org/viewvc/blink/trunk/Source/core/page/ContentSecurityPolicy.cpp?r1=149184&r2=149193)).

Note that `DeprecateAs` is intended to replace `MeasureAs` in the IDL file.
Specifying both is redundant.
