# Conversion Cheatsheet

## Verifying committer access

This document is currently directed at
[committers](../getting-involved/become-a-committer.md), though we definitely
support read-only access to the code.

You do not need to register to access the code, but the script work better if
you have a Subversion account, which is controlled by [Chromium access (Google
chrome svn)](https://chromium-access.appspot.com/). You can request a read-only
SVN account, which will make the script work better and allow you to run try
jobs, by filling in the [request
form](https://chromium-access.appspot.com/request).

While you interact with the repo through the Git mirror, we use SVN to control
write access to the repositories, so to land changes you'll still need SVN
credentials (for git+svn).

Before starting, check that you have a SVN account at [Chromium access (Google
chrome svn)](https://chromium-access.appspot.com/), and that your login details
are stored in your local client.

Old:

    svn ls https://svn.webkit.org/repository/webkit/trunk

New:

    svn ls svn://svn.chromium.org/blink/trunk

You will also want to make sure credentials are registered for Chromium (not
just Blink) via:

    svn ls svn://svn.chromium.org/chrome

If you have not yet registered your username and password locally, these will
prompt you to enter them, and should store them.

## Getting the source code

Old:

    git clone git://git.webkit.org/WebKit.git
    WebKit/Tools/Scripts/update-webkit --chromium

New:

Get [depot_tools](../developers/how-tos/install-depot-tools.md), then use the
new `fetch` command (it's still pretty rough, admittedly :).

You can see the commands `fetch blink` will run if you pass it the `-n,
--dry-run` flag, e.g., `fetch --dry-run blink` – this can be useful if you need
to partially repair a checkout or want to understand what it's doing. Note that
`fetch blink` and `fetch chromium` both checkout *both* Chromium and Blink; the
only difference (for now) is which version of Blink the recipe is tracking:
`fetch blink` gets ToT, while `fetch chromium` only gets Blink via DEPS (latest
roll). These correspond to the `origin/master` and `gclient` branches in the old
WebKit-in-Chromium checkout (which is no longer supported).

There’s lots of ways to do checkouts, but we’re recommending the config at
[UsingGit](https://code.google.com/p/chromium/wiki/UsingGit) (formerly
“NewGit”); if working on Blink, we recommend pulling Blink ToT, not DEPS (also
using Git, of course).

With SVN account (**committers et. al.**):

    export GYP_GENERATORS=ninja
    fetch blink  # This will check out the code into a src/ directory

Without SVN account (**non-committers without read-only account**):

Skip all Subversion steps with the `--nosvn` flag (needs an argument due to
fetch's currently broken argument parsing; to be fixed soon):

    export GYP_GENERATORS=ninja
    fetch blink --nosvn=True

Blink source code is currently in `src/third_party/WebKit` – this is planned to
be moved to `src/blink` though there are some technical challenges.

We will no longer support the former “WebKit-in-Chromium” style checkout.

## Updating the source

Old:

    update-webkit --chromium

New:

    gclient sync

We’ll probably also update update-webkit so that it either wraps the recommended
command or errors out with a message pointing to the recommended command.

Note that the underlying commands (like “`git pull`”) will still work as well.

## Updating an existing checkout to use the new repo

We recommend using new checkouts if possible.

That said, there’s probably some interest in upgrading existing checkouts. I
think – but need to confirm – that NewGit / gclient-based workflows may
automatically re-point the repo. If not, there are certainly commands you can
run to accomplish this, and we can work up a separate page for possible
solutions.

## Building

Old:

    build-webkit --chromium

New:

    ninja -C $CHROMIUM_SRC/out/Release all_blink

We’ll probably update `build-webkit` to wrap the right thing as well. Of course,
you can always use shell aliases and other wrappers of your own devising.

## Running layout tests

Unchanged:

    run-webkit-tests [--chromium] [...]

`--chromium` is the default on all platforms.

## Checking your patch for style issues

Unchanged:

    check-webkit-style

Additionally:

    git-cl presubmit

Recall that `check-webkit-style` is located in the Blink tree in
`Tools/Scripts`.

You can also use `git-cl presubmit` to run the style checks plus the other hooks
(like OWNERS checks). Note that running ‘presubmit’ requires you to have
committed your changes locally.

You'll want to add the contents of
[/trunk/tools/build/slave/config](http://src.chromium.org/svn/trunk/tools/build/slave/config)
([ViewVC](http://src.chromium.org/viewvc/chrome/trunk/tools/build/slave/config))
to ~/.subversion/config first (if there's nothing in the file, you can just
replace it with this file).

## Create a bug

Old:

    (on #irc) sheriff-bot: create-bug
    webkit-patch create-bug

New:

Create a bug by hand on [crbug.com](http://crbug.com). Use the "Content"
component, and (if a contributor) the "Cr-Blink" label.

You can migrate existing WebKit bugs with the [WebKit Bugzilla Bug
Migrator](https://chrome.google.com/webstore/detail/webkit-bugzilla-bug-migra/oppbkaiopjpjhoianniidnggmcmfalef)
Chrome extension.

## Write a ChangeLog

Old:

    prepare-ChangeLog

New:

Preparing ChangeLogs are no longer a separate step (see “Upload a patch”,
below), and we will no longer store them into text files in the repository.

The Rietveld “issue description” is used as the commit message. Chromium
typically follows the format of:

    One line summary of change
    Detailed information about change ...
    R=eric@chromium.org
    BUG=123456

Using “R=” in the description will automatically populate the Reviewers field on
upload. ‘git cl upload’ will suggest a default set of reviewers for you, and
work is underway on an interactive suggesting tool as well.

We will probably have a separate discussion over whether we will want a more
formal or detailed convention in Blink that is closer to the WebKit ChangeLog
format (and what tools we can use to help populate that template). Mike West has
volunteered to take a look at this.

## Upload a patch

Old:

    webkit-patch upload

New:

    git cl upload

(CL is for "Change List".)

This will prompt you for a description, opening up the editor specified in
`GIT_EDITOR`, `SVN_EDITOR`, or `EDITOR` (in that order), or vim (notepad on
Windows). You can specify which bug the CL is associated with, if any, with the
`BUG=` line at the bottom. This will create a new issue on [Chromium Code
Reviews](https://codereview.chromium.org/) (the Rietveld server).

`git cl upload` does not send email (publish) by default; you can do this at the
web page with the "Publish+Mail Comments" link, or just hitting 'm'.
Alternatively, you can automatically send email with the `--send-mail` flag.

The title and description can also be given as arguments, rather than
interactively, via:

`-t, --title` to set title

`-m, --message` to set content

You can skip the editor (and other checks?) via the `-f, --force` option.

Further uploads in a given branch will be to the issue associated with the
branch. This can be checked via `git cl status` set via `git cl issue 12345` and
cleared via `git cl issue 0`

First time use of `git cl` will prompt for `git cl config`. Use Rietveld server
https://codereview.chromium.org (?). The other prompts are optional (?).

See also the comments above about ChangeLogs.

## Get reviewers for your patch

Old:

CC: people on bug and mark the patch as “r?”, use webkit-patch
suggest-reviewers, or rely on the style-bot to add people via the watchlist. An
email is automatically sent to said people.

New:

List people in the “Reviewers” field in the “edit issue” screen on Rietveld.
Click on “publish and send comments.”

## Review a change

Old:

Review bug in Bugzilla, add comments, set r+ flag if happy.

New:

Review bug in Rietveld, add comments, add “LGTM” in comments if happy (not case
sensitive).

## Try a change

Old:

EWS bots would pick up the patch from a bug automatically and report back.

New:

    git cl try

You need to manually run try jobs – see "[Try server
usage](../developers/testing/try-server-usage/index.md)" for more details.

You need a SVN account to run try jobs, but you do not need to be a committer –
a read-only account is sufficient. You can request a read-only SVN account via
the [request form](https://chromium-access.appspot.com/request). If you do not
have a SVN account, please ask someone with an account to kick off the try jobs,
which they can do through the Rietveld web UI.

Try jobs with binary patches (e.g., new baselines) don’t currently work. Fixing
this is a high priority task ([crbug.com/23608](http://crbug.com/23608))

You can follow the progress of your try jobs on the issue in Rietveld, or at
<http://build.chromium.org/p/tryserver.chromium/waterfall?committer=USERID@chromium.org>

(or whatever your normal username is). You will also get email sent to you when
the jobs complete. Be warned that loading that page is slow :).

Try jobs are not automatically run on patches when they’re uploaded, so make
sure to either run `git cl try` or select "try more bots" in Rietveld.

## Use the commit queue

Old:

Mark a bug as “cq?” in Bugzilla

New:

Check the "Commit" box next to your patch in Rietveld.

## Apply a patch to your checkout

Old:

    webkit-patch apply-from-bug 12345
    webkit-patch apply-attachment 12345

New:

    git cl patch 12345

(This is the Rietveld issue number, not the bug number).

Note that you can still use the webkit-patch commands to migrate old bugzilla
patches if you need to (or new patches that we want to merge across)..

## Land your local change

Old:

    webkit-patch land

New:

    git cl dcommit

Unlike `webkit-patch`, `git-cl dcommit` requires you to have all of your local
changes committed.

The description for the change comes from the description of the issue in
Rietveld. Although the patch that is landed uses the current local git branch,
the git commit messages themselves are ignored.

If you commit a change contributed by somebody else (e.g. after applying the
patch using `git cl patch`), you should specify the original contributor using
`git cl dcommit -c 'Foo Bar <foo@bar.com>'`.

See also the comments above about the commit queue.

## Revert/Roll out a change

Old:

    (on #irc) sheriffbot: rollout r12345 "reason"
    webkit-patch rollout 12345 "reason" 

New:

    cd /tmp
    drover -r 12345  # (and follow the prompts)

## Watch the tree for failures

Old:

check build.webkit.org and build.chromium.org/p/chromium.webkit

New:

check[
build.chromium.org/p/chromium.webkit](http://build.chromium.org/p/chromium.webkit)

*Blink tries to keep the tree green, and will close the tree when tests fail. You can check if the tree is open at the link above, and open and close the tree using the "[blink status](http://blink-status.appspot.com/)" link on that page*
We are now monitoring the tree for failures automatically and the presubmit
checks will refuse to land a patch if the tree is closed.

## Garden the tree

Unchanged:

    webkit-patch garden-o-matic
    webkit-patch rebaseline-expectations

We are still using the same model we used for WebKit changes ... Blink changes
land in the Blink repository, and eventually get rolled downstream into the main
Chromium configuration. Your job as gardener is to (1) increase the revision of
WebKit we use in Chromium without regressing any Chromium tests, and (2) prevent
or fix any WebKit regressions in tip of tree WebKit. For more information, see
http://dev.chromium.org/developers/how-tos/webkit-gardening

## Watch for changes affecting a part of the tree

Old:

Add entries to Tools/Scripts/webkitpy/common/checkout/watchlist

New:

Add entries to WATCHLISTS

which will be added to the top of the tree, but has more-or-less the same
format. We’ll need to check the tools to make sure this still works.

## Follow a particular user’s activity

Old:

Follow people in Bugzilla

New:

I don’t think we have a direct equivalent to this ... Thoughts?

## Create a release branch

Old:

I'm not actually sure what the old commands were :)

New:

    svn cp svn://svn.chromium.org/blink/trunk@<svn revision> \
        svn://svn.chromium.org/blink/branches/<BRANCH>

Only the Chrome TPMs should need to worry about this ...

## Merge a change to a release branch

Unchanged:

    drover --branch <branch> --merge <svn revision>

You can use the `drover.properties` file checked into
`svn://svn.chromium.org/blink/branches/chromium/drover.properties`, and then use
drover as you normally would.
