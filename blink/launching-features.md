# Launching Features

For questions, concerns or discussion of this process, please e-mail
[blink-api-owners-discuss@chromium.org](mailto:blink-api-owners-discuss@chromium.org).

## You do not need to use this process if:

*   Your change does not affect web API behavior to the point that developers
    need to be aware of it.
*   Your change is trivial and your reviewer agrees
    *   You must still create a [chromestatus](http://chromestatus.com/) entry
        for the feature if none exists or update any existing entry.
*   Your team has received explicit permission not to use this process
    *   You must still create a [chromestatus](http://chromestatus.com/) entry
    *   Send a "Web-Facing Change PSA" email to chromium-dev@ (CC blink-dev@)

## The process to launch a new feature

**Note:** You can combine the intent-to-implement email with an
intent-to-experiment or with an intent-to-ship. You must still complete steps 2
- 4.

1.  Email blink-dev using the [“Intent to Implement”
    template](https://docs.google.com/document/d/1vlTlsQKThwaX0-lj_iZbVTzyqY7LioqERU8DK3u3XjI/edit#bookmark=kix.p5nalpch13qw).
    *   Formal approval isn't necessary to proceed with implementation
    *   Code cannot be checked into trunk without an LGTM in code review
    *   Opposition on the "Implement" thread will likely resurface when you try
        to ship, so try to resolve issues early
2.  Create an entry on [chromestatus](http://chromestatus.com).
    *   You'll need to use an @chromium.org account for chromestatus.com. If you
        don't have one, please fill out this form.
    *   If you have trouble with chromestatus, please open an issue on GitHub.
3.  File an OWP launch tracking bug
    *   Some launches may require a formal Chrome review. If your feature has
        privacy, legal, or UI impact, please email web-platform-pms@google.com
        to set a review in motion.
4.  Most changes should start a [TAG
    review](https://w3ctag.github.io/workmode/#specification-reviews). If you're
    not sure, file one. And link to the bug tracker for that to make it easy.
5.  Implement your change as a Runtime-Enabled Feature.
6.  Your feature should have interop tests, preferably web-platform-tests. If
    Chrome is the first one implementing a spec, the requirements for
    web-platform-tests coverage + quality will be fairly high for shipping.
7.  Optionally, run an [origin trial](origin-trials/index.md) for your feature
    to collect developer feedback/data, as input to the standardization process.
    *   If you answer “no” to all of the following questions, an origin trial is
        unnecessary (see caveat in \[1\]).
        *   Is there disagreement about how well this API satisfies its intended
            use case?
        *   Are you unsure about what API shape will be the most ergonomic in
            real world scenarios?
        *   Is it hard to quantify performance gains without testing on real
            world sites?
        *   Is there a reason that this API needs to be deployed to real users,
            rather than behind a flag, for data to be meaningful?
    *   If you decide to run a trial, or are unsure, please first consult with
        the Origin Trials core team.
        *   Email
            [experimentation-dev@chromium.org](mailto:experimentation-dev@chromium.org).
            Google employees can alternatively schedule a meeting directly with
            [origin-trials-core@google.com](mailto:origin-trials-core@google.com).
    *   If you've decided to run an origin trial, do the following steps. If not
        then skip to step 8 of the launch process.
        *   Follow the instructions on [how to run an origin
            trial](origin-trials/running-an-origin-trial.md). Google employees
            should see
            [go/running-an-origin-trial](http://goto.google.com/running-an-origin-trial).
        *   Email blink-dev using the [“Intent to Experiment”
            template](https://docs.google.com/document/d/1vlTlsQKThwaX0-lj_iZbVTzyqY7LioqERU8DK3u3XjI/edit#bookmark=id.pygli2e122ic).
            Wait for LGTM from at least 1 API Owner (again see caveat in \[1\]).
        *   At the start of every subsequent release, post an update on usage of
            the feature on the intent to experiment thread in blink-dev.
        *   There is an automatic and mandatory 1 week period when your feature
            is disabled at the end of the origin trial, before it potentially
            graduates to Stable. Tokens will expire 1 week before the earliest
            stable channel launch date, but note that a stable launch takes many
            days to deploy to users. This exists to encourage feature authors to
            make breaking changes, if appropriate, before the feature lands in
            stable, and to make clear to clients of the origin trial feature
            that in all circumstances the feature will be disabled (hopefully
            only briefly) at some point.
8.  When your feature is ready to ship, email blink-dev using the [“Intent to
    Ship”
    template](https://docs.google.com/document/d/1vlTlsQKThwaX0-lj_iZbVTzyqY7LioqERU8DK3u3XjI/edit#bookmark=id.w8j30a6lypz0).
    *   Respond to any feedback or questions raised in the thread
    *   You need at least 3 LGTMs from API owners to launch.
    *   If you have resolved all feedback and are blocked on API owner LGTMs,
        add blink-api-owners-discuss@chromium.org requesting final approval.
9.  Enable by default.

\[1\] Origin trials should be run for a specific reason. These questions are
guidance to identifying that reason. However, there is still debate about the
right reasons, so the guidance may change. You can join the conversation in
[this
doc](https://docs.google.com/document/d/1oSlxRwsc8vTUGDGAPU6CaJ8dXRdvCdxvZJGxDp9IC3M/edit#heading=h.eeiog2sf7oht).
