# Animation Team

Primary contacts: [suzyh@chromium.org](mailto:suzyh@chromium.org),
[shans@chromium.org](mailto:shans@chromium.org), or find us on
[blink-dev@chromium.org](mailto:blink-dev@chromium.org)

### Charter

The Animation Team’s responsibility is to own, evolve and maintain:

*   The Web Animations engine (core/animation, cc/animation)
*   The Web Animations API (core/animation/\*.idl)
*   CSS Animations and Transitions (core/animation/css)

We’re also active on the [Web Animations
specification](https://w3c.github.io/web-animations/) and are the primary
authors of the [Web Animations
polyfill](http://github.com/web-animations/web-animations-js).

### Activities

Last updated: September 2016

*   Web Animations level 1 features and interop - contact:
    [suzyh@chromium.org](mailto:suzyh@chromium.org)
*   Unifying the compositor and Blink animation engines - contact:
    [loyso@chromium.org](mailto:loyso@chromium.org)
*   CSS custom properties in keyframes - contact:
    [alancutter@chromium.org](mailto:alancutter@chromium.org)
*   CSS offset paths - contact:
    [ericwilligers@chromium.org](mailto:ericwilligers@chromium.org)
*   Additive animations - contact:
    [alancutter@chromium.org](mailto:alancutter@chromium.org)
*   Parity with SMIL animation - contact:
    [ericwilligers@chromium.org](mailto:ericwilligers@chromium.org)
*   Scroll-linked animations - contact:
    [shans@chromium.org](mailto:shans@chromium.org)

### Bug Triage

We are responsible for bugs filed in the
[Blink>Animation](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink%3EAnimation+&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids)
component. We enforce the following protocol for these bugs:

*   The bug is not filed against any other component at the same time.
    *   There is an exception for "meta" bugs for tracking projects that cross
        multiple code areas. These bugs have no work associated with them; all
        work for these projects should be tracked in dependent bugs which follow
        the one-component rule. These "meta" bugs are identified by the label
        "Objective".
*   The bug has an Update-\* label, which gives a target for how often the bug
    should be updated.
    *   Update-Quarterly bugs are looked at during our quarterly planning and
        are not necessarily updated with a comment.

For more information, see [the doc introducing the update
scheme](https://docs.google.com/document/d/1ZjUxiBMeeh6hqMEq59eRhKqt1EQ9zLxcEER8_8tnOQg/edit).

### Links

*   [Team resources](resources.md)
*   [Google-internal
    resources](https://docs.google.com/document/d/1T_EbYFojmMWtCCheJ1hURmtf8o71gMdLalPi6b4jN-0/edit)
*   Specs
    *   [Web Animations](https://w3c.github.io/web-animations/)
    *   [CSS Animations](https://drafts.csswg.org/css-animations-1/)
    *   [CSS Transitions](https://drafts.csswg.org/css-transitions/)
    *   [~~Motion~~ Offset Path](https://drafts.fxtf.org/motion-1/)
    *   [SMIL Animation](https://www.w3.org/TR/smil-animation/)
*   Demos
    *   [Web Animations
        demos](https://web-animations.github.io/web-animations-demos/)
    *   [Web Animations
        codelabs](https://github.com/web-animations/web-animations-codelabs)
