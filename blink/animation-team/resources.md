# Animation Team Resources

### Dashboard

*   [Live
    dashboard](http://chromez-app.appspot.com/#configs/animations-ave.json)
*   The code lives here: <https://github.com/shans/chromez/>
*   How to contribute:
    *   Follow the developer instructions [in the
        README](https://github.com/shans/chromez/blob/master/README.md)
    *   Once your changes are merged, ask suzyh to push the update to the live
        dashboard

### Shared Animation Team Drive Folder

*   <https://drive.google.com/drive/folders/0B7LZx4I2hCOoTUNjelAzYmU1dW8>

This folder is public to view but has limited edit permissions. See the
Google-internal team resources for instructions on adding docs to this folder.

### Handy Monorail links

*   [Blink>Animation bugs grouped by owner and update
    label](https://bugs.chromium.org/p/chromium/issues/list?can=37830562&q=&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&sort=&groupby=&mode=grid&y=Owner&x=Update&cells=tiles&nobtn=Update)

### Bug Update Labels

Every bug in Blink>Animation must be labelled with an Update-\* label giving a
target for how often the bug is to be updated. The default categorization is
outlined below. These are guidelines only; individual bugs may be given a
different label if appropriate.

**
[Update-Daily](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink%3EAnimation+Update%3DDaily&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids)**

*   Security bug in stable
*   High visibility/P0 bug

**
[Update-Weekly](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink%3EAnimation+Update%3DWeekly&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids)**

*   Regression bug
*   Crash
*   Assertion failure
*   Test failure

**
[Update-Fortnightly](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink%3EAnimation+Update%3DFortnightly&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids)**

*   Memory leak
*   Test flake

**
[Update-Monthly](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink%3EAnimation+Update%3DMonthly&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids)**

*   Current OKR project work

**
[Update-Quarterly](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink%3EAnimation+Update%3DQuarterly&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids)**

*   Feature request
*   Code health and cleanup work
*   Non-regression performance issue
*   Non-regression interop or correctness bug

### Backlog

We are collecting a list of [things we would like to do
someday](https://docs.google.com/document/d/1HqyYRIM6bE5eAOvZHjvKS1mwqvKZT_JP9VSaiUGhzk0/edit).

### Useful commands

For testing:

    ninja -C out/GnDebug blink_tests
    out/GnDebug/webkit_unit_tests
    out/GnDebug/blink_platform_unittests
    blink/tools/run_layout_tests.sh -t GnDebug animations svg/animations web-animations-api imported/wpt/web-animations inspector/animation webexposed

### Upstreaming tests to the Web Platform Tests

TODO: Notes about contributing to the web-animations suite in WPT.

### Useful Docs and Presentations
