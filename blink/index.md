# Blink

Blink is the [rendering
engine](https://en.wikipedia.org/wiki/Web_browser_engine) used by Chromium.

## Blink's Mission:

*To improve the open web through *
* technical innovation and good citizenship*

## Participating

Blink is an inclusive open-source community that values fostering a supportive
culture. We welcome participation from anyone who shares [our
mission](https://www.chromium.org/blink#mission).

### Discussions

Transparency is one of the core values of Blink. Our goal is for anyone to be
able to participate, regardless of organizational affiliation. There are a
number of areas where discussions take place:

*   **Chromium Bug Tracker**: We use bugs to track and discuss desired features.
    We use Chromium's bug tracker, and mark our bugs with the ["Blink"
    component](https://crbug.com/?q=component%3ABlink). Click the star next to a
    bug’s name to receive email updates, and file new bugs using the "New issue"
    wizard at [crbug.com](https://crbug.com/).
*   **Code Reviews**: We use Rietveld, Chromium's code review tool, to comment
    on implementation strategies for changes we've decided to implement.
*   **#blink**: We hang out on the #blink IRC channel on Freenode to have quick,
    informal discussions and answer questions. For more substantive discussion,
    a format with a permanent record is preferred.
*   **[blink-dev@chromium.org](https://groups.google.com/a/chromium.org/group/blink-dev/topics)**
    is a general list for discussions relevant to the design and implementation
    of the project. The mailing list is analogous to chromium-dev and is the
    most high-traffic of the communication channels. There are also several more
    targeted sub-lists for discussion of specific areas of the codebase, which
    you can find [here](../developers/technical-discussion-groups.md). None of
    these lists are appropriate for web development inquiries.

### Watching for new features

For web developers interested in tracking new features, the signal-to-noise
ratio of discussion forums might be too low to be useful. There are several
dedicated channels for staying up-to-date on new features:

*   **Beta Chromium blog posts**: For each new Chrome Beta release (~every six
    weeks), the Chrome team publishes a blog post outlining changes to the web
    platform and the Chrome Apps & Extensions APIs. Here's an example of one of
    these
    [posts](https://blog.chromium.org/2013/04/chrome-27-beta-speedier-web-and-new.html).
*   [**chromestatus.com**](https://www.chromestatus.com/): A dashboard where we
    track new feature development. Learn more in the [“Feature Dashboard”
    section](https://www.chromium.org/blink#TOC-Feature-Dashboard) below.
*   [**bit.ly/blinkintents**](https://bit.ly/blinkintents): A Google Spreadsheet
    that lists all ["Intent"
    threads](https://www.chromium.org/blink#TOC-Web-Platform-Changes:-Process)
    and their approval status.
*   **Chrome Developer Relations**: Chrome DevRel posts about new features on
    both Twitter ([@ChromiumDev](https://twitter.com/ChromiumDev)) and Google+
    ([+GoogleChromeDevelopers](https://plus.google.com/+GoogleChromeDevelopers)).
*   **Blink launch tracking bugs**: These bugs track new features on the
    Chromium issue tracker. You can find launch bugs by applying the filter
    [Type=Launch-OWP](https://crbug.com/?q=Type%3DLaunch-OWP) (OWP stands for
    "open web platform").
*   [blink-reviews@chromium.org](https://groups.google.com/a/chromium.org/forum/?fromgroups#!forum/blink-reviews):
    a high-traffic list for Blink code reviews. The code for all new features
    gets reviewed on this list.

### Committing and reviewing code

The [process for becoming a
committer](../getting-involved/become-a-committer.md) or an OWNER is the same as
for the larger Chromium project, and code changes are approved by
[OWNERS](../developers/owners-files.md). Experience with the
[codebase](https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit)
from working on WebKit will be taken into account to accelerate the process of
becoming an OWNER.

### Developing Blink

Blink is implemented on top of an abstract platform and thus cannot be run by
itself. The [Chromium Content module](../developers/content-module/index.md)
provides the implementation of this abstract platform required for running
Blink. Developing for the Content module is covered in the [Chromium developer
docs.](../developers/_index.md) Testing your changes can be done with a build of
Content Shell or a full Chromium build. For some tips and suggestions on
debugging Blink, see the page on [Getting Started with Blink
Debugging](getting-started-with-blink-debugging.md).

Blink code uses the [Blink Coding Style Guidelines](coding-style/index.md)..

### How do I port Blink to my platform?

Blink runs on an abstract platform inside a sandbox and therefore has few
operating-system-specific dependencies. This design has two consequences: (1)
Blink cannot run alone, and (2) porting to other platforms happens at a
different layer. Instead of adding platform-specific code to Blink, you should
use Chromium’s content layer, which provides an implementation of this virtual
platform on a wide variety of operating systems including Windows, Linux, Mac OS
X, and Android. A separate project called the [Chromium Embedded
Framework](https://bitbucket.org/chromiumembedded/cef) is probably the easiest
way to use Chromium (and thus Blink) on your platform.

## Web Platform Changes: Guidelines

In addition to making the web platform faster and more secure, improving the web
platform also means adding new functionality and removing cruft. To fulfill our
good citizenship mission, we need to be careful to change the web-facing API in
a transparent, responsible, and compatible manner. We measure success as moving
the open web platform forward as a whole, not just moving one implementation
forward.

In practice, we strive to ensure that the features we ship by default have
**open standards**. As we work on features, we track their progress in the web
standards community with the [Chromium Features
Dashboard](https://www.chromestatus.com/features), which lets us be transparent
about the status of each feature and about how we make decisions about which
features to enable by default for the open web.

### Scope

These guidelines cover non-trivial changes that are exposed to the open web or
in some other way have implications for web developers, including changes that
add or remove functionality or APIs. Changes that are not exposed to the open
web (e.g., user interface features, extension APIs, or testing infrastructure)
are outside the scope of this policy.

### Policy for shipping and removing web platform API features

As browser developers, we find that there’s often tension between moving the web
forward and preserving compatibility. On one hand, the web platform API surface
must evolve to stay relevant. On the other hand, the web’s primary strength is
its reach, which is largely a function of interoperability. By definition, when
any vendor ships a new feature (or is the first to remove an obsolete feature),
the API change is not yet interoperable, so cross-browser compatibility is
sacrificed, even if briefly. This section outlines our approach to resolving
this tension.

**Evaluating “interoperability and compatibility risk”**

Interoperability risk is the risk that browsers will not eventually converge on
an interoperable implementation of the API. Interoperability at a given time
point is not a well-formed concept, because unless every browser shipped new
features at exactly the same time then there is a period of
non-interoperability. In practice, we forecast “interoperability risk” (a) by
observing the public behavior of others in the web ecosystem. Heuristics that
decrease risk a priori for an unshipped feature include, in rough order of
importance:

*   Other vendors already shipping interoperable implementations
*   A mature specification in the relevant standards body
*   A shared test suite for that specification
*   Positive signals from other browser vendors
*   Lack of negative signals from other browser vendors
*   A small API footprint

Compatibility risk is the likelihood that a change will break existing web
content loaded in Chromium. Compatibility risk is especially common with API
removal, but is also a factor when adding new features (e.g. [Unprefixed CSS
Animations](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/BytHPljnifk/PiKCn3Ix6IIJ)).
We rely on a set of [core
principles](https://docs.google.com/document/d/1RC-pBBvsazYfCNNUSkPqAVpSpNJ96U8trhNkfV0v9fk/edit#)
and [tools](https://www.chromium.org/blink/platform-predictability/compat-tools)
to evaluate and weigh compatibility risk, though given the complexity and nuance
individual engineers are not expected to be familiar with the details.

**Evaluating “moving the web forward”**

A change to Blink’s API surface is said to “move the web forward” if it either
(a) enables developers to build compelling applications for a large number of
users, both now and in the future, and/or (b) reduces code complexity in Blink
or Chromium.

Bucket (a) is subjective and changes over time. In 2014, we believe the most
important changes for developers---and, by extension, users---are those that
make the web more performant and easier to use on mobile devices. For more
information, see the [2014 Blink goals brainstorming
thread](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/Z5OzwYh3Wfk/IWooaY5FZowJ).

Bucket (b) is important to make sure that the web’s progress is not overly
constrained by its legacy. Simplifying the codebase allows us to make
performance improvements, more quickly add more important features, and reduce
binary size. In most cases, we believe these benefits will extend to other
browsers as well.

**Balancing “moving the web forward” and “interoperability and compatibility
risk”**

To manage the project-wide tension between moving the web forward and
interoperability and compatibility, we ask of each proposed API change: does
this individual change strike the right balance between making the web better
and minimizing interoperability and compatibility risk?

In an ideal world, all changes would both drastically move the web forward and
involve zero interoperability and compatibility risk. In practice, this is
rarely the case; features fit somewhere else on this chart:

Blink’s willingness to accept a change is largely determined by the change’s
location on the chart above:

*   If a change has low interoperability and compatibility risk and
    significantly moves the web forward, Blink usually welcomes it (e.g.,
    [shipping unprefixed CSS
    Transforms](https://groups.google.com/a/chromium.org/forum/#!topic/blink-dev/vjyd8It--3Y)).
*   If a change has low interoperability and compatibility risk but isn’t
    expected to significantly move the web forward, Blink usually still welcomes
    it. Occasionally, Blink will reject changes in this bucket to avoid
    technical complexity (e.g., [not shipping our old implementation of CSS
    Variables](https://groups.google.com/a/chromium.org/forum/#!topic/blink-dev/ScKw9zYRkBc)).
*   If a change has high interoperability risk and isn’t expected to
    significantly move the web forward, Blink will usually not welcome it (e.g.,
    [not shipping canvas
    supportsContext](https://groups.google.com/a/chromium.org/forum/#!topic/blink-dev/n1LP6cE2or4)).
*   If a change has high interoperability risk but is expected to significantly
    move the web forward, Blink will sometimes welcome it after careful,
    publicly-explained consideration (e.g. [shipping Shadow
    DOM](https://groups.google.com/a/chromium.org/forum/#!msg/blink-dev/ay9tVGRa8Rg/GQpvRbrgflgJ)).
    In such cases, the implementer is expected to:
    *   Propose an editor’s draft (or equivalent) to the relevant standards
        group.
    *   Discuss the feature publicly with implementers of other browser engines.
    *   Take on an active commitment to shepherd the feature through the
        standards process, accepting the burden of possible API changes.

Of course, equal minds can disagree about the fuzzier aspects of these policies.
For example, if a change increases runtime performance but consumes more
battery, does it “move the web forward”? Or: what combination of the heuristics
above precisely constitutes “high” compatibility risk?

To equitably resolve these kinds of questions on a per-change basis, Blink has
[a well-defined
process](https://www.chromium.org/blink#TOC-Web-Platform-Changes:-Process) for
discussing and approving web-facing API changes. It consists of code reviews,
announcements to blink-dev, and formal API review meetings. The most formal
requirement of the process is that API changes must receive an LGTM from at
least three different [API
owners](https://www.chromium.org/blink#TOC-API-Owners) before enabled by default
on trunk. The API owners are responsible for making sure the policy described
above is applied appropriately on a per-change basis.

### Trivial Changes

Trivial platform changes do not need to meet the requirements above. For
example, changes to existing APIs to improve compliance with web standards or to
fix bugs are welcome. When in doubt, please send at a PSA to blink-dev or ask an
API OWNER for advice.

However, any new API (no matter how small) is considered non-trivial. Trivial
changes should be small fixes that have low risk of disrupting web developers.
Trivial changes should be labeled as such in their code review, and may still
require the LGTM of one API OWNER if they change the [stable-build
output](https://code.google.com/p/chromium/codesearch#chromium/src/third_party/WebKit/LayoutTests/virtual/stable/webexposed/&sq=package:chromium)
of the [webexposed
tests](https://code.google.com/p/chromium/codesearch#chromium/src/third_party/WebKit/LayoutTests/webexposed/&sq=package:chromium).

If we’re unsure about the extent to which a change will impact web developers,
we may ask the contributor proposing the change to provide data quantifying the
impact. If a project member questions whether a change is trivial, we will err
on the side of caution and ask the contributor to meet the requirements above.

### Vendor Prefixes

Historically, browsers have relied on vendor prefixes (e.g., `-webkit-feature`)
to ship experimental features to web developers. This approach can be harmful to
compatibility because web content comes to rely upon these vendor-prefixed
names. Going forward, instead of enabling a feature by default with a vendor
prefix, we will instead keep the (unprefixed) feature behind the “enable
experimental web platform features” flag in `about:flags` until the feature is
ready to be enabled by default or exposed for a limited duration [origin
trial](origin-trials/index.md).

## Web Platform Changes: Process

### [Launch Process: launching a new feature](launching-features.md)

### [Launch Process: removing a feature](removing-features.md)

### API Owners

Here's [the current list of API
OWNERS](https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit/API_OWNERS).

All members of the project are responsible for enforcing that new features
follow the project’s policies. Project members who feel that a feature is
violating the policy should raise the issue first with the contributor and, if
that doesn’t resolve the issue, with the project’s public mailing list.

To complement this project-wide responsibility, we have a set of API owners who
are listed in the OWNERS file for the expected output of tests that monitor much
of the web-exposed surface area of blink (eg.
[these](https://code.google.com/p/chromium/codesearch#chromium/src/third_party/WebKit/LayoutTests/virtual/stable/webexposed/)).
When reviewing changes to these files, the API owners should ensure that the
changes meet the project’s guidelines for new and removed features.

### API Review

API Review meetings will be scheduled when API discussion over email is
insufficient (per the [Launch
Process](https://www.chromium.org/blink#TOC-Web-Platform-Changes:-Process)). API
owners and contributors of features under discussion are welcome to attend. The
purpose of the API Review meeting is to provide a high-bandwidth forum for
discussion between API owners and feature implementers. The group makes
decisions by consensus; at least three project OWNERS must be present for
quorum. After the meeting the organizer will send notes, including any
decisions, to blink-dev@.

### Feature Dashboard

To improve transparency, we track development of new features on our [Feature
Dashboard](https://www.chromestatus.com/features). For each feature, the
dashboard tracks our implementation status, the feature's progress through the
standards process, our understanding of the opinion of other browser vendors and
other key metrics.

We associate each value with a shade of red or green, corresponding to how the
value reflects our web citizenship. For example, “opposition from another
browser vendor” is red and “a similar implementation in another browser” is
green. Viewed in aggregate, these colors provide a quick snapshot of the
project’s overall web citizenship.

The dashboard data itself is also a useful high-level record of when features
were implemented and a peek at what’s coming next. If you’d like to monitor
lower-level changes as they happen, check out our
[Gitiles](https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit)
and [SVN logs](https://src.chromium.org/viewvc/blink/).

### Guiding Principles for Process

*   Minimize interoperability and compatibility risk for released features.
*   Minimize process burden once a change has been LGTM-ed by API owners.
*   Minimize ambiguity.
*   Block bad engineering investments upfront.
*   Block incomplete features from being runtime-enabled.
*   Create an audit trail, not necessarily a single approval funnel for all
    changes.
*   Prefer email over meetings.

## Testing

Openness and interoperability are core to the web platform’s philosophy and
success. We are committed to delivering features that are interoperable. In
addition to the checks and balances in our release process, we prioritize
testing as a way to promote compatibility across browsers. It is our intention
to increase our investment in testing over time.

In 2012, we submitted a comprehensive [Shadow DOM conformance test
suite](http://demo.unipro.ru/shadow/) to the W3C. We’ll continue to create
conformance test suites like these, so that implementations for new features are
interoperable from the beginning. To incentivize this, the feature dashboard has
a column for conformance tests. If a feature has a test suite, its cell in that
column is green. Otherwise, it’s red.

Going forward, we’ll be
[working](https://lists.webkit.org/pipermail/webkit-dev/2013-March/024055.html)
with the W3C and the broader web community to share more tests and testing
infrastructure as a way to encourage interoperability.

We’ve also been collaborating with Adobe to host [Test the Web
Forward](http://testthewebforward.org/events/2013/sydney.html) events, where web
developers and spec authors work together to write conformance tests that all
browsers are evaluated against. So far (April 2013) there have been four such
events, the most recent hosted by Google in Sydney. We're committed to this
effort and are currently organizing the next event in Tokyo.

If you encounter a bug in Blink or a browser interoperability issue, please use
the "New issue" wizard at [crbug.com](https://crbug.com/).

## Architectural Changes

With Blink we’re excited about the freedom to dream big for the Web. When
Chromium started, our goal was to change as little of WebKit as possible, easing
integration with the WebKit codebase. With Blink we are excited to make
large-scale architectural changes to the code, without having to worry about
breaking other consumers of WebKit.

One change we’re planning is adding [“out-of-process
iframes”](../developers/design-documents/oop-iframes/index.md). These allow
Chromium to separate individual parts of a page into separate sandboxed
processes. Implementing this will require large restructuring of how iframes are
handled in WebKit. Some of this restructuring is incompatible with other WebKit
ports and has thus been delayed until now.

As another example, we’d like to fix our networking code to be faster and
simpler. Our current networking code in WebKit is limited by old Mac WebKit API
obligations which cannot be changed. Chromium has worked around some of these
limitations over the years, but these workarounds have proven fragile and have
long been a source of bugs. With Blink, we’re excited to refresh this networking
code without forcing other WebKit consumers to break their WebKit API
obligations.

Finally, we’d like to explore even larger ideas like moving the entire Document
Object Model (DOM) into JavaScript. This has the potential to make JavaScript
DOM access dramatically faster, but will involve a very large re-write of
WebKit’s DOM implementation—something that would be difficult in WebKit which
has two supported JavaScript engines.

Some of the other changes we're considering:

*   Teach WebCore about multi-process history (currently it assumes same-process
    synchronous History access)
*   Delete the Widget tree (a Mac WebKit1 constraint)
*   Split WebCore into modules
*   Experiment with moving the DOM into the JS heap
*   Increase multicore use (e.g., html parser, style engine, javascript parser)
*   Remove obscure parts of the DOM and make backwards-incompatible changes to
    obscure parts of the DOM that benefit performance or remove complexity.
*   Use a modern, faster tcmalloc throughout all of Mac chrome
*   Experiment with incremental or parallel layout
*   Fix memory leaks by removing the ScriptValue/ScriptState abstractions now
    that there’s only one JavaScript engine.
*   Remove custom JavaScript bindings code
*   Bring WebCore up to speed with DOM3 Events / \[DOM\] UI Events.

Changes previously considered and now done:

*   Move code to use the sandbox Platform API directly instead of
    WebCore/platform where possible
*   Establish a simpler, stricter tree-gardening system that does not require 2
    full time engineers per day
*   Replace WebKitIDL with WebIDL

Platform layering

[This
document](https://docs.google.com/document/d/13XsbaBz7A2H0PZIdFcytHf5-fVOlAfkLlIUKhxKzs44/edit#heading=h.upclskvgtibl)
lays out the preferred code architecture for what classes and directories can
refer to each other. The Blink Onion Soup project is currently working to
organize code according to that architecture (it's a work in progress). New code
should conform to that architecture.

## Sub Teams

The larger Blink team is divided in to a set of sub-teams each specializing in a
particular part of the problem space and code base.

*   [Animation Team](animation-team/index.md)
*   [Binding Team](../teams/binding-team.md)
*   [DOM Team](../teams/dom-team.md), including HTML, XML and editing.
*   [Input Team](../teams/index.md)
*   [Layout Team](../teams/layout-team/index.md)
*   [Memory Team](memory-team.md)
*   [Paint Team](../teams/paint-team/index.md)
*   [Web Platform Predictability](platform-predictability/index.md)
*   [Style Team](../teams/style-team.md)
*   [Worker Team](../teams/worker-team.md)

## Developer FAQ

You can find answers to some of the most common web developer-facing questions
in the [Blink Developer FAQ.](developer-faq/index.md)

## Subpage Listing
