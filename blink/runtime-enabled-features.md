# Runtime Enabled Features

## **Overview**

Runtime flags enable Blink developers the ability to control access Chromium
users have to new features they implement. Features that are hidden behind a
runtime flag are known as Runtime Enabled Features. It is a requirement of the
[Blink Launch Process](http://www.chromium.org/blink#launch-process) to
implement new web exposed features behind a runtime flag until an [Intent To
Ship](http://www.chromium.org/blink#launch-process) has been approved.

### **Adding A Runtime Enabled Feature**

Runtime Enabled Features are defined in
[RuntimeEnabledFeatures.json5](https://code.google.com/p/chromium/codesearch#chromium/src/third_party/WebKit/Source/platform/RuntimeEnabledFeatures.json5)
in alphabetical order. Add your feature's flag to this file and the rest will be
generated for you automatically.

Example:

    {
      name: "AmazingNewFeature",
      status: "experimental",
    }

The `status` of the feature controls when it will be enabled in the Blink
engine.

**Status value** Web feature enabled during layout
content_shell tests \[1\] Web feature enabled as part of web experimental
features \[2\] Web feature enabled in stable release Non-web exposed feature
enabled through a command line flag \[3\] <missing> **No** **No** **No****Yes**
test **Yes** **No** **No****No** experimental **Yes** **Yes** **No****No**
stable **Yes** **Yes** **Yes****No**

\[1\] content_shell will not enable experimental/test features by default, the
--run-layout-test flag used as part of running LayoutTests enables this
behaviour.

\[2\] Navigate to about:flags in the URL bar and turn on "Enable experimental
web platform features" (formerly, "Enable experimental WebKit features")

** or** run Chromium with --enable-experimental-web-platform-features (formerly,
--enable-experimental-webkit-features)

Works in all Chromium channels: canary, dev, beta, and stable.

\[3\] For features that are not web exposed features but require code in Blink
to be triggered. Such feature can have a about:flags entry or be toggled

based on other signals. Such entries should be called out in a comment to
differentiate them from stalled entries.

**Runtime Enabled CSS Properties**

If your feature is adding new CSS Properties you will need to use the
runtime_flag argument in
[Source/core/css/CSSProperties.json5](https://cs.chromium.org/chromium/src/third_party/WebKit/Source/core/css/CSSProperties.json5).

## Using A Runtime Enabled Feature

**C++ Source Code**

Add this include:

    #include "RuntimeEnabledFeatures.h"

This will provide following static methods to check/set whether your feature is
enabled:

    bool RuntimeEnabledFeatures::amazingNewFeatureEnabled();
    void RuntimeEnabledFeatures::setAmazingNewFeatureEnabled(bool isEnabled);

**Note: **methodNames are in lowerCamelCase, while FeatureNames are in
UpperCamelCase. This is handled automatically in code generators, and works even
if the feature's flag name begins with an acronym such as "CSS", "IME", or
"HTML".

For example "CSSMagicFeature" becomes
`RuntimeEnabledFeatures::cssMagicFeatureEnabled()` and
`RuntimeEnabledFeatures::setCSSMagicFeatureEnabled(bool)`.

**IDL files**

Use the [Blink extended attribute](webidl/blink-idl-extended-attributes.md)
`[Runtime`Enabled\] as in \[`Runtime`Enabled`=AmazingNewFeature]` in your IDL
definition.

**Note:** FeatureNames are in UpperCamelCase; please use this case in IDL files.

You can guard the entire interface, as in this example:

    [
            RuntimeEnabled=AmazingNewFeature  // Guard the entire interface.
    ] interface AmazingNewObject {
        attribute DOMString amazingNewAttribute;
        void amazingNewMethod();
    };

Alternatively, you can guard individual definition members:

    interface ExistingObject {
        attribute DOMString existingAttribute;
        // Guarded attribute.
        [RuntimeEnabled=AmazingNewFeature] attribute DOMString amazingNewAttribute;
        // Guarded method.
        [RuntimeEnabled=AmazingNewFeature] void amazingNewMethod();
    };

**Note:** You *cannot* guard individual arguments, as this is very confusing and
error-prone. Instead, use overloading and guard the overloads.

For example, instead of:

    interface ExistingObject {
        foo(long x, [RuntimeEnabled=FeatureName] optional long y); // Don't do this!
    };

do:

    interface ExistingObject {
        // Overload can be replaced with optional if [RuntimeEnabled] is removed
        foo(long x);
        [RuntimeEnabled=FeatureName] foo(long x, long y);
    };

**Warning: **You will not be able to change the enabled state of these at
runtime as the V8 object templates definitions are created during start up and
will not be updated during runtime.

## Layout Tests (JavaScript)

Test whether a feature is enabled using:

    internals.runtimeFlags.amazingNewFeatureEnabled

This attribute is read only and cannot be changed.

**Note:** The `internals` JavaScript API is only available in ContentShell for
use by Layout Tests and does not appear in released versions of Chromium.

## Running layout tests

When content_shell is run with --stable-release-mode flag, test-only features
(ones listed in RuntimeEnabledFeatures.json5 with "test") are turned off.

## Generated Files

[Source/build/scripts/make_runtime_features.py](http://src.chromium.org/viewvc/blink/trunk/Source/build/scripts/make_runtime_features.py)
uses
[RuntimeEnabledFeatures.json5](https://cs.chromium.org/chromium/src/third_party/WebKit/Source/platform/RuntimeEnabledFeatures.json5)
to generate:

<compilation directory>/gen/webkit/RuntimeEnabledFeatures.h

<compilation directory>/gen/webkit/RuntimeEnabledFeatures.cpp

[Source/build/scripts/make_internal
runtime_flaps.py](http://src.chromium.org/viewvc/blink/trunk/Source/build/scripts/make_internal_runtime_flags.py)
uses
[RuntimeEnabledFeatures.json5](https://cs.chromium.org/chromium/src/third_party/WebKit/Source/platform/RuntimeEnabledFeatures.json5)
to generate:

<compilation directory>/gen/webkit/InternalRuntimeFlags.idl

<compilation directory>/gen/webkit/InternalRuntimeFlags.h

[Source/bindings/scripts/CodeGeneratorV8.pm](http://src.chromium.org/viewvc/blink/trunk/Source/bindings/scripts/CodeGeneratorV8.pm)
uses the generated InternalRuntimeFlags.idl to generate:

<compilation directory>/gen/webcore/bindings/V8InternalRuntimeFlags.cpp

## **Command-line switches**

`content` provides two switches which can be used to turn runtime enabled
features on or off, intended for use during development. They are exposed by
both `content_shell` and `chrome`.

    --enable-blink-features=SomeNewFeature,SomeOtherNewFeature
    --disable-blink-features=SomeOldFeature

After applying most other feature settings, the features requested feature
settings (comma-separated) are changed. "disable" is applied later (and takes
precedence), regardless of the order the switches appear on the command line.
These switches only affects Blink's state. Some features may need to be switched
on in Chromium as well; in this case, a specific flag is required.

**Announcement**

<https://groups.google.com/a/chromium.org/d/msg/blink-dev/JBakhu5J6Qs/re2LkfEslTAJ>
