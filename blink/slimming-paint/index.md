# Slimming Paint (a.k.a. Redesigning Painting and Compositing)

## Slimming Paint is a [Paint team](../../teams/paint-team/index.md) project to re-implement the Blink<->cc picture recording API to work in terms of a global display list rather than a tree of cc::Layers (~aka GraphicsLayer in Blink terminology). It will result in a drastic simplification of the way that composited layers are represented in Blink and cc, which in turn will yield improved performance, correctness and flexibility.

To get a sense of the extent of this rewrite, one side-effect will be the
deletion of the code in Sourcecore/rendering/compositing/.

## Performance tracking

Detailed data is in [this
document](https://docs.google.com/document/d/1moskT1Tkg8GcHZylRNQakyrCjk3LR_O5Yys9fDriVcY/edit#).
Improvements are tracked at [crbug.com/464910](http://crbug.com/464910)

## Presentations

[BlinkOn 3.0
Presentation](https://docs.google.com/presentation/d/1zpGlx75eTNILTGf3s_F6cQP03OGaN2-HACsZwEobMqY/edit?usp=sharing),
[video](https://www.youtube.com/watch?v=5Xv2A7aqJ9Y) (start here to find out
more about the project)

[BlinkOn 4
presentation](https://docs.google.com/presentation/d/17k62tf1zc5opvIfhCXMiL4UdI9UGvtCJbUEKMPlWZDY/edit)

[Blink Property
Trees](https://docs.google.com/presentation/d/1ak7YVrJITGXxqQ7tyRbwOuXB1dsLJlfpgC4wP7lykeo)
(also reviews SPV2 design)

## Project Management docs

Weekly meeting notes are
[here](https://docs.google.com/a/google.com/document/d/1XdLjBbSLZU0M_nrR4yUAUYQ6ZWxdjIYvErZ5xT-mWvM/edit#heading=h.3pw2adwrdq6j)
(Google only, sorry. silk-dev google group has public version)

The Blink-side work is ongoing, and has two major components: implement the
Display List version of Blink's painting code, and factor Blink's painting code
into the core/paint directly (tracked [here](http://crbug.com/412088)).

Ongoing tasks are coordinated in [this
spreadsheet](https://docs.google.com/a/chromium.org/spreadsheets/d/19QATJ5y7gBQW1xpYhn4lO83Zt0sE0-dKYj-BszFUZCY/edit?usp=sharing).
Bugs are tracked via the
[Hotlist-Slimming-Paint](https://code.google.com/p/chromium/issues/list?can=2&q=label:Hotlist-Slimming-Paint&sort=pri&colspec=ID%20Pri%20M%20Week%20ReleaseBlock%20Cr%20Status%20Owner%20Summary%20OS%20Modified)
hotlist (for Googlers: go/slimming-paint-bugs).

Code reviews are cc'd to slimming-paint-reviews@chromium.org.

### Core team members

Chris Harrelson (chrishtr@), overall TL

Philip Rogers (pdr@) Blink

Stephen Chenney (schenney@) Blink

Tien-Ren Chen (trchen@) Blink

Xianzhu Wang (wangxianzhu@) Blink

Walter Korman (wkorman@) Blink

Ali Juma (ajuma@) cc

Ian Vollick (vollick@) cc

Adrienne Walker (enne@) cc

Weiliang Chen (weiliangc@) cc

#### Close relatives

Fredrik Söderquist (fs@opera.com) Blink

Erik Dahlstrom (ed@opera.com) Blink

Florin Malita (fmalita@) Blink / Skia

Mike Klein (mtklein@) Skia

Mike Reed (reed@) Skia

A number of other people are involved at least tangentially for design
discussions and related projects.

## Design Docs

[Slimming paint
invalidation](https://docs.google.com/document/d/1M669yu7nsF9Wrkm7nQFi3Pp2r-QmCMqm4K7fPPo-doA)

[Tracking slimming paint
performance](https://docs.google.com/document/d/1moskT1Tkg8GcHZylRNQakyrCjk3LR_O5Yys9fDriVcY/edit#)

[Representation and implementation of display lists in
Blink](https://docs.google.com/document/d/1fWvFIY41BJHtB4qBHw3_IZYqScurID4KmE2_a6Be0J4/edit?usp=sharing)

[Layerization based on display
lists](https://docs.google.com/a/google.com/document/d/1L6vb9JEPFoyt6eNjVla2AbzSUTGyQT93tQKgE3f1EMc/edit)

[Blink paintlist update algorithm
details](https://docs.google.com/document/d/1bvEdFo9avr11S-2k1-gT1opdYWnPWga68CK3MdoYV7k/edit?usp=sharing)

[Bounding Rectangle Strategy for Slimming
Paint](https://docs.google.com/a/chromium.org/document/d/12G3rfM3EkLYDCRcO1EpEObfeoU24Sqof9S0sPHgELU4/edit?usp=sharing)

[Slimming Paint for UI
Compositor](https://docs.google.com/a/chromium.org/document/d/1Oxa3E-ymCqY2-7AlMrL1GEqAtyFE__0PRzhg9EEZt7Y/edit)

[Display Item
Debugging](https://docs.google.com/a/chromium.org/document/d/1XDz2paww41UjviZam1iTThS9XKx0KRcmzI83QuNLeR8/edit#)

Some out of date/historical docs are [here](historical-documents.md).
