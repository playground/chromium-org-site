# Web Components Project

## The [DOM team](../teams/dom-team.md) is working on the specifying and implementing Web Components.

## Projects

*   Shadow DOM Specification (hayato@) \[on-going\]
    *   [Editor's Draft](http://w3c.github.io/webcomponents/spec/shadow/)
    *   [Commits
        History](https://github.com/w3c/webcomponents/commits/gh-pages/spec/shadow/)
*   Shadow DOM - Support closed shadow trees (hayato@) \[on-going\]
    *   [W3C Bug](https://www.w3.org/Bugs/Public/show_bug.cgi?id=27775)
    *   [Chromium
        Bug](https://code.google.com/p/chromium/issues/detail?id=459136)
*   Shadow DOM - Better focus support (kochi@) \[on-going\]
    *   [Chromium
        Bug](https://code.google.com/p/chromium/issues/detail?id=380445)
    *   [Design
        Doc](https://docs.google.com/a/chromium.org/document/d/1k93Ez6yNSyWQDtGjdJJqTBPmljk9l2WS3JTe5OHHB50/edit)
*   Selection for Shadow DOM (yosin@, hoshihajime@) \[on-going\]
*   Shadow DOM as performance primitive \[2015 Q2 - \]
*   Upstream to HTML Living Standard (hayato@) \[Pending\]
*   Componentized Style Resolver \[2015 Q2 - \]
*   Custom Elements
*   HTML Imports (hiatus)

## Documents / Links

*   [Web Components Repository hosted at
    GitHub](https://github.com/w3c/webcomponents/)

## Contributors

*   hayato (TL, Shadow DOM)
*   domenic (Custom Elements)
*   dominicc (Custom Elements)
*   kochi (Shadow DOM)
*   kojii (Custom Elements)

Emeritus:

*   dglazkov
*   morrita
*   rolandsteiner
*   shinyak
*   slightlyoff
*   tasak
*   TODO: List past interns
