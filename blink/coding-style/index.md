# Blink Coding Style Guidelines

These guidelines are specific to the [Blink](../index.md) project, not to be
confused with the general [Chromium Coding Style
Guidelines](../../developers/coding-style/index.md).

## C++

This documentation has moved to the [Blink C++ Style
Guide](https://chromium.googlesource.com/chromium/src/+/master/styleguide/c++/blink-c++.md).

## Python

Python code should follow [PEP-8](http://www.python.org/dev/peps/pep-0008/),
except the line length limit is 132 instead of 80. This differs from [Chromium
coding style](../../developers/coding-style/index.md) (and the [Chromium OS
Python style guidelines](../../chromium-os/python-style-guidelines.md)) in three
respects:

*   line length limit is larger (132 chars, unlike PEP-8)
*   uses 4 space indent (like PEP-8)
*   uses `function_name, method_name` rather than `FunctionName, MethodName`
    (like PEP-8).

See
[WebKit/Tools/Scripts/webkitpy/pylintrc](https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit/Tools/Scripts/webkitpy/pylintrc).

## Layout tests

See [Layout Test Style Guidelines](layout-test-style-guidelines.md).

## License

Existing files in Blink use a longer header license block inherited from WebKit,
however new files should follow the Chromium [File Header
Style](https://chromium.googlesource.com/chromium/src/+/master/styleguide/c++/c++.md#File-headers):

    // Copyright 2017 The Chromium Authors. All rights reserved.
    // Use of this source code is governed by a BSD-style license that can be
    // found in the LICENSE file.

To use this license block you must make sure you have completed the [External
Contributor
Checklist](../../developers/contributing-code/external-contributor-checklist.md).

## License for this document

*This page began as the [WebKit Coding Style
Guidelines](http://www.webkit.org/coding/coding-style.html), Licensed** under
[BSD](http://www.webkit.org/coding/bsd-license.html):*

BSD License
Copyright (C) 2009 Apple Inc. All rights reserved.
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY APPLE INC. AND ITS CONTRIBUTORS “AS IS” AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## References

\[1\] Comments on comments
<https://lists.webkit.org/pipermail/webkit-dev/2011-January/015769.html>
