# Breaking changes

### Launch Process: breaking changes (removing features or changing existing behavior)

1.  Read the [guidelines for deprecating a
    feature](https://docs.google.com/a/chromium.org/document/d/1LdqUfUILyzM5WEcOgeAWGupQILKrZHidEXrUxevyi_Y/edit?usp=sharing).

2.  Measure feature usage in the wild. [Various
    tools](https://www.chromium.org/blink/platform-predictability/compat-tools)
    are available, but typically this is simply:

    *   Add your feature to the [UseCounter::Feature
        enum](https://code.google.com/p/chromium/codesearch#chromium/src/third_party/WebKit/Source/core/frame/UseCounter.h&sq=package:chromium&type=cs&q=file:UseCounter.h%20Feature).

    *   Add MeasureAs=<your enum value> to the feature's IDL definition.\*

3.  Email blink-dev using the ["Intent to Remove"
    template](https://docs.google.com/a/chromium.org/document/d/1Z7bbuD5ZMzvvLcUs9kAgYzd65ld80d5-p4Cl2a04bV0/edit#bookmark=id.cvoy7bb78bkl).

    *   Respond to any feedback or questions raised in the thread

    *   You need at least 3 LGTMs from [API
        owners](http://www.chromium.org/blink#TOC-API-Owners) to remove.

    *   API owners will attempt to apply the [principles of web
        compatibility](https://docs.google.com/document/u/2/d/1RC-pBBvsazYfCNNUSkPqAVpSpNJ96U8trhNkfV0v9fk/edit?usp=drive_web)
        (but you are not required to be familiar with the details here).

    *   If you have resolved all feedback and are blocked on API owner LGTMs (or
        would just like advice in a smaller forum), contact
        [blink-api-owners-discuss@chromium.org](mailto:blink-api-owners-discuss@chromium.org).

4.  Notify developers and measure usage.

    *   Update the appropriate chromestatus entry, noting the feature as
        deprecated. Make sure the entry links to suggested alternatives.

    *   Notify developers by adding a deprecation console message.

        *   Point to the updated chromestatus entry in the console message.

        *   Add the API to the big switch in
            [UseCounter::deprecationMessage](http://src.chromium.org/viewvc/blink/trunk/Source/core/frame/UseCounter.cpp#l120).

        *   Give developers as many milestones as possible to respond to the
            deprecation.

    *   Instrument your code by either:

        *   Adding DeprecateAs=\[your enum value here\] to the feature's IDL
            definition.\* -- See window.performance.webkitGetEntries.

        *   Adding a call to UseCounter::countDeprecation somewhere relevant (as
            we did for the [prefixed Content Security Policy
            headers](http://src.chromium.org/viewvc/blink/trunk/Source/core/page/ContentSecurityPolicy.cpp?r1=149184&r2=149193)).

5.  Remove the feature.

    *   Update the appropriate [chromestatus.com](http://chromestatus.com)
        entry, marking the feature as removed.

If you are unsure of when a feature could be removed, or would like to
discourage usage, you may deprecate a feature without a removal deadline. This
is strongly discouraged and will require significant justification:

*   Email blink-dev using the ["Intent to Deprecate"
    template](https://docs.google.com/a/chromium.org/document/d/1Z7bbuD5ZMzvvLcUs9kAgYzd65ld80d5-p4Cl2a04bV0/edit).

    *   1 LGTM necessary from API Owners

    *   Must justify why there is no removal date

\* It takes 12-18 weeks to hit Stable once you enable instrumentation. See [this
thread](https://groups.google.com/a/chromium.org/d/msg/blink-dev/bs3NIiqnc8Q/2kF7HNr4kSAJ)
for a discussion of which usage percentages are safe to remove.

**Lessons from the first year of deprecations and removals
**([thread](https://groups.google.com/a/chromium.org/forum/#!topic/blink-dev/1wWhVoKWztY))

*   We should weigh the benefits of removing an API more against the cost it
    has. Percent of page views by itself is not the only metric we care about.
*   The cost of removing an API is not accurately reflected by the UseCounter
    for older, widely implemented APIs. It's more likely that there's a
    longer-tail of legacy content that we're breaking.
*   We shouldn't remove APIs that have small value on the path towards a removal
    that has significant value. Getting rid of attribute nodes \*is\* valuable
    and would benefit the platform. Getting rid of half the attribute node
    methods is not. So we should evaluate the usage of all the APIs we need to
    remove together in order to get there. Also, if we remove them, we should
    remove them all in the same release. Breaking people once is better than
    breaking them repeatedly in small ways.
*   We should be more hesitant to remove older, widely implemented APIs.
*   For cases where we're particularly concerned about the compatibility hit, we
    should do the removal behind a flag so that we can easily re-enable the API
    on stable as we don't know the compat hit until the release has been on
    stable for a couple weeks.
