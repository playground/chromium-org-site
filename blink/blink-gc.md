# Garbage Collection for Blink C++ objects (a.k.a. Oilpan)

This documentation was moved to
<https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit/Source/platform/heap/BlinkGCAPIReference.md>.
