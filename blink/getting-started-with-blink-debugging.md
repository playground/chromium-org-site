# Getting Started with Blink Debugging

### Introduction

While many of the tools and tips on this page can be used for it, this page
focuses on debugging Blink outside the context of the Layout Tests. For more
Layout Test specific instructions, see [this
page](https://chromium.googlesource.com/chromium/src/+/master/docs/testing/layout_tests.md).
For more general Chromium debugging info, see the respective pages for debugging
on [Windows](../developers/how-tos/debugging-on-windows/index.md),
[Mac](../developers/how-tos/debugging-on-os-x/index.md), and
[Linux](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_debugging.md).

### Linux

#### Getting Started

There are two main ways to get into blink: via debugging the chromium binary
itself or `content_shell`. For most purposes of exclusive Blink debugging, the
latter is the recommended option because it drastically reduces size and
complexity. This means building `content_shell`, which should be as simple as
making it the build target for your build method of choice. This should stick a
`content_shell` binary in your `out/Default` directory.

`content_shell` itself takes as an argument the HTML file you wish to run Blink
on. Furthermore, one of the simplest types of debugging you might want to do is
to see the basic page structure after a page load (this internal structure in
Blink is called the Layout Tree, not to be confused with the DOM Tree or the
Line Box Tree). You can do this with a simple command line option of
`--run-layout-test`. Thus, one of your simplest debugging tools, seeing the page
structure after a page load, might look something like:

    content_shell --run-layout-test test.html

#### Starting the Debugger

Debugging on Linux is generally done with GDB, so we will assume that's what you
are using here. Not surprisingly, you will almost always want to compile Blink
in debug mode to get all the symbols and tools you will need.

**Important: Previous revisions of this document suggested running with the
`--single-process `when debugging. That flag is no longer supported; it may
work, it may fail outright, and it may appear to work yet manifest subtle
differences in behavior that will cause you to waste many hours in understanding
them.**

You may need to modify your system's ptrace security policy before you can debug
the renderer
([details](http://askubuntu.com/questions/41629/after-upgrade-gdb-wont-attach-to-process));
run this command:

    echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope

Both `content_shell` and `chromium` spawn renderer sub-processes; to debug
blink, you need to attach a debugger to one of those sub-processes. The simplest
way to do this is with the `--no-sandbox` and `--renderer-startup-dialog`
parameters:

    out/Default/content_shell --no-sandbox --renderer-startup-dialog test.html

When you do this, `content_shell` will print a message like this:

    [10506:10506:0904/174115:2537132352130:ERROR:child_process.cc(131)] Renderer (10506) paused waiting for debugger to attach. Send SIGUSR1 to unpause.

This tells you that the pid of the renderer process is 10506. You can now attach
to it:

    gdb -p 10506

When gdb loads up, set whatever breakpoints you want in the blink code and
'continue'; for example:

    (gdb) b blink::LayoutView::layout
    (gdb) c

Then, send SIGUSR1 to the renderer process to tell it to proceed:

    (gdb) signal SIGUSR1

If you are running layout test, Tools/Scripts/debug-webkit-test does almost
everything except for SIGUSR1 automatically.

If you see 'Could not find DWO CU' errors, you may need to have a symlink to the
build directory from the current working directory.

    % cd third_party/WebKit
    % ln -s ../../out/Default/obj .
    % ./Tools/Scripts/debug-webkit-tests --target=Default

### General Useful Debugging Tools

#### Debugging functions

There are some key functions built into objects once you've reach a breakpoint
inside Blink. For the examples here, we'll assume you're using GDB on Linux.
These can be incredibly useful for showing the trees midway during execution to
try and identify points when things change. You can use the GDB command print to
display them. Here are some of Blink's debugging functions:

Function Objects it's available on Description showTreeForThis() `Node`s and
LayoutObjects Outputs the DOM tree, marking this with a \*
showLayoutTreeForThis() LayoutObjects Outputs the Layout tree, marking this with
a \* showLineTreeForThis() LayoutObjects and InlineBoxes Outputs the Inline Box
tree for the associated block flow, marking all matching inline boxes associated
with this with a \* showDebugData() DisplayItemLists Outputs the list of display
items and associated debug data

Assuming a local variable `child` in scope that's a `LayoutObject`, the
following will print the Layout Tree:

    (gdb) print child->showLayerTreeForThis()

#### Blink GDB python library

When using a GDB build that supports python, there's a library of useful Blink
functions and pretty printers that can make working with some of Blink's types
easier and more convenient, such as pretty printers for LayoutUnit and
LayoutSize classes. You can find it at third_party/webkit/Tools/gdb/webkit.py;
see
[LinuxDebugging](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_debugging.md#Printing-Chromium-types)
for instructions.

### Printing back trace

#### Make ASSERT print symbols

Run Chrome with `--no-sandbox`. You'll see Chromium's back trace for SEGV.

#### Use Chromium's StackTrace

    #include <base/debug/stack_trace.h>
    ...
    base::debug::StackTrace().Print();

and run Chrome with `--no-sandbox` command line option. Because base is not
included in blink by default, this will only work if you do not use the
component build (or, you update gyp/gn to include base in blink).

### Debugging Printing related issues

It is difficult to understand whether an issue lies in the Print Preview logic
or the Rendering logic. This
[document](https://docs.google.com/document/d/1aK27hiUPEm75OD4Dw2yQ9CmNDkLjL7_ZzglLdHW6UzQ/edit?usp=sharing)
uses some of the tools available to us to help solve this issue.
