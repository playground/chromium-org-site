# Origin Trials

Origin trials are an approach to enable safe experimentation with web platform
features. For more, see <https://github.com/GoogleChrome/OriginTrials>.

Googlers should also refer to the internal documentation at
[go/origin-trials](http://goto.google.com/origin-trials).

**For Blink Contributors / Feature Authors**

So, you're a Blink contributor, and maybe want to run an origin trial for your
feature? Please see our [Feature Author Guide](running-an-origin-trial.md).

**For Web Developers**

Please see our [Web Developer
Guide](https://github.com/GoogleChrome/OriginTrials/blob/gh-pages/developer-guide.md).

**Links**

*   Do you have a trial token, but not sure if it's working? We have a [page to
    check tokens](https://googlechrome.github.io/OriginTrials/check-token.html).
*   [Current
    Trials](https://github.com/GoogleChrome/OriginTrials/blob/gh-pages/available-trials.md)
*   [Completed
    Trials](https://github.com/GoogleChrome/OriginTrials/blob/gh-pages/completed-trials.md)

Questions or comments? Contact us at
[experimentation-dev@chromium.org](mailto:experimentation-dev@chromium.org).
