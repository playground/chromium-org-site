# Issue Tracking Lifecycle

This article explores how issue tracking works on the Chromium project, covering
the four key process phases.

## The Four Phases

![image](Conceptual-Model.png)

### Reporting

....

What happens in this step?

Who is involved?

What's the forward objective?

### Routing

....

What happens in this step?

Who is involved?

What's the forward objective?

### Triage

....

What happens in this step?

Who is involved?

What's the forward objective?

### Action

....

What happens in this step?

Who is involved?

What's the forward objective?

## Status

![image](State-Diagram.jpg)
