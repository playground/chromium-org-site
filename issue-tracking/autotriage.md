# Sheriffbot: Auto-triage Rules for Bugs

Bug reporting, triage and fixing are essential parts of the software development
process. In large software projects such as Chromium, the bug tracking system is
the central hub for coordination that collects informal comments about bug
reports and development issues. Hundreds of bug reports are submitted every day,
and the triage process is often error prone, tedious, and time consuming. In
order to make triage more efficient, we automate certain tasks.

Googlers can view sheriffbot's source at
[go/sheriffbot-source](https://goto.google.com/sheriffbot-source).

### *   **Unconfirmed bugs which are not updated for a year**

    *   Purpose: Bugs often slip through the cracks. If they've gone unconfirmed
        for over a year, it is unlikely that any progress will be made.
    *   Action: Set the status to **Archived**.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/gt9yO5).

### *   **Available bugs which have no associated component and have not been
        modified for a year**

    *   Purpose: Clear out bug backlog by archiving bugs which are unlikely to
        receive attention and let reporters know that they should re-file if
        they still care about the issue.
    *   Action: Add a comment and change the status to **Archived**.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/ZdItuL).

### *   ** Available bugs with an associated component and are not modified for
        a year**

    *   Purpose: Recirculate stale bugs in case they were mistriaged initially.
    *   Action: Add a comment, apply the tracking label
        **Hotlist-Recharge-Cold** and change the status to **Untriaged**. The
        **Hotlist-Recharge-Cold** label is applied for tracking purposes, and
        should not be removed after re-triaging the issue.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/L9e6w5).

### *   Launch bug for experimental features - update and clean up

    *   Launch bugs that are at least 3 milestones older than current stable:
        *   Purpose: In this case, we assume that the launch bug has been
            forgotten and that the feature has already shipped.
        *   Action: Add a warning message asking for review. If no action is
            taken after 30 days, set the status to **Archived**.
        *   If you disagree or have any concern, please report it
            [here](https://goo.gl/p6d0Df).
    *   Launch bugs with no milestone and are not modified in the last 90 days:
        *   Purpose: Launch bugs should always be targeted for a specific
            milestone, so something has gone wrong if a bug is in this state.
        *   Action: Add a comment requesting that an appropriate milestone is
            applied to the issue.
        *   If you disagree or have any concern, please report it
            [here](https://goo.gl/CTpCYY).

### *   **Needs-Feedback bugs, which received feedback.**

    *   Purpose: Developers often request feedback, but don't follow-up after it
        is received.
    *   Action: If a reporter has provided feedback, remove the
        **Needs-Feedback** label, and add the feedback requester to the bug
        cc'list.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/nq6KdV).

### *   Needs-Feedback bugs where feedback is not received for more than 30 days

    *   Purpose: If required feedback is not received, it is unlikely that any
        progress can be made on the issue.
    *   Action: Add a comment and set the status to **Archived**.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/O87GzR).

### *   **P0 bug reminders**

    *   Purpose: P0 bugs are critical bugs which require immediate action.
    *   Action: If the bug is open for more than 3 days, we leave a comment as a
        reminder. Note that a maximum of 2 nags will be applied to the issue.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/y5F8jD).

### *   TE-Verified bugs which are not modified for 90 days (This rule has been
        removed as of 08/04/16. Reference bug: 630626)

    *   Purpose: Developers often forget to close bugs out after they've been
        verified.
    *   Action: Set the status to **Archived**.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/p6UePt).

### *   **Medium+ severity security bug nags on issues not updated by their
        owners in the last 14 days**

    *   Purpose: Security vulnerabilities are serious threats to our users that
        require developer attention.
    *   Action: Leave a comment asking for a status update. Note that a maximum
        of 2 nags will be applied to the issue.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/3fQo3H).

### *   Merge-Approval clean up

    *   Purpose: Developers often forget to remove **Merge-Approved-MX** label
        after merging change to specific branch.
    *   **Action: If the bug is not updated in specific milestone branch cycle
        (42 days), r**emove all label that starts with **Merge-Approved-MX**
        label and add a comment asking developers to re-request a merge if
        needed.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/PWhrVG).

### *   Milestone punting

    *   Purpose: Update issues with latest milestone after branch point.
    *   **Action: Replace milestone label with latest milestone for milestones
        that have passed their branch point. Certain issues, such as Launch
        bugs, Pri-0 issues, Release blockers, and issues where merges have been
        requested or approved are excluded.**
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/ToIu3l).

### *   **Open bugs with CL landed which are not modified for 6 months **(This
        rule has been disabled as of 07/22/16, pending furthur discussion.
        Reference bug: 629092)

    *   Purpose: Developers often forget to close the bugs after landing CLs.
    *   Action:
        *   Send a reminder by adding **Hotlist-OpenBugWithCL** label asking
            Developers to either:
            *   Close bug if all neccessary CLs have landed and no furthur work
                is needed OR
            *   Remove **Hotlist-OpenBugWithCL** label if more changes are
                needed.
        *   If no action is taken in 30 days, set the status to **Archived**.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/SMCepR).

*   *   Bugs with no stars, no owners and not modified for an year (This rule
        has been disabled as of 08/12/16. Reference bug: 637278)

*   *   Purpose: Issues with no owners, no stars are unlikely to receive any
        attention, so clear-out these issues and let reporters know that they
        should re-file if they still care about the issue.
*   *   **Action: **Add a comment and change the status to **Archived.**
*   *   If you disagree or have any concern, please report it
        [here](https://goo.gl/zeoYMX).

*   *   Set for re-triage, the issues with bouncing owners.

*   *   Purpose: Issues with owners who cannot be contacted (bouncing owners) is
        unlikely to get resolved by the owner, so clear out these issues by
        sending for re-triaging again.
*   *   **Action: R**emove the bounced owner, add comment, apply the tracking
        label **Hotlist-Recharge-BouncingOwner** and change the status to
        **Untriaged**.
*   *   If you disagree or have any concern, please report it
        [here](https://goo.gl/kF0Bhu).

*   *   Cleanup any auto-generated issues with no owners and are inactive for
        more than 90 days.

*   *   Purpose: Auto-generated issues with no owners and more than 90 days of
        inactivity is unlikely to be viewed and resolved, so clean up these
        issues by archiving them.
*   *   **Action: Add comment and **set the status to **Archived**.
*   *   If you disagree or have any concern, please report it
        [here](https://goo.gl/NqpRdm).

    *   Reminder for release blocking issues with no milestone and OS labels.

    *   Purpose: All release blocking bugs should have milestone and OS label
        associated with it in order to release the fixes promptly.
    *   **Action: **Leave a comment asking to add milestone and OS labels and cc
        the release block label adder to the issue. Note that a maximum of 2
        nags will be applied to the issue.
    *   If you disagree or have any concern, please report it
        [here](https://goo.gl/6AWK49).
