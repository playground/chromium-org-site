# Creating a Bug Template Url

The following fields can be pre-populated with a value supplied in the URL.
Using links with these query parameters present allows you to guide end-users to
enter an issue that is tailored to a specific need. Usually defining a template
and specifying just a template value is better than specifying other values in a
URL. The user must still review, edit, and submit the form.

Form field URL query parameter Values Template template Name of a defined
template in this project Summary summary Initial summary string Description
comment Prompt text to show in issue description area Status status Initial
status value Owner owner Username of initial issue owner CC cc List of users to
CC Components components Comma-separated list of initial component values Labels
labels Comma-separated list of initial label values

**Constructing your URL:**

*   Start w/ the base url -
    https://bugs.chromium.org/p/**<project_name>**/issues/entry
    *   Example: https://bugs.chromium.org/p/chromium/issues/entry
*   Add your query parameters
    *   Example (using the "Defect from user" template) -
        https://bugs.chromium.org/p/chromium/issues/entry***?template=Defect%20report%20from%20user***
    *   [Wiki](https://en.wikipedia.org/wiki/Query_string#Web_forms) - How url
        parameters work.
*   Publish as a quick link (<https://goo.gl/> works well).

**Tips and Tricks:**

*   For descriptions you'll need to url encode the text. Here's a
    [site](http://meyerweb.com/eric/tools/dencoder/) to help you.
