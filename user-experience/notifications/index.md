# Notifications

**UI under development. Designs are subject to change.**

Chrome OS Notifications

![image](Notifications.png)

The notifications panel is used for all OS and website notifications

Functionality:

*   Pops up along the bottom for any new notifications
*   Slides down after a delay
*   Can be closed to hide all notifications
*   Can be opened from the apps list
*   Sticky notifications (like the low battery notification) do not slide down
    unless the user clicks on the titlebar

Future goals

*   We may eventually anchor this panel in a special location on the screen, but
    for now it behaves as a standard panel, opening to the left of any existing
    panels.
*   Notifications should be able to badge application icons (like browser
    actions).
*   Better support for notification actions (links along the bottom).
