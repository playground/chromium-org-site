# User Data Directory

The user data directory contains data specific to a given user. Some examples of
this type of data are: history, bookmarks, and cookies.

Note that in Windows, the profile name is included in the directory hierarchy.

## Default Location

**Windows 10 / 8 / 7**

*   Google Chrome: C:\\Users\\%USERNAME%\\AppData\\Local\\Google\\Chrome\\User
    Data\\Default
*   Chromium: C:\\Users\\%USERNAME%\\AppData\\Local\\Chromium\\User
    Data\\Default

#### Mac OS X

*   **Google Chrome:** ~/Library/Application Support/Google/Chrome/Default
*   **Chromium:** ~/Library/Application Support/Chromium/Default

#### Linux

*   **Google Chrome:** ~/.config/google-chrome/Default
*   **Chromium:** ~/.config/chromium/Default

**Chrome OS**

*   /home/chronos/

### Cache Directory

The cache directory follows the XDG specs:

*   **Google Chrome:** $XDG_CACHE_HOME/google-chrome, which defaults to
    ~/.cache/google-chrome
*   **Chromium:** $XDG_CACHE_HOME/chromium, which defaults to ~/.cache/chromium

If you pass --user-data-dir=/path/to/foo, the cache will be under /path/to/foo.

## Running from a Custom Location

You can also start Chromium with a custom user data directory in order to run
multiple instances at the same time.

#### Windows

To do this, add the --user-data-dir flag to chrome.exe, like this: chrome.exe
--user-data-dir=c:\\foo

#### Mac OS X

1.  Open AppleScript Script Editor (either in Applications/Utilities or
    Applications/AppleScript)
2.  Enter this:
    1.  Chromium:
        do shell script "/Applications/Chromium.app/Contents/MacOS/Chromium
        --user-data-dir=/Users/$USER/Library/Application\\\\
        Support/ChromiumPersonal > /dev/null 2>&1 &"
    2.  Google Chrome: do shell script "/Applications/Google\\\\
        Chrome.app/Contents/MacOS/Google\\\\ Chrome
        --user-data-dir=/Users/$USER/Library/Application\\\\
        Support/Google/ChromePersonal > /dev/null 2>&1 &"
3.  If you did not install Chromium in the default location, modify the script
    as appropriate.
4.  Save the script in your Applications directory with the file format
    Application.
5.  Now close the Script Editor and find your newly created application and run
    it. Running this application will open another Chromium instance pointing to
    your new profile.
6.  If you want this application to have the same icon as Chromium, then select
    the Chromium application and type ***command+i ***to open the info dialog.
    Select the icon at the top left of the info dialog and you will see a blue
    highlight around the icon. Copy the icon using *****command***+c**. Now open
    the info dialog for the new script application using *****command***+i**.
    Select the icon at the top left and paste the copied icon using
    *****command***+v**.

#### Linux

To do this, add the **--user-data-dir** flag to chrome, like this: **chrome
--user-data-dir=/path/to/foo**
