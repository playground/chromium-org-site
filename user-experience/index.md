# User Experience

This section describes the motivations, assumptions, and directions behind
Chromium and Chromium OS's user interface design.

Its goal is to explain the current design in a way that further work can be
developed in-style, or so that our assumptions can be challenged, changed, and
improved.

![image](Chrome.png)

## Chrome Features

**Window**

[Window Frame](window-frame/index.md) | [Tabs](tabs/index.md) |
[Throbber](tabs/throbber/index.md) | [Toolbar](toolbar.md) |
[Omnibox](omnibox/index.md)

**Browsing**

[Bookmarks](bookmarks/index.md) | [History](history/index.md) | [New Tab
Page](new-tab-page/index.md)

**Additional UI**

[Downloads](downloads/index.md) | [Status Bubble](status-bubble/index.md) |
[Find in Page](find-in-page/index.md) | [Options](options/index.md) |
[Incognito](incognito/index.md)

[Notifications](notifications/index.md) | [Infobars](infobars/index.md) |
[Multiple Chrome Users](multi-profiles/index.md)

**Appearance**

[Visual Design](visual-design/index.md) | [Resolution
Independence](resolution-independence/index.md) | Themes

**Accessibility**

[Keyboard Access](keyboard-access.md) | Touch Access | [Low-Vision
Support](low-vision-support.md) | [Screen reader
support](assistive-technology-support.md)

## UX themes

**Content not chrome**

*   In the long term, we think of Chromium as a tabbed window manager or shell
    for the web rather than a browser application. We avoid putting things into
    our UI in the same way you would hope that Apple and Microsoft would avoid
    putting things into the standard window frames of applications on their
    operating systems.
*   The tab is our equivalent of a desktop application's title bar; the frame
    containing the tabs is a convenient mechanism for managing groups of those
    applications. In future, there may be other tab types that do not host the
    normal browser toolbar.
*   Chrome OS: A system UI that uses as little screen space as possible by
    combining apps and standard web pages into a minimal tab strip: While
    existing operating systems have web tabs and native applications in two
    separate strips, Chromium OS combines these, giving you access to everything
    from one strip. The tab is the equivalent of a desktop application's title
    bar; the frame containing the tabs is a simple mechanism for managing sets
    of those applications and pages. We are exploring [three main
    variants](../chromium-os/user-experience/window-ui/index.md) for the window
    UI. All of them reflect this unified strip.
*   Chrome OS: Reduced window management: No pixel-level window positioning,
    instead operating in a full-screen mode and exploring new ways to handle
    secondary tasks:
    *   Panels, floating windows that can dock to the bottom of the screen as a
        means of handling tasks like chat, music players, or other accessories.
    *   Split screen, for viewing two pieces of content side-by-side.

**Light, fast, responsive, tactile**

*   Chromium should feel lightweight (cognitively and physically) and fast.

**Web applications with the functionality of desktop applications**

*   Enhanced functionality through HTML 5: offline modes, background processing,
    notifications, and more.
*   Better access points and discovery: On Chromium-based browsers, we've
    addressed the access point issue by allowing applications to install
    shortcuts on your desktop. Similarly, we are using [pinned
    tabs](../chromium-os/user-experience/tab-ui/index.md) and search as a way to
    quickly access apps in Chromium OS.
*   While the tab bar is sufficient to access existing tabs, we are creating a
    new primary [access
    point](../chromium-os/user-experience/access-points/index.md) that provides
    a list of frequently used applications and tools.

**Search as a primary form of navigation**

*   Chromium's address bar and the Quick Search Box have simplified the way you
    access personal content and the web. In Chromium OS, we are unifying the
    behavior of the two, and exploring how each can be used to make navigation
    faster and more intuitive.

![image](ChromeOS.png)

## Chrome OS Features

**Note: UI under development. Designs are subject to change.**

**Primary UI**

[Window UI Variations](../chromium-os/user-experience/window-ui/index.md) |
[Window Management](../chromium-os/user-experience/window-management/index.md) |
[Pinned Tabs](../chromium-os/user-experience/tab-ui/index.md) | [Apps
Menu](../chromium-os/user-experience/access-points/index.md) |
[Panels](../chromium-os/user-experience/panels/index.md)

[UI Elements](../system/errors/NodeNotFound) | [Gestures](multitouch/index.md) |
[System Status
Icons](../chromium-os/user-experience/system-status-icons/index.md)

**Core Applications**

[Settings](javascript:void(0);) | [Content
Browser](../chromium-os/user-experience/content-browser/index.md) | [Open/Save
Dialogs](../chromium-os/user-experience/opensave-dialogs/index.md) |
[Shelf](../chromium-os/user-experience/shelf/index.md)

**Devices**

[Form Factors](../chromium-os/user-experience/form-factors/index.md) |
[Resolution Independence](resolution-independence/index.md)

## Video and Screenshots

The implementation, the concept video, and the screenshots are presenting
different UI explorations. Expect to see some variation.

**![image](http://www.youtube.com/watch?v=hJ57xzo287U)**

![image](../chromium-os/user-experience/sdres_0000_Basic.png)
![image](../chromium-os/user-experience/sdres_0001_App-Menu.png)
![image](../chromium-os/user-experience/sdres_0002_Panels.png)
