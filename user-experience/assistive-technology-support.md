# Accessibility: Assistive technology support

To discuss the use of Chrome with assistive technology, please join the
[chromium-accessibility](http://groups.google.com/a/chromium.org/group/chromium-accessibility)
group.

A *screen reader* is one type of assistive technology used by visually impaired
users, or users with other disabilities, who need the screen described to them,
often using synthesized text-to-speech or via a refreshable braille display.

A *magnifier* is another type of assistive technology, for users with
low-vision. It may magnify the entire screen or only a portion of the screen. It
might additionally provide extra visual cues to help the user find the cursor
and the current focused control.

Chrome is compatible with many screen readers and magnifiers.

In addition, Accessibility APIs can be used for automation or testing.

#### **Supported Assistive Technology**

We test Chrome with the following assistive technology:

#### *   [JAWS](http://www.freedomscientific.com/products/fs/jaws-product-page.asp)
         version 12 and higher (Windows)
#### *   [NVDA](http://www.nvda-project.org/) (Windows)
#### *   [System Access to Go](http://www.satogo.com/) (Windows)
#### *   [ZoomText](http://www.aisquared.com/zoomtext) version 10 and higher
         (Windows)
#### *   [VoiceOver](http://www.apple.com/accessibility/voiceover/) (Mac OS X)

#### These assistive technology are known not to work:

*   [Window-Eyes](https://www.gwmicro.com/Window-Eyes/) (Windows)
*   [Orca](https://live.gnome.org/Orca) (Linux)

#### **Getting the latest version of Chrome**

#### If you are reporting bugs, please test the "Canary" version of Chrome, which can be installed alongside the standard version you already have installed. It helps a lot to know if the bug reproduces in the latest version or not.

[Chrome Canary download
instructions](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&ved=0CDcQFjAA&url=https%3A%2F%2Fwww.google.com%2Fintl%2Fen%2Fchrome%2Fbrowser%2Fcanary.html&ei=3x52UcGWPMGpiAKZhoBg&usg=AFQjCNEmCd7QnngnWgQiA5M_Vyvwzq71Vw&sig2=lTuBDKvb6i4xs7Ph8fZjGQ&bvm=bv.45580626,d.cGE)

If you will be trying Chrome with a Windows screen reader, you may want to read
the page on [Keyboard Access](keyboard-access.md) first.

#### Other pages on accessibility

*   [Accessibility: Keyboard Access](keyboard-access.md)
*   [Accessibility: Touch Access](touch-access/index.md)
*   [Accessibility: Low-Vision Support](low-vision-support.md)
*   [Accessibility Design
    Document](../developers/design-documents/accessibility/index.md) (for
    developers)
