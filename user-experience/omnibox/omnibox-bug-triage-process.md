# Omnibox Bug Triage Process

The document describing the [omnibox bug triage
process](https://chromium.googlesource.com/chromium/src.git/+/HEAD/components/omnibox/bug-triage.md)
lives in the source tree. This page merely exists to aid search and discovery.
