# Become a Committer

## What is a committer?

Technically, a committer is someone who has write access to the Chromium Git
repository or the Chromium OS Git repository. A committer can submit his or her
own patches or patches from others.

This privilege is granted with some expectation of responsibility: committers
are people who care about the Chromium projects and want to help them meet their
goals. A committer is not just someone who can make changes, but someone who has
demonstrated his or her ability to collaborate with the team, get the most
knowledgeable people to review code, contribute high-quality code, and follow
through to fix issues (in code or tests).

A committer is a contributor to the Chromium projects' success and a citizen
helping the projects succeed. See [Committer's
responsibility](../developers/committers-responsibility.md).

## Becoming a committer

In a nutshell, contribute 10-20 non-trivial patches and get at least three
different people to review them (you'll need three people to support you). Then
ask someone to nominate you. You're basically demonstrating your

*   commitment to the project (10+ good patches requires a lot of your valuable
    time),
*   ability to collaborate with the team,
*   understanding of how the team works (policies, processes for testing and
    code review, [OWNERS](http://www.chromium.org/developers/owners-files)
    files, etc),
*   understanding of the projects' code base and coding style, and
*   ability to write good code (last but certainly not least)

A current committer nominates you by sending email to committers@chromium.org
containing the following information. Please do not CC the nominee on the
nomination email.

*   your first and last name
*   your email address. You can also ask to get an @chromium.org email address
    at this time, if you don't already have one.
*   an explanation of why you should be a committer,
*   embedded list of links to revisions (about top 10) containing your patches

Two other committers need to second your nomination. If no one objects in 5
working days (U.S.), you're a committer. If anyone objects or wants more
information, the committers discuss and usually come to a consensus (within the
5 working days). If issues can't be resolved, there's a vote among current
committers.

That's it! There is no further action you need to take on your part. The
committers will get back to you once they make a decision.

In the worst case, this can drag out for two weeks. Keep writing patches! Even
in the rare cases where a nomination fails, the objection is usually something
easy to address like "more patches" or "not enough people are familiar with this
person's work."

Once you get approval from the existing committers, we'll send you instructions
for write access to Git. You'll also be added to committers@chromium.org. If you
work for Google, you are expected to [become a
sheriff](../developers/tree-sheriffs/index.md) at this point as well (see the
internal instructions for how to add yourself to the rotations).

## Other statuses

If you just want to edit bugs, see: [Get Bug-Editing
Privileges](get-bug-editing-privileges.md).

### Try job access

If you are contributing patches but not (yet) a committer, you may wish to have
be able to run jobs on the [try
servers](../developers/testing/try-server-usage/index.md) directly rather than
asking a committer or reviewer to do so for you. To get access:

If you have an @chromium.org email address and wish to use it for your account:

*   Fill in the [request form](https://chromium-access.appspot.com/request) at
    [Chromium access](https://chromium-access.appspot.com/) (read-only, so leave
    the Write access box unchecked, and write "Try access" in the comments).

If you do not have an @chromium.org email address, or wish to use a different
email address;

*   Ask someone you're working with (a frequent reviewer, for example) to send
    email to accounts@chromium.org nominating you for try job access.
*   You must provide an email address and at least a brief explanation of why
    you'd like access.
*   It is helpful to provide a name and company affiliation (if any) as well.
*   It is very helpful to have already had some patches landed, but is not
    absolutely necessary.

If no one objects within two (U.S.) working days, you will be approved for
access. It may take an additional few days for the grant to propagate to all of
the systems (e.g., Rietveld) and for you to be notified that you're all set.

Googlers can look up the committers list
[here](https://chromium-committers.googleplex.com/).

## Maintaining committer status

You don't really need to do much to maintain committer status: just keep being
awesome and helping the Chromium projects!

A community of committers working together to move the Chromium projects forward
is essential to creating successful projects that are rewarding to work on. If
there are problems or disagreements within the community, they can usually be
solved through open discussion and debate.

In the unhappy event that a committer continues to disregard good citizenship
(or actively disrupts the project), we may need to revoke that person's status.
The process is the same as for nominating a new committer: someone suggests the
revocation with a good reason, two people second the motion, and a vote may be
called if consensus cannot be reached. I hope that's simple enough, and that we
never have to test it in practice.

Also see [Using your @chromium.org email
address](get-bug-editing-privileges.md).

\[Props: Much of this was inspired by/copied from the committer policies of
[WebKit](http://www.google.com/url?q=http%3A%2F%2Fwebkit.org%2Fcoding%2Fcommit-review-policy.html&sa=D&sntz=1&usg=AFrqEze4W4Lvbhue4Bywqgbv-N5J66kQgA)
and
[Mozilla](http://www.google.com/url?q=http%3A%2F%2Fwww.mozilla.org%2Fhacking%2Fcommitter%2F&sa=D&sntz=1&usg=AFrqEzecK7iiXqV30jKibNmmMtzHwtYRTg).\]

## Chromium OS Commit Access (Code-Review +2)

Access to Code-Review +2 bits in Chromium OS repos is gated by being in a
[Chromium OS sheriff
rotation](../developers/tree-sheriffs/new-chromium-os-faq.md). See
[go/ChromeOS-Sheriff-Schedule](https://go/ChromeOS-Sheriff-Schedule) for details
for how to get into the rotation.

Note that any registered user can do Code-Review +1 or Verified +1 or
Commit-Queue +1. Only access to Code-Review +2 is restricted.
