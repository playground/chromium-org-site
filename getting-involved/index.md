# Getting Involved

Here are some ways you can get involved with Chromium:

*   Join some of our [developer discussion
    groups](../developers/discussion-groups/index.md)
*   Visit the [Help
    Center](https://productforums.google.com/forum/#!forum/chrome) and answer
    users' questions
*   Help with [Testing](../for-testers/index.md)
    *   Provide [reduced test cases](../system/errors/NodeNotFound) to help
        improve web compatibility
    *   Get on the [Beta or Dev channel](dev-channel/index.md) or grab the
        [latest trunk build of Chromium](download-chromium.md)
    *   [File bugs](http://code.google.com/p/chromium/issues/entry)
    *   [Triage bugs](bug-triage.md) to make existing bug reports more useful
    *   [Report
        translation](http://code.google.com/p/chromium/issues/entry?template=Translation%20Issue)
        issues
*   Develop Chromium
    *   See the pages for [Developers](../developers/_index.md)
    *   Read the [Life of a Chromium
        Developer](https://docs.google.com/a/google.com/present/view?id=0AetfwCoL2lQAZGQ5bXJ0NDVfMGRtdGQ0OWM2)
        introduction and the
        [Contributing](http://dev.chromium.org/developers/contributing-code)
        page
    *   Adopt a bug that's marked
        [GoodFirstBug](http://code.google.com/p/chromium/issues/list?q=Hotlist:GoodFirstBug&can=2)
    *   Submit [patches](../developers/contributing-code/index.md) (submit
        enough and you can [become a committer](become-a-committer.md)!)
