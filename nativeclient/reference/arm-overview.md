# Overview of Native Client for ARM

This page has moved to the [Native Client ARM 32-bit
Sandbox](http://developer.chrome.com/native-client/reference/sandbox_internals/arm-32-bit-sandbox).
