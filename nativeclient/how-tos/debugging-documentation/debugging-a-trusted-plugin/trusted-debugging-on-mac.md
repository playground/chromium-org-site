# Debugging a Trusted Plugin on Mac

1.  Make an new Xcode project, use the GUI to make a Bundle project that uses
    Core Foundation.
2.  Add existing NaCl sources
3.  DEPS in ppapi from chromium, add the C++ sources - create a Group and add
    the ppapi files directly. Adding ppapi as a folder reference doesn't work.
4.  Set "Header search paths" to point to the chromium ppapi headers
    (|SDK_root|/third_party), NOT the built-in NaCl headers.
5.  Add the SDK root to "Header Search Paths"
6.  Build the plugin.
7.  Edit hello_world/hello_world.html so that the embed tag has
    type="application/x-hello-world" and no nacl= attribute.
8.  Instead of handling a onload event in the EMBED tag, you have to call
    moduleDidLoad() directly after the EMBED tag.
9.  Make sure to uncheck "Load Symbols Lazily" in the Debugging panel of Xcode
    preferences.
10. To debug, you have to use Chromium - best is to get a waterfall build from
    http://build.chromium.org/f/chromium/snapshots/Mac/. This style of debugging
    is not supported with Google Chrome Dev Channel
    1.  In Xcode, ctrl-click on "Executables" and select "Add Custom
        Executable…".
    2.  Call the new custom exec, say, "Chromium Dev"
    3.  Point it at the .app wrapper for Chromium that you got from the
        waterfall, e.g. ~/Downloads/chrome-mac/Chromium.app.
    4.  Add these arguments in the Arguments tab:
        1.  --user-data-dir=/tmp/nacl-debug-profile
        2.  --register-pepper-plugins="$HOME/Source/nacl-sdk/src/examples/hello_world/HelloWorld/build/Debug/HelloWorld.bundle;application/x-hello-world"
        3.  --single-process
        4.  file://$HOME/Source/nacl-sdk/src/examples/hello_world/hello_world.html
11. It is possible to debug a plugin using Chrome Dev channel, but it's a little
    more raw:
    1.  In a shell, run Chrome like this: Google\\
        Chrome.app/Contents/MacOS/Google\\ Chrome
        --user-data-dir=/tmp/nacl-debug-profile
        --register-pepper-plugins="$HOME/Source/nacl-sdk/src/examples/hello_world/HelloWorld/build/Debug/HelloWorld.bundle;application/x-hello-world"
        file://$HOME/Source/nacl-sdk/src/examples/hello_world/hello_world.html
    2.  In Chrome, create a new tab and visit about:memory, this will list the
        PID of the plugin tab.
    3.  In Xcode, Run -> Attach To Process, then pick the appropriate PID.
        1.  Note: if you get various errors about formatting, just click
            "Continue"
