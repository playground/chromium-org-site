# Debugging NaCl apps in Eclipse CDT

Eclipse CDT plugin is designed for development with GNU tools. Its flexibility
allows to configure it for developing NaCl applications without using any
additional plugins. The cost of this flexibility is that a lot of advanced
features are not available. The following instructions are written for Windows
but they can be used on any OS if you replace Windows paths to paths on your OS.

## Creating NaCl project

Create NaCl Project directory inside Eclipse workspace
(C:\\Users\\username\\workspace by default). Copy all files from
nacl_sdk\\pepper_22\\examples\\hello_world except hello_world.c to this
directory. Create src subdirectory and copy hello_world.c file there. Placing
source files in the src directory makes cleaner project structure. Now go to
File->New->Makefile Project with Existing Code menu in Eclipse. Choose project
name, enter project folder location and select Cross GCC toolchain.

![image](eclipse-new-project.png)

Your new project will look like this.

![image](eclipse-project-view.png)

## Building NaCl project

Open Makefile in the editor. Comment out lines
ALL_TARGETS+=win/Debug/hello_world.nmf and
ALL_TARGETS+=win/Release/hello_world.nmf and replace hello_world.c with
src/hello_world.c (Edit->Find/Replace... in the menu or Ctrl+F with default key
bindings). Then right click on the project and choose Preferences in the popup
menu. Go to C/C++ Build->Build Variables page and add NACL_SDK_ROOT variable.
Point the variable to the pepper version you want to build with. If you
downloaded NaCl SDK to c:\\nacl_sdk directory and want to use 22 pepper, set it
to c:\\nacl_sdk\\pepper_22.

![image](eclipse-new-build-variable.png)

![image](eclipse-build-variables.png)

Now we can use this variable to set build command on C/C++ Build page to
${NACL_SDK_ROOT}/tools/make NACL_SDK_ROOT=${NACL_SDK_ROOT}. On Linux and Mac use
make NACL_SDK_ROOT=${NACL_SDK_ROOT}.

![image](eclipse-build.png)

Enable parallel build on Behaviour tab.

![image](eclipse-build-behaviour-tab.png)

Press OK in properties window. You can now build the project by right clicking
on it and selecting Build Project in the popup menu or by pressing hammer icon
![image](eclipse-hammer-icon.png) in the toolbar. Make build log will be shown
in console view.

## Set up eclipse indexer

Eclipse indexer doesn't use external compiler and so it should be configured
separately. Open project properties and go to C/C++ General->Paths and Symbols
page. Open Source Location tab, add src folder using Add Folder... button and
remove root of the project using Delete button.

![image](eclipse-source-location.png)

Then go to Output Location path, add glibc and newlib folders there and remove
root of the project.

![image](eclipse-output-location.png)

Now let us setup include paths. Go to Includes tab and press Add... button.
Enter ${NACL_SDK_ROOT}/include in the directory field and select Add to all
configurations and Add to all languages checkboxes.

![image](eclipse-add-directory-path-1.png)

Then press Add... button again and add
${NACL_SDK_ROOT}/toolchain/win_x86_glibc/x86_64-nacl/include directory (replace
win_x86_glibc with linux_x86_glibc or mac_x86_glibc if you use Linux or Mac OS X
accordingly).

![image](eclipse-add-directory-path-2.png)

Press OK button in properties window and open hello_world.c. There should not be
any compile errors now.

![image](eclipse-with-indexer-enabled.png)

## Debug NaCl newlib application

Launching and debugging applications in Eclipse is done via running and
debugging configurations. Usually they are created automatically. Unfortunately,
NaCl debugging is done using remote debugging protocol. Setting up remote
connection requires parameters that can't be determined automatically. So we
have to create debugging configuration by hand. Go to Run->Debug
Configurations... menu or press on debug icon arrow
![image](eclipse-debug-icon.png) in toolbar and choose Debug Configurations...
in the popup menu. Select C/C++ Remote Application in Debug Configurations
windows and press new button ![image](eclipse-new-debug-configuration-icon.png)
above filter text field or choose New in the popup menu.

Change configuration name to NaCl Project newlib and fill C/C++ Application
field with ${workspace_loc}\\NaCl
Project\\newlib\\Debug\\hello_world_x86_64.nexe. We use ${workspace_loc} instead
of ${project_loc} since the later depends on currently selected project.

![image](eclipse-debug-configuration-main.png)

Now press Select other link at the bottom of the window. If you don't see the
link, skip this step. Select Use configuration specific settings checkbox and
choose GDB (DSF) Manual Remote Debugging Launcher in the list.

![image](eclipse-select-preferred-launcher.png)

Press OK and go to the debugger tab. Change Stop on startup at field to
Instance_DidCreate or deselect the Stop on startup checkbox. On Main tab in
Debugger Options set GDB debugger field to
c:\\nacl_sdk\\pepper_canary\\toolchain\\win_x86_glibc\\bin\\x86_64-nacl-gdb.exe.
Ensure that Non-stop mode checkbox is not selected since nacl-gdb doesn't
support this mode.

![image](eclipse-debug-configuration-Debugger-Main.png)

Set port number to 4014 in Gdbserver Settings tab.

![image](eclipse-debug-configuration-Debugger-Gdbserver-Settings.png)

Save debug configuration by clicking Apply button.

Now you need to launch chrome with --no-sandbox and --enable-nacl-debug flags
and run your NaCl application. NaCl debug stub will stop application at first
instruction. Then you need to open Debug Configuration window, select our debug
configuration and press Debug button. Next time, you can launch it by pressing
debug icon arrow that shows 10 last debug configurations.

![image](eclipse-last-debug-configurations.png)

When program will stop on the start breakpoint or breakpoint you set in project,
Eclipse will switch to debug perspective.

![image](eclipse-hit-breakpoint.png)

## Debugging NaCl glibc application

Create gdb_init.txt file and add line nacl-manifest
"c:\\\\Users\\\\username\\\\workspace\\\\NaCl
Project\\\\glibc\\\\Debug\\\\hello_world.nmf". If you use debugger from
pepper_29+, you don't need to double slashes.

![image](eclipse-gdb_init-file.png)

Go to Run->Debug configuration... menu and duplicate newlib debug configuration
using Duplicate item in popup menu. Change new configuration name to NaCl
Project glibc and C/C++ Application field to ${workspace_loc}\\NaCl
Project\\glibc\\Debug\\lib64\\runnable-ld.so.

![image](eclipse-debug-configuration-glibc-main.png)

Then go to Debugger tab and change GDB command file on Main subtab to
c:\\Users\\username\\workspace\\NaCl Project\\gdb_init.txt. Ensure that Non-stop
mode checkbox is not selected.

![image](eclipse-debug-configuration-glibc-Debugger-Main.png)

Press Apply button and close the window. Now you need to run your NaCl
application in chrome with --enable-nacl-debug and --no-sandbox flags, select
this debug configuration in Debug Configurations window and press Debug button.
You may see some error messages like

    No source file named hello_world.c.
    warning: Could not load shared library symbols for runnable-ld.so.
    Do you need "set solib-search-path" or "set sysroot"?

You should ignore them.

![image](eclipse-glibc-hit-breakpoint.png)

## Troubleshooting

If something goes wrong, you can look on gdb input and output. Press on the
arrow of console icon in Console view to show "NaCl Project glibc \[C/C++ Remote
Application\] gdb traces" console.

![image](eclipse-consoles.png)
