# Servo

## Introduction

Servo is a debug board used for Chromium OS test and development. It connects to
most Chrome devices through a debug header on the mainboard. The debug header is
used primarily during development and is often removed before a device is
released to consumers. If you have a production device the debug header might
need reworking before servo can be connected.

Servo is a key enabler for automated testing, including [automated firmware
testing](../../for-testers/faft/index.md). It provides:

*   Software access to device GPIOs, through hdctools
*   Access to EC and CPU UART ports, for convenient debugging
*   Reflashing of EC and system firmware, for easy recovery of bricked systems

For example, it can act as a USB host to simulate connection and removal of
external USB devices. It also provides JTAG support.

Though servo boards are not publicly distributed or sold (by Google), detailed
information on [Servo V2 block diagram, BOM, schematic and
layout](chromium_os_servo_v2.tar.gz) is available.

## Connecting Servo

In a typical debug setup, you connect servo to the debug header on a Chrome
device, and to a host machine through the HOST_IN USB port. See the annotated
[FAFT setup image](../../for-testers/faft/Servo2_with_labels.jpg) for details.

Most newer Chrome OS mainboards attach to servo with a 50-pin flex cable. The
[schematic and layout for this cable](chromium_os_yoshi_flex.tar.gz) is also
available. The standard DUT-side debug header is an
[AXK750347G](http://www3.panasonic.biz/ac/ae/search_num/index.jsp?c=detail&part_no=AXK750347G)
from Panasonic, though shorter variants are sometimes used.

The basic steps to connect servo are:

1.  Check the direction printed on the flex cable.
2.  Connect the DUT end to the debug header on the Chrome device, metal side
    down.
3.  Connect the Servo end to the header on the servo board, metal side up. Make
    sure to engage the black bottom clip of the header on the servo board by
    pushing it inwards after inserting the ribbon cable. This will hold the
    ribbon cable in place and press the contacts.
4.  Use a USB cable to connect the servo board to your Linux workstation.

You should be able to use the power button on servo to power the Chrome device
on and off.

## Using Servo

To use servo, on your Linux workstation you need to [build Chromium
OS](../developer-guide/index.md) and create a chroot environment.

The hdctools (Chrome OS Hardware Debug & Control Tools) package contains several
tools needed to work with servo. Make sure the latest version is installed in
your chroot:

    sudo emerge hdctools

On your workstation, servod must also be running to communicate with servo:

    sudo servod -b $BOARD &

With servod running, dut-control commands can be used to probe and change
various controls. For a list of commands, run dut-control with no parameters:

    dut-control

You can toggle GPIOs by specifying the control and the state. For example, to
perform a DUT cold reset:

    dut-control cold_reset:on
    sleep 1
    dut-control cold_reset:off

Higher-level controls may set several sub-controls in sequence. For example, to
transition a DUT to recovery mode:

    dut-control power_state:rec

To access the CPU or EC UARTs, first check the port mapping with dut-control,
then attach a terminal emulator program to the port:

    dut-control cpu_uart_pty
    dut-control ec_uart_pty
    sudo minicom -D /dev/pts/$PORT

Servo can also be used for flashing firmware. To flash EC firmware:

    sudo emerge openocd
    /mnt/host/source/src/platform/ec/util/flash_ec --board=$BOARD --image=$IMAGE

The procedure for flashing system firmware may vary slightly by platform. Here
is a typical command sequence for flashing system firmware on Baytrail-based
Chrome devices:

    dut-control spi2_buf_en:on spi2_buf_on_flex_en:on spi2_vref:pp1800 cold_reset:on
    sudo flashrom -V -p ft2232_spi:type=servo-v2 -w $IMAGE
    dut-control spi2_buf_en:off spi2_buf_on_flex_en:off spi2_vref:off cold_reset:off

To set up servo to run automated tests, connect the servo board and the test
device to the network via Ethernet, and load a Chromium OS image onto USB memory
stick. The networking and build image steps are not described here; see
[FAFT](http://www.chromium.org/for-testers/faft) for details on configuring
servo to run automated tests. For information on writing tests, see the [servo
library
code](https://chromium.googlesource.com/chromiumos/third_party/autotest/+/master/server/cros/servo/)
in the [Chromium OS autotest
repo](https://chromium.googlesource.com/chromiumos%2Fthird_party%2Fautotest).

### Single-Step Debugging with Servo, GDB, and OpenOCD

Using the Servo along with GDB and OpenOCD, you can single-step debug via JTAG
on TI EC targets. For example, here's how you would get setup for samus.

1. Start up servod for the target which you will attach to and enable JTAG
access.

    sudo servod -b samus &
    dut-control jtag_buf_on_flex_en:on jtag_buf_en:on

2. Now that servod is up and running, we need to launch OpenOCD. There are some
configuration files present and we need to instruct OpenOCD to use the
configuration file for the Servo V2. The arguments to openocd here are for
samus.

    cd src/platform/ec/util/openocd
    sudo openocd -f servo.cfg -f lm4_chip.cfg

Successful Output

    Open On-Chip Debugger 0.8.0 (2015-03-11-15:39)
    Licensed under GNU GPL v2
    For bug reports, read
    	http://openocd.sourceforge.net/doc/doxygen/bugs.html
    Info : only one transport option; autoselect 'jtag'
    adapter speed: 500 kHz
    flash_emerged_link
    Info : clock speed 500 kHz
    Info : JTAG tap: lm3s.cpu tap/device found: 0x4ba00477 (mfg: 0x23b, part: 0xba00, ver: 0x4)
    Info : lm3s.cpu: hardware has 6 breakpoints, 4 watchpoints

Once you see a line indicating the number of breakpoints, you should be ready to
attach with gdb. When OpenOCD launches, it starts up a gdbserver listening on
port 3333 by default. This can be overridden by setting the `gdb_port` option in
the configuration file. For more information, consult the [OpenOCD User's
guide](http://openocd.org/doc/html/GDB-and-OpenOCD.html).

3. Now that OpenOCD is up and running, we can launch GDB and load the symbols
from our EC ELF file. Make sure to use the correct gdb binary from the
toolchain.

    arm-none-eabi-gdb -ex “target extended-remote localhost:3333” -s <path to>/build/samus/ec.RO.elf

4. Once GDB launches and the connection has been established correctly, set the
number of breakpoints according to the output received earlier from launching
OpenOCD.

    set remote hardware-breakpoint-limit 6
    set remote hardware-watchpoint-limit 4

At this point you can now halt or reset the target, set breakpoints, step
through functions, and even single step through assembly instructions.

You send commands to the target using the "`monitor`" command. Here's how you
would halt, reset, or resume the target.

    monitor halt
    monitor reset
    monitor resume

#### Useful Commands

Within GDB, here are some commands that you may find useful. For more
information, consult the [GDB
documentation](http://www.gnu.org/software/gdb/documentation/).

Set the layout to split mode which shows you the disassembly, source, and the
gdb command window. (You can use ‘`C-x o`’ to switch between the panes and
‘`layout next`’ to cycle the layouts)

    layout split

Set breakpoints on functions or addresses (Tab complete works here for loaded
symbols)

    break gesture_chipset_suspend
    break *0x1234

Print breakpoints

    info break

Dump registers

    info reg
    info reg pc

Print variables

    p var

Step

    s

Step exactly 1 assembly instruction

    si

Run until the current stack frame returns

    finish

## Board Images

![image](servo_v2_top.jpg) ![image](servo_v2_bot.jpg)

[Servo V2 Board Schematic](810-10010-03_20120227_servo_SCH_0.pdf)

[Servo V2 Block Diagram + BOM + Schematic + Layout](chromium_os_servo_v2.tar.gz)

## PD Reworks

Servo v2 reworks are needed to flash the PD MCU on certain boards and access the
PD MCU console. These reworks are not mutually compatible, so only apply the one
relevant to your board.

samus_pd:

![image](image00.jpg)

glados_pd / kunimitsu_pd / chell_pd:

![image](IMG_20151019_085815%20%281%29%20%281%29.jpg)
