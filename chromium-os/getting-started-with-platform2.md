# Getting started with Platform2

### What is platform2?

platform2 is an effort to make the build of platform packages faster/simpler,
we're transitioning to using GYP+Ninja to build everything.

The common build system includes:

*   protobuf and dbus adaptors generation
*   incremental builds
*   pkg-config support
*   dependencies extraction (dependencies are extracted from your gyp files and
    be used to generate pkg-config files.

The platform2 philosophy for a package `${package_name}`:

*   The code should live in `src/platform2/${package_name}`
*   The code should be build from a single gyp file`
    src/platform2/${package_name}/${package_name}.gyp`
*   Each package should have their own ebuild. The logic to build platform2
    packages is contained in platform.eclass.

### Writing an ebuild for a platform package

Packages in platform2 should have very lean ebuilds. Most of the build
configuration should happen behind the scene (platform.eclass) or in the common
gyp logic.

In most cases, your ebuild will only contain dependencies, an install section
and a list of tests to run.

Note: as for all cros_workon packages, you only need to create the -9999 version
of the ebuild (`${package_name}-9999.ebuild`). The stable version of the ebuild
(`${package_name}-x.y.z-r${revision_number}.ebuild`) will be created by the CQ
automatically.

Example ebuild:

    # Copyright 2017 The Chromium OS Authors. All rights reserved.
    # Distributed under the terms of the GNU General Public License v2
    EAPI=5
    CROS_WORKON_INCREMENTAL_BUILD=1
    CROS_WORKON_LOCALNAME="platform2"
    CROS_WORKON_PROJECT="chromiumos/platform2"
    CROS_WORKON_DESTDIR="${S}/platform2"
    PLATFORM_SUBDIR="package" **# name of the directory in src/platform2**
    inherit cros-workon platform
    DESCRIPTION="description"
    HOMEPAGE="http://www.chromium.org/"
    LICENSE="BSD-Google"
    SLOT=0
    KEYWORDS="~*"
    RDEPEND="
    	chromeos-base/libbrillo
    "
    DEPEND="
    	${RDEPEND}
    "
    src_install() {
    	dobin "${OUT}/binary" # ${OUT} is the build output directory containing
                                  # all your binaries
    }
    platform_pkg_test() {
    	local tests=(
    		package_test **# name of tests defined in gyp**
    	)
    	local test_bin
    	for test_bin in "${tests[@]}"; do
    		platform_test "run" "${OUT}/${test_bin}" # platform_test takes care
                    # of setting up your test environment
    	done
    }

### Adding generators

Platform2 already has pre-made generators for dbus-xml2cpp and protoc, but if
you need to add a new generator, here's how.

1.  Create a new .gypi file `src/platform2/common-mk` named appropriately to
    describe your generator
2.  Copy the contents of an existing one, such as protoc.gypi
3.  Modify the rule specified to call your tool with the arguments
4.  Update the inputs/outputs lists to reflect the files your tool will generate
    (make sure your tool is in virtual/target-sdk if necessary

You can now include this generator in a target for a project you're porting to
Platform2, by writing a target in your project that looks something like the
following:

`{`
` 'target_name': 'libdisks-adaptors',`
` 'type': 'none',`
` 'variables': {`
` 'xml2cpp_type': 'adaptor',`
` 'xml2cpp_out_dir': 'include/cros-disks',`
` },`
` 'sources': [`
` 'cros-disks-server.xml',`
` ],`
` 'includes': ['../common-mk/xml2cpp.gypi'],`
` },`

Note: if your generator generates source files, you likely want to make the
target type "static_library" and not "none", otherwise you're likely to see link
errors.

### Adding a new USE flag conditional

It is possible to have conditional gyp rules that depends on the value of a USE
flag at the time of build. This is useful, for example, if certain targets must
only be built if a certain feature (controlled by a USE flag) is enabled. This
can be done as follows:

1.  Make sure the USE flag is listed in the $`{package_name}``-9999.ebuild` in
    `IUSE`.
2.  Add the USE flag to `src/platform2/common-mk/common.gypi`
    Example: say, you want to depend on pam USE flag. You'd add the following
    line to common.gypi's variables:

        'USE_pam': 0

3.  USE_flagname may now be used in the package's gyp file.
    Example:

         'conditions': [
           ['USE_pam == 1', {
             'sources': [
               'pam_helper_mock.cc'
              ],
            }],

### Rapid development

`cros_workon_make` will use the ebuild to build a package, but will build
incrementally and skip as many step as possible from the normal emerge process
(not checking dependencies, not installing, etc..).

For extra speed, you can also use --noreconf which skip the configure stage
(creating the ninja file from the gyp files).

    cros_workon_make --board=${BOARD} --noreconf shill

You can inspect the produced artifacts and test binaries in`
/build/${BOARD}/var/cache/portage/chromeos-base/shill`.

#### Running the unittests in gdb

Once your unittests are built, you need to enter the sysroot (in
`/build/${BOARD}`) with `platform2_test.py`. `platform2_test.py` will setup the
environment so that the binaries can be executed correctly (entering the
sysroot, using qemu, etc..).

**NOTE**: qemu doesn't support the ptrace syscall used by gdb, so even though
the program will run under the debugger, breakpoints/traps/etc won't work.

To run a shell within the sysroot:

    common-mk/platform2_test.py --board=${BOARD} /bin/bash

From within the shell, you can run your binaries including unit tests, and run
them with gdb. Within the sysroot, the gdb accessible from `$PATH` is the
correct target gdb. Since you have entered the sysroot, you will find the build
artifacts under `/var/cache/portage/chromeos-base/${package_name}`.

### Running unit tests

To run unit tests for package using platform2, simply run cros_workon_make
--board=${BOARD} ${PACKAGE_NAME} --test.

Another option is to run FEATURES=test emerge-${BOARD} ${PACKAGE_NAME}, which
might give you more messages.

You can select a subset of tests to run by setting P2_TEST_FILTER.
cros_workon_make will run only the tests whose name match one of the regex
specified in `P2_TEST_FILTER`.
`P2_TEST_FILTER="StringUtils``.*``" `cros_workon_make --test` libchromeos `for
the string utilities test for libchromeos

Upstream documentation for gtest can be found here:
<https://code.google.com/p/googletest/wiki/AdvancedGuide#Running_a_Subset_of_the_Tests>

## Questions?

If you want to get started porting your project across to Platform2 but feel
this page is missing something you need, please feel free to drop me an e-mail
(bsimonnet@) and I'll update this page.

## Further reading

*   <https://code.google.com/p/gyp/wiki/GypUserDocumentation>
*   <https://code.google.com/p/gyp/wiki/GypLanguageSpecification>
*   <https://code.google.com/p/gyp/wiki/InputFormatReference>
