# Access Points

**UI under development. Designs are subject to change.**

The apps menu is the primary access point in Chromium OS. It is available via an
icon in the upper left, and provides access to universal search, document and
tab creation, bookmarks, and other OS tools. Its primary functions may include:

*   Search Box
*   Tab creation
*   Document Templates
*   New Tab / Window
*   Bookmarks
*   History
*   Downloads
*   Options
*   Notifications
*   Sign out / Shut down

We are exploring a variety of different form factors for this access point:
standard pull down menus, a full-screen tab, and partial-screen overlays.
