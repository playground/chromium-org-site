# Tablet

**This is a concept UI under development. Designs are subject to change.**

This page contains visual explorations of how a Chrome OS tablet UI might look
in hardware. Some possibilites they explore include:

![image](Tablet.png)

*   Keyboard interaction with the screen: anchored, split, attached to focus.
*   Launchers as an overlay, providing touch or search as means to access web
    sites.
*   Contextual actions triggered via dwell.
*   Zooming UI for multiple tabs
*   Tabs presented along the side of the screen (see [Side
    tabs](http://www.chromium.org/chromium-os/user-experience/window-ui))
*   Creating multiple browsers on screen using a launcher

## UI Concepts

![image](tablet2.100.png)

![image](tablet2.105.png)

![image](tablet2.106.png)

![image](tablet2.107.png)

![image](tablet2.108.png)

![image](tablet2.141.png)

![image](tablet2.150.png)

Video Concepts

![image](tablet_concept.mp4)

Please see attached video at the bottom of this page.
