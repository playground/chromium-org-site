# Open/Save Dialogs

**UI under development. Designs are subject to change.**

**Application type:** Sheet/Dialog

**Description: **Allows a location to be specified for a download action. This
defaults to the shelf, but a modifier or contextual menu can be used to choose
an alternate location.

Functionality:

*   Shows the same sources as the [content browser](../content-browser/index.md)
*   Surfaces [shelf](../shelf/index.md) as a source
*   Could be floating, or attached to tab bar

### Open

### Save

*   Adds a text field for name
*   Shows the to-be-created file
*   We should start downloading to get the preferred name headers and resolve
    all redirects

![image](Save.png)
