# System Status Icons

**UI under development. Designs are subject to change.**

![image](Screen-shot-2009-12-01-at-12.23.56-PM.PNG.png)

**Power**

Displays battery/charging status

** Network**

Displays active network interface/status. Combines wireless/wired/cellular
connections. Provides access to network [settings](../settings/index.md)

![image](NetworkMenu.png)

**Time**

Displays the time in 12/24 hour formats. May provide access to a clock app.

**Input Method**

See [Design Doc](../../chromiumos-design-docs/text-input/index.md)

**Audio Muted**

TBD

**Alarm Enabled**

TBD
