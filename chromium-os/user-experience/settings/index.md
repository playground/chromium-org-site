# Settings

**UI under development. Designs are subject to change.**

Chrome OS settings extend the standard chrome settings with new system and
network tabs.

### System

Provides access to most general settings from input devices to displays and
sound.

![image](1-General.png)

### Network

Network settings has a basic top level ui for the interface types, with options
dialogs that provide deeper customization

![image](2-Internet.png)

### Browser

As in Chrome

![image](3-Browser.png)

### Personal Stuff

As in Chrome

![image](4-PersonalStuff.png)

### Under the Hood

As in Chrome

![image](5-UnderTheHood.png)
