# Shelf

**UI under development. Designs are subject to change.**

**Application type:** Panel

**Description:**

The shelf is an on demand target for downloading and uploading. It acts as a
temporary resting place for files that have been accessed, and for content being
uploaded to websites. It uses the same display format as the [content
browser](../content-browser/index.md) and has similar actions available.

Functionality:

*   Surfaces as a source in [open/save dialog
    boxes](../opensave-dialogs/index.md)
*   Surfaces as a source in [content browser](../content-browser/index.md)
*   Appears when content is dragged into the panel bar
*   Allows simple actions to be initiated from each item
*   Replaces the standard download shelf in Chrome.
*   Back button goes up to show other sources

Future goals

*   Sync to cloud
