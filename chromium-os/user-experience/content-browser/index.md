# Content Browser

**UI under development. Designs are subject to change.**

**** Application type:** Panel/Tab**

**Description:**

The content browser is a simple hierarchical file browser. Specifically, it is
an enhanced version of file:// rendering that allows easier navigation.

Functionality:

*   Read only access
*   Attach to email
*   Pop-out for media content
*   File actions menu

Future goals

*   Offline access to sync'd content
*   Browsing external file:// and smb:// resources
*   Read/Write access
*   Integration with open/save dialog

Related: [Shelf](../shelf/index.md) | [Open/Save](../opensave-dialogs/index.md)

Panel Style (with menu)

The pop out menu allows a variety of file manipulation and upload actions.

![image](ContentBrowserPanel2.png)

**Tab Style (for future versions)**

The full tab view of the content browser will show the files in a columnar
hierarchy. These columns can be popped out into panels.

![image](Full.png)
