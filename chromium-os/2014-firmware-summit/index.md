# 2014 Firmware Summit

February 20, 2014

firmware@chromium.org

## Chrome OS Firmware Overview

#### Duncan Laurie

## [Presentation](https://docs.google.com/presentation/d/1h-nsDGlQmYI21dr95nYgLmyCYDgBIpJWSt9b7AqTZaw/pub?start=false&loop=false&delayms=3000)

## [Video](http://youtu.be/HwsTRThChn0)

This talk presents an overview of the Chrome OS firmware stack including
components and organization and the Verified Boot feature.

The second half provides an introduction to coreboot and a summary of some
important features.

## Porting coreboot to x86 platforms

#### Aaron Durbin

## [Presentation](https://docs.google.com/presentation/d/1hwkzVrbAWUFqdEeWPaNSTp0MKpEGxY1gt90UmG7bBFg/pub?start=false&loop=false&delayms=3000)

## [Video](http://youtu.be/HKJNJh6JW_Y)

This talk goes into detail about porting coreboot to new x86 platforms.

## Porting coreboot to ARM platforms

#### Stefan Reinauer

## [Presentation](https://docs.google.com/presentation/d/1jWOVuJ59LqvoweS9xbKY4RJw9pVDQNVk8aMQKtwHAqk/pub?start=false&loop=false&delayms=3000)

## [Video](http://youtu.be/d0d9gxmg0xQ)

This talk goes into detail about porting coreboot to new ARM platforms.

## Porting coreboot to a new Mainboard

## Shawn Nematbakhsh

## [Presentation](https://docs.google.com/presentation/d/1ogewSJS8kOhqVEZgH1AlY3e5XD66jXdeoWoPjjLIKb0/pub?start=false&loop=false&delayms=3000)

## [Video](http://youtu.be/8wPskpLHFIo)

This talk goes into detail about porting coreboot to a new Mainboard for an
existing architecture.

## Depthcharge: The Chrome OS Bootloader

## Gabe Black

## [Presentation](https://docs.google.com/presentation/d/1pH8ltQ3cGKy9dRaTxHtZbA50QLZCw6HE8LDoi1y_gcs/pub?start=false&loop=false&delayms=3000)

## [Video](http://youtu.be/6ZKeDGI75vw)

This talk describes the Depthcharge project which is a custom coreboot payload
integrated with Verified Boot that is intended to boot Chrome OS.

## Chrome OS Embedded Controller

## Bill Richardson

## [Presentation](https://docs.google.com/presentation/d/1Xa_Z5SjW-soPvkugAR8__TEJFrJpzoZUa9HNR14Sjs8/pub?start=false&loop=false&delayms=3000)

## [Video](http://youtu.be/Ie7LRGgCXC8)

This talk provides information on the Chrome OS Embedded Controller, including
source organization and porting.

## Chrome OS Process Part 1

## Bernie Thompson

## [Presentation](https://docs.google.com/presentation/d/1hqo_LT4jAjlckZPxADFYI_rpCr9IRlydnbRVjyTTCiM/pub?start=false&loop=false&delayms=3000)

## [Video](http://youtu.be/RWxyMdVrGiI)

This talk introduces several Chrome OS Firmware process details, including the
use of repo and Git, basics of Portage and ebuilds, firmware branches, keys and
signing, and the Gerrit code review process.

## Chrome OS Process Part 2

## Dave Parker

## [Presentation](https://docs.google.com/a/chromium.org/presentation/d/1hqo_LT4jAjlckZPxADFYI_rpCr9IRlydnbRVjyTTCiM/pub?start=false&loop=false&delayms=3000#slide=id.g2bc8270bb_55)

## [Video](http://youtu.be/jqtoZc3X3lE)

This talk goes into detail about Chrome OS Firmware process, including the
Chrome Partner Front End, working with git, a detailed example of the firmware
build process, and FAFT.
