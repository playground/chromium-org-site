# Simple developer workflow

## Making changes to packages whose source code is checked into Chromium OS git repositories

## The steps in this section describe how to make changes to a Chromium OS
   package whose source is checked into the Chromium OS source control system.
   Specifically, this is a package where:

## *   The `**ebuild**` for the package lives in the
       `src/third_party/chromiumos-overlay` or `src/overlays/overlay-${BOARD}`
       directories.
## *   There is an ebuild for the package that ends with `9999.ebuild`.
## *   The ebuild inherits from the `cros-workon` class.
## *   The ebuild has a `KEYWORDS` in the ebuild containing this architecture
       name (like "`x86`").

## You can see a list of all such packages by running the following command from
   inside the ~/trunk/src/scripts directory:

##     cros_workon --board=${BOARD} list --all 

### Run cros_workon start

## The first thing you need to do is to mark the package as active. Use the
   command below, replacing ${PACKAGE_NAME} with your package name (e.g.,
   `chromeos-wm`):

##     cros_workon --board=${BOARD} start ${PACKAGE_NAME}

## This command:

## *   Indicates that you'd like to build the `9999` version of the `ebuild`
       instead of the stable, committed version.
## *   Indicates that you'd like to build from source every time.
## *   If you specified that you wanted the `minilayout` when you did your `repo
       init`, this command adds a clause to your `.repo/local_manifest.xml` to
       tell `repo` to sync down the source code for this package next time you
       do a `repo sync`.

### Run repo sync

## After running cros_workon, sync down the sources. This is critical if you're
   using the `minilayout`, but is probably a good idea in any case to make sure
   that you're working with the latest code (it'll help avoid merge conflicts
   later). Run the command below from outside the chroot, anywhere under your
   `~/chromiumos` directory:

##     repo sync

### Find out which ebuilds map to which directories

## The cros_workon tool can help you find out what ebuilds map to each
   directory. You can view a full list of ebuilds and directories using the
   following command:

##     cros_workon info --board=${BOARD} --all

## If you want to find out which ebuilds use source code from a specific
   directory, you can use grep to find them. For example:

##     cros_workon info --board=${BOARD} --all | grep platform/ec

## This returns the following output:

## chromeos-base/ec-utils chromiumos/platform/ec src/platform/ec

## This tells you the following information:

## 1.  The name of the ebuild is chromeos-base/ec-utils
## 2.  The path to the git repository on the server is chromiumos/platform/ec
## 3.  The path to the source code on your system is src/platform/ec

## You can similarly find what source code is associated with a given ebuild by
   grepping for the ebuild name in the list.

## To find out where the ebuild lives:

##     equery-${BOARD} which ${PACKAGE_NAME}

## As an example, for PACKAGE_NAME=ec-utils, the above command might display:
## `/home/.../trunk/src/third_party/chromiumos-overlay/chromeos-base/ec-utils/ec-utils-9999.ebuild`

## **SIDE NOTE:** If you run the same command **without** running cros_workon
   first, you can see the difference:

## `/home/.../trunk/src/third_party/chromiumos-overlay/chromeos-base/ec-utils/ec-utils-0.0.1-r134.ebuild`

### Create a branch for your changes

## Since Chromium OS uses `repo`/`git`, you should always create a local branch
   whenever you make changes.
## First, find the source directory for the project you just used cros_workon
   on. This isn't directly related to the project name you used with
   cros_workon. (This isn't very helpful - someone with more experience,
   actually tell us how to find it reliably? --Meredydd)

## cd into that directory, in particular the "files/" directory in which the
   actual source resides. In the command below, replace ${BRANCH_NAME} with a
   name that is meaningful to you and that describes your changes (nobody else
   will see this name):

##     repo start ${BRANCH_NAME} .

## The branch that this creates will be based on the remote branch (which one?
   --Meredydd). If you've made any other local changes, they will not be present
   in this branch.

### Make your changes

## You should be able to make your changes to the source code now. To
   incrementally compile your changes, use

##     cros_workon_make --board=${BOARD} ${PACKAGE_NAME}

## This will build your package inside your source directory. Change a single
   file, and it will rebuild only that file and re-link. If your package
   contains test binaries, using

##     cros_workon_make --board=${BOARD} ${PACKAGE_NAME} --test

## will build and run those binaries as well. Call `cros_workon_make --help` to
   see other options that are supported.

## You probably want to get your changes onto your device now. You need to
   install the changes you made by using

##     cros_workon_make --board=${BOARD} ${PACKAGE_NAME} --install

## `You can then rebuild an image with `build_image` and reimage your device.
   ...but you might also want to take a peek at the [devserver
   instructions](http://www.chromium.org/chromium-os/how-tos-and-troubleshooting/using-the-dev-server)
   if you want something a little quicker.`

### Submit changes locally

## When your changes look good, commit them to your local branch using `git`.
   Full documentation of how to use git is beyond the scope of this guide, but
   you might be able to commit your changes by running something like the
   command below from the project directory:

##     git commit -a

## The git commit command brings up a text editor. You should describe your
   changes, save, and exit the editor. Note that the description you provide is
   only for your own use. When you upload your changes for code review, the repo
   upload command grabs all of your previous descriptions, and gives you a
   chance to edit them.

### Upload your changes and get a code review

## Once your changes are committed locally, upload your changes using repo
   upload. The repo upload command takes all of the changes that are unmerged
   and asks them if you want to upload them. You can specifically say to only
   look for unmerged changes in your current repo by passing in '.'. Please note
   that you must have a [Gerrit
   account](http://www.chromium.org/chromium-os/developer-guide/gerrit-guide)
   before you can upload changes.

##     repo upload [.|project-name] [--current-branch]

## It is important to note that repo uses the Change-Id in your git commits to
   track code reviews. So in order to work on a CL the standard work flow is to
   use `git commit --amend` rather than make a new commit. This differs from our
   old `git-cl` style.

## In your local commit logs make sure to add a BUG= field and TEST= field. For
   BUG=, you should put something that looks like: `BUG=bug-tracker:number`. You
   can get a tracker name by looking in the upper-left corner of the tracker
   page (e.g. in the image on the right, the tracker name is "chromium-os"). If
   your changes are related to more than one tracker issue, you can list all the
   issues separated with commas.

## For TEST=, you should describe what you did to test the changes.

## Here's what a sample description should look like:

##     # Enter a description of the change.
##     # This will displayed on the codereview site.
##     # The first line will also be used as the subject of the review.
##     Here's a SHORT, one-line summary of my change.
##     And here are more details
##     ...this can be as long as I want.
##     BUG=chromium-os:99999, chromium:88888
##     TEST=Ran all the white box tests
##     Change-Id: I8d7f86d716f1da76f4c85259f401c3ccc9a031ff

## Once you run `repo upload`, this uploads the changes and prints out a URL for
   the code review (if its a new code review). Go to that URL (log in with your
   chromium.org account, which you might want to do in an "incognito" window in
   Chrome), and use the "**Review->Publish Comments**" link to mail your changes
   to your reviewers.

## You should pick reviewers that know the code you're working on well and that
   will do the best reviews. Picking reviewers who will just rubber-stamp your
   changes is a bad idea. The point of submitting changes is to submit good
   code, not to submit as much code as you can. If you don't know who should
   review your changes, start by looking at the `git log` for the project that
   you're working on. Simply type the command below in a directory related to
   your project:

##     git log

## Your reviewers will likely provide comments about changes that you should
   make before submitting your code. You should make such changes, submit them
   locally, and then re-upload your changes for code review by amending your
   changes to your git commit and re-running repo upload.

##     # make some changes
##     git add -u .
##     git commit --amend

### Try seeing the mainline again

## While you're working on your changes, you might want to go back to the
   mainline for a little while (maybe you want to see if some bug you are seeing
   is related to your changes, or if the bug was always there). If you want to
   go back to the mainline without abandoning your changes, you can run the
   following command from within a directory associated with your project:

##     git checkout cros/master

## When you're done, you can get back to your changes by running:

##     git checkout ${BRANCH_NAME} 

### Work on something else while you're waiting

## If you want to start on another (unrelated) change while waiting for your
   code review, you can `repo start` another branch. When you want to get back
   to your first branch, run the following command from within a directory
   associated with your project:

##     git checkout ${BRANCH_NAME} 

### Send your changes to the Commit Queue

## Eventually, all your reviewers will be happy and will give you a Looks Good
   and Approved (the latter is only available to owners of the code) message in
   Gerrit. If they choose to test it they can also mark Verify. Both Looks Good
   and Approved + Verified must be set in order to commit your CL. Once they are
   set, set the Commit Ready bit on your CL, and the commit queue will test the
   change.

## Note it is possible that your change will be rejected because of a merge
   conflict. If it is, rebase against any new changes and re-upload your patch.
   This patch will have to be re-approved before it is allowed to be committed.
   (Note: If it's just a trivial rebase, you can approve the rebase yourself.)

### Make sure your changes didn't break things

## After you commit, make sure you didn't break the build by checking the
   [buildbot](http://build.chromium.org/buildbot/chromiumos).

### Clean up after you're done with your changes

## After you're done with your changes, you're ready to clean up. The most
   important thing to do is to tell `cros_workon` that you're done by running
   the following command:

##     cros_workon --board=${BOARD} stop ${PACKAGE_NAME}

## This command tells cros_workon to stop forcing the `-9999.ebuild` and to stop
   forcing a build from source every time.

## You can also delete the branch that repo created. There are a number of ways
   to do so; here is one way:

##     repo abandon ${BRANCH_NAME} ${CROS_WORKON_PROJECT}

## **SIDE NOTES:**

## *   If you don't specify a project name, the `repo abandon` command will
       throw out any local changes across **all projects**. You might also want
       to look at `git branch -D` or `repo prune`.
## *   If you're using the `minilayout`, doing a cros_workon stop **will not**
       remove your source code. The code will continue to stay on your hard disk
       and get synced down.
