# Gerrit credentials setup (for Chromium OS and Chrome OS)

## Introduction

*If you want gerrit access for non-Chromium OS/Chrome OS projects, please use
the old instructions [here](legacy.md)***. **

We have two gerrit instances: Chromium OS and the (internal) Chrome OS gerrit
instance.

For the Chromium OS instance: This is where most of the development happens. You
do not have to be an "official" Chromium contributor or Googler or anything else
to interact with the Chromium OS gerrit instance and/or upload changes to it. We
restrict access by only allowing certain people to *approve* changes before
they're allowed to go into the main tree.

For the internal Chrome OS instance: Access is restricted to Googlers.

The gerrit instance for Chromium OS and Chrome OS uses Google Accounts to
provide authentication. This means any account you can use to log into
google.com can also be used to authenticate with Gerrit.

#### **(EVERYONE) To get access to the Chromium OS gerrit instance:**

1.  Go to <https://chromium-review.googlesource.com/new-password>
2.  Log in with the email you use for your git commits.
    1.  **If you are a Googler, use your @chromium.org account.**
    2.  **You can verify this by ensuring that the Username field looks like
        git-<user>.chromium.org**
3.  Follow the directions on the new-password page to set up/append to your
    .gitcookies file.
    1.  You should click the radio button labeled "only
        chromium.googlesource.com" if it exists.
4.  **Verification: **Run `git ls-remote
    https://chromium.googlesource.com/a/chromiumos/manifest.git`
    1.  This should **not **prompt for any credentials, and should just print
        out a list of git references.
5.  Make sure to set your real name.
    1.  Visit <https://chromium-review.googlesource.com/#/settings/> and check
        the "Full Name" field.
    2.  If it isn't set, you will need to update your [Google+
        profile](https://plus.google.com).
    3.  Once your Google+ profile is up-to-date, you might have to logout/login
        in Gerrit.

#### **(Googler) To get access to the internal Chrome OS gerrit instance:**

1.  You must also do the steps above for your @chromium account first
2.  Go to <http://google.com/> and verify you are logged into your @google.com
    account
3.  In the same window and session, load
    <https://chrome-internal-review.googlesource.com/new-password>
    1.  **Make sure you are logged into your @google.com account. **
    2.  **You can verify this by ensuring that the Username field looks like
        git-<user>.google.com**
4.  Follow the directions on the new-password page to append to your .gitcookies
    file. You should click the radio button labeled "only
    chrome-internal.googlesource.com" if it exists.
5.  **Verification: **Run `git ls-remote
    https://chrome-internal.googlesource.com/a/chromeos/manifest-internal.git`
    1.  This should **not **prompt for any credentials, and should just print
        out a list of git references.
6.  Make sure to join the
    [chromeos-team](https://groups.google.com/a/google.com/d/forum/chromeos-team)
    mailing list. This enables access to things like trybots.

For more non-chromium-specific info on using repo to interact with gerrit, read
Android's [Git Workflow](http://source.android.com/source/version-control.html)
using repo/gerrit and [life of a
patch](http://source.android.com/source/life-of-a-patch.html).

#### (Googler) Link @chromium.org & @google.com accounts

We use memberships of @google.com identities in particular groups to control
access to Chrome OS repositories on chromium-review and chrome-internal-review
sites. Since you will be primarily using @chromium.org account, you need to tell
Gerrit that your @chromium.org and @google.com accounts are linked together.
Follow the steps:

1.  Login into <https://chromium-review.googlesource.com> using your
    @chromium.org account.
2.  **If you are using the new UI** (should be the default for new accounts):
    1.  Click on the circle with your profile picture on the top-right of the
        page, then choose [Settings -> Email
        Addresses](https://chromium-review.googlesource.com/settings#EmailAddresses).
    2.  Enter your @google.com email address in the *New email address* field
        and click the *Send Verification* button. In your google.com mail, click
        the confirmation link.
    3.  Open [Settings -> Email
        Addresses](https://chromium-review.googlesource.com/settings#EmailAddresses)
        again and confirm that you have @google.com and @googlers.com addresses
        listed.
3.  **If you are using the old UI**:
    1.  Go to [Settings -> Contact
        Information](https://chromium-review.googlesource.com/#/settings/contact).
    2.  Click "Register new email...", enter your @google.com account and follow
        the instructions.
    3.  To verify that it worked, open [Settings ->
        Identities](https://chromium-review.googlesource.com/#/settings/web-identities)
        and verify your @chromium.org, @google.com and ldapuser/\* identities
        are listed.
4.  Repeat the previous steps on
    <https://chrome-internal-review.googlesource.com>, but use your @google.com
    email to login, and @chromium.org in "Register new email" dialog.
5.  If you see any errors during this process, file [Infra-Git
    ticket](https://code.google.com/p/chromium/issues/entry?template=Infra-Git)
    with the subject "Link my <id>@chromium.org and <id>@google.com accounts".
    If it is urgent, add jparent@chromium.org to CC on the ticket. Otherwise,
    the request should be handled within 2-3 days.

Once your accounts are linked, you'll be able to use both @chromium.org and
@google.com emails in git commits. It is particularly useful if you have your
@chromium.org email in global git config, and you try to trigger chrome-internal
trybots (that otherwise require @google.com email).

## **Uploading your changes for review**

You can use "`repo upload . --cbr`" (instead of "`git cl upload`"). Use "`repo
help`" for options on adding reviewers and CC'ing developers from the command
line. You can do this from the Gerrit web interface too once you upload the
change.

More documentation on [Uploading changes for
review](https://gerrit-review.googlesource.com/Documentation/user-upload.html).

## Watching Projects

You can select Projects (and branches) you want to "watch" for any changes on by
adding the Project under Account Settings --> Watched Projects

Please feel free to add/modify any other Gerrit related information you think
may be useful for other developers here.

## Not getting email?

In case you think you should be receiving email from Gerrit but don't see it in
your inbox, be sure to check your spam folder. It's possible that your mail
reader is mis-classifying email from Gerrit as spam.

## Still having a problem?

Check out the [Gerrit
Documentation](https://gerrit-review.googlesource.com/Documentation/index.html)
to see if there are hints in there.

If you have any problems please [open a Build Infrastructure
issue](http://code.google.com/p/chromium/issues/entry?template=Build+Infrastructure)
on the **Chromium** issue tracker (the "Build Infrastructure" template should be
automatically selected).
