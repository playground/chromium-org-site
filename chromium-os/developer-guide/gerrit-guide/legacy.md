# Setting up your Account on gerrit.chromium.org

## Initial Account Setup

**If you want Gerrit access for Chromium OS/Chrome OS please use the [updated
instructions](index.md).**

**NOTE: If you are a Googler, use your @chromium.org account for
gerrit.chromium.org and @google.com for gerrit-int.chromium.org**

These directions will work for everyone (unless otherwise noted). You do not
have to be an "official" Chromium contributor or Googler or anything else.
Anyone can create a gerrit account and upload changes to it. We restrict access
by only allowing certain people to *approve* changes before they're allowed to
go into the main tree.

Here is how to get started:

1.  Go to <http://gerrit.chromium.org/>
2.  Click Register in the upper right hand corner
3.  Click "Register with a Google Account"
4.  Select the account you want to use, or log in to a different one
    1.  Googlers: remember to use your @[chromium.org](http://chromium.org)
        account for this step
5.  In the [Settings page](https://gerrit.chromium.org/gerrit/#/settings/), set
    your name and pick a username
    1.  Googlers: make sure your username is the same as your
        @[chromium.org](http://chromium.org) which is the same as your
        @[google.com](http://google.com) which is the same as your LDAP username
6.  Add your SSH public key (usually is ~/.ssh/id_rsa.pub)
    1.  Add all public keys if you don't know which one you are using and don't
        want to muck with .ssh/config
    2.  You can add/remove keys at any point in the future
7.  (optional) Play around with Gerrit and repo in the "playground"

        repo init -u http://git.chromium.org/playground/manifest.git
        repo sync
        cd src/foo
        repo start bug1 .
        <edit/commit some files>
        repo upload . --cbr

P.S.: Click on "Save Changes" and "Select username" button to make sure your
changes are saved.

Following steps are for Google committers only

1.  **Goto <http://google.com/> and verify you are logged into your @google.com
    account**
2.  In the same window and session, load <http://gerrit-int.chromium.org>
3.  Repeat steps 1-6 above from Initial Account Setup.
4.  Register your @chromium.org with gerrit-int
    1.  Click on "Settings->Contact Information->Register New Email"
    2.  Enter your @chromium.org account
    3.  You will receive a verification e-mail -- click the link in it
    4.  Don't forget to set your @google.com address as Preferred E-mail
    5.  You do not want to "Link another identity" under the Identity settings
5.  If you skip these steps, repo init may fail saying "not a Gerrit Project" or
    other weird ssh errors
6.  If you mess things up, you will have to contact people to get things fixed
    -- see the end of this document

## Troubleshooting Gerrit Account issues

1.  In a new incognito window, go to http://gerrit.chromium.org and log in with
    your @chromium.org account
2.  Log Out. Wait 1 minute. Log back in. \[If you got migrated to GA+, this
    should fix your account.\]
3.  Click on "Settings" at the top right corner. Make sure the "Username" is set
    to your LDAP username.
4.  Make sure your "Full Name" is set correctly under "Contact Information"
5.  Make sure your SSH Public keys are correctly added. Add both your id_rsa.pub
    and chromium.pub if you have one.
6.  Close your incognito window
7.  In a new incognito window, go to <http://gerrit-int.chromium.org> and log in
    with your @google.com account
8.  Repeat step 3,4,5,6
9.  In a shell, type "`ssh -p 29418 gerrit.chromium.org gerrit ls-projects`"
        If you get an error, please mention "ls projects fails on gerrit" in your problem description
10. In a shell, type "`ssh -p 29419 gerrit-int.chromium.org gerrit ls-projects`"
11. Your username (\`id\` in your shell) should match your LDAP username and the
    gerrit/gerrit-int username

Open an issue at <http://crbug.com> (Under Build Infrastructure) to
troubleshoot.

More reading on Android's [Git
Workflow](http://source.android.com/source/version-control.html) using
repo/gerrit and a [life of a
patch](http://source.android.com/source/life-of-a-patch.html).

## **Uploading your changes for review**

You can use "repo upload . --cbr" (instead of "git cl upload"). Use "repo help"
for options on adding reviewers and CC'ing developers from the command line. You
can do this from the Gerrit web interface too once you upload the change.

More documentation on [Uploading changes for
review](http://gerrit.chromium.org/gerrit/Documentation/user-upload.html).

## Watching Projects

You can select Projects (and branches) you want to "watch" for any changes on by
adding the Project under Account Settings --> Watched Projects

Please feel free to add/modify any other Gerrit related information you think
may be useful for other developers here.

## Not getting email?

In case you think you should be receiving email from Gerrit but don't see it in
your inbox, be sure to check your spam folder. It's possible that your mail
reader is mis-classifying email from Gerrit as spam.

## Still having a problem? Open a ticket...

Check: <http://gerrit.chromium.org/gerrit/Documentation/user-upload.html>

If you have any problems please [open a Build Infrastructure
issue](http://code.google.com/p/chromium/issues/entry?template=Build+Infrastructure)
on the **Chromium** issue tracker (the "Build Infrastructure" template should be
automatically selected). If you need emergency assistance IM the currently
scheduled trooper listed [here](https://chromium-build.appspot.com).
