# Directory Structure (Chromium OS)

This document describes the typical Chromium OS source tree hierarchy. Most of
the directories below are present when you download the source code. However,
some directories (e.g. build) are created during the build process.

Starting from the trunk, here are the directories you are likely to see:

Directory Description **src/** The main source directory. All files that are
built and end up in the final Chromium OS image are located here. ** build/**
This directory is created by the `build_image` script and contains the build
results. **images/** Contains images built by the `build_image` script. Images
are arranged in subfolders by corresponding board and build number. For a given
board, the latest build image can always be found by looking at the
`src/build/images/${BOARD}/latest` symlink.
**overlays/** Contains board and variant overlays. Used when a board is
configured using `setup_board --board ${BOARD}`. See the [overlays directory
README](https://chromium.googlesource.com/chromiumos/overlays/board-overlays.git/+/master/README).
** platform/** Contains all source code for packages that were not developed
originally by a third party (those go in `third_party`). If you're using the
minilayout, this directory will appear nearly empty until you start working on
something. **libchromeos/** Contains files related to `libchromeos`.
** scripts/** Contains the many necessary scripts that help you build Chromium
OS. **chromite/** Contains the "next generation" build scripts and related
libraries. Note that there is duplication in some cases where the same (or
similar) files are present in the current (non-chromite) scripts and the next
generation (chromite) build scripts. Also note that some of the bits in the
chromite directory are already being used.
**third_party/** Contains open source packages that have been modified for the Chromium OS project. This includes any local forks of open source projects, such as [flimflam](https://chromium.googlesource.com/chromiumos/platform/flimflam). If you're using the minilayout, this directory will appear nearly empty until you start working on something. **chromiumos-overlay/** This directory is overlaid on top of the portage directory (see below) and contains ebuild files, patches, and configuration information. Since the portage directory is kept as a pure snapshot of the upstream portage, all Chromium OS changes to those files are placed here. It's admittedly a bit strange to put this directory in `third_party`, but you'll get used to it. **chromeos-base/** **Contains the ebuilds for many chromeos-related files. Most of these files have corresponding source code in `src/platform`. If you have the minilayout, you need to `cros_workon` them first.**
**profiles/** Contains portage profiles.
**targets/** Contains directories for each general class of build target. At the
moment, there is only one overlay target (chromeos). **chromeos/** Contains
Chromium OS-specific target information. Includes things like: overrides of
package masks, unmasks, and keywords; resolutions of virtuals; etc.
**portage-stable/** Contains ebuild files, patches, and configuration
information from a snapshot of the portage build system (the current snapshot is
a little out of date, though). These ebuild files tell where to find the
upstream sources for various packages and how to patch/build them in the context
of the portage build system.
**chromite/** Contains build tools. **chromium/** Contains source repos that
come from the Chromium browser. **chroot/** This directory is created by
`chromite/bin/cros_sdk `(in PATH) and contains the whole SDK chroot environment
(compiler, tools, etc) for building Chromium OS. You're expected to "enter" this
chroot (make it the root of your filesystem) by using `cros_sdk` as well.
**DO NOT use `rm -rf` to delete this directory. Use `cros_sdk --delete`
instead.**
**home/$USER/trunk****/** Is a bind mount of the entire source tree so that
things may be accessed inside of the chroot. **.cache/** Cached files that can
be shared across chroots. Or safely deleted to recover some space.
** distfiles/** Downloaded source code packages (tarballs/etc...).
** sdks/** Downloaded sdk tarballs -- used to bootstrap the chroot/ tree.
**.****repo/** Contains information managed by the repo source control tool.
Includes the file `manifest.xml`, which specifies where various projects are
placed in your source tree, and the file `local_manifest.xml`, which is
populated automatically if you use `cros_workon` with the minilayout.
TODO: would it be useful to also have a document describing directory structure
as seen from inside the chroot?
