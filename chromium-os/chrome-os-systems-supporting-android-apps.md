# Chrome OS Systems Supporting Android Apps

Soon, you’ll be able to use the Google Play Store and Android apps on many
Chromebooks. Find out more on the [Google Chrome
blog](https://chrome.googleblog.com/2016/05/the-google-play-store-coming-to.html).

The Chromebooks, Chromeboxes, and Chromebases that will be able to install
Android apps are listed below. Roll-out of Android Apps is done on a
device-per-device basis as it is dependent on a number of factors including the
hardware platform that the device is based on and each device must be
[compatible with Android](https://source.android.com/compatibility/). While we
won't be able to bring Android apps to every Chromebook ever made, we're
continuing to evaluate more devices and we'll update this list as new devices
are added. Even if your Chromebook isn't on the list below, it will continue to
get other new features and improvements.

To learn how to install Android apps on supported Chromebooks, [click
here](https://support.google.com/chromebook/answer/7021273).

Manufacturer

Device

Status1

Acer

Chromebook R11 (CB5-132T, C738T)

Stable Channel

Chromebook Spin 11 (R751T)

Stable Channel

Chromebook R13 (CB5-312T)

Beta Channel

Chromebook 11 N7 (C731, C731T)

Beta Channel

Chromebook 14 for Work (CB5-471)

Beta Channel

Chromebook 15 (CB3-532)

Beta Channel

Chromebook 11 (C740)

Planned

Chromebook 11 (C730 / CB3-111 / / C730E / CB3-131)

Planned

Chromebook 14 (CB3-431)

Planned

Chromebook 15 (CB5-571 / C910)

Planned

Chromebook 15 (CB3-531)

Planned

Chromebox CXI2

Planned

Chromebase 24

Planned

AOpen

Chromebox Mini

Stable Channel

Chromebase Mini

Stable Channel

Chromebox Commercial

Planned

Chromebase Commercial

Planned

Asus

Chromebook Flip C100PA

Stable Channel

Chromebook Flip C213

Stable Channel

Chromebook Flip C302

Beta Channel

Chromebook C202SA

Beta Channel

Chromebook C300SA / C301SA

Beta Channel

Chromebit CS10

Planned

Chromebook C200

Planned

Chromebook C201PA

Planned

Chromebook C300

Planned

Chromebox CN62

Planned

Bobicus

Chromebook 11

Planned

CTL

NL61 Chromebook

Beta Channel

J2 / J4 Chromebook

Planned

N6 Education Chromebook

Planned

J5 Convertible Chromebook

Planned

Dell

Chromebook 11 (3180)

Beta Channel

Chromebook 11 Convertible (3189)

Beta Channel

Chromebook 13 (3380)

Beta Channel

Chromebook 11 (3120)

Planned

Chromebook 13 (7310)

Planned

eduGear

Chromebook R Series

Planned

Chromebook K Series

Planned

Chromebook M Series

Planned

CMT Chromebook

Planned

Edxis

Chromebook

Planned

Education Chromebook

Planned

Google

Chromebook Pixel (2015)

Stable Channel

Haier

Chromebook 11

Planned

Chromebook 11e

Planned

Chromebook 11 G2

Planned

Chromebook 11 C

Planned

Hexa

Chromebook Pi

Planned

HiSense

Chromebook 11

Planned

Lava

Xolo Chromebook

Planned

HP

Chromebook x360 11 G1 EE

Beta Channel

Chromebook 11 G5 EE

Beta Channel

Chromebook 13 G1

Beta Channel

Chromebook 11 G3

Planned

Chromebook 11 G4 / G4 EE

Planned

Chromebook 11 G5

Planned

Chromebook 14 G4

Planned

Lenovo

ThinkPad 11e Chromebook (Gen 4)

Stable Channel

Thinkpad 11e Yoga Chromebook (Gen 4)

Stable Channel

Flex 11 Chromebook

Beta Channel

N23 Yoga Chromebook

Beta Channel

N22 Chromebook

Beta Channel

N23 Chromebook

Beta Channel

N42 Chromebook

Beta Channel

100S Chromebook

Planned

N20 / N20P Chromebook

Planned

N21 Chromebook

Planned

ThinkPad 11e Chromebook

Planned

ThinkPad 11e Yoga Chromebook

Planned

Thinkpad 11e Chromebook (Gen 3)

Planned

ThinkPad 11e Yoga Chromebook (Gen 3)

Planned

Thinkpad 13 Chromebook

Planned

ThinkCentre Chromebox

Planned

Medion

Chromebook Akoya S2013

Planned

Chromebook S2015

Planned

M&A

Chromebook

Planned

Mercer

Chromebook NL6D

Beta Channel

NComputing

Chromebook CX100

Planned

Nexian

Chromebook 11.6"

Planned

PCMerge

Chromebook PCM-116E

Planned

Chromebook PCM-116T-432B

Planned

Prowise

Chromebook Entryline

Planned

Chromebook Proline

Planned

Poin2

Chromebook 11

Planned

Positivo

Chromebook CH1190

Planned

Samsung

Chromebook Plus

Stable Channel

Chromebook Pro

Stable Channel

Chromebook 3

Beta Channel

Chromebook 2 11" - XE500C12

Planned

Sector 5

E1 Rugged Chromebook

Planned

Senkatel

C1101 Chromebook

Planned

Toshiba

Chromebook 2

Planned

Chromebook 2 (2015)

Planned

True IDC

Chromebook 11

Planned

Viglen

Chromebook 11

Planned

Chromebook 360

Planned

1. If a device status is marked as available on the Beta channel this does not
mean that it will be available in the Stable channel on the next release of
Chrome OS. For more information on how to move your device between Chromebook
release channels [click
here](https://support.google.com/chromebook/answer/1086915?hl=en-GB).
