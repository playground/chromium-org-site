# Documented Certificate of Ownership

The DCO is the standard release for Linux kernel code, and is required to submit
all Linux kernel code upstream, and hence to Google.

     By making a contribution to this project, I certify that: 
    (a) The contribution was created in whole or in part by me and I havethe right to submit it under the open source license indicated in thefile; or
     (b) The contribution is based upon previous work that,to the best of my knowledge, is covered under an appropriate opensource license and I have the right under that license to submit thatwork with modifications, whether created in whole or in part by me,under the same open source license (unless I am permitted to submitunder a different license), as indicated in the file; or
     (c)The contribution was provided directly to me by some other person whocertified (a), (b) or (c) and I have not modified it; and
     (d)In the case of each of (a), (b), or (c), I understand and agree thatthis project and the contribution are public and that a record of thecontribution (including all personal information I submit with it,including my sign-off) is maintained indefinitely and may beredistributed consistent with this project or the open source licenseindicated in the file.
