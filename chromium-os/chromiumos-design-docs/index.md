# Design Documents

This page has links to a preliminary set of Chromium OS design docs. (Also see
the [User Experience](../../user-experience/index.md) pages.)

**Note: These designs will evolve significantly, based on implementation
challenges, community feedback, and other factors. In many cases, these design
docs represent early planning and may change direction. We'll publish more
design docs in the coming months.**

### General

*   [Software Architecture](software-architecture/index.md)
*   [Source Code Management](source-code-management/index.md)
*   [Upstream First](upstream-first.md)

### Security

*   [Security Overview](security-overview.md)
*   [Protecting Cached User Data](protecting-cached-user-data/index.md)
*   [System Hardening](system-hardening.md)
*   [Chaps Technical
    Design](../../developers/design-documents/chaps-technical-design/index.md)
*   [TPM Usage](../../developers/design-documents/tpm-usage.md)
*   [Verified Boot
    Presentation](https://docs.google.com/a/chromium.org/presentation/d/1HHf_0nKrceQr_NQYGMpVlYTIYF8ky-eNxP7W5Lxw94Y/present#slide=id.g341ad2000_020)
    (2014)
*   [Verified Boot
    Presentation](https://docs.google.com/presentation/d/14haBMrbpc2zlgdWmiaTlp_iDG_A8t5PTTXFMz5kqHSM/present?slide=id.g11a5e5b4cf_0_140)
    (2016)
*   [Keyboard-Controlled Reset Circuit](keyboard-controlled-reset-circuit.md)

### Firmware

*   [Firmware Boot and Recovery](firmware-boot-and-recovery/index.md)
*   [Disk Format](disk-format/index.md) (including boot process)
*   [Developer Mode](developer-mode.md)
*   [EC-3PO](ec-3po/index.md) (EC console interpreter)
*   [Recovery Mode](recovery-mode/index.md)
*   [SAFT](../../for-testers/saft.md) (semi-automated firmware test)
*   [Verified Boot](verified-boot/index.md)
*   [Verified Boot Crypto Specification](verified-boot-crypto/index.md)
*   [Verified Boot Data Structures](verified-boot-data-structures/index.md)

### Platform

*   [Cros](chromium-os-libcros.md) (how Chromium talks to Chromium OS)
*   [File System/Autoupdate](filesystem-autoupdate/index.md)
*   [Kernel](chromium-os-kernel/index.md)
*   [User-Land Boot Design](boot-design/index.md)
*   [Library optimization](library-optimization.md)
*   [Login](login.md)
*   [Lucid Sleep](lucid-sleep.md)
*   [Out of memory handling](out-of-memory-handling/index.md)
*   [Partition Resizing](partition-resizing/index.md)
*   [Power Manager](../packages/power_manager/index.md)
*   [Printing](http://www.chromium.org/chromium-os/chromiumos-design-docs/chromium-os-printing-design)
*   [Text Input](text-input/index.md)
*   [User Accounts and Management](user-accounts-and-management.md)
*   [CRAS: ChromeOS Audio Server](cras-chromeos-audio-server.md)
*   [Chromium OS Cgroups](chromium-os-cgroups.md)

### Stateless

*   *Coming soon*

**UI**

*   [Immersive
    fullscreen](../../developers/design-documents/immersive-fullscreen.md)
*   [Volume keys](chrome-os-volume-keys.md)
*   [system notifications](system-notifications.md)
