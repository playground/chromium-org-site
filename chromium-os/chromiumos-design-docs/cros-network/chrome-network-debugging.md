# chrome network debugging

chrome has several debugging tools:

*   netlog (chrome://net-internals -> events). see [NetLog: Chrome’s network
    logging
    system](../../../developers/design-documents/network-stack/netlog/index.md),
    or [the list of netlog events and their
    meanings](http://src.chromium.org/viewvc/chrome/trunk/src/net/base/net_log_event_type_list.h)
*   chrome://net-internals/#chromeos -> controls to enable chromeos network
    debugging and to package up log files
*   chrome://net-internals/#tests -> test loading a particular URL
*   network debugger app (in development; contact ebeach@)
