# Cellular Activation (and Chrome/CrOS network management API)

ActivateCellular (network_menu.cc)

->
ash::Shell::GetInstance()->delegate()->OpenMobileSetup(cellular->service_path())

-> MobileSetupDialog::Show(service_path) \[if kEnableMobileSetupDialog\]

-> MobileSetupDialogDelegate::GetInstance()->ShowDialog(service_path)

-> MobileSetupDialogDelegate::GetDialogContentURL \[presumably\]

// returns chrome::kChromeUIMobileSetupURL + service_path_

-> browser->OpenURL(chrome::kChromeUIMobileSetupURL) \[otherwise; resolves to
chrome://mobilesetup\]

MobileActivator::SetTransactionStatus (mobile_activator.cc)

-> StartOTASP

-> EvaluateCellularNetwork

MobileActivator::OnNetworkManagerChanged (mobile_activator.cc)

-> EvaluateCellularNetwork

MobileActivator::OnNetworkChanged (mobile_activator.cc)

-> EvaluateCellularNetwork

MobileActivator::StartActivation

-> EvaluateCelluarNetwork

Who calls OnNetworkManagerChanged?

- NetworkLibraryImplBase::NotifyNetworkManagerChanged

-> NetworkLibraryImplBase::SignalNetworkManagerObservers

- NetworkMessageObserver::NetworkMessageObserver

- AshSystemTrayDelegate

- NetworkMenuButton

- NetworkScreen

MobileActivator::ChangeState (mobile_activator.cc)

-> FOR_EACH_OBSERVER(Observer, observers_, OnActivationStateChanged(network,
state_, error_description));

-> MobileSetupHandler::OnActivationStateChanged (mobile_seutp_ui.cc)
\[presumably\]

-> web_ui()->CallJavascriptFunction(kJsDeviceStatusChangedCallback, device_dict)

-> mobile.MobileSetup.deviceStateChanged (mobile_setup.js)

-> updateDeviceStatus_

-> changeState_

-> stopSpinner_ \[if PLAN_ACTIVATION_DONE\]

---

MobileSetupDialogDelegate::OnActivationStateChanged (mobile_setup_dialog.cc)

// does nothing

chromeos.connectionManager.setTransactionStatus (connection_manager.js)

-> reportTransactionStatus_

-> postMessage(msg, 'chrome://mobilesetup')

// seems to be billing related. see onMessageReceived_ in mobile_setup.js

NetworkLoginObserver

// The network login observer reshows a login dialog if there was an error.

NetworkMessageObserver

// The network message observer displays a system notification for network

// messages.

NetworkLibrary interface declarations

- NetworkManagerObserver::OnNetworkManagerChanged

- NetworkObserver::OnNetworkChanged

- NetworkDeviceObserver::OnNetworkDeviceChanged

- NetworkDeviceObserver::OnNetworkDeviceFoundNetworks

- NetworkDeviceObserver::OnNetworkDeviceSimLockChanged

- CellularDataPlanObserver::OnCellularDataPlanChanged

- PinOperationObserver::OnPinOperationCompleted

- UserActionObserver::OnConnectionInitiated

NetworkLibraryImplCros::NetworkManagerStatusChangedHandler

NetworkLibraryImplCros::NetworkManagerUpdate

-> NetworkManagerStatusChanged

-> NotifyNetworkManagerChanged \[ONLY for PROPERTY_INDEX_OFFLINE_MODE\]

CrosMonitorNetworkManagerProperties

-> new NetworkManagerPropertiesWatcher

->
DBusThreadManager::Get()->GetFlimflamManagerClient()->SetPropertyChangedHandler

NetworkLibraryImplCros::UpdateNetworkStatus

-> NotifyNetworkManagerChanged

NetworkLibraryImplCros::UpdateTechnologies

-> NotifyNetworkManagerChanged

NetworkLibraryImplCros::ParseNetwork

-> NotifyNetworkManagerChanged

NetworkLibraryImplCros::ParseNetworkDevice

-> NotifyNetworkManagerChanged

AshSystemTrayDelegate: updates status icons

---

Network::SetState (network_library.cc)

// processes state change events from connection manager
