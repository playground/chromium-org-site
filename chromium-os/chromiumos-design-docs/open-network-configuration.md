# Open Network Configuration

The Open Network Configuration specification describes a network and certificate
configuration format that could be used across operating systems. Chromium OS
will support it natively.

The specification can be found here:

<https://chromium.googlesource.com/chromium/src/+/master/components/onc/docs/onc_spec.md>
