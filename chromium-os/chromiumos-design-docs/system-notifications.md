# system notifications

## Summary

ChromeOS has several types of system status and some of them should be notified
to the user. This document summarizes which types of notifications are currently
used and what kind of UI flow is necessary here.

The notifications are usually generated through the internal C++ API for the
[desktop
notifications](../../developers/design-documents/extensions/proposed-changes/apis-under-development/desktop-notification-api.md),
which will appear as the [rich
notification](http://blog.chromium.org/2013/05/rich-notifications-in-chrome.html)
as well.

## Blocking and system notifications

Sometimes normal notifications should be blocked due to the current system
status. For example, all notifications should be queued during the lock screen,
and they should appear when unlocked. It also should be blocked when fullscreen
state; notifications should be annoying when the user watches YouTube videos, or
has a presentation (note: the notifications **should** appear in [immersive
fullscreen](../../developers/design-documents/immersive-fullscreen.md) mode
instead, because the shelf / message center UI is visible in such case).

However, the system status could be so important that they should appear in such
cases. This does not mean all system notifications should appear; some could be
misleading. For example, the notification for taking screenshot would behave as
same as the normal notifications, so it should not appear in the lock screen. In
addition, screenshot notification has the message to 'click to view' but click
should not work in lock screen. On the other hand, connecting display would be
okay to be notified even in the lock screen, it just notifies the new system
status.

See also the [notification blocking code
update](https://docs.google.com/document/d/1Ox0Gb659lE2eusk-Gwm-a_JkARva7LydQc3hZNJvDn0/edit?usp=sharing)
design doc.

## List of the system notifications

message center?ComponentSourceMessageShow Always? ("System")TimeoutSecure? (Show
on Lock Screen)Customize?
(can be disabled)Click ActionButtonTriggersYesdisplayDisplayErrorObserverCould
not mirror displays...NoYesYesNoNonedisplay connection
YesdisplayDisplayErrorObserverThat monitor is not supportedNoYesYesNoNonedisplay
connection
YesdisplayResolutionNotificationController..resolution was changed
to...YesBothNoNoAccept / Revert changeAccept / Revert changesettings change from
chrome://settings/displayYesdisplayScreenCaptureTrayItem... is sharing your
screenYesNoNoNoneStop CaptureappsYesdisplayScreenShareTrayItemSharing control of
your screen...YesNoNoNoneStop ShareappsYesdisplayScreenshotTakerScreenshot
takenNoYesNoYesShow FIles Appkeyboard shortcutYesdisplayTrayDisplayMirroring
to...NoYesYesNoShow Display Settingssettings change from
chrome://settings/display, and keyboard shortcutYesdisplayTrayDisplayExtending
screen to...NoYesYesNoShow Display Settingssettings change from
chrome://settings/display, and keyboard shortcutYesdisplayTrayDisplayDocked
modeNoYesYesNoShow Display Settingsclose
lidYeslanguageLocaleNotificationControllerThe language has changed
from...NoNoNoNoAccept changeRevert changeloginNolanguageTrayAccessabilitySpoken
feedback is enabled.\*YesNoNonekeyboard shortcutNolanguageTrayCapsLockCAPS LOCK
is on\*
YesNoNonekeyboard shortcutYeslanguageTrayIMEYour input method has
changed...NoYesNoNoShow IME detailed viewkeyboard
shortcutYesnetworkDataPromoNotificationGoogle Chrome will use mobile
data...YesNo?NoPromo URL or settingsYesnetworknetwork_connectActivation of...
requires a network connectionYesNo?NoShow
SettingsYesnetworkNetworkStateNotifier...Your .. service has been
activatedYesNo?NoShow SettingsYesnetworkNetworkStateNotifierYou may have used up
your mobile data...YesNo?NoConfigure NetworkYesnetworkNetworkStateNotifierFailed
to connect to network...YesNo?NoShow SettingsNonetworkTraySmsSMS from
...\*NoNoShow SMS detailed viewNopowerTrayPowerBattery full OR %<X>
remaining\*No / NeverYesNoNonelow batteryYespowerTrayPowerYour Chromebook may
not charge...NoYesYesNoNoneusb charger
connectedYesuserTrayLocallyManagedUserThis user is supervised
by...YesNoNoNoNoneloginYesuserTraySessionLengthLimitThis session will end
in...YesNoNoNoNonetimeout set by policy / sync / remaining time change
NouserTrayUserYou can only have up to three accounts in multiple
sign-in\*\*YesNoNone
