# Sideload Android apps on your Chromebook

This article has been relocated to
<https://developer.android.com/topic/arc/sideload.html>
