# boot-complete.conf

boot-complete.conf is a boot marker used to signal that the main app is up and
running and ready to be used by the user.

On a Chrome OS install, this means that the core services are running, Chrome is
running and the login screen is visible to the user.

In order to reduce the boot time, services non critical to the login screen
(metrics reporting, crash reporting, update engine, etc) are only started when
boot-complete is started.
