# Setting up tcpdump

## Setting up tcpdump

tcpdump is installed by default on test builds. You should be able to run
tcpdump to collect traces, then copy the files back to your development machine
and analyze the traces with your favorite tool (tcpdump, WireShark, etc).
