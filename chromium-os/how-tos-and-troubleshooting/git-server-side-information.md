# Git server-side information

## Introduction

This guide contains some helpful recipes for the git source control tool that
may be useful to developers working in Chromium OS. The recipes here are focused
on tasks related to maintaining the git server / adding new projects.

Related pages:

*   [The Chromium OS Developer
    Guide](http://www.chromium.org/chromium-os/developer-guide) - The main place
    for developers to get started. Note that formatting / coloring conventions
    used on this page are explained on the developer guide.
*   [The git client-side FAQ](gclient-and-git-faq.md) - Contains recipes that
    are more common for everyday use.

## How do I create a new repository on the git server?

1.  Talk to relevant people on IRC or the chromium-os-dev mailing list to make
    sure it's ok to add the new repositories
2.  Open a ["Infra>Git" bug with label
    OS=Chrome](https://bugs.chromium.org/p/chromium/issues/entry?template=Infra-Git)
    to request the repository be created and who has access to it. If you are
    not a committer, please include the email address of a committer/sponsor who
    is working with you in the bug. Note that these bugs are marked private to
    the internal team and the reporter.
3.  If the repo needs special permissions i.e. custom groups need access etc,
    state that in the ticket and be as specific as possible.

Tips for getting your new repo created with minimal fuss:

*   Be clear in the bug title that you are requesting a new git repo.
*   Say whether it should be on chromium.googlesource.com (public) or
    chrome-internal.googlesource.com (not public).
*   Provide a project path and name for the new repository. For public repos,
    the path should start with /chromiumos/. For private repos, the path should
    start with /chromeos/. Look at existing projects in
    [git](https://chromium.googlesource.com/) or
    [git-internal](https://chrome-internal.googlesource.com/) for precedents on
    the path to use.
*   Note: You don't need to go into too much detail about why the repo is
    needed.

## How do I add files to a new repository on the git server?

Do this **OUTSIDE** your local chromiumos source tree. You will not be adding
the files directly, but starting by adding them via a Gerrit CL. Once the CL is
uploaded and reviewed, you can use Gerrit to complete pushing the CL into the
repository.

You'll want to set the `repo` shell variable to the name of your repository
(e.g. trunks) before starting, and repo-path to the full server-side path (e.g.
chromiumos/platform/trunks.git).

    mkdir ${repo}
    cp -rp /my/repo/seed/contents/* ${repo}
    cd ${repo}
    git init
    git add .
    git commit -am "init repo"
    # somelabel is just a throwaway pointer (name) used between the following 2 commands
    git remote add somelabel https://chromium-review.googlesource.com/${repo-path}
    git push somelabel HEAD:refs/for/master  # Creates Gerrit Issue

**IMPORTANT**: One of your files should be a
[README.chromium](../chromiumos-design-docs/source-code-management/index.md)
file. Another should probably be a `LICENSE` file, which is pointed to by the
`README.chromium` file. (Note: This applies mostly to local copies of upstream
repos, typically in src/third-party. We don't seem to follow this rule for repos
in src/platform.)

## How do I create a new branch on the git server?

**IMPORTANT**: Only authorized server branch owners can do this. You know who
you are :)

Once you are in a local repository and want to replicate a local branch on the
server, use the following commands:

    # If not yet added - add the remote server to your git repository
    # `somelabel' is just a throwaway pointer (name) used between the following
    # 2 commands
    git remote add somelabel https://chromium-review.googlesource.com/${repo-path}
    # This is the actual push which will create a branch named <remote-branch-name>
    # on the server with the contents of the <local-branch-name> from the local
    # repository.
    git push somelabel <local-branch-name>:refs/heads/<remote-branch-name>

## How do I add my project to the manifest?

**IMPORTANT NOTE: Please verify that this repo is public and non-empty before
adding it to a manifest.xml file.**

1.  Update either the `internal_full.xml` or `external_full.xml` file in the
    `manifest-internal` and `full.xml` in the `manifest` repositories.
    *   They are included in the root of your Chromium OS check out to make your
        life easier (use \`repo upload\` like normal).
    *   Note: the `default.xml` should be a symlink to `full.xml`; all the other
        files in there you can generally ignore.
    *   Note: if you're using a public checkout, it only includes the public
        `manifest` repository.
    *   Note: For internal repositories (those which come from `cros-internal`)
        use `internal_full.xml`. For external ones (which come from `cros`), use
        `external_full.xml`.
2.  Please follow the style for the section you're adding to (which usually
    means alphabetical order).
3.  See the [Directory Structure
    page](../developer-guide/directory-structure.md) for guidelines as to where
    to put the new repository in the checkout.
4.  To test your change with `repo sync`, you'll need to apply it to the
    `.repo/manifests/` directory within your Chromium OS checkout.
    *   This will verify that you didn't make an error in the XML format, and
        that the repository is being checked out to the location you expect.

    **# Change to the internal manifest**
    cd ~/chromiumos/manifest-internal
    **# Add your project in alphabetical order**
    vi internal_full.xml     # or external_full.xml
    **# Commit it locally like normal**
    git commit -a
    **# Upload it to the server**
    repo upload --cbr .
    **# Repeat all the above steps for the public manifest (if the project is public)**
    cd ~/chromiumos/manifest/
    ...
    **# Apply the change to your local checkout and verify it**
    cd ~/chromiumos/manifest-internal/
    git format-patch -1
    mv 0001-* ~/chromiumos/.repo/manifests/
    cd ~/chromiumos/.repo/manifests/
    git am -3 0001-*
    cd ~/chromiumos
    repo sync
    ... verify it checked out the repo to the right place ...
    ... undo the changes you made to .repo/manifests/ if you want ...

## How do I make a copy of an upstream repository on the git server?

1.  Talk to relevant people on IRC or the chromium-os-dev mailing list to make
    sure it's ok to add the new repositories
2.  [Create a new chromium-os repo](git-server-side-information.md) for your new
    repository.
3.  Ask chrome-infrastructure team for giving you permission to do perform
    merges and push branches on this repository.
4.  Initialize the initial set of branches. Example below:

    # The upstream git repository is at:
    # http://example.com/example.git
    # The corresponding chromium os repo is:
    # https://chromium.googlesource.com/repo.git
    git clone http://example.com/example.git
    cd example
    # chromiumoslabel is just a pointer to our chromium os repository
    # (where you want the upstream repo to be
    git remote add chromiumoslabel https://chromium.googlesource.com/repo.git
    # Now push a given commit hash from the upstream repo to the chromiumos copy.# Here we are pushing HEAD from the upstream repo as the master
    # branch on the chromium os copy. You can also use a specific
    # commit hash (man git-push for details).
    git push chromiumoslabel HEAD:refs/heads/master
    # Now also create a new branch on repo.git which may be used
    # to track upstream, pull in new patches, etc.
    git push chromiumoslabel HEAD:refs/heads/upstream
    # Finally, do not forget to add a README.chromium to the master
    # branch (This should be done as a normal Gerrit code review.)
