# Chrome OS build with LLVM Clang and ASAN

This page describes steps to build ChromeOS with Clang or
[ASAN](../../developers/testing/addresssanitizer.md) (which is Clang-based).
Address Sanitizer ([ASAN](../../developers/testing/addresssanitizer.md)) is a
fast memory error detector based on compiler instrumentation.

Note that currently we can reliably build ChromeOS Chrome with Clang. Other
Chrome OS modules are TBD.

If you are interested in use of Clang or ASAN, please subscribe to the "page
changes" to be notified when the process below is improved. Please send feedback
about this process to Denis Glotov and/or chromium-os-dev mailing list.

1. Have your chroot set up and all packages built. Just as in [Chromium OS
Developer Guide](../developer-guide/index.md).

2. Specify what you want to build with: either clang only or clang + ASAN.

    export USE="$USE clang asan"

3. Build ChromeOS Chrome as usual.

    emerge-$BOARD chromeos-chrome

Note that we have buildbots that uses Clang/ASAN and runs auto tests. See more
here: [Sheriff FAQ: Chromium OS ASAN
bots](../../developers/tree-sheriffs/sheriff-details-chromium-os/sheriff-faq-chromium-os-asan-bots.md).
