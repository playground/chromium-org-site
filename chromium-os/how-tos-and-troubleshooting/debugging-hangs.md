# Helping debug system hangs

If your Chromebook appears hung (i.e., the keyboard and/or trackpad seem
unresponsive), try the following steps:

1.  Press alt-volume_up-x key combination. If you're using a ChromeBox use F10
    instead of Volume-Up. These three keys need to be pressed in that order.
    This should cause Chrome browser to restart, and it may take a few seconds.
    If the system becomes responsive, go to step 6.
2.  If the system is still unresponsive, press the alt-volume_up-x key
    combination again. This should cause X to restart. If the system becomes
    responsive, go to step 6.
3.  If the system is still unresponsive, press the alt-volume_up-x key
    combination a final time. This should cause the kernel to panic-reboot. If
    the system becomes responsive, go to step 6.
4.  Press alt-volume_up-r key combination, the system should reboot, go to step
    6. (This won't work on older chromebooks or chromeboxes)
5.  If the system is still unresponsive, press the power button for 8+ seconds
    and the system will power off
6.  Boot the machine and log in and file feedback using alt-shift-i key
    combination.
