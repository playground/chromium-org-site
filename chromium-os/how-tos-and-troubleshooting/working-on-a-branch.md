# Working on a Chromium OS Branch

*This applies to commits to Chromium OS repositories. For changes to Chromium
repository branches, see the information about
[Drover](../../developers/how-tos/drover.md); for Blink, see [experimental
branches](../../developers/experimental-branches.md).*

### Note: When to update ebuilds when merging changes

This works the same on branches as on trunk: If you're merging a change to code
that's built as part of a `cros_workon` package, or to a `.ebuild` file for a
`cros_workon` package, the package will be uprev-ed automatically. If you're
changing a non-`cros_workon` package, you must uprev the corresponding `.ebuild`
file on the branch, just as you do when making changes to non-`cros_workon`
packages on trunk.

## Merge via Gerrit UI

The Gerrit UI now contains a 'Cherry Pick' button which is currently the easiest
way of merging a change which does not conflict, if the change you want to merge
to a branch is already on Gerrit, you need only a browser.

1. Open up the CL in question on Gerrit.

**\[OLD UI:\]**

2. Click the Cherry Pick button (near the middle of the page).

3. In the pop up 'Cherry Pick to Branch' field, type in some characters from the
branch you wish to merge to (e.g. R52) and Gerrit will provide a list of
possible branches, pick the one you want.

4. Click Cherry Pick Change at the bottom of the pop up.

**\[NEW UI:\]**

2. Select "Cherry Pick" from the "More" menu at the top right.

3. Enter the full branch name, for instance **release-R58-9334.B**. (Use "git
branch -a | grep R58" or equivalent to find the full name.)

4. Click "Cherry Pick" at the bottom of the pop up.

**\[BACK TO BOTH UIs:\]**

5. This should create a new CL against that branch in Gerrit. To land the CL,
mark it ready as usual (Code-Review +2, Verified +1, Commit-Queue +1).

Note this only works if the patch applies cleanly on the target branch, if there
is a conflict then the merge must be done manually as Gerrit provides no way of
dealing with merge conflicts in the UI.

Also note that you can change the commit message in Gerrit, including changing
CQ-DEPENDS directives as appropriate.

## cros_merge_to_branch

The second easiest way to create a change from a change you already committed on
ToT in Chrome OS is using `cros_merge_to_branch`. If you're inside the chroot,
it'll be in your $PATH. Otherwise, you can run it directly from
`~/chromite/bin/cros_merge_to_branch`.

Example usage:

    cros_merge_to_branch 32027 32030 32031 R25

This creates Gerrit changes for R25 from CL's 32027, 32030, and 32031 in less
than 10 seconds. After running, you can check Gerrit to actually commit the
changes (check your open commits for R25-\* branch). You can also run these
changes through a [try-bot](../build/using-remote-trybots.md) (make sure to
specify the branch with -b with the remote try-bot). For more advanced usage
information, use --help, or ping sosa@ or vapier@.

You should run with --dry-run the first time around to not actually upload your
change until you are sure about how to use the tool. Note this tool accepts
gerrit change numbers or change-id's, however, since the former is guaranteed to
be unique, it is advised you use those instead.

## Checking out the whole tree (with repo)

You must have different checkouts (yes, new chroots in a completely new
directory) for every branch you are working on. This is to ensure all the
prebuilts work automatically for you. You have to pass the -b <branch_name>
option to repo during init and you will follow exactly the same workflow
described in the [Chromium OS Developer Guide](../developer-guide/index.md) (i.e
cros_workon + repo start etc)

**If you have an existing repo checkout**: You can run repo init with
--reference to re-use the objects of your existing checkout, to reduce sync
time. Note that an absolute path is required (../../foo won't work) and that it
must be the topmost directory of the existing repo checkout, i.e. the one that
contains the .repo directory.

You can find the exact name of the branch by browsing the [manifest
repo](https://chromium.googlesource.com/chromiumos/manifest/+refs).

    mkdir release-R23-2913.B/
    cd release-R23-2913.B/
    repo init -u <URL> -b <branchname> [-g minilayout] [--reference /path/to/existing/checkout]
    repo sync

NOTE: For older branches (e.g. R22 and earlier), you'll need to specify -m
minilayout.xml instead of -g minilayout.

Example (See <http://goto/chromeos-building> if you are doing an internal build
and replace the manifest.git link with the appropriate one)

    repo init \
       -u https://chromium.googlesource.com/chromiumos/manifest.git \
       -b release-R23-2913.B \
       --repo-url https://chromium.googlesource.com/external/repo.git

Next, follow steps in the [developer's
guide](http://www.chromium.org/chromium-os/developer-guide#TOC-Get-the-source)
to sync/edit/modify files i.e repo sync, cros_work start, repo start, etc. After
you've cherry-picked/made the changes you want, upload the changes for review.
If cherry-picking/etc..., make sure to recreate the ChangeID in the log message
so that you get a new one -- simply delete the line and git will create a new
one for you.

    repo upload

### Testing w/remote trybot

Before you commit the change, test it! Launch a tryjob to verify it actually
builds properly. See [Using Remote Trybots](../build/using-remote-trybots.md)
for more information.

    cd <repo_root>/chromiumos/chromite
    git checkout cros/master
    cbuildbot --remote -g <review_id> -b <branchname> alex-paladin daisy-paladin

## Checking out a single repository (with repo)

If you don't have a full repo checked out already and want to do a quick one-off
merge, you can still check out the much smaller buildtools group:

    repo init \
       -u https://chromium.googlesource.com/chromiumos/manifest.git \
       --repo-url https://chromium.googlesource.com/external/repo.git \
       -g buildtools

This will get you chromite and all the tools it includes -- i.e.
cros_merge_to_branch. Make sure you pass the --nomirror option so it will fetch
the single git repo needed to cherry-pick & upload the CL.

Finally, use cbuildbot with --remote to run remote trybots. See the section
above for more info.

## Checking out a single repository (with git)

If you want to push up a few changes without checking out the entire tree, then
you can use git to do just that.

You can re-use an existing repo checkout if you like (but make sure you clean up
when you're done). Let's assume you're going to make a new checkout to keep
things clean though.

### Cloning a new repository

Find the git url you care about. You can get it by going into your repo checkout
and look at .git/config (the url field). You'll need to use the -review variant
of the URL to push to the special refs/for/\* refs. Let's demonstrate with the
chromite git tree.

    git clone https://chromium-review.googlesource.com/chromiumos/chromite.git

If you want to speed things up, you can use the --reference option to re-use a
local tree.

    git clone https://chromium-review.googlesource.com/chromiumos/chromite.git --reference ~/chromiumos/chromite/.git

### Setting your author/committer settings

If your normal user information is not your chromium.org e-mail, you'll need to
set it in the new repo.

    git config user.name "Awesome Developer"
    git config user.email ${USER}@chromium.org

### Start a new branch

Let's assume you want to work on the R23 branch. You need the full name of the
branch, and then create a new local branch to work on with that info.

    git branch -a | grep R23

That shows us the full branch name is "remotes/cros/release-R23-2913.B", so we
can do:

    git checkout -b R23 remotes/cros/release-R23-2913.B

### Make your changes

This part is where the real work happens :). Use git's or repo's cherry-pick
feature, or make the changes by hand, or apply patches, or whatever you want.

    # When the editor pops, try to change few attributes to help tracking commit history.
    # Change Reviewed-on to Previous-Reviewed-on, and
    # add a line like **(cherry picked from commit b9e382afa7e410745ac96b12b49d5a941070db1e).**
    # For changes in different branches, you can keep same Change-Id; otherwise remove that to get a new unique Change-Id.
    git cherry-pick -x -e <SHA1>

### Publish your commits to gerrit

Now for the last step. If you didn't create a new clone, you might have to
change "origin" to "cros", or replace it with the full git url. The "R23" is
whatever you called the local branch, and the "release-R23-2913.B" is exactly
what the official branch name is called -- make sure it's correct as gerrit will
allow you to push to anything.

    git push origin R23:refs/for/release-R23-2913.B
