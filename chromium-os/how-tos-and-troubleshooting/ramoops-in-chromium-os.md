# Kernel Crash Logs in Chromium OS

## Introduction

Chromium OS used to use a home grown "ramoops" method for storing crash logs in
Chromium OS. This has been replaced by a more friendly "pstore" format. This
change happened early in the 3.0 tree.

## Kernel crash logs in Chrome OS

Chromium OS crash logs are now stored in "text files" in /dev/pstore, under the
filenames "/dev/pstore/dmesg-ramoops-*number*", with individual files holding
the most recent system logs.

## Ramoops in Chromium OS -- the old and busted technique (kernel version 2.6.x and early 3.0)

Details on how ramoops works in general can be found in
Documentation/ramoops.txt in the kernel (which can be viewed online
[here](http://git.kernel.org/?p=linux/kernel/git/torvalds/linux.git;a=blob;f=Documentation/ramoops.txt;h=8fb1ba7fe7bfe208ca526d24c2c61c5a998df109;hb=HEAD)).

Ramoops has replaced preserved and uses the same memory area as it did to
preserve the crash log after a reboot.

The current values are set in the config files under CONFIG_CHROMEOS_RAMOOPS_\*.
At the time of the writing of this article they are set by change
[I9b99d433](http://gerrit.chromium.org/gerrit/#change,5428).

As described in the Ramoops documentation, the module has uses a area of memory
in which it writes records. Currently, the parameters for all platforms are as
follows:

*   Memory size: 0x00100000 bytes (1MB)
*   Record size: 0x00020000 bytes (128KB)

The parameters can be read at runtime from /sys/module/ramoops/parameters/\*

Ramoops is capable of writing multiple records in case of a multiple successive
bugs, however the current behavior is to reset on a kernel oops or panic (set by
panic_on_oops in
[sys-apps/baselayout/files/sysctl.conf](http://gerrit.chromium.org/gerrit/#patch,sidebyside,5159,1,sys-apps/baselayout/files/sysctl.conf)).

This leads to the fact that only 1 record will be used on a oops/panic (because
the chromebook will restart after that and the counter will be reset).

Therefore, as long as the current behavior does not change, additional data can
be stored and preserved in that area after the first record.

In light of this, ellyjones turned the amount of preserved space ramoops uses
down from 1MB to 512KB in http://gerrit.chromium.org/gerrit/7288, freeing up
512KB for various sinister purposes. This memory starts at address
CONFIG_CHROMEOS_RAMOOPS_RAM_START + CONFIG_CHROMEOS_RAMOOPS_RAM_SIZE, and is
allocated as follows:
64 bytes at offset 0: keystash (kernel/keystash.c) /dev/keystash backing store.
524224 bytes at offset 64: unallocated
