# Boot mainline kernel on veyron-jaq

## Note: work in progress.

## ChromiumOS

#### Setup

Follow the instructions in the [Chromium OS Developer
Guide](../../developer-guide/index.md), the tl;dr version being:

    mkdir ~/s/chromium

    cd ~/s/chromium

    repo init -u https://chromium.googlesource.com/chromiumos/manifest.git --repo-url https://chromium.googlesource.com/external/repo.git

    repo sync -j50

#### Build

    cros_sdk # to enter chroot
    BOARD=veyron_jaq
    ./setup_board --board=$BOARD
    ./build_packages --board=$BOARD
    ./build_image --board=$BOARD --noenable_rootfs_verification --enable_serial=ttyS2 test

## Firmware

Build netboot firmware for veyron to pull kernel image and command line from a
tftp share on the network. Do the following inside the chroot:

**Note: **The following uses a servo to update firmware. I think you can do the
flashrom command on the device itself, but that introduces a brick risk.

    cros_sdk # to enter chroot

    BOARD=veyron_jaq

    TFTP=<tftpserver>

    FTP_FOLDER=${BOARD}

    BOOTFILE=${FTP_FOLDER}/vmlinuz

    ARGSFILE=${FTP_FOLDER}/cmdline

    BIOS=/build/${BOARD}/firmware/image.net.bin

    emerge-${BOARD} chromeos-ec libpayload depthcharge coreboot chromeos-bootimage

    sudo gbb_utility -s --flags=0x239 ${BIOS}

    sudo ~/trunk/src/platform/factory/setup/update_firmware_settings.py \
      -i ${BIOS} \
      --tftpserverip=${TFTP} \
      --bootfile=${BOOTFILE} \
      --argsfile=${ARGSFILE}
    dut-control warm_reset:on spi2_buf_en:on spi2_buf_on_flex_en:on spi2_vref:pp1800
    sudo flashrom -p ft2232_spi:type=servo-v2${SERIAL_SERVO} --verbose -w ${BIOS}
    dut-control spi2_buf_en:off spi2_buf_on_flex_en:off spi2_vref:off warm_reset:off

## Kernel

#### Setup

Pull down a copy of mainline and use multiv7 defconfig (a .config is also
attached to this page, but will likely be stale by the time of reading)

    mkdir ~/s/kernel

    cd ~/s/kernel

    git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git .

    ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- make O=.build_multiv7 multiv7_defconfig

#### Download the its file attachment to this page and store it in ~/s/kernel/.build_mutliv7

#### Build & Deploy

Compile your kernel

    ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- make O=.build_multiv7 -j40

Create a fit image for veyron-jaq that contains the kernel and the device tree

    mkimage -D "-I dts -O dtb -p 2048" -f rk3228-veyron-jaq.its arch/arm/boot/vmlinuz

Copy the fit image to your tftp share

    scp arch/arm/boot/vmlinuz <tftpserver>:/tftpboot/veyron_jaq
