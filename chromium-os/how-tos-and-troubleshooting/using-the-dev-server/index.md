# Dev server

## Overview

​The dev server allows you to update your Chromium OS machine with new builds
without having to copy them via a USB drive. It also serves as a static file
repository, allowing you to install your own packages and scripts making it easy
to customize your machine for development purposes.

*Note: Every time you create a Chromium OS build, the URL of the dev server
corresponding to your development machine is put into `/etc/lsb-release`. This
file can be changed post-installation to manage a test machine's update source
or can be overridden in a `/mnt/stateful_partition/etc/lsb-release`.*

## Starting the Dev Server

​*Note: Before you start the dev server, you must have already run
`build_packages.sh`. If you have not, please run this now.*

​The first step in using the dev server is starting the web server on your
development machine:

    cd ~/chromiumos/; cros_sdk  # if not already in the chroot
    start_devserver

## Using the Dev Server

You can use the dev server in three ways:

1.  Updating your machine with the newest image built on your dev machine
2.  Customizing your machine with your own packages
3.  Building specific packages on your dev server and installing them on your
    test machine

### Setting up your image to allow gmerge to modify it

By default, the rootfs image is built with verification enabled. This prevents
gmerge from writing to it to update it. To get around this build the rootfs
image using the following option (**NOTE**: if you followed the instructions in
the [Chromium OS Developer Guide](../../developer-guide/index.md), you already
did this):

    ./build_image --noenable_rootfs_verification ...

### How to update

Updating replaces everything on the root filesystem of your test machine with
your latest build on your development machine. This replaces the process of
copying the full image to a usb key and re-running `/usr/sbin/chromeos-install`.

To update:

1.  Build an image on your workstation following the [build
    instructions](http://www.chromium.org/chromium-os/developer-guide#TOC-Building-Chromium-OS).
2.  On your test machine, log in and open the "About" page. (Note: This is done
    by clicking on the wrench icon and selecting "About Chromium OS". It is not
    done by typing "About:" in the omnibox.) The system should fetch and apply
    the update from the dev server. If you prefer not to use the UI or if you'd
    like to force an update to the same or older image version, run the
    following command on your test machine:

    update_engine_client --update

*Note: You can also specify a different url than you built with here using
--omaha_url=<url>.*

If everything went well, this will have updated the root filesystem (/dev/sda3
or /dev/sda5) and the kernel partition (/dev/sda2 or /dev/sda4). If you have
development tools you would also like to see updated (in chromeos-dev or
chromeos-test), or **if you use gmerge (see below)**, you should also update
your stateful partition. To do so, then run:

    sudo stateful_update

*Note: This will update your /usr/local/bin and /var directories. It will not
affect the other directories on your stateful partition (e.g. will not overwrite
your home directories).*

Now your system should be fully updated and you can restart to see the changes.

### Other options

Note that by default the devserver serves the latest image and runs on port
8080. You can change this among many other things. Here is a brief description
of some of the options. Please see `./start_devserver --help` for more
information.

*   `--use_cached` - Forces the devserver to use the update.gz it finds in its
    static dir. This allows you to pre-generate an update payload and place it
    in the devserver's static file hosting directory
    (~/src/platform/dev/static).
*   `--src_image` - Generates a delta update based from the target image on the
    source image. If the source image you provide is not the image on the
    machine, the delta update will fail. *Note that delta updates despite their
    name run slower than full updates (but will save you bandwidth).*
*   `--pregenerate_update` - Generates the update before starting the dev
    server. This keeps from getting nasty timeouts for big update payloads
    *(necessary for most delta updates).*
*   `--image` - Serve this image to a machine that requests it.
*   `--port` - Start the devserver on this port.
*   `-t` - From the directory (using the latest image and/or using the archive
    logic) use the test image found in that directory
    (chromiumos_test_image.bin)
*   `--static_dir` - Serve images from a file structure. Clients can prefix
    which image they want in their request by modifying their omaha_url. By
    default this changes the static directory to ~/src/platform/dev/static.

### How to customize your image with dev packages

By default build_packages and build_image both have the --withdev flag enabled,
so dev packages will be build and installed in your image.

### How to build a single package and install it without doing a full update

​If you want to make a change to a single package and install it on your target
machine, you can use the `gmerge` tool. `gmerge` runs on your target machine and
contacts your devserver to build packages. For instance, let's say you made a
change to shill and you want to re-install it on your machine. On your test
machine, run:

    sudo gmerge shill

This will kick off a build (emerge) of the `shill` package on your development
machine, transfer it to your test machine, and use emerge to install it. If you
have already built the package on your development machine and just want to
install it, you can use the -n flag to avoid the build step.

    sudo gmerge -n shill

This works for just about any package that is available in portage and can be
very convenient for installing packages that are not part of the default install
or even the dev install.

In order to protect you from a frustrating error case, `gmerge` will refuse to
install a `cros_workon` package unless you have run `cros_workon start` on that
package. You can override this behavior by passing `--accept_stable` to
`gmerge`.

You can pass USE= and FEATURES= environment variables to gmerge just like you
can on the host. (This is a change from older versions, which used --use=
arguments). For example, you can run:

    USE=install_test gmerge cromo

## Building Chrome Locally and using Gmerge

If you'd like to build chrome from source, you can still use the dev server. All
you need to do is declare a few variables before starting the dev server:

    export CHROME_ORIGIN="LOCAL_SOURCE" USE="-build_tests"
    ./start_devserver

NOTE: be sure to run the devserver from within the chroot. While normally you
may start it outside of the chroot (and it'll enter the chroot automatically),
exported environment variables aren't propagated into the chroot when you do
that.

Now on your device just run:

    sudo gmerge chromeos-base/chromeos-chrome

Another useful USE flag is "-runhooks" which will not run the gclient hooks each
time. While this step is a smaller percentage of the total time needed to do an
incremental build, it is unncessary for doing incremental builds if you haven't
changed any resources, or made changes to the Chrome gyp configuration.

Use flags can directly be passed to gmerge, so you can do:

    USE="-build_tests -runhooks" gmerge chromeos-chrome

## Common Tasks

To download and run functional tests, see
<http://www.chromium.org/chromium-os/testing>

## Using Devserver to Stage and Serve Artifacts from Google Storage

A running devserver listens for calls to its stage and xbuddy rpcs.

### Stage rpc

An abbreviated version of the documentation for the stage rpc is copied here,
but the up to date version can be found by going to host:port/doc/stage on any
running devserver.

Downloads and caches the artifacts Google Storage URL. Returns once these have
been downloaded on the devserver. A call to this will attempt to cache
non-specified artifacts in the background for the given from the given URL
following the principle of spatial locality. Spatial locality of different
artifacts is explicitly defined in the build_artifact module. These artifacts
will then be available from the static/ sub-directory of the devserver.

To download the autotest and test suites tarballs:

    http://devserver_url:/stage?archive_url=gs://your_url/path&artifacts=autotest,test_suites

To download the full update payload:

    http://devserver_url:/stage?archive_url=gs://your_url/path&artifacts=full_payload

To download just a file called blah.bin:

        http://devserver_url:/stage?archive_url=gs://your_url/path&files=blah.bin

### xBuddy rpc

see [xBuddy for Devserver > xBuddy Interface and Usage](xbuddy-for-devserver.md)

## Feature Requests

There are many things that can be done to improve this system. Feel free to make
suggestions and submit patches.
