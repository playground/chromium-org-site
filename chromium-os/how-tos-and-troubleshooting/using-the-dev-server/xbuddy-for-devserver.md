# XBuddy for Devserver

## **Overview**

XBuddy for devserver allows you to obtain ChromiumOS images off Google Storage
and reference locally built images. When used with devserver (or scripts that
use it), it can also be used to update your ChromiumOS machine to any image.

## **Prerequisites**

### Google Storage Credentials

You need them to get anything off of Google Storage!

Run:

    gsutil config

Ensure that the resulting .boto file is discoverable from the environment from
which you are running the devserver from, whether inside or outside the chroot,
so copy it over if necessary.

### Payload generation and chroot

Devserver needs to be run from inside chroot to generate an update payload from
an image binary.

## **Example Usage - Imaging a Device**

On your developer machine, start a devserver. (Please see [Using the
Devserver](index.md) for more details about the devserver.)

    cd ~/chromiumos/; cros_sdk  # if not already in the chroot
    start_devserver

### Update a device to the latest released build

On your test machine, with BOARD is your device board:

     update_engine_client --update --omaha_url="http://your_host:port/update/xbuddy/remote/${BOARD}/latest/dev"

### Update a device to the latest locally built image

On your test machine, get to a shell (Ctrl-Alt-F2), and use your_host:port is
where your devserver is running from.

    update_engine_client --update --omaha_url="http://your_host:port/update"

### Use an arbitrary XBuddy path.

On your test machine, with XBUDDY_PATH as some path to an image:

    update_engine_client --update --omaha_url="http://your_host:port/update/xbuddy/${XBUDDY_PATH}"

(See [XBuddy Paths](xbuddy-for-devserver.md) for all path options.)

## Example Usage - Staging Artifacts onto DevServer

### Get a particular artifact from the devserver

On your developer machine, call the xbuddy rpc with an xbuddy path to download
the latest local build on the devserver at host:port to the
chromiumos_test_image.bin

    wget http://host:port/xbuddy/local/x86-generic/latest/test -O chromiumos_test_image.bin

### Stage an artifact on the devserver and get the path to it

Similarly, download the path to the update payload on that devserver.

    wget http://host:port/xbuddy/remote/lumpy/latest-official/full_payload?return_dir=true -O path.txt

See XBuddy Interface and Usage for more details.

## XBuddy Paths

local/remote

board_name

version alias

image profile

Optional (defaults to local). If a path is explicitly remote, xBuddy will
interpret the rest of the path to query into google storage for artifacts. If
local, XBuddy will attampt to find the artifact locally.

Required. Mostly self-explanatory e.g. x86-mario

Optional (defaults to latest). A version string or an alias that maps to a
version string.

Optional (defaults to test). Any of the artifacts that devserver recognizes.

Version Aliases

latest (this is the default)

If local: latest locally built image for that board.
If remote: the newest stable image on Google Storage

latest-{dev|beta|stable}

Latest build produced for the given channel

latest-official

Latest available release image built for that board in Google Storage.

latest-official-{suffix}

Latest available image with that board suffix in Google Storage.

latest-{branch-prefix}

Latest build with given branch prefix i.e. RX-Y.X or RX-Y or just RX.

### XBuddy Path Overrides

There are abbreviations for some common paths.

For example, an empty path is taken to refer to the latest local build for a
board.

The defaults are found in src/platform/dev/xbuddy_config.ini, and can be
overridden with shadow_xbuddy_config.ini

## **XBuddy Interface and Usage**

### Devserver rpc: xbuddy

If there is a devserver running, stage artifacts onto it using xbuddy paths by
calling "xbuddy"

The path following xbuddy/ in the call url is interpreted as an xbuddy path, in
the form "{local|remote}/build_id/artifact"

The up to date documentation can be found under the help section of devserver's
index, http://host:port/doc/xbuddy

    http://host:port/xbuddy/{remote/local}/board/version/artifact
    http://host:port/xbuddy/remote/x86-generic/R26-4000.0.0/test # To stage an x86-generic test image
    http://host:port/xbuddy/remote/x86-generic/R26-4000.0.0/test?return_dir=true # To get the directory name that the artifact will be found in

### Devserver rpc: xbuddy_list

If there is a devserver running, check which images are currently cached on it
by calling "xbuddy_list"

    http://host:port/xbuddy_list

### Updating from a DUT: update_engine_client

If there is a devserver running, and you have access to the test device, update
the device by calling update_engine_client with an omaha_url that contains an
xbuddy path.

The omaha_url is a call to devserver's update rpc. Documentation found under the
help section of devserver's index. http://host:port/doc/update

    update_engine_client --update --omaha_url=host:port/update/
    # or
    update_engine_client --update --omaha_url=host:port/update/xbuddy/{remote/local}/board/version/{image_type}
    # or
    update_engine_client --update --omaha_url=host:port/update/board/version

### Scripts (cros flash)

To use xbuddy path with cros flash:

    cros flash ${DEVICE} xbuddy://${XBUDDY_PATH}

See [cros flash page](../../build/cros-flash.md) for more details.
