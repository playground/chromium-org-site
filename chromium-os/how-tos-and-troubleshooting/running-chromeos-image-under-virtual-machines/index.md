# Running a Chromium OS image under KVM

## Getting the VM image

Please follow the [Build
Instructions](http://www.chromium.org/chromium-os/developer-guide#TOC-Building-an-image-to-run-in-a-virtu)
document. It contains a section about building Chromium OS VM image. I will
assume that you have followed them to the point where you have a virtual machine
image with tweaks that you want there.

## Linux QEMU-KVM

[Linux KVM](http://www.linux-kvm.org/) is a **recommended** virtual machine
solution for Chromium OS (x86) since it is well supported by Linux community and
quite fast (kernel based virtualization, also supports native virtualization for
modern Intel and AMD CPUs). If present, KVM infrastructure can be utilized by
QEMU as well.

### Installing KVM on Ubuntu machine

In case you don't have KVM on installed on your machine yet, please run the
following command:

In Ubuntu run:

    $ sudo apt-get install qemu-kvm

### Enabling native virtualization in BIOS

Before running KVM for the first time, it's highly recommended that you reboot
your machine and check that your BIOS settings has enabled native
virtualization. Intel calls these technologies VT-x, on AMD machines they are
called AMD-V or AMD-Virtualization. You can see the list of supported CPUs at
<http://en.wikipedia.org/wiki/Intel_VT-x>, if you don't have one of them KVM
will still work but slower.

You can run the kvm-ok helper to verify things are working:

    $ sudo kvm-ok
    INFO: /dev/kvm exists
    KVM acceleration can be used

### Running Chromium OS

To simplify the process of starting/stopping VMs, there are helper scripts in
the Chromium OS checkout. Note: these must be executed outside of the SDK
chroot.

WARNING: After
[crbug/710629](https://bugs.chromium.org/p/chromium/issues/detail?id=710629),
'betty' is the only board regularly run through pre-CQ and CQ VMTest and so is
the most likely to work at ToT. 'betty' is based on 'amd64-generic', though, so
'amd64-generic' is likely to also work.

#### Starting a VM

    $ cd ~/chromiumos/src/scripts
    $ ./bin/cros_start_vm --image_path=../build/images/${BOARD}/latest/chromiumos_qemu_image.bin

#### Disabling the GUI

By default, this will include graphics which can slow things down (if you want
to run tests). You can use the --no_graphics option to disable that (which means
you can only access the system via ssh):

    $ ./bin/cros_start_vm --no_graphics --image_path=../build/images/${BOARD}/latest/chromiumos_qemu_image.bin

#### Stopping a VM

When you started the VM, it told you PID file it used. Pass that to this script
to stop the VM:

    $ ./bin/cros_stop_vm --kvm_pid /tmp/kvm.1234.pid

## Notes

Please note that KVM currently does not support graphics hardware acceleration.
As a result of that, you will see the red warning sign 'Unaccelerated graphics'
in the upper left corner as soon as Chromium OS GUI starts.

Please note that once you click within QEMU/KVM window, it will 'grab' your
mouse pointer. You can release the mouse by pressing **Ctrl+Alt**.

The virtual machine **monitor console** can be reached within the emulation
window by pressing **Ctrl+Alt+2**. This should open a qemu command where number
of useful commands can be run from. Their full description can be found at
<http://en.wikibooks.org/wiki/QEMU/Monitor>. You can always return back to
Chromium OS by pressing **Ctrl+Alt+1** from the monitor screen.

**Tip**: to use virtual terminal in Chromium OS, enter virtual machine monitor
console by **Ctrl+Alt+2** then type **sendkey ctrl-alt-f2** and switch back from
monitor by **Ctrl+Alt+1**. The opposite can be done by **sendkey ctrl-alt-f1**.
