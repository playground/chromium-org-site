# Gerrit Dependent Changes

### \[These instructions used to be on the gerrit groups page which have since ceased to exist after to switch to the new Google Groups\]

### Working in the same package: Dependent changes in the same package

If you have further work to do in the same package, and your new changes depend
on (or build upon) your old work, you need to create a dependent branch.

$ repo start new-branch .
$ git reset --hard old-branch

To switch back and forth between the two branches (e.g. to make additional
changes to the uploaded branch) you can use git checkout. You must be sure to
stage and commit your work in progress.

$ git add .
$ git commit --amend # caution: don't use --amend the first time you commit in a
branch
$ git checkout old-branch

Remember, each time you do this, your local sources will "flip". However, when
you flip from old-branch to new-branch, you will probably want the changes from
old-branch to propagate into new-branch - here's how:

$ git add .
$ git commit --amend # caution: don't use --amend the first time you commit in a
branch
$ repo upload . # optional - assumes you are ready to upload old-branch for
further review
$ git checkout new-branch
$ git rebase --onto old-branch HEAD^

When you are ready to upload and submit, things are slightly more complicated.
Here are the three basic steps you may be taking (though not necessarily in this
order).

#### Upload old-branch or new-branch for review

You should be able to do this at any time. Be sure to carefully un-comment the
desired change when you type `repo upload`.

#### Submit old-branch after reviews & approvals

You should be able to do this at any time (if you have the necessary approvals).
After this is done, you can quickly update your working tree:

$ git checkout new-branch
$ repo sync .
$ repo prune

After which "new-branch" should be properly rebased on the submitted old-branch,
and thus no longer have outstanding dependencies.

TBD: Are these steps sufficient? What might go wrong or might require extra
steps?

#### Submit new-branch after reviews & approvals

Because new-branch builds on old-branch, this would only happen after old-branch
has been submitted (see above).

TBD: Because time may have elapsed since old-branch was submitted, and cleanup
steps may or may not have been done, it would be nice if we could show here how
to "check" and confirm that new-branch has been properly rebased and is
independent of the no-longer-pending old-branch.
