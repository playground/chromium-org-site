# Git Client-Side FAQ

## Introduction

This guide contains some helpful recipes for the git source control tool that
may be useful to developers working in Chromium OS.

Related pages:

*   [The Chromium OS Developer Guide](../developer-guide/index.md) - The main
    place for developers to get started. Note that formatting / coloring
    conventions used on this page are explained on the developer guide.
*   [Tips and Tricks for Chromium OS
    Developers](../tips-and-tricks-for-chromium-os-developers.md) - Other tips
    and tricks for developers. At the moment, there may be some git tricks on
    that page too. **TODO**: move all git tricks here.
*   [Git server-side information](git-server-side-information.md) - Git recipes
    focused on tasks like creating new repositories / modifying the repo
    manifest.

## How do I see the date a changelist was committed to our tree?

If you do a `git log` in many of our repositories, you'll probably find yourself
confused about exactly what date something showed up. You'll probably find your
life much happier if you change your default log format to `fuller`.

Specifically, a current git log in the kernel tree shows:

    commit b0981007443498f81cb5610f1e41b98d10714f7f
    Author: Andrew Chew <achew@nvidia.com>
    Date:   Tue Sep 13 13:55:45 2011 -0700
        ...
    commit 9bf8e29dd45aebb5f77ae47457e8494ab31bff86
    Author: Doug Anderson <dianders@chromium.org>
    Date:   Wed Sep 14 16:09:28 2011 -0700
        ...
    commit 9160d57a8a038b37771daa95e29f878cc1d49ca1
    Author: Rajkumar Manoharan <rmanohar@qca.qualcomm.com>
    Date:   Tue Sep 13 19:09:22 2011 +0530
        ...

...note that the dates above are the dates that the CLs were created, but not
the dates that they were committed to the tree. Now, try the following command
to change your default log format:

    git config --global format.pretty fuller

Running git log again:

    commit b0981007443498f81cb5610f1e41b98d10714f7f
    Author:     Andrew Chew <achew@nvidia.com>
    AuthorDate: Tue Sep 13 13:55:45 2011 -0700
    Commit:     Doug Anderson <dianders@chromium.org>
    CommitDate: Fri Sep 16 15:27:46 2011 -0700
        ...
    commit 9bf8e29dd45aebb5f77ae47457e8494ab31bff86
    Author:     Doug Anderson <dianders@chromium.org>
    AuthorDate: Wed Sep 14 16:09:28 2011 -0700
    Commit:     Doug Anderson <dianders@chromium.org>
    CommitDate: Wed Sep 14 17:48:32 2011 -0700
        ...
    commit 9160d57a8a038b37771daa95e29f878cc1d49ca1
    Author:     Rajkumar Manoharan <rmanohar@qca.qualcomm.com>
    AuthorDate: Tue Sep 13 19:09:22 2011 +0530
    Commit:     Paul Stewart <pstew@chromium.org>
    CommitDate: Wed Sep 14 13:08:20 2011 -0700
        ...

Much better!

## How do I diff between current HEAD and the branch point?

I believe that the main branch for Chromium OS is always called cros/master, by
convention. Thus, the magic command is:

    git diff cros/master..

If you want to see a pretty side-by-side comparison instead of the inline
changes, try something like this:

    git difftool cros/master..

## How do I diff a single commit?

If you've want to see the commit with hash `10390f`, you can do:

    git diff 10390f^!

You can also use `git show`:

    git show 10390f

## How do I show the sha1 hashes/revs on a remote repo?

**TODO:** Give example.

    git ls-remote URL | grep HEAD

## How can I recover and close an open code review if I used 'git push' instead of 'git cl push'?

**TODO**: Does anyone know?

## How do I commit an issue for a non-commiter (ie, someone who does not have write access to the repo)?

**TODO: **Someone should verify this, especially given someone's comments below
that the procedure kept failing?

There are contributions from many partners who do not have write access to our
repo. They still submit changes with 'git cl', but they do not perform the final
push after the LGTM.

A committer must push the patch for the issue. This is how it is supposed to
work:

    git cl patch -b $ISSUE_NUMBER $ISSUE_NUMBER

By convention, the issue number is given first for a new branch name and is used
as the parameter to 'git checkout -b' to create the new branch. It is given a
second time to identify the issue from which to fetch the author's patch set.

Unfortunately, my one patch with this procedure kept failing with:

    subprocess.CalledProcessError: Command 'curl --silent http://codereview.chromium.org/download/issue1019005_53001.diff | sed -e 's|^--- a/|--- |; s|^+++ b/|+++ |' | git apply --index -p0' returned non-zero exit status 1

So I did it manually. First I manually downloaded my patch set from the issue.
Then:

    patch -p1 < $PATCHSET
    git commit -a -s --author="$AUTHOR_NAME <$AUTHOR_EMAIL>"
    git cl push -c "$AUTHOR_NAME <$AUTHOR_EMAIL>"
    # The git cl push above would have worked if I had included the "Review URL" in the comment.  But I did not.  So now I must amend and git push (no cl):
    git commit --amend
    git push origin HEAD:master

The commit text should contain signoff and review information:

    Signed-off-by: $AUTHOR_NAME <$AUTHOR_EMAIL>
    Signed-off-by: $MY_NAME <$MY_EMAIL>
    Review URL: http://codereview.chromium.org/$ISSUE_NUMBER
