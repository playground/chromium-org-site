# Factory Software

The Chromium OS software contains everything you need for a standard device
manufacturing flow - from pre-flash, system imaging, calibration and
qualification, functional tests, finalization to RMA.

The related documents are now moved to [factory
repo](https://chromium.googlesource.com/chromiumos/platform/factory/+/master/README.md).
