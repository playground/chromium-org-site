# How Tos and Troubleshooting

The pages in this section have practical instructions to help you use and modify
a Chromium-based OS. Also see [Chromium OS Developer
Guide](../developer-guide/index.md).
