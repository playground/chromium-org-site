# Poking around your Chrome OS Device

## Intro

This is the page to come to if you want to poke around your Chrome OS device.
These are devices that *shipped from the factory* with Google Chrome OS on them.
For some details about Google Chrome OS and how it differs from Chromium OS, see
the [note in the FAQ about Google Chrome
OS](http://www.chromium.org/chromium-os/chromium-os-faq#TOC-What-s-the-difference-between-Chrom).

This guide is not targeted at people doing Chromium OS development or
building/installing a hand-built Chromium OS image. For that information, see
the [Chromium OS Developer Guide](../developer-guide/index.md). This guide is
instead targeted at people who just want to hack around on their hardware.

## Putting your Chrome OS Device into Developer Mode

All of the instructions on this page require your Chrome OS device to be in
Developer Mode. The way to do this is a little different on each device, so
you'll have to refer to the [developer information for your Chrome OS
device](../developer-information-for-chrome-os-devices/index.md) to figure out
how.

**Caution: Modifications you make to the system are not supported by Google, may cause hardware, software or security issues and may void warranty.**

Note that putting your device into developer mode inherently makes it a little
less secure. Specifically, it makes the "verified boot" that's built-in to your
hardware a little bit more lax, allowing your hardware to run custom
(non-Google-signed) images. It also gives you access to a "root" shell.

The procedure for booting custom images on your Chrome OS device may also be
device-specific.
You can tell that you're in Developer Mode if you see one of these screens when
you turn the device on:

![image](dev.bmp) ![image](meh2.jpg)

### Getting your Chrome OS Device back into Normal Mode

Likewise, the process for restoring your Chrome OS device to normal mode is
likely to be
[device-specific](../developer-information-for-chrome-os-devices/index.md). In
addition, if you've made changes to the SSD image while in developer mode, you
may have to use the [recovery process](http://www.google.com/chromeos/recovery)
to restore your device to the factory condition. However, as long as you don't
crack open the case, you shouldn't be able to do anything that can't be undone
by recovery (software).

## Getting to a command prompt

If you're a Linux hacker, you probably know that Google Chrome OS is built on
top of Linux and you're wondering how you can jailbreak your device so you can
get to a command prompt. It turns out: there's no need. The command prompt is
built in to your device!

IMPORTANT NOTE: Before following these instructions, remember to put your device
into Developer Mode (see above).

### Get the command prompt through VT-2

One way to get the login prompt is through something called VT-2, or "virtual
terminal 2". This is probably familiar to the Linux hackers out there. You can
get to VT-2 by pressing:

\[ Ctrl \] \[ Alt \] \[ => \]

...where the \[ => \] key is the right-arrow key just above the number 3 on your
keyboard.

Once you have the login prompt, you should see a set of instructions telling you
about command-line access. By default, you can login as the `chronos` user with
no password. This includes the ability to do password-less sudo. The
instructions on the screen will tell you how you can set a password. They also
tell you how to disable screen dimming.

The instructions do tell you how to get back to the browser, but because it's so
important, I'll also put it here. Just press:

\[ Ctrl \] \[ Alt \] \[ <= \]

...where the \[ <= \] key is the left-arrow key just above the number 1 on your
keyboard.

**SIDE NOTE**: For the technical-minded, you may realize that the top-rows of
the keyboard on a Chrome OS device are actually treated by Linux as the keys F1
through F10. Thus, the \[ => \] key is actually F2 and the \[ <= \] key is
actually F1.

**SIDE NOTE**: If you're fooling around, you might also notice that kernel
messages show up on VT-8.

### Getting the command prompt through crosh

Because you booted into developer mode, you also have an alternate way to get a
terminal prompt. I'll mention this as well, since the alternate shell is a
little nicer (in the very least, it keeps your screen from dimming on you), even
if it is a little harder to get to. To use this alternate way:

1.  Go through the standard Chrome OS login screen (you'll need to setup a
    network, etc) and get to the web browser. It's OK if you login as guest.
2.  Press \[ Ctrl \] \[ Alt \] \[ T \] to get the
    [crosh](https://chromium.googlesource.com/chromiumos/platform2/+/master/crosh/)
    shell.
3.  Use the `shell` command to get the shell prompt. NOTE: even if you set a
    password for the `chronos` user, you won't need it here (though you still
    need it for `sudo` access)

Note that entering the shell this way doesn't give you all the instructions that
VT-2 does (like how to set your password). You might want to follow the VT-2
steps once just to get the instructions.

If you want to get back to the browser without killing the shell, you can do it
with \[ Alt \] \[ Tab \].

**SIDE NOTE**: You can actually create as many shells as you want. Just hit \[
Ctrl \] \[ Alt \] \[ T \] again and a second shell will be opened. You can \[
Alt \] \[ Tab \] between them.

### Making changes to the filesystem

The Chromium OS rootfs is mounted read-only. In developer mode you can disable the rootfs verification, enabling it to be modified.
**NOTE: If you mount the root filesystem in writeable mode, even if you make no
changes, it will no longer be verifiable and you'll have to use a recovery image
to restore your system when you switch back to normal mode. Auto updates may
also fail until a full payload is downloaded.**
To make your rootfs writable, run this from a shell:

    sudo /usr/share/vboot/bin/make_dev_ssd.sh --remove_rootfs_verification

Then reboot. Your rootfs will be mounted read/write.

### Specifying command line flags for chrome

See [this page](http://www.chromium.org/for-testers/command-line-flags). Note you will need to make the root FS writable; see above.
