# Crash Reporting (Chrome OS System)

*   There's a Chrome OS [Crash Reporting FAQ](faq.md).
*   Getting [Stack Traces](stack-traces-from-chrome-os-crash-dumps.md) from
    crash reports.
*   There's also documentation on how to [Debug a
    Minidump](debugging-a-minidump.md).
*   How [Kernel
    Crashes](../../how-tos-and-troubleshooting/ramoops-in-chromium-os.md) are
    handled.
*   The original [Design Doc](ChromeOSCrashReporting.pdf) ([Google Drive
    version](https://docs.google.com/document/d/1TE1ZXqIzYz-fkbT8xeaRWN7KqxfUnQI3ET014YiRBXs/edit?authkey=CMf2--cG)).
