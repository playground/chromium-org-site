# pydevi2c

Placeholder page for pydevi2c (python bindings for talking to /dev/i2c devices).

See: http://git.chromium.org/gitweb/?p=chromiumos/platform/pydevi2c.git
