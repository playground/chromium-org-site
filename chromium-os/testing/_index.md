# Testing Home

## This is the home page for testing. You should find everything related to writing tests, modifying existing tests, and running tests here. Please feel free to send an email to [chromium-os-dev@chromium.org ](mailto:chromium-os-dev@chromium.org)if you have any questions.

### Unit Tests

We run unit tests using the Portage test FEATURE. For instructions on how to run
and add your own unit tests, follow these
[instructions](adding-unit-tests-to-the-build.md).

### Testing Workflows

Here's a description of some common [developer
workflows](common-testing-workflows.md)

When iterating on a test, you might save some time by using the
[autotest_quickmerge](../../system/errors/NodeNotFound) tool.

### **Testing Components**

These documents discuss test cases and (eventually) test suites.

*   [Power testing](index.md)
*   [Touch Firmware Tests](../../for-testers/touch-firmware-tests/index.md)
*   [FAFT](../../for-testers/faft/index.md) (Fully Automated Firmware Tests)

## Autotest

[Autotest User Doc](autotest-user-doc/index.md) describes the integration of
autotest into the build system, along with FAQ and specific descriptions of
various tasks.

This has additional info on how to [run the smoke test suite on a Virtual
Machine](running-smoke-suite-on-a-vm-image.md).

Before you check in a change, you should also do [a trybot run
locally](../build/local-trybot-documentation.md), which will run unit tests and
smoke tests in the same way that the buildbots do.

*   [AFE RPC Infrastructure](afe-rpc-infrastructure.md)
*   [Autoserv Packaging](autoserv-packaging.md)
*   [Collecting Stats for Graphite](collecting-stats-for-graphite.md)
*   [GS Offloader](gs-offloader.md)
*   [Autotest Keyvals](autotest-keyvals/index.md)
*   [Performance Tests and Dashboard](perf-data.md)

### Codelabs

*   [Server Side tests codelab](test-code-labs/server-side-test/index.md)
*   [Client side codelab](test-code-labs/autotest-client-tests/index.md)
*   [Dynamic suite codelab](test-code-labs/dynamic-suite-codelab.md)

### Writing Tests & Suites

*   [Autotest Best
    Practices](https://chromium.googlesource.com/chromiumos/third_party/autotest/+/refs/heads/master/docs/best-practices.md)
    - Start here when writing any code for Autotest.
*   [Autotest Developer FAQ](autotest-developer-faq/index.md) - Working on test
    cases within Autotest.
*   [Autotest Design Patterns](autotest-design-patterns.md) - Recipes for
    completing specific tasks in Autotest
*   [Existing Autotest Utilities](existing-autotest-utilities.md) - Coming Soon!
*   [Using Test Suites](test-suites.md) - How to write and modify Test Suites

### MobLab

MobLab is a self-contained automated test environment running on a Chromebox.

*   [MobLab Home](moblab/index.md)

### Related Links

*   [Slides](https://docs.google.com/present/edit?id=0AXd3jph7Jzc6ZGQ4NjI1el8waGszOXZwZmI)
    for testing in Chromium OS
*   [Autotest API docs](https://github.com/autotest/autotest/wiki/TestDeveloper)

### Design Docs

*   [Dynamic Test Suite Implementation](dynamic-test-suites.md)
*   [Test Dependencies in Dynamic
    Suites](test-dependencies-in-dynamic-suites.md)
*   [Suite Scheduler AKA Test Scheduler V2](suite_scheduler-1.md)

## Servo

Servo is a debug board for Chromium OS test and development. It can connect to
most Chrome devices through a debug header on the mainboard.

*   [Servo v2](../servo/index.md)

## Chamelium

*   [Chamelium](chamelium/index.md) automates external display testing across
    VGA, HDMI, and DisplayPort (DP).
*   [Chamelium with audio board](chamelium-audio-board/index.md) automate audio
    testing across 3.5mm headphone/mic, internal speaker, internal microphone,
    HDMI, Bluetooth A2DP/HSP, USB audio.
*   [Chamelium capturing and streaming tool](chamelium-audio-streaming/index.md)
    to monitor audio underruns.
