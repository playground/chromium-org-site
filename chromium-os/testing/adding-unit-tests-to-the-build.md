# Chromium OS Unit Testing

## Resources for writing unit tests

[Introduction: Why Google C++ Testing
Framework?](https://github.com/google/googletest/blob/master/googletest/docs/Primer.md)

[GoogleTest
FAQ](https://github.com/google/googletest/blob/master/googletest/docs/FAQ.md)

## Running unit tests

We run unit tests as part of the build process using chromite's
[cros_run_unit_tests](https://chromium.googlesource.com/chromiumos/chromite/+/master/scripts/cros_run_unit_tests.py).
In a nutshell, this script uses the portage method of building and running unit
tests using `FEATURES="test"`. To run all the Chromium OS unit tests, you can
run `cros_run_unit_tests` directly from **within your chroot**. If you want to
run just the unit tests for specific packages, you can use
`cro``s_run_unit_tests` --packages "space delimited list of portage package
names". See cros_run_unit_tests --help for more information about other options.

As an example, let's say you just want to run the unit tests for the metrics
package. To do so, you can run:

    cros_run_unit_tests --board ${BOARD} --packages "metrics"

## Running a single test

For platform2 packages, see the [platform2 testing
section](../getting-started-with-platform2.md).

For other packages that use gtest:

    GTEST_ARGS="--gtest_filter=FreeDiskSpaceTest.*" cros_run_unit_tests --board ${BOARD} --packages google-breakpad

## Adding unit tests

To add unit tests to your packages, create a src_test stanza in your ebuild that
builds and runs the unit tests (it is fine to have them build in the src_compile
stage as well). See [the metrics
ebuild](https://chromium.googlesource.com/chromiumos/overlays/chromiumos-overlay/+/master/chromeos-base/metrics/metrics-9999.ebuild)
as a good example of adding your unit test.

*   Portage
    [documentation](http://devmanual.gentoo.org/ebuild-writing/functions/src_test/index.html)
    of src_test (didn't find it that helpful though).

## How to Blacklist a package from running its unit tests

It's discouraged to have unit tests that need to be blacklisted. However, if you
really need to black list a package from running its unit tests as part of the
build, use either` RESTRCT="test"` in your ebuild or add it to the unit tests
blacklist in `chromite.lib.portage_util`.

## Non-native architectures (e.g. QEMU)

Platform2 supports running unittests via qemu for non-native architectures (low
level details can be found in [this doc](qemu-unittests.md)). The good news is
that the process is the same as described above! Simply use
`cros_run_unit_tests` for your board and specify the packages you want to run.

### Caveats

Sometimes qemu is not able to support your unittest due to using functionality
not yet implemented. If you see errors like "qemu: unknown syscall: xxx", you'll
need to filter that test (see below), and you should [file a
bug](http://crbug.com/new) so we can update qemu.

Since qemu only works when run inside the chroot, only ebuilds that use the
platform.eclass are supported currently.

## Filtering tests in ebuilds

### Packages using platform.eclass

Sometimes a test is flaky or requires special consideration before it'll work.
You can do this by using the existing gtest filtering logic. For packages using
platform.eclass, simply pass it as an argument to platform_test:

    platform_pkg_test() {
        local gtest_filter=""
        gtest_filter+="DiskManagerTest.*"
        platform_test "run" "${OUT}/some_testrunner" "0" "${gtest_filter}"
    }

If you want to disable the tests for non-native arches, update the global knob:

    PLATFORM_NATIVE_TEST="yes"

### Other Packages

If you want to filter gtests, you'll need to export the relevant gtest variables
directly in your src_test function.

If you want to disable the tests for non-native arches, write your src_test like
so:

    src_test() {
        if ! use x86 && ! use amd64; then
            ewarn "Skipping unittests for non-native arches"
            return
        fi
        ... existing test logic ...
    }

## Special Considerations

*   *By default, cros_run_unit_tests will only use FEATURES="test" on packages
    that:*
    *   *exist in the chromeos-base category*
    *   *are in the dependency tree of virtual/target-os*
    *   *have a src_test function*
