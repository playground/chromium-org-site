# MobLab FAQ

This page has answers to frequently asked questions about setting up and running
MobLab.

#### Q. Can I access the MobLab server (for example, to trigger a job) from another machine on the network?

A. Yes, after setting up SSL keys on that machine and on the MobLab server, you
can trigger and manage jobs remotely.

#### Q. Can I access the MobLab server from a host (a machine under test)?

A. Yes, this is another way to access the MobLab server. Since host and server
are on the same subnet no SSL keys are needed. However, proceed with caution to
avoid contaminating test results.

#### Q. Can I mix and match hardware platforms for hosts on the test subnet?

A. Yes, you may connect different DUT/hosts to one MobLab unit. When running
hostless tests (all "suites" are run hostlessly), during "Create Job" step, if
you specify "Image URL/Build" by pointing to a particular type of image for a
particular type of DUT, Moblab will automatically only push subtests to
corresponding DUT based on "Image URL/Build" type.

#### Q. Where can I download the latest MobLab recovery image?

A. MobLab recovery images are publicly shared at
gs://chromeos-moblab-public/moblab-images. Please refer to the [release
notes](moblab/releasenotes/index.md) to find out the latest release.

#### Q. How do I immaculate MobLab to restart from the beginning?

A. Powerwash is a typical solution to clean up your MobLab image. Please
remember that it will clean up all your local changes including boto key,
shadow_config.ini, etc.

1. Enter into VT2

2. Login as root

3. # echo 'fast safe' > /mnt/stateful_partition/factory_install_reset

4. # reboot

#### Q. How do I join the MobLab mailing list?

A. MobLab has a public discussion group, moblab-discuss@chromium.org. Please use
this
[link](https://groups.google.com/a/chromium.org/forum/#!forum/moblab-discuss) to
join this public group.

**Q. How do I access the Google cloud storage bucket from a web browser?**

A. Browser-based access of the storage bucket is by using the URL:
https://console.cloud.google.com/storage/browser/chromeos-moblab-<partner>
instead of gs://chromeos-moblab-<partner> which is the path from MobLab.

#### **Q. Mob\*Monitor is complaining about "devserver is unhealthy." How do i fix this?**

A. [First check "Moblab Setup"](moblab/setup/index.md) on your upper right
corner of AFE, it does an validation of your boto key and cloud storage bucket
location are a match. To be double sure, you can also [ssh into your moblab
unit](moblab/setup/index.md) as "moblab" user, and then run "gsutil ls
gs://<your bucket name>", if it displays the correct content as you would see at
https://console.cloud.google.com/storage/browser/<your bucket name> from a
browser, then your boto key is setup correctly. and you may ignore Mob\*Monitor
complain in this case.

**Q. Mob\*Monitor is complaining about xxx service not starting, how do i fix
this?**

A. try reboot moblab unit and see if you get a clearner Mob\*Monitor analysis.
if not, click "collect Logs" in Mob\*monitor screen, and upload the logs to a
google drive location and share it with your Google contact to investigate
further.

#### **Q. I reboot my MobLab, and now i can't get to the Moblab AFE from a browser, and looks like Apache is not up, what is happening?**

A. One most common cause could be you are out of storage space. To confirm, go
to Mob\*Monitor http://localhost:9991 from your moblab unit, or http://<your
moblab ip>:9991 from a remote machine on the same network. If you are out of
space, Mob\*Monitor will tell you.

**Q. Mob\*Monitor says "Moblab has less than 10 percent free disk space. Please
free space to ensure proper functioning of your moblab." what should i do?**

A: Moblab startup scripts look for a drive with at least 32 GB free or an
external USB labelled MOBLAB-STORAGE. So you can either ssh into your moblab and
clean up /usr/local/moblab/storage/results directory and reboot. or attach an
external storage labelled MOBLAB-STORAGE and reboot.

#### **Q. Why does my Moblab run out of space so often?**

A: Moblab offloads test results every 24 hours to google cloud storage. If your
network connectivity is down, the results will accumulate and if you are running
long running tests, the results will build up and eat up your storage space.

#### **Q. I just received a brand new Moblab unit and trying to recover it using the latest moblab image specified by [release notes](moblab/releasenotes/index.md). But when boot into recovery screen and plug in my usb-stick with the moblab recovery image, i'm getting "The device you inserted does not contain Chrome OS" message.**

A: Moblab image is a modification of Guado (ASUS Chromebox). If the unit you
purchased come with Guado stock image instead of Guado_moblab, you will
encounter the error you specified when you try to recovery a guado unit to
guado_moblab due to the key difference. The work around is the following:

1. prepare the recovery usb media as you have done using latest moblab image
specified in **[release notes](moblab/releasenotes/index.md)**

2. on your moblab unit, with a working OS(likely guado, not guado_moblab),
switch to dev mode, and in vt2 command line, run the following command:

$>crossystem dev_boot_usb=1

3. normal boot your moblab while your usb with the image you prepared i step #1

4. Ctrl+U at "Verification is off" screen

5. this should have your moblab boot from the USB

6. goto VT2 again, and type the following command at the command line

$>chromeos-install

**Q:My DUT kept on beeping during startup (when auto start from "verification is
off" screen), it is really annoying, can i silence the DUT?**

A: go to VT2 (ctrl+alt+F2), log in as root, and type the following in the
command line will silence the DUT

$ /usr/share/vboot/bin/set_gbb_flags.sh 0x19

#### **Q: I've kicked off a suite of tests. I'm not sure if anything is happening? How do I know something is running?**

A: Go to the Job List page in AFE and click on Running Jobs. This will give you
a list of jobs that are currently running. Also, make sure to enable the Auto
Refresh check box, so that AFE page is refreshed once every 5 seconds.
