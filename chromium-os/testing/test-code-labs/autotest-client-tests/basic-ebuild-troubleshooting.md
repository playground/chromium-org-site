# Basic Ebuild Troubleshooting

### This is a brief explanation of how one might trouble shoot local emerge/ebuild failures. It is in no way comprehensive, and is only meant as an introduction to the topic:

**Emerge Errors During build_image:**

When following the [Chromium OS Developer
Guide](http://dev.chromium.org/chromium-os/developer-guide) emerge errors can be
encountered for missing ebuild. A error that can be encountered when running
build_image is:

emerge: there are no ebuilds to satisfy XYZ

Where XYZ is a library or other missing package.

This can be resolved by running:

emerge-$BOARD XYZ

Then recalling build_image:

./build_image --board=${BOARD} --noenable_rootfs_verification dev

References:

\[1\] [Autotest ebuild
documentation](http://www.chromium.org/chromium-os/testing/autotest-user-doc)

\[2\][ Chromite
documentation](http://www.chromium.org/chromium-os/developer-guide/chromite-shell-quick-start)

From within the chroot, try running the a new test through run_remote_tests, as
described in the codelab on autotest client tests, without actually adding it to
an ebuild. This might give you an error about not being able to find
kernel_HdParmBasic’s control file.

To resolve this, from within chroot:

1. Find the package with autotest tests:

emerge --search autotest | grep tests

2. Dry emerge it and look for your test in the output:

emerge-lumpy -pv chromeos-base/autotest-tests

Two things to note:

a. The first line will say something like:
“chromeos-base/autotest-tests-0.0.1-r3531“

This means it’s emerging the r3531 ebuild.

Since we would like it to use our local bits, we need to cros_workon the
package.

b. You will not find kernel_HdParmBasic in the output.

3. cros_workon the appropriate package:

cros_workon --board=<board name> start <package name>

eg: cros_workon --board=lumpy start autotest-tests

emerge the package: emerge-<board name> <package name>

eg: emerge-lumpy -pv chromeos-base/autotest-tests

Two things to note:

a. The first line will say something like:

“chromeos-base/autotest-tests-9999”

This is good, it means it’s pulling local bits.

b. You still will not find kernel_HdParmBasic in the output.

This is because your new test hasn’t been added to the ebuild yet.

4. Edit the ebuild to include your test:

Find the ebuild file: find -iname “autotest-tests-9999.ebuild” from
src/third_party/.

a. It will most probably point you to:

third_party/chromiumos-overlay/chromeos-base/autotest-tests/autotest-tests-9999.ebuild.

b. open this file and add “+test_kernel_HdParmBasic” in the IUSE_TESTS section.

5. Emerge the new folder:

a. Make sure you’re cros_working on the right packages:

cros_workon --board=lumpy list

should show autotest_tests

b. Make sure the emerge will touch kernel_HdParmBasic:

emerge-lumpy -pv chromeos-base/autotest-tests | grep kernel_HdParmBasic

c. Actual emerge:

emerge-lumpy chromeos-base/autotest-tests

6. Check the staging area for your new test:

eg: ls /build/lumpy/usr/local/autotest/client/site_tests/kernel_HdParmBasic

7. Re-run the run_remote command and look for the results directory:

you should see a line like:

>>> Details stored under /tmp/run_remote_tests.KTQ4
Alternatively, you can specify your own results directory using
‘--results_dir_root’.

Some common ebuilds to cros_workon and emerge:

*   autotest-all (Meta ebuild for all packages providing tests),
*   autotest-factory (Autotest Factory tests),
*   autotest-chrome (Autotest tests that require chrome_test or pyauto deps),
*   autotest-tests (Pure Autotest tests),
*   autotest (Autotest scripts and tools).

If you have come this far, you may also be interested in reading the autotest
client tests [codelab](index.md).
