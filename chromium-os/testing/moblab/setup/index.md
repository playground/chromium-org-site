# Setting Up MobLab

**Notice: **Please refer to the [Mob Monitor](../mob-monitor/index.md) if you
have issues setting up your MobLab device.

This document is designed to walk you through setting up a MobLab configuration
at your site.

## Before Setting Up MobLab

Please see the list of [Required Hardware](../overview-of-moblab/index.md) for
specifications.

### MobLab requirements

*   External Internet access for automatic system updates and for archiving log
    files.
*   Proxy support may be necessary depending on the network setup.
*   SSH keys, for remote access to the MobLab device.
*   Access to a private Google Cloud Storage bucket for downloading images (your
    Google representative will set this up for you)

### Peripherals required

*   Monitor
*   Display cable (DVI or DP)
*   USB keyboard and mouse
*   Ethernet switch or hub (optional - only needed with more than 1 DUT)
*   An Ethernet cable for each device to test, plus one for MobLab
*   An USB Ethernet adapter (2.0 or 3.0) for each device to test, plus two for
    MobLab (see [Required Hardware](../overview-of-moblab/index.md) for
    specifications).
*   One or more Chrome devices to test

## Setting up MobLab

This section contains instructions for the following tasks:

*   Correctly connect the different parts of MobLab
*   Obtain the IP address of each device
*   Ensure all the Chrome OS devices should be running in developer mode, and**
    they MUST have test image installed** (not normal image from recovery).
*   Set up the boto key on MobLab
*   Connecting and booting MobLab

![image](image01.png)

![image](image00.png)

1.  Connect the devices according to one of the diagrams below depending on how
    many DUTs are being tested.

    ![image](image03.png)

    ![image](image02.png)

2.  Connect a USB keyboard to the MobLab for the initial setup. You can remove
    the keyboard after setup is complete.
3.  To boot up MobLab, press the backlit power button on the front panel of the
    Chromebox. In the Chrome OS welcome screen, verify that Ethernet is selected
    as the type of network by clicking on the bottom right corner. On the menu,
    it should show “Connected to Ethernet”.
4.  Either log in or click browse as a Guest.
5.  Verify that the MobLab device (Chromebox) and all hosts (devices under test
    (DUTs)) are running the correct images. The MobLab device must be running
    the MobLab server and hosts** must have test images installed**. Partners
    can access test images
    [here](https://www.google.com/chromeos/partner/fe/#release). For
    instructions on installing Chrome OS, see the [Chromium OS Developer
    Guide](http://www.chromium.org/chromium-os/developer-guide).
6.  Verify that all the devices are running in developer mode. For existing
    devices, instructions can be found
    [here](http://www.chromium.org/chromium-os/developer-information-for-chrome-os-devices).
    On Chromeboxes, there is a physical switch on the back of the device. On
    most Chromebooks, Developer mode is enabled in the following manner:
    1.  Invoke Recovery mode: Hold down the ESC and Refresh (F3) keys and press
        the Power button.
    2.  To enter Dev-mode, at the Recovery screen press Ctrl-D (there's no
        prompt - you have to know to do it). It will ask you to confirm, then
        reboot into dev-mode.
7.  Ensure that the MobLab device is connected and obtain its IP address (you
    will need the IP address of the MobLab for remotely accessing it later).
    1.  At the MobLab login screen, use the following command to access the
        developer console of MobLab: `Ctrl+Alt+F2`
    2.  You will be prompted for localhost login. Type “root”. You should not be
        prompted for a password.
    3.  Type the following command to get the IP address: `ifconfig`
    4.  You will get output with several IP addresses. The IP address of the
        MobLab is the IP address that is NOT one of the following.

127.0.0.1 ← this is the IP address of the local host
192.168.231.XXX ← this is the IP addresses of the DUT inside the subnet
The remaining IP address should be that of the MobLab.

    1.  Exit the developer console using the following command: `Ctrl+alt+F1`

1.  Ensure each of the DUTs is connected and obtain their IP addresses. For each
    DUT, use the steps in the previous step to enter VT2 and get the IP address.
    1.  Note that the IP address of the DUT is as follows:

192.168.231.XXX ← IP addresses of the DUT inside the subnet

1.  Insert a USB stick (at least 64 GB) that has been formatted according to the
    instructions in section, "Formatting External Storage for MobLab" below.

### Setting up the boto key for partners

Access to the Google Cloud Storage bucket is enabled by a boto key. To install
the key on the MobLab server, you upload it through the Autotest Front End
(AFE). You obtain a boto key from your Google contact.

To install the key:

1.  Get the boto file from your Google contact

1.  Access the AFE from any networked machine by going to
    http://<moblab_server_IPaddress> in a web browser, where
    moblab_server_IPaddress is the external IP address of the MobLab server
    (assigned by your ISP).
2.  In the top right hand corner of the screen, click on “MobLab Setup” to
    access the “MobLab Setup” page.

![image](Selection_971.png)

1.  Click on “Configure” Button to kick off the configuration wizard.
2.  First, validate network information and connectivity.

    ![image](Selection_972.png)

3.  Click on "Next" to get to Boto Key and Storage Bucket and Result Bucket URL
    configuration. You can check "Use Existing Boto File on Moblab Device" to
    keep using existing settings. Configure image_storage_server with your
    project bucket(gs://chromeos-moblab-<project>/) or partner
    bucket(gs://chromeos-moblab-<partner>/). Please be sure that the bucket
    string has "/" at the end of the string. Note: Browser-based access of the
    storage bucket is by using the URL:
    https://console.cloud.google.com/storage/browser/chromeos-moblab-<partner>

    ![image](Selection_973.png)

5. Click on "Finish" to submit your result. Configuration Wizard will
automatically reboot the server to save the new settings.

### Setting up the boto key for Google Users

If you are a Google user, you should have a boto key which can be accessed at
the location below. Follow the rest of the instructions above.

*   Copy your boto key from ~/.boto on your machine or in your source tree at
    src/private-overlays/chromeos-overlay/googlestorage_account.boto.

### Formatting external storage for MobLab

A USB drive or external hard drive provides a local staging area for test images
downloaded from Google Cloud Storage. From the staging area, MobLab can then
push the images to hosts. To recognize local storage MobLab needs an ext4
filesystem and a label. When running the commands in this section:

*   the memory must be unpartitioned. For the device path specify a directory
    such as /dev/sdc, not a pre-existing partition like /dev/sdc1 or /dev/sdc2.
*   the device must have the exact label MOBLAB-STORAGE

To format a USB drive or hard drive for use with MobLab:

*   1. Connect the memory stick or hard drive to an open USB port on the MobLab
    device.
*   2. Run the following commands from either the MobLab server or a Linux
    machine on the corporate network:

    *   Run $ sudo mkfs.ext4 <device_path>
    *   Run $ sudo e2label <device_path> MOBLAB-STORAGE
*   3. Reboot the MobLab server.

## Appendix

### Setting up SSH Access from a Remote Machine

This process will walk you through how to set up ssh access into MobLab (i.e.
for boto key access on the MobLab)

1.  From a remote machine with access to the Chrome OS source tree, download the
    ssh keys from the Chromium git repository at the following link:
    https://chromium.googlesource.com/chromiumos/chromite/+/master/ssh_keys
2.  Copy both keys into ~/.ssh/ on the remote machine where you want to enable
    SSH access to the MobLab server.
3.  Edit the SSH configuration file (~/.ssh/config) on the remote device as
    follows:

    Host <moblab_server_IPaddress>
    CheckHostIP no
    StrictHostKeyChecking no
    IdentityFile %d/.ssh/testing_rsa
    Protocol 2

1.  Save the file.
2.  Test ssh access using the following commands:

    $ ssh moblab@<moblab_server_IPaddress>
    $ exit

### Commands for copying the boto key to MobLab from a local machine

1.  From a machine where you have ssh access to MobLab (configured as above),
    run the following command to add the boto key.

` $ scp <botofilepath> moblab@[moblab_server_IPaddress]:/home/moblab/.boto`

### Configuring Additional MobLab Settings

1.  MobLab settings can be adjusted by going to the “MobLab Setup” page accessed
    by clicking in the top right corner of the AFE. Note that saving any
    settings will reboot the MobLab.
