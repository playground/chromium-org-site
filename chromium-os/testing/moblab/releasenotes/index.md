# MobLab Release Notes

## June 20, 2017: Release 9460.65.0

## Moblab has been updated to 9460.65.0 Here are important features/bug fixes
   launched in this release:

## *   The Run Suite UI now allows CTS_N and GTS suites to be run but with a
       filtered list of autotest tests. By default all jobs are run but if you
       supply a comma separated list of test only those tests will be run in the
       suite. When specifying the list of tests to run do not include the
       cheets_CTS_N or cheets_GTS prefix

## *   An example list of test cases to run for CTS_N: arm.CtsAppTestCases,
       x86.CtsAppTestCases, x86.CtsCameraTestCases

## *   An example list of test cases to run for GTS: GtsGmscoreHostTestCases,
       GtsFeaturesTestCases

## *   Setup for Partner Console notifications is now automatic. Previously if
       you were enabled for the Chrome OS Partner Console you had to upload your
       credentials and change some "Config Settings" in the Moblab UI. This
       process has been automated. When you perform a setup now, your moblab
       credentials will be uploaded from your bucket and settings changes are
       applied automatically.

## *   There have been some major changes to how the apache server runs on
       moblab. These changes should reduce the amount of memory consumed and in
       general make things faster. We still require you to have 16GB RAM if you
       are running CTS/GTS however some qual use cases should work with just 4GB
       RAM.

## *   Multiple bug fixes have been made to improve Moblab’s stability &
       performance

## *   Testing has shown that after the upgrade some users see blank page in the
       web browser, if you experience any issues like this, please try holding
       the shift button and reload of the page on chrome browser.

## We recommend using auto-update for this and all future releases, but if that
   doesn’t work, please download the new release from [this
   link](https://storage.googleapis.com/chromeos-moblab-public/moblab-images/stable-channel%252Fguado-moblab%252F9460.65.0%252Fchromeos_9460.65.0_guado-moblab_recovery_stable-channel_premp.bin.zip)

## May 15, 2017: Release 9334.68.0

## Moblab has been updated to 9334.68.0 Here are important features/bug fixes
   launched in this release:

## *   For all new installations and powerwashed devices the config setting:
       gs_offloader_limit_file_count will be set to true. Enabling this flag
       will tar and compress test result directories that contain more than 500
       files. This particularly affects CTS / GTS results where we see reduction
       in amount of upload network bandwidth used.
## *   If you want to use this feature without doing a full powerwash, please
       change the gs_offloader_limit_file_count in "Config Settings" of the ui
       moblab_setup and set the value to true.

## *   Multiple bug fixes have been made to improve Moblab’s stability &
       performance

## We recommend using auto-update for this and all future releases, but if that
   doesn’t work, please download the new release from [this
   link](https://storage.googleapis.com/chromeos-moblab-public/moblab-images/chromeos_9334.68.0_guado-moblab_recovery_stable-channel_premp.bin)

## April 14, 2017: Release 9202.70.0

## Moblab has been updated to 9202.70.0. Here are important features/bug fixes
   launched in this release:

## *   Partner building devices with Android N container in them can now run CTS
       tests using a Moblab
## *   Multiple bug fixes have been made to improve Moblab’s stability &
       performance

## We recommend using auto-update for this and all future releases, but if that
   doesn’t work, please download the new release from [this
   link](https://storage.googleapis.com/chromeos-moblab-public/moblab-images/stable-channel%252Fguado-moblab%252F9202.70.0%252Fchromeos_9202.70.0_guado-moblab_recovery_stable-channel_premp.bin.zip)

## December 23, 2016: Release 8872.67.0

## MobLab has been updated to 8872.67.0. Here are some of the important features that launched in this release:

## New UI to run test suites A simplified UI to run test suites is now available to moblab users. This UI needs the DUT board, DUT build and suite name (dropdown selection) to start running a test suite. The following screenshot shows the new UI

## Running CTS test suite Moblab users can now be able to run CTS test suite on eligible DUTs. Please refer to the above screenshot on initiating the CTS test. It is recommended that the CTS test suite is run across 3-5 DUTs which would take about 10 hours to finish running the suite.

## We recommend using auto-update for this and all future releases, but if that doesn’t work, please download the new release from this link: <https://storage.googleapis.com/chromeos-moblab-public/moblab-images/stable-channel%252Fguado-moblab%252F8872.67.0%252Fchromeos_8872.67.0_guado-moblab_recovery_stable-channel_premp.bin.zip>

## Nov 14, 2016: Release 8743.83.0

## MobLab has been updated to 8743.83.0

## *   In order to improve security on MobLab, users can no longer ssh into
       MobLab as a root user. Instead, in the rare case that users need this
       access (for example, to clean out Apache System logs) please use the
       moblab@ip address and use sudo.
## *   This release also contains minor bug fixes.

## We recommend using auto-update for this and all future releases, but if that doesn’t work, please download the new release from this link: <https://storage.googleapis.com/chromeos-moblab-public/moblab-images/stable-channel%252Fguado-moblab%252F8743.83.0%252Fchromeos_8743.83.0_guado-moblab_recovery_stable-channel_premp.bin.zip>

## Oct 13, 2016: Release 8530.95.0

## MobLab has been updated to 8530.95.0 This release contains minor bug fixes.

## We recommend using auto-update for this and all future releases, but if that doesn’t work, you can also download the new release from this link:

## <https://storage.googleapis.com/chromeos-moblab-public/moblab-images/stable-channel%252Fguado-moblab%252F8530.95.0%252FChromeOS-test-R53-8530.95.0-guado-moblab.tar.xz>

## Aug 30, 2016: Release 8350.73.0

## MobLab has been updated to 8350.73.0

## This release integrates a better way to view test results using MobLab. You
   can now see test results by platform, board, test suite, etc. To use this
   feature:

## 1.  Click "WMatrix" on the top right side.
## 2.  Select which view you'd like to see (recommended: "Suite List" from
       "Quick Links" section defined on the top left side, this will list all
       wmatrix dashboard based on test suite you've run, e.g.
       memory_qual_quick). Note: Not all test suites are showing due to a bug,
       this will be updated in the next release.
## 3.  Review your test results from this new view.

## This release also contains additional minor bug fixes.

## We recommend using auto-update for this and all future releases, but if that doesn’t work, you can also download the new release from this link: <https://storage.googleapis.com/chromeos-moblab-public/moblab-images/stable-channel%252Fguado-moblab%252F8350.73.0%252Fchromeos_8350.73.0_guado-moblab_recovery_stable-channel_premp.zip>

## June 17, 2016: Release 8172.52.0

## MobLab has been updated to 8172.52.0. New features include:

## *   Please look for the “Config Wizard” tab under MobLab Setup to see this
       feature.

## *   A new wizard has been added to streamline the most commonly needed
       operations for MobLab setup and configuration. When setting up a new
       MobLab, you can click “configure” and you’ll be guided through the needed
       steps to verify internet connection, set up the boto key and enter the
       image storage bucket URL.

## We recommend using auto-update for this and all future releases, but if that doesn’t work, you can also download the new release from this link:

## <https://storage.googleapis.com/chromeos-moblab-public/moblab-images/stable-channel-guado-moblab-8172.52.0-chromeos_8172.52.0_guado-moblab_recovery_stable-channel_premp.tar.xz>

## April 21, 2016 Release 7978.59.0

## Moblab has been updated to 7978.59.0 New features include:

## *   This update (and all future updates) will be available via auto-update!
       Please make sure that your device is on stable channel in order to
       receive these updates. You can change the channel by going to "Help >
       About Chrome OS> More info". Please see [this help
       article](https://support.google.com/chromebook/answer/1086915?) for more
       assistance.

## *   Save time by configuring recurring jobs from hostless suite jobs from
       Moblab Frontend (AFE)

## *   A link to the user's image bucket is available directly from the AFE
       configuration settings, making it easier to check the Cloud Storage
       bucket for troubleshooting.

## Download link (if auto-update doesn't work for you) <https://storage.googleapis.com/chromeos-moblab-public/moblab-images/chromeos_7978.59.0_guado-moblab_recovery_stable-channel_premp.bin.tar>

## ## Wednesday, Sept 30, 2015: Release 7466.0.0

## Moblab has been updated to 7466.0.0 New features:

## *   MobMonitor has been released to easily setup and maintain the Moblab
       device in good working condition. Learn more about how to use MobMonitor[
       here](https://www.chromium.org/chromium-os/testing/moblab/mob-monitor)

## *   Enhanced Functional Testing: Using Moblab, our partners can take
       advantage of the same functional tests (such as Video) which we use for
       daily release testing. Learn more [here](../partner_testing.md).

## *   Save time when running FAFT tests! servod will always be started
       automatically when running FAFT tests as long as servo v2 is installed

## Download link:

## <https://storage.googleapis.com/chromeos-moblab-public/moblab-images/chromeos_7466.0.0_guado-moblab_recovery_dev-channel_premp.bin.tbz2>

## Thursday, May 21, 2015: Release 7073.0.0

MobLab has been updated to 7073.0.0. New features:

*   Increased system stability. A number of changes have been added to ensure
    that MobLab has limits on the amount of CPU/memory used by Apache and MySQL.
*   A revamped Create Job Page:
    *   Automatic toggling of “Hostless (Suite) Job” depending on the control
        file(s) selected for a given test run.
    *   Less important options are hidden under “Advanced Options” drop down
        menu.
*   For partners who have enabled automatic uploading to Google Storage, test
    logs now offload to Google Storage after 1 day (versus 3 before) to allow
    for quicker troubleshooting turnaround with Google’s Partner Engineering
    Department.

## Friday, March 13, 2015: Release 6848.0.0

MobLab has been updated to 6848.0.0. There are two major features that come with
this update.

*   Servo V2 support which provide the following capabilities:
    *   Allow MobLab users to run FAFT (fully automated firmware tests) on their
        Chromebooks to validate firmware
*   A fully auto-update-able Panther (Asus Chromebox) MobLab image.

## Monday, February 9, 2015: Release 6715.0.0

MobLab has been updated to 6715.0.0 for all devices. This update comes with a
major feature release: `cros stage`. This new feature will allow MobLab users to
test their own locally built CrOS images, eliminating the need for changes to be
submitted to the main ChromeOS tree prior to testing. Instructions to use the
feature are located [here](../../../../system/errors/NodeNotFound).

## Wednesday, Dec 17, 2014: Release 6575.0.0

*   There is also now a MobLab image that can be installed onto the Asus
    Chromebox (board-name: panther). MobLabs will be supported on both the
    Samsung Series 3 Chromeboxes (board-name: stumpy) as well as the Asus
    Chromebox.
*   We have resolved an issue with very large gs_offloader logs taking up too
    much space. This mostly affected internal MobLab users.

## Thursday, Nov 20, 2014: Release 6482.0.0

MobLab has been updated to 6482.0.0 for all devices.

*   MobLab now have the ability to automatically upload test logs to partner
    Google Storage buckets. By default, this feature is automatically enabled.
    Test logs will remain on the device for 72 hours before automatically being
    uploaded to the GS bucket. To disable this feature, go to the MobLab Setup
    page, and change the config setting ‘gs_offloading_enabled’ under the CROS
    section to ‘false’. For more detailed information, refer to [the
    moblab-discuss
    forum](https://groups.google.com/a/chromium.org/forum/#!forum/moblab-discuss).

## Thursday, October 9, 2014: Release 6343.0.0

MobLab has been updated to 6343.0.0 for all devices. This build contains a patch
for the Shellshock vulnerability as well as several features. They are listed
below:

*   While MobLab's Apache Server does not shell out to Bash, Chromium OS's Bash
    has been patched thus preventing any unforeseen Shellshock attacks on MobLab
    devices.
*   MobLab test results have been moved to be stored on the external USB drive.
    This allows for a greater number of tests to be run without running out of
    space on the Moblab device. This fix increases stability and reduces the
    need for powerwashing.
*   Simplified versions of common MobLab commands run from the shell. For
    details of the code, please visit [this
    site](https://chromium-review.googlesource.com/#/c/218439/8/overlay-variant-stumpy-moblab/chromeos-base/chromeos-bsp-moblab/files/bash_profile).

If you find a new issue, please let us know by [filing a
bug](https://code.google.com/p/chromium/issues/entry?template=Moblab%20Issues).

## Tuesday, September 2, 2014: Release 6209.0.0

MobLab has been updated to 6209.0.0 for all devices. This build contains several
new features outlined below:

*   New MobLab setup page allowing users to make changes from the AFE. It can be
    accessed from the top right hand corner of the AFE by clicking “MobLab
    Setup”.
*   Ability to upload the boto key from the AFE (from the configuration page).
*   Additional test suites for partners partaking in memory and storage
    qualification.

### Screenshots

### MobLab Setup Page:

![image](Screenshot-from-2014-09-02-17_21_38.png)

## Monday, July 28, 2014: Release 6092.0.0

MobLab has been updated to 6092.0.0 for all devices. This build contains bug
fixes to improve MobLab's stability as well as a number of improvements.

*   Improvements to the Autotest Front End(AFE)
    *   New UI
    *   Faster
    *   Addition of Tool Tip Documentation
    *   Auto refresh option on each page
    *   Ability to manage host attributes (Admin Interface page)
    *   Ability to pass in arguments and filter test names while creating jobs
        (Create Jobs page)
    *   Ability to abort special tasks (View Host page)
    *   Ability to filter suite and individual jobs (Job List Page)
    *   Display child jobs on parent jobs (Job List Page)
    *   Improvements to MobLab stability and disk space management

If you find a new issue, please let us know by [filing a
bug](https://code.google.com/p/chromium/issues/entry?template=Moblab%20Issues).
