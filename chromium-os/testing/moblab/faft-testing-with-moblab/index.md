# Running FAFT with MobLab

## Background

## FAFT (Fully Automated Firmware Tests) is a collection of tests that exercise and verify low level capabilities of Chrome OS. The features tested by FAFT are implemented through low-level software (firmware/BIOS) and hardware. For additional information, please see the [main FAFT page](https://www.chromium.org/for-testers/faft).

## FAFT can either be run using a desktop set up or using a MobLab. MobLab is a
   self-contained automated test environment running on a Chromebox.

## **Below are instructions to run FAFT on a single DUT using MobLab.** These
   instructions assume that you have a [servo v2
   board](https://www.chromium.org/chromium-os/servo) as well as a development
   device and are aware of how to install a servo cable onto the device.

## Running FAFT tests with servo v2 requires a non-standard MobLab set-up. The
   servo v2 board provides several capabilities. It provides the ability to send
   additional hardware signals through the debug connector. These include
   recovery mode, dev mode, lid close, power button, reset, etc.

## Prerequisites:

*   Functional MobLab on version 7073 or later. You can confirm what version
    MobLab is running by reviewing the version and platform at chrome://chrome
    on your MobLab device.
*   Device Under Test (DUT):
*   Running test image
*   Servo cable attached. If you are unsure of how to attach a servo cable to
    your development device, please contact your Google Partner Representative.

## ## Additional Items needed for running FAFT through MobLab

## ## Servo V2 Setup

## *   [Servo V2 board](https://www.chromium.org/chromium-os/servo)
## *   8 GB USB stick for SERVO BOARD (for storing device image)
##     *   Note this is not the USB stick that is plugged into the MobLab.
## *   2 USB to micro-USB cables. Note that cables with surge protection could
       affect the data connection and lead to “No servos found” error when
       starting servod
## *   Ethernet cable
## *   [StarTech USB 2.0 to Gigabit Ethernet NIC Network Adapters
       (USB21000S2)](http://www.amazon.com/gp/product/B007U5MGDC/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)

## ## Instructions

## Running tests with servo v2 requires a non-standard MobLab set-up. At this
   time, you can only run FAFT tests on a single DUT using the MobLab. You
   should disconnect all DUTs from the MobLab except for the one you plan to
   test.

## ![image](Running%20FAFT%20%28Fully%20Automated%20Firmware%20Tests%29%20with%20MobLab.png)

## ![image](servo-v2-diagram.png)

## ### Connecting the Servo V2 Board to the MobLab

## 1.  Using an ethernet cable, connect the Servo V2 board to the MobLab. You
       will need a usb-to-ethernet dongle on the Moblab side.
## 2.  Connect the HOST_IN port on the servo v2 board to the MobLab using a
       micro-usb to usb cable.

## ### Connecting the DUT to the Servo V2 Board

## Your device needs to be a developer device, that is a debug port should be
   located on the motherboard in order to connect the servoflex cable. If you
   have additional questions regarding hardware setup, please see your Chrome OS
   partner representative.

## 1.  Connect the DUT_CONN_V2 port on the servo board to the DUT with the flex
       cable as pictured in the diagram above. Instructions is additional detail
       are available at <http://www.chromium.org/for-testers/faft>. The servo
       cable allows you to simulate keystrokes and mouse actions.
## 2.  Connect the DUT_HUB_IN port on the servo board to the DUT using a
       micro-USB to USB cable. The micro-USB connection should be plugged into
       the servo v2 board. NOTE: for newer devices with USB-C ports only, you
       would need a USB-C to USB-A converter that connects to a USB-A to
       micro-usb connector instead.
## 3.  Load a recovery test image onto a USB key and plug it into the USB port
       on the servo board.

## ### Servo software set up and basic tests

## Before running tests, you will need to register the DUT if it has not been
   registered. In addition, you will need to make some changes to run FAFT.

## 1.  [Register the
       DUT](http://www.chromium.org/chromium-os/testing/moblab/usingmoblab#TOC-Register-hosts)
## 2.  Configure host attributes for the DUT
##     1.  From the AFE
##         1.  Click on the “Admin Interface” link in the top right corner of
               the MobLab Front End.
##         2.  Select Labels
##             1.  Ensure label “pool:suites” exists
##             2.  If not click Add label and create it.
##         3.  Click on AFE at the top right to go back to the main menu.
##         4.  Select *Hosts*
##         5.  Select the host to which the servo will be attached and enter the
               host configuration page
##             1.  Add the following to the labels field for the DUT with servo
                   attached: pool:faft
##             2.  Add the following attribute to the DUT under test:
                   servo_host=localhost
## 3.  make sure servo board is connected to Moblab and DUT
##     1.  in VT2 login Moblab shell as user "moblab" run this at command line
           $sudo servod --board=$BOARD #where $BOARD should be the board name of
           your DUT. e.g. $ sudo servod --board=lumpy
##     2.  this will start servo itself
## 4.  in VT3 (ctrl+alt+F3) Test that servo is properly working with the DUT by
       typing the following commands in the terminal(moblab shell). See the
       troubleshooting section at the bottom if things are not working.
##     1.  dut-control power_state:off
##     2.  dut-control power_state:on
## 5.  Recommend running firmware_FAFTSetup test (test type: server) to ensure
       the setup is all correct before running either faft_ec or faft_bios.
       Please see next section "Running...a Single FAFT Test" for detailed
       instructions.

## ### Running FAFT Suites or a Single FAFT Test

## ![image](create_job_faft.png)

## 1.  To run FAFT on a given image, you will first need to put the recovery
       test image for DUT on the USB stick attached to the servo. To put a
       recovery image on the USB, you can use [cros
       flash](http://www.chromium.org/chromium-os/build/cros-flash) from the
       chroot at your workstation. The [Chrome OS developer
       guide](http://www.chromium.org/chromium-os/developer-guide) has
       additional information.
## 2.  To run a FAFT suite (faft_ec, faft_bios)
##     1.  Click *Create Job* link from moblab web url
##     2.  Next to *Job name: field*, enter an unique job name.
##     3.  Next to the *Image URL/Build:* field enter an available build name
           (ex veyron_jerry/R40-6380.0.0).
## 3.  \[Optional\] Expand *Firmware Build Options (optional)* in order to
       specify different firmware builds.
## 4.  \[Optional\] Specify *Test Source Build*.
## 5.  In the *Select Tests to Run*: section, select Server next to Filter by
       test type:
## 6.  \[For Suite\] Type in the suite name in *Filter by test name*. FAFT
       suites include include: “test_suites:faft_ec” AND “test_suites:faft_bios”
## 7.  \[For a single test\]
##     1.  Type in the test name in *Filter by test name.*
## 8.  Click on the check box for the test you want to run in the search result
## 9.  \[For a single test\] Under the *Select Hosts for Running Tests* section,
       click on the hostname. This should result in the same host to be
       displayed in the ‘Selected hosts’ section.
## 10. \[For Suite\] Click on *Advanced Options*. Next to the Pool (Optional):
       field, enter faft
## 11. Click Submit job to run.

## ## Troubleshooting

## *   The servo cable has a tendency to come loose from the DUT. Make sure that
       it has not fallen off. While running FAFT tests, you may need to prop the
       DUT sideways if the servo cable keeps falling off the DUT.
## *   You may need to restart the servo v2 board (by pressing the button on the
       board) or restart the MobLab.
## *   Make sure that you are not accidentally running two instances of servod.
       In addition, make sure that the one running is running on port 9999 which
       is the default port.
## *   If you use a different port you need to assign the servo_port host
       attribute.
##     *   From the AFE
##         *   Click on the *Admin Interface* link in the top right corner of
               the MobLab Front End.
##         *   Select Hosts
##         *   Select the host to which the servo will be attached and enter the
               host configuration page
##         *   Add the following attribute to the DUT under test:
               servo_port=<port>
