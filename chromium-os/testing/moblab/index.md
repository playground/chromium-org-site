# MobLab

Moblab is a self-contained automated testing environment running on a Chromebox.
It provides a similar set up to the Chrome OS lab and can be used for various
types of automated testing including

*   Device Bring up testing e.g. running BVTS and other similar tests
*   Firmware Testing (e.g. FAFT)
*   Component Testing
*   Others

**Join
[moblab-discuss](https://groups.google.com/a/google.com/forum/#!forum/chromeos-moblab)
forum to receive announcement and discussions!**

### Sections

*   [MobLab Overview](overview-of-moblab/index.md)
*   [Release Notes](releasenotes/index.md)
*   [Setting up MobLab](setup/index.md)
*   [Using MobLab](usingmoblab/index.md)
*   [Running FAFT with MobLab](faft-testing-with-moblab/index.md)
*   [Partner Testing](partner_testing.md)
*   [View Test Result using WMatrix](view-test-result-vis-wmatrix/index.md)
*   [Mob Monitor](mob-monitor/index.md)
*   [FAQ](../moblab-faq.md)
