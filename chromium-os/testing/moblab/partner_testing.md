# Partner Testing

## How to run partner tests using moblab

### Google Device Owner:

*   Point partners to
    <https://www.google.com/chromeos/partner/fe/#request_test_effort> to request
    the partner test effort template (this is done via the test request issue).

*   When the test run is complete, add the Test Tracker link to the test request
    issue.

### Partners:

First run the partners test suite on Moblab:

*   Navigate to <http://cautotest> on moblab, or navigate to http://<moblab-ip>.

*   Go to ‘Create Job’ section.

*   Enter Job name, Image URL.

*   Select ‘server’ in ‘Filter by test type’.

*   Enter ‘test_suite:partners’ in ‘Filter by test name’, then press the tab
    key.

*   Select test_suites:partners.

*   Submit job.

*   Watch the job run.

Updating the spreadsheet:

*   In the spreadsheet, search for test-cases with ‘Moblab’ label in the
    ‘TcTags’ column.

*   The test description field has ‘Moblab test’ name.

*   Search for that test in the moblab job run page to check the test result.

*   Update the test ‘Result’ field in the spreadsheet.

*   Update the test ‘Notes’ field in the spreadsheet with the test suite log
    gs:// path.

Run remaining tests manually and update spreadsheet accordingly.
