# Mob Monitor

## Introduction

The Mob Monitor is a tool available on Moblab devices that provides:

*   Health information about the Moblab device.
*   Health information about the important services running on Moblab.
*   Instructions and/or actions to repair many common issues.

Using the Mob Monitor can help you to:

*   Successfully setup your Moblab device.
*   Maintain your Moblab device in good working condition.

## How to use the Mob Monitor

### The Web UI

To access the Mob Monitor web interface, go to http://localhost:9991 on your
Moblab (even if your Apache server is not up, e.g. when http://localhost is not
functioning, you can still use this address to get to Mob Monitor).

![image](mobmonitor_colours.png)

The web interface presents all health information in single table. Each
compartment in the table

describes the state of a single service running on Moblab. In the above image,
we see health information

for the following illustrative services:

*   healthy_service
*   test_service
*   test_service_2
*   test_service_3

A service may be described by the monitor as being:

*   **Perfectly Healthy**:
    *   This means the service is functioning correctly!
*   **Quasi-Healthy**:
    *   This means the service is functioning correctly, but it is not
        functioning optimally.
*   **Unhealthy**:
    *   This means the service needs to be repaired.

The Mob Monitor colorizes each service based on their health status.

*   **Green**: Indicates the service is **Perfectly Healthy**.
*   **Orange**: Indicates the service is **Quasi-Healthy**.
*   **Red**: Indicates the service is **Unhealthy**.

When a service is **Quasi-Healthy** or **Unhealthy** the Mob Monitor will
display **Warnings** and **Errors**

for that service.

*   **Errors**: These are critical failures that must be repaired for the
    service to operate correctly.
*   **Warnings**: These do not indicate service failures, but indicate that the
    service may not be working optimally.

Looking at the above image, we see that:

*   healthy_service is healthy.
*   test_service is unhealthy and displays a single error and a single warning.
*   test_service_2 is unhealthy and displays a single error.
*   test_service_3 is quasi-healthy and displays a single warning.

### Diagnosing Issues

![image](mobmonitor_main_page.png)

When a service is **Quasi-Healthy** or **Unhealthy** you will be presented with
information about the problem and possible

ways to repair it.

For each displayed **Warning** and **Error**, the Mob Monitor will provide a
description of the issue and possibly one or more

repair actions.

In many cases, the issue description will provide enough information for you to
correct the problem on your own.

In other cases, the monitor will provide an action that it can execute on your
behalf.

### Repairing Issues

1.  If no repair actions are listed, follow the instructions given in the issue
    description.
2.  If repair actions are given, click to run!

Most repair actions will require no user input, you simply have to click the
button and wait ~10-15 seconds for the Mob Monitor

to execute the action in the background and for the web interface to refresh.

Some repair actions do require input. If that is the case, a dialog will pop up
when the run button is clicked.

The dialog will present the following information:

*   A description of the action in detail.
    *   This will contain additional information about the expected input.
*   An input for arguments.
    *   The dialog will tell you which arguments you must input.
    *   It will be greyed out if no arguments are expected.
*   An input for keyword arguments.
    *   The dialog will tell you which arguments are keyword arguments.
    *   It will be greyed out if no keyword arguments are expected.
    *   These are optional.

![image](mobmonitor_repair_dialog.png)

### Collecting Logs

In the event that something terrible happens with your Moblab device (related to
the Mob Monitor or not!), the Mob Monitor

provides a **Collect Logs** button in the upper right corner of the UI.

Clicking this button will open a separate tab that will provide a package of
relevant system logs for download to your local

machine. Note, this operation may be slow if there are many logs on the Moblab.

### Mob Monitor Best Practices

Some things to keep in mind when using the Mob Monitor:

*   There may be a 10 to 15 second delay from when you execute a repair action
    to when the Mob Monitor displays updated information.
*   Some errors and warnings are inter-dependent. If one action does not seem to
    have any effect, try another.
*   It's possible that when the Moblab device is in a very bad state that many
    repair actions may not have any effect. If you notice that repairs are
    ineffective, try restarting your Moblab.
