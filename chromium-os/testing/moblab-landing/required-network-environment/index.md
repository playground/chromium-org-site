# Required network environment

## Network Configuration

MobLab establishes a test subnet to isolate testing activities and machines from
the rest of the corporate network. It handles network setup and configuration
tasks, including requesting addresses and configuring services.

The connection to the corporate network and the Internet enables automatic
updates of the MobLab server image, as well as access to Google Cloud Storage.

![image](moblab-network.png)

*   \[Test Subnet\] Moblab serves DHCP for test subnet, starts from
    192.168.231.101. Traffic in test subnet only communicates to DUTs and
    MobLab.
*   \[Corporate Net\] Please connect Moblab to your corporate network by
    ethernet port, having proper routing for Moblab to access internet (or
    limited to all Google services) is a must.
*   \[Corporate Net\] Moblab doesn't support wireless connections.

## Network Security

*   The corporate network at the partner site should provide security features
    (firewall, VPN, etc.) to protect MobLab and the test subnet from potential
    external attacks.

## Network Troubleshooting
