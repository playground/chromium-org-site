# prepare-dut

## Connect Moblab

Connect the devices according to one of the diagrams below depending on how many
DUTs are being tested.

![image](prepare-moblab/1DUT.png)
**OR**
![image](prepare-moblab/3DUT.png)

*   Internet connection can only use ethernet port on Moblab
*   Please plug USB-ethernet adapter on Moblab before booting Moblab

## Setup Moblab

### Booting up

1.  Power on Moblab
2.  Make sure Moblab is in **Developer mode**, you should see booting screen
    like this
    ![image](prepare-moblab/a97e2-chrome_os_verification_off.jpg)
    if not, please follow [THIS
    PAGE](https://www.chromium.org/chromium-os/developer-information-for-chrome-os-devices/asus-chromebox#TOC-Entering)
    to enable developer mode.
3.  At welcome screen, check if Moblab connects to ethernet. Continue to next
    until login screen shows, login with any account or simply browse as guest.
    ![image](prepare-moblab/check_ethernet.png)
    ![image](prepare-moblab/ChromeOS---Login-Screen-Single.png)
4.  Enter developer console and login as **moblab**, you should not be prompted
    for password.

        **ctrl-alt-F2**
        localhost Login: **moblab**

5.  Check network configurations

        moblab@localhost ~$ **ifconfig**
        **eth0**: flags=4099<UP,BROADCAST,MULTICAST> mtu 1500 
        inet **172.30.253.16** netmask 255.255.255.0 broadcast 172.30.253.255
        ether f0:de:f1:62:c5:73 txqueuelen 1000 (Ethernet)
        RX packets 0 bytes 0 (0.0 B)
        RX errors 0 dropped 0 overruns 0 frame 0
        TX packets 0 bytes 0 (0.0 B)
        **lo**: flags=73<UP,LOOPBACK,RUNNING> mtu 65536 
        TX errors 0 dropped 0 overruns 0 carrier 0 collisions 0
        inet** 127.0.0.1** netmask 255.0.0.0
        inet6 ::1 prefixlen 128 scopeid 0x10<host>
        loop txqueuelen 1 (Local Loopback)
        RX packets 560719 bytes 339937974 (324.1 MiB)
        RX errors 0 dropped 0 overruns 0 frame 0
        TX packets 560719 bytes 339937974 (324.1 MiB)
        TX errors 0 dropped 0 overruns 0 carrier 0 collisions 0
        **eth1**: flags=4099<UP,BROADCAST,MULTICAST> mtu 1500 
        ether f0:de:f1:62:aa:bb txqueuelen 1000 (Ethernet)
        RX packets 0 bytes 0 (0.0 B)
        RX errors 0 dropped 0 overruns 0 frame 0
        TX packets 0 bytes 0 (0.0 B)
        TX errors 0 dropped 0 overruns 0 carrier 0 collisions 0
        **lxcbr0**: flags=4099<UP,BROADCAST,MULTICAST> mtu 1500
        inet **192.168.231.1** netmask 255.255.255.0 broadcast 192.168.231.255
        inet6 fe80:48cdd:d0ff:fe15:a473 prefixlen 64 scopeid 0x20<link>
        ether f0:de:f1:62:aa:bb txqueuelen 1000 (Ethernet)
        RX packets 0 bytes 0 (0.0 B)
        RX errors 0 dropped 0 overruns 0 frame 0
        TX packets 0 bytes 0 (0.0 B)
        TX errors 0 dropped 0 overruns 0 carrier 0 collisions 0

6.  Your network configurations should have info as below
    **eth0** : IP address from your internet network, can be corporate IP or
    public IP
    **eth1** : blank, network info for the USB-ethernet adapter.
    \*Note: for some cases you may find the internet IP assigned to eth1 and
    USB-ethernet adapter is in eth0, this is fine.
    **lo**: localhost of moblab
    **lxcbr0**: IP address (inet) = 192.168.231.1, this is the DHCP service of
    moblab
7.  Switch back to ChromeOS UI

        **ctrl-alt-F1**

    and browse any web page on internet to make sure internet works.

### Config Moblab

Before starting this section, you should have:

*   Completed Booting Up your moblab
*   Boto key and image storage bucket URL. If you don't have boto key files or
    bucket URL, please reach out to your Google contact.

1.  Open browser, enter **localhost/ or IP of your moblab**.

    ![image](prepare-moblab/afe.png)

2.  Click **Moblab** on top-right corner to enter setup page.
    Click **configure** to start.
    Moblab will show its IP as server IP and verify internet connections, if
    not, click **Query** to refresh.
    Click **Next** when confirmed.
    *Please note Moblab doesn't support connections via proxy, please consult
    with your IT support for solution.*

    ![image](prepare-moblab/moblab_config_1.png)![image](prepare-moblab/moblab_config_2-1.png)

3.  Enter your boto key info or follow this instruction to copy boto key file
    into moblab.
    Enter image storage bucket URL, the format is **gs://\[YOUR_BUCKET_URL\]/**
    Click **Finish** then **OK** to commit all changes.
    Moblab will restart some services in background and show all updated info.

    ![imageimage](prepare-moblab/moblab_config-3.png)

4.  Connect external storage (USB hard drive) to Moblab, enter developer mode
    (**ctrl-alt-F2**) then run commands below to prepare extra storage for
    Moblab

        sudo mkfs.ext4 <device_path>
        sudo e2label <device_path> MOBLAB-STORAGE

    *\*The external storage has to to unpartitioned before connecting to Moblab. *
    *\*\* Storage label has to be MOBLAB-STORAGE, otherwise won't be identified for Moblab.*
5.  Your Moblab is ready, you don't need to change anything in **Config Settings
    **or **Launch Control Key** unless you receive instructions from your Google
    contact.

    ![image](prepare-moblab/moblab_config_5.png)

## Common Issues & setup FAQ

*   Powered on moblab but unable to connect by ethernet.
    Please follow steps above and use developer console to get network
    configuration of your moblab.
    If there is no ethernet interfaces (eth0-3), please make sure your
    USB-ethernet adapter is installed and reboot Moblab.
