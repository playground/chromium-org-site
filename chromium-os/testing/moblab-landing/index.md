# Moblabv2

## Introduction

Moblab is a self-contained automated testing environment running on a Chromebox.

Moblab provides a similar setup of Chrome OS lab and can be used for various
types of automated testing including device bring up testing (e.g. BVTS),
firmware testing (FAFT), component testing and other similar tests.

The advantages of setting up MobLab and running tests at your site are

*   **Easy to setup and manage**

*   **MobLab requires virtually no administrative overhead. **

*   **Storage, network communication, and software updates are all managed automatically. The latest software is used for testing, and versions of the operating system and test suites are always in sync.**

*   **Full lab features supported**

*   **Any Autotest suite or individual test case can be run on a single box. **

*   **This includes test suites such as build validation tests (BVT), hardware component qualification tests, and custom automated tests\*. MobLab can run any test written using the Autotest framework.**

*   ***\*Depending on the type of component being tested, additional equipment may be required. For example, a WiFi signal is required for wireless connectivity tests.***

*   ### Scaleable

*   ### **The Autotest framework enables running across multiple machines and scheduling tests. Simpler tools and test frameworks only support manually launching tests, or running them on a single machine. With Autotest, you can also define pools of multiple hosts, to group machines and set scheduling priorities.**

*   ### Common ground

*   Moblab provides a common platform for test infrastructure. Using the same
    hardware, software, and test builds makes communication between partners and
    Google more efficient. When issues are found, having a common platform makes
    it much quicker and easier to reproduce them.

*   ### Open source project

*   **Community support provides greater security and faster response time for questions and issues. Source files are available for inspection and customization, and partner contributions are welcome. The only feature unavailable to custom builds of MobLab is automatic server updates.**

With MobLab, partners get a robust, low-maintenance test environment where they
can:

*   Run the same tests in a repeatable fashion as in the Chrome OS lab.
*   View test results.
*   Access build images for projects they are responsible for.
*   Run custom local tests that are not a part of the Chromium OS source tree.

## Get start

*   [Purchase / Prepare Moblab hardware](moblab-purchasing.md)
*   [Required network environment](required-network-environment/index.md)

## Configure Moblab & DUTs

*   [Prepare Moblab](prepare-moblab/index.md)
*   Prepare DUT

## Run your first Moblab test

*   [r](prepare-moblab/index.md)un it

## Advance usage

*   run it better
*   better
*   and better

## FAFT How-to

*   run it arrrr

*   Sections

*   [MobLab Overview](../moblab/overview-of-moblab/index.md)
*   [Release Notes](../moblab/releasenotes/index.md)
*   [Setting up MobLab](../moblab/setup/index.md)
*   [Using MobLab](../moblab/usingmoblab/index.md)
*   [Running FAFT with MobLab](../moblab/faft-testing-with-moblab/index.md)
*   [Partner Testing](../moblab/partner_testing.md)
*   [View Test Result using
    WMatrix](../moblab/view-test-result-vis-wmatrix/index.md)
*   [Mob Monitor](../moblab/mob-monitor/index.md)
*   [FAQ](../moblab-faq.md)
