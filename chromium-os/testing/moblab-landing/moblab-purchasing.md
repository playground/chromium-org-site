# Purchase / Prepare Moblab hardware

## Required Hardware

MobLab is a test system in a box: a customized Chrome OS image loaded onto a
Chromebox. The MobLab tests are the same tests that the Chrome OS team at Google
runs in the Chrome OS lab. A few customizations have been added to help
partners, like extra processes to streamline interactions with the test suite.

The hardware specs for the supported Moblab platform as of Oct 1, 2015 are
below. If your setup does not use the hardware below, our team will not be able
to assist in troubleshooting.

Hardware **Specification** Details Chromebox ASUS CN62 with Core i3
processor<https://www.asus.com/Chrome-Devices/ASUS_Chromebox_CN62/specifications/>
Internal SSDTranscend 128 GB SATA III 6Gb/s MTS400 42 mm M.2 SSD Solid State
Drive
TS128GMTS400<http://www.amazon.com/Transcend-MTS400-Solid-State-TS128GMTS400/dp/B00KLTPUU0>
USB-to-ethernet adapter StarTech USB 2.0 to Gigabit Ethernet NIC Network Adapter
(USB21000S2)<http://www.amazon.com/gp/product/B007U5MGDC/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1>
External storage 1TB USB
3.0<https://www.amazon.com/Elements-Portable-External-Drive-WDBUZG0010BBK-EESN/dp/B00CRZ2PRM/>
You can purchase equipments from local market or contact source listed below.

### Procurement Source

Synnex Technology International Corp.

Sanny Chiu

E-mail: sannychiu@synnex.com.tw

Tel : 886-2-2506-3320 Ext 2033

### List of equipment

### Minimal

*   1 x Moblab with upgraded internal SSD
*   1 x DUT (device under test, a.k.a. test device)
*   2 x USB-ethernet adapter
*   Ethernet cables, keyboard, mouse & monitor with HDMI/DP supported

### Recommended

*   1 x Moblab with upgraded internal SSD
*   1~5 x DUTs (For CTS testing, highly recommended using 5 DUTs)
*   1 x 1TB external storage
*   2~6 x USB-ethernet adapter (each DUT needs 1 adapter)
*   1 x switching hub (10/100 Mbps)
*   Ethernet cables, keyboard, mouse & monitor with HDMI/DP supported
*   1 x Chromebook for remote accessing Moblab (optional)

### FAFT

*   1 x Moblab with upgraded internal SSD
*   1 x DUT (device under test, a.k.a. test device)
*   1 x Servo board with flex cable
*   1 x 32GB USB thumb drive
*   2 x USB-ethernet adapter
*   2 x micro USB cable
*   Ethernet cables, keyboard, mouse & monitor with HDMI/DP supported
