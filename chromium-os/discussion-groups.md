# Technical Discussion Groups for Chromium OS

#### [We use the following groups for high technical content discussions about implementation details within Chromium OS.](http://groups.google.com/group/chromium-dev) [The intended audience is engineers contributing code to Chromium OS.](http://groups.google.com/group/chromium-dev) If you're interested in the browser, you might also want to subscribe to some [Chromium technical discussion groups](../developers/technical-discussion-groups.md).

#### #### These groups are not the right place to post feature requests, help/support queries, or other end user questions!

#### #### For that, see the [general discussion groups](http://www.chromium.org/developers/discussion-groups).

**[chromium-os-dev](http://groups.google.com/a/chromium.org/group/chromium-os-dev)**

The engineers on the Chromium OS project use this group to discuss technical
issues and to make announcements. This is a high-technical-content mailing list.
Typical posts include items such as plans to reduce memory consumption,
discussions around how certain code areas are implemented, developing for new
processors and devices, announcements of new tests being added to the buildbots,
and so forth.

#### [chromium-os-bugs](http://groups.google.com/a/chromium.org/group/chromium-os-bugs)

This is a read-only list for notifications of reports of Chromium OS bugs and
updates to existing bugs. Join this group if you want to monitor changes to the
Chromium OS bug database.

#### [chromium-os-checkins](http://groups.google.com/a/chromium.org/group/chromium-os-checkins)

This is a read-only list for notifications of individual check-ins to the
Chromium OS project. Join this group if you want to monitor changes to the
Chromium OS source code.

#### [chromium-os-reviews](http://groups.google.com/a/chromium.org/group/chromium-os-reviews)

This is a high-technical-content list containing reviews of proposed changes to
the source code of the Chromium OS open-source project. Join this group if you'd
like to participate in or be notified of code reviews.

### IRC Channel

### IRC: irc.freenode.net/#chromium-os

*Note: On 1/21/2010 we switched our mailing lists to be hosted on chromium.org.
Posts before that date for the above lists are available at
[chromium-os-discuss](http://groups.google.com/group/chromium-os-discuss),
[chromium-os-dev](http://groups.google.com/group/chromium-os-dev),
[chromium-os-bugs](http://groups.google.com/group/chromium-os-bugs),
[chromium-os-checkins](http://groups.google.com/group/chromium-os-checkins) and
[chromium-os-reviews](http://groups.google.com/group/chromium-os-reviews).*
