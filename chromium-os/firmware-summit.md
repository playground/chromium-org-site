# Firmware Summit 2014

Agenda and Slides

*   Chromium OS Firmware Overview
*   coreboot Overview and Features
*   coreboot Porting to x86
*   coreboot Porting to ARM
*   coreboot Porting to Mainboard
*   Depthcharge
*   Embedded Controller
*   Chromium Process
