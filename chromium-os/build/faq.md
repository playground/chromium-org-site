# Build FAQ

# cbuildbot

## Overview of cbuildbot

<https://www.chromium.org/chromium-os/build/using-remote-trybots>

## How to add metadata to cbuildbot

<https://sites.google.com/a/chromium.org/dev/cbuildbot-metadata-json-tutorial>

## How to run processes in parallel (chromite.lib.parallel)

## <https://sites.google.com/a/chromium.org/dev/chromium-os/build/chromite-parallel>

## Why is the HWTest phase stage 'orange'?

The hardware test phase is 'orange' when it fails with a warning. These warnings
occur when a test passed with warnings or after retries. They can also occur
with the tests are aborted (see below).

## How do I test that my change does not break an incremental build?

To test whether your change breaks an incremental build, you will need to create
a pristine local checkout, build it, and then add your change and verify that
your change does not break anything. First you will need to find a builder to
run both your tests on so that you can test a build. These are listed under
"Buildslaves" on this page:
<http://chromegw.corp.google.com/i/chromiumos.tryserver/builders/x86-generic-paladin>

Once you have found your builder, you can run your test. I have given an example
below with a 'buildXX-m2' placeholder, but please replace this placeholder with
the build slave you would like to use.

    cbuildbot --remote mario-incremental --slaves=buildXX-m2
    cbuildbot --remote mario-incremental --buildbot --debug --slaves=buildXX-m2

## In cbuildbot_config.py, what does important=False mean?

From the documentation in cbuildbot_config.py: If important=False then this bot
cannot cause the tree to turn red. Set this to False if you are setting up a new
bot. Once the bot is on the waterfall and is consistently green, mark the
builder as important=True.

# Google Storage

## What bucket can I use for testing?

If you are a Googler, you can use gs://chromeos-throw-away-bucket for testing.
Keep in mind that the contents can be read or deleted by anyone at Google.

We also have another similar bucket named gs://chromeos-shared-team-bucket .
Probably using gs://chromeos-throw-away-bucket makes more sense since it is
clear that the contents there can be deleted by anyone at Google.

If you are not a Googler, you should create your own bucket in Google Storage.

# Portage/Emerge

## How do I force emerge to use prebuilt binaries?

    emerge -g --usepkgonly
    (Note: The --getbinpkg only flag for emerge does not actually work)

## How do I check the USE flags for a package I am emerging?

Emerge will tell you what USE flags are used when the **-pv **flags are provided
e.g.

    emerge-$BOARD chromeos-chrome -pv

# Development

FAQ items covering general development questions

What should I read first/Where do I start?

<http://www.chromium.org/chromium-os/developer-guide>

## How do I work on multiple changes based on unsubmitted changes?

## <https://sites.google.com/a/chromium.org/dev/chromium-os/how-tos-and-troubleshooting/gerrit-dependent-changes>

## How can I make changes depend on each other with CQ-DEPEND?

<https://sites.google.com/a/chromium.org/dev/developers/tree-sheriffs/sheriff-details-chromium-os/commit-queue-overview#TOC-How-do-I-specify-the-dependencies-of-a-change->

## How do I create a new bucket in gsutil?

<https://sites.google.com/a/chromium.org/dev/gsutil-faq>

## How do I port a new board to Chromium OS?

<https://sites.google.com/a/chromium.org/dev/chromium-os/how-tos-and-troubleshooting/chromiumos-board-porting-guide>

## Making changes to the Chromium browser on ChromiumOS (AKA Simple Chrome)

<http://www.chromium.org/chromium-os/how-tos-and-troubleshooting/building-chromium-browser>

## How to install an image on a DUT via cros flash?

<http://www.chromium.org/chromium-os/build/cros-flash>
