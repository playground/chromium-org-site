# Updating our version of \`repo\`

## Introduction

How to sync the Chromium fork of repo to upstream.

### Branch Policies

*   `stable`: This is tagged and released and directly used by devs/users when
    syncing. This is the most important branch.
*   `stable-next`: Incoming CLs for review are staged through here, and can be
    tested ahead of time. Once they're ready, they get tagged and pushed to
    `stable`.
*   `stable-previous`: When doing a rebase onto newer upstream releases, we save
    the previous `stable` branch here.
*   `upstream-stable`: This is used to hold upstream's `stable` branch.

All other branches are unused so you can ignore them.

## Setup Working Tree

On your machine, clone a checkout of our forked repo, and update our
upstream-stable branch to point at upstream's stable branch.

    # Our copy of repo.
    ~$ git clone https://chromium.googlesource.com/external/repo
    ~$ cd repo
    # Upstream repo.
    ~/repo$ git remote add upstream https://gerrit.googlesource.com/git-repo
    ~/repo$ git fetch upstream

## Push Upstream Commits

We keep all of the upstream tags in our repo, as well as a copy of the stable
branch. Sync those now.

Don't worry: these are just to get the history into our repository. They won't
be used by anything (yet). If you're paranoid, you can use the -n option to see
a report of what git will push before you actually push it.

    # Push the upstream stable branch.
    ~/repo$ git push origin upstream/stable:upstream-stable
    # Push all the new tags.
    ~/repo$ git push origin --tags

## Rebasing

The possibly hard part. Rebase any extra changes in our stable onto
upstream-stable. We assume stable-next is up-to-date with stable, so we'll
ignore the stable branch.

    ~/repo$ git checkout -b stable-next origin/stable
    ~/repo$ git rebase -i upstream-stable

While rebasing, remove any patches that have already been upstreamed, and fix
any other merge conflicts.

## Review

### Normal Method

The preferred route is to upload the patches for review and get them merged via
gerrit.

You'll need to make sure that the `stable-next` branch is pointing to the
release you're basing things on. We'll use `v1.12.16` purely as an example.

    ~/repo$ git push --force origin v1.12.16:refs/heads/stable-next

Then you can upload your changes for review.

    ~/repo$ git push origin stable-next:refs/for/stable-next

### Direct Pushing

You can always push your work directly to `stable-next` and bypass review. Not
recommended, but sometimes you might need to do this.

    ~/repo$ git push --force origin stable-next:refs/heads/stable-next

## Test The Release

Test stable-next by running some builds that use the new version of repo (by
patching in a change that forces repo to use stable-next).

    $ cbuildbot --remote --buildbot --debug -g I6fabcc63613ec1a861c1f4b7a7fb45b5b4e0bf28 x86-mario-release x86-mario-paladin master-paladin

## Create a New Signed Release

Before you can push a new release, you need to tag and sign it. You can find
someone to do it, or you can do it yourself.

Note: All CLs must be landed in `stable-next` before it can be signed and
released.

### Authorized Developers

If you look at the existing [repo launcher in
depot_tools](https://chromium.googlesource.com/chromium/tools/depot_tools/+/master/repo#27),
there is a list of keys and the users who can tag new releases. Only keys listed
in that repo launcher are permitted to sign new tags. You'll want to talk to
them to sign & push.

Alternatively, you can become an authorized dev!

#### Setting Up A New Developer

You'll need a PGP key. If you don't have one, create it! Pick options "RSA and
RSA", 2048 bits, does not expire, enter your name, and use your @chromium.org
e-mail address.

    $ gpg --gen-key

Then you'll need to export the key in ascii format.

    $ gpg --armor --export KEYID

Then add it to the list of keys like in [this
commit](https://chromium.googlesource.com/chromium/tools/depot_tools/+/33c182be2dce9005292932df14741e51e8434577%5E%21/).

You'll need to update
[depot_tools](https://chromium.googlesource.com/chromium/tools/depot_tools/)
with the new keys embedded in repo. Then you'll need to update the depot_tools
snapshot in the [manifest](https://chromium-review.googlesource.com/385757) &
[manifest-internal](https://chrome-internal-review.googlesource.com/287355).

### Tagging The Release

We use a `-cr#` suffix to differentiate Chromium releases. So if the current
code is based on `v1.12.16`, you'll want to use `v1.12.16-cr1`, then
`v1.12.16-cr2`, and so on.

Then create a signed tag:

    ~/repo$ git tag -s -u KEYID -m 'Chromium-specific release' v1.12.16-cr1

## Release To Developers

Once everything is in place and tested and you're ready to release, you'll want
to update the `stable` branch.

    ~/repo$ git fetch origin

Record the previous stable branch, just in case a revert is necessary.

    ~/repo$ git push origin stable:stable-previous --force

Finally push stable-next to stable.

    ~/repo$ git push --force origin stable-next:stable

## Update Launcher in depot_tools

As a last step, you should copy the current `repo` script that you just released
into
[depot_tools](https://chromium.googlesource.com/chromium/tools/depot_tools/) and
send a CL (like [this
one](https://chromium.googlesource.com/chromium/tools/depot_tools/+/3bdfbeb0fe70d151d6c48b121f58265400af55b2))
for updating it to the latest.
