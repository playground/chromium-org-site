# Chromium OS Local Trybots

## Introduction

The local trybot allows you to emulate a buildbot run on your local machine with
a set of your changes. The changes are patched to tip of tree (TOT). It supports
running of paladin and full buildbot configurations.

It supports patching of both gerrit CL's and local changes.

For Google developers, please also take a look at the [Remote Trybot
documentation](using-remote-trybots.md).

NOTE: The first time you run the trybot it will sync down a fresh checkout of
the source, build a new chroot and board, which will make the initial run take
longer than subsequent runs. An incremental run with an existing board takes
30min-40min.

NOTE: Because the trybot syncs the source tree to TOT, your trybot run may fail
if the tree is red, or if TOT was broken at the time you ran trybot. With the
commit queue this should be less of a problem.

## **Pre-instructions (These are important!)**

1.  Make sure you’ve read through the [ChromiumOS Developer
    Guide](http://www.chromium.org/chromium-os/developer-guide) and can kick off
    a build properly.
2.  [Modify your sudo config](../tips-and-tricks-for-chromium-os-developers.md).
    If you are at Google, you need to follow [these
    instructions](https://sites.google.com/a/google.com/chromeos/resources/ubuntu-workstation-notes)
    first (Under 'Tweaking with /etc/sudoers' section).
3.  Run sudo aptitude install kvm pbzip2 zip.
4.  Run sudo aptitude safe-upgrade.
5.  Run gclient from outside the chroot to update depot_tools.
6.  If you haven’t rebooted your computer in a while, reboot it now after
    updating.

## **Verifying the Trybot**

It’s good to first verify things are working properly with your system setup.

To do so, run the following from within your repo source checkout:

1.  Run repo sync --jobs=<jobs> to get latest version of trybot.
2.  Run cbuildbot x86-generic-paladin to do a test run with the current TOT.
    Everything should pass. Contact rcui@chromium.org if you fail here (after
    you have run the pre-instructions).

## **Instructions For Using the Trybot**

Run the following from within your repo source checkout:

1.  Run repo sync --jobs=<jobs> to get latest version of trybot.
2.  Run cbuildbot --list to see a list of common configs to run with. Use the
    --all flag to see all possible configs.

### To patch in a gerrit CL

**3a.** Run

    cbuildbot -g '[*]<cl_1> [*]<cl_2> .. [*]<cl_N>' config

Substitute <config> with your desired config. Prepend a '\*' to the CL ID to
indicate it's an internal CL. The CL ID can be a Gerrit Change-ID or a change
number.

An example:

    cbuildbot -g I5bed88ef -g '4168 *Idfd9fcab' x86-alex-paladin

This patches in three CL's: 1) an external CL using Gerrit change-ID 2) an
external CL using Gerrit change number 3) internal CL using change-iD. In case a
CL has several patches associated with it, the latest patch is used.

### To patch in a rietveld CL

**3b.** Run

    cbuildbot --remote --rietveld-patches=id[:subdir1]...idN[:subdirN]

An example:

    cbuildbot --remote --rietveld-patches=11659002:src/tools/gyp x86-alex-paladin x86-alex-release

This patches the CL <https://codereview.chromium.org/11659002> into
chromium/src/tools/gyp.

Internal rietveld CLs are not yet supported.

### To patch in a local change

**3c.** Run

    cbuildbot -p '<project1>[:<branch1>]...<projectN>[:<branchN>]' config

Specify the name of the project (not the path) and optionally the project
branch. If no branch is specified the current branch of the project will be
used.

NOTE: To get the project name of the project you're working on run repo list.

NOTE: Do not do development within the trybot buildroot! Your changes will get
wiped out on the next trybot run. Develop within your source root, and patch in
changes.

NOTE: Use the --nosync option to prevent the trybot from updating its source
checkout. See the Tips section below for more info.

## **Sample Trybot Run**

    rcui@ryanc$ cbuildbot -g 'I5bed88ef 4168' -p 'chromiumos/chromite chromiumos/platform/crosutils' x86-alex-paladin

    WARNING: Using default directory /usr/local/google/home/rcui/trybot as buildroot

    Do you want to continue (yes/NO)? yes

    INFO: Saving output to cbuildbot.log file

    [sudo] password for rcui: 

NOTE: The output of the trybot is automatically saved to a cbuildbot.log file in
<trybot_root>/cbuildbot_logs. The log directory is printed out at the end of a
run.

## **Tips**

Say you’ve run the trybot once with your patches, and it fails. After making
some changes to fix it you want to test the patches again, but you don’t want
the trybot to update the source checkout. In this case, you can use the --nosync
option. Trybot will skip the sync stage and apply your patches to the previously
checked out source.

## **Overview of Trybot Operation**

The first time you run the trybot, it will sync down its own checkout of the
source, and build its own chroot. The CL's you specify to patch are patched to
the trybot's own source tree.

When you specify local changes to patch (by specifying a project and branch the
changes are on) the trybot will generate git patch files based on those changes
and apply the patch files to its private source checkout.

When you specify a Gerrit changelist, the trybot will look up the changelist
info from Gerrit, fetch the ref of the change, and rebase it to TOT in its
private source checkout.

## **Feedback**

Thanks for using the trybot! Please contact
[chromium-os-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/chromium-os-dev)
for any issues you run into. Please report any bugs you find, and file feature
requests. There will be active development on the trybot, so please sync to the
latest version before you run it.
