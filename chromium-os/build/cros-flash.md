# Cros Flash

## Overview

Cros Flash is a script to update a Chromium OS device with an image, or to copy
image onto a removable device (e.g. a USB drive). It replaces
`image_to_live.sh`, `cros_image_to_target.py `and `image_to_usb.sh.`

Cros Flash utilizes the
[devserver](http://www.chromium.org/chromium-os/how-tos-and-troubleshooting/using-the-dev-server)
to download images and/or generate payloads.

When updating a Chromium OS device, Cros Flash relies on SSH connection to talk
to the device (which is enabled in the test images). The main difference with
previous update tools is that Cros Flash assumes that the device is NOT capable
of initiating an SSH connection to your workstation. This allows Cros Flash to
be used in a more restricted/secured network environment.

To know more about the design and implementation of Cros Flash, please look at
the [design
doc](https://docs.google.com/a/google.com/document/d/1UdDTCC5V9R6VFMsiIzclMCSNwr-Iz42o6t_NfhEEGc4/)
(may be slightly outdated).

## Requirements

### Updating a chromium device

1.  Chroot: Cros Flash needs chroot to run devserver to generate payloads.
2.  A SSH-able Chromium OS device: Any Chromium OS test image will work.
3.  SSH test keys so that the script can SSH into the test device without a
    password. See [Setting up SSH Access to your test
    device](../testing/autotest-developer-faq/ssh-test-keys-setup.md).
4.  Full Chromium OS checkout (until [crbug.com/403086](http://crbug.com/403086)
    is addressed).

If your device is currently running a non-test image, you may want to use a USB
stick to image the device first (see [developer
guide](http://www.chromium.org/chromium-os/developer-guide)).

There are plans to eliminate both requirements so that Cros Flash can be
integrated into the
[SimpleChrome](http://www.chromium.org/chromium-os/how-tos-and-troubleshooting/building-chromium-browser)
workflow. You can track the progress [here](http://crbug.com/309848).

### Download images/payloads from Google Storage

1.  Chroot: Cros Flash relies on xbuddy/devserver to download the files.
    Devserver currently runs only in chroot (This is an artificial requirement
    and may be lifted in the future)
2.  Credentials to download from Google Storage. External developers may not
    have such credentials.

## Example Usage

    cros flash <device> <image>
    <device>: Required. ssh://IP[:port] of your ChromiumOS device, usb://path/to/removable/device, or file://path/to/a/file
    <image>: Optional. Can be a path to an image, an xbuddy path, a payload directory, or simply 'latest' for latest locally-built image. Defaults to 'latest'.

For example, to update the device with the latest locally-built image, you can
use the shortcut 'latest':

    cros flash ssh://192.168.1.7 latest

or simply type:

    cros flash ssh://192.168.1.7

To use a locally-built (test) image:

    cros flash ssh://192.168.1.7 xbuddy://local/x86-mario/R33-5100.0.0

To use a local image:

    cros flash ssh://192.168.1.7 path/to/image

To download the latest canary image:

    cros flash ssh://192.168.1.7 xbuddy://remote/x86-mario/latest-canary

You can replace `ssh://192.168.1.7 `in the above examples with`
usb://device/path `or `usb://` to copy the image onto a removable device.
However, you have to specify the board to use.

    cros flash ssh://192.168.1.7 board/latest

    cros flash usb:///dev/sdc path/to/image

    cros flash usb:// path/to/image

Finally, if you just want to look at the image you are installing (using
mount_gpt_image.sh) or save the file for later, you can use file:// to save it
to a file of your choice e.g.

    cros flash file://path/to/save/image.bin path/to/image

## Device

### Chromium OS Device

Device can be

    ssh://hostname[:port]

or

    hostname[:port]

Port number is the SSH port to use to connect to your device; it is optional
(defaults to 22).

### Removable Device

Device can be

    usb://path/to/removable/device

or

    usb://

If a device path is specified, Cros Flash will check if the device is indeed
removable. If no path is given, user will be prompted to choose from a list of
removable devices. Note that auto-mounting of USB devices should be turned off
as it may corrupt the disk image while it's being written.

## xBuddy (CrOS Buddy) paths

xbuddy path is recognized by devserver and can be very powerful and flexible.
You can specify a remote image for devserver to download and serve, or use any
locally-built image. To use xbuddy path, use the `xbuddy://` prefix to your
path.

*The following examples applies to both ssh:// and usb:// devices.*

    xbuddy://{local|remote}/board/version/image_type
    local/remote: optional (defaults to local)
    board:        required (e.g. x86-mario, peppy)
    version:      required. See the table below.
    image_type:   optional (e.g. test, base, dev, recovery; defaults to test)*

#### Version Aliases

latest

If local: latest locally built image for that board.
If remote: the newest stable image on Google Storage

latest-{dev|beta|stable|canary}

Latest build produced for the given channel

latest-official

Latest available release image built for that board in Google Storage.

latest-official-{suffix}

Latest available image with that board suffix in Google Storage.

latest-{branch-prefix}

Latest build with given branch prefix i.e. RX-Y.X or RX-Y or just RX.

\*Note that if you use Cros Flash to install a non-test image, Cros Flash will
not be able to confirm that the device has rebooted successfully after update.
Also, you will not be able to use Cros Flash to reimage again because Cros Flash
relies on being able to ssh into the device. Use a USB stick to image your
device (image_to_usb.sh) in this case.

For example, to use the latest test image from the beta channel for the board:

    cros flash 192.168.1.7 xbuddy://remote/board/latest-beta

For more details about xbuddy, please see the [xbuddy
doc](http://www.chromium.org/chromium-os/how-tos-and-troubleshooting/using-the-dev-server/xbuddy-for-devserver).

*Note: you may not be able to download the remote images as an external
developer.*

### GS path as an xbuddy path

By default, if you pass a remote xbuddy path, xbuddy will first check to see if
it's a valid Google Storage path before attempting to parse the board/version,
etc. This gives users the flexibility to download an arbitrary image as long as
it resides in our Google Storage bucket ('`gs://chromeos-image-archive`').

For example,

    cros flash 192.168.1.7 xbuddy://remote/x86-mario-paladin/R32-4830.0.0-rc1

If you are prompted to confirm due to board mismatch, simply type 'yes' to
proceed for now. Users will soon be given an option to skip the prompt.

### Using images produced by trybots

By leveraging the "GS path as xbuddy path" feature, you can also update your
device with an image generated by a trybot. You will need the`
${bot-id}/${version}` used for archiving in this case. The information is
available on your tryjob waterfall:

1.  Open the tryjob page (e.g.,
    https://uberchromegw.corp.google.com/i/chromiumos.tryserver/builders/x86-mario-paladin/builds/99)
2.  At Report stage, you will see links such as Artifacts\[x86-mario\]:
    x86-mario-paladin/R36-5737.0.0-rc3
3.  Copy the ${bot-id}/${version} at from the link above
    (x86-mario-paladin/R36-5737.0.0-rc3).

Alternatively, you can copy the "Artifact" link and find the same information
(https://storage.cloud.google.com/chromeos-image-archive/trybot-peach_pi-release/R34-5500.91.0-b37/index.html).
You can look at the stdio log of the Archive stage.

For example,

    cros flash 192.168.1.7 xbuddy://remote/trybot-peach_pi-release/R34-5500.91.0-b37

Again, if you are prompted to confirm due to board mismatch, simply type 'yes'
to proceed for now. Users will soon be given an option to skip the prompt.

### Pre-generated payloads

Cros Flash can use your pre-generated update payloads directly if you pass a
payload directory as `<image>`:

    cros flash 192.168.1.7 path_to_payload_directory

Cros flash looks for `update.gz` and/or `stateful.tgz` in the payload directory
and use them to update the device.

## Known problems and fixes

Many of the problems you have encountered may have been fixed. Make sure you
keep your chroot up-to-date before filing a bug.

### **UPDATE YOUR CHROOT**

Run `repo sync` to update your tree, and then update your chroot (must be run in
chroot) by:

    ~/trunk/src/scripts/update_chroot

### **Where is Cros Flash?**

Cros Flash is in the `chromite `repo. The script is
chromite/cli/cros/cros_flash.py. If you cannot find it, update your tree with
`repo sync`.

### **Import error: cherrypy**

If you see a cherrypy import error, that is because your device is running an
older image (<R33-4986.0.0) that does not have cherrypy package installed by
default. Cros flash launches a devserver instance on the device to serve the
update payload for rootfs update. You can fix this with one of the following
three options.

1.  Update the device to a newer image with a USB stick (see [developer
    guide](http://www.chromium.org/chromium-os/developer-guide)).
2.  Use '--no-rootfs-update' to update the stateful parition first (with the
    risk that the rootfs/stateful version mismatch may cause some problems).
3.  Use [cros deploy](cros-deploy.md) to deploy cherrypy package to your device.

### **Devserver error: cannot specify both update and return_dir**

Cros flash depends on a relatively newer version of devserver.py. If you have
not updated your chroot in a long time, you may see an error message like this:

    DEVSERVER HTTPError status: 500 message: Cannot specify both update and return_dir

To fix this, [update your chroot.](cros-flash.md)

### **Cannot download daisy_spring (or other boards where there are underscores in the board names) images**

There is inconsistency in how we name the Google Storage buckets. Xbuddy has
been fixed to do generate the correct URL for the image. Please [update your
chroot](cros-flash.md).

### **Failures related to update engine**

Make sure that update-engine service on device is running and
inhibit-update-engine service is stopped:

    # On device
    stop inhibit-update-engine
    start update-engine

## Complete usage

View the most up-to-date usage by running `cros flash -h`

    usage: cros flash [-h] [--log-level {fatal,critical,error,warning,info,debug}]
                      [--debug] [--nocolor] [--clear-cache] [--board BOARD]
                      [--yes] [--no-reboot] [--no-wipe] [--no-stateful-update]
                      [--no-rootfs-update]
                      [--src-image-to-delta SRC_IMAGE_TO_DELTA]
                      [--clobber-stateful]
                      device [image]
    Update the device with an image.
      This command updates the device with the image
      (ssh://<hostname>:{port}, copies an image to a removable device
      (usb://<device_path), or copies a xbuddy path to a local
      file path with (file://file_path).
      For device update, it assumes that device is able to accept ssh
      connections.
      For rootfs partition update, this command may launch a devserver to
      generate payloads. As a side effect, it may create symlinks in
      static_dir/others used by the devserver.
      
    positional arguments:
      device                ssh://device_hostname[:port] or usb://{device_path}.
                            If no device_path is given (i.e. usb://), user will be
                            prompted to choose from a list of removable devices.
      image                 A local path or an xbuddy path:
                            xbuddy://{local|remote}/board/version/{image_type}
                            image_type can be: 'test', 'dev', 'base', or
                            'recovery'. Note any strings that do not map to a real
                            file path will be converted to an xbuddy path i.e.,
                            latest, will map to xbuddy://latest.
    optional arguments:
      -h, --help            show this help message and exit
      --clear-cache         Clear the devserver cache. Default is not to clear.
    Debug options:
      --log-level {fatal,critical,error,warning,info,debug}
                            Set logging level to report at.
      --debug               Alias for `--log-level=debug`. Useful for debugging
                            bugs/failures.
      --nocolor             Do not use colorized output (or `export NOCOLOR=true`)
    Advanced device update options:
      --board BOARD         The board to use. By default it is automatically
                            detected. You can override the detected board with
                            this option
      --yes                 Force yes to any prompt. Use with caution.
      --no-reboot           Do not reboot after update. Default is always reboot.
      --no-wipe             Do not wipe the temporary working directory. Default
                            is always wipe.
      --no-stateful-update  Do not update the stateful partition on the device.
                            Default is always update.
      --no-rootfs-update    Do not update the rootfs partition on the device.
                            Default is always update.
      --src-image-to-delta SRC_IMAGE_TO_DELTA
                            Local path to an image to be used as the base to
                            generate delta payloads.
      --clobber-stateful    Clobber stateful partition when performing update.
    To update/image the device with the latest locally built image:
      cros flash device latest
      cros flash device
    To update/image the device with an xbuddy path:
      cros flash device xbuddy://{local, remote}/<board>/<version>
      Common xbuddy version aliases are 'latest' (alias for 'latest-stable')
      latest-{dev, beta, stable, canary}, and latest-official.
    To update/image the device with a local image path:
      cros flash device /path/to/image.bin
    Examples:
      cros flash 192.168.1.7 xbuddy://remote/x86-mario/latest-canary
      cros flash 192.168.1.7 xbuddy://remote/x86-mario-paladin/R32-4830.0.0-rc1
      cros flash usb:// xbuddy://remote/trybot-x86-mario-paladin/R32-5189.0.0-b100
      For more information and known problems/fixes, please see:
      http://dev.chromium.org/chromium-os/build/cros-flash
