# Chromium OS Remote Trybots

## Introduction

**Sorry, but this is only available to Google employees at the moment. Make sure
you've joined the [chromeos-team
group](https://groups.google.com/a/google.com/d/forum/chromeos-team).**

Remote trybots allow you to perform a trybot run of multiple configs/boards in
parallel on remote servers. A trybot allows you to emulate a cbuildbot run with
a set of your changes. The changes are patched to tip of tree (ToT).

It supports patching of both gerrit CL's and changes you've committed locally
but haven't uploaded.

The remote trybot waterfall is located at
<https://uberchromegw.corp.google.com/i/chromiumos.tryserver>.

## Instructions For Using the Remote Trybot

Run the following from within your repo source checkout:

1.  Run repo sync chromiumos/chromite to get latest version of cbuildbot.
2.  Run cbuildbot --list to see a list of common configs to run with. Use the
    --all flag to see all possible configs.

**Googlers**, the first time you run this, it will initiate the sign-in flow. If
you accidentally use your @chromium.org account, you can run cipd auth-logout &&
cipd auth-login to switch to your @google.com account.

### To patch in a [Gerrit](https://chromium-review.googlesource.com/) CL

**3a.** Run

    cbuildbot --remote -g [*]<cl_1> [-g ...] config [config...]

Substitute \[config \[config...\]\] with a space-separated list of your desired
configs. Prepend a '\*' to the CL ID to indicate it's an internal CL. The CL ID
can be a Gerrit Change-ID or a change number.

An example:

    cbuildbot --remote -g 4168 -g *1234 alex-paladin x86-alex-release

This runs a tryjob for the alex-paladin and x86-alex-release configs in
parallel. It patches in two CL's: 1) an external CL using a Gerrit change number
2) an internal CL using a Gerrit change number. In case a CL has several patches
associated with it, the latest patch is used.

### To patch in a [Rietveld](https://chromereviews.googleplex.com/) CL (less common)

**3b.** Run

    cbuildbot --remote --rietveld-patches=id[:subdir1]...idN[:subdirN]

For example, this patches CL
[356903002](https://codereview.chromium.org/356903002) into Chrome's /src
directory and tries one bot:

    cbuildbot --remote --rietveld-patches=356903002 x86-generic-tot-chrome-pfq-informational

This patches [11659002](https://codereview.chromium.org/11659002) into
chromium/src/tools/gyp and tries two bots:

    cbuildbot --remote --rietveld-patches=11659002:src/tools/gyp x86-alex-paladin x86-alex-release

If you are asked for a username and password check your ~/.netrc. If you don't
see entries for chromium.googlesource.com you need to [generate a new
password](https://chromium.googlesource.com/new-password).

Internal rietveld CLs are not yet supported.

### To patch in a local change:

**3c.** To patch in a local patch, run:

    cbuildbot --remote -p <project1>[:<branch1>] [-p ...] config [config..]

Specify the name of the project (not the path, look for projectname in
.git/config) and optionally the project branch. If no branch is specified the
current branch of the project will be used.

An example:

    cbuildbot --remote -p chromiumos/chromite -p chromiumos/overlays/chromiumos-overlay:my_branch x86-generic-paladin amd64-generic-paladin

This patches in any commits in project chromiumos/chromite's current branch and
on branch 'my_branch' in project chromiumos/overlays/chromiumos-overlay.

### Testing on a release branch

**3d. **To test patches for a release branch (i.e., R20, R21) use the --branch
(-b) option with -g:

    cd <repo_root>/chromiumos/chromite
    git checkout **cros/master**
    cbuildbot --remote **-b ****release-R20-2268.B** -g 4168 alex-paladin

Local patches (-p) to branches are not yet supported.
Non-release branches such as factory, u-boot, etc. are not yet supported.

**3e**. To create an official release with a trybot.

Consult the
[mini-branch](https://sites.google.com/a/google.com/chromeos/for-team-members/chronos-download/pmo/cros-releasetasks/stabilize)
guide.

**Important flags to consider**

**4. Generating test artifacts for hardware test.**

If you foresee wanting to run autotests using this image in the lab, you must
ensure that it produces test artifacts. Until [this
bug](https://bugs.chromium.org/p/chromium/issues/detail?id=299838) is fixed this
is not always guaranteed to occur, and the simplest way to guarantee it is to
use the --hwtest command line flag.

**Job launched? Wait for it to finish and then...**

**5. **Optionally download the archived artifacts that the trybot uploaded by
clicking on the link in the result email.

## Other useful flags

--remote-description

Attach an optional description to a --remote run to make it easier to identify
the results when it finishes.(eg: “testing CL:1234 after fixing NameError”).

## Finding your trybot runs on the waterfall

The trybot waterfall is organized by bot category. E.g., for x86-alex-pre-cq,
the category is "pre-cq". For x86-alex-no-vmtest-pre-cq, the category is
"no-vmtest-pre-cq". For pineview-release-group, the category is "release".

When you launch your tryjob, cbuildbot will print a link showing where to find
your run. For example:

$ cbuildbot -g 288223 --remote rambi-no-vmtest-pre-cq x86-alex-release

Verifying patches...

Submitting tryjob...

Tryjob submitted!

Go to
<https://uberchromegw.corp.google.com/i/chromiumos.tryserver/waterfall?committer=davidjames@google.com&builder=no-vmtest-pre-cq&builder=release>
to view the status of your job.

You can construct the link by replacing the committer and builder to see the
status.

If you want to double check the exact category name of any bot, you can look it
up either by launching a tryjob with it (see above), or by looking in
chromite/cbuildbot/config_dump.json (the template is listed as "_template")
under the bot name.
