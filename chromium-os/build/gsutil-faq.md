# Google Storage Utility (gsutil) FAQ

## How do you create new buckets?

It requires administrator access to create new buckets. If you are an
administrator, you can create new buckets using the following commands:

    $ gsutil mb -p 134157665460 <gs://your-bucket>
     
    $ gsutil defacl get gs://chromeos-image-archive > defacl.txt
    $ cat defacl.txt
    <?xml version="1.0" ?>
    <AccessControlList>
        <Entries>
            <Entry>
                <Scope type="GroupById">
                    <ID>00b4903a97f1459df490156b2b4b28883a83d872295783af92109fdf1d202c64</ID>
                    <Name>chromeos.bot owners</Name>
                </Scope>
                <Permission>FULL_CONTROL</Permission>
            </Entry>
            <Entry>
                <Scope type="GroupById">
                    <ID>00b4903a979ae5f41f9d14ad14bff79e0ed0b3d204897259b50ab97d94bf827b</ID>
                    <Name>chromeos.bot editors</Name>
                </Scope>
                <Permission>FULL_CONTROL</Permission>
            </Entry>
            <Entry>
                <Scope type="GroupById">
                    <ID>00b4903a9765d22f3a8f0b5ae8eb7f7547317e865b2857883cf2b2e518389ca2</ID>
                    <Name>chromeos.bot viewers</Name>
                </Scope>
                <Permission>READ</Permission>
            </Entry>
        </Entries>
    </AccessControlList>
    $ gsutil defacl set defacl.txt <gs://your-bucket> 
    $ gsutil acl set defacl.txt <gs://your-bucket>

The various hex IDs above refer to groups in Google Storage:

*   Owners of the chromeos.bot project:
    `00b4903a97f1459df490156b2b4b28883a83d872295783af92109fdf1d202c64`
*   Editors of the chromeos.bot project:
    `00b4903a979ae5f41f9d14ad14bff79e0ed0b3d204897259b50ab97d94bf827b`
*   Viewers of the chromeos.bot project:
    `00b4903a9765d22f3a8f0b5ae8eb7f7547317e865b2857883cf2b2e518389ca2`

If you want different ACLs, you can edit the defacl.txt file appropriately
before setting up the ACLs.

## **How do you set up prebuilts for a new private overlay?**

When you create a new private overlay, you will need to at a minimum create a
googlestorage_acl.txt file inside the overlay. This is needed for setting up the
builder.

## googlestorage_acl.txt file format

The googlestorage_acl.txt file is used to set the permissions for files
associated with a particular board. In it are arguments that are passed to the
"gsutil acl ch" command, and optional comments which start with a "#" character
and extend to the end of the line.

For example, if googlestorage_acl.txt could contain the following:

    # For information about this file, see
    # http://dev.chromium.org/chromium-os/build/gsutil-faq
    # Give owners of the chromeos.int.bot@gmail.com project full control.
    -g 00b4903a97fb6344be6306829e053825e18a04ab0cc5513e9585a2b8c9634c80:FULL_CONTROL
    # Give viewers of the chromeos.int.bot@gmail.com project read access.
    -g 00b4903a97ce95daf4ef249a9c21dddd83fdfb7126720b7c3440483b6229a03c:READ
    # Give all Googlers read access.
    -g google.com:READ

When that file is consumed, the comment will be thrown away and "gsutil acl ch
-g google.com:READ" will be called on newly added files.
