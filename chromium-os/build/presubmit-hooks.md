# Presubmit Hooks

## Introduction

When you run \`repo upload\` to upload your local commits to Gerrit, a preupload
phase runs. This is used to run linters or local unittests to catch various
common errors.

These can be customized on a per-git repo basis. You can have custom scripts
execute using the hooks phase, or opt in/out of common checks that are kept in
the main upload script.

## Sources

You can find the main preupload script in the
[src/repohooks/](https://chromium.googlesource.com/chromiumos/repohooks/+/master)
directory.

## Per-project Settings

All settings live in the PRESUBMIT.cfg file in the root of the git repo.

### Hook Scripts

You can run arbitrary scripts with the hooks section.

    [Hooks Scripts]
    hook0 = ./some_script.sh
    hook1 = some-program 2>&1

You do not need to send stderr to stdout since the script that runs them
combines these anyway.

You can list as many/few scripts as you want. Just keep adding a unique number
after the "hook" key name. Each script must fit on one line (so don't worry
about any 80-character limits).

The following variables are defined for use by your script:

*   PRESUBMIT_PROJECT - the project name being changed by the commit (e.g.
    'chromiumos/platform/firmware')
*   PRESUBMIT_COMMIT - the full commit hash of your change
*   PRESUBMIT_FILES - a line of files affected by your commit, each on one line

### Common Flags

Since many tests are not specific to a repo, we make them available we enable
them on a global basis. You can see the current list by looking at the
_DISABLE_FLAGS dict in the
[pre-upload.py](https://chromium.googlesource.com/chromiumos/repohooks/+/master/pre-upload.py)
file itself.

    [Hook Overrides]
    cros_license_check: false

## New Common Checks

When looking to add a new check, you should first consider whether it would
benefit more projects than just your own. If so, you should add it to the
repohooks/pre-upload.py file itself.

## Commit Queue Settings

This isn't directly related to repohooks, but you can learn more about
COMMIT-QUEUE.ini in the [Bypassing tests on a per-project basis
page](bypassing-tests-on-a-per-project-basis.md).
