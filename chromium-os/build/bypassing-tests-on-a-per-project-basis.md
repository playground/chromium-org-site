# Per-repo and per-directory configuration of CQ and pre-CQ

Different chromeos repositories have different testing needs. Using per-repo or
per-directory configuration files, it is possible to tailor the behavior of the
[Chromeos Commit
Queue](http://www.chromium.org/developers/tree-sheriffs/sheriff-details-chromium-os/commit-queue-overview)
to suit the particular change being tested.

**Per-repo and per-directory configuration**

Each repository may optionally contain a COMMIT-QUEUE.ini located in the root
directory of the repository, formatted as described below. In addition,
sub-directories of the repository may contain their own COMMIT-QUEUE.ini files.

When examining a change, the Commit Queue will look at the files that are
modified by the change, and will look for a config file for their common parent
directory. If no config file is found in that directory, it will continue up the
directory tree until it finds one. Note that this means the the configs for
subdirectories will fully override those for parent directories.

Warning: note that Chromite, in some cases, overrides or otherwise silently
changes or adds to the interpretation of the values below. For example, all
cheets-derived boards are excluded from certain tests by virtue of inclusion in
[a hardcoded
list](https://chromium.googlesource.com/chromiumos/chromite/+/38d0805d09bc583ed7a13c74c741294f28b5d577/cbuildbot/chromeos_config.py#593)
of boards.

**Specifying build targets to test with in the pre-CQ**

Some repositories or subdirectories (e.g. board specific overlays) contain code
that only affects a particular board or small set of boards. These repos can use
be tested in the pre-cq using a targeted set of build configs, rather than the
default set. This behavior is configured with COMMIT-QUEUE.ini as below.

    # Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
    # Use of this source code is governed by a BSD-style license that can be
    # found in the LICENSE file.
    # Per-project Commit Queue settings.
    # Documentation: http://goo.gl/5J7oND
    [GENERAL]
    # Board-specific pre-cq
    pre-cq-configs: lumpy-pre-cq stumpy-pre-cq

In this example, the pre-cq would test changes to this repo by launching
lumpy-pre-cq and stumpy-pre-cq tryjobs in parallel. Note, configs that are
specified in this list must have their config attribute pre_cq = True in order
to work. Once this config option is specified, it will take effect in the pre-cq
after the pre-cq-launcher is restarted.

**Submit changes in the Pre-CQ**

If a particular repository is not used by any builders in the CQ, then the CQ
doesn't add any value and it is safe to skip the CQ and submit changes in the
Pre-CQ. You can set this up by adding the following to your COMMIT-QUEUE.ini
file.

    # Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
    # Use of this source code is governed by a BSD-style license that can be
    # found in the LICENSE file.
    # Per-project Commit Queue settings.
    # Documentation: http://goo.gl/5J7oND
    [GENERAL]
    # This repository isn't used by the CQ at all, so we can submit in the
    # pre-cq.
    submit-in-pre-cq: yes

**Ignoring certain failures**

Some repositories (e.g. firmware repositories) contain code that is very
unlikely to break hardware or VM tests. When these tests fail, CLs in these
repositories would normally be rejected unfairly. To prevent this, it is
possible to configure what tests stages should be ignored in a
`COMMIT-QUEUE.ini` file. Here is an example `COMMIT-QUEUE.ini` file for the
`coreboot` repository, which lives in
`src/third_party/coreboot/COMMIT-QUEUE.ini`:

    # Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
    # Use of this source code is governed by a BSD-style license that can be
    # found in the LICENSE file.
    # Per-project Commit Queue settings.
    # Documentation: http://goo.gl/5J7oND
    [GENERAL]
    # Stages to ignore in the commit queue. If these steps break, your CL will be
    # submitted anyway. Use with caution.
    ignored-stages: HWTest VMTest

In the case where the `HWTest` or `VMTest` steps fail, cbuildbot will ignore the
failure and submit the CLs anyway.

### What stages can be ignored?

Currently, the following stages can be ignored:

*   `UnitTest`: Run unit tests for all packages.
*   `VMTest`: Run the ChromeOS smoke suite in a virtual machine
*   `HWTest`: Run the ChromeOS BVT in a virtual machine
*   `UploadPrebuilts`: Upload prebuilt binaries for all changes.
*   `Archive`: Miscellaneous steps for building and archiving artifacts,
    including building factory images, firmware tarballs, and recovery images.
