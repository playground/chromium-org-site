# Cros Deploy

## Overview

Cros Deploy is a script to deploy (install) a package to your Chromium OS
device. It copies the binary packages built in your chroot to the target device
and installs (emerges) on the device. Cros Deploy is written to replace
`cros_package_to_live` and `gmerge` in the near future.

NOTE: Cros Deploy does **NOT** build packages for you. Please make sure you have
built all the requested packages using `emerge` before invoking `cros deploy`.
NOTE: To be able to write to your rootfs partition, Cros Deploy may remount your
rootfs partition as read-write, or disables the rootfs verification on your
device.

Cros Deploy is part of the CrosD project which aims to provide tools with more
features, better and more consistent UI. Currently, CrosD tools include:

*   [Cros Deploy](cros-deploy.md): Install packages to device.
*   [Cros Flash](cros-flash.md): Update device with an image.

To know more about the design and implementation of Cros Deploy, please look at
the [design
doc](https://docs.google.com/a/google.com/document/d/1UdDTCC5V9R6VFMsiIzclMCSNwr-Iz42o6t_NfhEEGc4/)
(may be slightly outdated).

## Requirements

1.  Chroot: Cros Deploy needs to run in chroot.
2.  A SSH-able Chromium OS device: Any Chromium OS **test** image will work
    (base or dev images won't).

If your device is currently running a non-test image, you may want to use a USB
stick to image the device.

## Example usage

    cros deploy <device> <pkg1> <pkg2> ...
    <device>  : Required. The IP[:port] address (or hostname[:port]) of your ChromiumOS device; port is optional.
    <pkgs>    : Packages to install. 

For example, to install `cherrypy` package to your device:

    cros deploy 192.168.1.7 cherrypy

### Device

Device can be

    ssh://hostname[:port]

or

    hostname[:port]

Port number is the SSH port to use to connect to your device; it is optional
(defaults to 22)

### How to specify package names?

Cros Deploy does the best to figure out what packages you want to install, even
when you do not enter a complete/unique package name. When there is ambiguity,
Cros Deploy may prompt the user to choose.

A unique package name should includes three parts:

    ${category}/${package}-${version}

For example,

    dev-python/cherrypy-3.1.2

Cros Deploy accepts any of the following format:

    ${category}/${package}-${version}
    ${category}/${package}
    ${package}-${version}
    ${package}

When the given input such as ${package} (e.g. cherrypy) is unique, which is the
common case, Cros Deploy automatically selects the right package for you. If,
however, ${package} exists in multiple categories, Cros Deploy prompts the user
to choose. For example,

    Multiple matches found for cherrypy: 
      [0]: dev-python/cherrypy
      [1]: foo/cherrypy
    Enter your choice to continue [0-1]: 5

When there are multiple versions available, Cros Deploy chooses the best visible
one. For example, if you `cros_workon` a package, Cros Deploy will try to use
the 9999 version instead

### Installation root

By default, Cros Deploy installs all packages at root ("/"). User can override
this with "`--root`". Note that many dev packages are installed in` /usr/local`

## Known problems and fixes

### **Where is Cros Deploy?**

Cros Deploy is in the `chromite `repo. The script is
`chromite/cros/commands/cros_deploy.py`. If you cannot find it, update your tree
with` repo sync`.

### Failed to emerge package...**?**

Errors like the one below are likely caused by `emerge` missing on the target
system. (This can happen if the stateful partition is wiped after the OS image
has been deployed. Also remember that the target needs to run a **test** image.)

    13:14:55: ERROR: Failed to emerge package chromeos-chrome-46.0.2472.0_rc-r1.tbz2
    13:14:56: ERROR: Oops. Something went wrong.
    13:14:56: ERROR: cros deploy failed before completing.
    13:14:56: ERROR: <class 'chromite.lib.cros_build_lib.RunCommandError'>: return code: 127
    Failed command "ssh -p 22 '-oConnectionAttempts=4' '-oUserKnownHostsFile=/dev/null' '-oProtocol=2' '-oConnectTimeout=30' '-oServerAliveCountMax=3' '-oStrictHostKeyChecking=no' '-oServerAliveInterval=10' '-oNumberOfPasswordPrompts=0' -i /tmp/ssh-tmpb5S6kc/testing_rsa root@172.26.58.201 -- 'FEATURES=-sandbox' 'PORTAGE_CONFIGROOT=/usr/local' "CONFIG_PROTECT='-*'" 'PKGDIR=/usr/local/tmp/cros-deploy/tmp.6xIVEajmyJ/packages' 'PORTDIR=/usr/local/tmp/cros-deploy/tmp.6xIVEajmyJ' 'PORTAGE_TMPDIR=/usr/local/tmp/cros-deploy/tmp.6xIVEajmyJ/portage-tmp' emerge --usepkg /usr/local/tmp/cros-deploy/tmp.6xIVEajmyJ/packages/chromeos-base/chromeos-chrome-46.0.2472.0_rc-r1.tbz2 '--root=/'", cwd=None, extra env={'LC_MESSAGES': 'C'}

### No space left on device...?

The following mostly occurs when deploying large packages such as Chrome:

    rsync: write failed on "/usr/local/tmp/cros-deploy/tmp.Ts2Ayh1nKT/packages/chromeos-base/chromeos-chrome-46.0.2472.0_rc-r1.tbz2": No space left on device (28)
    rsync error: error in file IO (code 11) at receiver.c(393) [receiver=3.1.1]
    rsync: [sender] write error: Broken pipe (32)
    13:21:46: ERROR: Oops. Something went wrong.
    13:21:46: ERROR: cros deploy failed before completing.
    13:21:46: ERROR: <class 'chromite.lib.cros_build_lib.RunCommandError'>: return code: 11

This happens because the target's `/usr/local/tmp` has insufficient space to
download the package. A workaround is pointing `/usr/local/tmp` to `/tmp`:

    rm -r /usr/local/tmp
    ln -s /tmp /usr/local/tmp

### Complete usage

View the most up-to-date usage by running `cros deploy -h`

    usage: cros deploy [-h]
                       [--log-level {fatal,critical,error,warning,info,debug}]
                       [--debug] [--nocolor] [--board BOARD] [--no-strip]
                       [--unmerge] [--root ROOT] [--no-clean-binpkg]
                       [--emerge-args EMERGE_ARGS] [--private-key PRIVATE_KEY]
                       device packages [packages ...]
    Deploy the requested packages to the target device.
      This command assumes the requested packages are already built in the
      chroot. This command needs to run inside the chroot for inspecting
      the installed packages.
      Note: If the rootfs on your device is read-only, this command
      remounts it as read-write. If the rootfs verification is enabled on
      your device, this command disables it.
      
    positional arguments:
      device                IP[:port] address of the target device.
      packages              Packages to install.
    optional arguments:
      -h, --help            show this help message and exit
      --board BOARD         The board to use. By default it is automatically
                            detected. You can override the detected board with
                            this option.
      --no-strip            Do not run strip_package to filter out preset paths in
                            the package. Stripping removes debug symbol files and
                            reduces the size of the package significantly.
                            Defaults to always strip.
      --unmerge             Unmerge requested packages.
      --root ROOT           Package installation root, e.g. '/' or
                            '/usr/local'(default: '/').
      --no-clean-binpkg     Do not clean outdated binary packages. Defaults to
                            always clean.
      --emerge-args EMERGE_ARGS
                            Extra arguments to pass to emerge.
      --private-key PRIVATE_KEY
                            SSH identify file (private key).
    Debug options:
      --log-level {fatal,critical,error,warning,info,debug}
                            Set logging level to report at.
      --debug               Alias for `--log-level=debug`. Useful for debugging
                            bugs/failures.
      --nocolor             Do not use colorized output (or `export NOCOLOR=true`)
    To deploy packages:
      cros deploy device power_manager cherrypy
    To uninstall packages:
      cros deploy --unmerge cherrypy
    For more information of cros build usage:
      cros build -h
