# Chromium OS

Chromium OS is an open-source project that aims to build an operating system
that provides a fast, simple, and more secure computing experience for people
who spend most of their time on the web. Here you can review the project's
design docs, obtain the source code, and contribute. To learn more about the
project goals, read the [announcement blog
post](http://googleblog.blogspot.com/2009/11/releasing-chromium-os-open-source.html).

### Videos

*   [What is Google Chrome OS?](http://www.youtube.com/watch?v=0QRO3gKj3qw)
*   [Chromium OS Security](http://www.youtube.com/watch?v=A9WVmNfgjtQ)
*   [Chromium OS & Open Source](http://www.youtube.com/watch?v=KA5RQv9mBoY)
*   [Chromium OS Fast Boot](http://www.youtube.com/watch?v=mTFfl7AjNfI)

### For contributors

*   [Getting involved](getting-involved-in-chromium-os.md)
*   [Getting developer hardware](getting-dev-hardware_index.md)
*   [Official Chrome OS
    Devices](developer-information-for-chrome-os-devices/index.md)
*   [Process for
    contributing](http://dev.chromium.org/developers/contributing-code)
*   [Tree
    sheriffs](http://www.chromium.org/developers/tree-sheriffs/sheriff-details-chromium-os)
*   [Useful developer related Chrome
    extensions](../developers/useful-extensions.md)
*   [2014 Chrome OS Firmware Summit](2014-firmware-summit/index.md)

### For UI designers

*   [User experience: Chromium/OS](http://dev.chromium.org/user-experience)

### For developers

*   [Optimize Android Apps for Chrome OS device](android-apps.md)
*   [Sideload Android apps on your
    Chromebook](sideload-android-apps-on-chromebook.md)
*   [Developing apps on your Chromium OS
    device](developing-apps-on-your-chromium-os-device.md)
*   [Chromium OS Build](build/index.md)

### For everyone

*   [Chromium OS FAQ](chromium-os-faq.md)
*   Join the right [discussion groups](../developers/discussion-groups/index.md)
    (if in doubt, go to
    [chromium-os-discuss](http://groups.google.com/a/chromium.org/group/chromium-os-discuss)
    or use the IRC channel irc.freenode.net/#chromium-os-users)
*   [Report bugs](http://dev.chromium.org/for-testers/bug-reporting-guidelines)
    or [view existing bugs](https://bugs.chromium.org/p/chromium/issues/list)

### ![image](comp2_200x146.jpg)

### Source code

*   First time? Use the [Chromium OS Quick Start Guide](quick-start-guide.md)
*   Read the [Chromium OS Developer Guide](developer-guide/index.md) to learn
    how to build the source
*   [Overview](developer-guide/directory-structure.md) of the source
*   [Browse](https://chromium.googlesource.com/) the source
*   Coding Style
    *   [Chromium OS Python Style Guide](python-style-guidelines.md)
    *   [Chromium OS Shell Style Guide](shell-style-guidelines.md)
    *   For other languages, follow the [Google Style
        Guide](https://github.com/google/styleguide/#google-style-guides)
    *   For third-party code, follow the external project style guides
*   [Testing](testing/_index.md)
*   [Licenses](licenses.md)

### Developer docs

*   [Chromium OS Developer Guide](developer-guide/index.md)
*   [High-Level Developer FAQ](how-tos-and-troubleshooting/developer-faq.md)
*   [Firmware (Coreboot / U-Boot) Overview and Porting
    Guide](firmware-porting-guide/index.md)
*   [Embedded Controller (EC) Development](ec-development/index.md)
*   [All How Tos and Troubleshooting](how-tos-and-troubleshooting/index.md)
*   [Poking around Your Chrome OS
    Device](poking-around-your-chrome-os-device/index.md)
*   [Design docs](http://www.chromium.org/chromium-os/chromiumos-design-docs)

### Hardware Reference docs and schematics

Software for the microcontroller on these USB Type-C example designs is in the
[EC source code](ec-development/index.md)

*   [USB Type-C debug dongle and sniffer for USB-PD
    communications](twinkie/index.md)
*   [USB Type-C to DisplayPort Adapter](dingdong/index.md)
*   [USB Type-C to HDMI Adapter](hoho/index.md)
*   [45W USB Type-C Charging Adapter Control Board](minimuffin/index.md)
*   [Cable and Adapter Tips and
    Tricks](cable-and-adapter-tips-and-tricks/index.md)
*   [USB Type-C functional testing
    board](https://www.chromium.org/chromium-os/plankton)
