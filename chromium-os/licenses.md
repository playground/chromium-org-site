# Licenses

Chrome OS credits all the open source projects used in each version of Chrome OS
on each device.

There is no single license file that is correct for all devices and all
versions, so please go to settings/help (or type
[chrome://help](javascript:void(0);)) and click on "open source software" (or
type [chrome://credits](javascript:void(0);) and
[chrome://os-credits](javascript:void(0);)).

If you would like an online version, a not necessarily up to date version of the
OS credits can be found here:
<http://src.chromium.org/viewvc/chrome/trunk/src/chrome/browser/resources/chromeos/about_os_credits.html>
