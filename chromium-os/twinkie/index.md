# USB-PD Sniffer

## Overview

This page describes a USB-PD sniffing dongle with Type-C connectors. The dongle
can be supported by Chrome devices as part of a USB-Type C implementation.

## Hardware Capabilities

*   Sniffing USB Power Delivery traffic on both Control Channel lines (CC1/CC2)
*   Transparent interposer on a USB Type-C connection
*   Monitoring VBUS and VCONN voltages and currents
*   Injecting PD packets on CC1 or CC2
*   Putting Rd/Rp/Ra resistors on CC1 or CC2

![image](Twinkie-diagram.png)

Complete schematics, layout, and enclosure are attached at the bottom of this
page.

## **Firmware Source Code**

The firmware is located In the Chromium Embedded Controller repository under
*[board/twinkie/](https://chromium.googlesource.com/chromiumos/platform/ec/+/master/board/twinkie/)*
:

<https://chromium.googlesource.com/chromiumos/platform/ec/+/master/board/twinkie/>

### **Building Firmware**

Within your Chromium OS chroot, the syntax is:

    For the impatient, here is a very cut-down version of the detailed instructions. 

    If anything here gives you trouble, refer to the [official documentation](http://www.chromium.org/chromium-os/developer-guide) instead.

    ## Install the prerequisite tools

    goobuntu$  	sudo apt-get install git-core gitk git-gui subversion curl

    goobuntu$  	git clone \ <https://chromium.googlesource.com/chromium/tools/depot_tools.git>

    goobuntu$  	export PATH=$(pwd)/depot_tools:$PATH

    ## Check out the sources

    goobuntu$  	mkdir myhappyfundir

    goobuntu$  	cd myhappyfundir

    goobuntu$  	repo init -u \

    https://chromium.googlesource.com/chromiumos/manifest.git \

    --repo-url https://chromium.googlesource.com/external/repo.git \

    -g minilayout --config-name

    goobuntu$  	repo sync

    ## Enter the chroot

    goobuntu$  	cros_sdk

    ## Install the board-specific compiler toolchain

    chroot$  	./setup_board --board=peppy

    ## Check out the EC sources

    chroot$  loman add chromiumos/platform/ec src/platform/ec --remote cros

    chroot$  loman add … 

    chroot$	exit

    goobuntu$  	repo sync

    ## Make sure the EC sources are up to date

    goobuntu$  	cd src/platform/ec

    goobuntu$  	repo sync .

    ## Build things

    goobuntu$ cros_sdk

    chroot$	 cd ../platform/ec

    chroot$  make BOARD=twinkie

    chroot$	 exit

    goobuntu$	ls -l build/*

Alternately, you can use the pre-built firmware attached at the bottom of this
page.

### **Flashing Firmware**

The USB-PD dongle behaves as a USB DFU device when the ID pin is grounded on the
USB micro-B connector.

This is done by plugging an A-A USB cable into an A-to-microB OTG adapter, as
shown in the photo below.

Within your Chromium OS chroot:

    ./util/flash_ec --board=twinkie

or on Ubuntu Linux:

    sudo apt-get install dfu-util

    sudo dfu-util -a 0 -s 0x08000000 -D ec-twinkie.bin

![Programming Twinkie with OTG and A-to-A cables](twinkie_programming.jpg)

## Using the Integrated Command Line over USB

The USB-PD dongle exports its internal command-line console as a pair of USB
bulk endpoints.

TBD.

On a Linux system, you get the console as a /dev/ttyUSB*n* device using the
usbserial kernel module :

    echo '18d1 500A' | sudo tee /sys/bus/usb-serial/drivers/generic/new_id

If this fails, you might need to run `sudo modprobe usbserial` first.

## Using as a PD Packet Sniffer

You can use the opensource [Sigrok](http://sigrok.org) framework to acquire and
decode USB Power Delivery traces with the USB-PD dongle. You can then use
[Pulseview ](http://sigrok.org/wiki/PulseView)to display them.

The patches for the Sigrok hardware driver for the dongle and the USB Power
Delivery protocol decoder are not yet available. If you use Ubuntu Trusty, you
can try the experimental packages with that support:

[libsigrok1_20141111-1_amd64.deb](http://storage.googleapis.com/chromeos-vpa/sigrok-20141111/libsigrok1_20141111-1_amd64.deb)

[libsigrokdecode1_20141111-1_amd64.deb](http://storage.googleapis.com/chromeos-vpa/sigrok-20141111/libsigrokdecode1_20141111-1_amd64.deb)

[sigrok-cli_20141111-1_amd64.deb](http://storage.googleapis.com/chromeos-vpa/sigrok-20141111/sigrok-cli_20141111-1_amd64.deb)

[pulseview_20141111-1_amd64.deb](http://storage.googleapis.com/chromeos-vpa/sigrok-20141111/pulseview_20141111-1_amd64.deb)

the ARM version of the packages :

[libsigrok1_0.3.0-7df7cdb-1_armhf.deb](http://storage.googleapis.com/chromeos-vpa/sigrok-20150303/libsigrok1_0.3.0-7df7cdb-1_armhf.deb)

[libsigrokdecode1_0.3.0-8a6e61d-1_armhf.deb](http://storage.googleapis.com/chromeos-vpa/sigrok-20150303/libsigrokdecode1_0.3.0-8a6e61d-1_armhf.deb)

[sigrok-cli_0.5.0-c9edfa2-1_armhf.deb](http://storage.googleapis.com/chromeos-vpa/sigrok-20150303/sigrok-cli_0.5.0-c9edfa2-1_armhf.deb)

[pulseview_20150303-321ce42-1_armhf.deb](http://storage.googleapis.com/chromeos-vpa/sigrok-20150303/pulseview_20150303-321ce42-1_armhf.deb)

Once you have downloaded the .deb packages, you can use the following
command-line to install them easily:

    sudo dpkg -i libsigrok1_20141111-1_amd64.deb libsigrokdecode1_20141111-1_amd64.deb sigrok-cli_20141111-1_amd64.deb pulseview_20141111-1_amd64.deb
    sudo apt-get install -f

You can also use the unofficial package source by copying
[chromium-sigrok.list](chromium-sigrok.list) to `/etc/apt/sources.list.d/`.

    sudo apt-get update
    sudo apt-get install sigrok

To regenerate the binary packages above, you can get the full sources of the
package version as listed below:

for libsigrok :

    git clone https://chromium.googlesource.com/chromiumos/third_party/libsigrok
    cd libsigrok
    git fetch origin refs/changes/54/229654/2 && git checkout FETCH_HEAD

for libsigrokdecode :

    git clone https://chromium.googlesource.com/chromiumos/third_party/libsigrokdecode
    cd libsigrokdecode
    git fetch origin refs/changes/80/205180/2 && git checkout FETCH_HEAD

If you want to do a full build from sources of sigrok and pulseview with Twinkie
support,

now the USB PD protocol decoder is now upstreamed in libsigrokdecode (and
sigrok-cli / pulseview are working out-of-the-box).

You only need to add the Chromium Twinkie hardware driver in libsigrok : [last
version of the patch applying on
libsigrok-0.4.0](https://github.com/vpalatin/libsigrok/commit/c159668460c9568a356d4efefc05a8e312727195)

A recipe to do this build is written down in [Build Sigrok and Pulseview from
sources](build-sigrok-and-pulseview-from-sources.md).

## Capturing traces with the Sigrok tool

    sudo sigrok-cli -d chromium-twinkie --continuous -o test.sr

### **Displaying and decoding PD traces**

    pulseview test.sr &

Add the pd_bmc decoder from the Decoders menu, then edit the instantiated pd_bmc
decoder to select the appropriate CC line and the usb_pd_packet stack decoder.

![image](PulseviewSourceCap_request.png)

#### ![image](PulseviewRequestAccept.png)

## Using as a Power Sink

1.  Use the tw sink command. Exit sink mode by using the reboot command.
2.  When a power source is detected, the dongle negotiates a power contract,
    activating the green LED for 5V, the red LED for 20V, or the blue LED for
    other voltage.
3.  You can change the maximum negotiated voltage with the following command in
    the dongle USB shell:
    pd 0 dev 12
    This example sets a limit of 12 V.
