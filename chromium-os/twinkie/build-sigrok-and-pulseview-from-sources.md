# Build Sigrok and Pulseview from sources

If you want to build Sigrok and Pulseview from sources with the Twinkie support
in libsigrok,

the easiest path is to use the last released versions:

[libsigrok-0.4.0](http://sigrok.org/gitweb/?p=libsigrok.git;a=shortlog;h=refs/heads/libsigrok-0.4.x)
(with [twinkie-0.4.0
patch](https://github.com/vpalatin/libsigrok/commit/c159668460c9568a356d4efefc05a8e312727195))
[libsigrokdecode-0.4.0](http://sigrok.org/gitweb/?p=libsigrokdecode.git;a=shortlog;h=refs/heads/libsigrokdecode-0.4.x)
[sigrok-cli-0.6.0](http://sigrok.org/gitweb/?p=sigrok-cli.git;a=shortlog;h=refs/heads/sigrok-cli-0.6.x)
[pulseview-0.3.0](http://sigrok.org/gitweb/?p=pulseview.git;a=shortlog;h=refs/heads/pulseview-0.3.x)

Here is a recipe working on Ubuntu Trusty LTS:

    sudo apt-get install gcc g++ libtool automake autoconf libftdi-dev libusb-1.0-0-dev libglib2.0-dev check
    sudo apt-get install libzip-dev libglibmm-2.4-dev doxygen python3.4-dev python-gobject-dev
    sudo apt-get install qt4-dev-tools libqt4-dev cmake
    sudo apt-get install libboost-dev libboost-filesystem-dev libboost-system-dev libboost-thread-dev libboost-test-dev
    git clone https://github.com/vpalatin/libsigrok.git -b twinkie-0.4.0
    git clone https://github.com/vpalatin/libsigrokdecode.git -b twinkie-0.4.0
    git clone https://github.com/vpalatin/sigrok-cli.git -b twinkie-0.6.0
    git clone https://github.com/vpalatin/pulseview.git -b twinkie-0.3.0
    cd libsigrok
    ./autogen.sh
    ./configure --prefix=/usr
    make install
    cd ../libsigrokdecode/
    ./autogen.sh
    ./configure --prefix=/usr
    make install
    cd ../sigrok-cli/
    ./autogen.sh
    ./configure --prefix=/usr
    make install
    cd pulseview/
    cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .
    make install
