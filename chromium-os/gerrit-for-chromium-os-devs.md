# Gerrit for Chromium OS devs

**This page is under construction and should not be considered normative**

### If you're looking for the Chromium guide, it can be found [here](../developers/contributing-code/index.md)

This document assumes that you've downloaded the source code and you're ready to
make a change. If you want to download the source code, [here are some
instructions](developer-guide/index.md).

[Tips and Tricks](tips-and-tricks-for-chromium-os-developers.md)

## Did you sign a CLA?

Read '[Legal Stuff](../developers/contributing-code/index.md)' in the Chromium
guide. There is a Contributor Licensing Agreement if you or your company has not
already signed it.

## Uploading your code to Gerrit

Repo is the primary code managment tool for Chrome OS. Repo manages multiple git
repositories under one project. Repo usage can be found on the Android site.
They use it too. http://source.android.com/source/using-repo.html

Upload your current project: TODO

Changes that span multiple projects: TODO

Interdepenent changes (CQ-DEPEND): TODO

## Finding a reviewer

## Code review process

## +1, +2, Verified, Try-bot ready?

## Commit queue

## Don't be a chump

To get the path for a mysterious package: repo info <package name>
