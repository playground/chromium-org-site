# Getting Involved

Here are some ways you can get involved with Chromium OS:

*   Join some of our [developer discussion groups](discussion-groups.md).
*   Join the IRC channel irc.freenode.net/#chromium-os
*   Contribute to our [unofficial
    wiki](http://code.google.com/p/chromium-os/w/list) (or just read [about the
    wiki](http://code.google.com/p/chromium-os/wiki/AboutThisWiki)).
*   Help with testing Chromium OS:
    *   [Test Chromium](http://dev.chromium.org/for-testers) on Chromium OS
        ([build instructions](developer-guide/index.md)).
    *   Test Chromium OS on [development
        hardware](getting-dev-hardware_index.md).
    *   Look at the [bug list](http://code.google.com/p/chromium/issues/list)
        and comment on bugs when you have useful information about them:
        *   If the bug is marked with status "Unconfirmed", try the reproduction
            steps provided by the original reporter. Update the bug to say
            whether it happens for you too.
        *   Is it a duplicate of another bug? Add a comment referencing the
            other bug.
        *   Provide enough feedback and you might get bug editing privileges.
*   Develop Chromium OS:
    *   Adopt a bug that's marked
        [helpwanted](http://code.google.com/p/chromium/issues/list?q=label:Hotlist-GoodFirstBug&can=2).
    *   Submit [patches](http://dev.chromium.org/developers/contributing-code)
        (submit enough and you can [become a
        committer](http://dev.chromium.org/getting-involved/become-a-committer)!).
    *   Port Chromium OS to [new
        processors](getting-dev-hardware/dev-hardware-list/index.md).
    *   Add support for more chipsets and devices.
