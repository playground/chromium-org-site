# Boot Milestones

The Chromium OS boot sequence has a number of key milestones for services and
tasks that must be accomplished during boot. For the majority of jobs you'll be
able to use one of these rather than creating complex Upstart start on matches
for your job.

**start on startup**

*   Jobs that can run with only /proc, /sys and /dev. Other filesystems,
    including /tmp, are not available.
*   Core components of boot that run in parallel with the key chromeos_startup
    script.
*   Most jobs here will likely also need to be explicitly listed as dependencies
    of boot-services.

**start on starting boot-services**

*   Key boot services, including the X server and UI; and dependencies of system
    services.
*   Jobs may use the stateful partition or /tmp.
*   Jobs may depend on core services such as udevd from being running, see
    /etc/init/boot-services.conf. There is no need to separately depend on them.

**start on starting system-services**

*   Remaining system services that are not required for the user to login; or
    the client software handles the service being started in an eventful way.
*   Jobs may depend on any core service or boot service being running, such
    udevd and dbus, there is no need to separately depend on them.
*   *If in doubt, this is the most likely location for your job.*

**start on started system-services**

*   Post-processing tasks of boot, for example those that examine log files.

**start on starting failsafe**

*   Equivalent to **starting system-services**, except that these services will
    *also* be run in the case of a failed boot.
