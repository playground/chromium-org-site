# The Chromium OS Platform

## Introduction

Chromium OS has many userland programs and sub-projects that make up the
"platform" of the system. You can find them listed (as well as more details on
each one) in the main [Packages page](packages/index.md). This guide is about
the platform those projects build off. It is meant as the authoritative guide
when writing a new project (or updating an existing one). You should not deviate
from the choices outlined here without a very very good reason (and chances are
that what you think is a very good reason is really not).

## Goals

Here are some explicit goals for the Chromium OS platform:

*   We should align & share code with Chromium whenever possible.
*   C++ is the language you should be using.
    *   Only use C when C++ is not a viable option (e.g.
        kernel/bootloader/etc...).
    *   Shell code is rarely acceptable beyond short snippets.
*   Each platform project should align & share code whenever possible.
    *   Use the same build system.
    *   Use the same set of utility libraries.

## Decisions

### Build System

Rather than have every package implement its own build system (scons, cmake,
autotools, hand written makefiles, common.mk, etc...), The One True Build System
we use is [GYP](https://code.google.com/p/gyp/). More details on how it
integrates can be found in the [Platform2
page](getting-started-with-platform2.md).

### Style

We follow the various Google style guides for Chromium OS projects. For third
party projects, you should follow the style that project has decided on.

*   [C++](http://www.chromium.org/developers/coding-style) - Chromium C++ Style
    Guide
*   [Python](python-style-guidelines.md) - Chromium OS Python Style Guide (a
    superset of the Google Python Style Guide)
*   [Shell](shell-style-guidelines.md) - Chromium OS shell-script Style Guide

### Inter-Process Communication (IPC)

All communication between processes is done via D-Bus. See the ["D-Bus best
practices"
doc](https://chromium.googlesource.com/chromiumos/docs/+/master/dbus_best_practices.md)
for more information.

#### Data Serialization

If you need to serialize data, either for reading back later (from
disk/cache/etc...) or for passing to another process (via ipc/network/etc...),
then you should use:

*   IPC (local machine or network)
    *   [dbus](http://dbus.freedesktop.org/) - for simple stuff, dbus itself
        includes marshaling of basic types
    *   [protocol buffers](https://code.google.com/p/protobuf/) (protobufs) -
        for larger/binary content, or when communicating via the network
*   Reading/Writing to disk
    *   base::JSON\* - for general saved runtime state and/or config files
        (reading & writing)
    *   base::IniParser - for simple config files (read only); but json is still
        preferred

### Utility Libraries

First off, whenever you want to do some new that you suspect someone else has
done already, you should look through Chromium's base library and Chromium OS
library (libchromeos). We build & ship these in Chromium OS; see the [libchrome
page](packages/libchrome.md) and [libchromeos page](packages/libchromeos.md) for
building against them.

Here is a common run down of functionality provided by libchrome that platform
projects use:

logging base/logging.h command line flags base/command_line.h string parsing
base/strings/\* file path manipulation base/file_path.h
file read/write utilities base/file_util.h JSON manipulation base/json/\* INI
file reading base/ini_parser.h DBus bindings dbus/\*
Message Loop base/message_loop.h

For libchromeos, see the [libchromeos page](packages/libchromeos.md) for and
overview of the functionality implemented there.

In particular, you should avoid using:

*   [gflags](https://code.google.com/p/gflags/) - command line parsing
*   [glog](https://code.google.com/p/google-glog/) - logging

### Metrics

If you want your program to gather & report statistics at runtime (like UMA
stats), then you should use:

*   metrics - the Chromium OS metrics library

### Sandboxing (User/Process/etc... Isolation)

All daemons should always be run with as restricted permissions as possible.
This often means using a dedicated UID/GID, chroots, and seccomp filters. This
is all handled by:

*   [minijail](developer-guide/chromium-os-sandboxing.md) - the Chromium OS jail
    manager

### Install Locations

Type Where public executables `/usr/bin/` public libraries `/usr/$(get_libdir)/` internal executables/libraries/plugins `/usr/$(get_libdir)/${PN}/` data files `/usr/share/${PN}/`
include files `/usr/include/`

You should avoid /sbin/ and /bin/ and /usr/sbin/ paths. They are unnecessary and
you should just stick to /usr/bin/.

Similarly, you should avoid /$(get_libdir)/ (e.g. /lib/).

You should avoid /opt/. It is for exceptional cases only (mostly
non-public/binary-only projects).

### Crypto

You should use [OpenSSL](http://www.openssl.org/) for crypto services. A few
projects still use
[NSS](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS), but the
plan is to migrate away from that, so no new code should be using it.

### Testing

Since you will of course be writing unittests for all of your projects, you
should use (which is to say unittests are a hard requirement):

*   [gtest](https://code.google.com/p/googletest/) - unittest framework
*   [gmock](https://code.google.com/p/googlemock/) - mocking framework
