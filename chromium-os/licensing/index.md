# Licensing in Chromium OS

**Note: This is not legal advice; if you want that, consult a lawyer. This
section discusses purely technical measures that are available in the Chromium
OS distribution.**

Select the area that you're interested in:

*   [Licensing Handling For Package
    Owners](../licensing-for-chromiumos-package-owners.md)
    *   e.g. people writing ebuilds
*   [Licensing Handling For OS Builders](building-a-distro.md)
    *   e.g. people generating images
*   [Internal Licensing Handling For Chromium OS
    Developers](../licensing-for-chromiumos-developers.md)
    *   e.g. people dealing with the license generation logic
