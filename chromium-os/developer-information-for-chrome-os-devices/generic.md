# Generic Chrome OS Device Instructions

## Introduction

Unfortunately, a page specific to your device does not yet exist. This page
contains generic information for working with your Chrome OS device that is
interesting and/or useful to software developers. It should hopefully work for
your device, but there are no guarantees.

For general information about getting started with developing on Chromium OS
(the open-source version of the software on the Chrome Notebook), see the
[Chromium OS Developer Guide](../developer-guide/index.md).

## Developer Mode

**Caution: Modifications you make to the system are not supported by Google, may
cause hardware, software or security issues and may void warranty.**

An unrelated note: Holding just Refresh and poking the Power button hard-resets
the machine without entering Recovery. That's occasionally useful, but use it
with care - it doesn't sync the disk or shut down politely, so there's a nonzero
chance of trashing the contents of your stateful partition.

### Introduction

Enabling Developer mode is the first step to tinkering with your Chromebook.
With Developer mode enabled you can do things like poke around on a command
shell (as root if you want), install Chromium OS, or try other OS's. Note that
Developer mode turns off some security features like verified boot and disabling
the shell access. If you want to browse in a safer, more secure way, leave
Developer mode turned OFF. Note: Switching between Developer and Normal
(non-developer) modes will remove user accounts and their associated information
from your Chromebook.

### Entering

To invoke Recovery mode, you hold down the ESC and Refresh (F3) keys and poke
the Power button.
Note: On some devices, the keyboard might have a lock button and the power
button is on the side of the device.

To enter Dev-mode you first invoke Recovery, and at the Recovery screen press
Ctrl-D (there's no prompt - you have to know to do it). It will ask you to
confirm, then reboot into dev-mode.

Dev-mode works the same as always: It will show the scary boot screen and you
need to press Ctrl-D or wait 30 seconds to continue booting.

### USB Boot

By default, USB booting is disabled. Once you are in Dev-mode and have a root
shell, you can run:

    sudo crossystem dev_boot_usb=1

and reboot once to boot from USB drives with Ctrl-U.

### Legacy Boot

For newer x86 systems, SeaBIOS firmware might be included to support booting
images directly like a legacy BIOS would. Note: the BIOS does not provide a
fancy GUI for you, nor is it easy to use for beginners. You will need to
manually boot/install your alternative system.

For ARM systems, there is no legacy boot mode.

Like USB boot, support for this is disabled by default. You need to get into
Dev-mode first and then run:

    sudo crossystem dev_boot_legacy=1

and reboot once to boot legacy images with Ctrl-L.

### Leaving

To leave Dev-mode and go back to normal mode, just follow the instructions at
the scary boot screen. It will prompt you to confirm.

If you want to leave Dev-mode programmatically, you can run `crossystem
disable_dev_request=1; reboot` from a root shell. There's no way to enter
Dev-mode programmatically, and just seeing the Recovery screen isn't enough -
you have to use the three-finger salute which hard-resets the machine first.
That's to prevent a remote attacker from tricking your machine into dev-mode
without your knowledge.
