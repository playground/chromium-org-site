# Developer Information for Chrome OS Devices

## Introduction

This page provides developer-related information for Chrome devices. These are
systems that *shipped from the factory* with Google Chrome OS on them. For
details about Google Chrome OS and how it differs from Chromium OS, see the
[note in the FAQ about Google Chrome
OS](http://www.chromium.org/chromium-os/chromium-os-faq#TOC-What-s-the-difference-between-Chrom).
Chrome OS devices typically require special setup in order to be used for
Chromium OS development.

**Caution: Modifications you make to the system are not supported by Google, may
cause hardware, software or security issues and may void warranty.**

Remember: Chrome OS devices are **not** general-purpose PCs. We believe you
should be able to hack on your own property, but if you do it's not our fault if
something breaks.

Googlers not finding what they're looking for may want to look at
[go/cros-names](http://goto.google.com/cros-names).

## Notebooks and Desktops

These are your standard Chromebook/Chromebox/etc... devices. See the list at the
bottom of the page.

## Routers

These are WiFi routers that are marketed under the name
[OnHub](https://on.google.com/hub/).

**Release date** ** Manufacturer** ** Model** ** Project code name** ** Board
name(s)** **Base board** August 2015 TP-LINK [OnHub Router
TGR1900](tp-link-onhub-tgr1900/index.md) Whirlwind whirlwind storm November 2015
ASUS OnHub SRT-AC1900 Arkham arkham storm November 2016 Google [Google
WiFi](https://madeby.google.com/wifi/) Gale gale gale

## USB Type-C

**Release date** ** Model** ** Board name(s)** January 2015 [USB Type-C to DP
Adapter](../dingdong/index.md) dingdong January 2015 [USB Type-C to HDMI
Adapter](../hoho/index.md) hoho January 2015 USB Type-C to VGA Adapter hoho, but
substitute a DP to VGA chip January 2015 [USB-PD Sniffer](../twinkie/index.md)
twinkie
