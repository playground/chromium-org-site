# Energy Aware Scheduling

Energy Aware Scheduling reading list

In Kernel Switcher (IKS)<https://lwn.net/Articles/481055/>
<https://lwn.net/Articles/501501/>
<https://lwn.net/Articles/539840/>
<https://wiki.linaro.org/projects/big.LITTLE.MP/Big.Little.Switcher/Docs/in-kernel-code>
<https://events.linuxfoundation.org/images/stories/slides/elc2013_poirier.pdf>Energy-Aware
Scheduling (EAS)<http://lwn.net/Articles/602479/>
<http://www.linaro.org/blog/core-dump/energy-aware-scheduling-eas-project/>
<https://lwn.net/Articles/609969/>
<https://docs.google.com/document/d/1YV44gufokmuKbeNG8vjOJirEY_GdsJymxP2ayHG4FAM/edit>Power
State Coordination Interface
(PSCI)<http://infocenter.arm.com/help/topic/com.arm.doc.den0022c/DEN0022C_Power_State_Coordination_Interface.pdf>ARM
Trusted Firmware (ARM TF)<https://github.com/ARM-software/arm-trusted-firmware>
<http://www.slideshare.net/linaroorg/arm-trusted-firmareforarmv8alcu13>
<http://www.slideshare.net/linaroorg/hkg15502-arm-trusted-firmware-evolution>
<http://www.slideshare.net/linaroorg/hkg15-505-power-management-interactions-with-optee-repaired>
ARMv8-A
Architecture<http://www.arm.com/products/processors/armv8-architecture.php>big.LITTLE<http://www.arm.com/files/pdf/big_LITTLE_Technology_the_Futue_of_Mobile.pdf>
<http://www.arm.com/products/processors/technologies/biglittleprocessing.php>
<https://wiki.linaro.org/projects/big.LITTLE.MP>
<http://events.linuxfoundation.org/sites/events/files/slides/clement-smp-bring-up-on-arm-soc.pdf>
cpufreq<http://www.ibm.com/developerworks/linux/library/l-cpufreq-1/index.html>
<https://www.kernel.org/doc/Documentation/cpu-freq/>cpuidle<https://lwn.net/Articles/384146/>
<https://www.kernel.org/doc/Documentation/cpuidle/>
