# QUIC, a multiplexed stream transport over UDP

QUIC is a new transport which reduces latency compared to that of TCP. On the
surface, QUIC is very similar to TCP+TLS+HTTP/2 implemented on UDP. Because TCP
is implemented in operating system kernels, and middlebox firmware, making
significant changes to TCP is next to impossible. However, since QUIC is built
on top of UDP, it suffers from no such limitations.

**Key features of QUIC over existing TCP+TLS+HTTP2 include **

*   Dramatically reduced connection establishment time
*   Improved congestion control
*   Multiplexing without head of line blocking
*   Forward error correction
*   Connection migration

**Documentation**

*   [QUIC
    overview](https://docs.google.com/document/d/1gY9-YNDNAB1eip-RTPbqphgySwSNSDHLq9D5Bty4FSU/edit?usp=sharing)
*   [QUIC FAQ](quic-faq.md)
*   [QUIC wire
    specification](https://docs.google.com/document/d/1WJvyZflAO2pq77yOLbp9NsGjC1CHetAXV8I0fQe-B_U/edit?usp=sharing)
*   [QUIC crypto design
    doc](https://docs.google.com/document/d/1g5nIXAIkN_Y-7XJW5K45IblHd_L2f5LTaDUDwvZ5L6g/edit?usp=sharing)
*   [Getting started with the QUIC toy client and server](playing-with-quic.md)
*   [QUIC tech talk](https://www.youtube.com/watch?v=hQZ-0mXFmk8)
*   [QUIC
    Discovery](https://docs.google.com/document/d/1i4m7DbrWGgXafHxwl8SwIusY2ELUe8WX258xt2LFxPM/edit?usp=sharing)
*   [QUIC FEC
    v1](https://docs.google.com/document/d/1Hg1SaLEl6T4rEU9j-isovCo8VEjjnuCPTcLNJewj7Nk/edit?usp=sharing)
*   [QUIC flow
    control](https://docs.google.com/document/d/1F2YfdDXKpy20WVKJueEf4abn_LVZHhMUMS5gX6Pgjl4/edit?usp=sharing)

**IETF Material**

*   [QUIC: A UDP-Based Secure and Reliable Transport for
    HTTP/2](https://tools.ietf.org/html/draft-tsvwg-quic-protocol)
    (draft-tsvwg-quic-protocol)
*   [QUIC Loss Recovery And Congestion
    Control](https://tools.ietf.org/html/draft-tsvwg-quic-loss-recovery)
    (draft-tsvwg-quic-loss-recovery)
*   Presentations from the IETF-93 BarBoF on QUIC
    ([Video](http://recordings.conf.meetecho.com/Playout/watch.jsp?recording=IETF93_QUIC&chapter=BAR_BOF))
    *   [Introduction](https://docs.google.com/presentation/d/15bnWhEBVRVZDO5up7UTpZU-o6jPOfGU4fT5JlBZ7-Cs/edit?usp=sharing)
    *   [QUIC Protocol
        Overview](https://docs.google.com/presentation/d/15e1bLKYeN56GL1oTJSF9OZiUsI-rcxisLo9dEyDkWQs/edit?usp=sharing)
    *   [Implementing QUIC For Fun And
        Learning](https://docs.google.com/presentation/d/1BjPUowoOoG0ywmq5r8QNqnC9JPELUe02jvgyoOW3HFw/edit?usp=sharing)
        (Christian Huitema, Microsoft)
    *   [QUIC Congestion Control and Loss
        Recovery](https://docs.google.com/presentation/d/1T9GtMz1CvPpZtmF8g-W7j9XHZBOCp9cu1fW0sMsmpoo/edit?usp=sharing)

Code

*   [Chrome
    implementation](https://chromium.googlesource.com/chromium/src/+/master/net/quic/)
*   [Standalone test server and
    client](https://chromium.googlesource.com/chromium/src/+/master/net/tools/quic/)

**Mailing lists**

*   [proto-quic@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/proto-quic)
