# The Zen of Merge Requests

# Ownership for Approvals

*   These ownerships are independent of issue trackers:

    *   The Chrome desktop TPM is responsible for approving merge requests
        related to desktop or in Webkit.

    *   The ChromeOS TPM is responsible for approving merge requests related to
        ChromeOS, explicitly those related to systems software and the kernel

    *   The Android TPM is responsible for approvals related to the Android code
        base

    *   The Security team owns approval for security bugs (TPMs should still be
        consulted on larger changes and security “features”)

# Guidelines for Approvals

*   ## Phase 1: Branch Day

    *   Key Objective: Get a release to the Dev channel that we can validate for
        fitness as a branch point.

    *   What that means in practice:

        *   We keep the branch closed for merges until we are confident that we
            have a branch we can stabilize (across all platforms)

            *   The bar for confidence is basically:

                *   a.) Stability rates that we are comfortable with

                *   b.) Blessing from QA

                *   c.) No red flags from real world usage (which manifests in
                    response to our release blog post, issues, and stability
                    info (crash dumps/ UMA stats))

    *   What merges we accept:

        *   ReleaseBlock:Dev issues (across all trackers)

        *   Stability issues

    *   Merges that we will reject:

        *   At this point the we shouldn’t really be rejecting requests yet,
            until the branch is opened up.

    *   When do we move to the next phase?

        *   Once we are confident in our branch point, we signal the start of
            phase 2 by sending the "Branch XXX has been declared, we are open
            for merges" e-mail.

*   ## Phase 2: The Push to Beta

    *   Key Objective: Refine our Dev channel to get it Beta ready (as quickly
        as reasonably possible, to return the Dev channel back to trunk based
        builds), addressing critical regressions (stability, performance,
        behavior, etc...).

    *   What that means in practice:

        *   There are a few parallel objectives in this phase:

            *   Clear Stability Issues

            *   Clear ReleaseBlock-Beta issues

            *   Disable features/functionality that aren't ready to ship

            *   Cures for functional regressions to the default shipping
                experience of Chrome

        *   Important note about Risk: Because of the early phase of the branch,
            we are generally a little less risk averse at this point in the
            project (we have to extend a bit of trust (which we validate) to our
            engineers, that they are requesting reasonable items

        *   Merges that we will accept in this phase

            *   Must have at least been on the Canary channel for 1 day, and
                verified to have been fixed.

            *   ReleaseBlock:Dev issues

            *   Stability issues

            *   Cures for critical regressions

            *   Patches to turn off/ disable features

            *   Small refinements/fixes

                *   Important notes about what is acceptable:

                    *   Minor/ low risk patches that cure defects in the product
                        that would harm the default user experience

                        *   Examples: Polish fixes, a quick bug fix that didn't
                            make the branch point, etc...

                        *   Examples of things we’d reject: Major changes to
                            behavior, UI, features, a change to a feature that
                            was behind a flag, etc...

        *   Merges we will reject (i.e. things we should Merge-Reject)

            *   Changes to strings (i.e. .grd changes)

                *   At branch point, we are frozen from a localization
                    perspective

            *   Building out a feature that isn't complete on the branch (a hint
                of this is when there are more than a couple of requests for a
                single feature)

            *   Work for features that are behind flags or not enabled by
                default (e.g. building out a Finch experiment)

        *   Some logic tests to apply:

            *   Are we shipping a product that is of lower quality than the
                previous release?

            *   Does this patch have a material benefit to a group of end users?

            *   What size of the affected group?

            *   Is the risk (to all of our Beta channel users) of accepting this
                patch worth the benefit to the affected user base?

    *   When do we move to the next phase?

        *   Once we promote the release to the Beta channel

*   ## Phase 3: The Push to Stable

    *   Key Objective: Get a working release of Chrome that meets or exceeds the
        quality level of the existing stable channel.

    *   What this means in practice:

        *   We are looking to cure any remaining:

            *   Security issues

            *   Stability issues

            *   Critical regressions

        *   Merges that we will accept

            *   Should have at least been on a Dev channel for 1 day, and
                verified to have been fixed

            *   ReleaseBlock-Stable issues that fit the category of
                Security/Stability/Critical Regressions

        *   Important Note about Risk: Risk tolerance in this phase is greatly
            diminished from when we are in Dev (and diminishes as we approach
            our stable launch date). We need to be open to patches, but there
            needs to be a very clear ROI to the overall user population

        *   Merges that we will reject

            *   Everything at this point should be closely reviewed on a case by
                case basis, but as a general guideline anything that’s not a
                stability, security, or critical regression at this point should
                be rejected

    *   When do we exit this phase?

        *   When we promote to the stable channel.

*   ## Phase 4: Post Stable

    *   Key Objective: Maintenance mode to fix critical issues that affect
        stable channel users

    *   What this means in practice:

        *   Cure gross defects that are harming our stable user population

        *   Merges that we will accept

            *   Should have at least been on a Dev or Beta channel for 1 day,
                and verified to have been fixed

            *   Only very critical ReleaseBlock-Stable issues that fit the
                category of Security/Stability/Critical Regressions

        *   Important Note about Risk:

            *   Our tolerance for merges is at its lowest at this point in the
                cycle. Every change that lands needs to be safe and must have a
                very specific purpose.

        *   Under normal conditions we try to bucket these merges into 1-2 post
            stable updates. That said, depending on the urgency/ nature of the
            issues, we may need to do a special release (e.g. a critical issues
            that affects a non-trivial number of users).
