# bisect-builds.py

Want to bisect your local checkout rather than prebuilt binaries? Use
[run-bisect-manual-test.py](bisecting-bugs.1498668983515).

Have you ever hit a regression bug like this:* "In chromium 26.0.401.1, things
were broken. Back in chromium 26.0.201.0, it was fine."*? A good way to attack
bugs like this – where it's unclear what change could have caused the
regression, but where you have a reliable repro – is to bisect.

tools/bisect-builds.py automates downloading builds of Chrome across a
regression range, conducting a binary search for the problematic change.

If you don't have a chromium checkout, you can fetch it with linux commands
below:

    curl -s --basic -n "https://chromium.googlesource.com/chromium/src/+/master/tools/bisect-builds.py?format=TEXT" | base64 -d > bisect-builds.py

Windows (no curl, base64)

    python -c "import urllib2; import base64; print base64.b64decode(urllib2.urlopen(\"https://chromium.googlesource.com/chromium/src/+/master/tools/bisect-builds.py?format=TEXT\").read())" > bisect-builds.py

Note: Bisect builds needs Python 2.x. Python 3.x won't work.

Run it like this:

    python tools/bisect-builds.py -a *platform* -g *good-revision* -b *bad-revision* -- *flags-for-chrome*

For example,

    python tools/bisect-builds.py -a mac -g 3894 -b 4000 --use-local-cache -- --no-first-run --user-data-dir=/tmp http://example.com

Valid archive types (the -a parameter) are `mac, mac64, win, win64, linux,
linux64, linux-arm, and chromeos`.

You can also use the `-p` option to specify a profile. If no `-p` or
`--user-data-dir` option is specified, a new profile will be created in a
temporary directory each time you are asked to try a build. If you specify a
profile folder, point to the directory that's a parent of Default/.

The script will download a build in the revision range and execute it. You must
then manually check if the bug still repros. Quit Chromium, and the script will
ask you if the bug reproduced or not. It will use your answer to drive a binary
search, and after just a few steps it will tell you "this regression happened
somewhere between revisions 1234 and 1334". From that list, it's usually easy to
spot the offending CL. If not, you can use the
[run-bisect-manual-test.py](bisecting-bugs.1498668983515) script which will
further bisect down to a particular CL by syncing and building manually. If
you're adding the range as a comment to a bug, please always paste the output
from bisect-builds.py, as this includes links to the chromium changes in the
regression range.

View code changes in revision range with this [Useful
URL](http://code.google.com/p/chromium/wiki/UsefulURLs) (replacing SUCCESS_REV
and FAILURE_REV with the range start and end):

<http://test-results.appspot.com/revision_range?start=SUCCESS_REV&end=FAILURE_REV>

**Notes: **For internal usage, we also enabled bisect builds by commits. Please
refer to [internal
doc](https://sites.google.com/a/google.com/chrome-te/home/tools/bisect-builds)
for more information.

**Getting an initial revision range**

If you have two Chrome binaries, one which doesn't work, one which does, you can
check the [chrome://version](javascript:void(0);) page for the revision that it
was built at (look for the "(Official Build NNNNN)" text). You can also infer
the revision number from the version number.

You can use the "Version Information" tool on
[OmahaProxy](https://omahaproxy.appspot.com/) to find out the numeric "Branch
base position".

### Verifying the range

If your revision range is incorrect, or if something about your environment
interferes with your reproduction of the bug, you will not get useful results
from bisect-builds.py. If you would prefer to know this as soon as possible,
rather than after downloading and checking O(log n) builds, pass the
**--verify-range** option to bisect-builds.py. This will check the first and
last builds in the range before starting the bisect.

### Argh, the offending CL is a Skia / V8 roll!

This can be annoying, especially if it's a big roll. If that doesn't help, then
you can use the [run-bisect-manual-test.py](bisecting-bugs.1498668983515)
script, which will recurse into the V8 or Skia repositories if you give it a
start and end revision range which includes a roll from one of these
repositories.

### If Pepper Flash is required to repro

You will have to locate a Flash binary from an official build. If you suspect a
Chromium change causing the regression and the Flash version doesn't matter
locate any binary on your machine. For instance:

    ./bisect-builds.py -f /opt/google/chrome/PepperFlash/libpepflashplayer.so -b 232915 -g 230425 -a linux64

    python bisect-builds.py -f "C:\Program Files (x86)\Google\Chrome\Application\31.0.1650.39\PepperFlash\pepflashplayer.dll" -b 232915 -g 230425 -a win

    ./bisect-builds.py -f "/Applications/Google Chrome.app/Contents/Versions/33.0.1707.0/Google Chrome Framework.framework/Internet Plug-Ins/PepperFlash/PepperFlashPlayer.plugin" -b 232915 -g 230425 -a mac

**API Keys and Chrome OS builds**

Without API keys, Chrome OS won't allow you to log in as a specific user. To run
a chromeos bisect on your Linux desktop, add the following variables to your
environment (e.g., via .bashrc):

GOOGLE_API_KEY=<key>

GOOGLE_DEFAULT_CLIENT_ID=<id>

GOOGLE_DEFAULT_CLIENT_SECRET=<secret>

See <https://www.chromium.org/developers/how-tos/api-keys> for more info about
API keys.

**Bisecting Per-Revision Builds (Googlers Only)**

The -o and -r options available in the internal version of the script allow
tighter bisects. Google employees should visit
[go/bisect-builds](https://goto.google.com/bisect-builds) for configuration
instructions.
