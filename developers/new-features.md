# New Features

## Declare your intent to launch/change a feature

To encourage design discussions and sharing of information about upcoming
features and changes with the Chromium developer community,you should use the
following process to announce the intent to work on a new feature:

If your feature

*   will require a design doc for others to understand what the motivation
    behind it is, or will require a tracking bug to help connect a series of
    non-trivial patches, or
*   will require structural changes to larger parts of the code base, or change
    APIs (such as the content API) in a non-trivial fashion, or
*   will affect user visible features in a non-trivial way, and
*   is not already covered by another process such as the Blink intent to
    implement process

then you should send an email to
[chromium-dev@chromium.org](mailto:chromium-dev@chromium.org) using the new
"[intent to
implement](https://docs.google.com/document/d/1O4hws3_JFufi1Y8Cog4XNfOs03ZPgWC9rEGEu-RIeG4/edit?pli=1)"
template.

It's ok to modify the template, e.g., if all the information is in an existing
design doc, you can just point to it.

### Google internal

See go/newChromeFeature for additional steps to take for Googlers
