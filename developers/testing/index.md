# Testing and infrastructure

## Highlighted Child pages

*   [Running tests at home](running-tests.md)
*   [Chromium build infrastructure](chromium-build-infrastructure/index.md)
*   [WebKit Layout Tests](webkit-layout-tests/index.md)
*   [Try Server usage](try-server-usage/index.md)
*   [Commit Queue](commit-queue/index.md)
*   [Isolated testing](isolated-testing/index.md) and Swarming
*   [GPU Testing](gpu-testing/index.md)
*   [WebGL Conformance Tests](webgl-conformance-tests.md)
*   [Buildbot
    Annotations](chromium-build-infrastructure/buildbot-annotations.md)
*   [WebUI browser tests](../../Home/domui-testing/webui-browser_tests/index.md)
*   [chrome.test.\* APIs](chrome-test-apis.md)

## Overview

Chromium development is heavily test driven. In order to maintain a rapid rate
of development across multiple platforms and an ever increasing set of features,
it is imperative that test suites be updated, maintained, executed, and evolved.
Any new features should have test coverage and in addition most changes should
have test coverage. As a contributor to Chrome you are expected to write quality
tests that provide ample code coverage. As a reviewer you are expected to ask
for tests. **The **[**Chromium Continuous Integration
system**](chromium-build-infrastructure/index.md)** is employed to run these
tests 24x7**.

## Expectations

Developers contributing code are expected to run all tests. This is not
typically feasible on a single workstation, so [try
servers](try-server-usage/index.md) are used. When contributing code, consider
whether your change has enough testing. If it is a new feature or module, it
should almost certainly be accompanied by tests.

## Test Development Infrastructure

To assist with building tests, several pieces of infrastructure exist. Here are
some tools you might find useful:

*   chrome/test/automation - Chromium includes a mechanism for driving the
    browser through automation. This is primarily used with the UI tests.
*   [gtest](http://code.google.com/p/googletest/) - Google Test is Chromium's
    C++ test harness.
*   **image_diff** - A mechanism for comparing bitmaps.

## Inducing a crash

This can be useful to test breakpad:

*   about:crash - will cause a renderer crash.
*   about:inducebrowsercrashforrealz - will cause a browser crash.

## Subpages
