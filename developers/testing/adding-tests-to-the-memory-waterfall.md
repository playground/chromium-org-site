# Adding new tests to the Memory Waterfall

The Memory waterfall (<http://build.chromium.org/p/chromium.memory/console>) is
policed by tree sheriffs to keep it green as possible.

**IMPORTANT NOTE:** for historic reasons both Memory waterfall (running ASan)
and Memory FYI waterfall (running Valgrind et al.) have "memory" in their names.

But their purpose is completely different and they must not be confused. Also,
"FYI" in "Memory FYI" does not stand for "waterfall that nobody is watching".

When adding a new test to Memory bots, make sure to run them through ASan and
LSan locally. Any leaks or crashes reported by ASan and LSan need to be fixed
before adding the test to the waterfall.

To setup, build and run under ASan follow the instructions in
<http://www.chromium.org/developers/testing/addresssanitizer>, for LSan refer to
<http://www.chromium.org/developers/testing/leaksanitizer>.

Once the test is clean you'll need to add them to the TryServers and the Memory
waterfall.

Tryserver configs are spread across master.tryserver.chromium.{linux,mac} in
[masters](https://chromium.googlesource.com/chromium/tools/build/+/master/masters/)
(look for "asan", do not forget about ChromeOS builds).

Configs for the Memory waterfall live in
[master.chromium.memory](https://chromium.googlesource.com/chromium/tools/build/+/master/masters/master.chromium.memory/).

Please make sure you coordinate with the tree sheriffs before landing the CLs.

You'll also need to contact phajdan@, sergeyberezin@, sergiyb@ or sheyang@ to
add the test to the commit queue.
