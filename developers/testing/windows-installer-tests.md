# Windows Installer Tests

## About

The test_installer build step is a full lifecycle test of Chrome's Windows
installer. The test does variations along the following theme:

*   Ensure the machine is in a sufficiently clean state such that Chrome can be
    installed.
*   Install Chrome.
*   Verify various state on the machine to validate that the installer
    succeeded.
*   Launch Chrome.
*   Verify that something resembling a browser window opens.
*   Quit Chrome.
*   Verify that all Chrome processes go away.
*   Uninstall Chrome.
*   Verify that various state on the machine is no longer present.

## Diagnosing Failures

Due to issue [399499](http://crbug.com/399499), the output in the build step may
not contain the full output of the test. To see the full output, click the stdio
link for the steps build step and search for test_installer.py. The most common
failure will be a failure to launch Chrome, which generally indicates that
chrome.exe is crashing at startup.

## Running the Tests Locally

First build the mini_installer target. The minimal command to run the test is:

python chrome\\test\\mini_installer\\test_installer.py --config
chrome\\test\\mini_installer\\config\\config.config \[test_name\]

Specify individual test names as, for example:
__main__/InstallerTest/ChromeUserLevel.

Run it with --help for more options. Note that you should use the Python that
comes with
[depot_tools](https://www.chromium.org/developers/how-tos/install-depot-tools)
as other versions might not work.

## Questions

Contact grt at chromium dot org.
