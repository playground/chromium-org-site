# Isolated Testing

## What's happening

The Chrome team is in the process of retro-fitting the ability of running tests
outside a checkout to scale out testing so the tests results are returned
faster.

## Design documents

The whole LUCI/Swarming/Isolate project lives in its own project at
[github.com/luci](https://github.com/luci). The [design docs on the
wiki](https://github.com/luci/luci-py/wiki) are a recommended reading since it
gives background about the rationale why is it done in the first place.

## User guide

[I want to convert my test or use the infrastructure](for-swes.md) <- most
likely this one.

## Admin info (e.g. I'm curious and want to learn more)

[I'd like to know how the infrastructure works and how the integration is done
with buildbot](infrastructure.md) <- read this before asking questions if you
are confused.

## End goal

[Deduplicate test execution via deterministic builds](deterministic-builds.md)
