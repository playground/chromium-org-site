# Dr. Fuzz

**Dr. Fuzz** is an in-process fuzzing feature extended from [Dr.
Memory](http://drmemory.org/), which is a memory checking tool for identifying
memory-related programming errors including:

*   reads of uninitialized data,
*   accesses of unaddressable memory (e.g., buffer overflow/underflow,
    use-after-free),
*   double frees, memory leaks,
*   handle leaks (on Windows) ,
*   GDI API usage errors (on Windows) ,
*   and accesses to un-reserved thread local storage slots (on Windows) .

Based on dynamic binary instrumentation, **Dr. Fuzz** can repeatedly execute one
function in the target application with different execution context or argument
inputs without requiring source code access or recompilation.

**Build And Run Tests with Dr. Fuzz**

There are two ways to run your tests with **Dr. Fuzz**:

**Build and Run Normal Tests with Dr. Fuzz**

Configure and build with ninja:

gclient --runhooks

ninja -C out/Debug net_unittests

Run tests with **Dr. Fuzz**:

bin64/drmemory -light -fuzz_function QuicFramerFuzzFunc --
./out/Debug/net_unittests --single-process-tests
--gtest_filter=QuicFramerTests/QuicFramerTest.FramerFuzzTest/0

**Build and Run LibFuzzer Tests with Dr. Fuzz**

Configure and build 64-bit Windows tests with ninja

mkdir -p out/DrFuzz

gn gen out/DrFuzz --args=use_drfuzz=true

ninja -C out\\DrFuzz url_parse_fuzzer

Configure and build 32-bit Windows tests with ninja

mkdir -p out/DrFuzz

gn gen out/DrFuzz --args="use_drfuzz=true target_cpu=\\"x86\\""

ninja -C out\\DrFuzz url_parse_fuzzer

Run tests with **Dr. Fuzz**:

bin/drmemory -fuzz_function LLVMFuzzerTestOneInput --
./out/DrFuzz/url_parse_fuzzer
