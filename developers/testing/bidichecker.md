# BidiChecker

# What is BidiChecker?

BidiChecker (<http://code.google.com/p/bidichecker>) is a tool for the automated
testing of web pages for errors in support of right-to-left (RTL) languages
(such as Arabic and Hebrew), also known as bidirectional (BiDi) because they
routinely include left-to-right items such as numbers and Latin-script words and
phrases.

## Why you should care about BiDi

## BiDi support for a web page is a common requirement, even for pages in left-to-right scripts. Any page which accepts user input or displays multilingual content is likely to end up handling bidirectional text at some point, as it will eventually encounter data in Arabic, Hebrew or another RTL language. Unfortunately, handling bidirectional text can be tricky and requires special processing at every appearance of potentially BiDi data in the UI. ([Examples of common BiDi UI bugs can be found here.](http://doctype.googlecode.com/svn-history/trunk/bidihowto/index.html)) As a result, BiDi text support often regresses when a developer adds a new feature and simply forgets about BiDi.

**BidiChecker and Chrome**

Chrome has a mechanism called WebUI, which displays parts of the Chrome UI using
web content (e.g. Chrome's preferences). BidiChecker can automatically test such
pages for common BiDi errors, and report any regressions. Currently, BidiChecker
is being run as part of Chrome's standard unit test suite.

**Adding BidiChecker Tests for your WebUI page**

There are two cases which BidiChecker is used to verify:

1.  Running with an LTR UI and displaying RTL contents. Example: Active UI
    language is English while page has some Arabic content.
2.  Running with an RTL UI and displaying LTR contents. Example: Active UI
    language is Hebrew while page has some English content.

Your page needs to test for both cases. Luckily this is really simple:

1.  Add a test case in *chrome/browser/ui/webui/**bidi_checker_web_ui_test.cc*
    that uses the *WebUIBidiCheckerBrowserTestLTR* fixture class. Set up mock
    content in Hebrew or Arabic, and launch BidiChecker. This runs the test in
    an LTR environment. It should look something like this:
    `IN_PROC_BROWSER_TEST_F(WebUIBidiCheckerBrowserTestLTR, MyAwesomeTest) {`
    ` // Custom code to fill in some RTL (Arabic, Hebrew, etc) contents in the page`
    ` const char* myURL = // A URL to your page goes here`
    ` RunBidiCheckerOnPage(myURL);`
    ` }`
2.  Repeat the above but use WebUIBidiCheckerBrowserTestRTL as the test fixture
    class:
    `IN_PROC_BROWSER_TEST_F(WebUIBidiCheckerBrowserTestRTL, MyAwesomeTest) {`
    ` // Custom code to fill in some LTR (English) contents in the page`
    ` const char* myURL = // A URL to your page goes here`
    ` RunBidiCheckerOnPage(myURL);`
    ` }`

Note: As a convention, you should name your test with the same name both for RTL
and LTR.

**Handling BidiChecker Errors**

After first running your tests, it's possible that BidiChecker will find errors
in your page. When this happens you have two options:

1.  Fix the errors. This is usually what you're supposed to do for new code.
2.  For existing code, you may want to suppress existing errors. In order to do
    that, open *chrome/test/data/webui/bidichecker_tests.js* and edit the
    suppressions list at the top of the file.

**Running BidiChecker Tests**

BidiChecker tests are being run as part of the browser_tests executable. If you
want to execute only the BidiChecker tests, run

*/path/to/browser_tests --gtest_filter=WebUIBidiCheckerBrowserTest\**

To run only a specific BidiChecker test use

*/path/to/browser_tests --gtest_filter=WebUIBidiCheckerBrowserTest\*.MyTestName*

**What Should I Test For?**

BidiChecker tests are usually very good at catching errors in mixed
directionality UIs. As such, you should try to test your page with content that
has the opposite directionality from the active language (e.g. Hebrew text
entered into a en-us localized page). Common examples are:

*   Insert text into a text field. The text need to have the opposite direction
    of the UI. For example, check out the test for autofill settings.
*   Insert dynamic page content like history entries, download entires, etc. The
    entries' titles, details, etc should have the opposite text direction from
    the UI.
