# Chromium Commit Queue

See also: [Chromium OS Commit
Queue](../../tree-sheriffs/sheriff-details-chromium-os/commit-queue-overview/index.md).

## What (is it)?

It's a service (aka a bot) that commits
[rietveld](http://code.google.com/p/rietveld/) changes for you, instead of your
[directly committing](../../contributing-code/direct-commit.md) the change. It
is enabled for the following projects:

*   Chromium changes under
    [chromium/src](https://chromium.googlesource.com/chromium/src)
*   Chromium tools changes under
    [chrome/trunk/tools](http://src.chromium.org/viewvc/chrome/trunk/tools/),
    limited to build and depot_tools.
*   Native Client at
    [native_client](http://src.chromium.org/viewvc/native_client/trunk/src/native_client/)
*   Skia changes under
    [skia/master](https://skia.googlesource.com/skia/+/master)
*   V8 changes under
    [v8/master](https://chromium.googlesource.com/v8/v8/+/master)

...but not GYP, for instance. This list is not complete, though, and may grow
over time. Rietveld will show a 'Commit' checkbox if the CQ supports to land the
CL.

## Design document

The [design doc is in its own page](design.md) and there's a design doc about
the [Try Server <-> Rietveld <-> Commit Queue 3-way
integration](integration-with-rietveld.md).

## How (does it work)?

The commit queue is not really a queue at the moment, since it processes the
changes **out of order**. This may be changed eventually. This means a CL can be
committed before another CL that was triggered much earlier. This can happen
when a try job is flaky.

### Current process for the user

1.  Upload a review to rietveld where it gets reviewed and LGTM'ed.
2.  One of:
    1.  Check (select) the 'Commit' checkbox on a Rietveld issue, on the
        particular patchset that has been approved. Checking the checkbox is the
        only action required and there will be no immediate feedback in the form
        of messages, etc, to notify you that something has happened.
        *   Only the issue owner, someone at @chromium.org or @google.com can
            check the box.
        *   Yes, **non-Chromium committers are allowed **to use the commit queue
            but cannot LGTM a change.
    2.  At the command line, type `git cl set_commit`
    3.  Have a reviewer use 'Quick LGTM & CQ'.
3.  Wait an hour. The current list of patches to be queued can be found at
    [Commit Queue
    Patches](https://codereview.chromium.org/search?closed=3&commit=2), while CQ
    progress can be tracked at [Commit Queue
    Progress](https://chromium-cq-status.appspot.com/). The commit queue will
    wait automatically for the tree to reopen.
4.  Wait for an email from commit-bot@chromium.org with success or failure.

## Why (is it broken)?

Please follow these general guidelines:

1.  **Please report issues via process described
    [here](https://chromium.googlesource.com/infra/infra/+/master/doc/users/contacting_troopers.md).**
    In particular, if a try job result fails for a reason unrelated to your CL,
    please file Infra-Troopers issue with a link to the failed build: click on
    the red bubble and copy that URL; for extra credit copy-paste the failing
    step.
2.  If you have a feature request, feel free to file a bug, use label
    Infra-CommitQueue. Be sure to search for [current feature
    requests](http://code.google.com/p/chromium/issues/list?q=label:Build-CommitQueue)
    first.

## Options

    COMMIT=false

If you are working on experimental code and do not want to risk accidentally
submitting it via the CQ, then you can mark it with "COMMIT=false". The CQ will
immediately abandon the change if it contains this option.

To dry run through the CQ please use Rietveld's [dry
run](https://groups.google.com/a/chromium.org/forum/#!topic/chromium-dev/G5-X0_tfmok)
feature.

    TBR=<username> 

See [policy of when it's acceptable to use
TBR](https://chromium.googlesource.com/chromium/src/+/master/docs/code_reviews.md#TBR-To-Be-Reviewed)
("To be reviewed"). If a change has a TBR line with a valid reviewer, the CQ
will skip checks for LGTMs.

    NOPRESUBMIT=true

If you want to skip the presubmit check you can add this line and the commit
queue won't run the presubmit for your change. This should only be used when
there's a bug in the PRESUBMIT scripts. Please check that there's a bug filed
against the bad script, and if there isn't, file one.

    NOTRY=true

This should only be used for reverts to green the tree, since it skips try bots
and might therefore break the tree. You shouldn't use this otherwise.

    NOTREECHECKS=true

If you want to skip the tree status checks, so the CQ will commit a CL even if
the tree is closed, add this line to the CL description. Obviously this is
strongly discouraged, since the tree is closed for a reason. However, in rare
cases this is acceptable, primarily to fix build breakages (i.e., your CL will
help in reopening the tree).

    NO_DEPENDENCY_CHECKS=true

The CQ rejects patchsets with open dependencies. An open dependency exists when
a CL depends on another CL that is not yet closed. You can skip this check with
this keyword.

    CQ_INCLUDE_TRYBOTS=<trybots>

This flag allows you to specify some additional bots to run for this CL, in
addition to the default bots. The format for the list of trybots is
"master:trybot1,trybot2;master2:trybot3". This feature only works for recipe
based bots right now.

### Is the CQ broken?

Take a look at
<https://codereview.chromium.org/search?closed=3&commit=2&limit=100&order=modified>.
If there are issues older than ~4 hours, they could probably be stuck. Note that
the Commit Queue could be stuck only for some issues but not all of them. In
case of doubt, contact commit-bot@chromium.org.

If your CL hasn't been touched after a few minutes of checking the CQ bit, CHECK
THE PROJECT FIELD ON YOUR ISSUE. If there is no project field, it will be
ignored by the Commit Queue.

### The CQ seems hung

Is the tree open?

It commits (if not configured differently for a project) **4 CLs every 8
minutes**, so a maximum rate of 30 commits per hour.

### Please Help! I just want to ask on irc !

Please
[report](https://chromium.googlesource.com/infra/infra/+/master/doc/users/contacting_troopers.md)
issues to chrome-troopers.

### My patch failed to apply

See the [Try Server FAQ](../try-server-usage/index.md).

### What about LKGR?

The Commit Queue has never known, used or cared about LKGR. It always uses HEAD,
the tip of tree.

### Where is the dashboard?

It's at <https://chromium-cq-status.appspot.com/>. You can follow the link
posted to your CL to see the testing progress.

### What's my position on the queue?

The CLs are processed out of order, so it's not because another is "before"
yours that means it'll be committed before yours. You can see the load on the CQ
by looking at the number of tests CLs pending:

<https://codereview.chromium.org/search?closed=3&commit=2&limit=1000&order=modified>

### Sending CL through CQ in dry-run mode

To dry run through the CQ please use Rietveld's [dry
run](https://groups.google.com/a/chromium.org/forum/#!topic/chromium-dev/G5-X0_tfmok)
feature.

### **Picking custom trybots**

See the CQ_INCLUDE_TRYBOTS option, above.

### Try job results aren't showing up consistently on Rietveld

If you never had a HTTP 500 on GAE, chances are that [you
will](http://code.google.com/status/appengine).

### Binary files?

Sorry, unsupported. Some known issues include [bug
579778](https://code.google.com/p/chromium/issues/detail?id=579778), [bug
395626](https://code.google.com/p/chromium/issues/detail?id=395626), and [bug
375734](https://code.google.com/p/chromium/issues/detail?id=375734).

For reference, older bugs related to this include [bug
339068](http://crbug.com/339068) and [bug
23608](https://code.google.com/p/chromium/issues/detail?id=23608).

### My CL has a bazillion files, will it blend?

The CQ was able to commit a CL with 838 files so it is technically possible;
<https://codereview.chromium.org/12261012/>. The likelihood of the CQ failing
increases exponentially with the number of files in the CL.

### Moving, renaming or copying files

Was implemented in [bug
125984](https://code.google.com/p/chromium/issues/detail?id=125984) and [bug
125983](https://code.google.com/p/chromium/issues/detail?id=125983). If the diff
on Rietveld doesn't look right, use the `--similarity` (defaults to 50%) and
disable/enable file copy with `--find-copies`/`--no-find-copies`. In case of
confusing;

    git cl help
    man git diff

### Are CQ users required to be around when the patch is landed?

In general, no, as the CQ can land at any time (including very long time), and
any breaking patches can be kicked about by the sheriff. After all, that's the
job of the sheriff. You will get an email when the CQ commits, so you can jump
on your nearest laptop if necessary.

If you expect your patch to be hard to revert, is touching several files and
directories or move files around, you may want to stay around in case there is
an incremental build failure or something hard to diagnose.

Also, if you commit on the weekend, don't expect a build sheriff to back out
your crap so keep an eye open when you receive the CQ commit email.

### What determines the set of tests and targets the try bots run?

This is controlled by a config file
([chromium_trybot.json](http://chromium.googlesource.com/chromium/src/+/master/testing/buildbot/chromium_trybot.json)
for most trybots). Also see this [document](chromium_trybot-json.md) for details
on the analyze step.
