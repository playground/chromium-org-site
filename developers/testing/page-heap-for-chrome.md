# Page Heap for Chromium

Page Heap is a Windows mode to help identify memory errors.

### Enabling Page Heap

1. You need to use a heap allocator other than tcmalloc when using PageHeap.

By default, chrome.exe now uses tcmalloc to allocate pages. Because of this,
many of PageHeap's benefits don't work. To switch Chrome to use the default
allocator, you can set the CHROME_ALLOCATOR environment variable to "winheap":

set CHROME_ALLOCATOR=winheap

2. The easiest way to turn on page heap is using gflags, which is included in
Windows Debugging Tools. You need to use x86 version regardless of your system
is 32 bit platform or 64 bit platform. The current download page makes it hard
to find the 32-bit version, but [this previous 32-bit version of Windows
Debugging Tools](http://msdn.microsoft.com/en-us/windows/hardware/gg463016)
works fine. If you have depot_tools installed, there's a version in
"depot_tools\\win_toolchain\\vs2013_files\\win8sdk\\Debuggers\\x86\\".

Add the Windows Debugging Tools to your path: "c:\\Program Files
(x86)\\Debugging Tools for Windows (x86)"

3. Enable full page heap for a particular executable with this command:

gflags.exe /p /enable chrome.exe /full

If chrome gets too slow with full page heap turned on, you can enable it with
normal page heap:

gflags.exe /p /enable chrome.exe

Tip: since you need to run this as administrator, it might be easiest if you
right-click on your console program and select Run as administrator so that all
operations in that shell are already priveleged.

See Background section for more information on page heap and gflags.

4. You need to run chrome with sandbox disabled under page heap:

Debug/chrome.exe --no-sandbox

### Disabling Page Heap

To disable page heap when you're done, run:

gflags.exe /p /disable chrome.exe

### Background

1. Page heap is Window build-in support for heap verification. There are two
modes:

- Full-Page heap places a non-accessible page at the end of the allocation.
Full-page heap has high memory requirements. Its advantage is that a process
will access violate (AV) exactly at the point of illegal memory operation.

- Normal page heap checks fill patterns when the block gets freed. Normal page
heap can be used for testing large-scale process without the high memory
consumption overhead of full-page heap. However, normal page heap delays
detection until the blocks are freed - thus failures are more difficult to
debug.

When an application foo.exe is launched, Windows looks up in
"HKLM\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Image File Execution
Options\\foo.exe" for page heap and other settings of the executable and acts
accordingly.

2. To turn on page heap for an executable, one just needs to modify the settings
in registry. Gflags is a utility downloadable from Microsoft to edit settings
under "Image File Execution Options".

### ### Troubleshooting

*   Are you sure you're using the 32-bit version of gflags? If you use the
    64-bit version, you won't get any error messages, but nothing will happen.
*   Are you sure that CHROME_ALLOCATOR is set to winheap? If not, Chrome might
    freeze on startup.
*   Are you debugging browser_tests? You might want to enable page heap for both
    browser_tests.exe and chrome.exe, or try running browser_tests with the
    --single-process flag.
*   Are you sure you're running Chrome with --no-sandbox?
