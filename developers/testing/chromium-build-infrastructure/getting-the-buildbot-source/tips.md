# 5. Buildbot Tips and Tricks

### Applying a patch to all of your builders' source trees

If you create a patch which can be applied on all of your master's builders, you
can modify your Buildbot master so the patch is applied (as in the TryMaster
configuration).

1. Ensure your patch is relative to src. The following command should 'just
work':

cd /work/chromium && patch -p0 < /tmp/patch

2. Place the patch at /tmp/patch (this path should be readable by your Buildbot
master).

3. Modify ./scripts/master/chromium_step.py with the following patch:

Index: chromium_step.py

===================================================================

--- chromium_step.py (revision 27647)

+++ chromium_step.py (working copy)

@@ -58,7 +58,10 @@

args = copy.copy(self.args)

args\['revision'\] = revision

args\['branch'\] = branch

- args\['patch'\] = patch

+ # args\['patch'\] = patch

+ f = open('/tmp/patch', 'r')

+ args\['patch'\] = \[0, f.read()\]

+ f.close()

cmd = buildstep.LoggedRemoteCommand('gclient', args)

self.startCommand(cmd, warnings)

4. Restart your master. When each builder begins again, your patch should be
sent to the builders and applied locally just like a normal try job's patch.
