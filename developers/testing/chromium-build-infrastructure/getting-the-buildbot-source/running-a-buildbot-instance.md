# 2. Running a buildbot instance

## Running a buildbot master or slave

You can run multiple masters on the same machine as long as they don't bind the
same ports.

Windows

If you run both the master and the slave on the same account on Windows, the
"taskkill" step will **kill** python hence the master. You can fix that by using
multiple account or just keep sane and use a linux machine as the server.

#### Master

`cd masters\master.experimental`
`run_master.bat`

#### Slave

`cd slave`

`run_slave.bat`

#### How to terminate

To kill them, press Ctrl-C in the console window.

### Mac and Linux

#### Master

`cd masters/master.experimental`

`make restart`

#### Slave

`cd slave`
`make restart`

If you have set up buildbot.tac, you can just run "make restart". If not, you
will run something like this instead:

**TESTING_MASTER_HOST=localhost TESTING_MASTER=Experimental
TESTING_SLAVENAME=chromium-linux-experimental make restart**

#### Kill

To kill them, use ctrl-c in the console window to get back to the prompt, and
then run:

`make stop`

## Is that working?

Once you have started both the master and a slave, browse to the waterfall:
<http://localhost:9010/waterfall?show_events=true&reload=30> (replacing
localhost with the hostname of the master if your browsing on another computer
and the port with the appropriate port number. The port number can be found in
[site_config/config_default.py](http://src.chromium.org/viewvc/chrome/trunk/tools/build/site_config/config_default.py?view=markup)
or master_site_config.py by looking for the appropriate class definition for
your master). If you see one idle slave and two offline slaves they you've
configured them correctly! You can start it building by clicking on the name of
the idle slave (probably Chromium Linux Experimental) and then clicking the
"Force Build" button. If the Update and Compile steps succeed, congratulations
you're all set!

## Troubleshooting

Please feel free to update this part of the doc with other problems you have run
into.

*   #### twisted.cred.error.UnauthorizedLogin

    Traceback from remote host -- Traceback (most recent call last):

    Failure: twisted.cred.error.UnauthorizedLogin:

    Your slavename setting in slave/buildbot.tac is possibly not a valid slavename. Stop the master, update the buildbot.tac file with a valid slavename and restart.
*   **Message about needing to update a database**
    Run `make upgrade`

## Try server functionality

### Server

The try server listens to a http port and also polls a subversion repository.
See
[config.py](http://src.chromium.org/viewvc/chrome/trunk/tools/build/site_config/config.py?view=markup)
and
[config_default.py](http://src.chromium.org/viewvc/chrome/trunk/tools/build/site_config/config_default.py?view=markup)
for the server-side settings, mainly `config.Master.TryServer.try_job_port` and
`config.Master.TryServer.svn_url`.

### Client

See
[trychange.py](http://src.chromium.org/viewvc/chrome/trunk/tools/depot_tools/trychange.py?view=markup)
or [git-try](http://neugierig.org/software/git/?url=git-try/) for the client
code. Both demonstrate both ways of sending a job to the try server. Most of
users will run:

`gcl try <change_name> --host myserver --port 9018`

or whatever port you are using. The port is configured on the server in
chromium_config.Master.TryServer.try_job_port.. You can configure your
codereview.settings file accordingly.

## Fork it

You can now [fork your build infrastructure](forking-your-buildbot.md).

## Maintain it

Look for [maintenance](buildbot-maintenance.md) page.
