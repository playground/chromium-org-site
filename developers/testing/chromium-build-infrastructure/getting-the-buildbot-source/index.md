# Getting the buildbot source

## Whole raw sources

Our continuous build infrastructure is based on
[buildbot](http://buildbot.net/), written in python. All the Python scripts,
recipes, HTML, and Javascript that the [buildbot](http://build.chromium.org/)
uses is kept under source control. Check out the whole set with:

    # Ensure that [depot_tools is on your path](http://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html#_setting_up).
    # Then run, in an empty directory:
    fetch infra

### Private data

The build scripts assume that any private bits are in build_internal, beside
build. So a private configuration file would be a
`build_internal/site_config/config_private.py`.

## Buildbot instance types

Description of some of the masters:

*   master.chromiumos: [ChromiumOS continuous
    build](http://build.chromium.org/p/chromiumos/waterfall).
*   master.chromium: [Our main continuous
    buildbot](http://build.chromium.org/p/chromium/console).
*   master.chromium.fyi: [Our *For Your Information*
    waterfall](http://build.chromium.org/p/chromium.fyi/waterfall). Contains
    configurations and slaves that are expected to break.
*   master.experimental: At minimal buildbot master to play with. That's the
    best configuration to learn how to play with buildbot.
*   master.client.nacl: [Native Client](http://code.google.com/p/nativeclient/)
    buildbot.
*   master.tryserver.chromium: [Our try
    server](http://build.chromium.org/p/tryserver.chromium/waterfall). Permits
    testing patches before committing.
*   master.tryserver.nacl: Equivalent for NaCl.

## Configuring your buildbot instances

Next step, [configuring the buildbot](configuring-your-buildbot.md).
