# 3. Forking your buildbot

## Initial step

The idea is simple. Let's say you have your svn repository at
`svn://svn.awesomefork.com/repo` and you want to put your scripts in
/trunk/tools/build.

### Master

Create a build_internal directory that you can fetch with gclient; Create a
master at this directory:
`svn://svn.awesomefork.com/repo/trunk/tools/build/masters/master.awesome`. Then
it's as you prefer. Either create a DEPS file or configure 2 solutions in
gclient or simply sync manually with svn update.

Change the DEPS file for:

    deps = {
      "build":
        "http://src.chromium.org/chrome/trunk/tools/build",
    }

### Slave

Copy the directory <http://src.chromium.org/chrome/trunk/tools/buildbot/slave/>
to `svn://svn.awesomefork.com/repo/trunk/tools/buildbot/slave`.

Create

    deps = {
      "build":
        "http://src.chromium.org/chrome/trunk/tools/build",
    }

### Private settings

Put private settings in `build_internal/site_config/config.py`.

## Now what?

So you're set!

*   You can add other dependencies or use deps_os = { } to add OS specific
    dependencies to the DEPS files if you use DEPS files.
*   You may want to copy the whole directories or fetch specific revision with
    @revision notation in the .gclient files.

## Run your fork

### Master

    gclient config svn://svn.awesomefork.com/repo/trunk/tools/build_internal
    gclient sync
    cd build_internal/master/master.awesome
    make restart

### Slave

    gclient config svn://svn.awesomefork.com/repo/trunk/tools/build_internal
    gclient sync
    cd master
    make restart

## Maintain it

Now you only need to [maintain](buildbot-maintenance.md) your infrastructure.
