# Linux Builder (dbg-shlib)

## How does this builder differ from Linux Builder (dbg)

The Linux Builder (dbg-shlib) compiles Chrome using shared libraries instead of
static libraries. Many Linux developers use this setup, since linking is much
faster and allows for quicker code/compile/debug cycles. This bot ensures that
you don't accidentally break compilation for them.

## What's the difference between static and shared (dynamic) libraries

You can think of static libraries as a ZIP archive containing all compiled
object files. When linking using static libraries, the linker picks exactly
those symbols from the object files it needs to satisfy all dependencies. Shared
libraries on the other hand are more like executables containing all symbols
from the object files. With this, it's easy to understand why linking shared
libraries fails while linking static libraries still works (note that it might
also happen that you can link with shared libraries, but not with static
libraries. In that case, you'll first get an error when you try to run the
resulting executable):

### **Your CL adds incomplete code**

For example a virtual class that is nowhere used and not all declared methods
are implemented. Since the class is nowhere used, it won't be included when
linking with static libraries. With dynamic libraries, it will be included, and
the linker will fail to create the vtable, since it can't find the declared but
not implemented method.

#### How to fix

Add the missing symbols, e.g. remove the class or add an empty method body.

### **Your CL includes headers from a gyp target that the code does not depend on**

If your CL changes a file in base/ to include a header from chrome/browser, and
the header includes a virtual class definition which is, however, not used in
your CL, static linking works again, while dynamic linking will fail to create
the vtable for that class. The methods for that class are defined somewhere in
chrome/browser. But since base does not depend on browser, those methods are not
available when linking base, and the dynamic linker will fail.

#### How to fix

Either make base depend on browser (no, you don't want that), or don't include
headers from chrome/browser in base/.

## How to build with shared libraries locally

Assuming you have already have all sources checked out, run:

export GYP_DEFINES="library=shared_library"
gclient runhooks
rm -rf out

make
