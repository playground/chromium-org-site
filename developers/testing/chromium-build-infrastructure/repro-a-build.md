# How to repro a build from a waterfall.

Reproducing a builder's build can generally be done by following the
instructions located under the 'run_recipe' link on the build page itself.

In particular, you need to [install
depot_tools](../../how-tos/install-depot-tools.md), and then get a build
checkout:

    $ cd <empty dir>
    $ # One of:
    $ fetch infra           # external user
    $ fetch infra_internal  # internal user
    $ cd build

And then navigate to a build that you want to repo. Look for the build step
which looks like:

1.  setup_build setup_build
1.  running recipe: "<RECIPE_NAME>" ( 0 secs )

    *   stdio
    *   run_recipe

Then click the 'run_recipe' link and you'll get some instructions on how to
repro the build.

If you don't see a setup_build step, then you'll have to run a local master and
slave, as [documented
here](getting-the-buildbot-source/configuring-your-buildbot.md).
