# Buildbot Annotations

The annotator allows your project to generate buildbot build stages as it runs,
simply by emitting special tags to stdout/stderr. This allows you to capture you
build process in a single root level script, run it locally, and have your
process branch with the project.

Please see
[scripts/common/annotator.py](https://chromium.googlesource.com/chromium/tools/build/+/master/scripts/common/annotator.py)
for a list of available annotations.
