# Android WebView tests

In general the Android WebView test code and infrastructure is similar to what
the rest of the project uses. Here we will only deal with details specific to
the WebView.

## Test code location and conventions

Most of the Android WebView test code will be found under
[android_webview/javatests](https://code.google.com/p/chromium/codesearch#chromium/src/android_webview/javatests).
There are two categories of tests here:

*   integration tests, which create one or more AwContents instances and derive
    from AwTestBase. These tests are intended to test public APIs in the
    org.chromium.android_webview package.
*   unit tests which derive from InstrumentationTestCase. These are intended to
    test functionality implemented entirely in Java.

The slightly misleadingly named folder
[android_webview/unittestjava/](https://code.google.com/p/chromium/codesearch#chromium/src/android_webview/unittestjava/src/org/chromium/android_webview/unittest/)
contains Java code used by native (C++) tests which happen to be testing C++
code containing JNI calls.

## The Android WebView shell (webview_instrumentation_apk ninja target)

The webview_instrumentation_apk target produces a standalone application
containing the C++ and Java code required in order to instantiate and use an
AwContents instance. If launched directly from launcher the test shell presents
a very simple UI which can be used to load pages into the single AwContents
instance on screen. The environment the shell creates is not identical to the
production environment (that is, when this code is actually powering the Android
system WebView). Here are the notable differences:

*   the shell is a self-contained application, where the native library and Java
    code are loaded out of the APK. The WebView runs from within the embedding
    application, and therefore the location of the 'data' folders (profile
    location, for example) changes from application to application, while the
    location of the native library/pak files/etc... is always the same,
*   the shell uses a relatively simple, fixed layout. The WebView is used in a
    wide variety of configurations. While we have tests to cover some of these,
    one can not make any assumptions about how the embedding application will
    display the WebView (it may even never connect it to the view hierarchy and
    manipulate a page in the background using JavaScript).
*   the Android platform injects hooks to the WebView which are necessary to
    render in hardware-accelerated mode. These hooks are not provided to regular
    applications, therefore the test shell is currently limited to rendering in
    software mode only,
*   the shell handles resources (strings/drawables/layout/etc..) in a different
    way to the system WebView,
*   finally, the shell contains a significantly simplified version of the 'glue
    layer' (normally provided by the code in frameworks/webview in the Android
    tree) and does not test any of that code.

## The Android WebView test cases (webview_instrumentation_test_apk ninja target)

The webview_instrumentation_test_apk target contains the test cases from the
android_webview/javatests folder. It is important to remember that this package
does not contain any of the remaining Java or C++ code. This means that when
making any changes to the implementation it is necessary to rebuild and
reinstall WebViewInstrumentation.apk (which is the result of building the
webview_instrumentation_apk ninja target).

## CTS tests

The Android platform contains tests which exercise the WebView API in a
production environment. More information on these tests can be [found
here](https://source.android.com/compatibility/cts-intro.html).

## The WebViewShell in the Android tree

The
[frameworks/webview](https://android.googlesource.com/platform/frameworks/webview/)
project within the Android source tree contains a WebViewShell package. This
should not be confused with the webview_instrumentation_apk shell. It's primary
use is for performance testing since it uses the WebView in a production
environment. The WebViewShell application is a very simple Android application
which uses the **system** WebView. This means that in order to make changes to
the WebView implementation used by the WebViewShell one has to update the
Android system image on which the WebViewShell is running.
