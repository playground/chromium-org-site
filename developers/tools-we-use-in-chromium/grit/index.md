# GRIT

GRIT is the internationalization tool used by the
[Chromium](http://www.chromium.org/) projects (the foundation for the [Google
Chrome](http://www.google.com/chrome) browser) and for several internal projects
at Google.

GRIT is designed to be flexible and extensible enough to use for various
different types of projects, although to date it has primarily been used for
client-side software projects targeting Windows, Mac and Unix platforms.

GRIT requires Python 2.6 or later.

Further resources:

*   [User's Guide](grit-users-guide.md)
*   [Design Overview](grit-design-overview.md)
*   [Regression Test Plan](grit-regression-test-plan.md)
*   [How to contribute to GRIT](how-to-contribute-to-grit.md)
