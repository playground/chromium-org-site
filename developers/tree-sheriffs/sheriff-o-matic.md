# sheriff-o-matic

[**sheriff-o-matic.appspot.com**](https://sheriff-o-matic.appspot.com/)

Sheriff-o-matic only shows you things that are failing right now. It tries to
group failures for you that have the same regression range and show you the most
important problems at the top.

## How to use

See the [help page](https://sheriff-o-matic.appspot.com/help-som) embedded in
Sheriff-o-matic.

(TODO: We can deprecate this page?)

### Trees

Use the menu bar on the left to pick the appropriate tree for your sheriffing
role.

### Snooze button

Snooze is WIP. For now it dims out the alert and moves it to the bottom of the
list for an hour. This is shared amongst all sheriffs.

Eventually we plan for it to allow you to give it a revision number at which
point you believe the problem should be fixed, in which case it would be snoozed
until that revision number passed (if it's still failing of course). We'll build
out the feature more, based off feedback from sheriffs.

### Link bug button

Associates a bug with a failure group. For now this only works with one bug;
eventually this will probably work with multiple bugs. This is shared amongst
all sheriffs.

Both these buttons require you to be logged in with your @google.com account.

### Settings

You can configure the tool e.g. choosing the builder type to use for the links
in the tool.

## Where is the code?

The frontend lives on appengine. The code is at
<https://chromium.googlesource.com/infra/infra/+/master/appengine/sheriff_o_matic/>.
See the README in that directory for more information.

The backend is just a cron that runs in a tight loop and generates the alerts
json feed that gets uploaded to and served from
<https://sheriff-o-matic.appspot.com/alerts>. The code for the cron lives in the
infra repo at
<https://chromium.googlesource.com/infra/infra/+/master/infra/tools/builder_alerts>.

## sheriff-o-matic is down, what do I do?

g.co/bugatrooper

The most common failure mode for sheriff-o-matic is that the backend that
generates the alerts json gets stuck. The good news is that you can run that
backend locally and have it upload the json to sheriff-o-matic. So, while you're
waiting for a trooper to get back to you, you can try running the backend
yourself:

1.  Get an infra checkout with "fetch infra" (this is fast)
2.  cd infra
3.  git fetch origin && gclient sync
4.  while true; do time python run.py infra.tools.builder_alerts
    <https://sheriff-o-matic.appspot.com/alerts>; done

That will generate the alert json and upload it to sheriff-o-matic.
sheriff-o-matic just reads that json and generates the UI from that.
