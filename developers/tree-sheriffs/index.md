# Tree Sheriffs

## I'm a new sheriff, now what?

1.  Read the [tour of the
    waterfall](../testing/chromium-build-infrastructure/tour-of-the-chromium-buildbot/index.md)
    to understand what you are doing.
2.  Read this page
3.  Read the relevant page for your sheriffing type:
    1.  [Chromium](sheriff-details-chromium/index.md) sheriff
    2.  [Memory](sheriff-details-chromium/memory-sheriff.md) sheriff
    3.  [Chromium OS](sheriff-details-chromium-os/index.md) sheriff
    4.  [NaCl](sheriff-details-nacl.md) sheriff
    5.  [GPU](../how-tos/gpu-wrangling/index.md) pixel wrangler
    6.  [Chromium
        Perf](http://dev.chromium.org/developers/tree-sheriffs/perf-sheriffs)
        sheriff

We also have non-tree sheriff rotations: Stability sheriff, [Security
sheriff](../../Home/chromium-security/security-sheriff/index.md), and [Blink bug
labeling](../../blink/blink-triaging.md)

## What is a sheriff?

*   Closes and opens the tree
*   Tracks down people responsible for breakage
*   Backs out broken changes
*   When idle, the sheriff:
    *   improves the tools,
    *   updates the doc,
    *   fix flaky tests,
    *   remove reliability signatures

Sheriffs should have a strong bias towards actions that keep the tree green and
then open. For main waterfall bots that are on the Commit Queue, if a simple
revert can fix the problem the sheriff **should revert first and ask questions
later**. For bots that are not covered by the Commit Queue and if the author is
online, it's fine to ask them to fix asynchronously (since it shouldn't be
blocking anyone, and it's not the author's fault as the change landed through
the CQ).

## What is sheriff-o-matic?

[sheriff-o-matic.appspot.com](http://sheriff-o-matic.appspot.com) is a tool to
automate the work of sheriffing. It shows you just the failures and it
automatically intersects regression ranges across the bots for you. Currently it
only works for the Chromium tree. Making it work for other trees is a small
amount of work: [crbug.com/409693](http://crbug.com/409693).

More on sheriff-o-matic and what to do if it's down:
<https://sites.google.com/a/chromium.org/dev/developers/tree-sheriffs/sheriff-o-matic>.

## What is a trooper?

Troopers know more about maintaining the buildbot masters and slaves themselves.
They're the people to look for when the bots need an OS update, a machine goes
offline, checkouts are failing repeatedly, and so on.

*   **Chromium troopers: **See [contacting a
    trooper](https://chromium.googlesource.com/infra/infra/+/master/doc/users/contacting_troopers.md).
    *   Refer to the [rotation
        calendar](https://calendar.google.com/calendar/embed?src=google.com_3aov6uidfjscpj2hrpsd8i4e7o%40group.calendar.google.com&ctz=America/Los_Angeles)
        to find the current trooper (look in "more details"->"Guests").
*   **Chromium OS troopers:**
    *   Same list as Chromium troopers (see above)
*   **NaCl troopers**: bradnelson, ncbray, TBD

## What is a gardener?

Gardeners are watchers of particular component interactions. They generally
watch a component's release or development and move the version included forward
when it is compatible. This is called "[rolling
DEPS](http://dev.chromium.org/developers/how-tos/get-the-code#Rolling_DEPS)",
and consists of committing a CL that changes the (Subversion) revision number in
some [`DEPS`](https://code.google.com/p/chromium/codesearch#search/&q=file:DEPS)
file, particularly the root
[`DEPS`](https://code.google.com/p/chromium/codesearch#chromium/src/DEPS), and
then seeing if anything breaks. In some cases the DEPS roll also needs to have a
[landmine](../../Home/chromium-clobber-landmines.md) to force a full rebuild
(clobber).

Of particular interest to the Chromium projects are the gardeners who watch the
interaction between Skia and Chromium, and those who watch the interaction of
[Chromium and ChromiumOS](chrome-in-chromeos-gardening.md).

## Our 7 out of 8 goal

*   We have a goal of keeping the tree open 7 hours out of every 8. To achieve
    this goal, sheriffs need to resolve issues as they arise.
*   If the tree is closed automatically, the sheriff needs to change the status
    within a few minutes to let other people know that someone is looking at the
    failures.
*   If the sheriff can keep track of the failures on the tree, and those
    failures are not catastrophic (e.g. compile failing or thousands of test
    failing), then the tree should stay open.
*   Want to know how you did? Check the status viewer for
    [Chromium](http://chromium-status.appspot.com/status_viewer?curView=peak&startTime=TODAY&numDays=60),
    [Chromium
    OS](http://chromiumos-status.appspot.com/status_viewer?curView=peak&startTime=TODAY&numDays=60)
    or [NaCl](http://nativeclient-status.appspot.com/status_viewer). (7/8 =
    87.5%)

## Sheriffing Details

See [Sheriff Details: Chromium](sheriff-details-chromium/index.md) to revert
changes, disable tests, etc.

## Common tasks

### Closing or opening the tree

The tree status can be **closed** or **open**. These status levels control the
activity of the commit queue. If the tree is **open**, the commit queue runs as
normal.

1.  Go to [chromium-status.appspot.com](http://chromium-status.appspot.com/)
    (Chromium) or
    [chromiumos-status.appspot.com](http://chromiumos-status.appspot.com/)
    (Chromium OS).
2.  Change the status.
    *   To close the tree, include the word "closed" in the status.
    *   To open the tree, include the word "open" in the status.

### Effectively communicating tree closure

Annotate the tree status with information about what is known about the status
of build failures. For example, automatic closure messages such as...

Tree is closed (Automatic: "compile" on "Mac Builder" from 12345:
foo@chromium.org)

... should be changed to:

Tree is closed (compile -> johnd)

... to indicate that committer 'johnd' has been notified of the problem and is
looking into it. Once a fix has been checked in, sheriffs often use status:

Tree is closed (cycling green)

... to indicate that a fix/revert has been checked in and the tree will likely
be opened soon. Alternatively, if the sheriffs decided to revert first and ask
questions later, then the tree status should be changed to:

Tree is open (reverted r54321)

### Effectively communicating tree repairs

If the tree has been closed for an extended time, particularly if the breakage
covered more than one working timezone (US Pacific, US Eastern, Europe, Asia),
it is considered best practice to communicate what was needed to fix the
breakage. That way the next sheriff knows what's been happening, and people in
other timezones know what to do next time it breaks the same way.

If the fix was simple, it can be listed in the tree-open status message, such
as...

Tree open (rebooted Chromium (dbg) to clear tempfiles)

Tree open (svn server came back)

Tree open (reverted r54321)

If a more detailed fix was needed, send email to the chromium-dev mailing list
explaining what happened. It's a good idea to CC the current and upcoming
sheriffs too.

## Tips and tricks

### It's clobberin' time!

Sometimes you just need to clobber (i.e. force a full, clean rebuild of) some
class of bots (win, mac+ninja, linux asan using make, etc.). You can do this by
landing a landmine change. Docs are here: [Chromium Clobber
Landmines](../../Home/chromium-clobber-landmines.md).

**Note:** if a specific CL is causing bots to break unless they are clobbered,
that CL should be reverted first and fixed to avoid this.

### Forcing a build

To retry the last build, you can force a build.

1.  Navigate to builder on [internal
    waterfall](https://uberchromegw.corp.google.com/i/chromium/)
    *   From an existing builder URL, you can replace:
        *   **build.chromium.org/p**
        *   to:
        *   **uberchromegw.corp.google.com/i**
2.  Click the name of the builder in the top gray row
3.  Enter your username and an optional reason
4.  Click "Force Build".

For Chromium only, if you check the "Clobber" checkbox, it will also delete the
build output directory before redoing the compile.

**Note:** If this is not a builder (no compile step), then doing a clobber won't
do anything. You need to clobber the "Builder" first.

### Stopping a build

There is an option to stop a build, but *do not use it! *If you stop the build
during the update step, the bot is going to be hosed for sure. Again, don't use
this option, and if you feel like using it, talk to the troopers *first.*

### **Use chromium extensions to annotate buildbot error pages**

Install the buildbot error extension to more quickly isolate errors on stdio
pages. See [Useful extensions for chromium developers](../useful-extensions.md)
for more information.

## PFQ Builds

Documentation is here in the [Pre Flight Queue
documentation](sheriff-details-chromium-os/pre-flight-queue-faq.md)

## **Sheriff schedule**

### Build sheriff calendar (authoritative)

The authoritative list is on Google Calendar. Here's how to add the sheriff
calendar to yours:

1.  Sign in to Google Calendar.
2.  Where it says "Other calendars - Add a friend's calendar" add the address
    for the calendar you want:
    *   Chromium:
        [google.com_r6oah4kurfoe0i3kee16kaitq0@group.calendar.google.com](https://www.google.com/calendar/b/0/render?cid=google.com_r6oah4kurfoe0i3kee16kaitq0@group.calendar.google.com)
    *   Chromium OS:
        [google.com_69d79f055f0a14nuulm9kse60c@group.calendar.google.com](https://www.google.com/calendar/b/0/render?cid=google.com_69d79f055f0a14nuulm9kse60c@group.calendar.google.com)
    *   Chromium OS (non-US West Coast group):
        [google.com_qvc0pmmoql1n9g73scnubmmh6c@group.calendar.google.com](https://www.google.com/calendar/b/0/render?cid=google.com_qvc0pmmoql1n9g73scnubmmh6c@group.calendar.google.com)
    *   Memory:
        [google.com_fv7snsgnk3hgem50fb952aug44@group.calendar.google.com](https://www.google.com/calendar/b/0/render?cid=google.com_fv7snsgnk3hgem50fb952aug44@group.calendar.google.com)
    *   NaCl:
        [google.com_necnlf26slgj8ebp8pen4020kk@group.calendar.google.com](mailto:google.com_necnlf26slgj8ebp8pen4020kk@group.calendar.google.com)
    *   Skia:
        [google.com_8clojmi0ktf75e95e6v9gijrq0@group.calendar.google.com](https://www.google.com/calendar/b/0/render?cid=google.com_8clojmi0ktf75e95e6v9gijrq0@group.calendar.google.com)
    *   GPU:
        [google.com_0eerjipjjbhlrbgf323lg2mb9o@group.calendar.google.com](https://www.google.com/calendar/b/0/render?cid=google.com_0eerjipjjbhlrbgf323lg2mb9o@group.calendar.google.com)
    *   Troopers:
        [google.com_iqfka4i9asiva67vlqqf1es094@group.calendar.google.com](https://www.google.com/calendar/render?cid=google.com_iqfka4i9asiva67vlqqf1es094%40group.calendar.google.com)
    *   Chromium Perf:
        [google.com_2fpmo740pd1unrui9d7cgpbg2k@group.calendar.google.com](https://www.google.com/calendar/b/0/render?cid=google.com_2fpmo740pd1unrui9d7cgpbg2k@group.calendar.google.com)
    *   Blink Bug Triage:
        [google.com_h1kjbmjo6p29ta0rluu8eh5qjo@group.calendar.google.com](https://calendar.google.com/calendar/ical/google.com_h1kjbmjo6p29ta0rluu8eh5qjo%40group.calendar.google.com/public/basic.ics)
    *   Chrome on Android:
        [google.com_kv3vb9rho589q3as0k3i6vlepc@group.calendar.google.com](https://calendar.google.com/calendar/ical/google.com_kv3vb9rho589q3as0k3i6vlepc%40group.calendar.google.com/public/basic.ics)

To see who the sheriff is, click an event and look at the guest list. (Yes, it
would be nice if it showed the people in the event title, but then there's the
issue of the event title and the guest list getting out of sync -- no easy
answer.) To find when a specific person is going to be sheriff, use google
calendar's advanced search box (click the down-triangle in the main search box),
select the appropriate sheriff calendar, and type the person's username into the
"Who" box.

**APAC sheriffs**: Note that the day shown in the calendar is the California /
PST day. The actual day you will sheriff (in your local time) will be the day
*after*. See "APAC and time zones" below.

The script/process that updates the calendars can be found in
svn://svn.chromium.org/chrome-internal/trunk/tools/build/scripts/tools/sheriff.

### Find out who is currently the sheriff (more authoritative)

There are two ways to see who the current sheriff is:

1.  [BuildBot page](https://build.chromium.org/): Listed under "sheriffs" at the
    top left of the page.
2.  [IRC](https://www.chromium.org/developers/irc): "/msg trungl-bot sheriffs"
    to privately ask who is sheriff. (You can also type "sheriffs: blah" to
    publicly ping the sheriffs.)

Please note that if it is Sunday PST / Monday APAC, there won't officially be a
sheriff on duty, but whoever is rostered on Friday in APAC should be sheriffing
(see "APAC and time zones" below).

### How to swap

Schedule conflicts happen. You may need to swap your assigned rotation dates. A
good approach is to pull up the [sheriff
calendar](https://www.chromium.org/developers/tree-sheriffs#TOC-Build-sheriff-calendar-authoritative-)
and reach out to individuals with rotations with nearby dates to yours --
they're often more willing to swap.

To swap shifts with someone, add them to the rotation so that the buildbot and
other tools display the proper people as sheriffs. To do this:

1.  Find your rotation event on **your** calendar (not sheriff cal) and
    **edit**.
2.  **Change your response**
    1.  **choose "No"** for "Are you coming?".
    2.  **Add a note** "swapping with _______"
3.  Below that, where Guests are listed, click **"Add Guest" and enter your
    replacement.**
4.  Hit **"Save"** at the top.
5.  Have your replacement repeat this process on their calendar for the days you
    are taking.

## Useful urls

*   See
    <https://chromium.googlesource.com/chromium/src/+/master/docs/useful_urls.md>
*   LKGR status <http://build.chromium.org/p/chromium/lkgr-status/>

## Random notes

*   Every committer is empowered and encouraged to do any of those things when
    needed, but the sheriff has overall responsibility in case somebody else is
    away or not paying attention.
*   The sheriffs receive gatekeeper emails when the tree is being closed
    automatically. Please take action on these as soon as you can.
*   **Everyone helps!** Committers must request to be signed up to be a sheriff.
    If you're a new committer, submit a code review to add yourself
    (Google-internal:
    <https://chrome-internal.googlesource.com/infra/infra_internal.git/+/master/infra_internal/services/sheriff/>)
    or ping [g.co/bugatrooper](http://g.co/bugatrooper). You can find your time
    as sheriff at the "Upcoming sheriffs" list at the end of the Sheriff details
    pages (e.g., for
    [Chromium](http://www.chromium.org/developers/tree-sheriffs/sheriff-details-chromium#TOC-Upcoming-sheriffs-not-authoritative)).
    If you need to change the schedule (you're out sick or on vacation), it's
    your responsibility to find a replacement for your time slot.

## Time Zones

PST(CA, WA) EST(NY, Montreal) UTC(London) CET(Munich) JST(Tokyo) 4PM 7PM 0AM 1AM
9AM 5PM 8PM 1AM 2AM 10AM 6PM 9PM 2AM 3AM 11AM 7PM 10PM 3AM 4AM 12PM 8PM 11PM 4AM
5AM 1PM 9PM 0AM 5AM 6AM 2PM 10PM 1AM 6AM 7AM 3PM 11PM 2AM 7AM 8AM 4PM 0AM 3AM
8AM 9AM 5PM 1AM 4AM 9AM 10AM 6PM 2AM 5AM 10AM 11AM 7PM 3AM 6AM 11AM 12PM 8PM 4AM
7AM 12PM 1PM 9PM 5AM 8AM 1PM 2PM 10PM 6AM 9AM 2PM 3PM 11PM 7AM 10AM 3PM 4PM 12PM
8AM 11AM 4PM 5PM 1AM 9AM 12PM 5PM 6PM 2AM 10AM 1PM 6PM 7PM 3AM 11AM 2PM 7PM 8PM
4AM 12PM 3PM 8PM 9PM 5AM 1PM 4PM 9PM 10PM 6AM 2PM 5PM 10PM 11PM 7AM 3PM 6PM 11PM
12PM 8AM

### APAC and time zones

This guideline is under discussion. See [this thread on
chromium-dev](https://groups.google.com/a/chromium.org/forum/#!topic/chromium-dev/W8kl0zREv6I).
The following is the practice that has been adopted by Sydney office.

If you are sheriffing from an APAC time zone (e.g., Tokyo, Sydney, etc), please
note that the calendar shows times in the California / PST time zone which is
one day behind. You should sheriff *when it is that day in California* (i.e.,
one day later in your local time zone).

*   If you are rostered for Monday, sheriff on **Tuesday** (Monday in PST).
*   If you are rostered for Tuesday, sheriff on **Wednesday** (Tuesday in PST).
    Et cetera.
*   **But,** if you are rostered for Friday, sheriff on the following **Monday**
    (Sunday in PST; you aren't expected to work on a Saturday!).

Note that the [BuildBot](https://chromium-build.appspot.com/p/chromium/console)
and [trungl-bot](https://www.chromium.org/developers/irc) systems will think you
are sheriffing on a Saturday, and will say the sheriff is "None" on the Monday
when you should be sheriffing. Sorry about that.

Infra is aware that this is awkward, and has plans to fix it eventually.

### IRC commands

trungl-bot: offices List office names for "time" command trungl-bot: time
*office* Display local time of specified office, e.g. trungl-bot: time NYC
