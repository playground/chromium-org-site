# Perf Sheriffing

Perf sheriffing has now been split into two separate rotations:

*   [**Performance Regression
    Sheriffing**](https://chromium.googlesource.com/chromium/src/+/master/docs/speed/perf_regression_sheriffing.md)
*   [**Perfbot Health
    Sheriffing**](https://chromium.googlesource.com/chromium/src/+/master/docs/speed/perf_bot_sheriffing.md)
