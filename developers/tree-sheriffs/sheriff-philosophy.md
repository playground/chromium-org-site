# Sheriff Philosophy

This page collects ideas about what it means to be a good sheriff. This provides
more background and motivation for why you're sheriffing and what you should do.

### Sheriffing is all about communication and availability.

The sheriff's number one job is to respond to tree closures and communicate what
is going on, not to fix the problems. (Job #2: public safety: re-open the tree
when it's safe to do so.) Quickly updating the tree status and then tracking
down owners for issues is much better than silently poring over logs for 15
minutes while other people are probably doing the same thing. You're the mutex,
not the main thread. Signal early, signal often.

### Sheriffing is a full-time job.

Please put your day job on hold for two days and focus on the commons. (You're
only asked to do this once or twice a year.) In fact, instead of passing the
quiet time by trying to do your day job, you should be thinking about making
sheriffing easier or fixing flaky tests. Your time is already blocked off for
sheriffing, so it's OK to dedicate that time to the commons.

Drop out of all meetings during your shift. People can deal with your absence
from a meeting when you're sick or on vacation. When you're sheriff, they should
pause the meeting for a moment of silence to thank you for watching the tree
while they're meeting :).

### IRC is your posse and we got your back.

Yes, you can take a break. Everyone has to go to lunch. It's conceivable that a
sheriff might benefit from not looking at the waterfall for 10 minutes while she
makes herself a coffee (or eliminates the previous coffee). Just see point #1:
communicate that you're going to be offline/AFK and for how long. If you're
lucky enough to have a co-sheriff, arrange for coverage. Update the tree
status/IRC channel when you go home for the day so everyone knows that
sheriffing is back in the public domain.

Moreover, the IRC channels are full of people who can help when you're out of
your depth.

Use <http://webchat.freenode.net/> or
[CIRC](https://chrome.google.com/webstore/detail/circ/bebigdkelppomhhjaaianniiifjbgocn)
if you don't have an IRC client installed.

Channels: #chromium or #chromium-os.

### Sheriffing is not about you, it's about helping the team.

If you can't be a good, full-time sheriff during your shift, [arrange for a
swap](index.md). If your job requires lots of meetings and interruptions, stay
off the sheriff rotation: you might think you're being a good citizen, but if
you really can't dedicate yourself to the shift full-time, you're actually
hurting the team.

Conversely, if you're new to this and don't think you can do a good job... don't
sweat it as long as you want to help the team. See points 1 and 3: communicate
and ask for help on IRC and you'll not only be helping the team, but you'll be
learning and gaining confidence to be even better next time.

Also, we don't need martyrs. If you're on the rotation for WebKit gardening, you
don't need to be on the Chromium rotation (and the memory rotation and the
Chromium OS rotation). If you're so awesome that you can be on multiple
rotations, join the troopers rotation and become the escalation point for mere
mortal sheriffs. (Contact a trooper for details - if you don't know any, you can
find the currently scheduled one [here](https://chromium-build.appspot.com/).)

### Pass the torch.

Whether you're a first-timer or an old hand, letting the team know about the
problems you had during your shift helps. Most of the time, these problems are
issues we need to address as a team but we can't take action until someone
brings them up. Sheriffing is not penance and there's no need to suffer in
silence. If there are flaky bots that plagued you or missing documentation or
*anything* you didn't feel prepared to handle, you're probably not alone.

Send email to the sheriffs who come after you to document the shi(f)t that
happened to you. It will help them. If you want to want, e-mail chromium-dev or
chromium-os dev tell help the rest of the team understand common patterns that
need to be addressed.

If you have theses to add, please do so. Keeping the trees green is a community
effort and depends on people sharing their knowledge.
