# Bypassing tests on a per-project basis

This document has moved to a [new
location](../../../../chromium-os/build/bypassing-tests-on-a-per-project-basis.md).
