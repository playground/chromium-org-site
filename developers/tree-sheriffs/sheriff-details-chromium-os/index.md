# Sheriff FAQ: Chromium OS

The[ thesis on Sheriffing](../sheriff-philosophy.md).

## The purpose of sheriffing is threefold:

1.  Make sure build blocking failures are identified and addressed in a timely
    fashion.

2.  Manually watch over our build system in ways automation doesn’t/can’t do.

3.  Give developers a chance to learn a little more about how build works and
    breaks.

## The expectations on a sheriff are as follows:

1.  Do not leave the tree unattended; coordinate with your co-sheriffs to ensure
    this. Normal work hours (for you) only.

2.  Address tree closure/throttling.

    1.  Promptly update the[ tree status](http://chromiumos-status.appspot.com/)
        to show you are investigating.

    2.  File or locate bugs. The tree should not close unless something broke.
        File new bugs, or update existing ones. Include links to the failed
        build. Raise priority as needed.

    3.  Find owners for bugs that break your builds. It’s not the Sheriff’s job
        to fix things, only to find the right person.

    4.  Reopen when ready. If the next run can reasonably be expected to pass,
        reopen.

3.  Watch for build (especially CQ) Failures

    1.  Mark CLs that fail a CQ run with Verified -1. If done before the CQ run
        finishes, this prevents innocent CLs from being blamed.

    2.  File bugs for flake that fails a build. This is how we get
        build/infrastructure/test flake fixed, so it stops happening. Identify
        existing bugs where you can, or file new ones. Some bugs are auto filed,
        this should be called out in waterfalls or logs.
        [go/cros-infra-bug](http://goto.google.com/cros-infra-bug)

    3.  Identify confusing output. If something about the waterfall or build
        results don’t make sense, file a bug. BE VERY SPECIFIC. This feedback
        needs to be actionable.

4.  While the tree is open, work on[ gardening
    tasks](https://code.google.com/p/chromium/issues/list?can=2&q=label%3AGardening)
    -- not your normal work items.

## **You can find the calendars here:**

*   [US](https://www.google.com/calendar/embed?src=google.com_69d79f055f0a14nuulm9kse60c@group.calendar.google.com)

*   [Non-US](https://www.google.com/calendar/embed?src=google.com_qvc0pmmoql1n9g73scnubmmh6c@group.calendar.google.com)

## What should I do when I report for duty?

## At the beginning of your stint as Sheriff, please perform the following tasks:

1.  (Public discussion) Sign on to irc.freenode.net / #chromium-os by browsing
    to <http://webchat.freenode.net/> or getting
    [CIRC](https://chrome.google.com/webstore/detail/circ/bebigdkelppomhhjaaianniiifjbgocn)
    if you don't have an IRC client installed. Then choose channel #chromium-os,
    and introduce yourself as an on-duty sheriff.
2.  (Internal discussion) Join the[
    #crosoncall](https://comlink.googleplex.com/crosoncall) channel on comlink
    or irc.corp.google.com
3.  Pull up the
    [public](http://build.chromium.org/p/chromiumos/waterfall?reload=60) and
    [internal](https://uberchromegw.corp.google.com/i/chromeos/waterfall?reload=60)
    Chrome OS buildbot waterfalls. If the status is out of date, update it or
    attach ?reload=60 at the end of URL to refresh page automatically. Internal
    waterfall is more closely related to Chromium OS.
4.  Pull up the Chrome OS Sheriff-o-Matic dashboard via
    [go/som](http://go/som/chromeos). Sheriff-o-Matic lists recent build
    failures as alerts that should be triaged. Alerts can be commented upon,
    linked to bugs, or snoozed once handled.
5.  When necessary, change the [tree status
    here](https://chromiumos-status.appspot.com/).
6.  Triage any buildbot failure messages in your Inbox.
7.  Read the email from yesterday’s Sheriffs and/or look at the [Chromium OS
    sheriff log](sheriff-log-chromium-os/index.md), and familiarize yourself
    with the [TreeCloser
    issues](https://code.google.com/p/chromium/issues/list?can=2&q=Hotlist%3DTreeCloser+os%3Dchrome)
    they cite. (Don't understand all the buzzwords? Check the
    [glossary](http://go/cros-sheriff-glossary).)

## What should I do as I prepare to end my shift?

At the end of your stint as Sheriff, please perform the following tasks:

1.  Ensure that TreeCloser issues have been filed for any ongoing failures (like
    a flaky Chrome SEGV that is under investigation) or infrastructure problems
    (git hangs during sync, say)
2.  Update the [Chromium OS sheriff log](sheriff-log-chromium-os/index.md) and
    [Email chromium-os-dev@
    ](mailto:chromium-os-dev@chromium.org?subject=Sheriff%20Handoff)with the
    list of issues and a blurb describing anything else of note -- perhaps you
    just reverted a bad change, and expect a bot to cycle green.

## How do I read the waterfall?

*   What do the different tree statuses (open, throttled, closed) mean and do?

    *   Open - ToT is believed to be a healthy, with builds that can make it
        through the commit queue and canary builders without failing. Commit
        queue will run unencumbered.

    *   Throttled - Set this state if the CQ is expected to fail. CQ will run,
        but CLs are not blamed for failure. Description should include cause and
        bug numbers. Often Pri-0.

    *   Closed - Stops new CQ runs. Set this state if further runs will cause
        harm. Description should include cause and bug numbers. Pri-0 bugs are
        expected.

*   What is the Commit Queue? (\*-paladin)

    *   Please read the [Commit Queue overview](commit-queue-overview/index.md).

    *   The CQ Master decides success/failure for a given run. You mostly
        shouldn’t look at slaves except to see failure details. The Master
        should link to appropriate slaves.

*   What is a Canary Build?

    *   An official build (with a unique version) is prepared and validated by
        canary builders three times a day.

    *   The Canary Master decides success/failure for a given run. You mostly
        shouldn’t look at slaves, except to see failure details. The Master
        should link to appropriate slaves.

*   What is the Pre-CQ?

    *   The Pre-CQ launches tryjobs to validate CLs before they enter the CQ,
        but doesn’t do anything other than launch them on standard tryjob
        servers.

*   What is a PFQ builder?

    *   Please read the [Pre Flight Queue FAQ](pre-flight-queue-faq.md). Contact
        the Chrome Gardener (listed on waterfall) if it stays red.

*   What is ASAN bot?

    *   Please read [Sheriff FAQ: Chromium OS
        ASAN](sheriff-faq-chromium-os-asan-bots.md).

*   A toolchain buildbot is failing. What do I do?

    *   These buildbots test the next version of the toolchain and are being
        sheriff'd by the toolchain team. Not your problem.

## How do I find out about build failures?

1.  Watch the tree status. On the waterfall.

2.  **Watch Sheriff-o-Matic. **[go/som](http://go/som/chromeos) shows the recent
    CQ, Canary and PFQ failures that should be addressed by Sheriffs.

3.  Watch the waterfalls (internal and external)

4.  Warning emails. As Sheriff, you’ll get automated emails about various
    failures. You should always follow up on these emails, and file bugs or
    close the tree as appropriate.

## How do I deal with build failures?

When Sheriffs encounter build failures on the public Chromium OS buildbot, they
should follow the following process:

1.  Update the[ Tree Status Page](http://chromiumos-status.appspot.com/) to say
    you're working on the problem.

    *   Example: Tree is throttled (build_packages failure on arm ->
        johnsheriff)

2.  See if you could fix it by reverting a recent patch

    *   If the build or test failure has a likely culprit, contact the author.
        If you can’t, revert![Tree Sheriffsi](../index.md)

    *   Infrastructure failure (repo sync hang, archive build failure, etc)?
        Contact a [Trooper](../index.md)!

        *   After contacting a [Trooper](../index.md), work with him/her to file
            a[ TreeCloser
            issue](http://code.google.com/p/chromium-os/issues/list?q=label%253ATreeCloser&order=pri).

3.  Update the[ Tree Status Page](http://chromiumos-status.appspot.com/) with
    details on what is broken and who is working on it.

    *   Example: Tree is throttled (libcros compile error -> jackcommitter,
        http://crosbug.com/1234).

4.  Make sure the issue is fixed.

    *   If the build-breaker is taking more than a 5-10 minutes to land a fix,
        ask him/her to revert.

    *   If the build-breaker isn’t responding, perform the revert yourself.

5.  Watch the next build to make sure it completes cleanly.

    *   Buildbots only send email when they change in status from green to red,
        and the tree is open. So, if a buildbot is red, it won't send email
        about failures until it completes successfully at least once.

    *   Sheriffs are responsible for watching buildbots and making sure that
        people are working on making them green.

    *   If there's any red on the dashboard, the Sheriffs should be watching it
        closely to make sure that the issues are being fixed. If there’s not,
        the Sheriffs should be working to improve the sheriffability of the tree

## What bugs do I file, and how do I assign them?

*   If a test fails, or a specific component fails to compile, file a bug
    against that component normally. Assign it to an owner for that component.

*   If you believe there's an issue with the lab or test infrastructure, you can
    file a bug following the instructions at
    [go/chromeos-lab-bug](http://go/chromeos-lab-bug)

## Ahh, the tree is green. I can go back to my work, right?

Wrong! When the tree is green, it’s a great time to start investigating and
fixing all the niggling things that make it hard to sheriff the tree.

*   Is there some red-herring of an error message? Grep the source tree to find
    out where it’s coming from and make it go away.

*   Some nice-to-have piece of info to surface in the UI? Talk to[
    ](http://chromegw/i/chromeos/chromeos_build_deputy.js)[Infrastructure
    Deputy](http://goto.google.com/cros-infra-deputy) and figure out how to make
    that happen.

*   Some test that you wish was written and run in suite:smoke? Go write it!

*   Has the tree been red a lot today? Get started on your[
    postmortem](https://sites.google.com/a/google.com/chromeos/resources/tree-closure-postmortems)!

*   Still looking for stuff to do? Try flipping through the[ Gardening
    Tasks](https://code.google.com/p/chromium/issues/list?can=2&q=label%3AGardening).
    Feel free to hack on and take any of them!

*   Run across a build-related term or acronym and didn't know what it meant?
    Check the [glossary](http://go/cros-sheriff-glossary) and add it if it's
    missing.

## How can I pick a specific set of changes to go through a commit queue run?

If the tree is broken, and you have a change or set of changes that you believe
should fix it, it may be desirable to put just that change or set of changes
through the commit queue. This can be accomplished by:

*   Set the tree to Throttled.

*   Set the Commit-Queue value of the desired CLs to +2 (along with the usual
    CR+2 and V+1). This will allow the CLs to be picked up by the CQ even when
    the tree is throttled.

If the CLs pass the commit queue, they will be committed to the tree, and all
the usual side-effects of a commit queue run will take place (such as ebuild
revbumping and prebuilt generation).

## What should I do if I see a commit-queue run that I know is doomed?

If there is a commit queue run that is known to be doomed due to a bad CL and
you know which CL is bad, you can manually blame the bad CL to spare the
innocent CLs from being rejected. Go to the Gerrit review page of the bad CL and
set verify to -1. CQ will recognize the gesture and reject only the bad CL(s).

If the commit queue run has encountered infrastructure flake and is doomed, most
of the time CQ will not reject any CLs (with chromite CLs being the exception).

In other cases where it is not necessary to wait for the full results of the
run, you can save developer time and hassle by [aborting the current cq
run](https://sites.google.com/a/google.com/chromeos/for-team-members/infrastructure/chromeos-admin/manual-cq-intervention).

## How do I deal with a broken Chrome?

If a bug in Chrome escapes the PFQ, you should engage with the Chrome gardener.
He/she will be responsible for finding and then fixing or reverting the Chrome
CL that caused the problem.

If the Chrome bug is serious enough to be causing failures on the canaries or
the CQ, you should temporarily pin Chrome back to the last working version with
this procedure:

*   Make sure there's a bug filed for the problem you're dealing with. You'll
    need the bug number for the next step; call it `$BUG`.
*   Get a chroot that's up-to-date with the bug (it must contain the new, broken
    version of Chrome). Inside that chroot, run this command:

    cros pinchrome --bug $BUG

*   The command will upload two CLs. Commit them both to the tree.
*   Once Chrome is fixed, you need to unpin Chrome. Do that with this command:

    cros pinchrome --unpin

## How can I revert a commit?

If you've found a commit that broke the build, you can revert it using these
steps:

1.  Find the Gerrit link for the change in question

    *   [Gerrit lists recently merged
        changes](http://gerrit.chromium.org/gerrit/#q,status:merged,n,z). View
        the change in question.

    *   In some cases (like the PFQ), you can see links to the CL that made it
        into the build straight from the waterfall.

2.  Click the “Revert Change” button and fill in the dialog box.

    *   The dialog box includes some stock info, but you should add on to it.
        e.g. append a sentence such as: This broke the tree (xxx package on xxx
        bot).

3.  You are not done. This has created an Open change for _you_ go find and
    submit it.

    *   Add the author of the change as a reviewer of the revert, but push
        without an LGTM from the reviewer (just approve yourself and push).

## Help with specific failure catagories

### How do I investigate VMTest failures?

There are several common reasons why the VMTests fail. First pull up the stdio
link for the VMTest stage, and then check for each of the possibilities below.

#### Auto-test test failed

Once you've got the VMTest stage's stdio output loaded, search for 'Total PASS'.
This will get you to the Autotest test summary. You'll see something like

Total PASS: 29/33 (87%)

Assuming the number is less than 100%, there was a failure in one of the
autotests. Scroll backwards from 'Total PASS' to identify the specific test (or
tests) that failed. You'll see something like this:

/tmp/cbuildbotXXXXXX/test_harness/all/SimpleTestUpdateAndVerify/<...>/login_CryptohomeMounted
\[ FAILED \]
/tmp/cbuildbotXXXXXX/test_harness/all/SimpleTestUpdateAndVerify/<...>/login_CryptohomeMounted
FAIL: Unhandled JSONInterfaceError: Automation call {'username':
'performancetestaccount@gmail.com', 'password': 'perfsmurf', 'command': 'Login'}
received empty response. Perhaps the browser crashed.

In this case Chrome failed to login for any of the 3 reasons: 1) could not find
network, 2) could not get online, 3) could not show webui login prompt. Look for
chrome log in /var/log/chrome/chrome, or find someone who works on UI.

(If you're annoyed by the long string before the test name, please consider
working on [crbug.com/313971](http://crbug.com/313971), when you're gardening.)

#### Crash detected

Sometimes, all the tests will pass, but one or more processes crashed during the
test. Not all crashes are failures, as some tests are intended to test the crash
system. However, if a problematic crash is detected, the VMTest stdio output
will have something like this:

Crashes detected during testing:
---------------------------------------------------------- chrome sig 11
login_CryptohomeMounted

If there is a crash, proceed to the next section, "How to find test
results/crash reports"?

#### ASAN error detected

The x86-generic-asan and amd64-generic-asan builders instrument some programs
(e.g. Chrome) with code to detect memory access errors. When an error is
detected, ASAN prints error information, and terminates the process. Similarly
to crashes, it is possible for all tests to pass even though a process
terminated.

If Chrome triggers an ASAN error report, you'll see the message "Asan crash
occurred. See asan_logs in Artifacts". As suggested in that message, you should
download "asan_logs". See the next section, "How to find test results/crash
reports" for details on how to download those logs.

Note: in addition to Chrome, several system daemons (e.g. shill) are built with
ASAN instrumentation. However, we don't yet bubble up those errors in the test
report. See [crbug.com/314678](http://crbug.com/314678) if you're interested in
fixing that.

#### ssh failed

The test framework needs to log in to the VM, in order to do things like execute
tests, and download log files. Sometimes, this fails. In these cases, we have no
logs to work from, so we need the VM disk image instead.

You'll know that you're in this case if you see messages like this:

Connection timed out during banner exchange Connection timed out during banner
exchange Failed to connect to virtual machine, retrying ...

When this happens, look in the build report for "vm_disk" and "vm_image" links.
These should be right after the "stdio" link. For example, if you're looking at
the [build
report](http://chromegw/i/chromeos/builders/lumpy%20nightly%20chrome%20PFQ/builds/3977)
for "lumpy nightly chrome PFQ Build #3977" :

1.  [VMTest (attempt
    1)](http://chromegw/i/chromeos/builders/lumpy%20nightly%20chrome%20PFQ/builds/3977/steps/VMTest%20%28attempt%201%29)
    VMTest (attempt 1) ( 19 mins, 5 secs )

1.  1.  [stdio](http://chromegw/i/chromeos/builders/lumpy%20nightly%20chrome%20PFQ/builds/3977/steps/VMTest%20%28attempt%201%29/logs/stdio)
1.  2.  [vm_disk:
        failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_disk.bin.8Fet3d.tar](https://storage.cloud.google.com/chromeos-image-archive/lumpy-chrome-pfq/R33-5080.0.0-rc1/failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_disk.bin.8Fet3d.tar)
1.  3.  [vm_memory:
        failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_mem.bin.TgS3dn.tar](https://storage.cloud.google.com/chromeos-image-archive/lumpy-chrome-pfq/R33-5080.0.0-rc1/failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_mem.bin.TgS3dn.tar)
1.  4.  [vm_test_results_1.tgz](https://storage.cloud.google.com/chromeos-image-archive/lumpy-chrome-pfq/R33-5080.0.0-rc1/vm_test_results_1.tgz)

2.  [VMTest (attempt
    2)](http://chromegw/i/chromeos/builders/lumpy%20nightly%20chrome%20PFQ/builds/3977/steps/VMTest%20%28attempt%202%29)
    VMTest (attempt 2) ( 36 mins, 29 secs )

2.  1.  [stdio](http://chromegw/i/chromeos/builders/lumpy%20nightly%20chrome%20PFQ/builds/3977/steps/VMTest%20%28attempt%202%29/logs/stdio)
2.  2.  [vm_disk:
        failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_disk.bin.uvzLqx.tar](https://storage.cloud.google.com/chromeos-image-archive/lumpy-chrome-pfq/R33-5080.0.0-rc1/failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_disk.bin.uvzLqx.tar)
2.  3.  [vm_memory:
        failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_mem.bin.6lSD2q.tar](https://storage.cloud.google.com/chromeos-image-archive/lumpy-chrome-pfq/R33-5080.0.0-rc1/failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_mem.bin.6lSD2q.tar)
2.  4.  [vm_test_results_2.tgz](https://storage.cloud.google.com/chromeos-image-archive/lumpy-chrome-pfq/R33-5080.0.0-rc1/vm_test_results_2.tgz)

Download the disk and memory images, and then resume the VM using kvm on your
workstation.

$ tar --use-compress-program=pbzip2 -xf \\
failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_disk.bin.8Fet3d.tar

$ tar --use-compress-program=pbzip2 -xf \\
failed_SimpleTestUpdateAndVerify_1_update_chromiumos_qemu_mem.bin.TgS3dn.tar

$ cros_start_vm \\
--image_path=chromiumos_qemu_disk.bin.8Fet3d \\
--mem_path=chromiumos_qemu_mem.bin.TgS3dn

You should now have a VM which has resumed at exactly the point where the test
framework determined that it could not connect.

Note that, at this time, we don't have an easy way to mount the VM filesystem,
without booting it. If you're interested in improving that, please see
[crbug.com/313484](http://crbug.com/313484).)

For more information about troubleshooting VMs, see [how to run Chrome OS image
under
VMs](http://www.chromium.org/chromium-os/how-tos-and-troubleshooting/running-chromeos-image-under-virtual-machines).

### How to find test results/crash reports?

The complete results from VMTest runs are available on googlestorage, by
clicking the \[ Artifacts \] link in-line on the waterfall display in the report
section:

![image](Screenshot.png)

From there, you should see a file named chrome.\*.dmp.txt that contains the
crash log. Example

![image](crash-example.png)

If you see a stack trace here, search for issues with a similar call stack and
add the google storage link, or file a new issue.

### How do I extract stack traces manually?

Normally, you should never need to extract stack traces manually, because they
will be included in the Artifacts link, as described above. However, if you need
to, here's how:

1.  Download and extract the test_results.tgz file from the artifact (above),
    and find the breakpad .dmp file.
2.  Find the build associated with your crash and download the file debug.tgz
    1.  Generally the debug.tgz in the artifacts should be sufficient
    2.  For official builds, see
        [go/chromeos-images](https://goto.google.com/chromeos-images)
    3.  TODO: examples of how to find this for cautotest and trybot(?) failures
3.  Untar (tar xzf) this in a directory under the chroot, e.g.
    ~/chromeos/src/scripts/debug
4.  From inside the chroot, run the following: minidump_stackwalk
    \[filename\].dmp debug/breakpad > stack.txt 2>/dev/null
5.  stack.txt should now contain a call stack!

If you successfully retrieve a stack trace, search for issues with a similar
call stack and add the google storage link, or file a new issue.

Note that in addition to breakpad dmp files, the test_results.tgz also has raw
linux core files. These can be loaded into gdb and can often produce better
stack traces than minidump_stackwalk (eg. expanding all inlined frames).

### A buildbot slave appears to be down (purple). What do I do?

Probably nothing. Most of the time, when a slave is purple, that just indicates
that it is restarting. Try wait a few minutes and it should go green on its own.
If the slave doesn't restart on its own, contact the Deputy and ask them to fix
the bot.

### platform_ToolchainOptions autotest is failing. What do I do?

This test searches through all ELF binaries on the image and identifies binaries
that have not been compiled with the correct hardened flags.

To find out what test is failing and how, look at the \*.DEBUG log in your
autotest directory. Do a grep -A10 FAILED \*.DEBUG. You will find something like
this:

05/08 09:23:33 DEBUG|platform_T:0083| Test Executable Stack 2 failures, 1 in
whitelist, 1 in filtered, 0 new passes FAILED:

/opt/google/chrome/pepper/libnetflixplugin2.so

05/08 09:23:33 ERROR|platform_T:0250| Test Executable Stack 1 failures

FAILED:

/path/to/binary

This means that the test called "Executable Stack" reported 2 failures, there is
one entry in the whitelist of this test, and after filtering the failures
through the whitelist, there is still a file. The name of the file is
/path/to/binary.

The "new passes" indicate files that are in the whitelist but passed this time.

To find the owner who wrote this test, do a git blame on this file:[
http://git.chromium.org/gitweb/?p=chromiumos/third_party/autotest.git;a=blob;f=client/site_tests/platform_ToolchainOptions/platform_ToolchainOptions.py;h=c1ab0c275a5995c2ad62eb9dd8ba677b5d10e5a2;hb=HEAD](http://git.chromium.org/gitweb/?p=chromiumos/third_party/autotest.git;a=blob;f=client/site_tests/platform_ToolchainOptions/platform_ToolchainOptions.py;h=c1ab0c275a5995c2ad62eb9dd8ba677b5d10e5a2;hb=HEAD)
and grep for the test name ("Executable Stack" in this case).

Find the change that added the new binary that fails the test, or changed
compiler options for a package such that the test now fails, and revert it. File
an issue on the author with the failure log, and CC the owner of the test (found
by git blame above).

## Who should I contact regarding ARC++ issues?

Visit [go/arc++docs](http://go/arc++docs) and see the Contact Information
section.

## How are sheriff rotations scheduled? How do I make changes?

Please see [go/ChromeOS-Sheriff-Schedule](http://go/ChromeOS-Sheriff-Schedule)
for all details on scheduling, shift swaps, etc.

## **Tips**

[Install helpful extensions](../../useful-extensions.md).

You can setup specific settings in the buildbot frontend so that you are
watching what you want and also having it refresh as you want it.

Navigate to the buildbot frontend you are watching and click on customize, in
the upper left corner. From there you can select what builds to watch and how
long between refreshes you want to wait.

If looking at the logs in your browser is painful, you can actually vim the
log's URL and vim will wget it and put you in a vim session to view it.

Other handy links to information:

*   [CrOS Sheriff Glossary](http://go/cros-sheriff-glossary) - a glossary of
    CrOS build terminology. Don't know what a word means? Check here.
*   [Cros-Sheriffing YAQ](https://yaqs.googleplex.com/eng/t/cros-sheriffing) - a
    Quora-like Q&A site monitored by a number of people with build clues.
