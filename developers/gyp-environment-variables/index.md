# GYP build parameters

GYP has many **environment variables** that configure the way Chrome is built.
By modifying these parameters, you can build chrome for different architectures,
or speed up the build process. Generally, we strive to have good defaults and
you shouldn't have to set many of these. The wiki also has some additional tips
on [common build
tasks](https://code.google.com/p/chromium/wiki/CommonBuildTasks).

If you are using GN instead of GYP, the corresponding page is [GN build
configuration](../gn-build-configuration.md).

## Specifying parameters

### Command-line

To specify build parameters for GYP, you can do so on the command line:

$ ./build/gyp_chromium -Dchromeos=1 -Dcomponent=shared_library

### Environment variable

Or with an environment variable:

$ export GYP_DEFINES="chromeos=1 component=shared_library" $
./build/gyp_chromium

### chromium.gyp_env file

Or in a chromium.gyp_env file in your chromium directory:

$ echo "{ 'GYP_DEFINES': 'chromeos=1 component=shared_library' }" >
../chromium.gyp_env

### Some common file examples:

**Windows**:

{ 'GYP_DEFINES': 'component=shared_library' }

**Mac**:

{ 'GYP_DEFINES': 'fastbuild=1' }

**Linux**:

{ 'GYP_DEFINES': 'component=shared_library remove_webcore_debug_symbols=1' }

## ChromeOS on Linux:

{ 'GYP_DEFINES': 'chromeos=1 component=shared_library
remove_webcore_debug_symbols=1' }

## Android:

{ 'GYP_DEFINES': 'OS=android' }

## Recommended Parameters

For a full list of the main build variables and their defaults, see the
variables section in any \*.gyp or \*.gypi file, e.g.:

$ cat build/common.gypi

### Variable

### Explanation

### Linux

### Windows

### Mac

component=shared_library If you dynamically link, you save a lot of time linking
for a small time cost during startup. On Windows, this uses a DLL build and
incremental linking, which makes linking much faster in Debug.

![image](menu_check.png)

![image](menu_check.png) ![image](menu_check.png) fastbuild=1 **Optional:
removes all debug information, but builds much faster. **0 means all debug
symbols (.pdb generated from .obj), 1 means link time symbols (.pdb generated
from .lib) and 2 means no pdb at all (no debug information). In practice 0 and
1, will generate the exact same PE. Only 2 will generate a PE that could have
it's .rdata to be ~40 bytes smaller.

![image](menu_check.png)

![image](menu_check.png) ![image](menu_check.png) remove_webcore_debug_symbols=1
If you don't need to trace into WebKit, you can cut down the size and slowness
of debug builds significantly by building WebKit without debug symbols.
![image](menu_check.png)

### Building a release build

To build Chrome in a release build, as well as building in the out/Release
directory, you may add the following flags:

### Variable

### Explanation

### Linux

### Windows

### Mac

buildtype=Official Builds the heavily optimized official release build (invalid
for builds in out/Debug). Other options include 'Dev', for development/testing.
Official builds take ***drastically*** longer to build, and are most likely not
what you want. ![image](menu_check.png) ![image](menu_check.png)
![image](menu_check.png) branding=Chrome Changes the branding from 'Chromium' to
'Chrome', for official builds. ![image](menu_check.png) ![image](menu_check.png)
![image](menu_check.png)

### Use goma distributed build (Googlers only)

See [go/ma](https://sites.google.com/a/google.com/goma/) for more information on
how to set up goma.

### Use the icecc linker (Linux only)

[Icecc](https://github.com/icecc/icecream) is the distributed compiler with a
central scheduler to share build load. Currently, many external contributors use
it. e.g. Intel, Opera, Samsung.

### Variable

### Explanation

### Linux

### Windows

### Mac

linux_use_bundled_binutils=0 -B option is not supported. See [this
commit](https://github.com/icecc/icecream/commit/b2ce5b9cc4bd1900f55c3684214e409fa81e7a92)
for more details. ![image](menu_check.png)
linux_use_debug_fission=0 [Debug fission](http://gcc.gnu.org/wiki/DebugFission)
is not supported. See [this bug](https://github.com/icecc/icecream/issues/86)
for more details. ![image](menu_check.png)
clang=0 Icecc doesn't support clang yet. ![image](menu_check.png)

### Build ChromeOS on Linux (Linux only)

To build ChromeOS on Linux, use the following gyp variables and the regular
chrome target will run ChromeOS instead of Chrome.

### Variable

### Explanation

### Linux

### Windows

### Mac

chromeos=1 Build for the ChromeOS platform instead of Linux.
![image](menu_check.png)

### Build Android (Linux only)

To build ChromeOS on Linux, use the following gyp variables and the either the
chrome_shell_apk or webview_instrumentation_apk target.

### Variable

### Explanation

### Linux

### Windows

### Mac

OS=android Build for the Android platform instead of Linux.
![image](menu_check.png)
target_arch=ia32 **Optional. **Use this if building for x86 targets.
![image](menu_check.png)
target_arch=mipsel **Optional. **Use this if building for MIPS targets.
![image](menu_check.png)
