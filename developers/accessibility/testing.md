# Accessibility Testing

There are multiple places in the code where accessibility can be tested. This
page documents some of the most widely-used systems.

### DumpAccessibilityTree

The preferred system for testing end-to-end accessibility functionality is
[DumpAccessibilityTree](http://code.google.com/searchframe#OAMlx_jo-ck/src/content/browser/accessibility/dump_accessibility_tree_browsertest.cc).

It's loosely patterned after DumpRenderTree and Blink/WebKit layout tests, so if
you've ever written a layout test, you already understand the idea behind an
accessibility test!

To run the DumpAccessibilityTree tests, build **content_browsertests** and then
run it with --gtest_filter="DumpAccessibilityTree\*", like this:

> ninja -C out/Debug content_browsertests

> out/Debug/content_browsertests --gtest_filter="DumpAccessibilityTree\*"

DumpAccessibilityTree tests allow you to test for different output on different
platforms. On each machine you run the tests on, it will output the
cross-platform tree, and if you're on a platform with native accessibility
support, the accessibility tree for that platform. It isn't that hard to write a
DumpAccessibilityTree test for a platform other than your primary platform using
the trybots.

The code for DumpAccessibilityTree is found in this main source file, plus many
helper classes in the same directory:

content/browser/accessibility/dump_accessibility_tree_browsertest.cc

Each individual test has a set of data files: an html files, and expectation
files for each platform, e.g.:

content/test/data/accessibility/footer.html

content/test/data/accessibility/footer-expected-mac.txt

content/test/data/accessibility/footer-expected-win.txt

content/test/data/accessibility/footer-expected-win.txt

content/test/data/accessibility/footer-expected-win.txt

content/test/data/accessibility/footer-expected-win.txt

For each test, DumpAccessibilityTree loads the html file into a browser window,
recursively explores the whole accessibility tree, then dumps the result in a
text format (subject to some filters that you can customize, so you don't dump
every attribute), and compares the result to the expectation file.

For more information on the format of the files, see:

### content/test/data/accessibility/readme.txt

### Blink Layout Tests

Another really important aspect of testing Chromium accessibility is Blink
layout tests. There are a number of accessibility tests in this directory:

third_party/WebKit/LayoutTests/accessibility/

These tests are critically important for testing Blink implementation of
accessibility at a low-level, including parsing html and sending notifications.

To run them, here's a sample command line:

> ninja -C out/Release blink_tests

> third_party/WebKit/Tools/Scripts/run-webkit-tests --no-show-results
--no-retry-failures --results-directory=results accessibility/

To run just one test:

> out/Release/content_shell --run-layout-test
third_party/WebKit/LayoutTests/accessibility/test_name.html

To run just one test, on Mac OS X:

> out/Release/Content\\ Shell.app/Contents/MacOS/Content\\ Shell
--run-layout-test third_party/WebKit/LayoutTests/accessibility/test_name.html
