# Google Plus communities related to Chrome OS and Chromium OS

You might also find these this G+ communities interesting:

*   [Chromium OS](https://plus.google.com/communities/118058839614744781012) -
    Community of hackers, testers, developers, or those using other builds of
    Chromium OS.
*   [Chrome OS](https://plus.google.com/communities/107021066849116516708) -
    chrome, chromebook, chromebooks, Samsung, Acer, android, google, Pixel, HP
    Pavilion, Lenovo, chromebox, chrome releases, chromecast
*   [Chromebook
    Users](https://plus.google.com/communities/112113270537114386848) - Tips and
    Hints from REAL users
*   [Crouton Users](https://plus.google.com/communities/109120069102230291151) -
    crouton: Chromium OS Ubuntu Chroot Environment
