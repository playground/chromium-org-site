# Running Telemetry on Chrome OS

### Setup

To run a Telemetry test on Chrome OS, you will need a Linux, Mac OS X, or
Windows host machine. Follow these steps to get set up. When setting up your DUT
(Device Under Test) on a network, please pay attention to the **Security
Issues** below.

1.  You must have a test image installed on your Chrome OS device.

    *   [Determine your device code
        name.](http://www.chromium.org/chromium-os/developer-information-for-chrome-os-devices)
    *   [Download a test image for that code
        name.](http://chromeos-images.corp.google.com/)
    *   Unzip the download file and copy `chromiumos_test_image.bin` to a USB
        stick. Be sure to replace `/dev/sdc` with the actual device of your USB
        stick (can be determined by running `lsblk`).

            $ sudo dd if=path/to/chromiumos_test_image.bin of=**/dev/sdc** bs=4M oflag=sync

    *   [Enable dev mode and boot from USB on your
        Chromebook.](http://www.chromium.org/chromium-os/developer-information-for-chrome-os-devices)
    *   Insert the USB stick into the device, reboot, and press Ctrl+U to boot
        from the USB.
    *   After booting, press Ctrl+Alt+F2 to enter a command prompt and run
        `/usr/sbin/chromeos-install`.

2.  Connect your Chromebook and your host machine to the same network. *Please
    read the ****Security Issues* ***section below.*

3.  Get the IP address of your Chromebook.

    *   Ctrl+Alt+F2 will get you to a bash prompt
    *   Sign in as chronos
    *   Enter command `sudo su`
    *   Enter command `ifconfig`
    *   You want the address for **inet:** in the **en0** section

4.  Set up a password-less ssh to your test machine.

        $ cp src/third_party/chromite/ssh_keys/testing_rsa ~/.ssh/$ cp src/third_party/chromite/ssh_keys/testing_rsa.pub ~/.ssh/

5.  Add the following lines to `~/.ssh/config`:

        Host <chromebook ip address>CheckHostIP noStrictHostKeyChecking noIdentityFile %d/.ssh/testing_rsaProtocol 2

6.  Use the following options when running Telemetry: `--browser=cros-chrome`
    and `--remote=<ip address>`

### Chrome OS (from ChromeOS src, using test_that)

To run a Telemetry server side test from ChromeOS src without setting up an AFE
server or devserver.

1.  You must have a test
    [setup](../../chromium-os/testing/test-code-labs/server-side-test/index.md)
    to run autotest.

2.  Change the server side test .py file as below. args={} is the extra argument
    which needs to be added.

        def run_open(self, host=None, test=None, args={}):local = args.get("local") == "True" 

3.  Change the control file as below.

        from autotest_lib.client.common_lib import utilsjot.run_test('telemetry_autotest', host=host, test='telemetry_test', args=utils.args_to_dict(args))

4.  Launch ChromeOS chroot pointing to Chrome source:

        $ cros_sdk  --chrome_root=<chrome_src_path>   # Full path before the src/ e.g. /local/storage/Chrome/src. The Chrome path should be /local/storage/Chrome.

5.  Run the server side autotest:

        $ test_that --args='local=True' <chromebook ip> telemetry_autotest

#### Security Issues When Running Telemetry Remotely

When run with the --remote option, Telemetry sets up a reverse ssh tunnel (ssh
-R) from your DUT to your workstation, for the purpose of serving web pages from
the Web Replay Server running on your workstation. This tunnel lets a malicious
program on your DUT, or other devices on the same network, execute arbitrary
commands on your workstation. Therefore, running Telemetry remotely in a secure
fashion requires:

1.  that the DUT be connected directly to your workstation on a private network
    with no access to the internet;
2.  that the DUT contain a fresh image that has never been exposed to the
    internet;
3.  that the private network includes no other devices (except other DUTs in the
    same state).
