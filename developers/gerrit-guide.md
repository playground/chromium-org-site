# Gerrit Guide

## Introduction

**(EVERYONE) To get access to the Chromium gerrit instance:**

1.  Go to <https://chromium-review.googlesource.com/new-password>
2.  Log in with the email you use for your git commits.
    1.  **If you are a Googler, use your @chromium.org account.**
    2.  **You can verify this by ensuring that the Username field looks like
        git-<user>.chromium.org**
3.  Follow the directions on the new-password page to set up/append to your
    .gitcookies file.
    1.  You should click the radio button labeled "only
        chromium.googlesource.com" if it exists.
4.  **Verification: **Run `git ls-remote
    https://chromium.googlesource.com/a/chromiumos/manifest.git`
    1.  This should **not **prompt for any credentials, and should just print
        out a list of git references.
5.  Run \`cd src && git config --local gerrit.host true\` to default to
    uploading your reviews to Gerrit
6.  Make sure to set your real name.
    1.  Visit <https://chromium-review.googlesource.com/#/settings/> and check
        the "Full Name" field.
    2.  If it isn't set, you will need to update your [Google+
        profile](https://plus.google.com).
    3.  Once your Google+ profile is up-to-date, you might have to logout/login
        in Gerrit.

#### (Googler) Link @chromium.org & @google.com accounts

We use memberships of @google.com identities in particular groups to control
access to Chromium repositories on chromium-review and chrome-internal-review
sites. Since you will be primarily using @chromium.org account, you need to tell
Gerrit that your @chromium.org and @google.com accounts are linked together.
Follow the steps:

1.  Login into <https://chromium-review.googlesource.com> using your
    @chromium.org account.
2.  Go to [Settings -> Contact
    Information](https://chromium-review.googlesource.com/#/settings/contact).
3.  Click "Register new email...", enter your @google.com account and follow the
    instructions.
4.  To verify that it worked, open [Settings ->
    Identities](https://chromium-review.googlesource.com/#/settings/web-identities)
    and verify your @chromium.org, @google.com and ldapuser/\* identities are
    listed.
5.  Repeat 1-4 on <https://chrome-internal-review.googlesource.com>, but use
    your @google.com email to login, and @chromium.org in "Register new email"
    dialog.
6.  If you see any errors during this process, file [Infra-Git
    ticket](https://code.google.com/p/chromium/issues/entry?template=Infra-Git)
    with the subject "Link my <id>@chromium.org and <id>@google.com accounts".
    If it is urgent, add jparent@chromium.org to CC on the ticket. Otherwise,
    the request should be handled within 2-3 days.

Once your accounts are linked, you'll be able to use both @chromium.org and
@google.com emails in git commits. It is particularly useful if you have your
@chromium.org email in global git config, and you try to trigger chrome-internal
trybots (that otherwise require @google.com email).

## Still having a problem?

Check out the [Gerrit
Documentation](https://gerrit-review.googlesource.com/Documentation/index.html)
to see if there are hints in there.

If you have any problems please [open a Build Infrastructure
issue](http://code.google.com/p/chromium/issues/entry?template=Build+Infrastructure)
on the **Chromium** issue tracker (the "Build Infrastructure" template should be
automatically selected).

For additional information, you can also visit the [PolyGerrit + Chromium
FAQ](https://polygerrit.appspot.com/).
