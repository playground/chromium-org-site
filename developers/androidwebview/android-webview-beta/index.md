# Try out WebView Beta

**How to join:**

*   On Android M or below? [Join the beta on Google
    Play](https://play.google.com/apps/testing/com.google.android.webview).
*   On Android N or above? Change your WebView implementation under developer
    options in Android Settings.
    *   To add new WebView providers you'll need the corresponding
        [stable](https://play.google.com/store/apps/details?id=com.android.chrome),
        [beta](https://play.google.com/store/apps/details?id=com.chrome.beta),
        [dev](https://play.google.com/store/apps/details?id=com.chrome.dev) or
        [canary](https://play.google.com/store/apps/details?id=com.chrome.canary)
        channel of Chrome. Once installed, you can select them as a WebView
        provider. To change WebView Provider, first enable Android developers
        options and then change the WebView Implementation
        *   Open Settings> About
        *   Tap on “Build number” seven times
        *   Go back to Settings menu to see “Developer options”
        *   Choose Developer options > WebView implementation (see figure)

            ![image](screen.png)

    *   See more on the relationship between WebView and Chrome in the FAQ
        below.

### Preview the latest features

WebView Beta lets you try out the newest WebView features as soon as they're
available.

### Give early feedback

Let us know what you think and help improve WebView for everyone on Android.
Please file bugs at <https://goo.gl/9qkbdn>.

### Chat with the developers and community

You can get ahold of the WebView development team by posting on our [public G+
community](https://plus.sandbox.google.com/communities/105434725573080290360) or
e-mailing android-webview-dev@chromium.org.

### Stay up to date

You'll always get the latest performance improvements and features.

### WebView FAQ

What is WebView?

WebView is a system component of Android which enables the apps you use every
day to show content from the web. Most apps you use every day use WebView in
some way.

Why do I need to update WebView?

WebView needs regular security updates just like your browser. We release a new
version every 6 weeks to make sure you stay safe while using apps on your phone.

What’s the relationship between WebView and Chrome?

WebView is also built on top of the open source Chromium project, but it doesn’t
share any data with Google Chrome.

Starting with Android N WebView is built into Chrome. Because they share so much
underlying code this saves space and memory on your device. They still don’t
share any data, however, and you can disable Google Chrome at any time without
impairing your device. When Chrome is disabled WebView will switch to a
standalone version which isn't combined with Chrome.

Are Chrome features like Sync or Data Saver available in WebView on Android N?

No. Although WebView and Chrome share a package starting with Android N, they
don’t share data, and Chrome-specific features like Sync and Data-Saver aren’t
available inside of WebView.

What happens if I disable WebView?

We don’t recommend disabling WebView on your device. Although disabling the
WebView package will stop updates, apps will still be able to load the WebView
included in the system image of your device. That version of WebView is likely
to be out of date, which exposes your phone to security vulnerabilities.
