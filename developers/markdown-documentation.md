# Markdown Documentation guide

New documentation related to code structure should be put in Markdown. The best
practices recommendation is to put a README.md file in the code directory
closest to your component or code.

Use
[src/tools/md_browser](https://chromium.googlesource.com/chromium/src/+/master/tools/md_browser)
to preview local changes to Markdown files.

Alternatively, there are Chrome extensions such as the the [Markdown
Viewer](https://chrome.google.com/webstore/detail/markdown-viewer/ehnambpmkdhopilaccgfmojilolcglhn)
extension and [Markdown editor
app](https://chrome.google.com/webstore/detail/minimalist-markdown-edito/pghodfjepegmciihfhdipmimghiakcjf).

Please write your Markdown in accordance with the [style
guide](https://github.com/google/styleguide/tree/gh-pages/docguide).

After committing the patch, you can view it at a URL like
https://chromium.googlesource.com/chromium/src/+/master/<my path>/README.md
([example](https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit/Source/core/paint/README.md)).

[Here are some more
examples](https://cs.chromium.org/search/?q=file:readme.md+-file:/third_party/&type=cs)
to learn from.
