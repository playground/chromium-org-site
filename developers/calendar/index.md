# Chromium Development Calendar and Release Info

## Branch Points:

**2017**

** Release**** Week of Branch Point (End of Day Thursday)**56 Nov 17th, 201657
Jan 19th, 201758 Mar 2nd, 201759 Apr 13th, 201760 May 25th, 201761 Jul 20th,
2017
62 Aug 31st, 201763 Oct 12th, 2017

## Estimated\* Stable Dates

\*These dates are subject to change without advance notice and provided here
only for rough planning purposes. New bugs, security incidents, holiday
schedules, partner dependencies and process changes can all affect these dates
and move it in either direction. The date only estimates the week of 1st stable
push of a release – it does not imply that all end points will be updated by
this week.

**Release** ** Estimated Week of Stable** 49 Mar 8th, 2016 50 Apr 19th, 2016 51
May 31st, 2016 52 Jul 26th, 2016 53 Sep 6th, 2016 54 Oct 18th, 2016 55 Dec 6th,
2016 56 Jan 31st, 2017

**2017**

** Release **** Estimated Week of Stable**56 Jan 31st, 201757 Mar 14th, 201758
Apr 25th, 201759 Jun 6th, 2017 (May 31st for Desktop)60 Aug 1st, 2017 (July 26th
for Desktop)61 Sep 12th, 2017 (Sep 6th for Desktop)
62 Oct 24th, 2017 (Oct 18th for Desktop)63 Dec 12th, 2017 (Dec 6th for Desktop)

Please note that Linux, Mac, and Windows Chrome is release one week earlier.

## Current Release Information

## Previous Release Information

*   To check from which revision a certain build is released, changelogs, etc:
    <https://omahaproxy.appspot.com>

### **Previous Branch Points**

**2016**

** Release**** Week of Branch Point (End of Day Thursday)**49 Jan 15th, 201650
Feb 26th, 201651 Apr 8th, 201652 May 19th, 201653 Jun 30th, 201654 Aug 25th,
2016
55 Oct 6th, 201656 Nov 17th, 2016

**2015**

** Release** ** Week of Branch Point (End of Day Friday)** 41 Jan 9th, 2015 42
Feb 20th, 2015 43 Apr 3rd, 2015 44 May 15th, 2015 45 Jul 10th, 2015 46 Aug 21st,
2015
47 Oct 2nd, 2015 48 Nov 13th, 2015

**2014**

** Release** ** Week of Branch Point** 33 Dec 16th, 2013 34 Feb 17th, 2014 35
Mar 31st, 2014 36 May 9th, 2014 37 Jun 20th, 2014 38 Aug 15th, 2014 39 Sep 26th,
2014
40 Nov 7th, 2014

**2013**

** Release** ** Week of Branch Point (Monday of the week)** 26 Feb 11th, 2013 27
Mar 25th, 2013 28 May 6th, 2013 29 Jun 24th, 2013 30 Aug 12th, 2013 31 Sept
23rd, 2013 32 Nov 4th, 2013

**2012**

** Release** ** Week of Branch Point (Monday of the week)** 17 Dec 5th, 2011 18
Jan 30th, 2012 19 Mar 26th, 2012 20 May 7th, 2012 21 Jun 18th, 2012 22 Aug 6th,
2012 23 Sept 17th, 2012 24 Oct 29th, 2012 25 Dec 17th, 2012
