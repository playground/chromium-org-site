# mus+ash

mus+ash (pronounced "mustash") is a project to separate the window management
and shell functionality of ash from the chrome browser process. The benefit is
performance isolation for system components outside of the browser, and a
sharper line between what components are considered part of the ChromeOS UX and
what are part of the browser. mus+ash is built on top of the external Mojo shell
built from src/mojo/runner and the Mandoline UI Service ("Mus").

**Links**

*   [Intent to
    Implement](https://groups.google.com/a/chromium.org/d/msg/chromium-dev/stof4wmbEDg/bhvWa-PrFQAJ)
    on chromium-dev with high level tactical details
*   [Bugs](https://code.google.com/p/chromium/issues/list?can=2&q=mustash)
*   Googlers: See go/mustash-intro and go/mustash-ui

**Build Instructions**

mus+ash builds only for Chrome OS (either for device or on Linux with
target_os="chromeos"). It requires aura and toolkit_views.

*Build the basic mash shell:*

ninja -C out/foo mash:all

*Build the basic mash shell and Chrome:*

ninja -C out/foo mash:all chrome

*Run*

out/foo/mash --service=mash_session

You can launch apps (eg. views_examples and task_viewer) from the QuickLaunch
window.

By default, each service will run in its own process, including chrome and all
of chrome's renderers.

For debugging you can pass --wait-for-debugger which will show an alert box on
Windows before starting each new process. The message box will show the name of
the service.
