# Password Form Styles that Chromium Understands

### Automatically Comprehensible Password Forms

You can help ensure that browsers' and extensions' password management
functionality can understand your site's sign-up, sign-in and change-password
forms by enriching your HTML with a dash of metadata. In particular:

1.  Add an
    `[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)`
    attribute with a value of `username` for usernames.
2.  If you've implemented an "[email
    first](https://developers.google.com/identity/toolkit/web/account-chooser#email_first)"
    sign-in flow that separates the username and password into two separate
    forms, include a form field containing the username in the form used to
    collect the password. You can, of course, hide this field via CSS if that's
    appropriate for your layout.
3.  Add an
    `[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)`
    attribute with a value of `current-password` for the password field on a
    sign-in form.
4.  Add an
    `[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)`
    attribute with a value of `new-password` for the password field on sign-up
    and change-password forms.
5.  If you require the user to type their password twice during sign-up or
    password update, add the `new-password`
    `[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)`
    attribute on both fields.

#### Sign-in Form:

<form id="login" action="login.php" method="post"> <input type="text"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="username"**>
<input type="password"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="current-password"**>
<input type="submit" value="Sign In!"> </form>

#### Email First Sign-in Flow:

Collect the email: <form id="login" action="login.php" method="post"> <input
type="text"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="username"**>
<input type="submit" value="Sign In!"> </form> Then collect the password, but
include the email as the value of a hidden form field: <style> #emailfield {
display: none; } </style> <form id="login" action="login.php" method="post">
<input id="emailfield" type="text" **value="me@example.test"**
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="username"**>
<input type="password"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="current-password"**>
<input type="submit" value="Sign In!"> </form>

#### Sign-up Form:

<form id="login" action="signup.php" method="post"> <input type="text"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="username"**>
<input type="password"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="new-password"**>
<input type="submit" value="Sign Up!"> </form> Or: <form id="login"
action="signup.php" method="post"> <input type="text"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="username"**>
<input type="password"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="new-password"**>
<input type="password"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="new-password"**>
<input type="submit" value="Sign Up!"> </form>

### Related advice

There are also useful [autocomplete attribute values for annotating address
forms](https://developers.google.com/web/updates/2015/06/checkout-faster-with-autofill).
Note that you can combine multiple autocomplete values in one attribute
separated with a space:

<input type="text"
**[autocomplete](https://html.spec.whatwg.org/multipage/forms.html#autofilling-form-controls:-the-autocomplete-attribute)="username
email"**>
