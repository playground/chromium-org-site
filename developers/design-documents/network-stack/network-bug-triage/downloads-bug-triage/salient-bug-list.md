# Salient Bug List

For reference, some bugs that may be useful to have an easy list for (usually
for duping). Feel free to edit this list as you feel moved.

FTP directory listing fails with pt-br locale (Bug reports may consist of a
single line description alluding to a FTP
error)[177428](http://crbug.com/177428) Auto-execution of JNLP files don't
work[92846](http://crbug.com/92846) Open With[333](http://crbug.com/333)
Downloads Interrupted by Sleep[110497](http://crbug.com/110497) Can't download
PDF as binary/octet-stream[104331](http://crbug.com/104331) Downloading a URL
already downloading but paused hangs[100529](http://crbug.com/100529) Make MHTML
a Save Page As ... format[120416](http://crbug.com/120416) Downloads
resumption/resume interrupted downloads[7648](http://crbug.com/7648) Make pdf a
Save Page As ... format[116749](http://crbug.com/116749) Make a whole lot of
things a Save Page As .. format[113888](http://crbug.com/113888) Mac
circumvention of downloads warning dialog incorrect for last incognito window
close[88419](http://crbug.com/88419) Downloads fail with 'Insufficient
Permissions'
error[161793](https://code.google.com/p/chromium/issues/detail?id=161793)
