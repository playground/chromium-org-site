# Network bug triage

## Triage Process

The goal of the Network Triage process is to review incoming networking bugs and
get them in front of relevant developers as quickly and efficiently as possible.

*   [Overview of the triage
    responsibilities](https://chromium.googlesource.com/chromium/src/+/master/net/docs/bug-triage.md)
*   [Suggested bug triage
    workflow](https://chromium.googlesource.com/chromium/src/+/master/net/docs/bug-triage-suggested-workflow.md)
*   [Description of many of the labels
    applied](https://chromium.googlesource.com/chromium/src/+/master/net/docs/bug-triage-labels.md)

## Rotation schedule

Each person does front-line network bug triage for two days. People with other
bug triage duties are exempt.

Adding the following calendar will allow you to see the schedule:
google.com_52n2p39ad82hah9v7j26vek830@group.calendar.google.com
