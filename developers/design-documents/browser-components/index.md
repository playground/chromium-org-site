# Browser Components

You probably intended to find one of these pages:

[Browser Components / Layered Components Cookbook](../cookbook/index.md)

[Life of a Browser Component.pdf](../cookbook/Life-of-a-Browser-Component.pdf)

[Layered Components: Design](../layered-components-design.md)

[Layered Components: Technical
Approach](../layered-components-technical-approach/index.md)

[Structure of Layered Components Within the Chromium
Codebase](../structure-of-layered-components-within-the-chromium-codebase.md)
