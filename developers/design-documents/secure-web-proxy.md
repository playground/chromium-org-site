# Secure Web Proxy

## Introduction

A secure web proxy is a web proxy that the browser communicates with via SSL, as
opposed to clear text. In insecure public networks, such as airports or cafes,
browsing over HTTP may leave the user vulnerable to cookie stealing, session
hijacking or worse. A secure web proxy can add a significant layer of defense in
these cases.

## **Using a Secure Web Proxy with Chrome**

To make use of a secure web proxy, Chrome needs to be configured to use a [proxy
auto-config](http://en.wikipedia.org/wiki/Proxy_auto-config) file which specify
the `HTTPS` proxy type. For example:

     function FindProxyForURL(url, host) { return "HTTPS secure-proxy.example.com:443"; }

This pac file can be specified by starting Chrome with the `--proxy-pac-url=...`
command line argument, or through the settings dialog. Please be aware that
other browser do not support the `HTTPS` proxy type in a .pac file, so modifying
the system-wide proxy configuration to use such a .pac file might be
inadvisable.

Alternatively, a secure web proxy can be specified by using the
`--proxy-server=https://<proxy>:<port>` command line argument. For example:

     chrome --proxy-server=https://secure-proxy.example.com:443

Since the communication between Chrome and the proxy uses SSL, next protocol
negotiation will be used. If the servers supports SPDY, then the proxy will act
as a [SPDY Proxy](../../spdy/spdy-proxy.md).

## **Running a Secure Web Proxy**

While all the details of running a secure web proxy are out of scope for this
document, here are two suggestions. If you are already running a web proxy, you
use [stunnel](http://www.stunnel.org/) to convert it into a secure web proxy.
For example:

    stunnel -f -d 443 -r localhost:8080 -p cert.pem 

**This would cause stunnel to listen for SSL connections on port 443 and send
any HTTP requests to the web proxy running on port 8080.**

**Alternatively, the popular proxy program Squid appears to offer support for
running as a secure web proxy via the [https_port
directive](http://www.squid-cache.org/Doc/config/https_port/).**
