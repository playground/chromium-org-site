# IDN in Google Chrome

### Background

Many years ago, domains could only consist of the Latin letters A to Z, digits,
and a few other characters. [Internationalized Domain Names
(IDNs)](http://en.wikipedia.org/wiki/Internationalized_domain_name) were created
to better support non-Latin alphabets for web users around the globe.

Different characters from different (or even the same!) languages can look very
similar. We’ve seen
[reports](https://bugs.chromium.org/p/chromium/issues/detail?id=683314) of
proof-of-concept attacks. These are called [homograph
attacks](https://en.wikipedia.org/wiki/IDN_homograph_attack). For example, the
Latin "a" looks a lot like the Cyrillic "а", so someone could register
<http://ebаy.com> (using Cyrillic "а"), which could be confused for
<http://ebay.com>. This is a limitation of how URLs are displayed in browsers in
general, not a specific bug in Chrome.
In a perfect world, domain registrars would not allow these confusable domain
names to be registered. Some TLD registrars do exactly that, mostly by
restricting the characters allowed, but many do not. As a result, all browsers
try to protect against homograph attacks by displaying
[punycode](http://en.wikipedia.org/wiki/Punycode) (looks like "xn-- ...")
instead of the original IDN, according to an IDN policy.

This is a challenging problem space. Chrome has a global user base of billions
of people around the world, many of whom are not viewing URLs with Latin
letters. We want to prevent confusion, while ensuring that users across
languages have a great experience in Chrome. Displaying either punycode or a
visible security warning on too wide of a set of URLs would hurt web usability
for people around the world.
Chrome and other browsers try to balance these needs by implementing IDN
policies in a way that allows IDN to be shown for valid domains, but protects
against confusable homograph attacks.

[Google Safe Browsing](https://safebrowsing.google.com/) continues to help
protect over two billion devices every day by showing warnings to users when
they attempt to navigate to dangerous or deceptive sites or download dangerous
files. Password managers (like [Google Smart
Lock](https://get.google.com/smartlock/)) continue to remember which domain
password logins are for, and won’t automatically fill a password into a domain
that is not the exactly correct one.

*How IDN works*

IDNs were devised to support arbitrary Unicode characters in hostnames in a
backward-compatible way. This works by having user agents transform a hostname
containing Unicode characters beyond ASCII to one fitting the traditional mold,
which can then be sent on to DNS servers. For example, <http://öbb.at> is
transformed to <http://xn--bb-eka.at>. The transformed form is called ASCII
Compatible Encoding (ACE) made up of the four character prefix ( xn-- ) and the
[punycode](http://en.wikipedia.org/wiki/Punycode) representation of Unicode
characters.

*April 2017 update*

Specific instances of IDN homograph attacks have been reported to Chrome, and we
continually update our IDN policy to prevent against these attacks. One
[specific instance](https://www.xudongz.com/blog/2017/idn-phishing/) of this
general issue was reported to Chrome security on Jan 20. It was marked a
medium-severity bug. A fix landed on March 23. The researcher whose report led
to the fix was awarded $2,000 under [Chrome's Vulnerability Reward
Program](https://g.co/ChromeBugRewards). That fix will be released in Chrome 58,
which has a stable release around the [end of
April](https://www.chromium.org/developers/calendar).

This fix is an attempt to balance the needs of our international userbase while
protecting against confusable homograph attacks.. The fix uses punycode for
domain names that are made entirely of Latin lookalike Cyrillic letters when the
[top-level domain](https://en.wikipedia.org/wiki/Top-level_domain) is not an
internationalized domain name, meaning that the check only applies to top-level
domains like "com", "net", and "uk". We’re working on additional fixes, for
example, for confusables within one script set -- “l” (lowercase L) could be
confused with “I” ([small dotless i
character](http://www.fileformat.info/info/unicode/char/0131/index.htm)). We
will keep this article updated with our current IDN policy below.

### Google Chrome's IDN policy

Starting with Google Chrome 51, whether or not to show hostnames in Unicode is
determined **independently of the language settings (the Accept-Language list).
**Its algorithm is similar to [what Firefox
does](https://wiki.mozilla.org/IDN_Display_Algorithm#Algorithm). ( [the
changelist description that implemented the new
policy](https://chromium.googlesource.com/chromium/src/+/62a928390ba06db29576bbb32606696b3e16a66c).)

Google Chrome decides if it should show Unicode or punycode for each domain
label (component) of a hostname separately. To decide if a component should be
shown in Unicode, Google Chrome uses the following algorithm:

*   Convert each component stored in the ACE to Unicode per [UTS 46 transitional
    processing](http://unicode.org/reports/tr46/#Processing) (*ToUnicode*).
*   If there is an error in *ToUnicode *conversion (e.g. contains [disallowed
    characters](http://unicode.org/cldr/utility/list-unicodeset.jsp?a=%5B%3Auts46%3Ddisallowed%3A%5D&abb=on&g=&i=),
    [starts with a combining
    mark](http://icu-project.org/apiref/icu4c/uidna_8h.html#a0411cd49bb5b71852cecd93bcbf0ca2da390a6b3d9844a1dcc1f99fb1ae478ecf),
    or [violates BiDi
    rules](http://icu-project.org/apiref/icu4c/uidna_8h.html#a0411cd49bb5b71852cecd93bcbf0ca2da8a9311811fb0f3db1644ac1a88056370)),
    punycode is displayed.
*   If any character is outside the union of the following sets, punycode is
    displayed.
    *   [Characters allowed in identifiers
        ](http://unicode.org/cldr/utility/list-unicodeset.jsp?a=%5B%3AIdentifierStatus%3DAllowed%3A&abb=on&g=&i=)per
        [Unicode Technical Standard 39 (UTS
        39)](http://www.unicode.org/reports/tr39/#Identifier_Status_and_Type)
    *   [Characters suitable for identifiers in 5 Aspirational
        scripts](http://unicode.org/cldr/utility/list-unicodeset.jsp?a=%5B%3AIdentifier_Type%3DAspirational%3A%5D&abb=on&g=&i=)
        per [Unicode Standard Annex 31 (UAX
        31)](http://www.unicode.org/reports/tr31/#Aspirational_Use_Scripts)
*   If the component contains either U+0338 or U+2027, punycode is displayed.
*   If the component uses characters drawn from multiple scripts, it is subject
    to a script mixing check based on ["Moderately Restrictive" profile of UTS
    39](http://www.unicode.org/reports/tr39/#Restriction_Level_Detection) with
    an additional restriction on Latin. Failing the check, the component is
    shown in punycode.
    *   Latin, Cyrillic or Greek characters cannot be mixed with each other
    *   Latin characters in the ASCII range can be mixed with characters in
        another script as long as it's not Greek nor Cyrillic.
    *   Han (CJK Ideographs) can be mixed with Bopomofo
    *   Han can be mixed with Hiragana and Katakana
    *   Han can be mixed with Korean Hangul
*   If two or more numbering systems (e.g. European digits + Bengali digits) are
    mixed, punycode is shown.
*   If there are any invisible characters (e.g. a sequence of the same combining
    mark or a sequence of Kana combining marks), punycode is shown.
*   Test the label for [*mixed script confusable *per UTS
    39](http://unicode.org/reports/tr39/#Mixed_Script_Confusables). If *mixed
    script confusable* is detected, show punycode.
*   If a hostname belongs to an non-IDN TLD(top-level-domain) such as 'com',
    'net', or 'uk' and all the letters in a given label belong to [a set of
    Cyrillic letters that look like Latin letters
    ](http://unicode.org/cldr/utility/list-unicodeset.jsp?a=%5B%D0%B0%D1%81%D4%81%D0%B5%D2%BB%D1%96%D1%98%D3%8F%D0%BE%D1%80%D4%9B%D1%95%D4%9D%D1%85%D1%83%D1%8A%D0%AC%D2%BD%D0%BF%D0%B3%D1%B5%D1%A1%5D&g=gc&i=)(e.g.
    [Cyrillic Small Letter
    IE](http://unicode.org/cldr/utility/character.jsp?a=0435) - е ), show
    punycode.
*   If the label matches a *[dangerous
    pattern](https://cs.chromium.org/chromium/src/components/url_formatter/url_formatter.cc?rcl=0&l=393),
    *punycode is shown.
*   Otherwise, Unicode is shown.

(This is implemented by
[IDNToUnicodeOneComponent](https://cs.chromium.org/chromium/src/components/url_formatter/url_formatter.cc?l=520&gs=cpp%253Aurl_formatter%253A%253A%253Canonymous-namespace%253E%253A%253AIDNToUnicodeOneComponent(const%2Bunsigned%2Bshort%2B*%252C%2Bunsigned%2Blong%252C%2Bstd%253A%253Abasic_string%253Cbase%253A%253Achar16%252C%2Bbase%253A%253Astring16_char_traits%253E%2B*)%2540chromium%252F..%252F..%252Fcomponents%252Furl_formatter%252Furl_formatter.cc%257Cdef&gsn=IDNToUnicodeOneComponent&ct=xref_usages)
and IsIDNComponentSafe() in
[components/url_formatter/url_formatter.cc](https://cs.chromium.org/chromium/src/components/url_formatter/url_formatter.cc))

#### Consequences / Examples

\[The old content here was completely inaccurate and has been removed. TODO: add
examples of the above\]

### Behavior of other browsers

#### IE

IE displays URLs in IDN form if every component contains only characters of one
of the languages configured in "Languages" on the "General" tab of "Internet
Options", similar to what Google Chrome does.

<http://msdn.microsoft.com/en-us/library/bb250505(VS.85).aspx>

#### Firefox

Firefox uses a script mixing detection algorithm based on the ["Moderately
Restrictive" profile of Unicode Technical Report
39](http://www.unicode.org/reports/tr39/#Restriction_Level_Detection). Domains
of any single script, any single script + Latin, or a small whitelist of other
combinations are displayed as Unicode; everything else is Punycode.

<https://wiki.mozilla.org/IDN_Display_Algorithm>

#### Opera

Like Firefox, Opera has a whitelist of TLDs and shows IDN only for these
whitelisted TLDs.

<http://www.opera.com/support/kb/view/788/>

#### Safari

Safari has a whitelist of scripts that do not contain confusable characters, and
only shows the IDN form for whitelisted scripts. The whitelist does not include
Cyrillic and Greek (they are confusable with Latin characters), so Safari will
always show punycode for Russian and Greek URLs.

<http://support.apple.com/kb/TA22996?viewlocale=en_US>
