# Web Bluetooth through Bluetooth Android class diagram for getCharacteristic, getPrimaryService

Diagram of some classes used in Web Bluetooth implementation down through the
Android implementation of Bluetooth.

Source file (attached) can be edited with [dia](http://dia-installer.de/)
program. Authored by tommyt@opera.com, updated by scheib@chromium.org.

![image](WebBluetoothThroughBluetoothAndroid.svg)
