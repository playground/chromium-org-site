# Site Isolation

## Motivation

Chrome's [multi-process architecture](../multi-process-architecture/index.md)
provides many benefits for speed, stability, and security. It allows web pages
in unrelated tabs to run in parallel, and it allows users to continue using the
browser and other tabs when a renderer process crashes. Because the renderer
processes don't require direct access to disk, network, or devices, Chrome can
also run them inside a restricted [sandbox](../sandbox/index.md). This [limits
the damage](http://crypto.stanford.edu/websec/chromium/) that an attacker can
cause if he exploits a vulnerability in the renderer, including making it
difficult for the attacker to access the user's filesystem or devices, as well
as privileged pages (e.g., settings or extensions) and pages in other profiles
(e.g., Incognito mode).

![image](ChromeSiteIsolationProject-arch.png)

However, there is still a large opportunity to use Chrome's sandbox for greater
security benefits: isolating web sites from each other. Chrome currently makes
an effort to place pages from different web sites in different renderer
processes when possible, but due to compatibility constraints, there are many
cases in which pages from different sites share a process (e.g., cross-site
iframes). In these cases, we rely on the renderer process to enforce the Same
Origin Policy and keep web sites isolated from each other.

This page describes our "site isolation" efforts to improve Chrome to use
renderer processes as a security boundary between web sites, even in the
presence of Blink vulnerabilities. Our goal is to add support for a
"**site-per-process**" policy that ensures certain renderer processes contains
pages from at most one web site. The browser process can then give renderer
processes limited access to cookies and other resources, based on which web
sites require dedicated processes.

## **Threat Model**

For the "site-per-process" security policy, we assume that an attacker can
convince the user to visit a page that exploits a vulnerability in the renderer
process, allowing the attacker to run arbitrary code within the sandbox. We
consider attackers that want to steal information or abuse privileges granted to
other web sites.

Here, we use a precise definition for a **site** that we use as a principal: a
page's site includes the scheme and registered domain name, including the public
suffix, but ignoring subdomains, port, or path. We use sites instead of origins
to avoid breaking compatibility with existing web pages that might modify their
document.domain to communicate across subdomains.

We consider the following threats in scope for the proposed policy:

*   **Stealing cross-site cookies and HTML5 stored data.** We can prevent a
    renderer process from receiving cookies or stored data from sites other than
    its own.
*   **Stealing cross-site HTML, XML, and JSON documents.** Using MIME type and
    content sniffing, we can prevent a renderer process from loading cross-site
    documents. We cannot block all cross-site resources, however, because
    images, scripts, and other opaque files are permitted across sites.
*   **Stealing saved passwords.** We can prevent a renderer process from
    receiving saved passwords from sites other than its own.
*   **Abusing permissions granted to another site.** We can prevent a renderer
    process from using permissions such as geolocation that the user has granted
    to other sites.
*   **Compromising X-Frame-Options.** We can prevent a renderer process from
    loading cross-site pages in iframes. This allows the browser process to
    decide if a given site can be loaded in an iframe or not based on
    X-Frame-Options headers.
*   **Accessing cross-site DOM elements via UXSS bugs.** An attacker exploiting
    a universal cross-site scripting bug in the renderer process will not be
    able to access DOM elements of cross-site pages, which will not live in the
    same renderer process.

We do not expect this policy to mitigate traditional cross-site attacks or
attacks that occur within the page of a victim site, such as XSS, CSRF, XSSI, or
clickjacking.

## Requirements

To support a site-per-process policy in a multi-process web browser, we need to
identify the smallest unit that cannot be split across renderer processes. This
is not actually a single page, but rather a group of documents from the same web
site that have references to each other. Such documents have full script access
to each other's content, and they must run on a single thread, not concurrently.
This group may span multiple frames or tabs, and they may come from multiple
sub-domains of the same site.

![image](ChromeSiteIsolationProject-siteinstances.png)

The HTML spec refers to this group as a "[unit of related similar-origin
browsing
contexts](http://dev.w3.org/html5/spec/single-page.html#unit-of-related-similar-origin-browsing-contexts)."
In Chrome, we refer to this as a **SiteInstance**. All of the documents within a
SiteInstance are allowed to script each other, and we must thus render them in
the same process.

Note that a single tab might be navigated from one web site to another, and thus
it may show different SiteInstances at different times. To support a
site-per-process policy, a browser must be able to swap between renderer
processes for these navigations.

There are also certain JavaScript interactions, such as postMessage() or
close(), that are allowed between windows or frames even when they are showing
documents from different sites. It is necessary to support these limited
interactions between renderer processes.

In addition, top-level documents may contain iframes from different web sites.
These iframes have their own security context and must be rendered in a process
based on their own site, not the site of their parent frame.

## Chrome's Current Status

As described on our [Process Models](../process-models.md) page, there are
currently several cases in which Chrome will place documents from different
sites in the same renderer process. This keeps Chrome compatible with documents
that make script calls across windows, at least until the project tasks
described below are completed. Some examples of cross-site pages that may share
a process:

*   Cross-site iframes are usually hosted in the same process as their parent
    document.
*   Most renderer-initiated navigations (including link clicks, form
    submissions, and scripted navigations) are kept within the current process
    even if they cross a site boundary. This is because other windows in the
    same process may attempt to use postMessage or similar calls to interact
    with them.
*   If too many renderer processes have been created, Chrome starts to reuse
    existing processes rather than creating new ones. This reduces memory
    overhead and avoid OS-specific limitations.

However, Chrome already takes many large steps towards site isolation. For
example, it swaps renderer processes for cross-site navigations that are
initiated in the browser process (such as omnibox navigations or bookmarks).
Cross-origin JavaScript interactions are now supported across processes, such as
postMessage() and navigating another window. Chrome also has preliminary support
for out-of-process iframes.

As a result of this progress, we have adopted a stricter security policy for
certain types of pages, such privileged WebUI pages (like the Settings page).
These pages are never allowed to share a process with regular web pages, even
when navigating in a single tab. This is generally acceptable from a
compatibility perspective because no scripting is expected between normal pages
and WebUI pages, and because these can never be loaded in subframes of
unprivileged pages. With the current level of support for out-of-process
iframes, Chrome can also keep web content out of privileged extension processes.

## Project Tasks

To support a site-per-process policy in Chrome, we need to complete the tasks
outlined below. These will ensure that cross-site navigations and script
interactions will not break, despite having all pages from different sites in
different processes. The master tracking bug is <https://crbug.com/467770>.

*   **Cross-Process Navigations**
    Any navigation to a different web site requires a process swap in the
    current tab or frame.
    *Status: *Complete. Cross-process navigations are supported in all frames,
    and they are used to keep web pages isolated from privileged WebUI or
    extension pages. They are also used opportunistically for cross-site
    browser-initiated (e.g., Omnibox) navigations.
*   **Cross-Process JavaScript**
    As mentioned above, some window and frame level interactions are allowed
    between pages from different sites. Common examples are postMessage, close,
    focus, blur, and assignments to window.location, notably excluding any
    access to page content. These interactions can generally be made
    asynchronous and can be implemented by passing messages to the appropriate
    renderer process.
    *Status:* Complete. Chrome supports all required interactions, including
    frame placeholders, postMessage, close, closed, focus, blur, and assignments
    to window.location between top-level windows in different processes.
*   **Out-of-Process iframes**
    Iframes have separate security contexts from their parent document, so
    cross-site iframes should be rendered in a different process from their
    parent. It is also important that an iframe that is from the same origin as
    a popup window shares a process with the popup window and not its own parent
    page. Our current plan is to render iframes in a separate RenderFrame
    composited into the correct visual location, much like plugins. This is the
    largest requirement for supporting site-per-process, as it involves a major
    architecture change to the Chrome and Blink codebases.
    *Status:* Partially complete. The first uses of [Out-of-Process
    iframes](../oop-iframes/index.md) (OOPIFs) have launched in Chrome 56,
    isolating extensions from web content. We are now expanding support for
    OOPIFs to handle additional use cases. Tracked at <https://crbug.com/99379>.
*   **Improved Renderer Process Limit Policy**
    How to handle Chrome's renderer process limit remains an open question.
    Pending study of the performance implications, we have several options:
    *   *\[Current plan\] *We could choose to protect only a set of sensitive
        web sites, perhaps including those the user has logged into, or all
        HTTPS sites. These sites would never share a process with others, while
        other sites would be allowed to share with each other when the process
        limit is reached. (Caveat: isolating an HTTPS site without isolating its
        HTTP version would allow renderer exploits to view non-secure cookies
        for the site, as well as set secure cookies for the site, per [RFC
        6265](http://tools.ietf.org/html/rfc6265#section-4.1.2.5).)
    *   We could repeal the process limit, particularly on clients with
        sufficient memory. This may cause OS-specific issues as resources are
        depleted, such as the inability to fork new processes.
    *   We could re-use processes according to some heuristics, allowing the
        security benefits of site isolation to degrade as more and more tabs are
        opened. This is less than ideal because the situation may not be evident
        or predictable to the user.
    *   When the limit is reached, other renderer processes may be killed and
        then restored when the user returns. This approach is already taken on
        many mobile devices, but it is less satisfactory on desktops, where
        there is more potential for data loss or visibly killed tabs.
    *   *Status:* We are planning to initially protect only a set of opt-in,
        high-value sites. We will expand this set as resource usage permits.
*   **Browser Enforced Navigation Policy**
    Some of Chrome's policy logic for deciding when to perform a cross-process
    navigation currently runs in the renderer process. Since we assume renderer
    processes may be exploited, we need to move this logic to the browser
    process. Similarly, any attempt or report of a navigation to a cross-site
    page within a renderer should cause the browser to kill the renderer, under
    the assumption it has been exploited. It is worth noting that renderers are
    allowed to request many cross-site resources (such as scripts and images),
    but we can prevent them from receiving cross-site HTML, XML, and JSON
    documents (based on a combination of MIME type and content sniffing).
    *Status:* Our [Cross-Site Document Blocking
    Policy](../blocking-cross-site-documents.md) is posted and tracked at
    <https://crbug.com/268640>.
*   **Security Principal Refactoring**
    Any actions that the browser process takes on behalf of a renderer process
    should be associated with a security principal. This will allow the browser
    to enforce access control based on the principal, rather than a potentially
    inaccurate guess about the tab's current contents. This will help govern
    access to cookies or other data, as well as special APIs for extensions,
    apps, and WebUI pages.
    *Status:* Proposed in <https://crbug.com/109792>, but on hold until a
    clearer need arises.

## Performance

Monitoring the performance impact of Site Isolation on Chrome is a critical part
of this effort. Site Isolation may affect performance in several ways, both
positive and negative: some frames may render faster by running in parallel with
the rest of the page, but creating additional renderer processes also increases
memory requirements and may introduce latency on cross-process navigations. We
are taking several steps to minimize the costs incurred, and we will use
performance monitoring infrastructure to measure these costs.

As mentioned above under "Renderer Process Limit Policy," one of the primary
tradeoffs we are making is that we will isolate only a subset of web sites. Most
web sites will continue to use Chrome's current process model, while web sites
that users are likely to log into will be isolated. This means that the vast
majority of iframes will not require separate renderer processes. We also plan
to reuse renderer processes for a given site across many iframes, reducing the
number of processes required.

For monitoring our performance impact, we are using the
[Telemetry](../../speed-infra/telemetry.1497938021882) framework for Chrome. We
are running existing benchmarks over new (and eventually existing) page sets,
with and without the --site-per-process flag. This allows us to compare metrics
like page load time and memory usage with and without out-of-process iframes.

*Status:* We currently run the "basic_oopif" page set (containing a handful of
real sites with cross-site iframes) with and without --site-per-process in
"page_cycler.basic_oopif" and "page_cycler_site_isolation.basic_oopif." Examples
of page load time and memory usage benchmarks can be seen on the [Chrome
Performance
Dashboard](https://chromeperf.appspot.com/report?sid=74ebe5d13c09e34915152f51ee178f77421b1703d2bf26a7a586375f2c5e2cc7).

## Experimental Support

For users eager to try site isolation before it is enabled by default, we have
added an experimental command-line flag and an entry in chrome://flags ("Enable
out of process iframes"). The --site-per-process flag starts to enforce the
security policy described in this document. This mode should be considered
experimental, as feature regressions are still likely.

The more limited --isolate-extensions mode has launched in Chrome 56, enabling
out-of-process iframes for keeping web content out of privileged extension
processes (and vice versa). It affects fewer cases than --site-per-process and
helps prevent malicious web content from gaining access to extension APIs.

## **Development Resources**

Updating Chrome Features:

*   [Feature Update FAQ for Out-of-Process
    iframes](https://docs.google.com/document/d/1Iqe_CzFVA6hyxe7h2bUKusxsjB6frXfdAYLerM3JjPo/edit?usp=sharing)
*   [Features to Update for Out-of-Process
    iframes](https://docs.google.com/document/d/1dCR2aEoBJj_Yqcs6GuM7lUPr0gag77L5OSgDa8bebsI/edit?usp=sharing)

Build Status:

*   Site Isolation FYI bots:
    [Linux](http://build.chromium.org/p/chromium.fyi/builders/Site%20Isolation%20Linux)
    and
    [Windows](http://build.chromium.org/p/chromium.fyi/builders/Site%20Isolation%20Win)
*   Site Isolation try bot:
    [linux_site_isolation](http://build.chromium.org/p/tryserver.chromium.linux/builders/linux_site_isolation)
*   Performance dashboard:
    [page_cycler_site_isolation.basic_oopif](https://chromeperf.appspot.com/report?sid=74ebe5d13c09e34915152f51ee178f77421b1703d2bf26a7a586375f2c5e2cc7)

## 2015 Site Isolation Summit Talks

Talks and discussion from January 2015.

### Site Isolation Overview

[Slides](https://docs.google.com/presentation/d/10HTTK4dsxO5p6FcpEOq8EkuV4yiBx2n6dBki8cqDWyo/edit?usp=sharing)

#### Site Isolation Overview

### Chromium Changes for OOPIF

[Slides](https://docs.google.com/presentation/d/1e25K7BW3etNDm1-lkMltMcCcRDeVwLibBcbACRhqZ1k/edit?usp=sharing)

#### Chromium Changes for OOPIF

### Blink Changes for OOPIF

[Slides](https://docs.google.com/presentation/d/11nrXiuXBTC72E5l_MUtu2eJN6rcW9PtBewDOPPTk9Bc/edit?usp=sharing)

#### Blink Changes for OOPIF

## Discussions/Questions

The mailing list for technical discussions on Site Isolation is
[site-isolation-dev@chromium.org](mailto:site-isolation-dev@chromium.org).
