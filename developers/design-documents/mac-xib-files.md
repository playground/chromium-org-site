# Mac XIB Files

Cocoa interfaces are typically [created using Xcode's Interface
Builder](https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/LoadingResources/CocoaNibs/CocoaNibs.html).
This program produces XIB files, which are XML representations of a user
interface. Despite being XML, the files are fairly opaque and should not
generally be edited manually, and they often do not merge cleanly via source
control. *The format and content of the XIB file will be different depending on
the OS version and devtools (Xcode) version that is used to edit them.* If you
edit files with consistent versions, the diffs remain marginally readable.
Because of this, Chromium has a policy that requires all XIBs to saved with a
specific developer setup.

### All XIBs must be saved using:

Xcode: **5.1.x**

Mac OS X: **10.9.5**

# Editing a XIB File

Prior to Xcode 4, Interface Builder was a standalone program that could be used
to edit XIBs independently of the Xcode program. In Xcode 4 however, Interface
Builder is now just an IDE component, not a separate program. The
chrome.xcodeproj is humongous and brings Xcode to its knees, making it unideal
for editing XIB files. To make things easier, Chromium has a special Xcode
project just for editing XIB files.

### To edit a XIB:

1.  Run the /src/build/mac/edit_xibs.sh script.
2.  Open the resulting .xcodeproj file in Xcode 5.
3.  Make any necessary user interface changes.
4.  If you are adding any new custom classes, follow the directions below.

The `edit_xibs.sh` script will execute [GYP](https://code.google.com/p/gyp/)
using the Xcode generator for `/src/chrome/chrome_nibs.gyp`. That script's
output will print the path to the Xcode project that you should use for editing
a XIB file.

The `chrome_nibs.gyp` file contains a fake target for use in Xcode. It lists
explicitly all the source files on which the XIBs (listed in `chrome_nibs.gypi`)
depend. For example, all NSWindowController and NSViewController class files are
listed in the sources list for the fake Xcode project. This is because Xcode
uses the class files to create connections between the user interface in the XIB
and the methods and variables to which the elements are bound.

**Any custom NSView, NSWindow, NSWindowController, or NSViewController class
files should be listed in chrome_nibs.gyp.** This will allow you to create new
IBOutlets and new IBActions for classes, as well as allowing Xcode to maintain
existing connections.

### CL Descriptions:

When it comes time to [prepare your changelist](../contributing-code/index.md),
you should list explicitly the XIB changes you have made. This allows your
reviewer to know what was done, and to, if necessary, repeat the steps if the
change were to be merged or re-done. Example:

    [Mac] Add a help center link to the foobar dialog.
    XIB changes:
    * Make the window 20px taller.
    * Add a new NSButton configured with a HyperlinkButtonCell.
    * Set the springs on the button to lock to lower-left corner.
    * Connect the button to |-goToHelpCenter:|.
    BUG=314159
    TEST=(1) Open the foobar dialog.
      (2) Click on the "What does this mean?" link.
      (3) A new tab opens for the help center article.

# Adding a XIB File

Don't add new XIB files. XIB files are difficult to maintain, difficult to
review, and difficult to merge.
