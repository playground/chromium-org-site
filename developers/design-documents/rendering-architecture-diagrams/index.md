# Rendering Architecture Diagrams

The following sequence diagrams illustrate various aspects of Chromium's
rendering architecture. The first picture below shows how Javascript and CSS
animations are scheduled using the requestAnimationFrame callback mechanism.

![image](chromium-request-anim-frame.png)

After the Javascript callback is executed or the CSS animations have been
updated, the web contents view generally needs to be redrawn. The following
(simplified) diagrams show the code execution flow during a repaint in the
non-composited software rendering, composited software rendering and (threaded)
composited GPU rendering modes. Note that the newer [multithreaded ("impl-side")
rasterization mode](../impl-side-painting.md) is not shown below.

Non-composited SW rendering Composited SW rendering Composited GPU rendering

![image](chromium-non-composited-sw-rendering.png)

![image](chromium-composited-sw-rendering.png)

![image](chromium-composited-hw-rendering.png)
