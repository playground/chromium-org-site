# FrameHandle

**This design document is out of date.**

Blink Frames are now implemented using an inheritance relationship, where
blink::Frame is a base class for blink::LocalFrame and blink::RemoteFrame. They
are now fully implemented so this documentation will not be updated.

Doc:
<https://docs.google.com/a/chromium.org/document/edit?id=1PeSpryRsXlLxHU58GOa0s67kuKaQeGitmM0iWS02CvU>

#### FrameHandle

FrameHandle

Diagram:
https://docs.google.com/a/chromium.org/drawings/d/1sqOb8DCNy7wlPMCp9oPyd981wBsYfvy4SsRZD4_HYos/view

#### FrameHandle

FrameHandle
