# API Proposals (New APIs Start Here)

This page on Google intranet:
[go/new-chrome-api](http://goto.google.com/new-chrome-api)

**Note**: Chrome will be removing support for Chrome apps on Windows, Mac, and
Linux over the next 2 years. Extensions will continue to be supported on all
platforms. [Read the
announcement](http://blog.chromium.org/2016/08/from-chrome-apps-to-web.html) and
learn more about [migrating your
app](https://developers.chrome.com/apps/migration).

So you want a new Extension or App API? That's great!

You should expect three distinct phases for your API's development: Proposal,
Development, and Going Live.

*   You need an LGTM from an Apps & Extensions approver: either rdevlin.cronin
    or rockot @ chromium.org
*   You need a second **LGTM** from a Security approver: security-enamel @
    chromium.org.

Email the [apps-dev@chromium.org](mailto:apps-dev@chromium.org) mailing list
twice: once before implementation (proposal), and once after implementation
(final review before Chrome stable).

Need help? Have feedback on this proposal process? Send an email to
[apps-dev@chromium.org](mailto:apps-dev@chromium.org).

Proposal Phase

1.  Read the [CRX API Security
    Checklist](https://docs.google.com/a/google.com/document/d/1RamP4-HJ7GAJY3yv2ju2cK50K9GhOsydJN6KIO81das/pub)
    and make sure your API follows its suggestions.

2.  Fill out an [API
    Proposal](https://docs.google.com/a/chromium.org/document/d/1VJrcI0ZFqN2x6eSamdoflODgn0EvqYfr8_jHCSCn9Xg/edit).

3.  Add your API to the [Chrome API tracking master
    document](https://docs.google.com/a/chromium.org/spreadsheet/ccc?key=0AjdhTmpdv-vvdENKZUI0cGloUGZrY2NJcWVRX3RvV1E#gid=0)
    with status "1:proposal." Make sure to paste a link to your proposal in the
    appropriate column.

4.  Email [apps-dev@chromium.org](mailto:apps-dev@chromium.org), cc'ing
    [security-enamel@chromium.org](mailto:security-enamel@chromium.org), to say
    that you have a new API you'd like reviewed, and include a link to your
    proposal in the email. Suggestion: identify any additional parties or
    individuals and CC them as well.

5.  You’ll get feedback on the apps-dev mailing list. If things are looking
    good, an Apps & Extensions approver (see top) will give LGTM for the API,
    and a Security approver (see top) will give **LGTM** for any security
    implications.

Development Phase

1.  In the [Chrome API tracking master
    document](https://docs.google.com/a/chromium.org/spreadsheet/ccc?key=0AjdhTmpdv-vvdENKZUI0cGloUGZrY2NJcWVRX3RvV1E#gid=0),
    change the status of your API to "2:coding."

2.  Code your extension/app API according to the [implementation
    guidelines](http://dev.chromium.org/developers/design-documents/extensions/proposed-changes/creating-new-apis).
    Your implementation must be kept behind its own feature flag until it works
    and is secure. See chrome/common/extensions/feature_switch.h.

3.  Write
    [documentation](http://dev.chromium.org/developers/design-documents/extensions/proposed-changes/creating-new-apis#TOC-Adding-documentation)
    for your API.

    1.  After your docs are uploaded to codereview, you can preview how it looks
        at http://developer.chrome.com/_patch/<your CL number>/<your api>, for
        example
        <http://developer.chrome.com/_patch/14990011/extensions/storage.html>.

    2.  After your docs CL lands, make sure it appears on the dev channel's API
        page for
        [extensions](http://developer.chrome.com/dev/extensions/api_index.html)
        and/or [apps](http://developer.chrome.com/dev/apps/api_index.html)
        (depending on what your API is targeted for).

4.  After your implementation both works and is secure, you should enable your
    API on the dev channel as described in the implementation guidelines.

5.  Encourage usage of the API in the dev channel; the more adoption during dev,
    the easier your final approval will be.

6.  Remain restricted to the dev channel for a full release cycle (in most
    cases).

7.  After receiving and addressing feedback from a few major extensions or apps,
    proceed to the going live phase.

Going Live Phase

1.  Note: Going live must be initiated at least 2 weeks before branch point.

2.  Make sure that your API is properly represented in the [Chrome API tracking
    master
    document](https://docs.google.com/a/chromium.org/spreadsheet/ccc?key=0AjdhTmpdv-vvdENKZUI0cGloUGZrY2NJcWVRX3RvV1E#gid=0).

3.  Email [apps-dev@chromium.org](mailto:apps-dev@chromium.org), cc'ing
    [security-enamel@chromium.org](mailto:security-enamel@chromium.org), to say
    that you'd like a final review for enabling your stable, well-tested API on
    all channels. You need a second LGTM from an Apps & Extension and Security
    approver (see top).

4.  Have your PM or sponsor file a Type-Launch bug. Your PM or sponsor will
    usher the API through all of the launch reviews, including privacy and
    security.

5.  Update all sample Extensions or Apps for your API.

6.  Land a CL to enable the API for all channels, and change the Status of your
    item in the [Chrome API tracking master
    document](https://docs.google.com/a/chromium.org/spreadsheet/ccc?key=0AjdhTmpdv-vvdENKZUI0cGloUGZrY2NJcWVRX3RvV1E#gid=0)
    to "3: complete."

7.  Work with your PM or sponsor to publicize your new API.
