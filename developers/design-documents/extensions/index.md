# Extensions

### Developers making extensions, see [Chrome Extensions Documentation](https://developer.chrome.com/extensions/index.html)

which includes reference documentation, samples, tutorials, FAQs, discussion
groups and even videos.

Chromium developers, you probably want to:

*   [Learn how the extension system was/is
    designed](how-the-extension-system-works/index.md)
*   [Propose a new Extension
    API](proposed-changes/apis-under-development/index.md)
