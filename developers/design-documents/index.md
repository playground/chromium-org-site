# Design Documents

## **Start Here: Background Reading**

*   [Multi-process Architecture](multi-process-architecture/index.md): Describes
    the high-level architecture of Chromium
    **Note:** Most of the rest of the design documents assume familiarity with
    the concepts explained in this document.
*   [How Chromium Displays Web Pages](displaying-a-web-page-in-chrome/index.md):
    Bottom-to-top overview of how WebKit is embedded in Chromium

## See Also: Design docs in source code

## *   <https://chromium.googlesource.com/chromium/src/+/master/docs/README.md>

## General Architecture

*   [Conventions and patterns for multi-platform
    development](conventions-and-patterns-for-multi-platform-development.md)
*   [Extension Security
    Architecture](http://webblaze.cs.berkeley.edu/2010/secureextensions/): How
    the extension system helps reduce the severity of extension vulnerabilities
*   [HW Video Acceleration in
    Chrom{e,ium}{,OS}](https://docs.google.com/a/chromium.org/document/d/1LUXNNv1CXkuQRj_2Qg79WUsPDLKfOUboi1IWfX2dyQE/preview#heading=h.c4hwvr7uzkfl)
*   [Inter-process Communication](inter-process-communication.md): How the
    browser, renderer, and plugin processes communicate
*   [Multi-process Resource Loading](multi-process-resource-loading/index.md):
    How pages and images are loaded from the network into the renderer
*   [Plugin Architecture](plugin-architecture/index.md)
*   [Process Models](process-models.md): Our strategies for creating new
    renderer processes
*   [Profile Architecture](profile-architecture/index.md)
*   [SafeBrowsing](safebrowsing/index.md)
*   [Sandbox](https://chromium.googlesource.com/chromium/src/+/master/docs/design/sandbox.md)
*   [Security Architecture](http://crypto.stanford.edu/websec/chromium/): How
    Chromium's sandboxed rendering engine helps protect against malware
*   [Startup](https://chromium.googlesource.com/chromium/src/+/master/docs/design/startup.md)
*   [Threading](https://chromium.googlesource.com/chromium/src/+/master/docs/design/threading.md):
    How to use threads in Chromium

Also see the documentation for [V8](http://code.google.com/apis/v8/), which is
the JavaScript engine used within Chromium.

## UI Framework

*   [UI Development Practices](ui-development-practices.md): Best practices for
    UI development inside and outside of Chrome's content areas.
*   [Views framework](chromeviews.md): Our UI layout layer used on
    Windows/Chrome OS.
*   [views Windowing system](views-windowing/index.md): How to build dialog
    boxes and other windowed UI using views.
*   [Aura](aura/index.md): Chrome's next generation hardware accelerated UI
    framework, and the new ChromeOS window manager built using it.
*   [NativeControls](native-controls/index.md): using platform-native widgets in
    views.
*   Focus and Activation with Views and Aura.

**Graphics**

## *   [Overview](chromium-graphics/index.md)
## *   [GPU Accelerated Compositing in
       Chrome](gpu-accelerated-compositing-in-chrome/index.md)
## *   [GPU Feature Status
       Dashboard](gpu-accelerated-compositing-in-chrome/gpu-architecture-roadmap.md)
## *   [Rendering Architecture
       Diagrams](rendering-architecture-diagrams/index.md)
## *   [Graphics and Skia](graphics-and-skia.md)
## *   [RenderText and Chrome UI text drawing](rendertext.md)
## *   [GPU Command Buffer](gpu-command-buffer.md)
## *   [GPU Program
       Caching](https://docs.google.com/a/chromium.org/document/d/1Vceem-nF4TCICoeGSh7OMXxfGuJEJYblGXRgN9V9hcE/edit)
## *   [Compositing in Blink/WebCore: from WebCore::RenderLayer to
       cc::Layer](https://docs.google.com/presentation/d/1dDE5u76ZBIKmsqkWi2apx3BqV8HOcNf4xxBdyNywZR8/edit?usp=sharing)
## *   [Compositor Thread Architecture](compositor-thread-architecture.md)
## *   [Rendering Benchmarks](rendering-benchmarks.md)
## *   [Impl-side Painting](impl-side-painting.md)
## *   [Video Playback and
       Compositor](http://www.chromium.org/developers/design-documents/video-playback-and-compositor)
## *   [ANGLE architecture
       presentation](https://docs.google.com/presentation/d/1CucIsdGVDmdTWRUbg68IxLE5jXwCb2y1E9YVhQo0thg/pub?start=false&loop=false)

**Network stack**

*   [Overview](network-stack/index.md)
*   [Network Stack Objectives](network-stack/network-stack-objectives.md)
*   [Crypto](crypto.md)
*   [Disk Cache](network-stack/disk-cache/index.md)
*   [HTTP Cache](network-stack/http-cache/index.md)
*   [Out of Process Proxy Resolving Draft
    \[unimplemented\]](../../system/errors/NodeNotFound)
*   [Proxy Settings and
    Fallback](network-stack/proxy-settings-fallback/index.md)
*   [Debugging network proxy problems](network-stack/debugging-net-proxy.md)
*   [HTTP Authentication](http-authentication/index.md)
*   [View network internals tool](../../system/errors/NodeNotFound)
*   [Make the web faster with SPDY](http://www.chromium.org/spdy/) pages
*   [Make the web even faster with QUIC](../../quic/index.md) pages
*   [Cookie storage and retrieval](network-stack/cookiemonster/index.md)

**Security**

*   [Security
    Overview](../../chromium-os/chromiumos-design-docs/security-overview.md)
*   [Protecting Cached User
    Data](../../chromium-os/chromiumos-design-docs/protecting-cached-user-data/index.md)
*   [System
    Hardening](../../chromium-os/chromiumos-design-docs/system-hardening.md)
*   [Chaps Technical Design](chaps-technical-design/index.md)
*   [TPM Usage](tpm-usage.md)
*   [Per-page Suborigins](per-page-suborigins.md)
*   [Encrypted Partition Recovery](encrypted-partition-recovery.md)

## Input

*   See [chromium input](../../teams/index.md) for design docs and other
    resources.

## Rendering

*   [Multi-column layout](multi-column-layout.md)
*   [Style Invalidation in
    Blink](https://docs.google.com/document/d/1vEW86DaeVs4uQzNFI5R-_xS9TcS1Cs_EUsHRSgCHGu8/)
*   [Blink Coordinate Spaces](blink-coordinate-spaces/index.md)

## Building

*   [IDL build](idl-build.md)
*   [IDL compiler](idl-compiler.md)

See also the documentation for [GYP](https://code.google.com/p/gyp/w/list),
which is the build script generation tool.

## Testing

*   [Layout test results dashboard](layout-tests-results-dashboard.md)
*   [Generic theme for Test Shell](generic-theme-for-test-shell/index.md)
*   [Moving LayoutTests fully upstream](../../system/errors/NodeNotFound)

## Feature-Specific

*   [about:conflicts](about-conflicts/index.md)
*   [Accessibility](accessibility/index.md): An outline of current (and coming)
    accessibility support.
*   [Auto-Throttled Screen Capture and
    Mirroring](auto-throttled-screen-capture-and-mirroring.md)
*   [Browser Window](browser-window/index.md)
*   [Chromium Print
    Proxy](http://www.chromium.org/developers/design-documents/google-cloud-print-proxy-design):
    Enables a cloud print service for legacy printers and future cloud-aware
    printers.
*   [Constrained Popup Windows](constrained-popup-windows.md)
*   [Desktop Notifications](desktop-notifications/index.md)
*   [DirectWrite Font Cache for Chrome on Windows](directwrite-font-cache.md)
*   [DNS Prefetching](dns-prefetching.md): Reducing perceived latency by
    resolving domain names before a user tries to follow a link
*   [Embedding Flash Fullscreen in the Browser
    Window](embedding-flash-fullscreen-in-the-browser-window/index.md)
*   [Extensions](extensions/index.md): Design documents and proposed APIs.
*   [Find Bar](find-bar/index.md)
*   [Form Autofill](form-autofill_index.md): A feature to automatically fill out
    an html form with appropriate data.
*   [Geolocation](https://docs.google.com/a/chromium.org/document/pub?id=13rAaY1dG0nrlKpfy7Txlec4U6dsX3PE9aXHkvE37JZo):
    Adding support for [W3C Geolocation
    API](http://www.w3.org/TR/geolocation-API/) using native WebKit bindings.
*   [IDN in Google Chrome](idn-in-google-chrome.md)
*   [IndexedDB](indexeddb/index.md) (early draft)
*   [Info Bars](info-bars/index.md)
*   [Installer](../installer.md): Registry entries and shortcuts
*   [Instant](instant/index.md)
*   [Isolated Sites](isolated-sites.md)
*   [Linux Resources and Localized
    Strings](linuxresourcesandlocalizedstrings.md): Loading data resources and
    localized strings on Linux.
*   [Media Router & Web Presentation API](media-router/index.md)
*   [Memory Usage Backgrounder: ](../memory-usage-backgrounder/index.md)Some
    information on how we measure memory in Chromium.
*   [Mouse Lock](mouse-lock.md)
*   Omnibox Autocomplete: While typing into the omnibox, Chromium searches for
    and suggests possible completions.
    *   [HistoryQuickProvider](../../omnibox-history-provider/index.md):
        Suggests completions from the user's historical site visits.
*   [Omnibox/IME Coordination](omnibox-ime-coordination/index.md)
*   [Ozone Porting
    Abstraction](https://chromium.googlesource.com/chromium/src/+/master/docs/ozone_overview.md)
*   [Password Form Styles that Chromium
    Understands](form-styles-that-chromium-understands.md)
*   [Password Generation](password-generation/index.md)
*   [Pepper plugin implementation](pepper-plugin-implementation/index.md)
*   [Plugin Power
    Saver](https://docs.google.com/document/d/1r4xFSsR4gtjBf1gOP4zHGWIFBV7WWZMgCiAHeepoHVw/edit?usp=sharing)
*   [Preferences](preferences.md)
*   [Prerender](prerender/index.md)
*   [Print Preview](print-preview.md)
*   [Printing](printing.md)
*   [Rect-based event targeting in views](views-rect-based-targeting.md): Making
    it easier to target views elements with touch.
*   [Replace the modal cookie prompt](cookie-prompt-replacement.md)
*   [SafeSearch](safesearch.md)
*   [Sane Time](sane-time.md): Determining an accurate time in Chrome
*   [Secure Web Proxy](secure-web-proxy.md)
*   [Service Processes](service-processes.md)
*   [Site Engagement](site-engagement.md): Tracking user engagement with sites
    they use.
*   [Site Isolation](site-isolation/index.md): In-progress effort to improve
    Chromium's process model for security between web sites.
*   [Software Updates: Courgette](software-updates-courgette.md)
*   [Sync](sync/index.md)
*   [Tab Helpers](../../system/errors/NodeNotFound)
*   [Tab to search](../../tab-to-search.md): How to have the Omnibox
    automatically provide tab to search for your site.
*   [Tabtastic2 Requirements](tabtastic-2-requirements.md)
*   [Temporary downloads](../../system/errors/NodeNotFound)
*   [Time Sources](time-sources.md): Determining the time on a Chrome OS device
*   [TimeTicks](https://docs.google.com/document/d/1ypBZPZnshJ612FWAjkkpSBQiII7vYEEDsRkDOkVjQFw/edit?usp=sharing):
    How our monotonic timer, TimeTicks, works on different OSes
*   [UI Mirroring Infrastructure](ui-mirroring-infrastructure/index.md):
    Describes the UI framework in ChromeViews that allows mirroring the browser
    UI in RTL locales such as Hebrew and Arabic.
*   [UI Localization](ui-localization/index.md): Describes how localized strings
    get added to Chromium.
*   [User scripts](user-scripts.md): Information on Chromium's support for user
    scripts.
*   [Video](video/index.md)
*   WebSocket: A message-oriented protocol which provides bidirectional
    TCP/IP-like communication between browsers and servers.
    *   [Design
        doc](https://docs.google.com/document/d/11n3hpwb9lD9YVqnjX3OwzE_jHgTmKIqd6GvXE9bDGUg/edit):
        mostly still relevant but some parts have been revised. In particular,
        mojo is used for the communication between Blink and //content now.
    *   [WebSocket
        handshake](https://docs.google.com/document/d/1r7dQDA9AQBD_kOk-z-yMi0WgLQZ-5m7psMO5pYLFUL8/edit):
        The HTTP-like exchange before switching to WebSocket-style framing in
        more detail.
    *   [Obsolete design
        doc](https://docs.google.com/document/d/1_R6YjCIrm4kikJ3YeapcOU2Keqr3lVUPd-OeaIJ93qQ/pub):
        WebSocket code has been drastically refactored. Most of the code
        described in this doc is gone. It is mostly only of historical interest.
*   [Web MIDI](web-midi/index.md)
*   [WebNavigation API internals](webnavigation-api-internals.md)

## OS-Specific

*   **Android**
    *   [Java Resources on Android](java-resources-on-android.md)
    *   [JNI Bindings](android-jni.md)
    *   [WebView code
        organization](https://docs.google.com/document/d/1a_cUP1dGIlRQFUSic8bhAOxfclj4Xzw-yRDljVk1wB0/edit?usp=sharing)
*   **Chrome OS**
    *   See the [Chrome OS design
        documents](../../chromium-os/chromiumos-design-docs/index.md) section.
*   **Mac OS X**
    *   [AppleScript Support](applescript.md)
    *   [BrowserWindowController Object
        Ownership](../../system/errors/NodeNotFound)
    *   [Confirm to Quit](confirm-to-quit-experiment/index.md)
    *   [Mac App
        Mode](http://dev.chromium.org/developers/design-documents/appmode-mac)
        (Draft)
    *   [Mac Fullscreen Mode](fullscreen-mac.md) (Draft)
    *   [Mac NPAPI Plugin Hosting](mac-plugins.md)
    *   [Mac specific notes on UI Localization](ui-localization/mac-notes.md)
    *   [Menus, Hotkeys, & Command Dispatch](command-dispatch-mac.md)
    *   [Notes from meeting on IOSurface usage and
        semantics](iosurface-meeting-notes.md)
    *   [OS X Interprocess Communication
        (Obsolete)](os-x-interprocess-communication.md)
    *   [Password Manager/Keychain
        Integration](os-x-password-manager-keychain-integration.md)
    *   [Sandboxing Design](sandbox/osx-sandboxing-design.md)
    *   [Tab Strip Design (Includes tab layout and tab
        dragging)](tab-strip-mac/index.md)
    *   [Wrench Menu Buttons](wrench-menu-mac/index.md)
*   **iOS**
    *   [WKWebView Problems Solved by web//
        layer](https://docs.google.com/document/d/1qgFVIhUdQf_RxhzF-sAsETvo7CNqSLmzlz5YY7rfRe0/edit)

## Other

*   [64-bit Support](64-bit-support.md)
*   [Layered Components](layered-components-design.md)
*   [Closure Compiling Chrome
    Code](https://docs.google.com/a/chromium.org/document/d/1Ee9ggmp6U-lM-w9WmxN5cSLkK9B5YAq14939Woo-JY0/edit#heading=h.34wlp9l5bd5f)
*   [content module](../content-module/index.md) / [content
    API](../content-module/content-api.md)
*   [Design docs that still need to be
    written](http://code.google.com/p/chromium/wiki/DesignDocsToWrite) (wiki)
*   [In progress refactoring of key browser-process architecture for
    porting](../Web-page-views/index.md)
*   [Network Experiments](../../network-speed-experiments.md)
*   [Transitioning InlineBoxes from floats to
    LayoutUnits](https://docs.google.com/a/chromium.org/document/d/1fro9Drq78rYBwr6K9CPK-y0TDSVxlBuXl6A54XnKAyE/edit)

## Full listing of all sub-pages:
