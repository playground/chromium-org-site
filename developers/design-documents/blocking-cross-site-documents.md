# Blocking Cross-Site Documents for Site Isolation

### Motivation

In general, Chrome's renderer processes should not be given access to data they
don't need. Our [Site Isolation project](site-isolation/index.md) is trying to
make it possible to limit a renderer process to pages from only a single site.
If that project succeeds, we would like to block such a process from receiving
network responses that the site isn't allowed to use.

A web site is allowed to request **documents** from its own site (or those that
have been made available using CORS), as well as **resources** from any site.

**Documents** include HTML, XML, and JSON files. **Resources** include a wide
variety of files that are "opaque" to the site (i.e., cannot be leaked back to
the site's owner), including images, JavaScript, CSS, fonts, and TextTrack
files, among others. **Cross-site documents** come from a different site than
the page that requests them and do not have CORS headers that would make them
accessible.

However, we can't rely solely on the MIME type of the HTTP response to
distinguish documents from resources, since the MIME type on network responses
is frequently wrong. Content sniffing can help to confirm whether a document is
what it claims to be, but using typical content sniffers would lead to
overzealous blocking and create compatibility problems. Fortunately, our
experiments show that we can enforce a policy that blocks most cross-site
documents without breaking existing sites, using a combination of MIME type and
custom content sniffers.

### Proposed Policy

Our proposed **cross-site document blocking policy** prevents a renderer process
from receiving cross-site documents that (1) have a HTML, XML, or JSON MIME
type, and (2) are confirmed to be that type by a custom content sniffer (or are
marked NoSniff). It does not protect files that have other MIME types. Documents
that are blocked appear as empty to the process that requested them.

While implementing versions of this policy, we made several observations about
how the content sniffing logic must behave:

*   For HTML, we cannot assume "<!--" is valid HTML prefix, since that also
    matches many JavaScript files in practice. Instead, we plan to find the end
    of the HTML comment and then sniff again.
*   For HTML and XML, we cannot block "<?xml" unless we can confirm it isn't an
    SVG. Cross-site SVG files are permitted.
*   There is no existing JSON sniffer, but it is necessary because many existing
    JavaScript files are mislabeled as JSON. We have implemented a basic
    sniffer.

To preserve compatibility, we are aiming to avoid disrupting the behavior of
existing web sites when blocking responses, within reasonable limits. We
consider certain behavior changes acceptable, though. For example, we prevent
actual HTML, XML, or JSON files from being parsed as CSS. CSS parsers have
traditionally been very lenient and can discover "rules" from almost any
content, which has been used by security researchers for attacks \[1\].

When measuring the potential impact of the policy, we discovered several
"non-disruptive" cases in which cross-site documents were blocked but that cause
no behavior change to the page. These include:

*   Responses with error status codes in any context except images. (Images
    continue to render regardless of status code, and this is sometimes used in
    practice for 404 images.)
*   Responses that could not have been parsed for the target. For example, files
    sniffed as HTML cannot be parsed as JavaScript, and non-SVG text files
    cannot be parsed as images.

We consider it harmless to block such network responses.

We only plan to enforce the policy on web renderer processes. Extension
processes in Chrome are allowed to request more documents (based on the host
permissions in the extension's manifest), and we could later enforce a similar
policy that accounts for their permissions. Plugin processes no longer route
their network requests through the renderer process (as of
<http://crbug.com/286074>), so we do not block documents for plugins.

Unfortunately, we do have to give up some security properties when extensions
with content scripts are installed. Such extensions can request permission in
their manifest to make XmlHttpRequests to other sites from within the renderer
process. We cannot block such documents from the renderer process, which would
allow an exploited renderer to also access such documents.

### Compatibility Results

To measure compatibility, we ran an experiment that collected user metrics on
how many network responses would be blocked using our policy. Our experiment
followed the policy described above, with two caveats. First, it incorrectly
blocks cross-site SVG files. Second, it incorrectly blocks cross-site requests
made by extension content scripts. Using one week of data collected from Chrome
Beta users (using Chrome's opt-in UMA system), we found that 0.16% of network
responses were blocked overall, but more than half of these we could prove to be
non-disruptive (e.g., error status code). Only 0.075% of network responses were
potentially disruptive.

We intend to refine our experiment to avoid blocking content scripts and SVG
files. Based on the data we collected, we found that content scripts could
account for up to 0.009% of responses. Accounting for SVG files could let us
conclude that up to another 0.063% of responses were not disruptive. This
suggests that our updated policy could be disruptive for as few as 0.003% of
network responses.

Using these results, we hope to deploy the cross-site document blocking policy
to real users. In combination with our Site Isolation work, this can help
protect confidential documents from leaking to other sites.

### Presentation

#### Tech Talk: Cross-Site Document Blocking

Tech Talk: Cross-Site Document Blocking

### References

\[1\] Huang, Lin-Shung, et al. "Protecting browsers from cross-origin CSS
attacks." Proceedings of the 17th ACM conference on Computer and communications
security. ACM, 2010.
