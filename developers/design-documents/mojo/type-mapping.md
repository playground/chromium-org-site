# Type Mapping in C++

This documentation has moved [into the Chromium
repository.](https://chromium.googlesource.com/chromium/src/+/master/mojo/public/cpp/bindings/README.md#Type-Mapping)
