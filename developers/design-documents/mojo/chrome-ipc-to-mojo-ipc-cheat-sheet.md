# Chrome IPC To Mojo IPC Cheat Sheet

## Overview

This document is intended to provide an overview of how legacy IPC concepts map
to Mojo constructs. Documentation for legacy IPC can be found in [Inter-process
Communication (IPC)](../inter-process-communication.md) and [Security Tips for
IPC](../../../Home/chromium-security/education/security-tips-for-ipc.md).

## Threading Model

### IPC

IPCs can be sent and recieved from any threads. If the sending/receiving thread
is not the IO thread, there is always a hop and memory copy to/from the IO
thread. Related IPCs are grouped in the same _messages.h file only for
convenience, so different messages within the same file can be sent from
different threads or received on different threads.

### Mojo

A binding or interface pointer can only be used on one thread at a time since
they're not thread-safe. However the message pipe can be unbound using either
Binding::Unbind or InterfacePtr::PassInterface and then it can be bound again on
a different thread. Making a method call doesn't involve a thread hop, but
receiving it on a thread other than the IO thread does involve a thread hop.

Ordering between legacy IPC and Mojo IPC is guaranteed only to the IO thread at
this time.

**SECURITY FOLLOWUPS:**

** TODO: are there any examples of type converters between mojo enums? Is this
something we want to encourage? Maybe not? Also, should chrome-security do a
periodic audit of things marked \[Extensible\]? A quick search shows that there
are some things marked \[Extensible\] that probably don't need to be.**

## Declaring Messages

### IPC

Messages are declared with macros like `IPC_MESSAGE_ROUTED1` and
`IPC_MESSAGE_CONTROL2`, which differentiate between control and routed messages.
Messages are generally grouped together by message classes, which are defined in
[//ipc/ipc_message_start.h](https://chromium.googlesource.com/chromium/src/+/master/ipc/ipc_message_start.h).

    IPC_SYNC_MESSAGE_CONTROL0_1(ClipboardHostMsg_ReadText,
                                std::string /* result */)
    IPC_MESSAGE_CONTROL1(ClipboardHostMsg_WriteText,
                         std::string /* text */)

### Mojo

There is no differentiation between control and routed messages in Mojo since
there are no routing IDs. Instead messages flow between two endpoints of a
message pipe. Mojo messages are declared as interfaces with methods, with
related methods grouped together in the same interface. A hypothetical
`clipboard.mojom` might look like this:

    interface ClipboardHost {
      ReadText() => (string text);
      WriteText(string text);
    };

## Receiving / Sending Messages

### IPC

Messages are received via the `IPC::Listener` interface. Message dispatch is
typically handled using helper macros:

    bool ClipboardMessageFilter::OnMessageReceived(const IPC::Message& message) {
      bool handled = true;
      IPC_BEGIN_MESSAGE_MAP(ClipboardMessageFilter, message)
        IPC_MESSAGE_HANDLER(ClipboardHostMsg_ReadText, OnReadText)
        IPC_MESSAGE_HANDLER(ClipboardHostMsg_WriteText, OnWriteText)
        IPC_MESSAGE_UNHANDLED(handled = false)
      IPC_END_MESSAGE_MAP()
      return handled;
    }
    void ClipboardMessageFilter::OnReadText(base::string16* result) {
      // Do stuff.
    }
    void ClipboardMessageFilter::OnWriteText(const base::string16& result) {
      // Do other stuff.
    }

Messages are sent via IPC::Sender::Send(). Typically, code will construct a
message, fill out the message params, and then call the IPC sender with the
message:

    std::string WebClipboardImpl::ReadText() {
      std::string result;
      sender_->Send(new ClipboardHostMsg_ReadText(&result));
      return result;
    }

### Mojo

The Mojo IDL compiler processes the .mojom files to generate corresponding C++
bindings (as well as bindings for other languages). The C++ bindings have
generated stub interfaces that a C++ implementation should override:

    #include "services/clipboard/clipboard.mojom.h"
    class ClipboardImpl : public Clipboard {
     public:
      void ReadText(const ReadTextCallback& callback) {
        // Do stuff.
      }
      void WriteText(const mojo::String& s) {
        // Do other stuff.
      }
    };

Sending messages in Mojo is simply a matter of calling the corresponding method
on the proxy:

    ClipboardPtr clipboard;
    render_frame->GetRemoteInterfaces()->GetInterface(
        mojo::GetProxy(&clipboard));
    clipboard->WriteText("Hello World!");

## Pickling Values

In IPC, messages are simply a `base::Pickle`. `base::Pickle` has some methods
for reading/writing primitive types, such as `uint32_t`, `float`, and
`base::StringPiece`. Serializing/deserializing a type that doesn't have a
corresponding `base::Pickle::WriteT` method requires specializing the
`IPC::ParamTraits` template. If a message parameter fails deserialization in the
browser process, the child process is killed. Currently with Mojo the message
pipe is closed, but we don't kill the process (<http://crbug.com/607293> tracks
doing that).

Mojo also has a set of primitives types that it understands how to
serialize/deserialize by default, such as `string`, `array`, `int`, `enum`, etc.

**TODO(dcheng): Make sure we still emphasize that we want to preserve
type-safety as much as possible. If that means defining a type mapping and
writing custom validators, then so be it. Doing that is much better than, say,
serializing a mojo::Array of bytes and then memcpying it into a struct.**

### Enums (e.g. IPC_ENUM_TRAITS and friends)

#### IPC

IPC has several different macros for creating a boilerplate `IPC::ParamTraits`
specialization for enums:

    // Serialization/deserialization of an enum with no validation: typically
    // used for enums that are actually bitfields.
    IPC_ENUM_TRAITS(blink::WebDragOperation)
    // Serialization/deserialization of an enum. Validates that the deserialized
    // enum value isn't less than 0 and isn't greater than the last declared
    // value.
    IPC_ENUM_TRAITS_MAX_VALUE(ui::DragDropTypes::DragEventSource,
                              ui::DragDropTypes::DRAG_EVENT_SOURCE_LAST)
    // Like the previous macro, but validates that the enum value isn't less
    // than a custom minimum.
    IPC_ENUM_TRAITS_MIN_MAX_VALUE(content::PageZoom,
                                  content::PageZoom::PAGE_ZOOM_OUT,
                                  content::PageZoom::PAGE_ZOOM_IN)

#### Mojo

Just declare an enum in the .mojom file.

    // Mojo automatically validates that the received enum has a legal value!
    enum PageZoom {
      OUT,
      RESET,
      IN
    };
    // The Extensible attribute tells Mojo to skip validation. For bitfields,
    // there can be many combinations of possible values. Extensible should only be
    // used for these sorts of situations.
    [Extensible]
    enum DragOperation {
      None = 0,
      Copy = 1,
      Link = 2,
      Generic = 4,
      Private = 8,
      Move = 16,
      Delete = 32,
    };

### Transport-only structs (e.g. IPC_STRUCT_BEGIN, IPC_STRUCT_MEMBER, and IPC_STRUCT_END)

IPC provides `IPC_STRUCT_BEGIN`, `IPC_STRUCT_MEMBER`, and `IPC_STRUCT_END`
macros as a way to define simple structs that are only used as IPC message
parameters.

#### IPC

Defining a struct with these macros looks like this today:

    IPC_STRUCT_BEGIN(ViewMsg_New_Params)
      IPC_STRUCT_MEMBER(int32_t, view_id, MSG_ROUTING_NONE)
      IPC_STRUCT_MEMBER(int32_t, main_frame_routing_id, MSG_ROUTING_NONE)
      IPC_STRUCT_MEMBER(int32_t, main_frame_widget_routing_id, MSG_ROUTING_NONE)
      IPC_STRUCT_MEMBER(int64_t, session_storage_namespace_id)
      IPC_STRUCT_MEMBER(int, opener_frame_route_id, MSG_ROUTING_NONE)
    IPC_STRUCT_END()

And the resulting struct can be used like this:

    bool RenderViewHostImpl::CreateRenderView(int opener_frame_route_id, ...) {
      /* do stuff */
      ViewMsg_New_Params params;
      params.view_id = GetRoutingID();
      /* …fill in rest of the fieldsu2026 */
      params.opener_frame_route_id = opener_frame_route_id;
      Send(new ViewMsg_New(params));
      /* do the rest of the stuff */
    }

#### Mojo

Structs are defined in `.mojom` files:

    struct CompositionSegment {
      uint32 start_offset;
      uint32 end_offset;
      bool emphasized;
    };

The struct can be used as a parameter type:

    interface ImeInstance {
      SetCompositionText(string text, array<CompositionSegment> segments);
    }

**Important: while this example uses the generated mojo structs directly, this
is highly discouraged: typemapping to native C++ types usually makes the code
more readable, and more importantly, makes sure that the mojo structs go through
data validation (e.g. the renderer is not sending the browser invalid GURLs,
etc)**

And using it in C++ looks like this:

    static mojo::Array<arc::mojom::CompositionSegmentPtr> ConvertSegments(
        const ui::CompositionText& composition) {
      mojo::Array<arc::mojom::CompositionSegmentPtr> segments =
          mojo::Array<arc::mojom::CompositionSegmentPtr>::New(0);
      for (const ui::CompositionUnderline& underline : composition.underlines) {
        arc::mojom::CompositionSegmentPtr segment =
            arc::mojom::CompositionSegment::New();
        segment->start_offset = underline.start_offset;
        segment->end_offset = underline.end_offset;
        segment->emphasized = (underline.thick ||
            (composition.selection.start() == underline.start_offset &&
             composition.selection.end() == underline.end_offset));
        segments.push_back(std::move(segment));
      }
      return segments;
    }
    void ArcImeBridgeImpl::SendSetCompositionText(
        const ui::CompositionText& composition) {
      ime_instance_->SetCompositionText(base::UTF16ToUTF8(composition.text),
                                        ConvertSegments(composition));
    }

### Pre-defined structs (e.g. IPC_STRUCT_TRAITS_BEGIN, IPC_STRUCT_TRAITS_MEMBER, and IPC_STRUCT_TRAITS_END)

#### IPC

Sometimes, IPC messages need to include structs that are already defined
elsewhere. To help serialize simple structs for this case, IPC provides
`IPC_STRUCT_TRAITS_BEGIN`, `IPC_STRUCT_TRAITS_MEMBER`, and
`IPC_STRUCTS_TRAITS_END`. For example, given:

    namespace ui {
    struct FileInfo {
      base::FilePath path;
      base::FilePath display_name; // Optional.
    };
    } // namespace ui
    IPC_STRUCT_TRAITS_BEGIN(ui::FileInfo)
      IPC_STRUCT_TRAITS_MEMBER(path)
      IPC_STRUCT_TRAITS_MEMBER(display_name)
    IPC_STRUCT_TRAITS_END()

C++ code can pass a `ui::FileInfo` as an IPC parameter and it Just Works™:

#### Mojo

There isn't really a Mojo equivalent for this. Structs that need to be sent over
Mojo need to have a corresponding Mojo struct. See the custom serialization
section below for more information.

### Custom serialization (e.g. custom IPC::ParamTraits)

#### IPC

There are some C++ types that can't be serialized with existing
`IPC::ParamTraits` specializations. Those types specialize `IPC::ParamTraits`
directly, rather than using the macros, and use the underlying `base::Pickle`
methods to serialize/deserialize. A simple custom `IPC::ParamTraits` might look
like this:

    template <>
    struct ParamTraits<std::string> {
      typedef std::string param_type;
      static void GetSize(base::PickleSizer* sizer, const param_type& p) {
        sizer->AddString(p);
      }
      static void Write(base::Pickle* m, const param_type& p) { m->WriteString(p); }
      static bool Read(const base::Pickle* m,
                       base::PickleIterator* iter,
                       param_type* r) {
        return iter->ReadString(r);
      }
      IPC_EXPORT static void Log(const param_type& p, std::string* l);
    };

#### Mojo

Mojo uses `StructTraits` specializations in conjunction with type maps to
transparently map between the native type and the Mojo structs at the binding
layer. It's a bit too complicated to provide a simple example here, so please
see [Type Mapping in C++](type-mapping.md) for how to use this feature.

Note that Mojo also has a legacy `TypeConverter` mechanism for going to/from
Mojo types. The use of type converters is discouraged in new code. Using
`StructTraits` simplifies client code (e.g. code can use `GURL` instead of
`url::mojom::Url`) and it allows validation of the data that needs custom
serialization/deserialization. The latter point is quite important, since Mojo
IPCs often go from the unprivileged renderer process to the privileged browser
process.

## Sync messages

In general, just like with legacy IPCs it's best to avoid synchronous messages
if possible. If something really must be synchronous for legacy reasons, the
`[Sync]` IDL attribute can be used. See the documentation on [Synchronous
Calls](synchronous-calls.md) for more information.
