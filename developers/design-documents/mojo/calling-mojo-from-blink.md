# Calling Mojo from Blink

## Variants

Let's assume we have a mojom file such as this:

`module example.mojom;`

`interface Foo {`

` SendData(string param1, array<int32> param2);`

`};`

Given the following GN mojom definition the compiler will generate two targets:
example and example_blink

`mojom("example") {`
` sources = [ "example.mojom" ]`
` use_new_wrapper_types = true`
`}`

The target "example" will generate Chromium-style C++ bindings using STL types:

// example.mojom.h
class Example {
virtual void SendArray(const std::string& param1, const std::vector<int32_t>&
param2) = 0;
}

The target "example_blink" will generate Blink-style C++ bindings using WTF
types:

// example.mojom-blink.h
class Example {
virtual void SendArray(const WTF::String& param1, const WTF::Vector<int32_t>&
param2) = 0;
}

Thanks to these separate sets of bindings no work is necessary to convert types
between Blink-style code and Chromium-style code. It is handled automatically
during message serialization and deserialization.

**Converting WTF::Function to base::Callback**

Mojo methods that return a value take an instance of base::Callback. To solve
this problem use WTF::convertToBaseCallback to turn a WTF::Function into a
base::Callback that accepts the same arguments. Do not try to use base::Bind
directly as it doesn't understand Oilpan. For example,

` #include "wtf/Functional.h"`
`void MyObject::reverseString(const String& string)`
`{`
` m_service->ReverseString(`
` string,`
` convertToBaseCallback(WTF::bind(&MyObject::onStringReversed, wrapPersistent(this))));`
`}`

Mojo is not traced by Oilpan so one must be careful. If an interface pointer is
owned by an managed object the message pipe will be closed during lazy sweeping.
This means that messages may be received after the object has become unreachable
and pointers to other managed objects are possibly invalid. To avoid this all
managed objects bound with WTF::bind must be either Persistent or WeakPersistent
references. This is done by wrapping pointers passed to WTF::bind with either
wrapPersistent() or wrapWeakPersistent(this).

WeakPersistent references should be used to avoid cycles whenever a callback is
expected to have the same lifetime as the object. The most common example of
this is the this pointer bound to the callback passed to
InterfacePtr::set_connection_error_callback.

Persistent references should be used at any other time when the interface
pointer is owned by the bound object. This is because if no other reference to
the object exists it will be garbage collected before the response is received,
canceling the request. Only if this behavior is desired should a WeakPersistent
be used instead.

If response callbacks are bound with wrapPersistent() then a connection error
handler that closes the message pipe should be attached to the interface
pointer. Otherwise callbacks will not be destroyed when the pipe is closed and
the object will leak.

` // m_service is a member of MyObject. This code should be run as soon as m_foo is bound to a message pipe.`
`m_service.set_connection_error_handler(`
` convertToBaseCallback(WTF::bind(&MyObject::onConnectionError,`
` wrapWeakPersistent(this))));`
`void MyObject::onConnectionError()`
`{`
` m_service.reset();`
`}`

**Implementing Mojo interfaces in Blink**

Only a mojo::Binding or mojo::BindingSet should be used when implementing a Mojo
interface in an Oilpan managed object. The object must then have a pre-finalizer
to close any open pipes when the object is about to be swept as lazy sweeping
means that it may be invalid long before the destructor is called. This requires
setup in both the object header and implementation.

` // MyObject.h`
`class MyObject : public GarbageCollected`
` , public example::blink::Example`
`{`
` USING_PRE_FINALIZER(MyObject, dispose);`
` public:`
` MyObject();`
` void dispose();`
` // Implementation of example::blink::Example.`
` private:`
` mojo::Binding<example::blink::Example> m_binding;`
`};`
`// MyObject.cpp`
`MyObject::MyObject() : m_binding(this)`
`{`
` ThreadState::current()->registerPreFinalizer(this);`
`}`
`void MyObject::dispose()`
`{`
` m_binding.Close();`
`}`
