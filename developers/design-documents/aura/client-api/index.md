# Client API

The Aura Client API is an API Aura uses to communicate with the client
application using the Aura system. Since Aura is very simple, providing just
basic window and event types, there are many details it cannot know about
directly. It delegates some functionality to its client (such as window
stacking) and also provides the client API as a conduit for users of Aura to
communicate with the embedding environment.

For example, Aura does not implement tooltips directly, these are a feature
implemented by an client application. The Client API has knowledge of tooltips
however so that a system like Views (which depends on Aura) can specify tooltip
parameters. The client application can interpret this data and show the relevant
UI.

The Aura Window class supports opaque properties, and a list of well known keys
is provided as part of the Client API.

In the ChromeOS Desktop Window Manager, the Aura Client is a component known as
the "Aura Shell" or "Ash":

![image](ClientAPI.png)

A breakdown of responsibility between Aura and the Aura Shell (as implemented
for ChromeOS):

### Aura

1.  Basic window type and hierarchy
2.  Event dispatch
3.  Compositor integration
4.  Client API

### Aura Shell

1.  "Window Manager" features like:
    1.  Window dragging and resizing
    2.  Transient and modal windows
    3.  Maximize/Restore/Full Screen
    4.  Tooltips
    5.  Drag and drop
    6.  Window embellishments like shadows
    7.  Window stacking
    8.  Advanced layout heuristics
2.  Shell features like:
    1.  Application launcher(s)
    2.  Customizable desktop background
    3.  Global keyboard shortcuts
    4.  Login/lock screens, screen savers, etc.

The Aura Shell implements the Aura Client API, and uses Aura and Views to
achieve all of this.

In the diagram above, I show a second Aura Client API implementation. This is
what we will end up with when we transition Desktop Chrome on Windows to use the
Aura system, sometime in 2012. This will allow us to hardware accelerate our UI.

With this in mind, it is important to keep the Aura layer of the framework clean
and simple, since much of the ChromeOS DWM functionality and feature set is not
relevant to desktop Chrome, just the basic windowing primitives.
