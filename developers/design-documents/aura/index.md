# Aura

[Original One-Page Design Doc](../aura-desktop-window-manager/index.md) (still
good for a high level overview, with contact information. Many technical details
are now obsolete)

[Aura Graphics Architecture](graphics-architecture/index.md)

[Aura Overview](aura-overview/index.md)

[Aura and Shell dependencies](aura-and-shell-dependencies/index.md)

[Aura Client API and Shell Separation](client-api/index.md)

[Layout Managers](layout-managers/index.md)

[Views](views/index.md)

[Gesture Recognizer](gesture-recognizer/index.md)

[Ash Color Chooser](ash-color-chooser/index.md) (the <input type="color"> UI for
ChromeOS)

[Event Handling](event-handling/index.md)

[Focus and Activation](focus-and-activation.md)

[Multi-desktop](multi-desktop.md)

[System Tray](system-tray.md)

[Immersive fullscreen](../immersive-fullscreen.md) - Part of the Ash window
manager. Collapses the tab strip and shelf into "light bar" strips at the top
and bottom of the screen.
