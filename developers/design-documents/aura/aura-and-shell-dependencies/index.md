# Aura and Shell dependencies

The diagram below shows the dependencies of Chrome, Ash (Aura shell),
views-based widgets, and the Aura windowing subsystem. Note that in many but not
all cases there are delegate interfaces each module can use to query the module
above.

![image](ChromeAshViewsAuradependencies.png)
