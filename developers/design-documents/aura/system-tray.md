# System Tray

## Summary

The system tray is part of the Ash status area. It displays system status and
settings, and expands into a menu with more information and settings controls.
Each system tray item may have any subset of the 4 view types:

*   **Tray view.** This is the view that shows in the status area, such as the
    clock or the Wi-Fi indicator image.
*   **Default view.** This is the view that appears in the list when the tray
    bubble is opened.
*   **Detailed view.** This view appears by itself. If the tray bubble is
    already open, the detailed view is displayed within the existing window for
    the tray bubble.
*   **Notification view.** Obsolete. The SMS notification is shown as a
    notification view. Other tray items use the message center or their detailed
    view to show notifications.

This document summarizes the behavior of each system tray item. Some system tray
items are only available while logged in.

## List of system tray items

System tray item Tray view Default view Detailed view Notifications Chrome OS
only Visible in sign-in screen, lock screen, OOBE **ScreenCaptureTrayItem** Yes
Yes No Message center Yes No? **ScreenShareTrayItem** Yes Yes No Message center
Yes No? **TrayAccessibility** When accessible option is enabled Yes, unless
disabled in settings Yes (from default view) Custom popup No (Windows, Linux)
Yes **TrayAudio** When muted Yes Pops up when volume changes Detailed view popup
Yes **TrayAudioChromeOs** When muted Yes Pops up when volume changes Detailed view popup Yes Yes **TrayAudioWin** No Yes Yes No No (Windows) N/A **TrayBluetooth** No Yes Yes (from default view) No No (Linux) Yes **TrayBrightness** No In TouchView Pops up when brightness changes Detailed view popup Yes Yes **TrayCapsLock** When enabled When enabled Pops up when caps lock is enabled Detailed view popup Yes Yes **TrayDate** Yes Yes No No No (Windows, Linux) Yes **TrayDisplay** No When display changes or is mirrored, extended or docked No Message center Yes Yes **TrayDrive** No For the owner when waiting on Drive Yes (from default view) No No (Linux) No **TrayEnterprise** No When device is managed (non-public user) No No Yes Yes **TrayIME** If multiple input methods are enabled If multiple input methods are enabled Yes (from default view) No No (Linux) Yes **TrayLocallyManagedUser** No For locally managed user No Message center Yes No **TrayNetwork** Sometimes Yes, once initialized Yes (from default view) Detailed view popup or message center Yes Yes **TrayPower** If battery present No No (part of TraySettings) Message centerYes Yes **TrayRotationLock** In TouchView In TouchView No No Yes Yes **TraySessionLengthLimit** When limited No No Message center Yes No **TraySettings** No Yes No No Yes Sometimes **TraySms** When messages available When messages available When messages available Notification view Yes ? **TrayTracing** When tracing is enabled When tracing is enabled No No Yes Yes **TrayUpdate** When upgrade is available When upgrade is available Yes (pops up periodically) Detailed view popup No (Windows, Linux) Yes **TrayUser**
Sometimes Yes Yes (new account management) No Yes No **TrayUserSeparator** No
When multiple users logged in No No Yes No **TrayVPN** No If VPN configured Yes
(from default view) No Yes Lock screen
