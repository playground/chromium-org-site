# Using Eclipse with Chromium

*   Linux:
    <https://chromium.googlesource.com/chromium/src/+/master/docs/linux_eclipse_dev.md>
*   Android:
    <https://chromium.googlesource.com/chromium/src.git/+/master/docs/eclipse.md>
