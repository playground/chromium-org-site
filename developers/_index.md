# For Developers

#### *See also: docs in the source code - **<https://chromium.googlesource.com/chromium/src/+/master/docs/README.md>*

### Start here

*   [Get the Code: Checkout, Build, &
    Run](https://www.chromium.org/developers/how-tos/get-the-code)
*   [Contributing code](https://www.chromium.org/developers/contributing-code)

### How-tos

#### *Note that some of these guides are out-of-date.*

#### Getting the Code

*   [Quick reference](quick-reference.md) of common development commands.
*   Look at our [Git
    Cookbook](https://chromium.googlesource.com/chromium/src/+/master/docs/git_cookbook.md)
    for a helpful walk-through, or the [Fast Intro to Git
    Internals](fast-intro-to-git-internals.md) for a background intro to git.
*   [Changelogs for Chromium and Blink](change-logs.md).

#### Development Guides

*   Debugging on [Windows](how-tos/debugging-on-windows/index.md), [Mac OS
    X](how-tos/debugging-on-os-x/index.md),
    [Linux](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_debugging.md)
    and
    [Android](https://chromium.googlesource.com/chromium/src/+/master/docs/android_debugging_instructions.md).
*   [Threading](https://chromium.googlesource.com/chromium/src/+/master/docs/threading_and_tasks.md)
*   [Subtle Threading Bugs and How to Avoid
    Them](design-documents/threading/suble-threading-bugs-and-patterns-to-avoid-them.md)
*   [Visual Studio tricks](how-tos/visualstudio-tricks.md)
*   [Debugging GPU related code](how-tos/debugging-gpu-related-code.md)
*   [How to set up Visual Studio debugger
    visualizers](how-tos/how-to-set-up-visual-studio-debugger-visualizers.md) to
    make the watch window more convenient
*   [Linux
    Development](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_development.md)
    tips and porting guide
*   [Mac Development](how-tos/mac-development.md)
*   [Generated files](generated-files.md)
*   [Chromoting (Chrome Remote Desktop)
    compilation](https://chromium.googlesource.com/chromium/src/+/master/docs/chromoting_build_instructions.md)
*   [Updating module dependencies](how-tos/chromium-modularization.md)
*   [Editing dictionaries](how-tos/editing-the-spell-checking-dictionaries.md)
*   Editors Guides
    *   [Atom](using-atom-as-your-ide.md)
    *   [Eclipse](using-eclipse-with-chromium.md)
    *   [Emacs cscope](how-tos/cscope-emacs-example-linux-setup/index.md)
    *   [QtCreator](https://chromium.googlesource.com/chromium/src/+/master/docs/qtcreator.md)
    *   [SlickEdit](slickedit-editor-notes.md)
    *   [Sublime Text](sublime-text/index.md)
    *   [Visual Studio
        Code](https://chromium.googlesource.com/chromium/src/+/master/docs/vscode.md)
*   [Learning your way around the code](learning-your-way-around-the-code.md)
*   [Guide to Important Libraries, Abstractions, and Data
    Structures](libraries-guide.md)
    *   [Important Abstractions and Data
        Structures](coding-style/important-abstractions-and-data-structures.md)
    *   [Smart Pointer Guidelines](smart-pointer-guidelines.md)
    *   [String usage](chromium-string-usage.md)
*   [Android WebView](androidwebview/index.md)
*   [GitHub Collaboration](github-collaboration.md)

See also: All [How-tos](how-tos/index.md).

### Blink

*   [Blink Project](http://www.chromium.org/blink)
    *   [Binding Team](../teams/binding-team.md)
    *   [Layout Team](../teams/layout-team/index.md)
    *   [Memory Team](../blink/memory-team.md)
    *   [Paint Team](../teams/paint-team/index.md)
    *   [Style Team](../teams/style-team.md)
    *   [Animation Team](../blink/animation-team/index.md)
    *   [Input Team](../teams/index.md)
*   [Running and Debugging the Blink layout
    tests](testing/webkit-layout-tests/index.md)
*   [Blink Sheriffing](../blink/sheriffing/index.md)
*   [Web IDL interfaces](web-idl-interfaces.md)
*   [Class Diagram: Blink Core to Chrome
    Browser](class-diagram-webkit-webcore-to-chrome-browser.md)
*   [Rebaselining Tool](http://code.google.com/p/chromium/wiki/RebaseliningTool)
*   [How repaint
    works](https://docs.google.com/a/chromium.org/document/d/1jxbw-g65ox8BVtPUZajcTvzqNcm5fFnxdi4wbKq-QlY/edit)
*   [Phases of
    Rendering](https://docs.google.com/a/chromium.org/document/d/1UkxPz9GDQXLBZcbw5OeUQpk1VIq_BKhm6BGvWJ5mKdU/edit)
*   [Blink, Testing, and the W3C](../blink/blink-testing-and-the-w3c.md)
*   [Baseline computation and some line layout
    docs](https://docs.google.com/a/chromium.org/document/d/1OP49xbB-D7A0qKNAwFTOfbDL-1dYxu74Jp38ZKAS6kk/edit)
*   [Fast Text Autosizer](http://tinyurl.com/fasttextautosizer)
*   [Debugging with
    ScopedLogger](https://chromium.googlesource.com/chromium/src/+/master/third_party/WebKit/Source/wtf/ScopedLogger.md)

### Testing and Infrastructure

*   [Tests](testing/index.md)
    *   [Tour of the Chromium Buildbot
        Waterfall](testing/chromium-build-infrastructure/tour-of-the-chromium-buildbot/index.md)
    *   [Getting the buildbot
        source](testing/chromium-build-infrastructure/getting-the-buildbot-source/index.md)
    *   [WebKit Layout Tests](testing/webkit-layout-tests/index.md)
    *   [Flakiness Dashboard HOWTO](testing/flakiness-dashboard/index.md)
    *   [Frame Rate Test](testing/frame-rate-test.md)
    *   [GPU Testing](testing/gpu-testing/index.md)
    *   [GPU Recipe](../system/errors/NodeNotFound)
    *   [WebGL Conformance Tests](testing/webgl-conformance-tests.md)
    *   [Blink, Testing, and the W3C](../blink/blink-testing-and-the-w3c.md)
    *   [The JSON Results format](the-json-test-results-format.md)
*   [Browser Tests](testing/browser-tests.md)
*   [Handling a failing
    test](tree-sheriffs/sheriff-details-chromium/handling-a-failing-test.md)
*   [Running Chrome
    tests](http://code.google.com/p/chromium/wiki/RunningChromeUITests)
*   [Reliability Tests](how-tos/reliability-tests/index.md)
*   [Using Valgrind](../system/errors/NodeNotFound)
*   [Page Heap for Chrome](testing/page-heap-for-chrome.md)
*   [Establishing Blame for Memory usage via Memory_Watcher](memory_watcher.md)
*   [GPU Rendering Benchmarks](design-documents/rendering-benchmarks.md)
*   [Infra
    documentation](https://chromium.googlesource.com/infra/infra/+/master/doc/index.md)
*   [Contacting a
    Trooper](https://chromium.googlesource.com/infra/infra/+/master/doc/users/contacting_troopers.md)

### Performance

*   [Adding Performance Tests](testing/adding-performance-tests/index.md)
*   [Telemetry: Performance testing framework](telemetry/index.md)
    *   [Cluster Telemetry](cluster-telemetry.md): Run benchmarks against the
        top 10k web pages
*   [Memory](../Home/memory.md)
*   [Profiling Tools](profiling-chromium-and-webkit.md):
    *   [Thread and Task Profiling and Tracking](threaded-task-tracking.md)
        (about:profiler)
        Also allows diagnosing per-task heap usage and churn if Chrome runs with
        "--enable-heap-profiling=task-profiler".
    *   [Tracing tool](how-tos/trace-event-profiling-tool/index.md)
        (about:tracing)
    *   [Deep Memory Profiler](deep-memory-profiler/index.md)
    *   Investigating [Windows Binary Sizes](windows-binary-sizes.md)
*   [Perf
    Sheriffing](http://www.chromium.org/developers/tree-sheriffs/perf-sheriffs)
*   [Optimizing Energy Consumption](how-tos/optimizing-energy-consumption.md)
*   [Speed Hall of Fame](speed-hall-of-fame.md)

### Sync

*   [Sync overview](design-documents/sync/index.md)
*   [Sync diagnostics](sync-diagnostics.md)
*   [Syncable Service API](design-documents/sync/syncable-service-api/index.md)

### **Diagnostics**

*   [Diagnostics](diagnostics/index.md)

### Documentation hosted in / generated by source code

*   [depot_tools](http://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools.html)
*   [C++ use in Chromium](http://chromium-cpp.appspot.com/)
*   [GN](https://chromium.googlesource.com/chromium/src/+/master/tools/gn/README.md):
    Meta-build system that generates NinjaBuild files; Intended to be GYP
    replacement.
*   [MB](https://chromium.googlesource.com/chromium/src/+/master/tools/mb#):
    Meta-build wrapper around both GN and GYP.
*   [Chrome
    Infra](https://chromium.googlesource.com/infra/infra/+/master/doc/index.md)

### Practices

*   [Core Product Principles](core-principles/index.md)
    *   [No Hidden Preferences](core-principles/no-hidden-preferences.md)
*   [Contributing code](contributing-code/index.md)
    *   [Coding style](coding-style/index.md)
        *   [C++ Dos and Don'ts](coding-style/cpp-dos-and-donts.md)
        *   [Cocoa Dos and Don'ts](coding-style/cocoa-dos-and-donts.md)
        *   [Web Development Style Guide](web-development-style-guide.md)
        *   [Java](coding-style/java.md)
        *   [Jinja](jinja.md)
    *   [Becoming a Committer](../getting-involved/become-a-committer.md)
    *   [Gerrit Guide (Googler/Non-Googler)](gerrit-guide.md)
    *   [Create experimental branches to work on](experimental-branches.md)
    *   [Committer's responsibility](committers-responsibility.md)
    *   [OWNERS
        Files](https://chromium.googlesource.com/chromium/src/+/master/docs/code_reviews.md)
    *   [Try server usage](testing/try-server-usage/index.md)
    *   [Commit queue](testing/commit-queue/index.md)
    *   [Tips for minimizing code review lag across
        timezones](contributing-code/minimizing-review-lag-across-time-zones.md)
*   [Declare your intent to launch/change a feature](new-features.md)
*   [Filing bugs](../for-testers/bug-reporting-guidelines/index.md)
    *   [Severity Guidelines for Security Issues](severity-guidelines.md)
*   [Network bug
    triage](design-documents/network-stack/network-bug-triage_index.md)
*   [GPU bug
    triage](https://docs.google.com/document/d/1Sr1rUl2a5_RBCkLtxfx4qE-xUIJfYraISdSz_I6Ft38/edit#heading=h.vo10gbuchnj4)
*   [The Zen of Merge Requests](the-zen-of-merge-requests.md)
*   [Ticket milestone
    punting](ticket-milestone-punting/ticket-milestone-punting-1.md)
*   [Tree Sheriffs](tree-sheriffs/index.md)
*   [Useful extensions for developers](useful-extensions.md)
*   [Adding 3rd-party libraries](adding-3rd-party-libraries.md)

Design documents

*   [Getting around the source code
    directories](how-tos/getting-around-the-chrome-source-code/index.md)
*   [Tech Talks: Videos & Presentations](index.md)
*   [Engineering design docs](design-documents/index.md)
*   [User experience design docs](../user-experience/index.md)
*   *Sharing design documents on Google drive: share on Chromium domain*
    If on private domain, share with self@chromium.org, then log in with
    self@chromium.org, click "Shared with Me", right-click "Make a copy", then
    set the permissions: "Public on the web" or "Anyone with the link",
    generally "Can comment". It a good idea to then mark your local copy
    *(PRIVATE)* and only edit the public copy.

### Communication

*   [General discussion groups](discussion-groups/index.md)
*   [Technical discussion groups](technical-discussion-groups.md)
*   [IRC](irc.md)
*   [Development calendar and release info](calendar/index.md)
*   [Common Terms & Techno
    Babble](http://code.google.com/p/chromium/wiki/Glossary)
*   [Making a web standards proposal](how-tos/make-a-web-standards-proposal.md)

### Status

*   [Status Update Email Best Practices](status-update-email-best-practices.md)
*   [chromestatus.com](http://chromestatus.com)

Usage statistics

*   [MD5 certificate statistics](md5-certificate-statistics.md)

### Graphics

*   [Graphics overview and design
    docs](design-documents/chromium-graphics/index.md)

### External links

*   Waterfalls
    *   [Continuous build](http://build.chromium.org/p/chromium/waterfall)
        ([console](http://build.chromium.org/p/chromium/console))
    *   [Memory](http://build.chromium.org/p/chromium.memory/waterfall)
        ([console](http://build.chromium.org/p/chromium.memory/console))
    *   [For Your Information
        build](http://build.chromium.org/p/chromium.fyi/waterfall)
        ([console](http://build.chromium.org/p/chromium.fyi/console))
    *   [Try
        Server](http://build.chromium.org/p/tryserver.chromium.linux/waterfall)
*   [Build Log Archives
    (chromium-build-logs)](http://chromium-build-logs.appspot.com/)
*   [Bug tracker](http://code.google.com/p/chromium/issues/list)
*   [Code review tool](http://codereview.chromium.org/)
*   [Viewing the source](https://chromium.googlesource.com/chromium/src/)
*   [GYP (Generate Your Projects) Project Page](http://code.google.com/p/gyp/)
*   [Glossary](http://code.google.com/p/chromium/wiki/Glossary) (acronyms,
    abbreviations, jargon, and technobabble)
