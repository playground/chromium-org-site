# Memory

**Where am I?**

This is a landing page for all things related to Chrome memory usage.

**Current, active fires**

*   none.

**How are we doing?**

*   \[ link to stats or landing pages. may be internal only. \]

**Who is doing what?**

*   project 1
*   project 2
*   project 3

**I've found a problem...now what?**

*   **link to tools**
    *   [memory-infra](https://chromium.googlesource.com/chromium/src/+/master/docs/memory-infra/README.md):
        How to explore memory with chrome://tracing, good for local
        investigation.
*   **link to bug filing**
*   **link to self diagnostics, etc**

**Teams**

*   [go/memory-infra](http://go/memory-infra): primiano@, perezju@, kraynov@,
    hjd@

**How do I help?**

*   **Link to bugs?**
*   **Link to team pages?**
*   **General advice on life?**
