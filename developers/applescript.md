# Information for Third-party Applications on Mac

JavaScript injection via Applescript will soon be disabled by default in Chrome
stable. We’re making this change in order to protect users from disruptive ads.
Applications that legitimately need to inject JavaScript into Chrome can do so
by publishing an [extension](https://developer.chrome.com/extensions) and using
Native Messaging to communicate with their application process. For more
information, see <https://developer.chrome.com/extensions/nativeMessaging>

For automation, developers can also use Telemetry:

<https://catapult.gsrc.io/telemetry>
