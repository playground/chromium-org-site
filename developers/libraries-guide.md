# Libraries Guide

## Chromium base libraries

*   [Important Abstractions and Data
    Structures](coding-style/important-abstractions-and-data-structures.md)
*   [Smart Pointer Guidelines](smart-pointer-guidelines.md)
*   [Chromium String usage](chromium-string-usage.md)

## Blink's WTF library

*   [Strings in
    Blink](https://docs.google.com/document/d/1kOCUlJdh2WJMJGDf-WoEQhmnjKLaOYRbiHz5TiGJl14/edit#)
