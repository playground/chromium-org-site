# How-Tos

*   Build instructions: [Windows](build-instructions-windows.md), [Mac OS
    X](https://chromium.googlesource.com/chromium/src/+show/master/docs/mac_build_instructions.md),
    [Linux](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_build_instructions.md),
    [ChromeOS](http://www.chromium.org/developers/how-tos/build-instructions-chromeos),
    [Android](https://chromium.googlesource.com/chromium/src/+/master/docs/android_build_instructions.md),
    and
    [iOS](https://chromium.googlesource.com/chromium/src/+show/master/docs/ios_build_instructions.md)
*   Debugging instructions: [Windows](debugging-on-windows/index.md), [Mac OS
    X](http://www.chromium.org/developers/how-tos/debugging-on-os-x),
    [Linux](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_debugging.md)
    and
    [Android](https://chromium.googlesource.com/chromium/src/+/master/docs/android_debugging_instructions.md)
*   [Running Chrome
    tests](http://code.google.com/p/chromium/wiki/RunningChromeUITests)
*   [Linux Development](http://code.google.com/p/chromium/wiki/LinuxDevelopment)
    tips and porting guide
*   [Using Git](http://code.google.com/p/chromium/wiki/UsingGit) for version
    control and code reviews

For more, see this list of sub-pages:
