# Run Chromium with flags

There are command line flags (or "switches") that Chromium (and Chrome) accept
in order to enable particular features or modify otherwise default
functionality.

Current switches may be found at
<http://peter.sh/examples/?/chromium-switches.html>

It is important to note that using these switches is not supported or
recommended. They should only be used for temporary cases and may break in the
future.

To use a command line switch:

### On Windows:

1.  Right click on your "Chrome" icon.
2.  Choose properties
3.  At the end of your target line add the command line flag. For example:
    *   `--disable-gpu-vsync`
4.  With that example flag, it should look like: `chrome.exe
    `--disable-gpu-vsync

### On OS X:

    /Applications/Chromium.app/Contents/MacOS/Chromium --remote-debugging-port=9222
    # for Google Chrome you'll need to escape spaces like so: 
    /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --remote-debugging-port=9222

### On Linux:

    chromium-browser --remote-debugging-port=9222

## Setting V8 Flags

V8 can take a number of flags as well, via Chrome's `js-flags` flag. For
example, this traces V8 optimizations:

    chrome.exe --js-flags="--trace-opt --trace-deopt --trace-bailout"

To get a listing of all possible V8 flags:

    chrome.exe --js-flags="--help"

Browse [the V8 wiki](http://code.google.com/p/v8/w/list) for more flags for V8.

### Setting Flags for Chrome on Android

Visit '`about:version`' to review the flags that are effective in the app.

Setting flags for Chrome on Android requires **a rooted device**, as it
necessitates write access to `/data/local`

You can change these like so:

    # For content shell
    adb shell 'echo "chrome <flags>" > /data/local/tmp/content-shell-command-line'
     # For chromium test shell
    adb shell 'echo "chrome <flags>" > /data/local/tmp/android-webview-command-line'
    # On a chromium checkout, the following utilities can be used:
    # For chrome public, or Google Chrome
    adb_chrome_public_command_line <flags>
    # For content shell:
    adb_content_shell_command_line <flags>
    # For chromium test shell:
    adb_android_webview_command_line <flags>
    # Or, you can launch content shell with flags directly
    abd_run_content_shell <flags>
    adb_run_android_webview <flags>

Remember to quit the app completely and restart for flags to take effect.

### Setting flags for ContentShell no Android

There's an alternative method for setting flags with ContentShell that doesn't
require a rooted device:

1. Download a [LKGR build of
Android](https://download-chromium.appspot.com/?platform=Android&type=continuous).

2. This will include both ChromePublic.apk and ContentShell.apk

3. Install ContentShell APK to your device.

4. Run this magic incantation

    adb shell am start \
      -a android.intent.action.VIEW \
      -n org.chromium.content_shell_apk/.ContentShellActivity \
      --es activeUrl "http://chromium.org" \
      --esa commandLineArgs --show-paint-rects,--show-property-changed-rects

This will launch contentshell with the supplied flags. You can apply whatever
commandLineArgs you want in that syntax.

### Setting Flags for Chrome OS

1.  Put the device into dev mode so you can get a root shell
2.  Modify /etc/chrome_dev.conf (read the comments in the file for more details)
3.  Restart the UI via:
    `sudo restart ui`
