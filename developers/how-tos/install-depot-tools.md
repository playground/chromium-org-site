# Install depot_tools

Chromium and Chromium OS use a package of scripts called
[**depot_tools**](depottools/index.md) to manage checkouts and code reviews.

The depot_tools package includes `gclient`, `gcl`, `git-cl`, `repo`, and others.

## Installing on Linux and Mac

1.  Confirm `git` and python are installed. git 2.2.1+ recommended. python 2.7+
    recommended.
2.  Fetch depot_tools:

        $ git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git

3.  Add `depot_tools` to your [PATH](http://www.lmgtfy.com/?q=PATH+environment):

        $ export PATH=`pwd`/depot_tools:"$PATH"

    *   Yes, you want to put depot_tools ahead of everything else, otherwise gcl
        will refer to the GNU Common Lisp compiler.
    *   You may want to add this to your `.bashrc` file or your shell's
        equivalent so that you don’t need to reset your $PATH manually each time
        you open a new shell.

### Installing on Windows

### Preamble

Chromium is mostly designed to be run using the native Windows tools and the[
Msys (Git for Windows)](http://msysgit.github.io/) toolchain. Cygwin is not
recommended, and likely things will fail in cryptic ways.

### Instructions

1.  Download
    [depot_tools.zip](https://storage.googleapis.com/chrome-infra/depot_tools.zip)
    and decompress it.
    *   **Do not use drag-n-drop or copy-n-paste extract **from Explorer, this
        will not extract the hidden ".git" folder which is necessary for
        depot_tools to autoupdate itself. You can use "Extract all..." from the
        context menu though.
    *   **Do not extract to a path containing spaces**. If you do, gclient will
        produce the error "update_depot_tools.bat was not expected at this time"
        or similar.
2.  Add `depot_tools` to the **start** (not end!) of your PATH:
    *   With Administrator access:
        *   Control Panel > System and Security > System > Advanced system
            settings
        *   Modify the PATH system variable to include depot_tools
    *   Without Administrator access:
        *   Control Panel > User Accounts > User Accounts > Change my
            environment variables
        *   Add a PATH user variable: C:\\path\\to\\depot_tools;%PATH%
3.  Run `gclient` **from the cmd**** shell**. The first time it is run, it will
    install its own copy of various tools. If you run gclient from a non-cmd
    shell, it may appear to run properly, but python, and other tools may not
    get installed correctly (while it should work fine from a msys bash shell,
    you may still trip over bugs from time to time).
    *   If you see strange errors with the file system on the first run of
        gclient, you may want to [disable Windows
        Indexing](http://tortoisesvn.tigris.org/faq.html#cantmove2).
    *   If you see errors like "The system cannot execute the specified
        program", try [installing "Microsoft Visual C++ 2008 Redistributable
        Package"](http://code.google.com/p/chromium/issues/detail?id=75886).
    *   If it complains that it can't find python, make sure you don't already
        have a .gclient file in the same directory.
    *   After running gclient open a command prompt and type where python and
        confirm that the depot_tools python.bat comes ahead of any copies of
        python.exe. Failing to ensure this can lead to overbuilding when using
        gn - see [crbug.com/611087](http://crbug.com/611087)

See also [Howto: depot tools](depottools/index.md)
