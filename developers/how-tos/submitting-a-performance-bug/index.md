# Submitting a Performance Bug

**These instructions are relevant for Windows, Mac, or Linux computers. **For
Android and more, click for [advanced
instructions](../trace-event-profiling-tool/recording-tracing-runs/index.md).

**Note: Uploading a trace to Google may share personal information such as the
titles and URLs of open tabs.**

1. In the address bar of a new tab, type **chrome://tracing**

2. In the upper left, press the **Record** button.

![image](Screenshot%20from%202015-03-10%2014%3A52%3A09.png)

3. In the dialog that opens, select **Manually select settings**

![image](Screenshot%20from%202015-03-24%2011%3A16%3A39.png)

4. Under **Record Categories**, click **All**.

5. In the lower right, click **Record**.

6. Complete whatever action reproduces the performance issue: opening a new tab,
navigating to a certain website, scrolling a page, etc. If possible, the
duration of your recording should be about 10 seconds or less.

7. Return to the tracing tab and press **Stop**.

![image](Screenshot%20from%202015-03-10%2014%3A54%3A06.png)

8. When the recording has been imported, click **Save** at the top of the
screen, then choose where to save it on your computer.

![image](Screenshot%20from%202015-03-10%2014%3A55%3A28.png)

9. File a [new performance
bug](https://code.google.com/p/chromium/issues/entry?summary=Performance+issue:&comment=Chrome+Version+++++++%3A%0AOperating+System+and+Version%3A+%0AURLs+(if+applicable)+%3A%0A%0ADescription+of+performance+problem:%0A%0A%0A%0ARemember%20to%20attach%20your%20trace%20file%20to%20this%20bug!&labels=Type-Bug,Pri-2,Hotlist-Slow,Performance&cc=rschoen@chromium.org&).
Make sure to add a descriptive title, your Chrome version, your operating system
and version, URLs (if applicable), and details about your issue.

10. Click **Attach a file** and locate the trace file you saved in step 7. There
is a 10MB limit, so you may need to compress the file first.

11. In the bottom left, click **Submit Issue**. Thank you!
