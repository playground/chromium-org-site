# Building with Ninja, Debugging with Xcode

You want to [build Chromium with
ninja](https://code.google.com/p/chromium/wiki/Clang). Trust me. But you
appreciate the graphical debugger of Xcode. What to do? The secret is that you
don't need to build with Xcode to use the Xcode debugger. Here are some
scenarios, in increasing order of difficulty, that you might find yourself in.

## Yikes, Chromium crashes when I do *\[insert action\]*!

This is the simplest scenario. The key here is that Xcode allows you to attach
to *any* process, even if you don't have the project for that process open. The
key here is to have *a* project open. So:

1.  Select File > New > Project... and make a new project. Dump it anywhere,
    call it anything. It doesn't matter.
2.  Launch Chromium.
3.  In Xcode, select Debug > Attach to Process > Chromium.
4.  Switch to Chromium, do *\[insert action\]*.
5.  The Xcode debugger will come to the front. Proceed to debug.

## But I want to catch Chromium before it crashes!

That's a bit tougher, because the Xcode debugger has issues dropping breakpoints
on executables it doesn't know about, so tying 'b' in the debugger pane won't
work. In this case, you'll need the Xcode project for what you're debugging. To
generate them, pass --ide=xcode to gn gen. Then:

1.  Open the most specific .xcodeproj file that contains the source file in
    which you want to put a breakpoint.
2.  Find the file in the file list on the left, apply your desired breakpoints.
3.  Launch Chromium.
4.  In Xcode, select Debug > Attach to Process > Chromium.
5.  Switch to Chromium, do whatever triggers the breakpoint.
6.  The Xcode debugger will come to the front. Proceed to debug.

## But Chromium crashes on launch before I can attach to it!

This is the trickiest scenario, because you need to launch Chromium from within
Xcode without allowing Xcode to build. I haven't yet figured out how to convince
Xcode to do that; Product > Perform Action > Run Without Building always opens a
dialog complaining that it hasn't built Chrome.
