# Inspecting Ash with DevTools

Ash can now be 'inspected' just like a webpage, using DevTools. This is
accomplished by re-using the existing frontend DevTools inspector and creating a
backend in Ash which interacts using the DevTools protocol language.

The design doc for this can be found
[here](https://docs.google.com/document/d/1zpXnSLFrTbLRBJNnO2lWXV--nTOUWfBLPXwA9BDEsKA/edit?usp=sharing).
An example of what the hierarchy looks like:

And,
[here](https://docs.google.com/presentation/d/1q3RBp-QEIx5snjbi3_FNl1pp8wf74DXFpt_NgT0KMB0/edit?usp=sharing)
is a slideshow containing numerous GIFs showing what this is like to use.

**Current Features**

*   View the window/widget/view hierarchy (as seen above)
*   Selecting nodes in the inspector displays their attributes (height, width,
    x, y) in the CSS side panel
*   Attributes can be edited right from the CSS side panel.
*   Any changes in the tree (addition/removal/rearranging of elements) in Ash
    will be reflected in the inspector
*   Hovering over elements in the inspector highlights them in Ash

**Planned Features**

*   Any animations initiated in Ash are displayed in the inspector under the
    "Animations" tab and can be replayed
*   Hovering over elements in Ash highlights and expands them in the inspector

**Instructions**

1.  Run Chromium with the UI DevTools flag:
    $ chrome.exe --enable-ui-devtools=<port>
2.  In your Chrome browser on Ash, visit `chrome://inspect#other`
3.  Click `inspect `Ash in the listing. This will open up the inspector in a new
    tab.

If you wish to debug remotely, simply open
***chrome-devtools://devtools/bundled/inspector.html?ws=localhost:<port>/0*** in
your Chrome browser (the 0 stands for the first inspect-able component which is
*Ash, *for now).
