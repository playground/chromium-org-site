# API Keys

Speech API:
** It is NOT possible to get additional quota for Chrome's Speech API.** Look at
the [Cloud Speech API](https://cloud.google.com/speech/) instead.** Do NOT post
to any Chromium groups/mailing lists for questions about the Speech API. ** This
page is about building Chromium. If you have arrived on this page to try to get
API keys for some other purpose, you should **not** follow the instructions on
this page. **Do NOT** post to any Chromium groups/mailing lists to ask how to
get quota for an API. The section "Getting Keys for Your Chromium Derivative"
below has the same answer you will get from the list.

Some features of Chromium use Google APIs, and to access those APIs, either an
API Key or a set of OAuth 2.0 tokens is required. Setting up API keys is
optional. If you don't do it, the specific APIs using Google services won't work
in your custom build, but all other features will run normally.

**Googlers only**: if you need a new API enabled in chrome, use
<http://b/new?component=165132>

If you build Chromium and Chromium OS yourself setting up API keys is mandatory
so that it will allow user login on Chromebooks!
You can specify the API keys to use either when you build Chromium, or at
runtime using environment variables. First, you need to acquire keys for
yourself.

Software distribution with keys acquired for yourself is allowed, but the keys
themselves cannot be shared with parties outside the legal entity that accepted
the API ToS. Keep in mind that a number of the APIs will have no or very limited
quota and not all of the APIs have additional quota available for purchase.

### Acquiring Keys

1.  Make sure you are a member of
    [chromium-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/?fromgroups#!forum/chromium-dev)
    (you can just
    [subscribe](https://groups.google.com/a/chromium.org/forum/?fromgroups#!forum/chromium-dev)
    to chromium-dev and choose not to receive mail). For convenience, the APIs
    below are only visible to people subscribed to that group.
2.  Make sure you are logged in with the Google account associated with the
    email address that you used to subscribe to chromium-dev.
3.  Go to <https://cloud.google.com/console>
4.  Find the **Create Project** button (e.g. click the hamburger menu at the top
    left, then** Home**, then **Create a Project**)
5.  (Optional) You may add other members of your organization or team on the
    Team tab.
6.  Open the **API Manager > Library **from the hamburger menu, search for all
    of the following APIs. If you're a member of the chromeos-dev Google group
    you should see all of them. For each of these APIs click on them when found
    by the search, and then click on "Enable API" button at the top, read and
    agree to the Terms of Service that is shown, check the "I have read and
    agree to <API name> Terms of Service" checkbox and click Accept: *(This list
    might be out of date; try searching for APIs starting with "Chrome" or
    having "for Chrome" in the name.)*
    *   Calendar API
    *   Contacts API
    *   Drive API (Optional, enable this for Files.app on Chrome OS and
        SyncFileSystem API)
    *   Chrome Remote Desktop API
    *   Chrome Spelling API
    *   Chrome Suggest API
    *   Chrome Sync API
    *   Chrome Translate Element
    *   Chrome Web Store API
    *   Chrome OS Hardware ID API (Optional, Chrome OS)
    *   Device Registration API (Optional, Chrome OS)
    *   Google Cloud DNS API
    *   Google Cloud Storage
    *   Google Cloud Storage JSON API
    *   Google Maps Geolocation API (requires [enabling
        billing](https://developers.google.com/console/help/#EnableBilling) but
        is free to use; you can skip this one, in which case geolocation
        features of Chrome will not work)
    *   Google Maps Time Zone API
    *   Google Now For Chrome API (Optional, enabled to show Google Now cards)
    *   Google+ API
    *   Nearby Messages API
    *   Safe Browsing API
    *   Speech API **(See the "Speech API" box at the top of the page)**

    ***If any of these APIs are not shown, recheck step 1.** *

1.  Go to the **Credentials** sub tab under the **API Manager**.
2.  Click the "**Create credentials**" button then click on the **OAuth client
    ID** item in the drop-down list.
    *   Click on the "Configure consent screen" button. Fill in the "Product
        name" (name it anything you want) and other details if you have
        available then click on "Save" at the bottom.
    *   Return to the Credentials tab and click the "Add credentials" button
        again, then select "OAuth 2.0 client ID" from the drop-down list.
    *   In the "Application type" section check the "Other" option and give it a
        name in the "Name" text box, then click "Create"
3.  In the pop-up window that appears you'll see a **client ID **and a "**client
    secret**" string. Copy and paste those in a text file on your dev box then
    click OK to dismiss it.
    *   A new item should now appear in the "OAuth 2.0 client IDs" list. You can
        click on the name of your client id to retrieve the ID and secret at any
        time. In the next sections, we will refer to the values of the “Client
        ID” and “Client secret” fields.
4.  Click the "**Create credentials**" button *again *on the same page.

    *   In the pop-over window that shows up click the "**API key**" button.
    *   A pop-over should show up giving you the API key. Copy and paste it in a
        text file to save it, although you can access it later as well.
    *   Click OK to dismiss this.

You should now have an API key and a OAuth 2.0 client ID in on the Credentials
tab. The next sections will refer to the value of the “API key” field too.

2.  Note that the keys you have now acquired **are not for distribution purposes
    and must not be shared with other users.**

1.  Providing Keys at Build Time

If you are building Chromium yourself, you can provide keys as part of your
build configuration, that way they are always baked into your binary.

Specify three variables in your args.gn file (which you can edit by running `gn
args out/your_out_dir_here`)

    google_api_key = "your_api_key"
    google_default_client_id = "your_client_id"
    google_default_client_secret = "your_client_secret"

**Providing Keys at Runtime**

If you prefer, you can build a Chromium binary (or use a pre-built Chromium
binary) without API keys baked in, and instead provide them at runtime. To do
so, set the environment variables GOOGLE_API_KEY, GOOGLE_DEFAULT_CLIENT_ID and
GOOGLE_DEFAULT_CLIENT_SECRET to your "API key", **"Client ID" and **"Client
secret" values respectively.
On Chromium OS to specify the keys as environment variables append them to the
end of /etc/chrome_dev.conf:
GOOGLE_API_KEY=*your_api_key*
GOOGLE_DEFAULT_CLIENT_ID=*your_client_id*
GOOGLE_DEFAULT_CLIENT_SECRET=*your_client_secret*

**Getting Keys for Your Chromium Derivative**

Many of the Google APIs used by Chromium code are specific to Google Chrome and
not intended for use in derived products. In the API Console
(<http://developers.google.com/console>) you may be able to purchase additional
quota for some of the APIs listed above. **For APIs ****that do not have a
"Pricing" link, additional quota is not available for purchase.**
