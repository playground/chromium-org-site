# How to make a web standards proposal

One of the web platform's greatest strengths is that no one company or
organization controls the platform. For developers to use a platform feature,
that feature needs to be present in some critical mass of browsers, which means
the web platform is some rough consensus of what browser vendors ship.

In evolving the web platform, our goal is to help move the whole web forward.
That means we need to collaborate and engage with other browser vendors. In rare
cases, we're willing to act unilaterally when we perceive the benefits are worth
the risks, but those exceptional circumstances are few and far between.

There are many ways to engage with other browser vendors, including meetings,
informal lunches, and IRC chatrooms, but the most transparent and open way is to
engage with other browser vendors and with other stakeholders in the web
platform is through the web standards process. This document explains one
approach to navigating the web standards process that has worked well in the
past. It's written specifically for major features that will require a new spec
or standards proposal.

## Doing your homework

So, you've got an idea for a shiny new web platform feature. Great! That's how
the platform improves. The first step is to do your homework. The web platform
has been around for a while, and there's a fair chance that someone, somewhere
has tried to solve a similar problem before. Before you invest a lot of effort
in developing your idea, you should ask around (or use your favorite search
engine) to see what happened in nearby problem spaces before. Doing your
homework has two benefits:

1.  You'll learn about related problems and solutions, which can help to improve
    your proposal, perhaps by generalizing slightly to cover a use case you
    hadn't originally considered.
2.  If previous attempts to solve this problem weren't successful, you'll learn
    why, which can help you avoid the same sand traps.

One good outcome from this step, actually, is to find a dusty old proposal that
solves the problem you're interested in. When that happens, you can brush the
dust off, tune things up a bit, and you'll likely have found someone else who's
invested in your proposal succeeding.

## Internal vetting

Before marching off to convince the world of the benefits of your feature, it's
now time to vet your proposal internally within the Chromium project. This step
helps us focus our efforts and avoid spamming the world with too many crazy
ideas (instead, we prefer spamming the world with crazy-awesome ideas).

You should prepare a short (one or two page) design doc of what problem you're
trying to solve, what approach you're planning to use, whatever you've learned
by doing your homework, and some examples of your idea making the world a better
place. It's common to use Google Docs for this document because Docs has nice
commenting and collaboration features.

When you're ready, send your design doc to the [web-standards-dev at
chromium.org](https://groups.google.com/a/chromium.org/group/web-standards-dev/topics).
Ideally, one of us on
[web-standards-dev](https://groups.google.com/a/chromium.org/group/web-standards-dev/topics)
will give you feedback on your idea, help you iterate on the design, and let you
know what the next steps are for your feature.

## External vetting

After you've got the Chromium project behind you, it's time to talk with other
browser vendors to get a sense for their level of interest. Often it's helpful
to start by contacting folks individually because that lets you get quick
feedback and refine your pitch. If you know the right folks to contact, go for
it. If you're unsure, please feel encouraged to ask folks on the team who've
done this before for recommendations and introductions.

The key question to ask at this point is not "will you implement this feature?",
which can be hard for someone to answer on the spot, but rather "what do you
think of this approach to solving this problem?" The response you get should
give you a sense for whether these folks hate your idea (and will write tweet
after tweet about how you're single-handedly destroying the web as we know it)
or whether they'll be supportive. At this point, try to avoid getting into
arguments over details of the proposal. (There's plenty of time for that later!)

## Fleshing out the details

Once you've got the lay of the land (and if you still think this bird will fly),
it's time to engaging the wider web standards community. In order to have a
meaningful conversation about your feature, you'll want to have a more detailed
description of your proposal in the form of a specification. Don't worry too
much about being locked into what's in your first draft. Think of this document
as the primordial soup out of which your specification will emerge. As you get
more feedback and implementation experience, you can evolve this document and
shape it into a high quality specification.

At this point, you might want to consider moving your specification to a
vendor-neutral location. One option is to put your specification on the WhatWG
wiki, which has a [category for
proposals](http://wiki.whatwg.org/wiki/Category:Proposals). Another option is to
ask our friends at the W3C to create a [dvcs repository](http://dvcs.w3.org/hg).
If your proposal is destined for the IETF, you can create an
[Internet-Draft](http://www.ietf.org/id-info/). Moving your specification to a
vendor-neutral location signals that you're serious about engaging with the
standards community.

## To the mailing lists!

Proto-specification in hand, it's now time to email a standards mailing list.
It's something of an art to pick the right mailing list. If you're in doubt,
please don't hesitate to ask folks who've been through this process before. The
[whatwg list](http://lists.whatwg.org/htdig.cgi/whatwg-whatwg.org/) or the
[public-webapps list](http://lists.w3.org/Archives/Public/public-webapps/) can
be good places to start, even if the actual standards work will end up in a more
specific forum. After some healthy discussion, you should also consider emailing
[webkit-dev](http://lists.webkit.org/mailman/listinfo/webkit-dev) if your
feature involves changing WebKit. You can find out more about [what's expected
in such emails](http://www.webkit.org/coding/adding-features.html) on the WebKit
web site.

Try to have an open mind. It's true that some folks on these lists are bozos
(people on the Internet are wrong???), but a goodly number of these folks have
valuable experience and insight that you can benefit from. Keep in mind that you
don't have to convince everyone on these mailing lists that your feature is the
best thing since sliced bread. Your goal should be to raise awareness about what
you're up to, gather useful feedback, and find out if anyone is actively opposed
to your proposal. (If some folks are actively opposed to what you're doing,
you'll probably want to circle back to
[web-standards-dev](https://groups.google.com/a/chromium.org/group/web-standards-dev/topics)
for advice.)

## Experimental implementation

Adding new features to Blink is a two-step process. The first step is
experimental implementation (i.e. behind a flag), and the second step is
shipping. The former doesn't require approval, the latter does. Read the [Blink
Launch Process](http://www.chromium.org/blink#launch-process) for details. Once
you've got some discussion going on mailing lists, you should consider sending
an [Intent to
Implement](https://docs.google.com/a/chromium.org/document/d/1vlTlsQKThwaX0-lj_iZbVTzyqY7LioqERU8DK3u3XjI/edit)
and writing an experimental implementation of your feature.

Balancing discussion and implementation of your feature is somewhat tricky and
situation-dependent. Don't hesitate to ask for guidance and feedback, especially
if you feel stuck on either front. The team is behind you, and we can help.

The goal of an experimental implementation is to improve your feature by getting
implementation experience and feedback from web developers. That means you'll
want the freedom to change your implementation based on what you learn. To avoid
getting locked into our initial implementation, you should start by implementing
your feature behind a runtime flag. With this approach, your feature will be
hidden unless someone launches Chrome with a specific command line flag that
enables your feature.

Once you've started implementing, consider creating a [W3C Community
Group](http://www.w3.org/community/) around your feature (if no appropriate
group already exists). That creates a forum that you can use to interact with
other browser vendors and the broader community. A community group also smooths
the transition onto the standards track.

## Standards track

Deciding when to move your specification onto the standards track can be tricky.
Hopefully in this process you've gotten enough traction to interest another
browser vendor in implementing your feature. That's often a sign that it's time
to move your specification on to the standards track because the other
implementor will start giving you lots of feedback as they work through
implementing your specification. Inevitably you'll disagree on some important
aspects of how the feature should work. The standards process can help you
resolve these sorts of disagreements amicably.

Where and when your feature ends up on the standards track is often a result of
timing and politics. There are many hidden reasons (e.g., patents) why folks
will want to steer you in one direction or another. If you feel like you're
getting the runaround from working group chairs, please don't hesitate to ask
for help. Often folks on the team with more experience can guess the hidden
motivations and help decode whatever mixed messages you're getting.

Different standards bodies have different structures for how documents progress.
Once your feature is on the standards track, your working group chair can help
you sort out what comes next.

## Conclusions

Adding a feature to the web platform can be a rewarding process. You have a
chance to make a big impact on a platform used by many developers and many, many
users. Because the web platform is shared between many parties with different
interests, moving the platform forward requires interacting with these
stakeholders and winning them over to your point of view. Adding features to the
platform is part technical, part political but largely a matter of patience and
persistence.

"You can resist an invading army; you cannot resist an idea whose time has
come." -- Victor Hugo

We have folks on the team with a great deal of experience in navigating these
waters. If you're feeling stuck or frustrated, please don't hesitate to reach
out to
[web-standards-dev](https://groups.google.com/a/chromium.org/group/web-standards-dev/topics).
