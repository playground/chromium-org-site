# How to use drover

## What is Drover?

Drover is a tool, included with [depot tools](install-depot-tools.md), that
allows developers (**should be a
[committer](../../getting-involved/become-a-committer.md)**) to rapidly merge
and revert changes on our trunk/branches without any previously checked out
working copy. A typical drover command merge/revert with no conflicts, takes
about 1 minute to run from start to finish.

## Merging Notes

*Merging to a branch and don't know the branch number? The TPM (release manager)
approving the merge will have it. *

After merging to a release branch, try to watch <http://go/stablebuilders> (for
stable branches) and <http://go/branchbuilders> (for dev/beta branches) to make
sure everything's all right.

## Git Drover

`git-drover` is a new drover for working with git repositories. It is not:

*   The way to do all merges/reverts in the "post git migration" world.
    If you are working with a repository that is still svn-based, see the old
    SVN Drover instructions below.
*   The way to do anything that is not a merge of a git commit (e.g. DEPS
    rolls).
    Those changes should follow the normal commit workflow, or use other tools
    made for the job (e.g. `roll-dep`)

### Requirements/ recommendations

*   Note that these instructions assume you have a working depot_tools and
    chromium checkout as described in the [depot_tools
    tutorial](http://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html).
*   You'll need your [git (.gitcookies)
    credentials](https://www.chromium.org/developers/gerrit-guide).
*   You'll also need your chromium.org credentials (for code reviews).
*   Before working with branches, you must gclient sync --with_branch_heads at
    least once to fetch the branches.

### Usage

*   Merge a change to a release branch
    *   git drover --branch <branch_id> --cherry-pick <revision> (e.g. git
        drover --branch 9999 --cherry-pick b5a049)

For merges on Windows and reverts, please see `man git-drover` or the [online
git-drover
doc](http://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/git-drover.html)
for instructions to achieve equivalent functionality using other git and
depot_tools commands.

### **Troubleshooting**

#### Cannot find 'refs/branch-heads/9999'

Follow [the steps for checking out a release
branch](https://www.chromium.org/developers/how-tos/get-the-code/working-with-release-branches)
to sync the branch heads.

#### Not in chromium-committers Gerrit group

If this is the first time you've tried to merge into a branch, you may run into
the following error when trying to git cl land the change:

error: failed to push some refs to
'https://chromium.googlesource.com/a/chromium/src.git'

ERROR:git-retry:Process failure was not known to be transient; terminating with
return code 1

Push failed with exit code 1.

To https://chromium.googlesource.com/a/chromium/src.git

!       HEAD:refs/pending/branch-heads/2311     \[remote rejected\] (prohibited
by Gerrit)

If you see this error, and you are a committer (went through the process
described [here](../../getting-involved/become-a-committer.md)) you may not have
been added to the chromium-committers gerrit group yet (the group update process
lags behind a bit). File a bug with the Infra-Git label, so you can be added to
the group manually.

#### Uploading merge CL after manual edit

If you've started merge with "git drover" and made any manual edit, "git cl
upload" doesn't work to upload the new snapshot. Use "git drover --continue".

#### Build failure may be due to DEPS changes

If the merged branch doesn't build locally, you may need to port DEPS change
from the version-number named branch (e.g.
<https://chromium.googlesource.com/chromium/src.git/+/53.0.2785.24>). DEPS
changes after branch cut are not made on the release branch but in the
version-number named branches.

**Check which branches your patch has been merged to**

If you have a patch and want to check which branches it has been merged to, or
which version it first got released in, you can use:

$ git find-releases eeac3e6a187e362d796fa01489927e773be705ac

commit eeac3e6a187e362d796fa01489927e773be705ac was:

initially in 55.0.2867.0

merged to 54.0.2840.36 (as 72308707baae9aac0bea97387c1b4426bd378f51)

## **Merging Blink revisions into branches using git**

**NOTE: this is only needed for branches older than M46. As of M46, Blink is
merged into Chromium.**

See [Merging Blink revisions into branches using
git](https://docs.google.com/a/chromium.org/document/d/11o74oGwPkA58HBIZlDDV_CEbf3r-eUz_PDAh-DyH9iw/edit#heading=h.87eks4ierg7d)
