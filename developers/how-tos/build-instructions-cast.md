# Build Instructions (Cast)

**Cast docs have moved! Linux instructions are
[here](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_cast_build_instructions.md)
and Android are
[here](https://chromium.googlesource.com/chromium/src/+/master/docs/android_cast_build_instructions.md).**
