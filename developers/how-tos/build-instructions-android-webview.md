# Build Instructions (Android WebView)

## **Summary**

These are instructions for building the [Android
WebView](http://developer.android.com/reference/android/webkit/WebView.html).
These instructions are necessary if you wish to run the WebView code as an
Android system component (which is useful when working on performance or
application compatibility). To run tests and for certain development tasks it is
sufficient to use the standalone test shell as described
[here](https://chromium.googlesource.com/chromium/src.git/+/master/docs/android_test_instructions.md).

### **Prerequisites**

A machine capable of [building
Android](http://source.android.com/source/building.html). Being able to checkout
and build [Chrome for
Android](https://chromium.googlesource.com/chromium/src/+/master/docs/android_build_instructions.md).

## Building with ninja

For googlers, see [internal
instructions](https://goto.google.com/clank-webview/building-webview/android_webview-tests).

### One-time Android OS set up

This workflow currently requires a device with **AOSP builds** of Android
Lollipop 5.0.0 release, with root access. Follow instructions at
<https://source.android.com/source/building.html> to build and flash the correct
AOSP version to your device. Here's the rough flow of building AOSP for Nexus 5.

    # Checkout code
    repo init -u https://android.googlesource.com/platform/manifest -b android-5.0.0_r19
    repo sync -j<number>
    # Obtain and extract binary drivers at <https://developers.google.com/android/nexus/drivers> for your device
    # Building
    source build/envsetup.sh
    lunch aosp_<device>-userdebug # hammerhead for nexus 5, mako for nexus 4, flo for newer nexus 7
    make -j<number>
    # Flash to device
    adb reboot bootloader
    fastboot -w flashall

### Building

Follow the instructions
[here](https://www.chromium.org/developers/how-tos/android-build-instructions)
for getting the source and building chromium for android.

    gn gen out/Release --args='target_os="android"'  # For 64-bit devices, use: --args='target_os="android" target_cpu="arm64"'
    ninja -C out/Release system_webview_apk

### Installing onto the device

There is a one time device set up to remove the existing webview. This is needed
because it is not possible to install over the existing apk due to mismatched
signing key.

    # Uninstall any webview updates
    adb uninstall com.google.android.webview  # May fail
    adb uninstall com.android.webview  # May fail
    # On M and up:
    adb disable-verity; adb reboot
    # Remove webview from system partition
    adb root
    adb remount
    adb shell stop
    adb shell rm -rf /system/app/webview /system/app/WebViewGoogle /system/app/WebViewStub
    adb shell start

Install the built apk.

    adb install -r -d out/Release/apks/SystemWebView.apk

### Test shell: system_webview_shell_apk

### *   This is a simple shell app that uses the system webview.
### *   The activity is called 'WebView Browser'
### *   Build and install:
### *   `ninja -C out/Default system_webview_shell_apk`
###     `adb install -r out/Default/apks/SystemWebViewShell.apk`
### *   Open pages (http:// prefix is required, otherwise you'll get an empty
        page):
###     `build/android/adb_run_system_webview_shell http://www.google.com`

### Test shell: webview_instrumentation_apk

*   This embeds a copy of the webview in the apk rather than using the system
    webview.
*   The activity is called 'Android WebView'
*   It is essentially a TestContainerView with a URL bar at the top and runs in
    **hardware mode**.
*   Build and install:
    `ninja -C out/Default webview_instrumentation_apk`
    `adb install -r out/Default/apks/WebViewInstrumentation.apk`
*   Open pages (http:// prefix is required, otherwise you'll get an empty page):
    `build/android/adb_run_android_webview_shell http://www.google.com`
