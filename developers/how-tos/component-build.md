# Component build / Shared Library / Multi-DLL build

This documentation has moved to the source tree. See t[he Chromium component
build](https://chromium.googlesource.com/chromium/src/+/master/docs/component_build.md).
