# Working with Release Branches

### Syncing and building a release tag

Releases are tagged in git with the release version number.
Note: You cannot commit to a release tag. This is purely for acquiring the
sources that went into a release build.

# Make sure you have all the release tag information in your checkout.

git fetch --tags

# Checkout whatever version you need (known versions can be seen with 'git
show-ref --tags')

git checkout -b your_release_branch 34.0.1847.9 # or more explicitly,
tags/34.0.1847.9

gclient sync --with_branch_heads --jobs 16

### Checking out a release branch

**Note: it is usually NOT possible to sync and build a release branch** (i.e.
with consistent third_party DEPS); refer to the internal documentation
(*go/ChromeReleaseBranches*) for that. The instructions below show how to check
out a release branch only for a specific project (e.g. src.git).

# Make sure you are in 'src'.
# This part should only need to be done once, but it won't hurt to repeat it.
The first
# time might take a while because it fetches an extra 1/2 GB or so of branch
commits.
gclient sync --with_branch_heads # You may have to explicitly 'git fetch origin'
to pull branch-heads/
git fetch
# Checkout the branch 'src' tree.
git checkout -b branch_$BRANCH branch-heads/$BRANCH
# Checkout all the submodules at their branch DEPS revisions.
gclient sync --jobs 16

Edit files, 'git commit', and 'git cl upload' as normal. After that, 'git cl
land' commits to the right branch magically! (Don't try to use the CQ on a
branch.)

To get back to the "trunk":

# Make sure you are in 'src'.
git checkout -f master
gclient sync --jobs 16

Also, if you need to merge changes to DEPS, see the internal
*go/**ChromeReleaseBranches* page for some notes.
