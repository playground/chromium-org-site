# Committing and reverting changes manually

With git you should make all your changes in a local branch. Once your change is
committed, you can delete this branch.

Create a local branch named "mywork" and make changes to it.

cd src
git new-branch mywork
vi ...

Commit your change locally (this doesn't commit your change to the SVN or Git
server)

git commit -a

If you added new files, you should tell git so by running git add <files> before
committing.

Upload your change for review

git cl upload

Send a try job

git cl try

See [Contributing code](http://www.chromium.org/developers/contributing-code)
for more detailed git instructions, including how to update your CL when you get
review comments.

There's a short tutorial that might be helpful to try before your first change:
[C++ in Chromium 101](../../cpp-in-chromium-101-codelab.md).

# Commit your change manually

Follow [these instructions](../../contributing-code/direct-commit.md).
