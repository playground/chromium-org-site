# Managing Multiple Working Directories

If you are a multitasker or want to build chromium for different targets without
clobbering each other, then perhaps you'll like the
[gclient-new-workdir.py](https://chromium.googlesource.com/chromium/tools/depot_tools.git/+/master/gclient-new-workdir.py)
script located in
[depot_tools.](http://www.chromium.org/developers/how-tos/depottools) The script
works by creating a new working directory with symlinks pointing to the git
database(s) found in your original chromium checkout. You can have as many
working directories as you want without the overhead and hassle of cloning
chromium multiple times.

    gclient-new-workdir.py /path/to/original/chromium chromium2

### Windows devs

gclient-new-workdir.py doesn't support Windows, but you can try[
https://github.com/joero74/git-new-workdir](https://github.com/joero74/git-new-workdir)
to do the same thing **(needs to be run as admin)**. For the curious, the script
essentially uses mklink /D and other minor tricks to setup the mirrored .git
repo.

### Chromium OS devs

gclient-new-workdir.py uses symlinks that will not work inside the cros_sdk
chroot. If using a [local Chromium source for Chromium
OS](https://www.chromium.org/chromium-os/developer-guide#TOC-Making-changes-to-non-cros_workon-able-packages),
be sure to use the original working directory.
