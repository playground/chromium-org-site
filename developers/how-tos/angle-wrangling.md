# ANGLE Wrangling

**What is going on? Who am I? What am I doing here?**

You are an ANGLE Wrangler. Your job is threefold:

1.  Watch the [ANGLE Try
    Waterfall](http://build.chromium.org/p/tryserver.chromium.angle/waterfall)
    and [Chromium GPU FYI
    Waterfall](https://build.chromium.org/p/chromium.gpu.fyi/console) for
    breakage (unexpected non-greenness).
2.  Report and triage breaks to the right person.
3.  Ensure they follow through and fix the break.

**Task #1: Observe**

The two waterfalls we care about are the [ANGLE Try
Waterfall](http://build.chromium.org/p/tryserver.chromium.angle/waterfall) and
[Chromium GPU FYI
Waterfall](https://build.chromium.org/p/chromium.gpu.fyi/console). The try
waterfall shows recent runs of the ANGLE trybots, and the FYI waterfall gives
thorough canary testing against top-of-tree ANGLE. The FYI waterfall should
always be green. The trybots may have failures where devs are testing their code
and find bugs.

At the start of your shift [the
log](https://docs.google.com/document/d/1tOLH-26FgZVxR4OEukOl6wvPyo_w0dXI50JVQeIQ_FI/edit#)
may help you get up to speed. Remember to add helpful tips for the next
wrangler.

**Task #2: Contain**

If you see a failure, meaning a red or purple bot, first quickly check for a
known issue, and if you can't find it, file a bug. You can file either on the
[Chromium issue tracker](http://crbug.com/new) or the [ANGLE issue
tracker](https://bugs.chromium.org/p/angleproject/issues/entry) for ANGLE bugs.

IMPORTANT: Info to include in bug reports:

*   Links to as many first failing builds as possible (eg first windows failure,
    first mac failure, etc)
*   Link to the last successful build
*   The Chromium and ANGLE regression ranges. (See the note below on how to
    determine the ANGLE regression range on the FYI bots)
*   A snippet of relevant error text
*   Tag the bug with the ANGLE tag in the Chromium tracker and cc some ANGLE
    folks

HOWTO: Determine the ANGLE regression range on the FYI bots:

1.  Pull up the first failing and last passing builds
2.  Look for 'parent_got_angle_revision' in both builds (for tester failures..
    for compile failures just look for 'got_angle_revision')
3.  Use the URL: https://chromium.googlesource.com/angle/angle.git/+log/<last
    good revision>..<first bad revision>

NOTE: Offline bots

Offline bots show up as grey. If there's a bot that has been offline for more
than a half hour, there's a good chance it needs a kick to get it started again.
Visit [go/bug-a-trooper](http://go/bug-a-trooper) to fix this.

After you file the issue, you should decide if you're going to triage the issue
yourself or find an appropriate owner. If it's a clear ANGLE bug, you can either
assign the issue to the culprit or proceed to section 3 to fix it yourself. If
it's not clear who or what broke the build, you can ask for help, aka cc lots of
people on the bug.

**Task #3: Protect**

If you assigned the bug to someone and they haven't responded, or don't seem to
be working on it. Please bug them, escalate the priority, and ensure the next
ANGLE Wrangler is aware of the unfinished job.

If you're fixing the bug yourself, you can either make a small patch, or simply
revert the CL to get the tree green again. Small build fixes are fine, but don't
make large complex fixes on top of a broken tree. Revert first in this case.

**But I'm not an ANGLE Wrangler..**

Fair enough. This page wasn't really written for you. But if you're interested
please email angle-team@.
