# Contributing Code

This page covers contributing code to the main Chromium repository. It assumes
you already have a working [checkout and
build](https://www.chromium.org/developers/how-tos/get-the-code). A full
checkout pulls many other repositories such as v8 and Skia which have their own
repositories and processes. Chromium it itself pulled into
[ChromiumOS](http://dev.chromium.org/chromium-os/developer-guide) which has its
own process.

#### Related resources

*   [Life of a Chromium
    Developer](https://docs.google.com/a/google.com/present/view?id=0AetfwCoL2lQAZGQ5bXJ0NDVfMGRtdGQ0OWM2)
    slides which are still mostly up-to-date.
*   [Tutorial](http://meowni.ca/posts/chromium-101) by a member of the team.

#### Communicate

*   Whether you're writing a new feature or fixing an existing bug, it pays to
    get a second opinion before you get too far. If it's a new feature idea,
    post to the appropriate discussion group
    ([Chromium](../discussion-groups/index.md) | [Chromium
    OS](http://dev.chromium.org/chromium-os/discussion-groups)) and propose it.
    If it is in the existing code base, it is a good idea to talk to some of the
    folks in the "OWNERS" file (see [code review
    policies](https://chromium.googlesource.com/chromium/src/+/master/docs/code_reviews.md)
    for more) for the code you want to change.
*   Behavior changes and anything nontrivial (i.e. anything other than simple
    cleanups and style fixes) should generally be tracked in the [bug
    system](https://bugs.chromium.org/p/chromium/issues/list). Please [file a
    bug](https://bugs.chromium.org/p/chromium/issues/entry) and describe what
    you're doing if there isn't one already.
*   Keep in mind that just because there is a bug in the bug system doesn't
    necessarily mean that a patch would be accepted.

#### Legal stuff

*   You must complete the [Individual Contributor License
    Agreement](https://cla.developers.google.com/about/google-individual?csw=1).
    You can do this online, and it only takes a minute. If you are contributing
    on behalf of a corporation, you must fill out the [Corporate Contributor
    License
    Agreement](https://cla.developers.google.com/about/google-corporate?csw=1)
    and send it to us as described on that page.
*   If you've never submitted code before, you must add your (or your
    organization's) name and contact info to the AUTHORS file for
    [Chromium](https://code.google.com/p/chromium/codesearch#chromium/src/AUTHORS)
    or [Chromium
    OS](https://chromium.googlesource.com/chromiumos/chromite/+/master/AUTHORS).
*   Reviewers should follow the [External Contributor
    Checklist](http://dev.chromium.org/developers/contributing-code/external-contributor-checklist).

## Create a local branch

Start with a branch in git. Here we create a branch called `mychange` (use
whatever name you want here) based off of the `origin/master` branch (the
upstream repository):

    git checkout -b mychange -t origin/master

Write and test your code.

*   Conform to the [style
    guidelines](http://dev.chromium.org/developers/coding-style).
*   Include appropriate unit tests.
*   Patches should be a reasonable size to review. Giant patches are unlikely to
    get reviewed quickly.

Commit your patch locally in git (you may want to do search for git tutorials if
you are unfamiliar with it).

    git commit -a

## Uploading a change for review

## **Initial git setup**

Get credentials for uploading code reviews by visiting
https://chromium.googlesource.com/new-password and following the on-screen
instructions.

Tell git about your name, email and some other settings.

    git config --global user.name "My Name"
    git config --global user.email "myemail@chromium.org"
    git config --global core.autocrlf false
    git config --global core.filemode false
    git config --global branch.autosetuprebase always
    git config --local gerrit.host true

**The upload command**

We use the [Gerrit](https://gerrit.googlesource.com/gerrit) code review tool
running at
[chromium-review.googlesource.com](https://chromium-review.googlesource.com). To
upload your change to Gerrit, use the `git-cl` tool that came with depot_tools
when you checked the code out.

    git cl upload

This will create a Gerrit change for you. You will be prompted for a
description, and some presumbit checks will also be run to identify common
errors. When it is done it will print the URL you can use to see the change on
the web.

You can use command line options to specify the reviewers that you want on the
CL, and the bugs that will be fixed by the change:

    git cl upload -r foo@example.com,bar@example.com -b 123456

### **Code quality guidelines**

We want this code to be the best codebase you've ever worked on, and the
maintainability of the code is critical:

*   Follow the [Core Principles](../core-principles/index.md).
*   Make sure your code is readable.
*   Don't hesitate to refactor code where it makes it less platform-specific or
    it improves the design.
*   Don't take shortcuts; development is more like a marathon than a sprint.
*   And [leave the code cleaner than you found
    it](http://pragmaticcraftsman.com/2011/03/the-boy-scout-rule/).

### Writing change list descriptions

Use the following form:

    Summary of change
    Longer description of change addressing as appropriate: why the change is made,
    context if it is part of many changes, description of previous behavior and
    newly introduced differences, etc.
    Long lines should be wrapped to 80 columns for easier log message viewing in
    terminals.
    Bug: 123456

A short subject and a blank line after the subject are crucial. Use the bug
number from the [issue tracker](https://crbug.com) (see [more on bug
formatting](http://dev.chromium.org/developers/contributing-code/-bug-syntax)).
If you include links to previous CLs then consider using crrev.com/c/NUMBER
format rather than https://chromium-review.googlesource.com/c/NUMBER format. The
crrev.com format is shorter, and avoids confusion when the CL is submitted. The
submitted CL will have a link to its code review page and using crrev.com for
referenced CLs avoids clicking on the wrong one.

Some good thoughts on how to write good git commit messages can be found
[here](https://chris.beams.io/posts/git-commit/).

If there are instructions for testers to verify your change is correct, append:

    Test: Load example.com/page.html and click the foo-button; see
    crbug.com/123456 for more details.

## Code review

#### Code reviews are covered in more detail on the [code review policies](https://chromium.googlesource.com/chromium/src/+/master/docs/code_reviews.md) page.

#### Finding a reviewer

Ideally the reviewer is someone who is familiar with the area of code you are
touching. If you have doubts, look at the `git blame` for the file and the
OWNERS files.

*   Anybody can review code, but there must be at least one owner for each
    directory you are touching.
*   If you have multiple reviewers, make it clear in the message you send
    requesting review what you expect from each reviewer. Otherwise people might
    assume their input is not required or waste time with redundant reviews.
*   The `git cl owners` command can help find owners.

#### Requesting review

Open the change on the web (if you can't find the link, run `git cl issue` to
see the issue for your current branch, or go to the Web UI
chromium-review.googlesource.com, log in, and look at "Outgoing Reviews").

Reviewers expect to review code that compiles and passes tests. If you have
access, now is a good time to run your change through the automated tests (see
below).

Click **Add Reviewers** in the upper-left (if you don't see this link, make sure
you are logged in). In the **Reviewers** field, enter a comma-separated list of
the reviewers you picked.

In the same dialog, type the initial message to your reviewers and click
**Send**. This will send email to notify the reviewers you are requesting a
review. If you have any particular questions or instructions for the code
review, enter them in the **Message** box, but it's fine to leave this field
empty.

#### The review process

All changes must be reviewed, see: [Code Review
Policy](https://chromium.googlesource.com/chromium/src/+/master/docs/code_reviews.md).

#### Approval

When the reviewer is happy with your patch, they will set the "Code-Review +1"
label.

You need approval from owners of all affected files before committing, see [Code
Review Policy: Owners
section](https://chromium.googlesource.com/chromium/src/+/master/docs/code_reviews.md#OWNERS-files).

## Running automated tests

Before being submitted, a change must pass a large series of compilations and
tests across many platforms. To trigger this process, press the **CQ dry run**
(CQ = "Commit Queue") in the upper right corner of the code review tool. This is
equivalent to setting the "Commit-Queue +1" label. This label is onlyis
available to everyone, but the CQ may choose not to run the tests if it thinks
the code isn't safe. To be regarded as safe:

*   If you have an @chromium.org email address, request [try job
    access](http://www.chromium.org/getting-involved/become-a-committer#TOC-Try-job-access)
    for yourself.
*   If you've made a few patches you can request a reviewer to nominate you for
    try job access.
*   Otherwise, write in the code review request message that this is your first
    patch and request try jobs be run for you.

## Committing

Changes should generally be committed via the [commit
queue](http://dev.chromium.org/developers/testing/commit-queue). This is done by
clicking the **Subit to CQ** button in the upper right corner, or setting the
"Commit-Queue +2" label on the change. The commit queue will then send your
patch to the try bots, which will eventually appear as colored bubbles near the
checkbox in the code review tool (the same thing that happens for dry runs). If
all tests pass, your change will be auto committed. If it fails, click on the
red (failure) bubbles for a direct link to the failures. Sometimes a test might
be flaky, if you have an isolated failure that appears unrelated to your change,
wait a while and click commit again.

Alternatively, it is possible to [directly
commit](http://dev.chromium.org/developers/contributing-code/direct-commit) your
change, bypassing the commit queue. This should only be used in emergencies
because it will bypass the tests.

## Tips

During the lifetime of a review you may want to rebase your change onto a newer
source revision to minimize eventual merge pain. The reviewer-friendly way to do
this is to upload the rebase as its own patchset (with no changes other than the
rebase) when there are no outstanding comments. Then upload another patch with
your changes. This way the reviewer can see what changes you made independent of
the rebase.

Code authors and reviewers should keep in mind that Chromium is a global
project: contributors and reviewers are often in time zones far apart. Please
read these [guidelines on minimizing review
lag](http://dev.chromium.org/developers/contributing-code/minimizing-review-lag-across-time-zones)
and take them in consideration both when writing reviews and responding to
review feedback.
