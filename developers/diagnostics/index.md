# Diagnostics

For diagnostics that any user can run check [Diagnostic
Mode](../../administrators/diagnostic-mode/index.md).
