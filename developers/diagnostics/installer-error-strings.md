# Installer Error Strings

Chrome's Windows installer provides localized error strings to Google Update for
presentation to the user. The strings are written to the registry by
[WriteInstallerResult](http://codesearch.google.com/codesearch#OAMlx_jo-ck/src/chrome/installer/util/installer_state.h&exact_package=chromium&q=WriteInstallerResult&l=183&ct=rc&cd=1).
As of this writing, the set of error string message identifiers (search for them
in
[google_chrome_strings.grd](http://src.chromium.org/viewvc/chrome/trunk/src/chrome/app/google_chrome_strings.grd?view=markup)
for their corresponding text) is:

**Message ID** **Product(s)** **Operation** IDS_INSTALL_DIR_IN_USE
All First install IDS_INSTALL_FAILED All All IDS_INSTALL_HIGHER_VERSION Chrome
Update IDS_INSTALL_HIGHER_VERSION_CB_CF Chrome + Chrome Frame Update
IDS_INSTALL_HIGHER_VERSION_CF Chrome Frame Update
IDS_INSTALL_INCONSISTENT_UPDATE_POLICY All Update
IDS_INSTALL_INSUFFICIENT_RIGHTS All All system-level IDS_INSTALL_INVALID_ARCHIVE
All All IDS_INSTALL_MULTI_INSTALLATION_EXISTS All Update or Repair
IDS_INSTALL_NON_MULTI_INSTALLATION_EXISTS Chrome Frame Update or Repair
IDS_INSTALL_NO_PRODUCTS_TO_UPDATE Chrome First install IDS_INSTALL_OS_ERROR All
All IDS_INSTALL_OS_NOT_SUPPORTED All All IDS_INSTALL_READY_MODE_REQUIRES_CHROME
Chrome Frame All IDS_INSTALL_SYSTEM_LEVEL_EXISTS All Update or Repair
IDS_INSTALL_TEMP_DIR_FAILED All All IDS_INSTALL_UNCOMPRESSION_FAILED All All
IDS_SAME_VERSION_REPAIR_FAILED Chrome Repair IDS_SAME_VERSION_REPAIR_FAILED_CF
Chrome Frame Repair IDS_SETUP_PATCH_FAILED All All
