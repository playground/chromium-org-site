# Code Coverage

### Accessing code coverage results

1.  Download **DynamoRIO** from [www.dynamorio.org](http://www.dynamorio.org/)
    *   Download the **DynamoRIO** release package from
        [here](https://github.com/DynamoRIO/dynamorio/wiki/Downloads) or nightly
        builds from
        [here](http://build.chromium.org/p/client.dynamorio/builds/).
    *   Extract downloaded **DynamoRIO** package.
2.  Build the application normally with debug information
    *   $ ninja -C out\\Debug pdfium_unittests
3.  Generate the code coverage data by running the application with **Dr. Cov**
    in **DynamoRIO**
    *   $ DynamoRIO\\bin32\\drrun -t drcov --
        .\\out\\Debug\\pdfium_unittests.exe
    *   The command above will run the tests under the Dr. Cov and generate
        several drcov log files, e.g.,
        drcov.pdfium_unittests.exe.07140.0000.proc.log.
    *   Generate coverage info (in
        [lcov](http://ltp.sourceforge.net/coverage/lcov.php) format) from the
        drcov log files using drcov2lcov
    *   $ tools\\bin32\\drcov2lcov.exe -input
        drcov.pdfium_unittests.exe.07140.0000.proc.log -output
        pdfium_unittests.cov
4.  View the result
    *   Using genhtml from
        [**DynamoRIO**](https://github.com/DynamoRIO/dynamorio/tree/master/third_party/lcov)
        to generate the coverage html for viewing
    *   $ dynamorio.git/third_party/lcov/genhtml pdfium_unittests.cov -o output
    *   The html files are in output/ and ready for viewing.
    *   The example of code coverage results for pdfium tests can be found
        [here](http://build.chromium.org/p/client.dynamorio/pdfium/).
