# Postmortems

### Why Write a Postmortem

To understand how failures happen, in order to prevent future occurrences by
education and process changes.

### When To Write a Postmortem

A postmortem is expected for any tree closures lasting longer than 4 hours,
within 72 hours of the outage.

### Who Should Write the Postmortem

The postmortem should be written by someone involved with detecting and
correcting the issue, preferably someone who can take responsibility for the
followup.

### What to Include

Please use the postmortem template found
[here](https://docs.google.com/document/d/1oBYQmpBthPfxMvW0XgHn7Bu918n6eFLlQM7nVhEdF_0/edit?usp=sharing)
(file -> make a copy).

Your postmortem should include the following sections:

1.  Title
2.  Summary of the event
3.  Full timeline
4.  Root cause(s)
5.  What worked and what didn't (a.k.a., lessons learned)
6.  Action items (followup bugs assigned to specific people)

### Where to Put It

Whenever possible, postmortems should be accessible to the entire Chromium
community. If you are a Google employee, and your postmortem contains internal
details, see the internal infrastructure team's postmortem site instead.

1.  With your chromium.org, write it in a Google Doc, set sharing permissions to
    “Anyone who has the link can comment”
2.  Add it to the list below.
3.  Send the link to
    [chromium-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/chromium-dev)
    or
    [infra-dev@chromium.org](https://groups.google.com/a/chromium.org/forum/#!forum/infra-dev),
    as relevant.

See also:

*   <http://codeascraft.com/2012/05/22/blameless-postmortems/>
*   [Example of a great
    postmortem](https://docs.google.com/document/d/1AyeS2du6wp_Pw8Grg8WovbE_A_HV4EUMqdiqeq1KUZ8/edit#heading=h.40nli0xmdtb5).
