# Home

The Chromium projects include Chromium and Chromium OS, the open-source projects
behind the [Google Chrome](https://www.google.com/chrome) browser and Google
Chrome OS, respectively. This site houses the documentation and code related to
the Chromium projects and is intended for developers interested in learning
about and contributing to the open-source projects.

**[Chromium](../Home/_index.md) **
Chromium is an open-source browser project that aims to build a safer, faster,
and more stable way for all users to experience the web. This site contains
design documents, architecture overviews, testing information, and more to help
you learn to build and work with the Chromium source code.

<https://www.google.com/chrome>

Looking for Google Chrome?

[Download Google Chrome](https://www.google.com/chrome)

**[Chromium OS](../chromium-os/index.md)**

Chromium OS is an open-source project that aims to provide a fast, simple, and
more secure computing experience for people who spend most of their time on the
web. Learn more about the [project
goals](https://googleblog.blogspot.com/2009/11/releasing-chromium-os-open-source.html),
obtain the latest build, and learn how you can get involved, submit code, and
file bugs.

<https://www.google.com/chromeos>

Looking for Google Chrome OS devices?
[Visit the Google Chrome OS site](https://www.google.com/chromeos)
