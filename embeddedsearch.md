# Embedded Search API

**This document describes a replacement for the [SearchBox
API](http://dev.chromium.org/searchbox) designed for search providers included
with Chromium to allow them to integrate more tightly with Chrome’s Omnibox and
New Tab Page. This document will be kept up to date with the changes in trunk
Chromium and is to be considered the authoritative reference on this new API.**

**Overview**
**The Embedded Search API currently provides two primary points of integration
for a default search provider:**

1.  The provider is responsible for rendering the New Tab Page (NTP).
2.  The Omnibox continues to show users' search terms when they arrive at a
    search results page, and route any further searches to the existing results
    page. This allows the results page to choose not to render its own search
    box.

**Search providers who wish to try EmbeddedSearch NewTabPage API can do so by
modifying the TemplateURL data for their provider (in
[src/components/search_engines/prepopulated_engines.json](https://code.google.com/p/chromium/codesearch#chromium/src/components/search_engines/prepopulated_engines.json))
to add a "****new_tab_url****" field pointing to an URL that will render the
NTP. Implementers can find an example in the chromium sources at
[src/chrome/browser/resources/local_ntp/local_ntp.js](https://code.google.com/p/chromium/codesearch#chromium/src/chrome/browser/resources/local_ntp/local_ntp.js).**

**Reference**

**// This interface is exposed as window.chrome.embeddedSearch to the embedded search page.**
**interface EmbeddedSearch {**

** // Interface to allow the embedded search page to interact with and control the **
** // navigator searchbox. Exposed as window.chrome.embeddedSearch.searchBox.**
** interface SearchBox {**

// The text entered by the user into the omnibox. This is only populated if a

// user starts on a search results page and enters a new search in the omnibox.

** readonly attribute DOMString value;**

** // A callback function called by the browser when the user submits a new
search**

** // while on a search results page, typically by pressing enter. There are
no**

** // arguments. (Note that value will be set to the text of the search terms**

** // entered.)**
** attribute Function onsubmit;**

// Specifies params that are necessary to complete the embedded search request.

// This includes the impression of query suggestions, original query, search
query etc.

readonly attribute EmbeddedSearchRequestParams **requestParams**;

// Specifies the Instant suggested text and its metadata.

// Used to tell the server what suggestion to prefetch.

readonly attribute InstantSuggestion suggestion;

// Fired when the suggestion query is changed.

// There are no arguments. Server will get those details from |suggestion|.

attribute Function onsuggestionchange;

// Indicates whether the browser is rendering its UI in rigth-to-left mode.

readonly attribute unsigned boolean rtl;

// Requests the searchbox to enter (or exit) a mode where it is not visibly
focused

** // but if the user presses a key will capture it and focus itself. Conceptually **
** // similar to an invisible focus() call.**
** void startCapturingKeyStrokes();**
** void stopCapturingKeyStrokes();**

** // Attribute and listener to detect if the searchbox is currently capturing user key**
** // strokes.**
** readonly attribute boolean isKeyCaptureEnabled;**
** attribute Function onkeycapturechange;**
** }; // interface Searchbox**
** // Interface to allow the embedded search page to interact with and control
the**

** // New Tab Page. Exposed as window.chrome.embeddedSearch.newTabPage.**
** interface NewTabPage {**

// Attribute and listeners that indicate to the new tab page whether user input

// is in progress in the omnibox.

readonly attribute boolean **isInputInProgress**;

attribute Function **oninputstart**;

attribute Function **oninputcancel**;

// List of user's most visited items.

readonly attribute Array<MostVisitedItem> **mostVisited**;

attribute Function **onmostvisitedchange**;

// Deletes the most visited item corresponding to |restrictedID|.

function **deleteMostVisitedItem(**restrictedID**)**;

// Undoes the deletion of the item for |restrictedID|.

function **undoMostVisitedDeletion(**restrictedID**)**;

// Undoes the deletion of all most visited items.

function **undoAllMostVisitedDeletions()**;

// Information necessary to render the user's theme background on the NTP.

** readonly attribute ThemeBackgroundInfo themeBackgroundInfo;**

attribute Function **onthemechange**;

** }; // interface NewTabPage**
**};**
// Format for a most visited object supplied by the browser.

//

// Most visited items are represented by an opaque identifier, the "rid". Chrome

// provides two special URLs that the page can render as <iframes> to display

// most visited data:

//

// chrome-search://most-visited/title.html?rid=<RID>

// chrome-search://most-visited/thumbnail.html?rid=<RID>

//

// Both URLs also accept the following query parameters to modify the style of
the

// content inside the iframe:

//

// 'c' : Color of the text in the iframe.

// 'f' : Font family for the text in the iframe.

// 'fs' : Font size for the text in the iframe.

// 'pos' : The position of the iframe in the UI.

//

// For example, the page could create the following iframe

//

// <iframe
src="chrome-search://most-visited/title.html?rid=5&c=777777&f=arial%2C%20sans-serif&fs=11&pos=0">

//

// to render the title for the first most visited item.

MostVisitedItem {

// The URL of the most visited item.

readonly attribute string **url**;

// The host name of the most visited item URL.

readonly attribute string **domain**;

// The title of the most visited page. May be empty, in which case the |url| is
used as the title.

readonly attribute string title;

// Specifies the title text direction.

readonly attribute string **direction**;

// The restricted ID (an opaque identifier) to refer to this item.

readonly attribute unsigned long rid;

// URL to load the favicon for the most visited item as an image.

//

// NOTE: the URL is particular to the current instance of the New Tab page and

// cannot be reused.

readonly attribute string faviconUrl;

// URL to load the thumbnail for the most visited item as an image.

//

// NOTE: the URL is particular to the current instance of the New Tab page and

// cannot be reused.

readonly attribute string thumbnailUrl;

}

**// Format of the ThemeBackgroundInfo object which indicates information about the user's**
**// chosen custom UI theme necessary for correct rendering of the new tab page.**
**ThemeBackgroundInfo {**

// NOTE: The following fields are always present.

** // Theme background color in CSS "background-color" format.**
** readonly attribute String colorRgba;**

// Theme background color in RGBA format.
readonly attribute Array backgroundColorRgba;

// Theme header color in RGBA format.

readonly attribute Array headerColorRgba;

// Theme link color in RGBA format.

readonly attribute Array **linkC****olorRgba**;

// Theme section border color in RGBA format.

readonly attribute Array **sectionBorderColor****Rgba**;

// Theme text color light in RGBA format.

readonly attribute Array **textColorLight****Rgba**;

// Theme text color in RGBA format.

readonly attribute Array **textColor****Rgba**;

// True if the default theme is selected.

readonly attribute boolean **usingDefaultTheme**;

// True if the theme has an alternate logo.

readonly attribute boolean **alternateLogo**;

** // NOTE: The following fields are all optional.**

** // Theme background-image url, if any.**
** readonly attribute String imageUrl;**
** // Theme background-position x component.**
** readonly attribute String imageHorizontalAlignment;**
** // Theme background-position y component.**
** readonly attribute String imageVerticalAlignment;**
** // Theme background-repeat.**
** readonly attribute String imageTiling;**

** // Theme attribution url, if any.**
** readonly attribute String attributionUrl;**
**};**

EmbeddedSearchRequestParams {

readonly attribute String searchQuery;

readonly attribute String originalQuery;

// NOTE: The following fields are all optional.

readonly attribute String inputEncoding;

readonly attribute String assistedQueryStats;

readonly attribute String **rlzParameterValue**;

};

InstantSuggestion {

// Suggestion text to prefetch.

readonly attribute String **text;**

// JSON metadata from the server response which produced this suggestion.

readonly attribute String **metadata**;

};

Last edited: Wednesday, December 23, 2015, 12:00 AM PST
