# Policy Templates

To ease your policy setup, several policy templates can guide you easily through
the configurable options.

Templates can also be generated locally by building the policy_templates
Chromium project.

**Windows**

There are two types of templates available, an ADM and an ADMX template. You
will want to verify that the template type you can use on your network.

[ZIP file of ADM/ADMX/JSON templates and
documentation](https://dl.google.com/dl/edgedl/chrome/policy/policy_templates.zip).
[Beta](https://dl.google.com/chrome/policy/beta_policy_templates.zip) and
[Dev](https://dl.google.com/chrome/policy/dev_policy_templates.zip)
Administrative templates are also available for testing them before a stable
release.

Google Update (auto-update) has its own templates as well, in
[ADM](https://dl.google.com/update2/enterprise/GoogleUpdate.adm) and
[ADMX](http://dl.google.com/dl/update2/enterprise/googleupdateadmx.zip) forms.

The recommended way to configure policy on Windows is Group Policy Object (GPO),
however on machines that are joined to an Active Directory domain, policy
settings may also be stored in the registry under HKEY_LOCAL_MACHINE or
HKEY_CURRENT_USER in the following paths:

*   Google Chrome: Software\\Policies\\Google\\Chrome\\
*   Chromium: Software\\Policies\\Chromium\\

**Mac**

The manifest plist file is in the Google Chrome / Chromium bundle. So if you've
already downloaded the package, you can find the plist by:

1. Open the bundle

2. Find the resources folder

3. You will find a file called com.google.Chrome.manifest inside.

To see how to load this file into the Workgroup Manager, see the [Mac Quickstart
guide](mac-quick-start.md).

**Linux**

For Linux, consult the HTML documentation contained in the [ZIP file of ADM/ADMX
templates and
documentation](https://dl.google.com/dl/edgedl/chrome/policy/policy_templates.zip)
for the JSON keys that correspond to each policy.

To see where to put this file, see the [Linux Quickstart
guide](linux-quick-start.md).
