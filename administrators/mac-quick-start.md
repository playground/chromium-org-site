# Mac Quick Start

This page describes how to get started with managing Chromium / Google Chrome on
a Mac network.

Note: these instructions are temporary but should help getting started.

*   Install the Workgroup Manager from Apple
    *   For 10.9: <http://support.apple.com/kb/dl1698>
*   Open the Workgroup Manager and connect to the machine to administer
    *   E.g. enter "localhost" as the Address, and login using an administrator
        account
*   Select a user from the left column and then click Preferences on the
    toolbar.
*   Select the Details tab. It lists applications that can be managed.
    *   If Google Chrome isn't listed, click the + button and add it.
*   Select Google Chrome from the list of applications and click the pencil
    button, next to + and - buttons.
*   Select "Always". Click "New Key" to add a new policy.
    *   Then click "New Item" to select the policy from the menu that pops up.
    *   Don't change the Type.
    *   Select an appropriate Value for that policy.
*   Click "Apply Now" and "Done" once all policies are set.

Login with the corresponding user account, start Chrome, and the policies should
be enforced now. Some policies will disable controls in the Preferences, and an
informative banner will be displayed (e.g. try forcing to show the home button).
Recent versions of Chrome (15 and later?) have an "about:policy" page that lists
the policies that are currently affecting Chrome.

**Debugging**

If you have trouble, it usually pays of to examine whether the settings are
correctly stored and read by Chrome. First of all, navigate to about:policy in
Chrome. It lists any policy settings that Chrome has picked up. If your settings
show up, good. If not, you can dig deeper and check whether Mac OS X actually
put them into place correctly. Mandatory policy is stored in /Library/Managed
Preferences/<username>/com.google.Chrome.plist while recommended policy is
stored in /Library/Preferences/com.google.Chrome.plist. The plutil command can
be used from a terminal to convert it to XML format:

*   # sudo -s
*   # cd /Library/Managed Preferences/<username>
*   # plutil -convert xml1 com.google.Chrome.plist
*   # cat com.google.Chrome.plist

For debugging, this file is in place it can be edited manually. Chrome will pick
up the updated preferences automatically. Note that this is not recommended for
making persistent changes to policy, since Mac OS X will rewrite the file with
settings configured through Workgroup Manager.

**Notes for Chromium**

Chromium can be managed in a similar way, with a few differences:

*   The Chromium application must be managed, instead of Google Chrome
*   Chromium is identified as "org.chromium.Chromium"
*   The plist file is in the same place but is named
    "org.chromium.Chromium.plist"

**Known issues**

*   Clicking "Apply Now" or "Done" in the Workgroup Manager sometimes hangs the
    machine. This isn't Chrome-specific, and seems to be more common the longer
    the machine has been running.

**Unknown issues**

Please file a bug report at <http://new.crbug.com> and select the "Enterprise
Issue" template.
