# ESET Anti-Virus

Some home security / anti-virus software can interact badly with Chrome. If you
have been directed here you may need to configure your anti-virus software to
allow Chrome to access the Internet.

**ESET Smart Security**

Under \`Setup’ click \`Toggle Advanced mode’:

![image](1.png)

Once in advanced mode, click \`Enter entire advanced setup tree’:

![image](2.png)

Find \`SSL’ under \`Protocol filtering’ and select \`Do not scan SSL protocol’:

![image](3.png)
