# Documentation for Administrators

This page describes the features and details of Chromium’s central management of
policies and preferences.

Chromium supports methods of managing the browser's behavior centrally, through
group policy, MCX files, and external configuration files. This gives you, as an
administrator, a way to manage the installations of Chromium-based browsers in
your organization.

These policies are strictly intended to be used to configure instances of Chrome
internal to your organization. Use of these policies outside of your
organization (for example, in a publicly distributed program) is considered
malware and will likely be labeled as malware by Google and anti-virus vendors.

**Getting Started**

*   [Windows Quick Start](windows-quick-start/index.md)
*   [Mac Quick Start](mac-quick-start.md)
*   [Linux Quick Start](linux-quick-start.md)
*   [Turning off Auto-Updates](turning-off-auto-updates.md)
*   [Full list of supported Policies](policy-list-3/index.md)

**Full Documentation**

*   [Common problems and solutions](common-problems-and-solutions.md)
*   [Policy templates](policy-templates.md)
*   [Configuring other preferences that are not
    policies](configuring-other-preferences.md)
*   [Configuring Apps and Extensions by
    Policy](configuring-policy-for-extensions.md)
*   [Pre-installing Extensions](pre-installed-extensions.md)
*   [Frequently Asked Questions](frequently-asked-questions.md)
*   [Supported directory path
    variables](policy-list-3/user-data-directory-variables.md)
*   [Known
    Issues](http://code.google.com/p/chromium/issues/list?can=2&q=Feature%3DEnterprise)
*   [iOS MDM Policy Format](ios-mdm-policy-format/index.md)
*   [Advanced Integration for SAML SSO on Chrome
    Devices](advanced-integration-for-saml-sso-on-chrome-devices.md)
*   [Complex policies on Windows](complex-policies-on-windows/index.md)
*   [Policies on
    Webview](https://chromium.googlesource.com/chromium/src/+/master/docs/webview_policies.md)

You can view all of the Enterprise feature requests and bugs sorted by their
milestone on the [Chromium bug
tracker](http://code.google.com/p/chromium/issues/list?can=2&q=Cr%3DEnterprise&mode=grid&y=&x=Mstone&cells=tiles).
