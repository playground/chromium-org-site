# Google Chrome Installation

### Standard Meta-installer

The standard Google Chrome meta-installer can be downloaded from
<https://www.google.com/chrome/>.

### MSI

The MSI can be downloaded from <https://www.google.de/work/chrome/browser/>.
