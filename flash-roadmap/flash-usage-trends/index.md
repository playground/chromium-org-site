# Flash Usage Trends

## Volume

Absolute volume of Flash Impressions (on Chrome Stable for Windows and MacOS)

![image](Flash-Volume---Update-Apr-27.png)

## Percent of Daily Actives

Percent of users who have encountered at least one page w/ Flash Player content
in a day (on Chrome Stable for Windows and MacOS)

![image](Daily-Actives---Update-Apr-27.png)

## Anticipated Impact of Upcoming Plugin Power Saver (PPS) Refinements

Currently tiny Flash content accounts for more than half of Flash Impressions on
the web (see the first image below). Coming in Chrome 59 and Chrome 60 we will
be introducing refinements to Plugin Power Save Tiny, designed to block small
form factor Flash content (demonstrated in the second image). The first
refinement, coming in Chrome 59 will affirmatively block all invisible 0x0
content and the second, coming in Chrome 60, will block all same origin small
Flash Content.

### Chrome 58: Current PPS versus PPS (sans Same Origin exception)

We are removing the [same origin
exception](https://www.chromium.org/flash-roadmap#TOC-PPS-Tiny---No-Same-Origin-Exceptions-Target:-Chrome-60---Aug-2017-)
in Chrome 60 (Aug 2017), which should further reduce in overall Flash usage.

![image](PPS-Tiny---0x0-before.png)

### Chrome 59: PPS (sans 0x0 exception) vs Chrome: 60 PPS (sans 0x0 + Same Origin exception)

We are removing the [0x0
exception](https://www.chromium.org/flash-roadmap#TOC-PPS-Tiny---Remove-Un-sized-0x0-or-hidden-content-Exceptions-Target:-Chrome-59---June-2017-)
in Chrome 59 (June 2017), which should result in a substantial decrease in
overall Flash usage.

![image](PPS-Tiny---0x0-after.jpg)

## Top Flash Sites (by volume)

Snapshot (2017-04-14) of the per/ origin Flash usage by % of Volume. Link to raw
[data](https://docs.google.com/spreadsheets/d/1efrCE0RysFOqQPagEBxiUoCwdTjT-oVo8bip28fmKDs/edit#gid=730499447).

RankSitePercent1[facebook.com](http://facebook.com)0.0815862[yandex.ru](http://yandex.ru)0.024793[ok.ru](http://ok.ru)0.023024[google.com](http://google.com)0.0223185[youtube.com](http://youtube.com)0.0191086[twitch.tv](http://twitch.tv)0.0185037[yahoo.com](http://yahoo.com)0.0173668[mail.ru](http://mail.ru)0.0147929[vk.com](http://vk.com)0.00973910[amazon.com](http://amazon.com)0.00866611[pornhub.com](http://pornhub.com)0.00785212[friv.com](http://friv.com)0.00757213[naver.com](http://naver.com)0.00738914[aliexpress.com](http://aliexpress.com)0.00717115[xfinity.com](http://xfinity.com)0.0071116[cnn.com](http://cnn.com)0.00686517[uol.com.br](http://uol.com.br)0.00653918[espn.com](http://espn.com)0.00643819[salesforce.com](http://salesforce.com)0.00627820[ebay.com](http://ebay.com)0.00609821Empty0.0060822[hotstar.com](http://hotstar.com)0.00585523[aol.com](http://aol.com)0.0057724[pandora.com](http://pandora.com)0.005525[dailymail.co.uk](http://dailymail.co.uk)0.00547426[bet365.com](http://bet365.com)0.00547227[foxnews.com](http://foxnews.com)0.00529228[msn.com](http://msn.com)0.00511829[baidu.com](http://baidu.com)0.00489330[roblox.com](http://roblox.com)0.00477431[nicovideo.jp](http://nicovideo.jp)0.00474632[sahibinden.com](http://sahibinden.com)0.00471533[paypal.com](http://paypal.com)0.0044934[coolmath-games.com](http://coolmath-games.com)0.0043635[chaturbate.com](http://chaturbate.com)0.00418836[xhamster.com](http://xhamster.com)0.00405937[hao123.com](http://hao123.com)0.00364338[ebay.co.uk](http://ebay.co.uk)0.00357639[intuit.com](http://intuit.com)0.00343640[mynet.com](http://mynet.com)0.00340141[dingit.tv](http://dingit.tv)0.00333742[milliyet.com.tr](http://milliyet.com.tr)0.0033343[yandex.com.tr](http://yandex.com.tr)0.00316144[huffingtonpost.com](http://huffingtonpost.com)0.00315945[y8.com](http://y8.com)0.00315646[weibo.com](http://weibo.com)0.00290247[animaljam.com](http://animaljam.com)0.002948[zillow.com](http://zillow.com)0.00279449[livejasmin.com](http://livejasmin.com)0.001812

## External Sites

*   [w3techs.com](https://w3techs.com/) - Flash Trends
    [Yearly](https://w3techs.com/technologies/history_overview/client_side_language/all/y)
    |
    [Quarterly](https://w3techs.com/technologies/history_overview/client_side_language/all/q)
    |
    [Monthly](https://w3techs.com/technologies/history_overview/client_side_language/all)
